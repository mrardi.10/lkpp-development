<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('dummy', 'PesertaController@dummy');

Route::post('captcha', 'Auth\LoginController@captchaValidate');
Route::get('refreshcaptcha', 'Auth\LoginController@refreshCaptcha');
Route::get('create-hash/{id}', 'RegisterController@destroy');
Route::get('jadwal-inpassing-lkpp', 'MenuController@index');
Route::post('jadwal-inpassing-lkpp', 'MenuController@index');
Route::get('jadwal-inpassing-penyelenggara', 'MenuController@indexTwo');
Route::post('jadwal-inpassing-penyelenggara', 'MenuController@indexTwo');
Route::get('menu-peserta-inpassing/{id}', 'MenuController@show');
Route::get('menu-peserta-instansi/{id}', 'MenuController@showTwo');

Route::get('hasil-ujian/{jenisUjian}/{file}', 'MenuController@HasilUjian');

Route::get('register', 'RegisterController@index');
Route::post('checkemail', 'RegisterController@checkEmail');
Route::post('checknip', 'RegisterController@checkNip');
Route::post('peserta_checkemail', 'PesertaController@checkEmail');
Route::post('peserta_checknip', 'PesertaController@checkNip');
Route::post('peserta_checktelp', 'PesertaController@checkTelp');
Route::post('peserta_checknosertifikat', 'PesertaController@checkNoSertifikat');
Route::post('register-store', 'RegisterController@store');
Route::get('register-info', 'RegisterController@show');
Route::get('prosedur', 'MenuController@prosedur');
Route::get('kontak', 'MenuController@kontak');
Route::get('statik-plaksanaan-ujian', 'MenuController@statik');
Route::post('statik-plaksanaan-ujian', 'MenuController@statik');
Route::get('menu-statik-inpassing/{id}', 'MenuController@statikRegular');
Route::get('menu-statik-instansi/{id}', 'MenuController@statikInstansi');
Route::get('menu-priview-file/{path}/{file}', 'MenuController@FileView');
Route::post('login', [ 'as' => 'login', 'uses' => 'HomeController@login']);
Route::get('verifikasi-email/{hash}', 'RegisterController@verifikasiEmail');
Route::get('lupa-password', 'Auth\ResetPasswordController@index');
Route::post('lupa-password', 'Auth\ResetPasswordController@send');
Route::get('ganti-password/{hash}', 'Auth\ResetPasswordController@reset');
Route::post('ganti-password/{hash}', 'Auth\ResetPasswordController@resetStore');
Route::get('get-kota/{id}','PesertaController@regencies');
Route::get('cek-jenjang/{jenjang}/{id}','PesertaController@cekJenjang');
Route::get('cek-jenjang-edit/{jenjang}/{id}/{jenjang_lama}/{id_lama}','PesertaController@cekJenjangEdit');
Route::get('prosedur-pendaftaran/{path}/{file}', 'MenuController@FileView');
Route::get('get-jadwal/{date}', 'ApiController@listJadwal');
Route::get('insert-jadwal/{jenis_jadwal}/{id}', 'ApiController@getJadwal');
Route::get('get-jenjang/{jenis_jadwal}/{id}', 'ApiController@getJenjang');
Route::get('get-peserta/{jenis_jadwal}/{id}/{jenjang}', 'ApiController@getListPeserta');

		
Route::get('inpassing', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('inpassing/', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register/', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register/', 'Auth\RegisterController@register');


Route::group(['middleware' => ['checkauth']], function () {

	Route::get('priview-file/{path}/{file}', 'HomeController@FileView');
	//ADMIN KPP AKSES ROUTE
	Route::group(['middleware' => ['checkkpp']], function () {
		Route::get('/', 'HomePPKController@index');
		Route::get('/home', 'HomePPKController@index');
		Route::get('biodata-admin-ppk', 'HomePPKController@show');
		Route::get('biodata', 'HomePPKController@index');
		Route::post('biodata-admin-ppk', 'HomePPKController@update');
		Route::get('pengusulan-eformasi', 'EformasiController@index');
		Route::get('tambah-pengusulan-eformasi', 'EformasiController@create');
		Route::post('tambah-pengusulan-eformasi', 'EformasiController@store');
		Route::get('pengajuan-ulang-eformasi/{id}', 'EformasiController@pUlang');
		Route::post('pengajuan-ulang-eformasi/{id}', 'EformasiController@updatepUlang');
		Route::get('berkas-ulang-eformasi/{id}', 'EformasiController@bUlang');
		Route::post('berkas-ulang-eformasi/{id}', 'EformasiController@updatebUlang');
		Route::get('ubah-eformasi/{id}', 'EformasiController@editFormasi');
		Route::post('ubah-eformasi/{id}', 'EformasiController@updateFormasi');
		Route::get('detail-pengajuan-eformasi/{id}', 'EformasiController@showFormasi');
		Route::group(['middleware' => ['checkeformasi']], function () {
		Route::get('data-peserta-ppk', 'PesertaController@index');
		Route::get('tambah-peserta-ppk', 'PesertaController@create');
		Route::post('tambah-peserta-ppk', 'PesertaController@store');
		Route::get('detail-peserta/{id}', 'PesertaController@detail');
		Route::get('edit-peserta/{id}', 'PesertaController@edit');
		Route::post('edit-peserta/{id}', 'PesertaController@updatePeserta');
		Route::get('input-ulang-berkas/{id}', 'PesertaController@InputUlang');
		Route::post('input-ulang-berkas/{id}', 'PesertaController@StoreInputUlang');
		Route::get('hapus-peserta/{id}', 'PesertaController@destroy'); 

		Route::get('jadwal-ujian-lkpp', 'JadwalController@indexPpk');
		Route::post('jadwal-ujian-lkpp', 'JadwalController@indexPpk');
		Route::get('tambah-peserta-ujian/{id}', 'JadwalController@addPeserta');
		Route::post('upload-portofolio', 'JadwalController@uploadPortofolio');
		Route::post('tambah-peserta-ujian/{id}', 'JadwalController@storePeserta');
		Route::get('daftar-instansi', 'JadwalInstansiController@index');
		Route::post('daftar-instansi', 'JadwalInstansiController@index');
		Route::get('permohonan-ujian-instansi', 'JadwalInstansiController@create');
		Route::post('permohonan-ujian-instansi', 'JadwalInstansiController@store');
		Route::get('tambah-peserta-ujian-instansi/{id}', 'JadwalInstansiController@addPeserta');
		Route::post('tambah-peserta-ujian-instansi/{id}', 'JadwalInstansiController@storePeserta');
		});
		Route::get('ganti-password', 'DataAdminController@gantiPassword');
		Route::post('ganti-password', 'DataAdminController@updatePassword');
		Route::get('riwayat-user-ppk/{id}', 'RiwayatUserController@index');
		
		Route::post('unggah-dokumen-ujian', 'JadwalController@unggahDokumen');
		Route::get('hapus-portofolio-dokumen/{id}/{id_peserta}/{id_jadwal}', 'JadwalController@hapusDokumen');
		Route::post('unggah-dokumen-ujian-instansi', 'JadwalInstansiController@unggahDokumen');
		Route::post('unggah-dokumen-ujian-lab', 'JadwalController@unggahDokumenLab');
		Route::get('unggah-dokumen-ujian/{id_peserta}/{id_jadwal}', 'JadwalController@unggahDokumen2');
		Route::get('unggah-dokumen-ujian-instansi/{id_peserta}/{id_jadwal}', 'JadwalInstansiController@unggahDokumen2');
		Route::post('unggah-dokumen-portofolio', 'JadwalController@simpanDokumen');
		// Route::get('create-dummy-file', 'JadwalController@createdummy');
		Route::post('store-dokumen-ujian', 'JadwalController@storeDokumen');
		Route::get('unggah-surat-usulan/{id}/{peserta}', 'JadwalController@unggahUsulan');
		Route::post('unggah-surat-usulan/{id}/{peserta}', 'JadwalController@unggahStoreUsulan');
		Route::get('unggah-surat-usulan-int/{id}/{peserta}', 'JadwalInstansiController@unggahUsulan');
		Route::post('unggah-surat-usulan-int/{id}/{peserta}', 'JadwalInstansiController@unggahStoreUsulan');
	});

	Route::group(['middleware' => ['checklkpp']], function () {
		//dashboard
		Route::get('dashboard', 'HomeController@dashboard');
		Route::get('home-lkpp', 'HomeController@index');

		//pengusulan formasi
		Route::get('pengusulan-eformasi-lkpp', 'EformasiController@indexLkpp');
		Route::get('pengusulan-eformasi-lkpp/json', 'EformasiController@indexLkppJson');
		Route::get('verifikasi-usulan/{id}', 'EformasiController@edit');
		Route::post('verifikasi-usulan/{id}', 'EformasiController@update');
		Route::get('hapus-pengusulan-formasi/{id}', 'EformasiController@destroy');
		Route::get('riwayat-eformasi/{id}', 'EformasiController@riwayat');
		Route::get('pengusulan-eformasi-excell', 'EformasiController@printExcell');

		//data peserta
		Route::get('detail-peserta-lkpp/{id}', 'PesertaController@detailPesertaLkpp');
		Route::get('detail-peserta-lkpp/{id}/{id_jadwal}', 'PesertaController@detailPesertaLkpp1');
		Route::get('detail-peserta-lkpp2/{id}/{id_jadwal}', 'PesertaController@detailPesertaLkpp2');
		Route::get('data-peserta', 'PesertaController@indexLkpp');
		Route::get('verifikasi-peserta/{id}', 'PesertaController@show');
		Route::post('verifikasi-peserta/{id}', 'PesertaController@update');
		Route::post('assign-verifikator-peserta', 'PesertaController@assign');
		Route::get('unassign-verifikator-peserta/{id}', 'PesertaController@unassign');
		Route::get('hapus-peserta-lkpp/{id}', 'PesertaController@destroyLkpp');
		Route::get('data-peserta-excell', 'PesertaController@printExcell');

		//data admin ppk
		Route::get('data-admin', 'DataAdminController@index');
		Route::get('hapus-admin-ppk/{id}', 'DataAdminController@destroy');
		Route::get('detail-admin-ppk/{id}', 'DataAdminController@show');
		Route::get('update-status-admin-ppk/{id}', 'DataAdminController@updateStatus');
		Route::get('update-ulang-admin-ppk/{id}', 'DataAdminController@updateUlangAktivasi');
		Route::get('updateNot-status-admin-ppk/{id}', 'DataAdminController@updateStatusNot');
		Route::get('ganti-password-admin-ppk/{id}', 'DataAdminController@editPassword');
		Route::post('ganti-password-admin-ppk/{id}', 'DataAdminController@updatePasswordLkpp');
		Route::get('data-admin-excell', 'DataAdminController@printExcell');

		//data admin lkpp
		Route::get('data-admin-lkpp', 'DataAdminController@indexLkpp');
		Route::get('tambah-admin-lkpp', 'DataAdminController@createLkpp');
		Route::post('tambah-admin-lkpp', 'DataAdminController@storeLkpp');
		Route::get('edit-admin-lkpp/{id}', 'DataAdminController@editLkpp');
		Route::post('edit-admin-lkpp/{id}', 'DataAdminController@updateLkpp');
		Route::get('hapus-admin-lkpp/{id}', 'DataAdminController@destroyLkpp');

		Route::get('riwayat-ujian-peserta', 'RiwayatUserController@ujianPeserta');
		Route::post('riwayat-ujian-peserta', 'RiwayatUserController@ujianPeserta');
		Route::get('riwayat-ujian-peserta-inpassing-excell', 'RiwayatUserController@printExcell');

		//jadwal inpassing
		Route::get('jadwal-inpassing', 'JadwalController@index');
		Route::post('jadwal-inpassing', 'JadwalController@index');
		Route::get('tambah-jadwal-inpassing', 'JadwalController@create');
		Route::post('tambah-jadwal-inpassing', 'JadwalController@store');
		Route::get('edit-jadwal-inpassing/{id}', 'JadwalController@edit');
		Route::post('edit-jadwal-inpassing/{id}', 'JadwalController@update');
		Route::get('hapus-jadwal-inpassing/{id}', 'JadwalController@destroy');
		Route::get('peserta-jadwal-inpassing/{id}', 'JadwalController@show');
		Route::get('verifikasi-portofolio-peserta/{nip}/{id}', 'JadwalController@verifPortofolio');
		Route::post('verifikasi-portofolio-peserta/{nip}/{id}', 'JadwalController@StoreVerifPortofolio');
		Route::get('hasil-tes-peserta/{nip}/{id}', 'JadwalController@inputHasil');
		Route::post('hasil-tes-peserta/{nip}/{id}', 'JadwalController@storeHasil');
		Route::post('pindah-jadwal/{id}/{id_jadwal}', 'JadwalController@pindahJadwal');
		Route::get('cetak-absensi-reguller/{id}', 'JadwalController@printAbsensi');
		Route::post('hapus-peserta-jadwal-lkpp/{id}', 'PesertaController@destroyJadwalLkpp');
		Route::get('peserta-jadwal-inpassing-excell/{id}', 'JadwalController@printExcell');
		
		//jadwal instansi
		Route::get('jadwal-inpassing-instansi', 'JadwalInstansiController@indexLkpp');
		Route::post('jadwal-inpassing-instansi', 'JadwalInstansiController@indexLkpp');
		Route::get('verifikasi-permohonan-instansi/{id}', 'JadwalInstansiController@edit');
		Route::post('verifikasi-permohonan-instansi/{id}', 'JadwalInstansiController@update');
		Route::get('lihat-peserta-instansi/{id}', 'JadwalInstansiController@show');
		Route::get('hasil-tes-peserta-instansi/{nip}/{id}', 'JadwalInstansiController@inputHasil');
		Route::post('hasil-tes-peserta-instansi/{nip}/{id}', 'JadwalInstansiController@storeHasil');
		Route::get('hapus-jadwal-instansi/{id}', 'JadwalInstansiController@destroy');
		Route::get('cetak-absensi-instansi/{id}', 'JadwalInstansiController@printAbsensi');
		Route::post('hapus-peserta-instansi-jadwal-lkpp/{id}', 'PesertaController@destroyJadwalInstansiLkpp');
		Route::get('lihat-peserta-instansi-excell/{id}', 'JadwalInstansiController@printExcell');

		//verifikasi portofolio
		Route::get('verifikasi-portofolio', 'VerifikasiPortofolioController@index');
		Route::get('verifikasi-portofolio-detail/{id}', 'VerifikasiPortofolioController@show');
		Route::get('edit-hasil-pleno/{id}', 'VerifikasiPortofolioController@edit');
		Route::post('edit-hasil-pleno/{id}', 'VerifikasiPortofolioController@store');

		//data verifikator
		Route::get('data-verifikator', 'VerifikatorController@index');
		Route::get('tambah-verifikator', 'VerifikatorController@create');
		Route::post('tambah-verifikator', 'VerifikatorController@store');
		Route::get('ubah-verifikator/{id}', 'VerifikatorController@edit');
		Route::post('ubah-verifikator/{id}', 'VerifikatorController@update');
		Route::get('detail-verifikator/{id}', 'VerifikatorController@show');
		Route::get('hapus-verifikator/{id}', 'VerifikatorController@destroy');

		//data asesor
		Route::get('data-asesor', 'AsesorController@index');
		Route::get('tambah-asesor', 'AsesorController@create');
		Route::post('tambah-asesor', 'AsesorController@store');
		Route::get('detail-asesor/{id}', 'AsesorController@show');
		Route::get('ubah-asesor/{id}', 'AsesorController@edit');
		Route::post('ubah-asesor/{id}', 'AsesorController@update');
		Route::get('hapus-asesor/{id}', 'AsesorController@destroy');
		Route::post('assign-asesor', 'AsesorController@assign');

		//setingg
		//text running
		Route::get('text-running', 'TextRunningController@index');
		Route::get('prosesmasuk', 'TextRunningController@prosesmasuk');
		Route::get('tambah-text-running', 'TextRunningController@create');
		Route::post('tambah-text-running', 'TextRunningController@store');
		Route::get('edit-text-running/{id}', 'TextRunningController@edit');
		Route::post('edit-text-running/{id}', 'TextRunningController@update');
		Route::get('hapus-text-running/{id}', 'TextRunningController@destroy');
		Route::get('riwayat-user-lkpp/{id}', 'RiwayatUserController@indexlkpp');
		
		//data instansi
		Route::get('data-instansi', 'InstansiController@index');
		Route::get('tambah-instansi', 'InstansiController@create');
		Route::post('tambah-instansi', 'InstansiController@store');
		Route::get('edit-instansi/{id}', 'InstansiController@edit');
		Route::post('edit-instansi/{id}', 'InstansiController@update');
		Route::get('hapus-instansi/{id}', 'InstansiController@destroy');

		//menu prosedur
		Route::get('data-prosedur', 'ProsedurController@index');
		Route::get('tambah-berkas-prosedur', 'ProsedurController@create');
		Route::post('tambah-berkas-prosedur', 'ProsedurController@store');
		Route::get('edit-berkas-prosedur/{id}', 'ProsedurController@editBerkas');
		Route::post('edit-berkas-prosedur/{id}', 'ProsedurController@updateBerkas');
		Route::get('edit-prosedur/{id}', 'ProsedurController@edit');
		Route::post('edit-prosedur/{id}', 'ProsedurController@update');
		Route::get('hapus-berkas-prosedur/{id}', 'ProsedurController@destroy');

		//menu statistik
		Route::get('data-statistik', 'StatistikController@index');
		Route::get('tambah-statistik', 'StatistikController@create');
		Route::post('tambah-statistik', 'StatistikController@store');
		Route::get('edit-statistik/{id}', 'StatistikController@edit');
		Route::post('edit-statistik/{id}', 'StatistikController@update');
		Route::get('hapus-statistik/{id}', 'StatistikController@destroy');


		//deskripsi portofolio
		Route::get('data-deskripsi-portofolio', 'JadwalController@indexDeskripsi');
		Route::get('tambah-deskripsi-portofolio', 'JadwalController@createDeskripsi');
		Route::post('tambah-deskripsi-portofolio', 'JadwalController@storeDeskripsi');
		Route::get('edit-deskripsi-portofolio/{id}', 'JadwalController@editDeskripsi');
		Route::post('edit-deskripsi-portofolio/{id}', 'JadwalController@updateDeskripsi');
		Route::get('hapus-deskripsi-portofolio/{id}', 'JadwalController@destroyDeskripsi');

		//menu kontak
		Route::get('data-kontak', 'KontakController@index');
		Route::get('tambah-kontak', 'KontakController@create');
		Route::post('tambah-kontak', 'KontakController@store');
		Route::get('edit-kontak/{id}', 'KontakController@edit');
		Route::post('edit-kontak/{id}', 'KontakController@update');
		Route::get('hapus-kontak/{id}', 'KontakController@destroy');

		//menu register
		Route::get('data-register', 'MenuRegisterController@index');
		Route::get('tambah-register', 'MenuRegisterController@create');
		Route::post('tambah-register', 'MenuRegisterController@store');
		Route::get('edit-register/{id}', 'MenuRegisterController@edit');
		Route::post('edit-register/{id}', 'MenuRegisterController@update');
		Route::get('hapus-register/{id}', 'MenuRegisterController@destroy');

		//generate no ser
		Route::get('generate-noseri', 'GenerateSeriController@index');
		Route::post('generate-noseri', 'GenerateSeriController@store');

		//Cetak Sertifikat
		Route::get('cetak-sertifikat', 'CetakSertifikatController@index');
		Route::get('cetak-sertifikat-peserta/{tipe}/{id}', 'CetakSertifikatController@show');
		Route::get('print-sertifikat-peserta/{tipe}/{id}', 'CetakSertifikatController@print');
		Route::get('print-sertifikat-belakang/{tipe}/{id}/{id_jadwal}', 'CetakSertifikatController@templateBelakang');
		Route::get('edit-sertifikat-peserta/{tipe}/{id}/{id_jadwal}', 'CetakSertifikatController@editSertifikat');
		Route::post('edit-punya-sertifikat', 'CetakSertifikatController@sertifikatEdit');
		Route::get('data-sertifikat', 'CetakSertifikatController@data');
		Route::post('data-sertifikat', 'CetakSertifikatController@storedata');

		// upload hasil ujian
		Route::get('up-hasil-ujian/{id}', 'JadwalController@upHasilUjian');
		Route::post('up-hasil-ujian/{id}', 'JadwalController@StoreupHasilUjian');

		// upload hasil ujian Intansi
		Route::get('up-hasil-ujian-instansi/{id}', 'JadwalInstansiController@upHasilUjian');
		Route::post('up-hasil-ujian-instansi/{id}', 'JadwalInstansiController@StoreupHasilUjian');
		

		//Cetak SKTL (Surat Keterangan Tidak Lulus)
		Route::get('cetak-sktl', 'CetakSktlController@index');
		Route::get('cetak-sktl-peserta/{tipe}/{id}', 'CetakSktlController@show');
		Route::get('print-sktl-peserta/{tipe}/{id}', 'CetakSktlController@print');
		Route::get('print-sktl-peserta-word/{tipe}/{id}', 'CetakSktlController@printWord');

		//import hasil ujian
		Route::get('import-hasil-ujian', 'ImportUjianController@index');
		Route::post('import-hasil-ujian', 'ImportUjianController@store');
		Route::post('cek-tgl-import-ujian', 'ImportUjianController@cekTanggal');
		Route::get('cek-tgl-import-ujian', 'ImportUjianController@cekTanggal');

		//Pertek SPHU
		Route::get('pertek-sphu', 'PertekSphuController@index');
		Route::get('tambah-pertek', 'PertekSphuController@createPertek');
		Route::post('tambah-pertek', 'PertekSphuController@createPertek');
		// Route::get('search-instansi-pertek', 'PertekSphuController@searchInstansi');

		Route::get('get-peserta-pertek/{id}','PertekSphuController@PesertaPertek');
		Route::get('tambah-sphu', 'PertekSphuController@createSPHU');
		Route::post('tambah-sphu', 'PertekSphuController@createSPHU');
		Route::post('tambah-sphu-aksi', 'PertekSphuController@storeSPHU');
		Route::post('tambah-pertek-aksi', 'PertekSphuController@storePertek');

		Route::get('draft-pertek', 'PertekSphuController@DraftPertek');
		Route::get('draft-sphu', 'PertekSphuController@DraftSPHU');

		Route::get('edit-pertek/{id}', 'PertekSphuController@editPertek');
		Route::post('edit-pertek/{id}', 'PertekSphuController@updatePertek');

		Route::get('up-draft-pertek/{id}', 'PertekSphuController@upPertek');
		Route::post('up-draft-pertek/{id}', 'PertekSphuController@upPertek');

		Route::get('edit-sphu/{id}', 'PertekSphuController@editSPHU');
		Route::post('edit-sphu/{id}', 'PertekSphuController@updateSPHU');

		Route::get('up-draft-sphu/{id}', 'PertekSphuController@upSPHU');
		Route::post('up-draft-sphu/{id}', 'PertekSphuController@upSPHU');

		Route::post('upload-pertek/{id}', 'PertekSphuController@uploadFile');

		Route::post('upload-sphu/{id}', 'PertekSphuController@uploadSphu');

		Route::get('download-pertek-sphu/{id}', 'PertekSphuController@downloadFile');

		Route::get('download-sphu/{id}', 'PertekSphuController@downloadSphu');

		Route::get('download-sphu-word/{id}', 'PertekSphuController@downloadSphuWord');

		Route::post('ganti-ak', 'PertekSphuController@storeAK');

		Route::get('hapus-pertek/{id}', 'PertekSphuController@destroyPertek');
		Route::get('hapus-sphu/{id}', 'PertekSphuController@destroySPHU');

		Route::get('get-no-surat/{id}','PertekSphuController@NoSurat');
		Route::post('upd-status-porto/{nip}/{id}','JadwalController@statusPorto');
		

		
	});

	// Route::get('verifikasi-portofolio-detail', function () {
	// 	return view('verifikasi_portofolio');
	// });

	Route::get('jadwal&daftar-regulerLKPP', function () {
		return view('jadwal&daftar-regulerLKPP');
	});

	Route::get('tanggal-verifikasi', function () {
		return view('tanggal_verifikasi');
	});

	Route::get('verifikasi-dokumen', function() {
		return view('verifikasi_dokumen');
	});

	// Route::get('home', function() {
	// 	return view('home1');
	// });

	Route::get('jadwal-inpassing-reguler', function() {
		return view('jadwal_inpassing_reguler1');
	});

	Route::get('jadwal-verifikasi-portofolio', function() {
		return view('jadwal_verifikasi_portofolio');
	});

	Route::get('instansi', function() {
		return view('instansi');
	});


	Route::get('cetak-pertek', function() {
		return view('cetak_pertek');
	});

	Route::get('daftar-peraturan', function() {
		return view('daftar_peraturan');
	});

	Route::get('data-pengusulan-eformasi', function() {
		return view('data_pengusulan_eformasi');
	});

Route::get('peserta-ujian', function() {
	return view('peserta_ujian');
});

Route::get('permohonan-instansi', function() {
	return view('permohonan_instansi');
});

});