@extends('layout.app')

@section('title')
Data Admin PPK
@stop

@section('css')
<style type="text/css">
	.div-print{
		text-align: right;
	}
	.div-print button{
		text-align: right;
		color: black;
	}
</style>
@stop

@section('content')
@if (session('msg'))
@if (session('msg') == "berhasil")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil simpan data</strong>
		</div>
	</div>
</div> 
@endif
@if (session('msg') == "gagal")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal simpan data</strong>
		</div>
	</div>
</div> 
@endif

@if (session('msg') == "berhasil_update")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil ubah data</strong>
		</div>
	</div>
</div> 
@endif
@if (session('msg') == "gagal_update")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal ubah data</strong>
		</div>
	</div>
</div> 
@endif

@if (session('msg') == "berhasil_ganti")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil ubah password</strong>
		</div>
	</div>
</div> 
@endif
@if (session('msg') == "gagal_ganti")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal Ubah Password</strong>
		</div>
	</div>
</div> 
@endif
@if (session('msg') == "berhasil_hapus")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil Hapus Akun</strong>
		</div>
	</div>
</div> 
@endif
@if (session('msg') == "gagal_hapus")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal Hapus Akun</strong>
		</div>
	</div>
</div> 
@endif
@if (session('msg') == "berhasil_aktif_ulang")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil Kirim Email AKtivasi Ulang</strong>
		</div>
	</div>
</div> 
@endif
@if (session('msg') == "gagal_aktif_ulang")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil Kirim Email AKtivasi Ulang</strong>
		</div>
	</div>
</div> 
@endif
@endif
<div class="main-box">
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2">
				<select name='length_change' id='length_change' class="form-control">
					<option value='50'>50</option>
					<option value='100'>100</option>
					<option value='150'>150</option>
					<option value='200'>200</option>
				</select>
			</div>
			<div class="col-md-4 col-12">
				<div class="input-group">
					<div class="input-group addon">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
						<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
					</div>
				</div>
			</div>
			<div class="col-md-5 col-12 div-print">
					<a href="{{ url('data-admin-excell') }}"><button class="btn btn-success" style="color: #fff"><i class="fa fa-print"></i> Print Excel</button></a>
			</div>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No.</th>
					<th>Nama</th>
					<th style="width: 20%;">Instansi</th>
					<th>Email</th>
					<th>No. Telepon/HP</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($data as $key => $datas)
				<tr>
					<td>{{ $key++ + 1 }}</td>
					<td>{{ $datas->name }}</td>
					<td style="width: 20%;">{{ $datas->instansis }}</td>
					<td>{{ $datas->email }}</td>
					<td>{{ $datas->no_telp }}</td>
					<td>{{ $datas->email_verified_at != "" ? 'Aktif' : 'Tidak Aktif' }}</td>
					<td>
						<div class="dropdown">
							<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
							<ul class="dropdown-menu">
								<li><a href="{{ url('detail-admin-ppk/'.$datas->id) }}">Lihat Detail</a></li>
								@if (Auth::user()->role == 'superadmin')
								@if ($datas->email_verified_at == "")
								<li><a href="#" data-toggle="modal" data-target="#modal-email{{ $datas->id }}">Verifikasi Email Admin</a></li>
								@else 
								<li><a href="#" data-toggle="modal" data-target="#modal-email{{ $datas->id }}">Verifikasi Email Admin</a></li>
								@endif
								<li><a href="#" data-toggle="modal" data-target="#modal-email-kirim{{ $datas->id }}">Kirim Email Aktivasi Ulang</a></li>
								<li><a href="{{ url('ganti-password-admin-ppk/'.$datas->id) }}">Ubah Password</a></li>
								<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $datas->id }}">Hapus</a></li>
								@endif
							</ul>
						</div> 
					</td>
				</tr>
				<div class="modal fade" id="modal-email{{ $datas->id }}">
					<div class="modal-dialog" style="width:30%">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Verifikasi Email Admin PPK</h4>
								</div>
								<div class="modal-body">
									<p>Apakah Anda yakin akan melakukan verifikasi email Admin PPK?</p>
								</div>
								<div class="modal-footer">
									<a href="{{ url('update-status-admin-ppk/'.$datas->id) }}" type="button" class="btn btn-primary pull-left">AKTIF</a>
									<a href="{{ url('updateNot-status-admin-ppk/'.$datas->id) }}" type="button" class="btn btn-default pull-right">TIDAK AKTIF</a>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<div class="modal fade" id="modal-email-kirim{{ $datas->id }}">
					<div class="modal-dialog" style="width:30%">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Kirim Email Aktivasi Ulang Admin PPK</h4>
								</div>
								<div class="modal-body">
									<p>Apakah Anda yakin akan melakukan Kirim Email Aktivasi Ulang Admin PPK?</p>
								</div>
								<div class="modal-footer">
									<a href="{{ url('update-ulang-admin-ppk/'.$datas->id) }}" type="button" class="btn btn-primary pull-left">KIRIM</a>
									<button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
				<div class="modal fade" id="modal-default{{ $datas->id }}">
					<div class="modal-dialog" style="width:30%">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Hapus Admin PPK</h4>
								</div>
								<div class="modal-body">
									<p>Apakah Anda yakin menghapus Data Admin PPK?</p>
								</div>
								<div class="modal-footer">
									<a href="{{ url('hapus-admin-ppk/'.$datas->id) }}" type="button" class="btn btn-primary pull-left">HAPUS</a>
									<button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					@endforeach
			</tbody>
			</table>
		</div>
	</div> 
	@stop