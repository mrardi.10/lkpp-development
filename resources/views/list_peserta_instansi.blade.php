@extends('layout.app')

@section('title')
Peserta Uji Kompetensi (Instansi)
@endsection

@section('css')
<style>
	.dropdown-menu{
		left: -130px;
	}

	ul.dropdown-menu{
		right: 0px;
		left: auto;
	}
	.btn-default2{
		margin: 10px 10px 0px 5px !important;
	}
	.div-print{
		text-align: right;
	}
	.div-print button{
		text-align: right;
		color: black;
	}
</style>
@endsection

@section('content')
@if (session('msg'))
@if (session('msg') == "berhasil")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil simpan data</strong>
		</div>
	</div>
</div>
@endif 
@if (session('msg') == "gagal")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal simpan data</strong>
		</div>
	</div>
</div> 
@endif
@if (session('msg') == "berhasil_hapus")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil hapus data</strong>
		</div>
	</div>
</div>
@endif 
@if (session('msg') == "gagal_hapus")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal hapus data</strong>
		</div>
	</div>
</div> 
@endif
@endif
<div class="row">
	<div class="col-md-12">
		<h3>{{ $jadwal->metode == 'tes_tulis' ? Helper::tanggal_indo($jadwal->tanggal_tes) : Helper::tanggal_indo($jadwal->tanggal_verifikasi) }} Metode Ujian : {{ $jadwal->metode == 'tes_tulis' ? "Tes Tertulis" : "Verifikasi Portofolio" }}</h3>
	</div>
</div>
<div class="main-box">
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2">
				<select name='length_change' id='length_change' class="form-control">
					<option value='50'>50</option>
					<option value='100'>100</option>
					<option value='150'>150</option>
					<option value='200'>200</option>
				</select>
			</div>
			<div class="col-md-4 col-12">
				<div class="input-group">
					<div class="input-group addon">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
						<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
					</div>
				</div>
			</div>
			<div class="col-md-5 col-12 div-print">
					<a href="{{ url('lihat-peserta-instansi-excell/'.$id) }}"><button class="btn btn-success" style="color: #fff"><i class="fa fa-print"></i> Print Excel</button></a>
			</div>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>NIP</th>
					{{-- <th>Instansi</th> --}}
					<th>Pangkat/Gol.</th>
					<th>Jenjang</th>
					<th>Foto</th>
					<th>Status</th>
					@if($jadwal->metode == 'tes_tulis')
					<th>Verifikator</th>
					@endif
					@if($jadwal->metode == 'verifikasi_portofolio')
					<th>Asesor</th>
					@endif
					@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'verifikator' || Auth::user()->role == 'dsp' || Auth::user()->role == 'asesor')
					<th>Aksi</th>
					@endif
				</tr>
			</thead>
			<tbody>
				@foreach ($data as $key => $datas)
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->id == $datas->asesor|| Auth::user()->id == $datas->assign)
				<tr>
					<td>{{ $key++ + 1 }}</td>
					<td>{{ $datas->nama }}</td>
					<td>{{ $datas->nip }}</td>
					{{-- <td>{{ $datas->nama_instansi }}</td> --}}
					<td>{{ $datas->jabatan }}</td>
					<td>{{ ucwords($datas->jenjang) }}</td>
					<td>
						@if ($datas->fotos == "")
						-
						@else
						@php
						$foto = explode('.',$datas->fotos);
						@endphp
						@if ($foto[1] == 'pdf' || $foto[1] == 'PDF')
						<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$datas->fotos }}" target="_blank"><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="Lihat Foto"></i></a>
						@else
						<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$datas->fotos }}" target="_blank"><img src="{{ asset('storage/data/pas_foto_3_x_4/'.$datas->fotos) }}" alt="" class="img-responsive" width="60px"></a>
						@endif
						@endif
					</td>
					@if($datas->status == 'lulus' && $datas->publish == 'publish')
					<td>{{ 'Lulus' }}</td>
					@elseif($datas->status == 'tidak_lulus' && $datas->publish == 'publish')
					<td>{{ 'Tidak Lulus' }}</td>
					@elseif($datas->status == 'tidak_hadir' && $datas->publish == 'publish')
					<td>{{ 'Tidak Hadir' }}</td>
					@else
					<td>{{ $datas->statuss }}</td>
					@endif

					@if($jadwal->metode == 'tes_tulis')
					<td>{{ $datas->verifikators == "" ? "-" : $datas->verifikators }}</td>
					@endif
					@if($jadwal->metode == 'verifikasi_portofolio')
					<td>{{ $datas->asesors == "" ? "-" : $datas->asesors }}</td>
					@endif
					@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'verifikator' || Auth::user()->role == 'dsp' || Auth::user()->role == 'asesor')
					<td>
						<div class="dropdown">
							<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
							<ul class="dropdown-menu">
								@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof')
										<li><a href="#" data-toggle="modal" data-target="#myModal{{ $datas->id }}">Assign Verifikator Dokumen Persyaratan</a></li>
								{{-- <li><a href="#">Assign Assesor</a></li> --}}
								@endif
								@if (Auth::user()->role == 'superadmin')
										<li><a href="{{ url('verifikasi-peserta/'.$datas->id) }}">Verifikasi Dokumen Persyaratan</a></li>
								@endif

								@if (Auth::user()->role == 'verifikator')
								@if ($datas->assign == Auth::user()->id )
										<li><a href="{{ url('verifikasi-peserta/'.$datas->id) }}">Verifikasi Dokumen Persyaratan</a></li>	
								@endif
								@endif

								@if($datas->metodes == "verifikasi")
										<li><a href="#">Verifikasi Portofolio</a></li>
								@endif
								@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
										<li><a href="{{ url('hasil-tes-peserta-instansi')."/".$datas->nip."/".$datas->ids }}">Input Hasil Tes</a></li>
								@endif

								@if($jadwal->metode == 'verifikasi_portofolio')
								@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'verifikator' || Auth::user()->role == 'dsp' || Auth::user()->role == 'asesor')
										<li><a href="{{ url('detail-peserta-lkpp2/'.$datas->id."/".$datas->ids) }}">Lihat Detail</a></li>
								@endif
								@endif

								@if($jadwal->metode == 'tes_tulis')
								@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'verifikator' || Auth::user()->role == 'dsp' || Auth::user()->role == 'asesor')
								<li><a href="{{ url('detail-peserta-lkpp/'.$datas->id) }}">Lihat Detail</a></li>
								@endif
								@endif

								@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp' || Auth::user()->role == 'bangprof')
								<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $datas->id }}">Hapus</a></li>
								@endif
							</ul>
						</div>
					</td>
					@endif
				</tr>
				@endif
				<!-- Modal -->
				<div class="modal fade" id="myModal{{ $datas->id }}" role="dialog">
					<div class="modal-dialog">
						<form method="post" action="{{ url('assign-verifikator-peserta') }}">
							@csrf
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Assign Verifikator Dokumen Persyaratan</h4>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-md-3">
											Nama Peserta
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->nama }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Instansi
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->nama_instansis }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Pangkat/Gol.
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->jabatan }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Jenjang
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->jenjang }}
										</div>
									</div>
									<div class="row" style="margin-top:5px">
										<div class="col-md-3">
											Nama Verifikator
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{!! Form::select('verifikator', $admin_bangprof , $datas->assign, ['placeholder' => 'pilih verifikator', 'class' => 'form-control', 'id' => 'select-single', 'required']); !!}
										</div>
										<input type="hidden" name="id_peserta" value="{{ $datas->id }}">
										<input type="hidden" name="nama_peserta1" value="{{ $datas->nama }}">
										<input type="hidden" name="modal" value="myModal{{ $datas->id }}">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-sm btn-default2" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-sm btn-default1">Assign</button>
								</div>
							</div>
						</form>
					</div>
				</div>

				<!-- Modal Hapus -->
				<div class="modal fade" id="modal-default{{ $datas->id }}" role="dialog">
					<div class="modal-dialog" style="width:30%">
						<form method="post" action="{{ url('hapus-peserta-instansi-jadwal-lkpp')."/".$datas->id_pesertas }}">
							@csrf
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Hapus Peserta</h4>
								</div>
								<div class="modal-body">
									<p>Apakah Anda yakin menghapus Peserta dari jadwal?</p>
								</div>
								<input type="hidden" name="metodes"  readonly="" value="{{$datas->metodes == "tes_tulis" ? "Tes Tertulis" : "Verifikasi Portofolio"}}" class="metodes">
								<input type="hidden" name="id_jadwal"  readonly=""  value="{{$datas->jadwal_peserta}}" class="metodes">
								<input type="hidden" name="tgl_uji"  readonly=""  value="{{ Helper::tanggal_indo($datas->tgl_uji)}}" class="metodes">
								<div class="modal-footer">
									{{-- <a href="{{ url('hapus-peserta-instansi-jadwal-lkpp')."/".$datas->id }}" type="button" class="btn btn-primary pull-left">HAPUS</a> --}}
									<button type="submit" class="btn btn-sm btn-default1 pull-left">HAPUS</button>
									<button type="button" class="btn btn-sm btn-default2" data-dismiss="modal">BATAL</button>
								</div>
							</div>
						</form>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>

				@endforeach
			</tbody>
		</table>
	</div>
</div>    
@endsection