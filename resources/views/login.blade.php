@extends('layout.app2')

@section('title')
Login KPP
@stop

@section('css')
  <style>
    body{
      width: 100%;
    }

    .bg-top{
      width: 100%;
      background-image: url('{{ asset('assets/img/back.jpg') }}'); /* The image used */
      background-color: #cccccc; /* Used if the image is unavailable */
      height: 300px; /* You must set a specified height */
      background-position: center; /* Center the image */
      background-repeat: no-repeat; /* Do not repeat the image */
      background-size: cover; /* Resize the background image to cover the entire container */
    }

    .info-txt{
      padding: 50px;
    }

    .info-txt hr{
      border-top: 1px solid rgba(0, 0, 0, 0.43);
    }

    .txt-tp{
      font-weight: 600;
    }

    .txt-btm{
      font-size: 22px;
    }

    .lbl-form{
      width: 100%;
      font-weight: 600;
      color: gray;
    }

    .card-head{
      color: white;
      padding: 10px 20px 10px 20px;
      background: #ff4141;
      margin: -1px;
    }

    .card-head h3{
      font-weight: 600;
    }

    .card-body{
      padding-bottom: 10px;
    }

    .card-bottom{
      flex: 1 1 auto;
      padding: 0px 20px 10px 20px;
      text-align: right;
    }

    .card{
      margin-top: -110px;
      -webkit-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
      -moz-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
      box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
    }

    .card .form-control{
      -webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
      -moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
      box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    }

    .btn-log{
      background: #ff4141;
      font-weight: 600;
      color: white;
      width: 100px;
    }

    .btn-daf{
      background: slategray;
      font-weight: 600;
      color: white;
      width: 70px;
    }

    .p-daf{
      margin: 15px;
    }

    @media(min-width: 320px) and (max-width: 768px) {
      .card{
        margin-top: 10px;
      } 
    }

    .running{
      height: 20px;
      background: #f2f2f2;
    }
  </style>
@endsection

@section('content')
<div class="bg-top">

</div>
<div class="running">
  <div class="container-fluid">
    <h3>Hello</h3>
  </div>
</div>
<div class="container-fluid">
 <div class="row">
  <div class="col-lg-9 col-md-12 info-txt" style="padding: 50px !important;">
    <h5 class="txt-tp">Selamat datang di Sistem Informasi Penyesuaian/Inpassing JF PPBJ</h5>
    <hr>
    <p class="txt-btm">Sebelum mendaftar silahkan membaca <a href="{{ url('prosedur') }}">Prosedur Pendaftaran</a> Ujian terlebih dahulu</p><p><br><br> <b>HATI-HATI!</b><br>Banyak penipuan mengatasnamakan LKPP, baik undangan pelatihan maupun ujian palsu.<br>LKPP tidak memperjualbelikan kunci jawaban dan soal.</p>
  </div>
  <div class="col-lg-3 col-md-12">
    <div class="card">
      <div class="card-head"><h3>Masuk</h3></div>
      <div class="card-body">
        <label for="email" class="lbl-form">Email
          <input type="email" name="email" id="email" class="form-control">
        </label>
        <label for="password" class="lbl-form">Password
            <input type="password" name="password" id="password" class="form-control">
          </label>
      </div>
      <div class="card-bottom">
        <button type="submit" class="btn btn-sm btn-log">Masuk</button>
      </div>
    </div>
    <p class="p-daf">Belum Punya Akun? Klik <a href="{{ url('register') }}"><button class="btn btn-sm btn-daf">Daftar</button></a> </p>
  </div>
 </div>
</div>
@endsection