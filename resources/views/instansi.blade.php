@extends('layout.app')

@section('title')
Instansi
@stop

@section('css')
<style type="text/css">
	td{
		text-align: center;
	}

	.tab{
		margin-bottom: 10px;
	}

	.arr-right .breadcrumb-item+.breadcrumb-item::before {
 
  content: "›";
 
  vertical-align:top;
 
  color: #408080;
 
  font-size:35px;
 
  line-height:18px;
 
}
</style>
@stop

@section('content')
<div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
              	<div class="row">
              		<div class="col-sm-2">
              			<div class="dataTables_length" id="example1_length">
              				<label>Perlihatkan 
              					<select name="example1_length" aria-controls="example1" class="form-control input-sm">
              					<option value="10">10</option>
              					<option value="25">25</option>
              					<option value="50">50</option>
              					<option value="100">100</option>
              					</select> entries
              				</label>
              				</div>
              			</div>
              			<div class="col-sm-8">
              				<div id="example1_filter" class="dataTables_filter">
              					<label>Search : <input type="search" class="form-control input-sm" placeholder="" aria-controls="example1"></label>
              				</div>
              			</div>
              			
              		</div>
              		<div class="row">
              			<div class="col-sm-12">
              				<table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead class="htable">
                <tr role="row">
                	<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 182px;"> No. </th>
                	<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 224px;">Nama </th>
                	<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 199px;">NIP</th>
                	<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 156px;">Instansi</th>
                	<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">Pang/Gol</th>
                	<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">Jenjang</th>
                	<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">Foto</th>
                	<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">Status Verif & Persyaratan</th>
                  <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">Metode Ujian</th>
                	<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                	<tr>
                		<td></td>
                		<td></td>
                		<td></td>
                		<td></td>
                		<td></td>
                		<td></td>
                		<td></td>
                		<td></td>
                    <td></td>
                		<td>
                <div class="btn-group">
                  <!-- <button type="button" class="btn btn-default">Action</button> -->
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <!-- <li><a href="#">Edit</a></li>
                    <li><a href="#">Lihat</a></li>
                    <li><a href="#">Tambah</a></li> -->
                    <li class="divider"></li>
                    <li><a href="{{ url('jadwal-inpassing-reguler')}}">Lihat</a></li>
                  </ul>
                </div></td>
                	</tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                <div class="btn-group">
                  <!-- <button type="button" class="btn btn-default">Action</button> -->
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <!-- <li><a href="#">Edit</a></li>
                    <li><a href="#">Lihat</a></li>
                    <li><a href="#">Tambah</a></li> -->
                    <li class="divider"></li>
                    <li><a href="{{ url('jadwal-inpassing-reguler')}}">Lihat</a></li>
                  </ul>
                </div></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                <div class="btn-group">
                  <!-- <button type="button" class="btn btn-default">Action</button> -->
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <!-- <li><a href="#">Edit</a></li>
                    <li><a href="#">Lihat</a></li>
                    <li><a href="#">Tambah</a></li> -->
                    <li class="divider"></li>
                    <li><a href="">Lihat</a></li>
                  </ul>
                </div></td>
                  </tr>

                	
                
            	</tbody>

              </table>



@stop