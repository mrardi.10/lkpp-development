@extends('layout.app')
@section('title')
    Edit Data Sertifikat    
@endsection
@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }
</style>
@endsection
@section('content')
<form action="" method="post">
@csrf
	<div class="main-box">
		<div class="container">
			<div class="row">
				<div class="col-md-10">
					@if (session('msg'))					
						@if (session('msg') == "berhasil")
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>  Berhasil simpan data </strong>
							</div>
						@else
							<div class="alert alert-danger alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>  Gagal simpan data </strong>
							</div>
						@endif
					@endif
				</div>
				<div class="col-md-12">
					<h3>Edit Data Sertifikat</h3><hr>
				</div>
			</div>
			@if($data != "")
				@foreach($data as $datas)
				<div class="row">
					<div class="col-md-3 col-xs-10">
						Nama Deputi
					</div>
					<div class="col-md-1 col-xs-1">:</div>
						<div class="col-md-5 col-xs-12">
							<div class="form-group">
								<input type='text' class="form-control" name="nama" value="{{ $datas->nama_deputi }}" required />
							</div>
							<span class="errmsg">{{ $errors->first('nama') }}</span>
						</div>
				</div>
				<div class="row">
				<div class="col-md-3 col-xs-10">
					NIP
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='number' class="form-control" name="nip" value="{{ $datas->nip_deputi }}" required onkeypress="validate()" />
					</div>
					<span class="errmsg">{{ $errors->first('nip') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Jabatan
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="jabatan" value="{{ $datas->jabatan }}" />
					</div>
					<span class="errmsg">{{ $errors->first('jabatan') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Jabatan (In English)
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="jabatan_english" value="{{ $datas->jabatan_english }}"  />
					</div>
					<span class="errmsg">{{ $errors->first('jabatan_english') }}</span>
				</div>
			</div>
			<div class="row">
			@endforeach
		@endif
		@if($data == "")
		<div class="row">
			<div class="col-md-3 col-xs-10">
				Nama Deputi
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
				<div class="form-group">
					<input type='text' class="form-control" name="nama" value="" required />
				</div>
				<span class="errmsg">{{ $errors->first('nama') }}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-xs-10">
				NIP
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='number' class="form-control" name="nip" value="" required onkeypress="validate()" />
                  </div>
                  <span class="errmsg">{{ $errors->first('nip') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                Jabatan
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='number' class="form-control" name="jabatan" value="" required />
                  </div>
                  <span class="errmsg">{{ $errors->first('jabatan') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-10">
                Jabatan (English)
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <input type='text' class="form-control" name="jabatan_english" value="" required />
                  </div>
                  <span class="errmsg">{{ $errors->first('jabatan_english') }}</span>
            </div>
        </div>
        <div class="row">
        @endif
            <div class="col-md-9" style="text-align: right">
                <button type="submit" class="btn btn-sm btn-default1">Simpan</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection
@section('js')
<script>
	$('#metode').on('change', function() {
		var select = this.value;
		if(select == 'verifikasi_portofolio'){
			$('#input_verif').show();
			$('#input_tes').hide();
		}
		
		if(select == 'tes_tulis'){
			$('#input_verif').hide();
			$('#input_tes').show();
		}
	});
	
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker3').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker4').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker2').datetimepicker({
			format: 'LT'
		});
	});
	
	$(document).ready(function() {
		$('#input_verif').hide();
		$('#input_tes').hide();
	});

	function AddBusinessDays(weekDaysToAdd) {
		var curdate = new Date();
		var realDaysToAdd = 0;
		while (weekDaysToAdd > 0){
			curdate.setDate(curdate.getDate()+1);
			realDaysToAdd++;
			if (noWeekendsOrHolidays(curdate)[0]) {
				weekDaysToAdd--;
			}
		}
		return realDaysToAdd;
    }

	var date_billed = $('#datebilled').datepicker('getDate');
	var date_overdue = new Date();
	var weekDays = AddBusinessDays(30);
	date_overdue.setDate(date_billed.getDate() + weekDays);
	date_overdue = $.datepicker.formatDate('mm/dd/yy', date_overdue);
	$('#datepd').val(date_overdue).prop('readonly', true);
</script>
@endsection