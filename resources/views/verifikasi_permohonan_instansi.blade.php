@extends('layout.app')
@section('title')
	Verifikasi Permohonan Uji Kompetensi (Instansi)  
@endsection
@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }

    .btn-area{
        text-align: right;
    }

    .main-box img{
        width: 60px;
    }
    #datetimepicker3{
        width: 149px;
    }
    .datepicker{
        width: 149px;
    }
</style>
@endsection
@section('content')
<div class="main-box">
	<div class="container">
		<div class="row">
		@if (session('msg'))
			@if (session('msg') == "gagal")
				<div class="col-md-8">
					<div class="alert alert-warning alert-dismissible">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Tanggal Batas Waktu Pendaftaran tidak boleh lebih dari tanggal tes</strong>
					</div>
				</div>
			@endif
		@endif
			<div class="col-md-12">
				<h3>Verifikasi Permohonan Uji Kompetensi (Instansi)</h3><hr>
			</div>
		</div>
		<form action="" method="post" id="formlagi">
		@csrf
		<div class="row">
			<div class="col-md-7">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-xs-11">
						Nama Admin Instansi/Admin PPK
					</div>
					<div class="col-lg-1 col-md-1 col-xs-11">
						:
					</div>
					<div class="col-lg-8 col-md-8 col-xs-12">
						{{ $data->names }} 
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-xs-11">
						NIP
					</div>
					<div class="col-lg-1 col-md-1 col-xs-11">
						:
					</div>
					<div class="col-lg-8 col-md-8 col-xs-12">
						{{ $data->nips }} 
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-xs-11">
						Instansi
					</div>
					<div class="col-lg-1 col-md-1 col-xs-11">
						:
					</div>
					<div class="col-lg-8 col-md-8 col-xs-12">
						{{ $data->instansis }} 
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-xs-11">
						Metode Ujian
					</div>
					<div class="col-lg-1 col-md-1 col-xs-11">
						:
					</div>
					<div class="col-lg-8 col-md-8 col-xs-12">
						{{ $data->metode == 'tes_tulis' ? 'Tes Tertulis' : 'Verifikasi Portofolio' }} 
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-xs-11">
						Tanggal Ujian
					</div>
					<div class="col-lg-1 col-md-1 col-xs-11">
						:
					</div>
					<div class="col-lg-8 col-md-8 col-xs-12">
						{{ Helper::tanggal_indo($data->tanggal_ujian) }}
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-xs-11">
						Waktu Ujian
					</div>
					<div class="col-lg-1 col-md-1 col-xs-11">
						:
					</div>
					<div class="col-lg-8 col-md-8 col-xs-12">
					@if($data->metode == 'tes_tulis')
						{{ $data->waktu_ujian }}
					@elseif($data->metode == 'verifikasi_portofolio')
						{{ $data->waktu_verifikasi }}
                    @endif
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-xs-11">
						Lokasi 
					</div>
					<div class="col-lg-1 col-md-1 col-xs-11">
						:
					</div>
					<div class="col-lg-8 col-md-8 col-xs-12">
						{{ $data->lokasi_ujian }}
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-xs-11">
						Jumlah Ruang
					</div>
					<div class="col-lg-1 col-md-1 col-xs-11">
						:
					</div>
					<div class="col-lg-8 col-md-8 col-xs-12">
						{{ $data->jumlah_ruang }}
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-xs-11">
						Kuota
					</div>
					<div class="col-lg-1 col-md-1 col-xs-11">
						:
					</div>
					<div class="col-lg-8 col-md-8 col-xs-12">
						{{ $data->kapasitas }}  
					</div>
				</div>
                <div class="row" id="batas">
					<div class="col-lg-3 col-md-3 col-xs-11">
                        Batas Akhir Pendaftaran
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                        :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
					@php
                        $value_batas = '';
                        $batas_input = $data->batas_waktu_input;
                        if ($batas_input == ''){
                            $value_batas = '00/00/0000'; 
                        }
                        else{
                            $pisah = explode('-',$batas_input);
                            $value_batas = $pisah[2].'/'.$pisah[1].'/'.$pisah[0];
                        }
                    @endphp
						<div class="form-group">
							<div class='input-group date' id="datetimepicker3">
								<input type='text' class="form-control" id="batas_waktu_input" name="batas_waktu_input" value="{{ $value_batas }}" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/ >
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <span class="errmsg" id="errBatas"></span> 
                            <span class="errmsg">{{ $errors->first('batas_waktu_input') }}</span> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                        Nama CP 1
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                        :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        {{ $data->nama_cp_1 }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                        Telp CP 1
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                        :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        {{ $data->telp_cp_1 }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                        Nama CP 2
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                        :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        {{ $data->nama_cp_2 }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                        Telp CP 2
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                        :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        {{ $data->telp_cp_1 }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                        Surat Permohonan
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                        :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
					@php
						$surat_permohonan = explode('.',$data->surat_permohonan);
					@endphp
                    @if ($surat_permohonan[1] == 'pdf' || $surat_permohonan[1] == 'PDF')
						<a href="{{ url('priview-file')."/surat_permohonan/".$data->surat_permohonan }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
					@else
						<a href="{{ url('priview-file')."/surat_permohonan/".$data->surat_permohonan }}" target="_blank"><img src="{{ asset('storage/data/surat_permohonan')."/".$data->surat_permohonan }}" class="img-rounded"></a>
					@endif
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                        Status Permohonan
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                        :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        {!! Form::select('rekomendasi', array('setuju' => 'Setuju', 'tidak_setuju' => 'Tidak Setuju'), $data->status_permohonan, ['placeholder' => 'Pilih Status', 'class' => 'form-control form-pjg', 'id' => 'rekomendasi']); !!}
                         <span class="errmsg" id="errStatus"></span> 
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">

                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">

                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        @php
                        $value_tanggal = '';
                        $batas_input = $data->tanggal_ujian;
                        if ($batas_input == ''){
                            $value_tanggal = $data->tanggal_ujian; 
                        }
                        else{
                            $pisah = explode('-',$batas_input);
                            $value_tanggal = $pisah[2].'/'.$pisah[1].'/'.$pisah[0];
                        }
                        @endphp
                        <div class="form-group hidden">
                            <div class='input-group date' >
                                <input type='hidden' class="form-control datepicker" name="tanggal_ujian" value="{{ $value_tanggal }}" >
                                <input type='hidden' class="form-control" name="metode" value="{{ $data->metode }}" >
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <span class="errmsg">{{ $errors->first('tanggal_ujian') }}</span> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 btn-area">
                <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
                <button type="button" class="btn btn-sm btn-default1" id="sbmt">Kirim</button>
            </div>
        </div>
    </form>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	$('.datepicker').datepicker({
		format: "dd/mm/yyyy"
	});

	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD/MM/YYYY'
		});
    });
    
    $(function () {
        $('#datetimepicker3').datetimepicker({
			format: 'DD/MM/YYYY'
		});
    });
    
    $(function () {
        $('#datetimepicker4').datetimepicker({
			format: 'DD/MM/YYYY'
		});
    });
	
	$(function () {
		$('#datetimepicker2').datetimepicker({
			format: 'LT'
		});
    });

    $(document).ready(function() {
        $('#batas').hide();
        var rekomendasi = $('#rekomendasi').val();
        if(rekomendasi == 'setuju'){
            $('#batas').show();
        } else {
            $('#batas').hide();
        }
    });

    $('#rekomendasi').on('change', function() {
        var select = this.value;

        if(select == 'setuju'){
            $('#batas').show();
        } else {
            $('#batas').hide();
        }
    });

    $('#sbmt').on('click', function() {
        var select = $('#batas_waktu_input').val();
        var rekomendasi = $('#rekomendasi').val();
        if (rekomendasi == 'setuju') {
            if(select == ''){
                $('#errBatas').html("Batas Akhir Pendaftaran tidak boleh kosong.");
            } else {
                $('#formlagi').submit();
                $('#errBatas').html("");
            }
        }else if(rekomendasi == 'tidak_setuju'){
               $('#formlagi').submit();
        }else {
                $('#errStatus').html("Status Permohonan <br> tidak boleh kosong.");
        }
    });
</script>
@endsection