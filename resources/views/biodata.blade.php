@extends('layout.app2')
@section('title')
	Biodata Admin PPK
@endsection
@section('css')
<style>
	body{
		background-color: whitesmoke;
	}

	.main-page{
		margin-top: 20px;
	}

	.box-container{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		background-color: white;
		border-radius: 5px;
		padding: 3%;
	}

	.shadow{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		max-width: 250px;
		line-height: 15px;
		width: 100%;
		margin: 5px;
	}

	div.dataTables_wrapper div.dataTables_info{
		display: none;
	}

	div.dataTables_wrapper .row.col-sm-12{
		width: 120px;
	}

	p{
		font-weight: 500;
	}

	b{
		font-weight: 500;
	}

	.row a.btn{
		text-align: left;
		font-weight: 600;
		font-size: small;
	}

	.chart-two{
		margin-top: 20px;
	}

	.btn-default2{
        background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
        color: black;
        font-weight: 600;
        width: 100px; 
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

	.imgs{
		width: 90px;
	}
</style>
@endsection
@section('content')
@inject('request', 'Illuminate\Http\Request')
<div class="main-page">
	<div class="container">
		<div class="row box-container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-9" style="">
						<div class="row">
							<div class="col-md-12">
								<p>Selamat datang <b>{{ Auth::user()->name }}</b>,<br>Silakan klik menu di samping untuk melanjutkan proses Penyesuaian/Inpassing JF PPBJ.</p>
							</div>
							<div class="col-md-3">
								<img src="{{ asset('storage/data/foto_profile/'.Auth::user()->foto_profile) }}" style="width: 100%;border-radius: 5px;" onerror="this.src='{{ asset('assets/img/blank.jpg') }}';">
							</div>
							<div class="col-md-9">
								<table style="width: 100%">
									<tr>
										<td style="width: 25%">Nama</td><td>:</td><td>{{ Auth::user()->name }}</td>
									</tr>
									<tr>
										<td style="width: 25%">NIP</td><td>:</td><td>{{ Auth::user()->nip }}</td>
									</tr>
									<tr>
										<td style="width: 25%">Jabatan</td><td>:</td><td>{{ Auth::user()->jabatan }}</td>
									</tr>
									<tr>
										<td style="width: 25%">Nama Instansi</td><td>:</td><td>{{ $request->session()->get('instansis_user') }}</td>
									</tr>
									<tr>
										<td style="width: 25%">Nama Satker/OPD</td><td>:</td><td>{{ Auth::user()->nama_satuan }}</td>
									</tr>
									<tr>
										<td style="width: 25%">No. Telepon/HP</td><td>:</td><td>{{ Auth::user()->no_telp }}</td>
									</tr>
									<tr>
										<td style="width: 25%">Email</td><td>:</td><td>{{ Auth::user()->email }}</td>
									</tr>
									<tr>
										<td style="width: 25%">SK Pengangkatan Administrator Penyesuaian/Inpassing</td><td>:</td>
										<td>
										@php
											$sk_admin = Auth::user()->sk_admin_ppk;
										@endphp
										
										@if (is_null($sk_admin))
											-
										@else
										@php
											$sk_admin_ppk = explode('.',$sk_admin);
										@endphp
											@if ($sk_admin_ppk[1] == 'pdf' || $sk_admin_ppk[1] == 'PDF')
												<a href="{{ url('priview-file')."/sk_admin_ppk/".$sk_admin }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:40px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
											@else
												<a href="{{ url('priview-file')."/sk_admin_ppk/".$sk_admin }}" target="_blank"><img src="{{ asset('storage/data/sk_admin_ppk')."/".$sk_admin }}" class="img-rounded imgs"></a>
											@endif 
										@endif
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-5" style="padding: 5px;"><a href="{{ url('biodata-admin-ppk')}}"><button type="submit" class="btn btn-default2" style="float: right;padding: 3px;">UBAH</button></a></div>
						</div>
					</div>
					<div class="col-md-3 text-center">
						@include('layout.button_right_kpp')
					</div>
				</div>
			</div> 
		</div>
		<div class="row box-container chart-two">
			<canvas id="myChart"></canvas>
			@php
				$lulus = '';
				$tidak_lulus = '';
				for ($i=1; $i < count($userLulus) + 1; $i++) { 
					$lulus .= $userLulus[$i];
					if (count($userLulus) != $i) {
						$lulus .= ',';
					}
				}

				for ($i=1; $i < count($userTidakLulus) + 1 ; $i++) { 
					$tidak_lulus .= $userTidakLulus[$i];
					if (count($userTidakLulus) != $i) {
						$tidak_lulus .= ',';
					}
				}
			@endphp
		</div>
	</div>
</div>
<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
@endsection
@section('js')
<script>
	var canvas = document.getElementById("myChart");
	var ctx = canvas.getContext('2d');

    // Global Options:
    Chart.defaults.global.defaultFontColor = 'black';
    Chart.defaults.global.defaultFontSize = 16;

    var data = {
		labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
		datasets: [{
			label: 'LULUS',
			backgroundColor: 'rgba(0,255,255,0.1)',
			borderColor: 'rgba(0,255,255,1)',
			data: [{!! $lulus !!}]
		},
		{
			label: 'TIDAK LULUS',
			backgroundColor: 'rgba(153,0,255,0.1)',
			borderColor: 'rgba(153,0,255,1)',
			data: [{!! $tidak_lulus !!}]
		}]
	};

    // Notice the scaleLabel at the same level as Ticks
    var options = {
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				},
				scaleLabel: {
					display: true,
					labelString: 'Moola',
					fontSize: 20 
				}
			}]            
		}  
	};

    // Chart declaration:
    var myBarChart = new Chart(ctx, {
		type: 'line',
		data: data,
		options: {
			responsive: true,
			legend: {
				position: 'bottom',
			},
			hover: {
				mode: 'label'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Bulan'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Jumlah'
					},
					ticks: {
						beginAtZero: true,
						stepSize: 1
					}
				}]
			},
			title: {
				display: true,
				text: 'Statistik Peserta Ujian'
			}
		}
    });
</script>
@endsection