@extends('layout.app')

@section('title')
Detail Verifikasi Portofolio
@stop
@section('css')
<style type="text/css">
	.form-control
	{
		border-radius: 5px;
		height: 27px;
    padding: 0px;
    padding-left: 10px;
	}

	.how{
		padding-bottom: 10px;
	}
</style>
@stop
@section('content')
<div class="col-md-8">
<div class="box">
            <div class="box-header">
              <h3 class="box-title" style="padding-top: 10px;">Bordered Table</h3>
              <hr>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	<div class="how">
            		<div class="col-md-4">
              	<label>Nama Peserta </label>
              </div>
              <div class="col-md-1">
              	<label>:</label>
              </div>
              <div class="col-md-7">
              	<label>Budi</label>
              </div>
          </div>
              
              <div class="how">
              <div class="col-md-4">
              	<label>No Ujian </label>
              </div>
              <div class="col-md-1">
              	<label>:</label>
              </div>
              <div class="col-md-7">
              	<label>516161</label>
              </div>
          </div>
              
              <div class="how">
              <div class="col-md-4">
              	<label>NIP </label>
              </div>
              <div class="col-md-1">
              	<label>:</label>
              </div>
              <div class="col-md-7">
              	<label>1525151</label>
              </div>
              </div>

              <div class="how">
              <div class="col-md-4">
              	<label>Jenjang </label>
              </div>
              <div class="col-md-1">
              	<label>:</label>
              </div>
              <div class="col-md-7">
              	<label>Peserta</label>
              </div>
              </div>

              <div class="how">
              <div class="col-md-4">
              	<label>Skor</label>
              </div>
              <div class="col-md-1">
              	<label>:</label>
              </div>
              <div class="col-md-7">
              	<label>69</label>
              </div>
              </div>

              <div class="how">
              <div class="col-md-4">
              	<label>Aksesor Verifikasi </label>
              </div>
              <div class="col-md-1">
              	<label>:</label>
              </div>
              <div class="col-md-7 how">
              	<select class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
              </div>
          </div>


          <div class="how">
              <div class="col-md-4">
              	<label>Rekomendasi </label>
              </div>
              <div class="col-md-1">
              	<label>:</label>
              </div>
              <div class="col-md-7 how">
              	<select class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
              </div>
          </div>

          <div class="how">
              <div class="col-md-4">
              	<label>Aksesor Pleno 1 </label>
              </div>
              <div class="col-md-1">
              	<label>:</label>
              </div>
              <div class="col-md-7 how">
              	<select class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
              </div>
          </div>

          <div class="how">
              <div class="col-md-4">
              	<label>Aksesor Pleno 2 </label>
              </div>
              <div class="col-md-1">
              	<label>:</label>
              </div>
              <div class="col-md-7 how">
              	<select class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
              </div>
          </div>

          <div class="how">
              <div class="col-md-4">
              	<label>Keputusan Pleno </label>
              </div>
              <div class="col-md-1">
              	<label>:</label>
              </div>
              <div class="col-md-7 how">
              	<select class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
              </div>
              </div>
            <div class="col-md-6"></div>
            <div class="col-md-3"><button type="button" class="btn btn-block btn-default">Batal</button></div>
            <div class="col-md-3"><button type="button" class="btn btn-block btn-danger">Simpan</button></div>

              
                  
              </div>
          </div>


              
                  
              </div>
            </div>
            <!-- /.box-body -->
          </div>
</div>
@stop