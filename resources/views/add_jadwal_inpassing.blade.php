@extends('layout.app')
@section('title')
	Tambah Jadwal Inpassing
@stop
@section('css')
<style>
	.main-box .col-md-3{
		font-weight: 600;
		font-size: medium;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }

    .btng,.err{
        color: red;
    }
</style>
@endsection
@section('content')
@if (session('msg'))
	@if (session('msg') == "gagal")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Tanggal Batas Waktu Pendaftaran tidak boleh lebih dari tanggal tes</strong>
				</div>
			</div>
		</div> 
	@endif
@endif

<form action="" method="post">
@csrf
	<div class="main-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>Tambah Jadwal Inpassing Reguler</h3><hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Metode Ujian <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<select name="metode_ujian" id="metode" class="form-control" required>
						<option value="" disabled selected>Pilih Metode Ujian</option>
						<option value="verifikasi_portofolio">Verifikasi Portofolio</option>
						<option value="tes_tulis">Tes Tertulis</option>
					</select>
					<span class="errmsg">{{ $errors->first('metode_ujian') }}</span>
				</div>
			</div><br>
			<div id="input_verif">
				<div class="row">
					<div class="col-md-3 col-xs-10">
						Tanggal verifikasi Portofolio <span class="btng">*</span>
					</div>
					<div class="col-md-1 col-xs-1">:</div>
					<div class="col-md-5 col-xs-12">
						<div class="form-group">
							<div class='input-group date' id='datetimepicker1'>
								<input type='text' class="form-control" name="tanggal_verifikasi" value="{{ old('tanggal_verifikasi') }}" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
						<span class="errmsg">{{ $errors->first('tanggal_verifikasi') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-xs-10">
						Waktu Verifikasi Portofolio <span class="btng">*</span>
					</div>
					<div class="col-md-1 col-xs-1">:</div>
					<div class="col-md-5 col-xs-12">
						<div class="form-group">
							<input type='text' class="form-control" name="waktu_verifikasi" value="{{ old('waktu_verifikasi') }}" />
						</div>
						<span class="errmsg">{{ $errors->first('waktu_verifikasi') }}</span>
					</div>
				</div>
			</div>
			<div id="input_tes">
				<div class="row">
					<div class="col-md-3 col-xs-10">
						Tanggal Tes Tertulis <span class="btng">*</span>
					</div>
					<div class="col-md-1 col-xs-1">:</div>
					<div class="col-md-5 col-xs-12">
						<div class="form-group">
							<div class='input-group date' id='datetimepicker4'>
								<input type='text' class="form-control" name="tanggal_tes" value="{{ old('tanggal_tes') }}" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
						<span class="errmsg">{{ $errors->first('tanggal_tes') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-xs-10">
						Waktu Tes Tertulis <span class="btng">*</span>
					</div>
					<div class="col-md-1 col-xs-1">:</div>
					<div class="col-md-5 col-xs-12">
						<div class="form-group">
							<input type='text' class="form-control" name="waktu_ujian" value="{{ old('waktu_ujian') }}" />
						</div>
						<span class="errmsg">{{ $errors->first('waktu_ujian') }}</span>
					</div>
				</div>
			</div>
			<div class="row" id="lokasi_ujian">
				<div class="col-md-3 col-xs-10">
					Lokasi Ujian <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="lokasi_ujian" value="{{ old('lokasi_ujian') }}" />
					</div>
					<span class="errmsg">{{ $errors->first('lokasi_ujian') }}</span>
				</div>
			</div>
			<div class="row" id="jumlah_ruang">
				<div class="col-md-3 col-xs-10">
					Jumlah Ruang <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="jumlah_ruang" value="{{ old('jumlah_ruang') }}" maxlength="20" onkeypress='validate(event)'/>
					</div>
					<span class="errmsg">{{ $errors->first('jumlah_ruang') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Kuota <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="kapasitas" value="{{ old('kapasitas') }}" maxlength="20" onkeypress='validate(event)' required/>
					</div>
					<span class="errmsg">{{ $errors->first('kapasitas') }}</span>
				</div>
			</div>
			<div class="row" id="jumlah_unit">
				<div class="col-md-3 col-xs-10">
					Jumlah Unit <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="jumlah_unit" value="{{ old('jumlah_unit') }}" maxlength="20" onkeypress='validate(event)'/>
					</div>
					<span class="errmsg">{{ $errors->first('jumlah_unit') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Batas Akhir Pendaftaran <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<div class='input-group date' id='datetimepicker3'>
							<input type='text' class="form-control" name="batas_waktu_input" value="{{ old('batas_waktu_input') }}" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						<span class="errmsg">{{ $errors->first('batas_waktu_input') }}</span> 
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Publish Jadwal <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<select name="publish_jadwal" class="form-control">
							<option value="" disabled selected>Status Publish</option>
							<option value="ya">Ya</option>
							<option value="tidak">Tidak</option>
						</select>
					</div>
					<span class="errmsg">{{ $errors->first('publish_jadwal') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Nama CP 1 <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="nama_cp_1" value="{{ old('nama_cp_1') }}" />
					</div>
					<span class="errmsg">{{ $errors->first('nama_cp_1') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					No.Telp CP 1 <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="no_telp_cp_1" onchange="checkTelp1(this)" value="{{ old('no_telp_cp_1') }}" maxlength="12" id="no_telp1" onkeypress='validate(event)' required/>
					</div>
					<span class="err" id="errTelp1"></span>
					<span class="errmsg">{{ $errors->first('no_telp_cp_1') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Nama CP 2 <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="nama_cp_2" value="{{ old('nama_cp_2') }}" />
					</div>
					<span class="errmsg">{{ $errors->first('nama_cp_2') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					No.Telp CP 2 <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="no_telp_cp_2" onchange="checkTelp2(this)" value="{{ old('no_telp_cp_2') }}" maxlength="12" id="no_telp2" onkeypress='validate(event)' required/>
					</div>
					<span class="err" id="errTelp2"></span>
					<span class="errmsg">{{ $errors->first('no_telp_cp_2') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9" style="text-align: right">
					<button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
					<button type="submit" class="btn btn-sm btn-default1">Simpan</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('js')
<script type="text/javascript">
	function modifyVar(obj, val) {
		obj.valueOf = obj.toSource = obj.toString = function(){ return val; };
    }

    function setToFalse(boolVar) {
		modifyVar(boolVar, false);
    }

    function setToTrue(boolVar) {
		modifyVar(boolVar, true);
    }

    var validasi_telp = new Boolean(false);
    
	function checkTelp1(element){
		if ($('#no_telp1').val().length >= 13 || $('#no_telp1').val().length <=1) {
			$('#errTelp1').html("No. Telepon/HP harus berjumlah 2 hingga 12 digit.")
			setToFalse(validasi_telp);
		} else {
			setToTrue(validasi_telp);
			$('#errTelp1').html("")
		}
	}
  
	function checkTelp2(element){
		if ($('#no_telp2').val().length >= 13 || $('#no_telp2').val().length <=1) {
			$('#errTelp2').html("No. Telepon/HP harus berjumlah 2 hingga 12 digit.")
			setToFalse(validasi_telp);
		} else {
			setToTrue(validasi_telp);
			$('#errTelp2').html("")
		}
	}

	$('#metode').on('change', function() {
		var select = this.value;
		if(select == 'verifikasi_portofolio'){
			$('#input_verif').show();
			$('#input_tes').hide();
			$('#jumlah_unit').hide();
			$('#jumlah_ruang').hide();
			$('#lokasi_ujian').hide();
		}	
		
		if(select == 'tes_tulis'){
			$('#input_verif').hide();
			$('#input_tes').show();
			$('#jumlah_unit').show();
			$('#jumlah_ruang').show();
			$('#lokasi_ujian').show();
		}
	});

	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker3').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker4').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});
	
	$(function () {
		$('#datetimepicker2').datetimepicker({
			format: 'LT'
		});
	});

	$(document).ready(function() {
		$('#input_verif').hide();
		$('#input_tes').hide();
		var metode = $('#metode').val();

		if (metode == 'verifikasi_portofolio') {
			$('#input_verif').show();
			$('#input_tes').hide();
			$('#jumlah_unit').hide();
			$('#jumlah_ruang').hide();
			$('#lokasi_ujian').hide();
		}

		if (metode == 'tes_tulis') {
			$('#input_verif').hide();
			$('#input_tes').show();
			$('#jumlah_unit').show();
			$('#jumlah_ruang').show();
			$('#lokasi_ujian').show();
		}
	});

	function AddBusinessDays(weekDaysToAdd) {
		var curdate = new Date();
		var realDaysToAdd = 0;
		while (weekDaysToAdd > 0){
			curdate.setDate(curdate.getDate()+1);
			realDaysToAdd++;			
			//check if current day is business day
			if (noWeekendsOrHolidays(curdate)[0]) {
				weekDaysToAdd--;
			}
		}
		return realDaysToAdd;
    }


	var date_billed = $('#datebilled').datepicker('getDate');
	var date_overdue = new Date();
	var weekDays = AddBusinessDays(30);
	date_overdue.setDate(date_billed.getDate() + weekDays);
	date_overdue = $.datepicker.formatDate('mm/dd/yy', date_overdue);
	$('#datepd').val(date_overdue).prop('readonly', true);
</script>
@endsection