@extends('layout.app2')
@section('title')
    Register
@endsection
@section('css')
<style>
    .jud-register{
        padding: 15px;
        color: #3a394e;
    }

    .box{
        -webkit-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
        -moz-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
        box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
    }

    .btn-default1{
        background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
        color: white;
        font-weight: 600;
        width: 100px; 
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .btn-default2{
        background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
        color: black;
        font-weight: 600;
        width: 100px; 
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .btn-default1:hover{
        color: #eee;
        background: #c10013;
    }

    .btn-area{
        text-align: right;
    }

    .navi ul{
		padding:0;
		font-size:0;
		overflow:hidden;
		display:inline-block;
		width: 100%;
    }

    .navi li{
		display:inline-block;
		text-align: center;
    }

    .navi a{
		font-size:1rem;
		position:relative;
		display:inline-block;
		background:#eee;
		text-decoration:none;
		color:#555;
		padding:13px 25px 13px 10px;
		width: 100%;
		font-weight: 600;
    }

    .navi a:after,
    .navi a:before{
		position: absolute;
		content: "";
		height: -10px;
		width: 0px;
		top: 50%;
		left: -28px;
		margin-top: -25px;
		border: 25px solid #eee;
		border-right: 2 !important;
		border-left-color: transparent !important;
    }
    
	.navi a:before{
		left:-26px;
		border: 24px solid #555;
    }
    
	/* ACTIVE STYLES */
    .navi a.active{
        background: #e74c3c;
        color: #fff;
        -webkit-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
    }
    
	.navi a.active:after{border-color:#e74c3c;}
    /* HOVER STYLES */
    .navi a:hover{
        background: #e74c3c;
        color: #fff;
        -webkit-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
    }
    
	.navi a:hover:after{ border-color:#e74c3c;}

    #page-two{
        display: none;
    }

    #page-three{
        display: none;
    }

    div#page-two div.container div.row{
        padding: 0px 5%; 
        margin-bottom: 1%;
    }

    div#page-two h5 span{
        color: #e74c3c;
    }

    label.custom-file-label{
        margin: 0px 15px;
    }

    .row .form-control{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    }

    .row .custom-file-label{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19); 
    }

    div.text-center{
        padding: 5%;
    }
</style>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3 class="jud-register">Daftar Akun Administrator Penyesuaian/Inpassing (Admin Instansi/Admin PPK)</h3>
		</div>
		<div class="col-md-12">
			<div class="box">
			@if ($msg == "berhasil kirim email")
				<div class="text-center">
					<h2>Selamat! Anda berhasil membuat akun Admin Instansi/Admin PPK.<br>Silakan cek email Anda untuk mengaktivasi akun.</h2>
					<a href="{{ url('inpassing') }}"><button class="btn btn-default1">Masuk</button></a>
				</div>
			@else				
				<div class="text-center">
					<h2>Mohon maaf terjadi kesalahan saat daftar di web LKPP, silakan hubungi admin untuk info lebih lanjut.</h2>
					<a href="{{ url('inpassing') }}"><button class="btn btn-default1">Masuk</button></a>
				</div>
			@endif
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script>
	$('#btnOne').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-two").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "block";
        document.getElementById("page-three").style.display = "none";
    });

    $('#btnTwoBack').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-one").addClass("active");
        document.getElementById("page-one").style.display = "block";
        document.getElementById("page-two").style.display = "none";
        document.getElementById("page-three").style.display = "none";        
    });

    $('#btnTwoNext').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-three").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "none";
        document.getElementById("page-three").style.display = "block";        
    });

    $('#btnThreeBack').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-two").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "block";
        document.getElementById("page-three").style.display = "none";
        
    });

    $('#li-one').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-one").addClass("active");
        document.getElementById("page-one").style.display = "block";
        document.getElementById("page-two").style.display = "none";
        document.getElementById("page-three").style.display = "none";        
    });

    $('#li-two').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-two").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "block";
        document.getElementById("page-three").style.display = "none";        
    });

    $('#li-three').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-three").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "none";
        document.getElementById("page-three").style.display = "block";        
    });

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });    
</script>
@endsection