<!DOCTYPE html>
<html>
<head>
	<title>Absensi Jadwal Reguller</title>
</head>
<style type="text/css">
body {
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 16px;
}
.header {
  text-align: center;
  width: 100%;
}
.content{
	text-align: center;
	width: 100%;
}
.img-rounded{
	margin: 10px;
	width: 40%;
}
/*.footer {
  position: fixed;
  text-align: center;
  bottom: 0;
  width: 100%;
}*/
table.tbl {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.tbl td, th {
  border: 1px solid #eae7e7;
  text-align: left;
  padding: 8px;
}

.tbl tr:nth-child(even) {
  background-color: #f9f9f9;
}
tr.head{
	background-color: #d4d4d4;
}
</style>
<body>
<div class="header">
<h4>LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH<br>DEPUTI BIDANG PENGEMBANGAN DAN PEMBINAAN SDM</h4>
<h5>Direktorat Sertifikat Profesi<br>Daftar Peserta Uji Kompetensi Inpassing JF PPBJ</h5>
</div>
<div class="content">
	<p>Tanggal : {{ $penyelengara == 'LKPP' ? Helper::tanggal_indo($jadwal->tanggal_ujian) : Helper::tanggal_indo($jadwal->tanggal_tes_tertulis) }} di {{ $jadwal->lokasi_ujian }}<br>Penyelenggara : {{ $penyelengara }} <br> Jenjang : Pertama</p>
	<table class="tbl">
	  <tr class="head">
	    <th>No</th>
	    <th>No Ujian</th>
	    <th>Foto</th>
	    <th>Nama</th>
	    <th>NIP</th>
	    <th>Nama Instansi</th>
	    <th>Tanda Tangan</th>
	  </tr>
	@foreach($data as $key => $datas)
	  <tr>
	    <td>{{ $key++ + 1 }}</td>
	    <td>{{ $datas->no_ujian == '' ? '-' : $datas->no_ujian }}</td>
	    <td>
	    	@if ($datas->fotos == "")
				Tidak Ada foto
			@else
			@php
            $foto = explode('.',$datas->fotos);
            @endphp
            @if ($foto[1] == 'pdf' || $foto[1] == 'PDF')
            -
            @else
            <a href="{{ url('priview-file')."/pas_foto_3_x_4/".$datas->fotos }}" target="_blank"><img src="{{ asset('storage/data/pas_foto_3_x_4/'.$datas->fotos) }}" alt="" class="img-responsive" width="60px"></a>
            @endif
			@endif
	    </td>
	    <td>{{ $datas->nama }}</td>
	    <td>{{ $datas->nip }}</td>
	    <td>{{ $datas->nama_instansis }}</td>
	    <td></td>
	  </tr>
	 @endforeach
	</table>
</div>
{{-- <div class="footer">
	<p>Robin Asad Suryo, Ph.D<br>NIP. 12345678912345678</p>
</div> --}}
</body>
</html>