<!DOCTYPE html>
<html>
<head>
	<title>SKTL (Surat Keterangan Tidak Lulus)</title>
</head>
<style type="text/css">
	body{
		font-family: 'arial';
		font-weight: normal;
		font-size: 12px;
		
	}
	.badan{
		margin-right: 5%;
margin-left:  5%;
margin-top: 5%;
		margin-bottom: 2%;
	}
</style>
<body>
	@php
		$bulan = date('m');
		$tahun = date('Y');
	@endphp
<div class="badan">
	<img src="{{ asset('assets/img/sphu.png')}}" class="img-responsive">
	<br><br>
<div align="center">
<font face="arial" style="font-size: 16px"><b>Surat Keterangan Hasil <br> Uji Kompetensi Penyesuaian/<i>Inpassing</i> <br> No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; /D.3.3/{{$bulan}}/{{$tahun}}</b></font>
</div>
<br>
<br>
<font face="arial" >Peserta Uji Kompetensi Penyesuaian/<i>Inpassing</i></font><br><br>
<table style="width: 100%">
    		<tr>
				<td style="text-align:justify;width: 26%">Nama</td>
				<td style="text-align:justify;width: 5%">:</td>
				<td style="text-align:justify;width: 69%">{{ $data->nama }}</td>
			</tr>
			<tr>
				<td style="text-align:justify;width: 26%"> NIP</td>
				<td style="text-align:justify;width: 5%">:</td>
				<td style="text-align:justify;width: 69%">{{ $data->nip }}</td>
			</tr>
			<tr>
				<td style="text-align:justify;width: 26%">Unit Kerja</td>
				<td style="text-align:justify;width: 5%">:</td>
				<td style="text-align:justify;width: 69%">{{ $data->satuan_kerja.' '.$data->instansis }}</td> 
			</tr> 
</table>
<br>
<font face="arial" >telah mengikuti  Uji Kompetensi Penyesuaian/<i>Inpassing</i> untuk :</font><br><br>
		<table style="width: 100%">
			<tr>
				<td style="text-align:justify;width: 26%">Jenjang Jabatan</td>
				<td style="text-align:justify;width: 5%">:  </td>
				<td style="text-align:justify;width: 69%">{{ $data->jenjang }}</td>
			</tr>
			<tr>
				<td style="text-align:justify;width: 26%">Metode Ujian</td>
				<td style="text-align:justify;width: 5%">:  </td>
				<td style="text-align:justify;width: 69%">{{ $data->metodes == 'tes' ? 'Tes Tertulis' : 'Verifikasi Portofolio' }}</td>
			</tr>
			<tr>
				<td style="text-align:justify;width: 26%">Tanggal Uji Kompetensi</td>
				<td style="text-align:justify;width: 5%">:  </td>
				@if($data->tanggal_ujian != 0000-00-00)
				<td style="text-align:justify;width: 69%">{{ Helper::tanggal_indo($data->tanggal_ujian,true) }}</td>
				@endif
			</tr>
			<tr>
				<td style="text-align:justify;width: 26%">Tempat Uji Kompetensi</td>
				<td style="text-align:justify;width: 5%">:  </td>
				<td style="text-align:justify;width: 69%">{{ $data->lokasi_ujian }}</td>
			</tr>
		</table>	
		<br><br>
<p style="text-align: justify;text-indent: 50px">Peserta tersebut dinyatakan <b>TIDAK LULUS</b>, sehingga belum dapat diterbitkan Sertifikat Kompetensi Penyesuaian/<i>Inpassing</i>. Selanjutnya untuk mendapatkan Sertifikat, peserta dapat mengajukan asesmen ulang untuk metode verifikasi portofolio atau melalui metode tes tertulis sesuai dengan jadwal yang tertera di <a href="">https://ppsdm.lkpp.go.id.</a> </p>
<br>
<p style="text-indent: 50px;text-align: justify;">Demikian surat keterangan ini dibuat untuk menjadi perhatian dan agar dapat digunakan sebagaimana semestinya.</p><br>

<div style="margin-left: 54%;">Jakarta, {{ Helper::tanggal_indo($hariini) }}<br>Direktur Sertifikasi Profesi selaku <br>Ketua Lembaga Sertifikasi Profesi LKPP
			<br>
			<br>
			<br>
			<br>
			<br>
			<b>	Dwi Wahyuni Kartianingsih</b>
</div>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
<font face="arial" size="1" ><b>
		Tembusan:<br>
			1. Direktur Pengembangan Profesi dan Kelembagaan LKPP;
			<br>
			2. Kepala BKD /Kepala Biro Kepegawaian ybs.
			</b>
	</font>
	<div align="center" style="visibility: hidden;">
    <img src=""  style="visibility: hidden;width: 0px"/>
</div>
</div>	
</body>
</html>


