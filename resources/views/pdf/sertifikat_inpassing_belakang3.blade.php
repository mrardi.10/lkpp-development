<?php
header("Content-type:application/pdf");
$square = "https://inpassing.lkpp.go.id/assets/img/square.png";
$chk = "https://inpassing.lkpp.go.id/assets/img/checkbox.png";
?>
<style>
table {
  border-collapse: collapse;
}

p.ex1 {
  margin-left: 30px;
}

th {
  background-color: #dddddd;
}


</style>
<table style="width:100%">
  <tr>
    <td>&nbsp;</td>
    <td><h2><center>JENJANG {{ strtoupper($data->jenjang) }}</center></h2></td> 
    <td>&nbsp;</td>
  </tr>
</table><br><br>
<table style="width:100%" border=1>
  <tr>
    <th>No.</th>
    <th>Jenis Kompetensi</th>
    <th>Lulus</th>
  </tr>
  <?php 
	$jum = 0;
	foreach($judul as $j):?>
    <tr>
	<td align="center" valign="top">{{ $jum + 1 }}.</td>
	<td valign="top">{{ $j }}
	<?php foreach($detail[$jum] as $d): ?>
		<span style="margin-left:30px;"><ul>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- {{ $d }} <br><br></ul></span>
	<?php endforeach; ?>
		<br>
	</td>
	<td align="center">&nbsp;
	<?php foreach($lulus[$jum] as $d): ?>
		<ul><img src={{ $d == 1 ? $chk : $square }} width="20" height="20"><br><br></ul>
	<?php endforeach; ?><br>
	</td>
  <?php $jum++; endforeach; ?>
   </tr> 
</table>
