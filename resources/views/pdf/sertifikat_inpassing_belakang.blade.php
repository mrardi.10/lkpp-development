<?php
header("Content-type:application/pdf");

?>

<table style="width:100%" border=1>
  <tr>
    <th>No.</th>
    <th>Jenis Kompetensi</th>
    <th>Lulus</th>
  </tr>
  <tr>
    <td>1.</td>
    <td>Perencanaan PBJP
		<ul>- Penyusunan Spesifikasi Teknis dan KAK</ul>
		<ul>- Penyusunan Perkiraan Harga untuk setiap tahapan PBJP</ul>
		<ul>- Identifikasi/Reviu Kebutuhan dari Penetapan Barang/Jasa</ul>
	</td>
    <td>
		<ul><input type="checkbox" name="vehicle3" value="Boat" ></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat" ></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat" ></ul>
	</td>
  </tr>
  <tr>
    <td>2.</td>
    <td>Pemilihan Penyedia Barang/Jasa
		<ul>- Reviu terhadap Dokumen Persiapan PBJP</ul>
		<ul>- Penyusunan dan Penjelasan Dokumen Pemilihan</ul>
		<ul>- Evaluasi Penawaran</ul>
		<ul>- Penilaian Kualifikasi</ul>
		<ul>- Negosiasi dalam PBJP</ul>
		<ul>- Melakukan Pengadaan secara E-Purchasing dan pembelian melalui Toko Daring</ul>
	</td>
    <td>
		<ul><input type="checkbox" name="vehicle3" value="Boat" checked></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat" checked></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat" checked></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat" checked></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat" checked></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat"></ul>
	</td>
  </tr>
  <tr>
    <td>3.</td>
	<td>Pengelolaan Kontrak PBJP
		<ul>- Perumusan Kontrak PBJP</ul>
		<ul>- Pengendalian Pelaksanaan Kontrak PBJP</ul>
		<ul>- Serah Terima Hasil PBJP</ul>
		<ul>- Evaluasi Kinerja Penyedia PBJP</ul>
	</td>
    <td>
		<ul><input type="checkbox" name="vehicle3" value="Boat" ></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat" ></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat" ></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat" ></ul>
	</td>
  </tr>
  <tr>
    <td>4.</td>
    <td>Pengelola PBJP secara swakelola
		<ul>- Penyusunan Rencana dan Persiapan PBJP secara Swakelola</ul>
		<ul>- Pelaksanaan PBJP secara Swakelola</ul>
		<ul>- Pengawasan, Pengendalian, dan Pelaporan PBJP secara Swakelola</ul>
	</td>
    <td>
		<ul><input type="checkbox" name="vehicle3" value="Boat" ></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat" ></ul>
		<ul><input type="checkbox" name="vehicle3" value="Boat" ></ul>
	</td>
  </tr>
  <tr>
    <td>5.</td>
    <td><ul>Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan</ul></td>
    <td><ul><input type="checkbox" name="vehicle3" value="Boat" ></ul></td>
  </tr>
</table>
