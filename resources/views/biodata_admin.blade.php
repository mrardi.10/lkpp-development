@extends('layout.app2')
@section('title')
	Ubah Biodata Admin PPK
@endsection
@section('css')
<style>
	body{
		background-color: whitesmoke;
	}

	.main-page{
		margin-top: 20px;
	}

	.box-container{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		background-color: white;
		border-radius: 5px;
		padding: 3%;
	}

	.shadow{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		max-width: 250px;
		line-height: 15px;
		width: 100%;
		margin: 5px;
	}

	div.dataTables_wrapper div.dataTables_info{
		display: none;
	}

	div.dataTables_wrapper .row.col-sm-12{
		width: 120px;
	}

	p{
		font-weight: 500;
	}

	b{
		font-weight: 500;
	}

	.row a.btn{
		text-align: left;
		font-weight: 600;
		font-size: small;
	}

	input{
		margin :5px;
		-webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		-moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		border-radius: 5px;
	}

	textarea{
		margin :5px;
		-webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		-moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		border-radius: 5px;
	}

	button{
		width: 120px;
	}

	.form-control{
		height: 30px;
		width: 100% !important;
	}

	.btn-default1{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
        color: white;
        font-weight: 600;
        width: 100px; 
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .btn-default2{
        background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
        color: black;
        font-weight: 600;
        width: 100px; 
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .custom-file label.custom-file-label{
		margin: 5px;
		-webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		-moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
	}

	span.mk{
		font-size: 12px;
		padding: 2px;
		margin-left: 5px;
	}

	.select2-container{
        margin: 5px;
        border: 1px solid #ced4da;
        width: auto;
        padding: 1px;
        box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    }   
    
	.select2-container--default .select2-selection--single{
        border:0px !important;
    }
</style>
@endsection
@section('content')
<div class="main-page">
	<div class="container">
		<div class="row box-container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-9" style="">
					<form action="" method="post" enctype="multipart/form-data" id="pesertaForm">
					@csrf
						<div class="row">
							<div class="col-md-12">
								<h5>Biodata Admin PPK</h5><hr>
								
								@if (session('msg'))
									@if (session('msg') == "berhasil simpan")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil ubah data</strong>
										</div> 
									@else
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal ubah data</strong>
										</div> 
									@endif
								@endif
							</div>
							<div class="col-md-3">
								<img src="{{ asset('storage/data/foto_profile/'.Auth::user()->foto_profile) }}" style="width: 100%;border-radius: 5px;" onerror="this.src='{{ asset('assets/img/blank.jpg') }}';">
							</div>
							<div class="col-md-8">
								<table style="width: 100%">
									<tr>
										<td style="width: 25%">Nama</td><td>:</td>
										<td><input type="text" name="nama" style="width: 75%" class="form-control" value="{{ Auth::user()->name }}">
											<span class="errmsg">{{ $errors->first('nama') }}</span>
										</td>
									</tr>
									<tr>
										<td style="width: 25%">NIP</td><td>:</td>
										<td><input type="text" name="nip" style="width: 75%" class="form-control" maxlength="18" onkeypress='validate(event)' value="{{ Auth::user()->nip }}">
											<span class="errmsg">{{ $errors->first('nip') }}</span>
										</td>
									</tr>
									<tr>
										<td style="width: 25%">Jabatan</td><td>:</td>
										<td><input type="text" name="jabatan" style="width: 75%" class="form-control" value="{{ Auth::user()->jabatan }}">
											<span class="errmsg">{{ $errors->first('jabatan') }}</span>
										</td>
									</tr>
									<tr>
										<td style="width: 25%">Nama Instansi</td><td>:</td>
										<td><input type="text" name="nama_instansi" style="width: 75%" class="form-control" value="{{  $data->nama }}" readonly="">
											<span class="errmsg">{{ $errors->first('nama_instansi') }}</span>
										</td>
									</tr>
									<tr>
										<td style="width: 25%">Nama Satker/OPD</td><td>:</td>
										<td><input type="text" name="nama_satuan" style="width: 75%" class="form-control" value="{{ Auth::user()->nama_satuan }}">
											<span class="errmsg">{{ $errors->first('nama_satuan') }}</span>
										</td>
									</tr>
									<tr>
										<td style="width: 25%">No. Telepon/HP</td><td>:</td>
										<td><input type="text" name="no_telp" style="width: 75%" class="form-control" value="{{ Auth::user()->no_telp }}" maxlength="12" onkeypress='validate(event)'>
											<span class="errmsg">{{ $errors->first('no_telp') }}</span>
										</td>
									</tr>
									<tr>
										<td style="width: 25%">Email</td><td>:</td>
										<td><input type="text" name="email" style="width: 75%" class="form-control" value="{{ Auth::user()->email }}" readonly>
											<span class="errmsg">{{ $errors->first('email') }}</span>
										</td>
									</tr>
									<tr>
										<td style="width: 25%">Foto</td><td>:</td>
										<td>
											<div class="custom-file">
												<input type="file" class="custom-file-input foto_profile" id="customFile" name="foto_profile" accept=".jpg, .jpeg,image/jpg, image/jpeg">
												<label class="custom-file-label" for="customFile">Pilih Foto</label>
												<span class="mk">*min. 100KB max. 2MB, format JPG, JPEG</span>
											</div>
											<span class="errmsg">{{ $errors->first('foto_profile') }}</span>
											<span class="errmsg errfoto_profile"></span>
										</td>
									</tr>
									<tr>
										<td style="width: 25%">SK Pengangkatan Administrator Penyesuaian/Inpassing</td><td>:</td>
										<td>
											<div class="custom-file">
												<input type="file" class="custom-file-input sk_admin_ppk" id="customFile" name="sk_admin_ppk" accept=".jpg, .jpeg, .pdf, image/jpg, image/jpeg, application/pdf">
												<label class="custom-file-label" for="customFile">Pilih File</label>
											</div>
											<span class="mk">*min. 100KB max. 2MB, format JPG, JPEG, PDF</span>
											<span class="errmsg">{{ $errors->first('sk_admin_ppk') }}</span>
											<span class="errmsg errsk_admin_ppk"></span>
										</td>
									</tr>
								</table>
								<input type="hidden" name="id" value="{{ Auth::user()->id }}">
							</div>
							<div class="col-md-11" style="text-align : right">
								<button type="reset" class="btn btn-default2" onclick="window.location='{{ Helper::getBiodataPage() }}'">Batal</button>
								<button type="button" class="btn btn-default1" id="btnSubmit">Simpan</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-3 text-center">
					@include('layout.button_right_kpp')
				</div>
			</div>
		</div> 
	</div>
</div>
@endsection
@section('js')
<script>
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});
	
	$(document).ready(function() {
		$('.js-example-basic-single').select2();
	});    

	function modifyVar(obj, val) {
        obj.valueOf = obj.toSource = obj.toString = function(){ return val; };
    }

    function setToFalse(boolVar) {
        modifyVar(boolVar, false);
    }

    function setToTrue(boolVar) {
        modifyVar(boolVar, true);
    }

	var foto_profile = new Boolean(true);
	var sk_admin_ppk = new Boolean(true);

	$('.foto_profile').bind('change', function() {
        var size_foto_profile = this.files[0].size;
        if(size_foto_profile > 2097152){
            $('.errfoto_profile').html("file tidak boleh lebih dari 2MB");
            setToFalse(foto_profile);
        }

        if(size_foto_profile < 102796){
            $('.errfoto_profile').html("file tidak boleh kurang dari 100kb");
            setToFalse(foto_profile);
        }

        if (102796 < size_foto_profile && size_foto_profile < 2097152) {
            $('.errfoto_profile').html("");
            setToTrue(foto_profile);
        }
    });

	$('.sk_admin_ppk').bind('change', function() {
        var size_sk_admin_ppk = this.files[0].size;
        if(size_sk_admin_ppk > 2097152){
            $('.errsk_admin_ppk').html("file tidak boleh lebih dari 2MB");
            setToFalse(sk_admin_ppk);
        }

        if(size_sk_admin_ppk < 102796){
            $('.errsk_admin_ppk').html("file tidak boleh kurang dari 100kb");
            setToFalse(sk_admin_ppk);
        }

        if (102796 < size_sk_admin_ppk && size_sk_admin_ppk < 2097152) {
            $('.errsk_admin_ppk').html("");
            setToTrue(sk_admin_ppk);
        }
    });

    $('#btnSubmit').click(function (e) {
        e.preventDefault();
        console.log('foto_profile =' + foto_profile + '\n');
        if(foto_profile == true){
            $('#pesertaForm').submit();
        }else{
            alert('upload file ada yang salah, silakan cek kembali file yang anda upload agar bisa mengusulkan formasi');
        }
    });
</script>    
@endsection