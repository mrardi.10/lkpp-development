@extends('layout.app2')
@section('title')
	Permohonan Penyelegaraan Ujian Di Instansi 
@endsection
@section('css')
<style>
	body{
		background-color: whitesmoke;
	}
	
	.main-page{
		margin-top: 20px;
	}
	
	.err{
		color: red;
	}
	
	.box-container{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		background-color: white;
		border-radius: 5px;
		padding: 3%;
	}

	.shadow{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		max-width: 250px;
		line-height: 15px;
		width: 100%;
		margin: 5px;
	}

	div.dataTables_wrapper div.dataTables_info{
		display: none;
	}

	div.dataTables_wrapper .row.col-sm-12{
		width: 120px;
	}

	p{
		font-weight: 500;
	}

	b{
		font-weight: 500;
	}

	.row a.btn{
		text-align: left;
		font-weight: 600;
		font-size: small;
	}
	
	.errmsgsp{
		color: red;
	}
  
	.btn-default1{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
		color: white;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default2{
		background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
		color: black;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default3{
		background: #E6341E;
		color: #fff;
		font-weight: 600;
		width: 140px;
	}

	.row-input{
		padding-bottom: 10px;
	}

	.page div.verifikasi{
		display: none;
	}

	.btn-area{
		text-align: right;
	}

	.clickable-row:hover{
		cursor: pointer;
	}

	label.custom-file-label{
		margin: 0px 15px;
	}

	.form .row{
		margin: 1%;
	}

	span.mk{
		font-size: 12px;
		padding: 2px;
		margin-left: 6px;
	}
</style>
@endsection
@section('content')
<div class="main-page">
	<div class="container">
	@if (session('msg'))
		@if (session('msg') == "gagal")
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-warning alert-dismissible">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Tanggal Batas Waktu Pendaftaran tidak boleh lebih dari tanggal tes</strong>
					</div>
				</div>
			</div> 
		@endif
    @endif
	<form action="" method="post" enctype="multipart/form-data" autocomplete="on">
	@csrf
		<div class="row box-container">
			<div class="col-md-12 form">
				<div class="row">
					<div class="col-md-12" style="">
						<h3>Permohonan Penyelenggaraan Ujian Di Instansi</h3><hr>
					</div>
				</div>
				<input type="hidden" name="metode_ujian" value="tes_tulis" class="form-control">
				<div id="input_tes">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-9">
							Tanggal Tes Tertulis 
						</div>
						<div class="col-lg-1 col-md-1 col-1">:</div>
						<div class="col-lg-5 col-md-5 col-11">
							<div class='input-group date' >
								<input type='text' class="form-control form-control-sm datepicker" name="tanggal_tes" value="{{ old('tanggal_tes') }}"  id="datepicker" autocomplete="off"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
						<span class="errmsg">{{ $errors->first('tanggal_tes') }}</span>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-9">
							Waktu Tes Tertulis 
						</div>
						<div class="col-lg-1 col-md-1 col-1">:</div>
						<div class="col-lg-5 col-md-5 col-11">
							<input type='text' class="form-control form-control-sm" name="waktu_ujian" value="{{ old('waktu_ujian') }}" />
						</div>
						<span class="errmsg">{{ $errors->first('waktu_ujian') }}</span>
					</div>
				</div>
				<div class="row" id="lokasi_ujian">
					<div class="col-lg-3 col-md-3 col-9">Lokasi Tes Tertulis</div>
					<div class="col-lg-1 col-md-1 col-1">:</div>
					<div class="col-lg-5 col-md-5 col-11">
						<input type="text" name="lokasi_ujian" id="" class="form-control form-control-sm" value="{{ old('lokasi_ujian') }}" >
						<span class="errmsg">{{ $errors->first('lokasi_ujian') }}</span>
					</div>
				</div>
				<div class="row" id="jumlah_ruang">
					<div class="col-lg-3 col-md-3 col-9">Jumlah Ruang</div>
					<div class="col-lg-1 col-md-1 col-1">:</div>
					<div class="col-lg-5 col-md-5 col-11">
						<input type="text" name="jumlah_ruang" id="" class="form-control form-control-sm" value="{{ old('jumlah_ruang') }}" maxlength="12" onkeypress='validate(event)' >
						<span class="errmsg">{{ $errors->first('jumlah_ruang') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-9">Kuota</div>
					<div class="col-lg-1 col-md-1 col-1">:</div>
					<div class="col-lg-5 col-md-5 col-11">
						<input type="text" name="kapasitas" id="" class="form-control form-control-sm" value="{{ old('kapasitas') }}" maxlength="12" onkeypress='validate(event)' required>
						<span class="errmsg">{{ $errors->first('kapasitas') }}</span>
					</div>
				</div>
				<div class="row" id="jumlah_unit">
					<div class="col-lg-3 col-md-3 col-9">Jumlah Unit</div>
					<div class="col-lg-1 col-md-1 col-1">:</div>
					<div class="col-lg-5 col-md-5 col-11">
						<input type="text" name="jumlah_unit" id="" class="form-control form-control-sm" value="{{ old('jumlah_unit') }}" maxlength="12" onkeypress='validate(event)' >
						<span class="errmsg">{{ $errors->first('jumlah_unit') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-9">Nama CP 1</div>
					<div class="col-lg-1 col-md-1 col-1">:</div>
					<div class="col-lg-5 col-md-5 col-11">
						<input type="text" name="nama_cp_1" id="" class="form-control form-control-sm" value="{{ old('nama_cp_1') }}" >
						<span class="errmsg">{{ $errors->first('nama_cp_1') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-9">Telp CP 1</div>
					<div class="col-lg-1 col-md-1 col-1">:</div>
					<div class="col-lg-5 col-md-5 col-11">
						<input type="text" name="no_telp_cp_1" id="no_telp1" class="form-control form-control-sm" value="{{ old('no_telp_cp_1') }}" maxlength="12" onkeypress='validate(event)' onchange="checkTelp1(this)" >
						<span class="err" id="errTelp1"></span>
						<span class="errmsg">{{ $errors->first('telp_cp_1') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-9">Nama CP 2</div>
					<div class="col-lg-1 col-md-1 col-1">:</div>
					<div class="col-lg-5 col-md-5 col-11">
						<input type="text" name="nama_cp_2" id="" class="form-control form-control-sm" value="{{ old('nama_cp_2') }}">
						<span class="errmsg">{{ $errors->first('nama_cp_2') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-9">Telp CP 2</div>
					<div class="col-lg-1 col-md-1 col-1">:</div>
					<div class="col-lg-5 col-md-5 col-11">
						<input type="text" name="no_telp_cp_2" id="no_telp2" class="form-control form-control-sm" value="{{ old('no_telp_cp_2') }}" maxlength="12" onkeypress='validate(event)' onchange="checkTelp2(this)">
						<span class="err" id="errTelp2"></span>
						<span class="errmsg">{{ $errors->first('telp_cp_2') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-9">Surat Permohonan</div>
					<div class="col-lg-1 col-md-1 col-1">:</div>
					<div class="col-lg-5 col-md-5 col-11">
						<input type="file" name="surat_permohonan" class="custom-file-input" id="customFile" value="{{ old('surat_permohonan') }}" accept="application/pdf, .pdf, .jpg, .jpeg" required>
						<label class="custom-file-label" for="customFile">Choose file</label>
						<span class="mk">*ukuran berkas minimal 100kb maksimal 2MB.</span><br>
						<span class="errmsgsp">{{ $errors->first('surat_permohonan') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-9 btn-area">
						<button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
						<button type="submit" class="btn btn-sm btn-default1">Simpan</button>
					</div>
				</div>
			</div> 
		</div>
    </form>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	$('#metode').on('change', function() {
		var select = this.value;
		
		if(select == 'verifikasi_portofolio'){
			$('#input_verif').show();
			$('#input_tes').hide();
			$('#jumlah_unit').hide();
			$('#jumlah_ruang').hide();
			$('#lokasi_ujian').hide();
		}

		if(select == 'tes_tulis'){
			$('#input_verif').hide();
			$('#input_tes').show();
			$('#jumlah_unit').show();
			$('#jumlah_ruang').show();
			$('#lokasi_ujian').show();
		}
	});
	
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});

	$('.datepicker').datepicker({
		format: "dd/mm/yyyy"
	});


	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker3').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker4').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

	$(function () {
		$('#datetimepicker2').datetimepicker({
			format: 'LT'
		});
	});

	function AddBusinessDays(weekDaysToAdd) {
		var curdate = new Date();
		var realDaysToAdd = 0;
		while (weekDaysToAdd > 0){
			curdate.setDate(curdate.getDate()+1);
			realDaysToAdd++;
			//check if current day is business day
			if (noWeekendsOrHolidays(curdate)[0]) {
				weekDaysToAdd--;
			}
		}
		return realDaysToAdd;
    }

    var date_billed = $('#datebilled').datepicker('getDate');
    var date_overdue = new Date();
    var weekDays = AddBusinessDays(30);
    date_overdue.setDate(date_billed.getDate() + weekDays);
    date_overdue = $.datepicker.formatDate('mm/dd/yy', date_overdue);
    $('#datepd').val(date_overdue).prop('readonly', true);

    function modifyVar(obj, val) {
		obj.valueOf = obj.toSource = obj.toString = function(){ return val; };
    }

    function setToFalse(boolVar) {
		modifyVar(boolVar, false);
    }

    function setToTrue(boolVar) {
		modifyVar(boolVar, true);
    }

    var validasi_telp = new Boolean(false);
   
    function checkTelp1(element){
		if ($('#no_telp1').val().length >= 13 || $('#no_telp1').val().length <=1) {
			$('#errTelp1').html("No. Telepon/HP harus berjumlah 2 hingga 12 digit.")
			setToFalse(validasi_telp);
		} else {
			setToTrue(validasi_telp);
			$('#errTelp1').html("")
		}
	}
	
	function checkTelp2(element){
		if ($('#no_telp2').val().length >= 13 || $('#no_telp2').val().length <=1) {
			$('#errTelp2').html("No. Telepon/HP harus berjumlah 2 hingga 12 digit.")
			setToFalse(validasi_telp);
		} else {
			setToTrue(validasi_telp);
			$('#errTelp2').html("")
		}
	}

	var sp = new Boolean(true);

	$('.custom-file-input').bind('change', function() {
		var size_sp = this.files[0].size;
		var type_sp = this.files[0].type;
		
		if (type_sp == 'application/pdf') {
			if(size_sp > 2097152){
				$('.errmsgsp').html("file tidak boleh lebih dari 2MB");
				setToFalse(sp);
			}

			if(size_sp < 102796){
				$('.errmsgsp').html("file tidak boleh kurang dari 100kb");
				setToFalse(sp);
			}

			if (102796 < size_sp && size_sp < 2097152) {
				$('.errmsgsp').html("");
				setToTrue(sp);
			}
		} else if (type_sp == 'image/jpeg') {
			if(size_sp > 2097152){
				$('.errmsgsp').html("file tidak boleh lebih dari 2MB");
				setToFalse(sp);
			}

			if(size_sp < 102796){
				$('.errmsgsp').html("file tidak boleh kurang dari 100kb");
				setToFalse(sp);
			}
			
			if (102796 < size_sp && size_sp < 2097152) {
				$('.errmsgsp').html("");
				setToTrue(sp);
			}
		} else {
			$('.errmsgsp').html("type file tidak boleh selain jpg,jpeg & pdf");
			setToFalse(sp);
		}
	});
</script>
@endsection