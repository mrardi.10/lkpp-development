@extends('layout.app')
@section('title')
	Verifikasi Portofolio
@endsection
@section('css')
<style>
	.main-box{
		font-weight: 600;
		font-size: medium;
		padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }
    
	.float-right{
        float: right;
    }
    
	.publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }
    
	[data-toggle="collapse"] .fa:before {  
		content: "\f139";
    }

    [data-toggle="collapse"].collapsed .fa:before {
		content: "\f13a";
    }

    .accor{
		margin-top: 35px;
    }

    .accor-body{
		margin: 0px 10px;
    }
    
	ol.ol-sub-a{
		padding-inline-start: 18px !important;
    }

    .btn-link{
		color: #000 !important;
    }

    .btn-judul{
		width: 90%;
		text-align: left;
		background: #f8f8f8;
		-webkit-box-shadow: 0px 3px 4px -3px #000000; 
		box-shadow: 0px 3px 4px -3px #000000;
    }

    .collapse-body{
		width: 90%;
		padding: 15px;
		border: 1px solid #b3aeae;
    }

    .collapse-all{
		margin: 10px 0px;
    }

    span.err{
        color: red;
    }

    .lis{
		padding: 10px 30px;
		border-bottom: 1px solid #c3bdbd;
		margin-bottom: 10px;
    }

    .bg-active{
		background: #f8f8f8;
    }

    @media(max-width:992px){
		.wrapper{
			width:100%;
		} 
    }
    
	.panel-heading {
		padding: 0;
        border:0;
    }
    
	.panel-title>a, .panel-title>a:active{
		display:block;
        padding:15px;
		color:#555;
		font-size:16px;
		font-weight:bold;
        text-transform:uppercase;
        letter-spacing:1px;
		word-spacing:3px;
        text-decoration:none;
    }

    .panel-heading  a:before {
		font-family: 'Glyphicons Halflings';
		content: "\e114";
		float: right;
		transition: all 0.5s;
    }
    
	.panel-heading.active a:before {
        -webkit-transform: rotate(180deg);
        -moz-transform: rotate(180deg);
        transform: rotate(180deg);
    }
    
	.main-box img{
        width: 120px;
    }
    .bg-darkgrey{
    	background:#57504e;
    	color: #fff
    }
    .bg-red{
    	background:#c91c1c;
    	color: #fff
    }
</style>
@endsection
@section('content')
<form action="" method="post">
@csrf
	<div class="main-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>Verifikasi Portofolio</h3><hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-xs-11">
							Nama Peserta
						</div>
						<div class="col-lg-1 col-md-1 col-xs-11">
							:
						</div>
						<div class="col-lg-8 col-md-8 col-xs-12">
							{{ $data->nama }}
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-xs-11">
							No. Ujian
						</div>
						<div class="col-lg-1 col-md-1 col-xs-11">
							:
						</div>
						<div class="col-lg-8 col-md-8 col-xs-12">
							{{ $data->no_ujian}}
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-xs-11">
							NIP
						</div>
						<div class="col-lg-1 col-md-1 col-xs-11">
							:
						</div>
						<div class="col-lg-8 col-md-8 col-xs-12">
							{{ $data->nip }}
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-xs-11">
							Pangkat/Gol.
						</div>
						<div class="col-lg-1 col-md-1 col-xs-11">
							:
						</div>
						<div class="col-lg-8 col-md-8 col-xs-12">
							{{ $data->jabatan }}
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-xs-11">
							Jenjang
						</div>
						<div class="col-lg-1 col-md-1 col-xs-11">
							:
						</div>
						<div class="col-lg-8 col-md-8 col-xs-12">
							{{ $data->jenjang }}
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-xs-11">
							SK KP Terakhir
						</div>
						<div class="col-lg-1 col-md-1 col-xs-11">
							:
						</div>
						<div class="col-lg-8 col-md-8 col-xs-12">
						@if($data->sk_kenaikan_pangkat_terakhir == "")
							-
						@else
							@php
								$sk_kenaikan_pangkat_terakhir = explode('.',$data->sk_kenaikan_pangkat_terakhir);
							@endphp
							@if ($sk_kenaikan_pangkat_terakhir[1] == 'pdf' || $sk_kenaikan_pangkat_terakhir[1] == 'PDF')
								<a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$data->sk_kenaikan_pangkat_terakhir }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
							@else
								<a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$data->sk_kenaikan_pangkat_terakhir }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->sk_kenaikan_pangkat_terakhir }}" class="img-rounded"></a>
							@endif    
						@endif    
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-xs-11">
							Asesor
						</div>
						<div class="col-lg-1 col-md-1 col-xs-11">
							:
						</div>
						<div class="col-lg-8 col-md-8 col-xs-12">
							{{ $data->asesors }}
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-xs-11">
							Rekomendasi Asesor
						</div>
						<div class="col-lg-1 col-md-1 col-xs-11">
							:
						</div>
						@php
							$isiselect = [];
							if ($data->statuss != 2) {
								$isiselect = array('lulus' => 'Sesuai','tidak_lulus' => 'Tidak Sesuai');
							}
							
							if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp'){
								if($data->statuss == 2){
									$isiselect = array('lulus' => 'Sesuai','tidak_lulus' => 'Tidak Sesuai','tidak_lengkap' => 'Dokumen Persyaratan Tidak Lengkap');
								}
							}
						@endphp
						<div class="col-lg-8 col-md-8 col-xs-12">
							{!! Form::select('rekomendasi', $isiselect , $data->status_ujian, ['placeholder' => 'Pilih Rekomendasi', 'class' => 'form-control form-pjg']); !!}
						</div>
					</div>
					@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
					<div class="row">
						<div class="col-lg-3 col-md-3 col-xs-11"></div>
						<div class="col-lg-1 col-md-1 col-xs-11"></div>
						<div class="col-lg-8 col-md-8 col-xs-12">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="publish" id="publish" value="publish" class="publish" {{ $data->publish == 'publish' ? 'checked' : '' }}>
									Publish
								</label>
							</div>
						</div>
					</div>
					@endif
				</div>
			</div>
			<input type="hidden" name="nama_peserta" value="{{ $data->namas}}">
			<input type="hidden" name="jenjang" value="{{ $data->jenjang }}">
			<input type="hidden" name="tanggal_ujian" value="{{ Helper::tanggal_indo($data->tgl_ujian) }}">
			<div class="row">
				<div class="col-md-12">
					<h4>Dokumen Portofolio</h4><hr>
				</div>
			</div>
			<div class="wrapper" style="width:90%">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				@php
					$jumlah_poin = 0;
					$jumlah_upload = 0;
					$jumlah_upload_lain = 0;
					$total_sesuai = 0;
					$total_td_sesuai = 0;
				@endphp
				@foreach ($judul_input as $key => $poins)
					@php
						$jumlah_sebelumnya = $jumlah_poin;
						$key++;
                        $alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
						foreach($detail_input as $details){
							if($details->id_judul_input == $poins->id){
								$jumlah_dokumen = 0;
								$jumlah_sesuai = 0;
								$jumlah_td_sesuai = 0;
                                $jumlah_poin++;
                                foreach ($input_form as $inputs){
                                    if($inputs->id_detail_input == $details->id){
                                        if($dokumen != ""){
                                            foreach($dokumen as $dokumens){
												if($inputs->title_1 != "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {
													if($inputs->id == $dokumens->id_detail_file_input){
														$jumlah_dokumen++;
													}	
												}
												
												if ($inputs->title_1 == "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {
													if($inputs->id == $dokumens->id_detail_file_input){
														$jumlah_upload_lain++;
													}
												}
                                            }
                                        }
                                        
										if(($status_file != "") && ($status_file != "[]")){
                                            foreach($status_file as $statusf){
												if($inputs->id == $statusf->id_detail_file_input){
													if($statusf->status == 'sesuai'){
														$jumlah_sesuai++;
													} elseif($statusf->status == 'tidak_sesuai') {
														$jumlah_td_sesuai++; 
													}
												}
											}
										}
									}                              
								}
								
								if ($details->nama != "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {
									if($jumlah_dokumen == 2) {
										$jumlah_upload++;
									} 
								}
								
								if ($jumlah_td_sesuai == 2 || $jumlah_td_sesuai == 1 ) {
									$total_td_sesuai++;
								} 
								
								if ($jumlah_sesuai == 2 && $details->nama != "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {
									$total_sesuai++;
                                }

                                if ($jumlah_sesuai == 1 && $details->nama == "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {
									$total_sesuai++;
                                }

							}
						}
					@endphp
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="heading{{ $key }}">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}" aria-expanded="true"  
									@if($jumlah_upload == 0 ) @elseif($jumlah_upload < $poins->min_komponen) style="background: #fcba03;color: #fff" @else style="background: #5693f5;color: #fff" @endif 

									@if($jumlah_upload_lain != 0) style="background: #5693f5;color: #fff" @endif aria-controls="collapse{{ $key }}">{{ $alphabet[$key - 1].'.'.$poins->nama.' '.$poins->keterangan }}

									<span  @if($total_sesuai == 0) class="float-right badge bg-red"  @elseif($total_sesuai < $jumlah_poin - $jumlah_sebelumnya) class="float-right badge bg-red"  @else class="float-right badge bg-green" @endif style="margin-right: 1%;">
									Sesuai : {{ $total_sesuai }}/{{ $jumlah_poin - $jumlah_sebelumnya }}</span> <span class="float-right badge bg-darkgrey" style="margin-right: 1%;">Tidak Sesuai :    {{ $total_td_sesuai }}/{{ $jumlah_poin - $jumlah_sebelumnya }}</span> 
									
									<span class="float-right badge bg-black" style="color: #000;margin-right: 1%;">
										@if($poins->nama != "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan"){{ $jumlah_upload }}/{{ $jumlah_poin - $jumlah_sebelumnya }} @endif @if($poins->nama == "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {{ $jumlah_upload_lain }}/{{ $jumlah_poin - $jumlah_sebelumnya }} @endif</span>
								@php
									$jumlah_upload = 0;
									$jumlah_upload_lain = 0;
									$total_td_sesuai = 0;
									$total_sesuai = 0;
								@endphp
								</a>
							</h4>
						</div>
						<div id="collapse{{ $key }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $key }}">
							<div class="panel-body">
								<ol type="1">
								@foreach ($detail_input as $key_d => $details)
									@if ($details->id_judul_input == $poins->id)
										<li>{{ $details->nama }} @if($details->keterangan == "") @else <a href="#" data-toggle="modal" style="margin-left: 1%;" class="btn btn-sm btn-info float-right" data-target="#modalDetailDeskripsi{{ $details->id }}">Lihat Deskripsi</a>@endif
											<ol type="a" class="ol-sub-a">
											@foreach ($input_form as $inputs)
												@if ($inputs->id_detail_input == $details->id)
												<li>
													<ul style="list-style-type:none; padding: 5px 10px" class="{{ $key++ % 2 == 0 ? 'bg-active' : '' }}">
													@if ($inputs->title_1 != "")
														<li>{{ $inputs->title_1 }} : <br>
														@if($dokumen != "")
															@foreach ($dokumen as $dokumens)
																@if($inputs->id == $dokumens->id_detail_file_input)
																	@if($dokumens->file == "")
																		-
																	@else
																		@php
																			$file = explode('.',$dokumens->file);
																		@endphp
																		@if ($file[1] == 'pdf' || $file[1] == 'PDF')
																			<a href="{{ url('priview-file')."/dokumen_portofolio/".$dokumens->file }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
																		@else
																			<a href="{{ url('priview-file')."/dokumen_portofolio/".$dokumens->file }}" target="_blank"><img src="{{ asset('storage/data/dokumen_portofolio')."/".$dokumens->file }}" class="img-rounded"></a>
																		@endif    
																	@endif
																@endif
															@endforeach
														@else
															echo "-";
														@endif
														@if($dokumen_other != "")
															@foreach ($dokumen_other as $dokumens)
																@if($inputs->id == $dokumens->id_detail_file_input)
																	@if($dokumens->file == "")
																		-
																	@else
																		@php
																			$file = explode('.',$dokumens->file);
																		@endphp
																		@if ($file[1] == 'pdf' || $file[1] == 'PDF')
																			<a href="{{ url('priview-file')."/dokumen_portofolio/".$dokumens->file }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>({{ $dokumens->edited}})
																		@else
																			<a href="{{ url('priview-file')."/dokumen_portofolio/".$dokumens->file }}" target="_blank"><img src="{{ asset('storage/data/dokumen_portofolio')."/".$dokumens->file }}" class="img-rounded"></a>({{ $dokumens->edited}})
																		@endif    
																	@endif
																@endif
															@endforeach
														@else
															echo "-";
														@endif
														</li>
													@endif
													@if ($inputs->title_3 != "")
														<li>{{ $inputs->title_3 }} : <br>
															<b style="font-size:25px">
															<?php 
																if($dokumen != ""){
																	foreach ($dokumen as $dokumens) {
																		if ($inputs->id == $dokumens->id_detail_file_input) {
																			echo $dokumens->nama_sertifikat;
																		}
																	}
																} else {
																	echo "-";
																}
															?>
															</b>
														</li>
													@endif
													@if ($inputs->title_2 != "")
														<li>{{ $inputs->title_2 }} : <br>
															<b style="font-size:25px">
															<?php 
																if($dokumen != ""){
																	foreach ($dokumen as $dokumens) {
																		if ($inputs->id == $dokumens->id_detail_file_input) {
																			echo $dokumens->tahun;
																		}
																	}
																} else {
																	echo "-";
																}
															?>
															</b>
														@if (($status_file != "") && ($status_file != "[]"))
															@foreach ($status_file as $statuf)
																@if ($statuf->id_detail_file_input == $inputs->id)
																	@if($statuf->perbaikan == 1)
																		<span class="badge label-warning">Perbaikan</span>
																		<br>
																	@endif
																@endif
															@endforeach
														@endif
														</li>
													@endif

													<li>Status :<br>
													@if (($status_file != "") && ($status_file != "[]"))
														@foreach ($status_file as $statuf)
															@if ($statuf->id_detail_file_input == $inputs->id)
																
																{!! Form::select('status_'.$inputs->id, array('sesuai' => 'Sesuai', 'tidak_sesuai' => 'Tidak Sesuai') , $statuf->status, ['placeholder' => 'pilih status', 'class' => 'form-control', 'id' => 'status_'.$inputs->id]); !!}
															@endif
														@endforeach
													@endif
													@if ($status_file == "[]")
														{!! Form::select('status_'.$inputs->id, array('sesuai' => 'Sesuai', 'tidak_sesuai' => 'Tidak Sesuai') , null, ['placeholder' => 'pilih status', 'class' => 'form-control', 'id' => 'status_'.$inputs->id]); !!}
													@endif
													</li>
													<li>Keterangan :<br>
														@if ($status_file != "")
															@foreach ($status_file as $statuf)
																@if ($statuf->id_detail_file_input == $inputs->id)
																	<textarea name="keterangan_{{ $inputs->id }}"   class="form-control alp" rows="5" id="keterangan_{{ $inputs->id }}">{{ $statuf->keterangan }}</textarea>
																@endif
															@endforeach
														@endif
														@if ($status_file == "[]")
															<textarea name="keterangan_{{ $inputs->id }}" class="form-control alp" rows="5" id="keterangan_{{ $inputs->id }}"></textarea>
														@endif
													</li>    
												</ul>
											</li>
										@endif
									@endforeach
                                    </ol>
                                </li>
                                <!-- Modal Deskripsi-->
								<div class="modal fade" id="modalDetailDeskripsi{{ $details->id }}" role="dialog">
									<div class="modal-dialog">			
										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title">Detail Deskripsi {{ $details->nama}}</h4>
												<button type="button" class="close" data-dismiss="modal">&times;</button>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<p>Dokumen Hasil Pekerjaan:</p>
													</div>
													<div class="col-md-12">
														<p>{!! $details->keterangan !!}</p>
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-sm btn-default2" data-dismiss="modal">Kembali</button>
												{{-- <button type="submit" class="btn btn-sm btn-default1">Assign</button> --}}
											</div>
										</div>
									</div>
								</div>
							@endif
                        @endforeach
                    </ol>
				</div>
			</div>
		</div>
	@endforeach
	</div>
</div>
        {{-- <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-11">
                Kompetensi Perencanaan PBJP
            </div>
            <div class="col-lg-1 col-md-1 col-xs-1">
                :
            </div>
            <div class="col-lg-7 col-md-7 col-xs-12">
            @php
            $kompetensi_perencanaan_pbjp = explode('.',$data->kompetensi_perencanaan_pbjp);
            @endphp
            @if ($kompetensi_perencanaan_pbjp[1] == 'pdf' || $kompetensi_perencanaan_pbjp[1] == 'PDF')
            <a href="{{ url('priview-file')."/kompetensi_perencanaan_pbjp/".$data->kompetensi_perencanaan_pbjp }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
            @else
            <a href="{{ url('priview-file')."/kompetensi_perencanaan_pbjp/".$data->kompetensi_perencanaan_pbjp }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->kompetensi_perencanaan_pbjp }}" class="img-rounded"></a>
            @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-11">
                Kompetensi Pemilihan PBJ
            </div>
            <div class="col-lg-1 col-md-1 col-xs-1">
                :
            </div>
            <div class="col-lg-7 col-md-7 col-xs-12">
            @php
            $kompetensi_pemilihan_pbj = explode('.',$data->kompetensi_pemilihan_pbj);
            @endphp
            @if ($kompetensi_pemilihan_pbj[1] == 'pdf' || $kompetensi_pemilihan_pbj[1] == 'PDF')
            <a href="{{ url('priview-file')."/kompetensi_pemilihan_pbj/".$data->kompetensi_pemilihan_pbj }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
            @else
            <a href="{{ url('priview-file')."/kompetensi_pemilihan_pbj/".$data->kompetensi_pemilihan_pbj }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->kompetensi_pemilihan_pbj }}" class="img-rounded"></a>
            @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-11">
                Kompetensi Pengelolaan Kontrak PBJP
            </div>
            <div class="col-lg-1 col-md-1 col-xs-1">
                :
            </div>
            <div class="col-lg-7 col-md-7 col-xs-12">
            @php
            $kompetensi_pengelolaan_kontrak_pbjp = explode('.',$data->kompetensi_pengelolaan_kontrak_pbjp);
            @endphp
            @if ($kompetensi_pengelolaan_kontrak_pbjp[1] == 'pdf' || $kompetensi_pengelolaan_kontrak_pbjp[1] == 'PDF')
            <a href="{{ url('priview-file')."/kompetensi_pengelolaan_kontrak_pbjp/".$data->kompetensi_pengelolaan_kontrak_pbjp }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
            @else
            <a href="{{ url('priview-file')."/kompetensi_pengelolaan_kontrak_pbjp/".$data->kompetensi_pengelolaan_kontrak_pbjp }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->kompetensi_pengelolaan_kontrak_pbjp }}" class="img-rounded"></a>
            @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-11">
                Kompetensi PBJP Secara Swakelola
            </div>
            <div class="col-lg-1 col-md-1 col-xs-1">
                :
            </div>
            <div class="col-lg-7 col-md-7 col-xs-12">
            @php
            $kompetensi_pbj_secara_swakelola = explode('.',$data->kompetensi_pbj_secara_swakelola);
            @endphp
            @if ($kompetensi_pbj_secara_swakelola[1] == 'pdf' || $kompetensi_pbj_secara_swakelola[1] == 'PDF')
            <a href="{{ url('priview-file')."/kompetensi_pbj_secara_swakelola/".$data->kompetensi_pbj_secara_swakelola }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
            @else
            <a href="{{ url('priview-file')."/kompetensi_pbj_secara_swakelola/".$data->kompetensi_pbj_secara_swakelola }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->kompetensi_pbj_secara_swakelola }}" class="img-rounded"></a>
            @endif
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-10" style="text-align: right">
                <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
                {{-- <button type="submit" class="btn btn-sm btn-default1">Simpan</button> --}}
                <input type="submit" name="simpan" value="Simpan" class="btn btn-sm btn-default1" id="standar">
                <input type="submit" name="simpan" value="status" id="status_btn" class="hidden">
            </div>
        </div>
    </div>
</div>
</form>
@endsection
@section('js')
<script>
	$(document).ready(function() {
		@foreach ($judul_input as $poins)
			@foreach($detail_input as $details)
				@if ($details->id_judul_input == $poins->id)
					@foreach ($input_form as $inputs)
						@if ($inputs->id_detail_input == $details->id)
							$('#keterangan_{{ $inputs->id }}').hide();
						@endif
					@endforeach
				@endif
			@endforeach
		@endforeach
    });

	$(document).ready(function() {
		@foreach ($judul_input as $poins)
			@foreach($detail_input as $details)
				@if ($details->id_judul_input == $poins->id)
					@foreach ($input_form as $inputs)
						@if ($inputs->id_detail_input == $details->id)
							var keterangan_{{ $inputs->id }} = $('#status_{{ $inputs->id }}').val();
                            if(keterangan_{{ $inputs->id }} == 'tidak_sesuai'){
                                $('#keterangan_{{ $inputs->id }}').show();
                            }
						@endif
					@endforeach
				@endif
			@endforeach
		@endforeach
	});
	
	@foreach ($judul_input as $poins)
		@foreach($detail_input as $details)
			@if ($details->id_judul_input == $poins->id)
				@foreach ($input_form as $inputs)
					@if ($inputs->id_detail_input == $details->id)
					$(document).ready(function() {
   

						$('#status_{{ $inputs->id }}').on('change', function() {
							var select_{{ $inputs->id }} = this.value;
							if(select_{{ $inputs->id }} == 'tidak_sesuai'){
								$('#keterangan_{{ $inputs->id }}').show();

								$('#keterangan_{{ $inputs->id }}').on('change', function() {
										if ($('#keterangan_{{ $inputs->id }}').val() != "") {
									
										$("#status_btn").trigger('click'); 
									}
								});
								
							}
							
							if(select_{{ $inputs->id }} == 'sesuai'){
								$('#keterangan_{{ $inputs->id }}').hide();
								$("#status_btn").trigger('click'); 
							}
							
							if(select_{{ $inputs->id }} == ''){
								$('#keterangan_{{ $inputs->id }}').hide();
								$("#status_btn").trigger('click'); 
							}
						});
				});
					@endif
				@endforeach
			@endif
		@endforeach
	@endforeach

	$('.panel-collapse').on('show.bs.collapse', function () {
		$(this).siblings('.panel-heading').addClass('active');
	});

	$('.panel-collapse').on('hide.bs.collapse', function () {
		$(this).siblings('.panel-heading').removeClass('active');
	});

	$(".alp").keyup(function(event) {
		if(event.which != 13)
			return;
		var elm = $(this);
		var lines = elm.val().split("\n");
		for(var i=0; i<lines.length; i++)
			lines[i] = lines[i].replace(/([a-zA-Z ]+\.\s|^)/, (i+10).toString(36) + ". ");
		
		elm.val(lines.join("\n"));
	});
</script>
@endsection