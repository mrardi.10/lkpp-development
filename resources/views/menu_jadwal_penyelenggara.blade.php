@extends('layout.app2')
@section('title')
	Jadwal Uji Kompetensi (Instansi)
@endsection
@section('css')
<style>
	body{
		background-color: whitesmoke;
	}
	
	.main-page{
		margin-top: 20px;
	}

	.box-container{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		background-color: white;
		border-radius: 5px;
		padding: 3%;
	}

	.shadow{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		max-width: 250px;
		line-height: 15px;
		width: 100%;
		margin: 5px;
	}

	div.dataTables_wrapper div.dataTables_info{
		display: none;
	}

	div.dataTables_wrapper .row.col-sm-12{
		width: 120px;
	}

	p{
		font-weight: 500;
	}

	b{
		font-weight: 500;
	}

	.row a.btn{
		text-align: left;
		font-weight: 600;
		font-size: small;
	}

	.btn-default1{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
		color: white;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default2{
		background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
		color: black;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default3{
		background: #E6341E;
		color: #fff;
		font-weight: 600;
		width: 140px;
	}

	.row-input{
		padding-bottom: 10px;
	}

	.page div.verifikasi{
		display: none;
	}

	.btn-area{
        text-align: right;
    }
</style>
@endsection
@section('content')
<div class="main-page">
	<div class="container">
		<div class="row box-container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12" style="">
						<div class="row">
							<div class="col-md-9">
								<h5>Jadwal Uji Kompetensi (Instansi)</h5><hr>
								@if (session('msg'))
									@if (session('msg') == "berhasil")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil Simpan Data</strong>
										</div> 
									@else
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal Simpan Data</strong>
										</div> 
									@endif
								@endif
							</div>
							<div class="col-md-3" style="text-align:right"></div>
								<div class="col-md-12 page">
									<div class="main-box" id="view">
										<div class="min-top">
											<div class="row">
												<div class="col-md-1 text-center">
													<b>Perlihatkan</b>
												</div>
												<div class="col-md-2">
													<select name='length_change' id='length_change' class="form-control form-control-sm">
														<option value='50'>50</option>
														<option value='100'>100</option>
														<option value='150'>150</option>
														<option value='200'>200</option>
													</select>
												</div>
												<div class="col-md-4 col-12">
													<div class="form-group" style="margin-bottom:0px !important">
														<div class="input-group input-group-sm">
															<div class="input-group-prepend">
																<span class="input-group-text"><i class="fa fa-search"></i></span>
															</div>
															<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
														</div>
													</div>
												</div>
												<div class="col-md-2"></div>
												<div class="col-md-1 text-center">
													<b>Bulan</b>
												</div>
												<div class="col-md-2 float-left">
													<form action="" method="post">
													@csrf
														<select name='bulan' id='bulan' class="form-control form-control-sm" onchange="this.form.submit()">
															<option value='' {{ $bulan == '' ? 'selected' : '' }}>Pilih Bulan</option>
															<option value='1' {{ $bulan == 1 ? 'selected' : '' }}>Januari</option>
															<option value='2' {{ $bulan == 2 ? 'selected' : '' }}>Februari</option>
															<option value='3' {{ $bulan == 3 ? 'selected' : '' }}>Maret</option>
															<option value='4' {{ $bulan == 4 ? 'selected' : '' }}>April</option>
															<option value='5' {{ $bulan == 5 ? 'selected' : '' }}>Mei</option>
															<option value='6' {{ $bulan == 6 ? 'selected' : '' }}>Juni</option>
															<option value='7' {{ $bulan == 7 ? 'selected' : '' }}>Juli</option>
															<option value='8' {{ $bulan == 8 ? 'selected' : '' }}>Agustus</option>
															<option value='9' {{ $bulan == 9 ? 'selected' : '' }}>September</option>
															<option value='10' {{ $bulan == 10 ? 'selected' : '' }}>Oktober</option>
															<option value='11' {{ $bulan == 11 ? 'selected' : '' }}>November</option>
															<option value='12' {{ $bulan == 12 ? 'selected' : '' }}>Desember</option>
														</select>
													</form>
												</div>
											</div> 
										</div>
										<div class="table-responsive"> 
											<table id="example1" class="table table-bordered table-striped">
												<thead>
													<tr>
														<th>No</th>
														<th>Nama Instansi</th>
														<th>Tanggal</th>
														<th>Metode</th>
														<th>Waktu</th>
														<th>Lokasi</th>
														<th>Kuota</th>
														<th>Batas Akhir Pendaftaran</th>
														<th>Aksi</th>
													</tr>
												</thead>
												<tbody>
												@foreach ($data as $key => $datas)
													<tr>
														<td>{{ $key++ + 1 }}</td>
														<td>{{ $datas->instansis }}</td>                                        
														<td>{{ Helper::tanggal_indo($datas->tanggal_ujian) }}</td>
														<td>{{ Helper::getMetodeInpassing($datas->metode) }}</td>
														<td>{{ $datas->metode == 'tes_tulis' ? $datas->waktu_ujian : $datas->waktu_verifikasi}}</td>
														<td>{{ $datas->lokasi_ujian }}</td>
														<td>{{ $datas->jumlah_peserta }}/{{ $datas->jumlah_unit }}</td>
														@if($datas->batas_waktu_input == "0000-00-00" || $datas->batas_waktu_input == null )
															<td>{{ '-' }}</td>
														@else
															<td>{{ Helper::tanggal_indo($datas->batas_waktu_input) }}</td>
														@endif
														<td>
															<a href="{{ url('menu-peserta-instansi/'.$datas->id) }}" class="btn btn-sm btn-primary" >Lihat Peserta</a>
															@if($datas->hasil_ujian != "")<br>
																<a href="{{ url('hasil-ujian')."/instansi/".$datas->hasil_ujian }}" target="_blank" class="btn btn-sm btn-warning">Unduh Hasil Ujian</a>
															@else
															@endif
														</td>
													</tr>
												@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> 
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script>
	$('#btn-tambah').click(function (e) {
		e.preventDefault();
        document.getElementById("verif").style.display = "block";
        document.getElementById("view").style.display = "none";
    });
    
    $('#btn-back').click(function (e) {
        e.preventDefault();
        document.getElementById("verif").style.display = "none";
        document.getElementById("view").style.display = "block";
    });

    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
</script>  
@endsection