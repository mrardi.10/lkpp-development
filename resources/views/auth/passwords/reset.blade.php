@extends('layout.app2')
@section('title')
	Ubah Password
@endsection
@section('css')
<style>
    body{
		width: 100%;
    }

    .bg-top{
		width: 100%;
		background-image: url('{{ asset('assets/img/back.jpg') }}'); /* The image used */
		background-color: #cccccc; /* Used if the image is unavailable */
		height: 600px; /* You must set a specified height */
		background-position: center; /* Center the image */
		background-repeat: no-repeat; /* Do not repeat the image */
		background-size: cover; /* Resize the background image to cover the entire container */
    }

    .info-txt{
		padding: 50px;
    }

    .info-txt hr{
		border-top: 1px solid rgba(0, 0, 0, 0.43);
    }

    .txt-tp{
		font-weight: 600;
    }

    .txt-btm{
		font-size: 22px;
    }

    .lbl-form{
		width: 100%;
		font-weight: 600;
		color: gray;
    }

    .card-head{
		color: white;
		padding: 10px 20px 10px 20px;
		background: #ff4141;
		margin: -1px;
    }

    .card-head h3{
		font-weight: 600;
    }

    .card-body{
		padding-bottom: 10px;
    }

    .card-bottom{
		flex: 1 1 auto;
		padding: 0px 20px 10px 20px;
		margin: 35px 0px;
    }

    .card{
		top : 100px; 
		-webkit-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
		-moz-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
		box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
    }

    .card .form-control{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    }

    .btn-log{
		background: #ff4141;
		font-weight: 600;
		color: white;
		width: 100px;
    }

    .btn-daf{
		background: slategray;
		font-weight: 600;
		color: white;
		width: 100px;
    }

    .p-daf{
		margin: 15px 0px;
    }

    @media(min-width: 320px) and (max-width: 768px) {
		.card{
			margin-top: 10px;
		} 
    }
    
	.running{
		height: 50px;
		background: #3a394e;
    }

    #type {
		margin-bottom: 15px;
		font-size: 18px;
		font-weight: 200;
		color: #ffffff;
    }
    
	@media screen and (min-width: 768px) {
        #type {
            font-size: 23px;
        }
    }

    .txt-running{
		line-height: 2;
    }

    span.badge{
		vertical-align: -webkit-baseline-middle;
		width: 100%;
		font-size: 16px;
		font-weight: 600;
		color: #3a394e;
    }

    .btn-login{
		margin: 10px;
    }
    
    .err{
		color: red;
    }
</style>
@endsection

@section('content')
<div class="bg-top">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Ubah Password</div>
						<div class="card-body">
						@if (session('status'))
							<div class="alert alert-success" role="alert">
								{{ session('status') }}
							</div>
						@endif
						
						@if ($msg == 'salah')
						<div class="alert alert-success" role="alert">
							Terjadi Kesalahan
						</div>  
						@endif
						
						@if ($msg == 'benar')
						<form method="POST" action="">
						@csrf
							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Password Baru</label>
								<div class="col-md-6">
									<input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" required>
									<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword()"></span>
									<span class="err">{{ $errors->first('password') }}</span>
								</div>
							</div>
							<div class="form-group row">
								<label for="c_password" class="col-md-4 col-form-label text-md-right">Ulangi Password Baru</label>
								<div class="col-md-6">
									<input id="c_password" type="password" class="form-control" name="c_password" value="{{ old('c_password') }}" required>
									<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password2" onclick="showPassword2()"></span>
									<span class="err">{{ $errors->first('c_password') }}</span>
								</div>
							</div>
							<div class="form-group row mb-0">
								<div class="col-md-6 offset-md-4">
									<button type="submit" class="btn btn-primary">
										Ubah Password
									</button>
								</div>
							</div>
						</form>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	function showPassword() {
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}	
	}
	
	function showPassword2() {
		var x = document.getElementById("c_password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
</script>
@endsection