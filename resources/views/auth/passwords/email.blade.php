@extends('layout.app2')
@section('title')
	Lupa Password
@endsection
@section('css')
<style>
	body{
		width: 100%;
    }

    .bg-top{
		width: 100%;
		background-image: url('{{ asset('assets/img/back.jpg') }}'); /* The image used */
		background-color: #cccccc; /* Used if the image is unavailable */
		height: 600px; /* You must set a specified height */
		background-position: center; /* Center the image */
		background-repeat: no-repeat; /* Do not repeat the image */
		background-size: cover; /* Resize the background image to cover the entire container */
    }

    .info-txt{
		padding: 50px;
    }

    .info-txt hr{
		border-top: 1px solid rgba(0, 0, 0, 0.43);
    }

    .txt-tp{
		font-weight: 600;
    }

    .txt-btm{
		font-size: 22px;
    }

    .lbl-form{
		width: 100%;
		font-weight: 600;
		color: gray;
    }

    .card-head{
		color: white;
		padding: 10px 20px 10px 20px;
		background: #ff4141;
		margin: -1px;
    }

    .card-head h3{
		font-weight: 600;
    }

    .card-body{
		padding-bottom: 10px;
    }

    .card-bottom{
		flex: 1 1 auto;
		padding: 0px 20px 10px 20px;
		margin: 35px 0px;
    }

    .card{
		top : 100px; 
		-webkit-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
		-moz-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
		box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
    }

    .card .form-control{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    }

    .btn-log{
		background: #ff4141;
		font-weight: 600;
		color: white;
		width: 100px;
    }

    .btn-daf{
		background: slategray;
		font-weight: 600;
		color: white;
		width: 100px;
    }

    .p-daf{
		margin: 15px 0px;
    }

    @media(min-width: 320px) and (max-width: 768px) {
      .card{
			margin-top: 10px;
      } 
    }
    .running{
		height: 50px;
		background: #3a394e;
    }

    #type {
		margin-bottom: 15px;
		font-size: 18px;
		font-weight: 200;
		color: #ffffff;
    }
    
	@media screen and (min-width: 768px) {
        #type {
            font-size: 23px;
        }
    }

    .txt-running{
		line-height: 2;
    }

    span.badge{
		vertical-align: -webkit-baseline-middle;
		width: 100%;
		font-size: 16px;
		font-weight: 600;
		color: #3a394e;
    }

    .btn-login{
		margin: 10px;
    }

    .email-input{
		margin-top: 12px;
    }
</style>
@endsection
@section('content')
<div class="bg-top">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Ubah Password</div>
					<div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($msg == 'berhasil_kirim')
                    <div class="alert alert-success" role="alert">
						Link ubah password telah terkirim. Silakan cek email Anda.
                    </div>  
                    @endif
                    @if ($msg == 'email_tidak_terdaftar')
                    <div class="alert alert-warning" role="alert">
						Masukan email yang terdaftar
                    </div>  
                    @endif
                    @if ($msg == 'berhasil_reset')
                    <div class="alert alert-success" role="alert">
						Password berhasil diubah
                    </div>  
                    @endif
                    @if ($msg == 'gagal_reset')
                    <div class="alert alert-warning" role="alert">
						Password gagal diubah
                    </div>  
                    @endif
                    @if ($msg == 'salah')
                    <div class="alert alert-warning" role="alert">
						Terjadi Kesalahan silahkan Cek email anda lagi.
                    </div>  
                    @endif
                    <form method="POST" action="">
					@csrf
						<div class="form-group row">
							<label for="email" class="col-md-4 col-form-label text-md-right">Masukkan email yang terdaftar</label>
                            <div class="col-md-6">
								<input id="email" type="email" class="form-control email-input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
								<button type="submit" class="btn btn-primary">
                                    Kirim Link Ubah Password
                                </button>
                            </div>
                        </div>
                    </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection