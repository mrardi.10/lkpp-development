@extends('layout.app')
@section('title')
	Data Asesor
@stop
@section('css')
<style>
	.btn-tbh{
		text-align: right;
	}

	.btn-jadwal{
		width: 120px;
		background: #E8382A;
		color: #fff;
		font-weight: 600;
	}

	.btn-jadwal:hover{
		color: aliceblue;
	}
</style>
@stop
@section('content')
	@if (session('msg'))
		
		@if (session('msg') == "berhasil")
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Berhasil Simpan Data</strong>
					</div>
				</div>
			</div>
		@endif 
		
		@if (session('msg') == "gagal")
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-warning alert-dismissible">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Gagal Simpan Data</strong>
					</div>
				</div>
			</div> 
		@endif
		
		@if (session('msg') == "berhasil_update")
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Berhasil Update Data</strong>
					</div>
				</div>
			</div>
		@endif 
		
		@if (session('msg') == "gagal_update")
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-warning alert-dismissible">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Gagal Update Data</strong>
					</div>
				</div>
			</div> 
		@endif
		
		@if (session('msg') == "berhasil_hapus")
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Berhasil Hapus Data</strong>
					</div>
				</div>
			</div>
		@endif 
		
		@if (session('msg') == "gagal_hapus")
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-warning alert-dismissible">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Gagal Hapus Data</strong>
					</div>
				</div>
			</div> 
		@endif
	@endif
<div class="main-box">
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2">
				<select name='length_change' id='length_change' class="form-control">
					<option value='50'>50</option>
					<option value='100'>100</option>
					<option value='150'>150</option>
					<option value='200'>200</option>
				</select>
			</div>
			<div class="col-md-4 col-12">
				<div class="input-group">
					<div class="input-group addon">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
						<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
					</div>
				</div>
			</div>
			<div class="col-md-5 col-xm-12 btn-tbh">
			@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
				<a href="{{ url('tambah-asesor') }}"><button class="btn btn-sm btn-jadwal">Tambah Asesor</button></a>
			@endif
			</div>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th width="25%">Nama</th>
					<th width="20%">Email</th>
					<th width="20%">Unit Kerja</th>
					<th width="20%">Status</th>
					<th width="10%">Aksi</th>
				</tr>
				</thead>
				<tbody>
				@foreach ($data as $key => $datas)
				<tr>
					<td>{{ $key++ + 1 }}</td>
					<td>{{ $datas->name }}</td>
					<td>{{ $datas->email }}</td>
					<td>{{ $datas->unit_kerjas }}</td>
					<td>{{ $datas->status_admin == 'aktif' ? 'Aktif' : 'Tidak Aktif' }}</td>
					<td>
						<div class="dropdown">
							<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button">...</button>
							<ul class="dropdown-menu">
							@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
								<li><a href="{{ url('detail-asesor/'.$datas->id) }}">Lihat Detail</a></li>
								<li><a href="{{ url('ubah-asesor/'.$datas->id) }}">Ubah</a></li>
								<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $datas->id }}">Hapus</a></li>
							@endif
							</ul>
						</div>	
					</td>
				</tr>
				<div class="modal fade" id="modal-default{{ $datas->id }}">
					<div class="modal-dialog" style="width:30%">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Hapus Asesor</h4>
							</div>
							<div class="modal-body">
								<p>Apakah Anda yakin menghapus Akun Asesor?</p>
							</div>
							<div class="modal-footer">
								<a href="{{ url('hapus-asesor/'.$datas->id) }}" type="button" class="btn btn-primary pull-left">HAPUS</a>
								<button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
							</div>
						</div>
					</div>
				</div>
				@endforeach	
			</tbody>
		</table>
	</div>
</div>
@stop

@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop