@extends('layout.app2')
@section('title')
	Tambah Peserta PPK
@endsection
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
	.jud-register{
		padding: 15px;
        color: #3a394e;
    }

    .box{
        -webkit-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
        -moz-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
        box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
    }

    .btn-default1{
        background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
        color: white;
        font-weight: 600;
        width: 100px; 
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .btn-default2{
        background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
        color: black;
        font-weight: 600;
        width: 100px; 
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .btn-default1:hover{
        color: #eee;
        background: #c10013;
    }

    .btn-area{
        text-align: right;
    }

    .navi ul{
		padding:0;
		font-size:0;
		overflow:hidden;
		display:inline-block;
		width: 100%;
    }
	
	.navi li{
		display:inline-block;
		text-align: center;
    }
    
	.navi a{
		font-size:1rem;
		position:relative;
		display:inline-block;
		background:#eee;
		text-decoration:none;
		color:#555;
		padding:13px 25px 13px 10px;
		width: 100%;
		font-weight: 600;
    }
    
	.navi a:after,
    .navi a:before{
		position: absolute;
		content: "";
		height: -10px;
		width: 0px;
		top: 50%;
		left: -28px;
		margin-top: -25px;
		border: 25px solid #eee;
		border-right: 2 !important;
		border-left-color: transparent !important;
    }
    
	.navi a:before{
		left:-26px;
		border: 24px solid #555;
    }
    
	/* ACTIVE STYLES */
    .navi a.active{
        background: #e74c3c;
        color: #fff;
        -webkit-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
	}
    
	.navi a.active:after{border-color:#e74c3c;}
    
	/* HOVER STYLES */
    .navi a:hover{
        background: #e74c3c;
        color: #fff;
        -webkit-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
    }
    
	.navi a:hover:after{ border-color:#e74c3c;}

    #page-two{
        display: none;
    }

    #page-three{
        display: none;
    }

    div.container div.row{
        padding: 0px 2%; 
        margin-bottom: 1%;
    }

    h5 span{
        color: #e74c3c;
    }

    label.custom-file-label{
        margin: 0px 15px;
    }

    .row .form-control{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    }

    .row .custom-file-label{
        -webkit-box-shadow: 0px 0px 2px 2px rgba(0,0,0,0.1);
        -moz-box-shadow: 0px 0px 2px 2px rgba(0,0,0,0.1);
        box-shadow: 0px 0px 2px 2px rgba(0,0,0,0.1);
    }

    .line-txt{
        line-height: 2.5;
    }

    .select2-container{
        border: 1px solid #ced4da;
        padding: 1px;
        box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    }   
	
    .select2-container--default .select2-selection--single{
        border:0px !important;
    }
	
    span.err{
        color: red;
    }
</style>
@endsection
@section('content')
@inject('request', 'Illuminate\Http\Request')
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<h3 class="jud-register">Daftar Peserta Uji Kompetensi Inpassing JF PPBJ</h3>
        </div>
		<div class="col-md-12">
			<form action="" method="post" enctype="multipart/form-data" id="pesertaForm">
			@csrf
				<div class="box">
                    <div class="navi">
                        <ul>
                        <li style="width:35%"><a href="#" id="li-one" class="active">1.Data Peserta</a></li>
                        <li style="width:65%"><a href="#" id="li-two">2.Unggah Dokumen Persyaratan</a></li>
                        </ul>
                    </div>
                    <div class="page container-fluid" id="page-one">
                        <div class="container">
                                @if (session('msg') == "jenjang_penuh")
                                <div class="alert alert-warning alert-dismissible">
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                  <strong>Gagal Tambah Peserta, Jenjang Peserta Yang Anda pilih Sudah Memenuhi batas</strong>
                                </div> 
                                @endif
                            <div class="alert alert-info alert-dismissible">
                                  {{-- <button type="button" class="close" data-dismiss="alert">&times;</button> --}}
                                  <strong style="text-transform: uppercase;">Dokumen yang diunggah min. 100KB max. 2MB, dengan format PDF, JPG, JPEG</strong>
                            </div><br>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Nama<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="nama" id="nama" class="form-control form-control-sm" value="{{ old('nama') }}" required>
                                    <span class="err" id="errNama"></span>
                                    <span class="errmsg">{{ $errors->first('nama') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>NIP<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="nip" id="nip" class="form-control form-control-sm" value="{{ old('nip') }}" maxlength="18" onkeypress='validate(event)' onblur="duplicateNip(this)" required>
                                    <span class="err" id="errNip"></span>
                                    <span class="errmsg">{{ $errors->first('nip') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Pangkat/Gol.<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <select name="jabatan" id="pangkat" class="form-control form-control-sm" onchange="getval(this);">
                                        <option value="" {{ old('jabatan') == "" ? 'selected' : ''}}>Pilih Pangkat/Gol.</option>
                                        <option value="Penata Muda (III/a)" {{ old('jabatan') == "Penata Muda (III/a)" ? 'selected' : ''}}>Penata Muda (III/a)</option>
                                        <option value="Penata Muda Tk.I (III/b)" {{ old('jabatan') == "Penata Muda Tk.I (III/b)" ? 'selected' : ''}}>Penata Muda Tk.I (III/b)</option>
                                        <option value="Penata (III/c)" {{ old('jabatan') == "Penata (III/c)" ? 'selected' : ''}}>Penata (III/c)</option>
                                        <option value="Penata Tk.I (III/d)" {{ old('jabatan') == "Penata Tk.I (III/d)" ? 'selected' : ''}}>Penata Tk.I (III/d)</option>
                                        <option value="Pembina (IV/a)" {{ old('jabatan') == "Pembina (IV/a)" ? 'selected' : ''}}>Pembina (IV/a)</option>
                                        <option value="Pembina Tk.I (IV/b)" {{ old('jabatan') == "Pembina Tk.I (IV/b)" ? 'selected' : ''}}>Pembina Tk.I (IV/b)</option>
                                        <option value="Pembina Utama Muda (IV/c)" {{ old('jabatan') == "Pembina Utama Muda (IV/c)" ? 'selected' : ''}}>Pembina Utama Muda (IV/c)</option>
                                    </select>
                                    <span class="err" id="errPangkat"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>TMT Pangkat/Gol.<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="tmt_panggol" id="datepicker2" class="form-control form-control-sm tmt_panggol" value="{{ old('tmt_panggol') }}" placeholder="dd/mm/yyyy" required>
                                    <span class="err" id="errTmtPanggol"></span>
                                    <span class="errmsg">{{ $errors->first('tmt_panggol') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Usulan Jenjang<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="jenjang" id="jenjang" class="form-control form-control-sm" value="{{ old('jenjang') }}" required readonly>
                                    {{-- <input type="hidden" name="jenjang" id="jenjang"> --}}
                                    <span class="err" id="errJenjang"></span>
                                    <span class="errmsg">{{ $errors->first('jenjang') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>No. Surat Usulan Mengikuti Penyesuaian/Inpassing<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    {!! Form::select('no_surat', $no_surat , old('no_surat'), ['placeholder' => 'Pilih No. Surat Usulan', 'class' => 'form-control js-example-basic-single', 'id' => 'no_surat', 'disabled']); !!}
                                    <span class="err" id="errno_surat"></span>
                                    <span class="errmsg">{{ $errors->first('no_surat') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Tempat Lahir<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control form-control-sm" value="{{ old('tempat_lahir') }}" required onkeypress='validateHuruf(event)'>
                                    <span class="err" id="errTempatLahir"></span>
                                    <span class="errmsg">{{ $errors->first('tempat_lahir') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Tanggal Lahir<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="tanggal_lahir" id="datepicker" class="form-control form-control-sm tanggal_lahir" value="{{ old('tanggal_lahir') }}" placeholder="dd/mm/yyyy" required>
                                    <span class="err" id="errTanggalLahir"></span>
                                    <span class="errmsg">{{ $errors->first('tanggal_lahir') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Jenis Kelamin<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    {{-- {!! Form::select('jenis_kelamin', array('pria' => 'LAKI - LAKI', 'wanita' => 'PEREMPUAN') , old('jenis_kelamin'), ['placeholder' => 'pilih', 'class' => 'form-control jenis_kelamin', 'id' => 'select-single']); !!} --}}
                                    <label class="radio-inline">
                                        <input type="radio" name="jenis_kelamin" value="pria" checked>Laki-laki
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="jenis_kelamin" value="wanita">Perempuan
                                    </label>
                                    <span class="err" id="errJenisKelamin"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>No. KTP<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="no_ktp" id="no_ktp" class="form-control form-control-sm" value="{{ old('no_ktp') }}" maxlength="16" onkeypress='validate(event)' required>
                                    <span class="err" id="errKtp"></span>
                                    <span class="errmsg">{{ $errors->first('no_ktp') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>No. Sertifikat PBJ Tk. Dasar<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="no_sertifikat" id="no_sertifikat" class="form-control form-control-sm" value="{{ old('no_sertifikat') }}" maxlength="15" onkeypress='validate(event)' onblur="duplicateNoSertifikat(this)" required>
                                    <span class="err" id="errSertifikat"></span>
                                    <span class="errmsg">{{ $errors->first('no_sertifikat') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Pendidikan Terakhir<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    {!! Form::select('pendidikan_terakhir', array('D4' => 'D4', 'S1' => 'S1', 'S2' => 'S2', 'S3' => 'S3') , old('pendidikan_terakhir'), ['placeholder' => 'Pilih Pendidikan Terakhir', 'class' => 'form-control pendidikan_terakhir', 'id' => 'select-single']); !!}
                                    <span class="err" id="errPendidikan"></span>
                                    <span class="errmsg">{{ $errors->first('pendidikan_terakhir') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Email<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="email" name="email" id="email" class="form-control form-control-sm" value="{{ old('email') }}" onblur="duplicateEmail(this)" required>
                                    <span class="err" id="errEmail"></span>
                                    <span class="errmsg">{{ $errors->first('email') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>No. Telp/HP<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="no_telp" id="no_telp" class="form-control form-control-sm" value="{{ old('no_telp') }}" maxlength="12" minlength="2" onchange="checkTelp(this)" onkeypress='validate(event)' required>
                                    <span class="err" id="errTelp"></span>
                                    <span class="errmsg">{{ $errors->first('no_telp') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Nama Instansi<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="nama_instansi" id="nama_instansi" class="form-control form-control-sm" value="{{ $request->session()->get('instansis_user') }}" required disabled>
                                    <span class="err" id="errInstansi"></span>
                                    <span class="errmsg">{{ $errors->first('nama_instansi') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Nama Satuan Kerja/OPD<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="satuan_kerja" id="satuan_kerja" class="form-control form-control-sm" value="{{ old('satuan_kerja') }}" required onkeypress='validateHuruf(event)'>
                                    <span class="err" id="errSatuanKerja"></span>
                                    <span class="errmsg">{{ $errors->first('satuan_kerja') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Provinsi<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    {!! Form::select('provinsi', $provinsi , old('provinsi'), ['placeholder' => 'Pilih Provinsi', 'class' => 'form-control js-example-basic-single', 'id' => 'provinsi']); !!}
                                    <span class="err" id="errProvinsi"></span>
                                    <span class="errmsg">{{ $errors->first('provinsi') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Kabupaten/Kota<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    {!! Form::select('kab_kota', array('' => '') , old('kab_kota'), ['placeholder' => 'Pilih Kab/Kota', 'class' => 'form-control js-example-basic-single-2', 'id' => 'kab_kota']); !!}
                                    <span class="err" id="errKabKota"></span>
                                    <span class="errmsg">{{ $errors->first('kab_kota') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 btn-area">
							<button class="btn btn-sm btn-default2" type="button" onclick="window.history.go(-1); return false;"><i class="fa fa-angle-left"> </i>Kembali</button>
							<button class="btn btn-sm btn-default1" id="btnOne" onclick="btnOne()">Selanjutnya<i class="fa fa-angle-right"></i></button>
                        </div>
                    </div>
                    <div class="page container-fluid" id="page-two">
                        <div class="container">
                            <div class="alert alert-info alert-dismissible">
                                  <strong style="text-transform: uppercase;">Dokumen yang diunggah min. 100KB max. 2MB, dengan format PDF, JPG, JPEG</strong>
                            </div> 
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>Surat Ket Bertugas di bidang PBJ min. 2 tahun</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="surat_bertugas_pbj" class="custom-file-input surat_bertugas_pbj" id="customFile" lang="idn" lang="idn" value="{{ old('surat_bertugas_pbj') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errSuratBertugasPbj"></span>
                                    <span class="errmsg">{{ $errors->first('surat_bertugas_pbj') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>Bukti SK di bidang PBJ</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="bukti_sk_pbj" class="custom-file-input bukti_sk_pbj" id="customFile" lang="idn" lang="idn" value="{{ old('bukti_sk_pbj') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errbukti_sk_pbj"></span>
                                    <span class="errmsg">{{ $errors->first('bukti_sk_pbj') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>Ijazah terakhir (min. S1/D4)</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="ijazah_terakhir" class="custom-file-input ijazah_terakhir" id="customFile" lang="idn" lang="idn" value="{{ old('ijazah_terakhir') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errijazah_terakhir"></span>
                                    <span class="errmsg">{{ $errors->first('ijazah_terakhir') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>SK Kenaikan Pangkat Terakhir</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="sk_kenaikan_pangkat_terakhir" class="custom-file-input sk_kenaikan_pangkat_terakhir" id="customFile" lang="idn" lang="idn" value="{{ old('sk_kenaikan_pangkat_terakhir') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errsk_kenaikan_pangkat_terakhir"></span>
                                    <span class="errmsg">{{ $errors->first('sk_kenaikan_pangkat_terakhir') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>SK Pengangkatan ke dalam Jabatan Terakhir</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="sk_pengangkatan_jabatan_terakhir" class="custom-file-input sk_pengangkatan_jabatan_terakhir" id="customFile" lang="idn" lang="idn" value="{{ old('sk_pengangkatan_jabatan_terakhir') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errsk_pengangkatan_jabatan_terakhir"></span>
                                    <span class="errmsg">{{ $errors->first('sk_pengangkatan_jabatan_terakhir') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b> Sertifikat PBJ Tk. Dasar </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="sertifikasi_dasar_pbj" class="custom-file-input sertifikasi_dasar_pbj" id="customFile" lang="idn" lang="idn" value="{{ old('sertifikasi_dasar_pbj') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errsertifikasi_dasar_pbj"></span>
                                    <span class="errmsg">{{ $errors->first('sertifikasi_dasar_pbj') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>SKP (2 tahun terakhir)</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="skp_dua_tahun_terakhir" class="custom-file-input skp_dua_tahun_terakhir" id="customFile" lang="idn" lang="idn" value="{{ old('skp_dua_tahun_terakhir') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errskp_dua_tahun_terakhir"></span>
                                    <span class="errmsg">{{ $errors->first('skp_dua_tahun_terakhir') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>Surat Ket Tidak Sedang Dijatuhi Hukuman</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="surat_ket_tidak_dijatuhi_hukuman" class="custom-file-input surat_ket_tidak_dijatuhi_hukuman" id="customFile" lang="idn" lang="idn" value="{{ old('surat_ket_tidak_dijatuhi_hukuman') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errsurat_ket_tidak_dijatuhi_hukuman"></span>
                                    <span class="errmsg">{{ $errors->first('surat_ket_tidak_dijatuhi_hukuman') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>Formulir Kesediaan Mengikuti Penyesuaian/Inpassing </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="formulir_kesediaan_mengikuti_inpassing" class="custom-file-input formulir_kesediaan_mengikuti_inpassing" id="customFile" lang="idn" lang="idn" value="{{ old('formulir_kesediaan_mengikuti_inpassing') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errformulir_kesediaan_mengikuti_inpassing"></span>
                                    <span class="errmsg">{{ $errors->first('formulir_kesediaan_mengikuti_inpassing') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>SK Pengangkatan CPNS  </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="sk_pengangkatan_cpns" class="custom-file-input sk_pengangkatan_cpns" id="customFile" lang="idn" lang="idn" value="{{ old('sk_pengangkatan_cpns') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errsk_pengangkatan_cpns"></span>
                                    <span class="errmsg">{{ $errors->first('sk_pengangkatan_cpns') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>SK Pengangkatan PNS </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="sk_pengangkatan_pns" class="custom-file-input sk_pengangkatan_pns" id="customFile" lang="idn" lang="idn" value="{{ old('sk_pengangkatan_pns') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errsk_pengangkatan_pns"></span>
                                    <span class="errmsg">{{ $errors->first('sk_pengangkatan_pns') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>KTP </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="ktp" class="custom-file-input ktp" id="customFile" lang="idn" lang="idn" value="{{ old('ktp') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errktp"></span>
                                    <span class="errmsg">{{ $errors->first('ktp') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>Pas Foto 3 X 4 </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="pas_foto_3_x_4" class="custom-file-input pas_foto_3_x_4" id="customFile" lang="idn" lang="idn" value="{{ old('pas_foto_3_x_4') }}" accept=".jpg, .jpeg, image/jpg, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errpas_foto_3_x_4"></span>
                                    <span class="errmsg">{{ $errors->first('pas_foto_3_x_4') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>Surat Pernyataan Bersedia diangkat JF PPBJ</b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="surat_pernyataan_bersedia_jfppbj" class="custom-file-input surat_pernyataan_bersedia_jfppbj" id="customFile" lang="idn" lang="idn" value="{{ old('surat_pernyataan_bersedia_jfppbj') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errsurat_pernyataan_bersedia_jfppbj"></span>
                                    <span class="errmsg">{{ $errors->first('surat_pernyataan_bersedia_jfppbj') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>Surat Usulan Mengikuti Penyesuaian/Inpassing </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="file" name="surat_usulan_mengikuti_inpassing" class="custom-file-input surat_usulan_mengikuti_inpassing" id="customFile" lang="idn" lang="idn" value="{{ old('surat_usulan_mengikuti_inpassing') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg">
                                    <label class="custom-file-label" for="customFile">Pilih Berkas</label>
                                    <span class="err" id="errsurat_usulan_mengikuti_inpassing"></span>
                                    <span class="errmsg">{{ $errors->first('surat_usulan_mengikuti_inpassing') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-10 line-txt">
                                    <b>No. Surat Usulan Mengikuti Penyesuaian/Inpassing </b>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1 line-txt">
                                    :
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="text" name="no_surat_usulan_mengikuti_inpassing" class="form-control" id="no_surat_usulan_mengikuti_inpassing" value="{{ old('no_surat_usulan_mengikuti_inpassing') }}" {{-- accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg" --}}>
                                    <span class="err" id="errno_surat_usulan_mengikuti_inpassing"></span>
                                    <span class="errmsg">{{ $errors->first('no_surat_usulan_mengikuti_inpassing') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 btn-area">
							<button class="btn btn-sm btn-default2" id="btnTwoBack" onclick="btnTwoBack()"><i class="fa fa-angle-left"></i>Kembali</button>
							<button class="btn btn-sm btn-default1" type="button" id="btnSubmit" onclick="btnSubmit()">Tambah <i class="fa fa-angle-right"></i></button>
                        </div>
                    </div>
				</div>
            </form>
		</div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $('#btnTwoBack').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-one").addClass("active");
        document.getElementById("page-one").style.display = "block";
        document.getElementById("page-two").style.display = "none";
        
    });

    $('#btnTwoNext').click(function (e) {
        e.preventDefault();
        $("ul li a.active").removeClass("active");
        $("ul li a#li-three").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "none";
    });


    $('#li-one').click(function (e) {
        e.preventDefault();
        $("ul li a.active").removeClass("active");
        $("ul li a#li-one").addClass("active");
        document.getElementById("page-one").style.display = "block";
        document.getElementById("page-two").style.display = "none";
    });

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

	function getval(sel){
        jQuery(document).ready(function(){
            var value = sel.value;
            if(value == 'Penata Muda (III/a)'){
                $('#jenjang').val('Pertama');
            }
        
			if(value == 'Penata Muda Tk.I (III/b)'){
                $('#jenjang').val('Pertama');
            }
            
			if(value == 'Penata (III/c)'){
                $('#jenjang').val('Muda');
            }
            
			if(value == 'Penata Tk.I (III/d)'){
                $('#jenjang').val('Muda');
            }
            
			if(value == 'Pembina (IV/a)'){
                $('#jenjang').val('Madya');
            }
            
			if(value == 'Pembina Tk.I (IV/b)'){
                $('#jenjang').val('Madya');
            }
            
			if(value == 'Pembina Utama Muda (IV/c)'){
                $('#jenjang').val('Madya');
            }
            
			if(value == ''){
                $('#jenjang').val('');
                $('#no_surat').attr('disabled',true);
            }            
			
			$('#no_surat').prop('disabled',false);     
        });
	}
    
	function modifyVar(obj, val) {
        obj.valueOf = obj.toSource = obj.toString = function(){ return val; };
    }

    function setToFalse(boolVar) {
        modifyVar(boolVar, false);
    }

    function setToTrue(boolVar) {
        modifyVar(boolVar, true);
    }

    var validasi_nama = new Boolean(false);
    var validasi_nip = new Boolean(false);
    var validasi_pangkat = new Boolean(false);
    var validasi_tmt_panggol = new Boolean(false);
    var validasi_jenjang = new Boolean(false);
    var validasi_no_surat = new Boolean(false);
    var validasi_tempat_lahir = new Boolean(false);
    var validasi_tanggal_lahir = new Boolean(false);
    var validasi_ktp = new Boolean(false);
    var validasi_sertifikat = new Boolean(false);
    var validasi_pendidikan = new Boolean(false);
    var validasi_email = new Boolean(false);
    var validasi_telp = new Boolean(false);
    var validasi_instansi = new Boolean(false);
    var validasi_satuan = new Boolean(false);
    var validasi_provinsi = new Boolean(false);
    var validasi_kota = new Boolean(false);


    function duplicateEmail(element){
        var email = $(element).val();
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var validate = re.test(email);
        if (validate) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{url('peserta_checkemail')}}',
                data: {email:email},
                dataType: "json",
                success: function(res) {
                    if(res.exists){
                        setToTrue(validasi_email);
                        $('#errEmail').html("")
                    } else {
                        setToFalse(validasi_email);
                        $('#errEmail').html("Email sudah Terdaftar silahkan gunakan email lain.")
                    }
                },
                error: function (jqXHR, exception) {
                    
                }
            });
        } else {
            setToFalse(validasi_email);
            $('#errEmail').html("Alamat email tidak valid.")
        }
    }

    function duplicateNip(element){
        var nip = $(element).val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '{{url('peserta_checknip')}}',
            data: {nip:nip},
            dataType: "json",
            success: function(res) {
                if(res.exists){
                    setToTrue(validasi_nip);
                    $('#errNip').html("")
                } else {
                    setToFalse(validasi_nip);
                    $('#errNip').html("NIP sudah Terdaftar, NIP hanya boleh di gunakan sekali.")
                }
            },
            error: function (jqXHR, exception) {

            }
        });
    }

    function checkTelp(element){
       if ($('#no_telp').val().length >= 13 || $('#no_telp').val().length <=1) {
            $('#errTelp').html("No. Telepon/HP harus berjumlah 2 hingga 12 digit.")
            setToFalse(validasi_telp);
		} else {
            setToTrue(validasi_telp);
            $('#errTelp').html("")
        }
    }

    function duplicateNoSertifikat(element){
        var no_sertifikat = $(element).val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '{{url('peserta_checknosertifikat')}}',
            data: {no_sertifikat:no_sertifikat},
            dataType: "json",
            success: function(res) {
                if(res.exists){
                    setToTrue(validasi_sertifikat);
                    $('#errSertifikat').html("");
                } else {
                    setToFalse(validasi_sertifikat);
                    $('#errSertifikat').html("No Sertifikat sudah Terdaftar, No Sertifikat hanya boleh di gunakan sekali.");
                }
            },
            error: function (jqXHR, exception) {

            }
        });
    }

	$('#btnOne').click(function (e) {
        e.preventDefault();

        if ($('#nama').val().length == 0) {
            $('#errNama').html("Nama Tidak Boleh Kosong");
            setToFalse(validasi_nama); 
        } else {
            $('#errNama').html("");
            setToTrue(validasi_nama);
        }

        if ($('#nip').val().length <= 17) {
            $('#errNip').html("NIP harus 18 angka")
            setToFalse(validasi_nip);
        } else {
             $('#errNip').html("")
             setToTrue(validasi_nip); 
         }

        if ($('#pangkat').val().length == 0) {
            $('#errPangkat').html("Pangkat/gol Tidak Boleh Kosong")
            setToFalse(validasi_pangkat);
        } else {
            $('#errPangkat').html("")
            setToTrue(validasi_pangkat); 
        }

        if ($('.tmt_panggol').val().length == 0) {
            $('#errTmtPanggol').html("TMT Pangkat/gol Tidak Boleh Kosong")
            setToFalse(validasi_tmt_panggol);
        } else {
            $('#errTmtPanggol').html("")
            setToTrue(validasi_tmt_panggol); 
        }

        if ($('#jenjang').val().length == 0) {
            $('#errJenjang').html("Usulan Jenjang Tidak Boleh Kosong")
            setToFalse(validasi_jenjang);
        } else {
            $('#errJenjang').html("")
            setToTrue(validasi_jenjang); 
        }

        if ($('#no_surat').val().length == 0) {
            $('#errno_surat').html("No Surat Tidak Boleh Kosong")
            setToFalse(validasi_no_surat);
        } else {
            $('#errno_surat').html("")
            setToTrue(validasi_no_surat); 
        }

        if ($('#tempat_lahir').val().length == 0) {
            $('#errTempatLahir').html("Tempat Lahir Tidak Boleh Kosong")
            setToFalse(validasi_tempat_lahir);
        } else {
            $('#errTempatLahir').html("")
            setToTrue(validasi_tempat_lahir); 
        }

        if ($('.tanggal_lahir').val().length == 0) {
            $('#errTanggalLahir').html("Tanggal Lahir Tidak Boleh Kosong")
            setToFalse(validasi_tanggal_lahir);
        } else {
            $('#errTanggalLahir').html("")
            setToTrue(validasi_tanggal_lahir); 
        }
       
        if ($('#no_ktp').val().length <= 15) {
            $('#errKtp').html("No. KTP harus 16 angka")
            setToFalse(validasi_ktp);
        } else {
            $('#errKtp').html("")
            setToTrue(validasi_ktp); 
        }

        if ($('#no_sertifikat').val().length <= 14) {
            $('#errSertifikat').html("No. Sertifikat harus 15 angka")
            setToFalse(validasi_sertifikat);
        } else {
            $('#errSertifikat').html("")
            setToTrue(validasi_sertifikat); 
        }
        
        if ($('.pendidikan_terakhir').val().length == 0) {
            $('#errPendidikan').html("Pendidikan Terakhir Tidak Boleh Kosong")
            setToFalse(validasi_pendidikan);
        } else {
            $('#errPendidikan').html("")
            setToTrue(validasi_pendidikan); 
        }
        
        if ($('#email').val().length == 0) {
            $('#errEmail').html("Email Tidak Boleh Kosong")
            setToFalse(validasi_email);
        } else {
             $('#errEmail').html("")
             setToTrue(validasi_email); 
         }

        if ($('#no_telp').val().length == 0) {
            $('#errTelp').html("No Telp Tidak Boleh Kosong")
            setToFalse(validasi_telp);
        } else {
             $('#errTelp').html("")
             setToTrue(validasi_telp); 
         }
         
        if ($('#nama_instansi').val().length == 0) {
            $('#errInstansi').html("Nama Instansi Tidak Boleh Kosong")
            setToFalse(validasi_instansi);
        } else {
            $('#errInstansi').html("")
            setToTrue(validasi_instansi); 
        }

        if ($('#satuan_kerja').val().length == 0) {
            $('#errSatuanKerja').html("Satuan Kerja Tidak Boleh Kosong")
            setToFalse(validasi_satuan);
        } else {
            $('#errSatuanKerja').html("")
            setToTrue(validasi_satuan); 
        }

        if ($('#provinsi').val().length == 0) {
            $('#errProvinsi').html("Provinsi Tidak Boleh Kosong")
            setToFalse(validasi_provinsi);
        } else {
            $('#errProvinsi').html("")
            setToTrue(validasi_provinsi); 
        }

        if ($('#kab_kota').val().length == 0) {
            $('#errKabKota').html("Kab/Kota Tidak Boleh Kosong")
            setToFalse(validasi_kota);
        } else {
            $('#errKabKota').html("")
            setToTrue(validasi_kota); 
        }

        if(validasi_nama == true && validasi_nip == true && validasi_pangkat == true && validasi_tmt_panggol == true && validasi_jenjang == true && validasi_tempat_lahir == true && validasi_tanggal_lahir == true && validasi_ktp == true && validasi_sertifikat == true && validasi_pendidikan == true && validasi_email == true && validasi_telp == true && validasi_satuan == true && validasi_provinsi == true && validasi_kota == true && validasi_no_surat == true){
			$("ul li a.active").removeClass("active");
			$("ul li a#li-two").addClass("active");
			document.getElementById("page-one").style.display = "none";
			document.getElementById("page-two").style.display = "block";
        }       
    });

    function modifyVar(obj, val) {
        obj.valueOf = obj.toSource = obj.toString = function(){ return val; };
    }

    function setToFalse(boolVar) {
        modifyVar(boolVar, false);
    }

    function setToTrue(boolVar) {
        modifyVar(boolVar, true);
    }
	
    var sertifikasi_dasar_pbj = new Boolean(true);
    var surat_usulan_mengikuti_inpassing = new Boolean(true);
    var surat_pernyataan_bersedia_jfppbj = new Boolean(true);
    var pas_foto_3_x_4 = new Boolean(true);
    var ktp = new Boolean(true);
    var sk_pengangkatan_pns = new Boolean(true);
    var sk_pengangkatan_cpns = new Boolean(true);
    var formulir_kesediaan_mengikuti_inpassing = new Boolean(true);
    var surat_ket_tidak_dijatuhi_hukuman = new Boolean(true);
    var skp_dua_tahun_terakhir = new Boolean(true);
    var sk_pengangkatan_jabatan_terakhir = new Boolean(true);
    var sk_kenaikan_pangkat_terakhir = new Boolean(true);
    var ijazah_terakhir = new Boolean(true);
    var bukti_sk_pbj = new Boolean(true);
    var surat_bertugas_pbj = new Boolean(true);
    var no_surat_usulan_mengikuti_inpassing = new Boolean(true);

    $('.surat_bertugas_pbj').bind('change', function() {
        var size_surat_bertugas_pbj = this.files[0].size;
        var type_surat_bertugas_pbj = this.files[0].type;

        if (type_surat_bertugas_pbj == 'application/pdf') {
            if(size_surat_bertugas_pbj > 2096103){
                $('#errSuratBertugasPbj').html("file tidak boleh lebih dari 2MB");
                setToFalse(surat_bertugas_pbj);
            }

            if(size_surat_bertugas_pbj < 102796){
                $('#errSuratBertugasPbj').html("file tidak boleh kurang dari 100kb");
                setToFalse(surat_bertugas_pbj);
            }

            if (102796 < size_surat_bertugas_pbj && size_surat_bertugas_pbj < 2096103) {
                $('#errSuratBertugasPbj').html("");
                setToTrue(surat_bertugas_pbj);
            }
        } else if (type_surat_bertugas_pbj == 'image/jpeg') {
            if(size_surat_bertugas_pbj > 2096103){
                $('#errSuratBertugasPbj').html("file tidak boleh lebih dari 2MB");
                setToFalse(surat_bertugas_pbj);
            }

            if(size_surat_bertugas_pbj < 102796){
                $('#errSuratBertugasPbj').html("file tidak boleh kurang dari 100kb");
                setToFalse(surat_bertugas_pbj);
            }

            if (102796 < size_surat_bertugas_pbj && size_surat_bertugas_pbj < 2096103) {
                $('#errSuratBertugasPbj').html("");
                setToTrue(surat_bertugas_pbj);
            }
        } else {
            $('#errSuratBertugasPbj').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(surat_bertugas_pbj);
        }
    });

    $('.bukti_sk_pbj').bind('change', function() {
        var size_bukti_sk_pbj = this.files[0].size;
        var type_bukti_sk_pbj = this.files[0].type;
        if (type_bukti_sk_pbj == 'application/pdf') {
            if(size_bukti_sk_pbj > 2096103){
                $('#errbukti_sk_pbj').html("file tidak boleh lebih dari 2MB");
                setToFalse(bukti_sk_pbj);
            }

            if(size_bukti_sk_pbj < 102796){
                $('#errbukti_sk_pbj').html("file tidak boleh kurang dari 100kb");
                setToFalse(bukti_sk_pbj);
            }

            if (102796 < size_bukti_sk_pbj && size_bukti_sk_pbj < 2096103) {
                $('#errbukti_sk_pbj').html("");
                setToTrue(bukti_sk_pbj);
            }
        } else if (type_bukti_sk_pbj == 'image/jpeg') {
            if(size_bukti_sk_pbj > 2096103){
                $('#errbukti_sk_pbj').html("file tidak boleh lebih dari 2MB");
                setToFalse(bukti_sk_pbj);
            }

            if(size_bukti_sk_pbj < 102796){
                $('#errbukti_sk_pbj').html("file tidak boleh kurang dari 100kb");
                setToFalse(bukti_sk_pbj);
            }

            if (102796 < size_bukti_sk_pbj && size_bukti_sk_pbj < 2096103) {
                $('#errbukti_sk_pbj').html("");
                setToTrue(bukti_sk_pbj);
            }
        } else {
            $('#errbukti_sk_pbj').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(bukti_sk_pbj);
        }
    });

    $('.ijazah_terakhir').bind('change', function() {
        var size_ijazah_terakhir = this.files[0].size;
        var type_ijazah_terakhir = this.files[0].type;

        if (type_ijazah_terakhir == 'application/pdf') {
            if(size_ijazah_terakhir > 2096103){
                $('#errijazah_terakhir').html("file tidak boleh lebih dari 2MB");
                setToFalse(surat_ijazah_terakhir);
            }

            if(size_ijazah_terakhir < 102796){
                $('#errijazah_terakhir').html("file tidak boleh kurang dari 100kb");
                setToFalse(ijazah_terakhir);
            }

            if (102796 < size_ijazah_terakhir && size_ijazah_terakhir < 2096103) {
                $('#errijazah_terakhir').html("");
                setToTrue(ijazah_terakhir);
            }
        } else if (type_ijazah_terakhir == 'image/jpeg') {
            if(size_ijazah_terakhir > 2096103){
                $('#errijazah_terakhir').html("file tidak boleh lebih dari 2MB");
                setToFalse(surat_ijazah_terakhir);
            }

            if(size_ijazah_terakhir < 102796){
                $('#errijazah_terakhir').html("file tidak boleh kurang dari 100kb");
                setToFalse(ijazah_terakhir);
            }

            if (102796 < size_ijazah_terakhir && size_ijazah_terakhir < 2096103) {
                $('#errijazah_terakhir').html("");
                setToTrue(ijazah_terakhir);
            }
        } else {
            $('#errijazah_terakhir').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(ijazah_terakhir);
        }
    });

    $('.sk_kenaikan_pangkat_terakhir').bind('change', function() {
        var size_sk_kenaikan_pangkat_terakhir = this.files[0].size;
        var type_sk_kenaikan_pangkat_terakhir = this.files[0].type;
        if (type_sk_kenaikan_pangkat_terakhir == 'application/pdf') {
            if(size_sk_kenaikan_pangkat_terakhir > 2096103){
                $('#errsk_kenaikan_pangkat_terakhir').html("file tidak boleh lebih dari 2MB");
                setToFalse(sk_kenaikan_pangkat_terakhir);
            }

            if(size_sk_kenaikan_pangkat_terakhir < 102796){
                $('#errsk_kenaikan_pangkat_terakhir').html("file tidak boleh kurang dari 100kb");
                setToFalse(sk_kenaikan_pangkat_terakhir);
            }

            if (102796 < size_sk_kenaikan_pangkat_terakhir && size_sk_kenaikan_pangkat_terakhir < 2096103) {
                $('#errsk_kenaikan_pangkat_terakhir').html("");
                setToTrue(sk_kenaikan_pangkat_terakhir);
            }
        } else if (type_sk_kenaikan_pangkat_terakhir == 'image/jpeg') {
            if(size_sk_kenaikan_pangkat_terakhir > 2096103){
                $('#errsk_kenaikan_pangkat_terakhir').html("file tidak boleh lebih dari 2MB");
                setToFalse(sk_kenaikan_pangkat_terakhir);
            }

            if(size_sk_kenaikan_pangkat_terakhir < 102796){
                $('#errsk_kenaikan_pangkat_terakhir').html("file tidak boleh kurang dari 100kb");
                setToFalse(sk_kenaikan_pangkat_terakhir);
            }

            if (102796 < size_sk_kenaikan_pangkat_terakhir && size_sk_kenaikan_pangkat_terakhir < 2096103) {
                $('#errsk_kenaikan_pangkat_terakhir').html("");
                setToTrue(sk_kenaikan_pangkat_terakhir);
            }
        } else {
            $('#errsk_kenaikan_pangkat_terakhir').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(sk_kenaikan_pangkat_terakhir);
        }
    });

    $('.sk_pengangkatan_jabatan_terakhir').bind('change', function() {
        var size_sk_pengangkatan_jabatan_terakhir = this.files[0].size;
        var type_sk_pengangkatan_jabatan_terakhir = this.files[0].type;
        if (type_sk_pengangkatan_jabatan_terakhir == 'application/pdf') {
            if(size_sk_pengangkatan_jabatan_terakhir > 2096103){
                $('#errsk_pengangkatan_jabatan_terakhir').html("file tidak boleh lebih dari 2MB");
                setToFalse(sk_pengangkatan_jabatan_terakhir);
            }

            if(size_sk_pengangkatan_jabatan_terakhir < 102796){
                $('#errsk_pengangkatan_jabatan_terakhir').html("file tidak boleh kurang dari 100kb");
                setToFalse(sk_pengangkatan_jabatan_terakhir);
            }

            if (102796 < size_sk_pengangkatan_jabatan_terakhir && size_sk_pengangkatan_jabatan_terakhir < 2096103) {
                $('#errsk_pengangkatan_jabatan_terakhir').html("");
                setToTrue(sk_pengangkatan_jabatan_terakhir);
            }
        } else if (type_sk_pengangkatan_jabatan_terakhir == 'image/jpeg') {
            if(size_sk_pengangkatan_jabatan_terakhir > 2096103){
                $('#errsk_pengangkatan_jabatan_terakhir').html("file tidak boleh lebih dari 2MB");
                setToFalse(sk_pengangkatan_jabatan_terakhir);
            }

            if(size_sk_pengangkatan_jabatan_terakhir < 102796){
                $('#errsk_pengangkatan_jabatan_terakhir').html("file tidak boleh kurang dari 100kb");
                setToFalse(sk_pengangkatan_jabatan_terakhir);
            }

            if (102796 < size_sk_pengangkatan_jabatan_terakhir && size_sk_pengangkatan_jabatan_terakhir < 2096103) {
                $('#errsk_pengangkatan_jabatan_terakhir').html("");
                setToTrue(sk_pengangkatan_jabatan_terakhir);
            }
        } else {
            $('#errsk_pengangkatan_jabatan_terakhir').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(sk_pengangkatan_jabatan_terakhir);
        }
    });

    $('.skp_dua_tahun_terakhir').bind('change', function() {
        var size_skp_dua_tahun_terakhir = this.files[0].size;
        var type_skp_dua_tahun_terakhir = this.files[0].type;
        if (type_skp_dua_tahun_terakhir == 'application/pdf') {
            if(size_skp_dua_tahun_terakhir > 2096103){
                $('#errskp_dua_tahun_terakhir').html("file tidak boleh lebih dari 2MB");
                setToFalse(skp_dua_tahun_terakhir);
            }

            if(size_skp_dua_tahun_terakhir < 102796){
                $('#errskp_dua_tahun_terakhir').html("file tidak boleh kurang dari 100kb");
                setToFalse(skp_dua_tahun_terakhir);
            }

            if (102796 < size_skp_dua_tahun_terakhir && size_skp_dua_tahun_terakhir < 2096103) {
                $('#errskp_dua_tahun_terakhir').html("");
                setToTrue(skp_dua_tahun_terakhir);
            }
        } else if (type_skp_dua_tahun_terakhir == 'image/jpeg') {
            if(size_skp_dua_tahun_terakhir > 2096103){
                $('#errskp_dua_tahun_terakhir').html("file tidak boleh lebih dari 2MB");
                setToFalse(skp_dua_tahun_terakhir);
            }

            if(size_skp_dua_tahun_terakhir < 102796){
                $('#errskp_dua_tahun_terakhir').html("file tidak boleh kurang dari 100kb");
                setToFalse(skp_dua_tahun_terakhir);
            }

            if (102796 < size_skp_dua_tahun_terakhir && size_skp_dua_tahun_terakhir < 2096103) {
                $('#errskp_dua_tahun_terakhir').html("");
                setToTrue(skp_dua_tahun_terakhir);
            }
        } else {
            $('#errskp_dua_tahun_terakhir').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(skp_dua_tahun_terakhir);
        }
    });

    $('.surat_ket_tidak_dijatuhi_hukuman').bind('change', function() {
        var size_surat_ket_tidak_dijatuhi_hukuman = this.files[0].size;
        var type_surat_ket_tidak_dijatuhi_hukuman = this.files[0].type;
        if (type_surat_ket_tidak_dijatuhi_hukuman == 'application/pdf') {
            if(size_surat_ket_tidak_dijatuhi_hukuman > 2096103){
                $('#errsurat_ket_tidak_dijatuhi_hukuman').html("file tidak boleh lebih dari 2MB");
                setToFalse(surat_ket_tidak_dijatuhi_hukuman);
            }

            if(size_surat_ket_tidak_dijatuhi_hukuman < 102796){
                $('#errsurat_ket_tidak_dijatuhi_hukuman').html("file tidak boleh kurang dari 100kb");
                setToFalse(surat_ket_tidak_dijatuhi_hukuman);
            }

            if (102796 < size_surat_ket_tidak_dijatuhi_hukuman && size_surat_ket_tidak_dijatuhi_hukuman < 2096103) {
                $('#errsurat_ket_tidak_dijatuhi_hukuman').html("");
                setToTrue(surat_ket_tidak_dijatuhi_hukuman);
            }
        } else if (type_surat_ket_tidak_dijatuhi_hukuman == 'image/jpeg') {
            if(size_surat_ket_tidak_dijatuhi_hukuman > 2096103){
                $('#errsurat_ket_tidak_dijatuhi_hukuman').html("file tidak boleh lebih dari 2MB");
                setToFalse(surat_ket_tidak_dijatuhi_hukuman);
            }

            if(size_surat_ket_tidak_dijatuhi_hukuman < 102796){
                $('#errsurat_ket_tidak_dijatuhi_hukuman').html("file tidak boleh kurang dari 100kb");
                setToFalse(surat_ket_tidak_dijatuhi_hukuman);
            }

            if (102796 < size_surat_ket_tidak_dijatuhi_hukuman && size_surat_ket_tidak_dijatuhi_hukuman < 2096103) {
                $('#errsurat_ket_tidak_dijatuhi_hukuman').html("");
                setToTrue(surat_ket_tidak_dijatuhi_hukuman);
            }
        } else {
            $('#errsurat_ket_tidak_dijatuhi_hukuman').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(surat_ket_tidak_dijatuhi_hukuman);
        }
    });

    $('.formulir_kesediaan_mengikuti_inpassing').bind('change', function() {
        var size_formulir_kesediaan_mengikuti_inpassing = this.files[0].size;
        var type_formulir_kesediaan_mengikuti_inpassing = this.files[0].type;
        if (type_formulir_kesediaan_mengikuti_inpassing == 'application/pdf') {
            if(size_formulir_kesediaan_mengikuti_inpassing > 2096103){
                $('#errformulir_kesediaan_mengikuti_inpassing').html("file tidak boleh lebih dari 2MB");
                setToFalse(formulir_kesediaan_mengikuti_inpassing);
            }

            if(size_formulir_kesediaan_mengikuti_inpassing < 102796){
                $('#errformulir_kesediaan_mengikuti_inpassing').html("file tidak boleh kurang dari 100kb");
                setToFalse(formulir_kesediaan_mengikuti_inpassing);
            }

            if (102796 < size_formulir_kesediaan_mengikuti_inpassing && size_formulir_kesediaan_mengikuti_inpassing < 2096103) {
                $('#errformulir_kesediaan_mengikuti_inpassing').html("");
                setToTrue(formulir_kesediaan_mengikuti_inpassing);
            }
        } else if (type_formulir_kesediaan_mengikuti_inpassing == 'image/jpeg') {
            if(size_formulir_kesediaan_mengikuti_inpassing > 2096103){
                $('#errformulir_kesediaan_mengikuti_inpassing').html("file tidak boleh lebih dari 2MB");
                setToFalse(formulir_kesediaan_mengikuti_inpassing);
            }

            if(size_formulir_kesediaan_mengikuti_inpassing < 102796){
                $('#errformulir_kesediaan_mengikuti_inpassing').html("file tidak boleh kurang dari 100kb");
                setToFalse(formulir_kesediaan_mengikuti_inpassing);
            }

            if (102796 < size_formulir_kesediaan_mengikuti_inpassing && size_formulir_kesediaan_mengikuti_inpassing < 2096103) {
                $('#errformulir_kesediaan_mengikuti_inpassing').html("");
                setToTrue(formulir_kesediaan_mengikuti_inpassing);
            }
        } else {
            $('#errformulir_kesediaan_mengikuti_inpassing').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(formulir_kesediaan_mengikuti_inpassing);
        }
    });

    $('.sk_pengangkatan_cpns').bind('change', function() {
        var size_sk_pengangkatan_cpns = this.files[0].size;
        var type_sk_pengangkatan_cpns = this.files[0].type;
        if (type_sk_pengangkatan_cpns == 'application/pdf') {
            if(size_sk_pengangkatan_cpns > 2096103){
                $('#errsk_pengangkatan_cpns').html("file tidak boleh lebih dari 2MB");
                setToFalse(sk_pengangkatan_cpns);
            }

            if(size_sk_pengangkatan_cpns < 102796){
                $('#errsk_pengangkatan_cpns').html("file tidak boleh kurang dari 100kb");
                setToFalse(sk_pengangkatan_cpns);
            }

            if (102796 < size_sk_pengangkatan_cpns && size_sk_pengangkatan_cpns < 2096103) {
                $('#errsk_pengangkatan_cpns').html("");
                setToTrue(sk_pengangkatan_cpns);
            }
        } else if (type_sk_pengangkatan_cpns == 'image/jpeg') {
            if(size_sk_pengangkatan_cpns > 2096103){
                $('#errsk_pengangkatan_cpns').html("file tidak boleh lebih dari 2MB");
                setToFalse(sk_pengangkatan_cpns);
            }

            if(size_sk_pengangkatan_cpns < 102796){
                $('#errsk_pengangkatan_cpns').html("file tidak boleh kurang dari 100kb");
                setToFalse(sk_pengangkatan_cpns);
            }

            if (102796 < size_sk_pengangkatan_cpns && size_sk_pengangkatan_cpns < 2096103) {
                $('#errsk_pengangkatan_cpns').html("");
                setToTrue(sk_pengangkatan_cpns);
            }
        } else {
            $('#errsk_pengangkatan_cpns').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(sk_pengangkatan_cpns);
        }
    });

    $('.sk_pengangkatan_pns').bind('change', function() {
        var size_sk_pengangkatan_pns = this.files[0].size;
        var type_sk_pengangkatan_pns = this.files[0].type;
        if (type_sk_pengangkatan_pns == 'application/pdf') {
            if(size_sk_pengangkatan_pns > 2096103){
                $('#errsk_pengangkatan_pns').html("file tidak boleh lebih dari 2MB");
                setToFalse(sk_pengangkatan_pns);
            }

            if(size_sk_pengangkatan_pns < 102796){
                $('#errsk_pengangkatan_pns').html("file tidak boleh kurang dari 100kb");
                setToFalse(sk_pengangkatan_pns);
            }

            if (102796 < size_sk_pengangkatan_pns && size_sk_pengangkatan_pns < 2096103) {
                $('#errsk_pengangkatan_pns').html("");
                setToTrue(sk_pengangkatan_pns);
            }
        } else if (type_sk_pengangkatan_pns == 'image/jpeg') {
            if(size_sk_pengangkatan_pns > 2096103){
                $('#errsk_pengangkatan_pns').html("file tidak boleh lebih dari 2MB");
                setToFalse(sk_pengangkatan_pns);
            }

            if(size_sk_pengangkatan_pns < 102796){
                $('#errsk_pengangkatan_pns').html("file tidak boleh kurang dari 100kb");
                setToFalse(sk_pengangkatan_pns);
            }

            if (102796 < size_sk_pengangkatan_pns && size_sk_pengangkatan_pns < 2096103) {
                $('#errsk_pengangkatan_pns').html("");
                setToTrue(sk_pengangkatan_pns);
            }
        } else {
            $('#errsk_pengangkatan_pns').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(sk_pengangkatan_pns);
        }
    });

    $('.ktp').bind('change', function() {
        var size_ktp = this.files[0].size;
        var type_ktp = this.files[0].type;

        if (type_ktp == 'application/pdf') {
            if(size_ktp > 2096103){
                $('#errktp').html("file tidak boleh lebih dari 2MB");
                setToFalse(ktp);
            }

            if(size_ktp < 102796){
                $('#errktp').html("file tidak boleh kurang dari 100kb");
                setToFalse(ktp);
            }

            if (102796 < size_ktp && size_ktp < 2096103) {
                $('#errktp').html("");
                setToTrue(ktp);
            }
        } else if (type_ktp == 'image/jpeg') {
            if(size_ktp > 2096103){
                $('#errktp').html("file tidak boleh lebih dari 2MB");
                setToFalse(ktp);
            }

            if(size_ktp < 102796){
                $('#errktp').html("file tidak boleh kurang dari 100kb");
                setToFalse(ktp);
            }

            if (102796 < size_ktp && size_ktp < 2096103) {
                $('#errktp').html("");
                setToTrue(ktp);
            }
        } else {
            $('#errktp').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(ktp);
        }
    });

    
    $('.pas_foto_3_x_4').bind('change', function() {
        var size_pas_foto_3_x_4 = this.files[0].size;
        var type_pas_foto_3_x_4 = this.files[0].type;

        if (type_pas_foto_3_x_4 == 'image/jpeg') {
            if(size_pas_foto_3_x_4 > 2096103){
                $('#errpas_foto_3_x_4').html("file tidak boleh lebih dari 2MB");
                setToFalse(pas_foto_3_x_4);
            }

            if(size_pas_foto_3_x_4 < 102796){
                $('#errpas_foto_3_x_4').html("file tidak boleh kurang dari 100kb");
                setToFalse(pas_foto_3_x_4);
            }

            if (102796 < size_pas_foto_3_x_4 && size_pas_foto_3_x_4 < 2096103) {
                $('#errpas_foto_3_x_4').html("");
                setToTrue(pas_foto_3_x_4);
            }
        } else {
            $('#errpas_foto_3_x_4').html("type file tidak boleh selain jpg & jpeg");
            setToFalse(pas_foto_3_x_4);
        }
    });

    $('.surat_pernyataan_bersedia_jfppbj').bind('change', function() {
        var size_surat_pernyataan_bersedia_jfppbj = this.files[0].size;
        var type_surat_pernyataan_bersedia_jfppbj = this.files[0].type;

        if (type_surat_pernyataan_bersedia_jfppbj == 'application/pdf') {
            if(size_surat_pernyataan_bersedia_jfppbj > 2096103){
                $('#errsurat_pernyataan_bersedia_jfppbj').html("file tidak boleh lebih dari 2MB");
                setToFalse(surat_pernyataan_bersedia_jfppbj);
            }

            if(size_surat_pernyataan_bersedia_jfppbj < 102796){
                $('#errsurat_pernyataan_bersedia_jfppbj').html("file tidak boleh kurang dari 100kb");
                setToFalse(surat_pernyataan_bersedia_jfppbj);
            }

            if (102796 < size_surat_pernyataan_bersedia_jfppbj && size_surat_pernyataan_bersedia_jfppbj < 2096103) {
                $('#errsurat_pernyataan_bersedia_jfppbj').html("");
                setToTrue(surat_pernyataan_bersedia_jfppbj);
            }
        } else if (type_surat_pernyataan_bersedia_jfppbj == 'image/jpeg') {
            if(size_surat_pernyataan_bersedia_jfppbj > 2096103){
                $('#errsurat_pernyataan_bersedia_jfppbj').html("file tidak boleh lebih dari 2MB");
                setToFalse(surat_pernyataan_bersedia_jfppbj);
            }

            if(size_surat_pernyataan_bersedia_jfppbj < 102796){
                $('#errsurat_pernyataan_bersedia_jfppbj').html("file tidak boleh kurang dari 100kb");
                setToFalse(surat_pernyataan_bersedia_jfppbj);
            }

            if (102796 < size_surat_pernyataan_bersedia_jfppbj && size_surat_pernyataan_bersedia_jfppbj < 2096103) {
                $('#errsurat_pernyataan_bersedia_jfppbj').html("");
                setToTrue(surat_pernyataan_bersedia_jfppbj);
            }
        } else {
            $('#errsurat_pernyataan_bersedia_jfppbj').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(surat_pernyataan_bersedia_jfppbj);
        }
    });

    $('.surat_usulan_mengikuti_inpassing').bind('change', function() {
        var size_surat_usulan_mengikuti_inpassing = this.files[0].size;
        var type_surat_usulan_mengikuti_inpassing = this.files[0].type;
        if (type_surat_usulan_mengikuti_inpassing == 'application/pdf') {
            if(size_surat_usulan_mengikuti_inpassing > 2096103){
                $('#errsurat_usulan_mengikuti_inpassing').html("file tidak boleh lebih dari 2MB");
                setToFalse(surat_usulan_mengikuti_inpassing);
            }

            if(size_surat_usulan_mengikuti_inpassing < 102796){
                $('#errsurat_usulan_mengikuti_inpassing').html("file tidak boleh kurang dari 100kb");
                setToFalse(surat_usulan_mengikuti_inpassing);
            }

            if (102796 < size_surat_usulan_mengikuti_inpassing && size_surat_usulan_mengikuti_inpassing < 2096103) {
                $('#errsurat_usulan_mengikuti_inpassing').html("");
                setToTrue(surat_usulan_mengikuti_inpassing);
            }
        } else if (type_surat_usulan_mengikuti_inpassing == 'image/jpeg') {
            if(size_surat_usulan_mengikuti_inpassing > 2096103){
                $('#errsurat_usulan_mengikuti_inpassing').html("file tidak boleh lebih dari 2MB");
                setToFalse(surat_usulan_mengikuti_inpassing);
            }

            if(size_surat_usulan_mengikuti_inpassing < 102796){
                $('#errsurat_usulan_mengikuti_inpassing').html("file tidak boleh kurang dari 100kb");
                setToFalse(surat_usulan_mengikuti_inpassing);
            }

            if (102796 < size_surat_usulan_mengikuti_inpassing && size_surat_usulan_mengikuti_inpassing < 2096103) {
                $('#errsurat_usulan_mengikuti_inpassing').html("");
                setToTrue(surat_usulan_mengikuti_inpassing);
            }
        } else {
            $('#errsurat_usulan_mengikuti_inpassing').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(surat_usulan_mengikuti_inpassing);
        }
    });

    $('.sertifikasi_dasar_pbj').bind('change', function() {
        var size_sertifikasi_dasar_pbj = this.files[0].size;
        var type_sertifikasi_dasar_pbj = this.files[0].type;

        if (type_sertifikasi_dasar_pbj == 'application/pdf') {
            if(size_sertifikasi_dasar_pbj > 2096103){
                $('#errsertifikasi_dasar_pbj').html("file tidak boleh lebih dari 2MB");
                setToFalse(sertifikasi_dasar_pbj);
            }

            if(size_sertifikasi_dasar_pbj < 102796){
                $('#errsertifikasi_dasar_pbj').html("file tidak boleh kurang dari 100kb");
                setToFalse(sertifikasi_dasar_pbj);
            }

            if (102796 < size_sertifikasi_dasar_pbj && size_sertifikasi_dasar_pbj < 2096103) {
                $('#errsertifikasi_dasar_pbj').html("");
                setToTrue(sertifikasi_dasar_pbj);
            }
        } else if (type_sertifikasi_dasar_pbj == 'image/jpeg') {
            if(size_sertifikasi_dasar_pbj > 2096103){
                $('#errsertifikasi_dasar_pbj').html("file tidak boleh lebih dari 2MB");
                setToFalse(sertifikasi_dasar_pbj);
            }

            if(size_sertifikasi_dasar_pbj < 102796){
                $('#errsertifikasi_dasar_pbj').html("file tidak boleh kurang dari 100kb");
                setToFalse(sertifikasi_dasar_pbj);
            }

            if (102796 < size_sertifikasi_dasar_pbj && size_sertifikasi_dasar_pbj < 2096103) {
                $('#errsertifikasi_dasar_pbj').html("");
                setToTrue(sertifikasi_dasar_pbj);
            }
        } else {
            $('#errsertifikasi_dasar_pbj').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(sertifikasi_dasar_pbj);
        }
    });

    $('#btnSubmit').click(function (e) {
        if ($('.surat_bertugas_pbj').val().length == 0){
            $('#errSuratBertugasPbj').html("berkas tidak boleh kosong");
            setToFalse(surat_bertugas_pbj);
        }
		
        if ($('.bukti_sk_pbj').val().length == 0){
            $('#errbukti_sk_pbj').html("berkas tidak boleh kosong");
            setToFalse(bukti_sk_pbj);
        }
		
        if ($('.ijazah_terakhir').val().length == 0){
            $('#errijazah_terakhir').html("berkas tidak boleh kosong");
            setToFalse(ijazah_terakhir);
        }
		
        if ($('.sk_kenaikan_pangkat_terakhir').val().length == 0){
            $('#errsk_kenaikan_pangkat_terakhir').html("berkas tidak boleh kosong");
            setToFalse(sk_kenaikan_pangkat_terakhir);
        }
		
        if ($('.sk_pengangkatan_jabatan_terakhir').val().length == 0){
            $('#errsk_pengangkatan_jabatan_terakhir').html("berkas tidak boleh kosong");
            setToFalse(sk_pengangkatan_jabatan_terakhir);
        }
		
        if ($('.skp_dua_tahun_terakhir').val().length == 0){
            $('#errskp_dua_tahun_terakhir').html("berkas tidak boleh kosong");
            setToFalse(skp_dua_tahun_terakhir);
        }
		
        if ($('.surat_ket_tidak_dijatuhi_hukuman').val().length == 0){
            $('#errsurat_ket_tidak_dijatuhi_hukuman').html("berkas tidak boleh kosong");
            setToFalse(surat_ket_tidak_dijatuhi_hukuman);
        }
		
        if ($('.formulir_kesediaan_mengikuti_inpassing').val().length == 0){
            $('#errformulir_kesediaan_mengikuti_inpassing').html("berkas tidak boleh kosong");
            setToFalse(formulir_kesediaan_mengikuti_inpassing);
        }
		
        if ($('.sk_pengangkatan_cpns').val().length == 0){
            $('#errsk_pengangkatan_cpns').html("berkas tidak boleh kosong");
            setToFalse(sk_pengangkatan_cpns);
        }
		
        if ($('.sk_pengangkatan_pns').val().length == 0){
            $('#errsk_pengangkatan_pns').html("berkas tidak boleh kosong");
            setToFalse(sk_pengangkatan_pns);
        }
		
        if ($('.ktp').val().length == 0){
            $('#errktp').html("berkas tidak boleh kosong");
            setToFalse(ktp);
        }
		
        if ($('.pas_foto_3_x_4').val().length == 0){
            $('#errpas_foto_3_x_4').html("berkas tidak boleh kosong");
            setToFalse(pas_foto_3_x_4);
        }
		
        if ($('.surat_pernyataan_bersedia_jfppbj').val().length == 0){
            $('#errsurat_pernyataan_bersedia_jfppbj').html("berkas tidak boleh kosong");
            setToFalse(surat_pernyataan_bersedia_jfppbj);
        }
		
        if ($('.surat_usulan_mengikuti_inpassing').val().length == 0){
            $('#errsurat_usulan_mengikuti_inpassing').html("berkas tidak boleh kosong");
            setToFalse(surat_usulan_mengikuti_inpassing);
        }
		
        if ($('#no_surat_usulan_mengikuti_inpassing').val().length == 0) {
            $('#errno_surat_usulan_mengikuti_inpassing').html("No Surat Usulan Mengikuti Inpassing Tidak Boleh Kosong")
            setToFalse(no_surat_usulan_mengikuti_inpassing);
        } else {
			$('#errno_surat_usulan_mengikuti_inpassing').html("")
            setToTrue(no_surat_usulan_mengikuti_inpassing);
        }

        e.preventDefault();
        if(sertifikasi_dasar_pbj == true && surat_usulan_mengikuti_inpassing == true && surat_pernyataan_bersedia_jfppbj == true && pas_foto_3_x_4 == true && ktp == true && sk_pengangkatan_pns == true && sk_pengangkatan_cpns == true && formulir_kesediaan_mengikuti_inpassing == true && surat_ket_tidak_dijatuhi_hukuman == true && skp_dua_tahun_terakhir == true && sk_pengangkatan_jabatan_terakhir == true && sk_kenaikan_pangkat_terakhir == true && ijazah_terakhir == true && bukti_sk_pbj == true && surat_bertugas_pbj == true && no_surat_usulan_mengikuti_inpassing == true){
            $('#pesertaForm').submit();
        } else {
            alert('Terdapat kesalahan/kekurangan pada berkas yang diunggah.\nSilakan periksa kembali berkas yang diunggah.');
        }
    });
	
	$('#datepicker').datepicker({
		uiLibrary: 'bootstrap4',
		format: 'dd/mm/yyyy'
	});

    $('#datepicker2').datepicker({
		uiLibrary: 'bootstrap4',
		format: 'dd/mm/yyyy'
	});
	
	$(document).ready(function() {
		$('.js-example-basic-single').select2();
	});
	
	$(document).ready(function() {
		$('.js-example-basic-single-2').select2();
	});

	$('#provinsi').on('change', function() {
		var select = this.value;
		event.preventDefault();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});

		jQuery.ajax({
			method: 'get',
			url: "get-kota/" + select,
			success: function(result){
				if (result.msg == 'berhasil') {
					$('#kab_kota').find('option').remove().end();
					$('#kab_kota').append(result.data);
				} else {
					$('#kab_kota').find('option').remove().end();
				}
			}
		});
	});
	
	$('#no_surat').on('change', function() {
		var select = this.value;
		var jenjang_get = $('#jenjang').val();
		var jenjang = jenjang_get.toLowerCase();
		event.preventDefault();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});

		jQuery.ajax({
			method: 'get',
			url: "cek-jenjang/" + jenjang + "/"+ select,
			success: function(result){
				if (result.msg == 'penuh') {
					alert('kuota penuh, silahkan pilih jenjang/no surat usulan yang lainnya.');
					$('#jabatan').val('');
					$('#pangkat').val('');
					$('#jenjang').val('');
					$('#errPangkat').html('jenjang penuh,silahkan pilih jenjang yang lain');
					$('#errno_surat').html('jenjang penuh,silahkan pilih jenjang yang lain');
				} else {
					$('#errno_surat').html('');
					$('#errPangkat').html('');
				}
			}
		});
	});

	$('#pangkat').on('change', function() {
		var value = this.value;
		if(value == 'Penata Muda (III/a)'){
			var jenjang = 'pertama';
		}
		
		if(value == 'Penata Muda Tk.I (III/b)'){
			var jenjang = 'pertama';
		}
		
		if(value == 'Penata (III/c)'){
			var jenjang = 'muda';
		}
		
		if(value == 'Penata Tk.I (III/d)'){
			var jenjang = 'muda';
		}
		
		if(value == 'Pembina (IV/a)'){
			var jenjang = 'madya';
		}
		
		if(value == 'Pembina Tk.I (IV/b)'){
			var jenjang = 'madya';
		}
		
		if(value == 'Pembina Utama Muda (IV/c)'){
			var jenjang = 'madya';
		}
		
		if(value == ''){
			var jenjang = '';
		}
		
		var select = $('#no_surat :selected').val();
		event.preventDefault();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});

		jQuery.ajax({
			method: 'get',
			url: "cek-jenjang/" + jenjang + "/"+ select,
			success: function(result){
				if (result.msg == 'penuh') {
					$('#jabatan').val('');
					$('#errPangkat').html('jenjang penuh,silahkan pilih jenjang yang lain');
					$('#errno_surat').html('jenjang penuh,silahkan pilih jenjang yang lain');
				} else {
					$('#errPangkat').html('');
					$('#errno_surat').html('');
				}
			}
		});
	});
</script>
@endsection