@extends('layout.app')

@section('title')
    Verifikasi Berkas Portofolio
@endsection

@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }
    [data-toggle="collapse"] .fa:before {  
      content: "\f139";
    }

    [data-toggle="collapse"].collapsed .fa:before {
      content: "\f13a";
    }

    .accor{
      margin-top: 35px;
    }

    .accor-body{
      margin: 0px 10px;
    }
    ol.ol-sub-a{
      padding-inline-start: 18px !important;
    }

    .btn-link{
      color: #000 !important;
    }

    .btn-judul{
      width: 90%;
      text-align: left;
      background: #f8f8f8;
      -webkit-box-shadow: 0px 3px 4px -3px #000000; 
      box-shadow: 0px 3px 4px -3px #000000;
    }

    .collapse-body{
      width: 90%;
      padding: 15px;
      border: 1px solid #b3aeae;
    }

    .collapse-all{
      margin: 10px 0px;
    }

    span.err{
        color: red;
    }

    .lis{
      padding: 10px 30px;
      border-bottom: 1px solid #c3bdbd;
      /* background: #f1f1f1; */
      margin-bottom: 10px;
    }

    .bg-active{
      background: #f8f8f8;
    }

    @media(max-width:992px){
    .wrapper{
    width:100%;
    } 
    }
    .panel-heading {
    padding: 0;
        border:0;
    }
    .panel-title>a, .panel-title>a:active{
        display:block;
        padding:15px;
    color:#555;
    font-size:16px;
    font-weight:bold;
        text-transform:uppercase;
        letter-spacing:1px;
    word-spacing:3px;
        text-decoration:none;
    }
    .panel-heading  a:before {
    font-family: 'Glyphicons Halflings';
    content: "\e114";
    float: right;
    transition: all 0.5s;
    }
    .panel-heading.active a:before {
        -webkit-transform: rotate(180deg);
        -moz-transform: rotate(180deg);
        transform: rotate(180deg);
    }
    .main-box img{
        width: 120px;
    }
</style>
@endsection

@section('content')
{{-- {asdasda} --}}
{{-- sadasd --}}
{{-- sda --}}
{{-- asdasd --}}
{{-- asdasd --}}
<form action="" method="post">
    @csrf
<div class="main-box">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Verifikasi Portofolio</h3>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    Nama Peserta
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                    {{ $data->nama }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    No Ujian
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                    Dummy
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    NIP
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                    {{ $data->nip }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    Pangkat/Gol.
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                    {{ $data->jabatan }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    Jenjang
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                    {{ $data->jenjang }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    SK KP Terakhir
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                    @if($data->sk_kenaikan_pangkat_terakhir == "")
                        -
                    @else
                    @php
                    $sk_kenaikan_pangkat_terakhir = explode('.',$data->sk_kenaikan_pangkat_terakhir);
                    @endphp
                    @if ($sk_kenaikan_pangkat_terakhir[1] == 'pdf' || $sk_kenaikan_pangkat_terakhir[1] == 'PDF')
                    <a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$data->sk_kenaikan_pangkat_terakhir }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                    @else
                    <a href="{{ url('priview-file')."/sk_kenaikan_pangkat_terakhir/".$data->sk_kenaikan_pangkat_terakhir }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->sk_kenaikan_pangkat_terakhir }}" class="img-rounded"></a>
                    @endif    
                    @endif    
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    Asesor
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                    {{ $data->asesors }}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    Rekomendasi
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    :
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                            {!! Form::select('rekomendasi', array('lulus' => 'Lulus', 'tidak_lulus' => 'Tidak Lulus'), $data->status_ujian, ['placeholder' => 'please select', 'class' => 'form-control form-pjg']); !!}
                    </div>
                </div>
                @if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-11">
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-11">
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="publish" id="publish" value="publish" class="publish" {{ $data->publish == 'publish' ? 'checked' : '' }}>
                                Publish
                            </label>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <input type="hidden" name="jenjang" value="{{ $data->jenjang }}">
        <div class="row">
            <div class="col-md-12">
                <h4>Dokumen Portofolio</h4>
                <hr>
            </div>
        </div>
        <div class="wrapper" style="width:90%">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            @foreach ($judul_input as $key => $poins)
                @php
                    $key++;
                    $alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
                @endphp
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading{{ $key }}">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}" aria-expanded="true" aria-controls="collapse{{ $key }}">
                        {{ $alphabet[$key - 1].'.'.$poins->nama }}
                        </a>
                    </h4>
                    </div>
                    <div id="collapse{{ $key }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $key }}">
                    <div class="panel-body">
                    <ol type="1">
                        @foreach ($detail_input as $key_d => $details)
                            @if ($details->id_judul_input == $poins->id)
                                <li>{{ $details->nama }}
                                    <ol type="a" class="ol-sub-a">
                                        @foreach ($input_form as $inputs)
                                            @if ($inputs->id_detail_input == $details->id)
                                            <li>
                                            <ul style="list-style-type:none; padding: 5px 10px" class="{{ $key++ % 2 == 0 ? 'bg-active' : '' }}">
                                            <li>{{ $inputs->title_1 }} : <br>
                                            @if($dokumen != "")
                                                @foreach ($dokumen as $dokumens)
                                                    @if($inputs->id == $dokumens->id_detail_file_input)
                                                        @if($dokumens->file == "")
                                                            -
                                                        @else
                                                            @php
                                                            $file = explode('.',$dokumens->file);
                                                            @endphp
                                                            @if ($file[1] == 'pdf' || $file[1] == 'PDF')
                                                                <a href="{{ url('priview-file')."/dokumen_portofolio/".$dokumens->file }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                                                @else
                                                                <a href="{{ url('priview-file')."/dokumen_portofolio/".$dokumens->file }}" target="_blank"><img src="{{ asset('storage/data/dokumen_portofolio')."/".$dokumens->file }}" class="img-rounded"></a>
                                                            @endif    
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @else
                                                echo "-";
                                            @endif
                                            </li>
                                            <li>{{ $inputs->title_2 }} : <br>
                                            <b style="font-size:25px">
                                            <?php 
                                                if($dokumen != ""){
                                                    foreach ($dokumen as $dokumens) {
                                                        if ($inputs->id == $dokumens->id_detail_file_input) {
                                                        echo $dokumens->tahun;
                                                        }
                                                    }
                                                }else{
                                                    echo "-";
                                                }
                                            ?>
                                            </b>
                                            </li>
                                            @if ($status_file != "")
                                            @php
                                                $jumlah_input = count($input_form);
                                                $status_array = [];
                                            @endphp
                                            @foreach ($status_file as $key => $statuf)
                                                @php
                                                    $status_array[$key]['id_id_detail_file_input'] = $statuf->id_detail_file_input;
                                                    $status_array[$key]['value_select'] = $statuf->status;
                                                    $status_array[$key]['value_text'] = $statuf->keterangan;
                                                @endphp
                                            @endforeach
                                            @php
                                                print_r($status_array);
                                            @endphp
                                            @for ($i = 0; $i < $jumlah_input; $i++)
                                                
                                            @endfor
                                            <li>Status :<br>
                                                {!! Form::select('status_'.$inputs->id, array('sesuai' => 'Sesuai', 'tidak_sesuai' => 'Tidak Sesuai') , null, ['placeholder' => 'pilih status', 'class' => 'form-control', 'id' => 'status_'.$inputs->id]); !!}
                                            </li>
                                            <li>Keterangan :<br>
                                                <textarea name="keterangan_{{ $inputs->id }}" cols="25" rows="5" id="keterangan_{{ $inputs->id }}"></textarea>
                                            </li>
                                            @else
                                                <li>Status :<br>
                                                    {!! Form::select('status_'.$inputs->id, array('sesuai' => 'Sesuai', 'tidak_sesuai' => 'Tidak Sesuai') , null, ['placeholder' => 'pilih status', 'class' => 'form-control', 'id' => 'status_'.$inputs->id]); !!}
                                                </li>
                                                <li>Keterangan :<br>
                                                    <textarea name="keterangan_{{ $inputs->id }}" cols="25" rows="5" id="keterangan_{{ $inputs->id }}"></textarea>
                                                </li>
                                            @endif
                                            {{-- <li>Status :<br>
                                            @if ($status_file != "")
                                                @foreach ($status_file as $statuf)
                                                    @if ($statuf->id_detail_file_input == $inputs->id)
                                                    {!! Form::select('status_'.$inputs->id, array('sesuai' => 'Sesuai', 'tidak_sesuai' => 'Tidak Sesuai') , $statuf->status, ['placeholder' => 'pilih status', 'class' => 'form-control', 'id' => 'status_'.$inputs->id]); !!}
                                                    @endif
                                                @endforeach
                                            @else
                                                {!! Form::select('status_'.$inputs->id, array('sesuai' => 'Sesuai', 'tidak_sesuai' => 'Tidak Sesuai') , null, ['placeholder' => 'pilih status', 'class' => 'form-control', 'id' => 'status_'.$inputs->id]); !!}
                                            @endif
                                            </li>
                                            <li>Keterangan :<br>
                                            @if ($status_file != "")
                                            @foreach ($status_file as $statuf)
                                                @if ($statuf->id_detail_file_input == $inputs->id)
                                                <textarea name="keterangan_{{ $inputs->id }}"  cols="25" rows="5" id="keterangan_{{ $inputs->id }}">{{ $statuf->keterangan }}</textarea>
                                                @endif
                                            @endforeach
                                            @endif
                                            @if ($status_file == "[]")
                                            <textarea name="keterangan_{{ $inputs->id }}" cols="25" rows="5" id="keterangan_{{ $inputs->id }}"></textarea>
                                            @endif
                                            </li>     --}}
                                            </ul>
                                            </li>
                                            @endif
                                        @endforeach
                                    </ol>
                                </li>
                            @endif
                        @endforeach
                    </ol>
                    </div>
                    </div>
                </div>
            @endforeach
            {{-- s;lakd --}}
            {{-- asdasda --}}
                {{-- <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Collapsible Group Item #2
                        </a>
                    </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Collapsible Group Item #3
                        </a>
                    </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                    </div>
                </div> --}}
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-11">
                Kompetensi Perencanaan PBJP
            </div>
            <div class="col-lg-1 col-md-1 col-xs-1">
                :
            </div>
            <div class="col-lg-7 col-md-7 col-xs-12">
            @php
            $kompetensi_perencanaan_pbjp = explode('.',$data->kompetensi_perencanaan_pbjp);
            @endphp
            @if ($kompetensi_perencanaan_pbjp[1] == 'pdf' || $kompetensi_perencanaan_pbjp[1] == 'PDF')
            <a href="{{ url('priview-file')."/kompetensi_perencanaan_pbjp/".$data->kompetensi_perencanaan_pbjp }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
            @else
            <a href="{{ url('priview-file')."/kompetensi_perencanaan_pbjp/".$data->kompetensi_perencanaan_pbjp }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->kompetensi_perencanaan_pbjp }}" class="img-rounded"></a>
            @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-11">
                Kompetensi Pemilihan PBJ
            </div>
            <div class="col-lg-1 col-md-1 col-xs-1">
                :
            </div>
            <div class="col-lg-7 col-md-7 col-xs-12">
            @php
            $kompetensi_pemilihan_pbj = explode('.',$data->kompetensi_pemilihan_pbj);
            @endphp
            @if ($kompetensi_pemilihan_pbj[1] == 'pdf' || $kompetensi_pemilihan_pbj[1] == 'PDF')
            <a href="{{ url('priview-file')."/kompetensi_pemilihan_pbj/".$data->kompetensi_pemilihan_pbj }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
            @else
            <a href="{{ url('priview-file')."/kompetensi_pemilihan_pbj/".$data->kompetensi_pemilihan_pbj }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->kompetensi_pemilihan_pbj }}" class="img-rounded"></a>
            @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-11">
                Kompetensi Pengelolaan Kontrak PBJP
            </div>
            <div class="col-lg-1 col-md-1 col-xs-1">
                :
            </div>
            <div class="col-lg-7 col-md-7 col-xs-12">
            @php
            $kompetensi_pengelolaan_kontrak_pbjp = explode('.',$data->kompetensi_pengelolaan_kontrak_pbjp);
            @endphp
            @if ($kompetensi_pengelolaan_kontrak_pbjp[1] == 'pdf' || $kompetensi_pengelolaan_kontrak_pbjp[1] == 'PDF')
            <a href="{{ url('priview-file')."/kompetensi_pengelolaan_kontrak_pbjp/".$data->kompetensi_pengelolaan_kontrak_pbjp }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
            @else
            <a href="{{ url('priview-file')."/kompetensi_pengelolaan_kontrak_pbjp/".$data->kompetensi_pengelolaan_kontrak_pbjp }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->kompetensi_pengelolaan_kontrak_pbjp }}" class="img-rounded"></a>
            @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-11">
                Kompetensi PBJP Secara Swakelola
            </div>
            <div class="col-lg-1 col-md-1 col-xs-1">
                :
            </div>
            <div class="col-lg-7 col-md-7 col-xs-12">
            @php
            $kompetensi_pbj_secara_swakelola = explode('.',$data->kompetensi_pbj_secara_swakelola);
            @endphp
            @if ($kompetensi_pbj_secara_swakelola[1] == 'pdf' || $kompetensi_pbj_secara_swakelola[1] == 'PDF')
            <a href="{{ url('priview-file')."/kompetensi_pbj_secara_swakelola/".$data->kompetensi_pbj_secara_swakelola }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
            @else
            <a href="{{ url('priview-file')."/kompetensi_pbj_secara_swakelola/".$data->kompetensi_pbj_secara_swakelola }}" target="_blank"><img src="{{ asset('storage/data/sk_kenaikan_pangkat_terakhir')."/".$data->kompetensi_pbj_secara_swakelola }}" class="img-rounded"></a>
            @endif
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-10" style="text-align: right">
                <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
                <button type="submit" class="btn btn-sm btn-default1">Simpan</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection
@section('js')
<script>
$(document).ready(function() {
    @foreach ($judul_input as $poins)
        @foreach($detail_input as $details)
            @if ($details->id_judul_input == $poins->id)
                @foreach ($input_form as $inputs)
                    @if ($inputs->id_detail_input == $details->id)
                        @foreach ($status_file as $statuf)
                            @if ($statuf->id_detail_file_input == $inputs->id)
                                $('#keterangan_{{ $inputs->id }}').hide();
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @endif
        @endforeach
    @endforeach
    });
</script>
<script>
$(document).ready(function() {
@foreach ($judul_input as $poins)
    @foreach($detail_input as $details)
        @if ($details->id_judul_input == $poins->id)
            @foreach ($input_form as $inputs)
                @if ($inputs->id_detail_input == $details->id)
                    @foreach ($status_file as $statuf)
                        @if ($statuf->id_detail_file_input == $inputs->id)
                            var keterangan_{{ $inputs->id }} = $('#status_{{ $inputs->id }}').val();
                            
                            if(keterangan_{{ $inputs->id }} == 'sesuai')
                            {
                                $('#keterangan_{{ $inputs->id }}').show();
                            }
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif
    @endforeach
@endforeach
});
</script>
<script>
@foreach ($judul_input as $poins)
    @foreach($detail_input as $details)
        @if ($details->id_judul_input == $poins->id)
            @foreach ($input_form as $inputs)
                @if ($inputs->id_detail_input == $details->id)
                    @foreach ($status_file as $statuf)
                        @if ($statuf->id_detail_file_input == $inputs->id)
                            $('#status_{{ $inputs->id }}').on('change', function() {
                                var select_{{ $inputs->id }} = this.value;

                                if(select_{{ $inputs->id }} == 'tidak_sesuai'){
                                    $('#keterangan_{{ $inputs->id }}').hide();
                                }

                                if(select_{{ $inputs->id }} == 'sesuai'){
                                    $('#keterangan_{{ $inputs->id }}').show();
                                }

                                if(select_{{ $inputs->id }} == ''){
                                    $('#keterangan_{{ $inputs->id }}').hide();
                                }
                            });
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif
    @endforeach
@endforeach
</script> 
<script>
 $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });
</script>
@endsection