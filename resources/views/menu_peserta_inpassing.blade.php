@extends('layout.app2')
@section('title')
	Data Peserta Uji Kompetensi (Regular LKPP)
@endsection
@section('css')
<style>
	body{
		background-color: whitesmoke;
	}
	
	.main-page{
		margin-top: 20px;
	}

	.box-container{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		background-color: white;
		border-radius: 5px;
		padding: 3%;
	}

	.shadow{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		max-width: 250px;
		line-height: 15px;
		width: 100%;
		margin: 5px;
	}

	div.dataTables_wrapper div.dataTables_info{
		display: none;
	}

	div.dataTables_wrapper .row.col-sm-12{
		width: 120px;
	}

	p{
		font-weight: 500;
	}

	b{
		font-weight: 500;
	}

	.row a.btn{
		text-align: left;
		font-weight: 600;
		font-size: small;
	}

	.btn-default1{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
		color: white;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default2{
		background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
		color: black;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default3{
		background: #fff;
		color: #000;
		font-weight: 500;
		width: 100px;
	}

	.row-input{
		padding-bottom: 10px;
	}

	.page div.verifikasi{
		display: none;
	}

	.btn-area{
		text-align: right;
	}
</style>
@endsection
@section('content')
<div class="main-page">
	<div class="container">
		<div class="row box-container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12" style="">
						<div class="row">
							<div class="col-md-12">
								<h5>Data Peserta Uji Kompetensi ({{ Helper::getMetodeInpassing($jadwal->metode) }}) tanggal {{ $jadwal->metode == 'tes_tulis' ? Helper::tanggal_indo($jadwal->tanggal_tes) : Helper::tanggal_indo($jadwal->tanggal_verifikasi) }}</h5>
								<hr>
								@if (session('msg'))
									@if (session('msg') == "berhasil")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil Simpan Data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "gagal")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal Simpan Data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "penuh")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Kuota Penuh Gagal Input Data</strong>
										</div> 
									@endif
								@endif
							</div>
							<div class="col-md-3" style="text-align:right"></div>
							<div class="col-md-12 page">
								<div class="main-box" id="view">
									<div class="min-top">
										<div class="row">
											<div class="col-md-1 text-center">
												<b>Perlihatkan</b>
											</div>
											<div class="col-md-2">
												<select name='length_change' id='length_change' class="form-control form-control-sm">
													<option value='50'>50</option>
													<option value='100'>100</option>
													<option value='150'>150</option>
													<option value='200'>200</option>
												</select>
											</div>
											<div class="col-md-4 col-12">
												<div class="form-group" style="margin-bottom:0px !important">
													<div class="input-group input-group-sm">
														<div class="input-group-prepend">
															<span class="input-group-text"><i class="fa fa-search"></i></span>
														</div>
														<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
													</div>
												</div>
											</div>
										</div> 
									</div>
									<div class="table-responsive">
										<table id="example1" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>No</th>
													<th>Nama</th>
													<th>NIP</th>
													<th>Pangkat/Gol.</th>
													<th>Jenjang</th>
													<th>Instansi</th>
													<th>Status</th>
												</tr>
											</thead>
											<tbody>
											@foreach ($data as $key => $datas)												
												<tr>
													<td>{{ $key++ + 1 }}</td>
													<td>{{ $datas->nama }}</td>
													<td>{{ $datas->nip }}</td>
													<td>{{ $datas->jabatan }}</td>
													<td>{{ ucwords($datas->jenjang) }}</td>
													<td>{{ $datas->instansis }}</td>
													<td>
													@if( ($datas->status_ujians == 'lulus' && $datas->publish == 'publish') || ($datas->status_ujians == 'tidak_lulus' && $datas->publish == 'publish') || ($datas->status_ujians == 'tidak_hadir' && $datas->publish == 'publish') || ($datas->statuss == 'Tidak Lulus') ||($datas->statuss == 'Lulus')  )
														Selesai
													@elseif($datas->status_ujians == 'tidak_lengkap' && $datas->publish == 'publish')
														Dokumen Persyaratan Tidak Lengkap
													@else
														{{ $datas->statuss }}
													@endif
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> 
    </div>
</div>
</div>
@endsection
@section('js')
<script>
	$('#btn-tambah').click(function (e) {
		e.preventDefault();
		document.getElementById("verif").style.display = "block";
		document.getElementById("view").style.display = "none";
	});

	$('#btn-back').click(function (e) {
		e.preventDefault();
		document.getElementById("verif").style.display = "none";
		document.getElementById("view").style.display = "block";
	});
</script>