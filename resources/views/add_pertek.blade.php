@extends('layout.app')
@section('title')
	Penyusunan Pertek
@stop
@section('css')
<link rel="stylesheet" href="{{ asset('assets/dist/css/bootstrap-multiselect.css') }}">
<style>
	.main-box .col-md-3{
		font-weight: 600;
		font-size: medium;
	}

	.form-pjg{
		width: 50% !important;
	}

	.publish{
		width: 20px;
		height: 20px;
		border: 2px solid black;
		padding: 5px;
	}

	.btng,.err{
		color: red;
	}
	
	.checkmark {
		top: 0;
		left: 0;
		height: 25px;
		width: 25px;
		background-color: #00aaaa;
	}
</style>
@endsection
@section('content')
<div class="main-box" style="padding-bottom: 2%;">
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<h3>Penyusunan Pertek </h3>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-12"><hr>
			@if (session('msg'))
				@if (session('msg') == "berhasil")
					<div class="alert alert-success alert-dismissible col col-md-8">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Berhasil simpan data</strong>
					</div> 
				@else
					<div class="alert alert-warning alert-dismissible col col-md-8">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Gagal simpan data</strong>
					</div> 
				@endif
			@endif
			</div>
		</div>
		<div class="row">
			<form id="PertekForm" action="" method="get">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-3 col-xs-10">
							Instansi <span class="btng">*</span>
						</div>
						<div class="col-md-1 col-xs-1">:</div>
						<div class="col-md-6 col-xs-12">
							<form action="" method="get"> 
								<select name="instansi" id="instansi" class="form-control js-example-basic-single" required="">
									<option selected="selected" disabled="">Pilih Instansi</option>
									@foreach($instansi as $instansis)
										@if($instansis->id == "")
											<option {{ $instansis->id == "" ? 'selected' : '' }}></option>
										@else
											<option value="{{ $instansis->id }}" {{ $instansis->id == $Pilihinstansi ? 'selected' : '' }} nama="{{ $instansis->nama}}" >{{ $instansis->nama}}</option>
										@endif
									@endforeach
								</select><br>
								<input type='hidden' class="form-control" name="nama_instansi" id="namainstansi" />
						</div>
					</div>
					<div class="row" style="margin-top: 3%">
						<div class="col-md-3 col-xs-10">
							No. Surat Usulan Mengikuti Inpassing 
						</div>
						<div class="col-md-1 col-xs-1">:</div>
						<div class="col-md-6 col-xs-12">              
							<select class="form-control" id="pilih_no" name='no_surat_usulan_peserta[]' multiple="multiple" data-live-search="true" >
							@if($NoSurat != "")
								@foreach ($NoSurat as $key => $data) 
									<option value='{{$data->no_surat}}' @if($input_nosurat != "") @foreach ($input_nosurat as $key => $data_input)  {{ $data->no_surat == $data_input ? 'selected' : '' }} @endforeach  @endif {{-- name='no_surat_usulan_peserta[]' --}} id='nosurat{{$data->no_surat}}'>{{$data->no_surat}}</option>
								@endforeach
							@endif
							</select> 
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-top: 2%;">
							<button type="submit" class="btn btn-primary pull-left" id="tampil-peserta">Tampil Peserta</button>
							</form>
						</div>
					</div>
				</div>
				<div id="pertek_tambah">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-3 col-xs-10">
								Status Pertek
							</div>
							<div class="col-md-1 col-xs-1">:</div>
							<div class="col-md-6 col-xs-12">
								<select name="status_pertek" id="statuspertek" class="form-control" required>
									<option value="penyusunan">Penyusunan</option>
									<option value="terkirim" >Terkirim</option>
								</select>
								<span class="errmsg">{{ $errors->first('status_pertek') }}</span>
							</div>
						</div>
						<div class="row" style="margin-top: 3%;">
							<div class="col-md-3 col-xs-10">
								No. Pertek 
							</div>
							<div class="col-md-1 col-xs-1">:</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<input type='text' class="form-control" name="no_pertek" id="no_pertek" />
								</div>
								<span class="errmsg">{{ $errors->first('no_pertek') }}</span>
							</div>
						</div>
						<div class="row" style="margin-top: 3%;">
							<div class="col-md-3 col-xs-10">
								Tanggal Pertek 
							</div>
							<div class="col-md-1 col-xs-1">:</div>
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<div class='input-group date' id='datetimepicker1'>
										<input type='text' class="form-control" name="tanggal_pertek" {{-- @if($statuspertek != "")
										@foreach($statuspertek as $perteks) @if($perteks->tanggal_pertek != "") value={{ date( 'm/d/Y',strtotime($perteks->tanggal_pertek)) }} @endif @endforeach @else @endif --}}{{-- data-inputmask="'alias': 'dd/mm/yyyy'" --}} id="tanggal_pertek" {{-- data-mask --}} autocomplete="off" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
								<span class="errmsg">{{ $errors->first('tanggal_pertek') }}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 2%;">
			<div class="container">
				<div class="col-md-3 col-xs-10">
					Daftar Peserta :
				</div>
			</div>
			<div class="col-xs-12" style="margin-top: 2%">
				<div class="min-top">
					<div class="row">
						<div class="col-md-1 text-center">
							<b>Perlihatkan</b>
						</div>
						<div class="col-md-2 col-6">
							<select name='length_change' id='length_change' class="form-control">
								<option value='50'>50</option>
								<option value='100'>100</option>
								<option value='150'>150</option>
								<option value='200'>200</option>
							</select>
						</div>
						<div class="col-md-3 col-6">
							<div class="input-group">
								<div class="input-group addon">
									<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
									<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
								</div>
							</div>
						</div>
					</div> 
				</div>
				<form action="{{ url('tambah-pertek-aksi') }}" method="post" id="form-pertek">
				@csrf
					<input type="hidden" name="instansi_input" @if($inputinstansi != "") value="{{ $inputinstansi}}" @else @endif>           
					<input type="hidden" name="tanggal_pertek" id="val_tanggalpertek">
					<input type="hidden" name="status_pertek" id="val_statuspertek">
					<input type="hidden" name="no_pertek" id="val_nopertek" >
					<input type="hidden" name="draft" id="val_draft" value="ya">					
					<div class="table-responsive">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Pilih</th>
									<th>Nama</th>
									<th>NIP</th>
									<th>Tanggal Ujian</th>
									<th>Metode Ujian</th>
									<th>Pangkat/Gol.</th>
									<th>TMT Pangkat/Gol.</th>
									<th>Jenjang</th>
									<th>AK Kumulatif</th>
									<th>Status</th>
									<th>Aksi</th> 
								</tr>
							</thead>
							<tbody>
							@if($pesertaInt != "")
								@foreach($pesertaInt as $pesertas)
								@php
									$status_check = "";
									$chechked1 = $terdaftar->where('id_peserta',$pesertas->id_peserta)->where('nomor_surat_usulan_peserta',$pesertas->no_surat_usulan_peserta)->first();
									if($chechked1 != ""){
										$status_check = "checked";
									} else {
										$status_check = "";
									}
								@endphp
								<tr> 
									<td class='text-center'> 
										<div class="checkbox">
											<input type='checkbox' class='checkmark' {{ $status_check }} value="{{$pesertas->id_peserta}}" id="daftar{{$pesertas->ids}}">
										</div>
										<input type="hidden" name="peserta_int[]" value="{{$pesertas->id_peserta}}" class="form-control formpeserta{{ $pesertas->ids}}" id="">
										<input type="hidden" name="pesertatidak_int[]" value="{{$pesertas->id_peserta}}" class="form-control formpesertatidak{{ $pesertas->ids}}" id="">
										<input type="hidden" name="no_surat_usulan_tidak_int[]" value="{{$pesertas->no_surat_usulan_peserta}}" class="form-control formpesertatidak{{ $pesertas->ids}}" id="">
										<input type="hidden" name="no_surat_usulan_int[]" value="{{$pesertas->no_surat_usulan_peserta}}" class="form-control formpeserta{{ $pesertas->ids}}" id="">
									</td>
									<td>{{$pesertas->nama}}</td>
									<td>{{$pesertas->nip}}</td>
									@if($pesertas->tanggal_ujian != 0000-00-00 || $pesertas->tanggal_ujian != 1970-01-01)
										<td>{{ Helper::tanggal_indo($pesertas->tanggal_ujian)}}</td>
									@else
										<td>{{ '-' }}</td>
									@endif
									
									@if($pesertas->metode_ujian == 'verifikasi')
										<td>{{ 'Verifikasi Portofolio' }}</td>
									@elseif($pesertas->metode_ujian == 'tes')
										<td>{{ 'Tes Tertulis' }}</td>
									@else
										<td>{{ '-' }}</td>
									@endif
									
									<td>{{$pesertas->jabatan}}</td>									
									@if($pesertas->tmt_panggol != 0000-00-00 || $pesertas->tmt_panggol != 1970-01-01)
										<td>{{ Helper::tanggal_indo($pesertas->tmt_panggol)}}</td>
									@else
										<td>{{ '-' }}</td>
									@endif
									
									<td>{{$pesertas->jenjang}}</td>
									@if($pesertas->ak != "")
										<td>{{$pesertas->ak}}</td>
									@else
										<td>-</td>
									@endif
									
									@if($pesertas->status == 'tidak_lulus')
										<td>{{ 'Tidak Lulus'}}</td>
									@elseif($pesertas->status == 'lulus')
										<td>{{ 'Lulus'}}</td>
									@elseif($pesertas->status == 'tidak_hadir')
										<td>{{ 'Tidak Hadir'}}</td>
									@elseif($pesertas->status == 'tidak_lengkap')
										<td>{{ 'Dokumen Persyaratan Tidak Lengkap'}}</td>
									@else
										<td>{{ '-' }}</td>
									@endif
										@if($pesertas->status == 'lulus')
											<td>
												<div class='dropdown'>
													<button class='btn btn-sm btn-default btn-action dropdown-toggle' data-toggle='dropdown' type='button'><i class='fa fa-ellipsis-h'></i></button>
													<ul class='dropdown-menu'>
														<li><a href='#' data-toggle='modal' data-target='#modal-ubah{{$pesertas->ids}}'>Ubah AK</a></li>
													</ul>
												</div>
											</td>
										@else
											<td></td>
										@endif
								</tr>              
							@endforeach
						@endif
						
						@if($peserta != "")
							@foreach($peserta as $pesertas
							@php
								$status_check = "";
								$chechked1 = $terdaftar->where('id_peserta', $pesertas->id_peserta)->where('nomor_surat_usulan_peserta', $pesertas->no_surat_usulan_peserta)->first();
								if($chechked1 != ""){
									$status_check = "checked";
								} else {
									$status_check = "";
								}
							@endphp
							<tr> 
								<td class='text-center'> 
									<div class="checkbox">
										<input type='checkbox' class='checkmark' {{ $status_check }} value="{{$pesertas->ids}}" id="daftar{{$pesertas->ids}}">
									</div>
									<input type="hidden" name="peserta[]" value="{{$pesertas->id_peserta}}" class="form-control formpeserta{{ $pesertas->ids}}" id="">
									<input type="hidden" name="pesertatidak[]" value="{{$pesertas->id_peserta}}" class="form-control formpesertatidak{{ $pesertas->ids}}" id="">
									<input type="hidden" name="no_surat_usulan_tidak[]" value="{{$pesertas->no_surat_usulan_peserta}}" class="form-control formpesertatidak{{ $pesertas->ids}}" id="">
									<input type="hidden" name="no_surat_usulan[]" value="{{$pesertas->no_surat_usulan_peserta}}" class="form-control formpeserta{{ $pesertas->ids}}" id="">
									<input type="hidden" name="id_jadwal[]" value="{{$pesertas->id_jadwal}}" class="form-control formpeserta{{ $pesertas->ids}}" id="">
									<input type="hidden" name="id_formasi[]" value="{{-- {{$pesertas->eformasis}} --}}" class="form-control formformasi{{ $pesertas->ids}}" id="">
								</td>
								<td>{{$pesertas->nama}}</td>
								<td>{{$pesertas->nip}}</td>
								@if($pesertas->tanggal_ujian != 0000-00-00 || $pesertas->tanggal_ujian != 1970-01-01)
									<td>{{ Helper::tanggal_indo($pesertas->tanggal_ujian)}}</td>
								@else
									<td>{{ '-' }}</td>
								@endif
								@if($pesertas->metode_ujian == 'verifikasi')
									<td>{{ 'Verifikasi Portofolio' }}</td>
								@elseif($pesertas->metode_ujian == 'tes')
									<td>{{ 'Tes Tertulis' }}</td>
								@else
									<td>{{ '-' }}</td>
								@endif
								<td>{{$pesertas->jabatan}}</td>
								@if($pesertas->tmt_panggol != 0000-00-00 || $pesertas->tmt_panggol != 1970-01-01)
									<td>{{ Helper::tanggal_indo($pesertas->tmt_panggol)}}</td>
								@else
									<td>{{ '-' }}</td>
								@endif
								
								<td>{{$pesertas->jenjang}}</td>
								@if($pesertas->ak != "")
									<td>{{$pesertas->ak}}</td>
								@else
									<td>-</td>
								@endif
								
								@if($pesertas->status == 'tidak_lulus')
									<td>{{ 'Tidak Lulus' }}</td>
								@elseif($pesertas->status == 'lulus')
									<td>{{ 'Lulus' }}</td>
								@elseif($pesertas->status == 'tidak_hadir')
									<td>{{ 'Tidak Hadir' }}</td>
								@elseif($pesertas->status == 'tidak_lengkap')
									<td>{{ 'Dokumen Persyaratan Tidak Lengkap' }}</td>
								@else
									<td>{{ '-' }}</td>
								@endif
								
								@if($pesertas->status == 'lulus')
									<td>
										<div class='dropdown'>
											<button class='btn btn-sm btn-default btn-action dropdown-toggle' data-toggle='dropdown' type='button'><i class='fa fa-ellipsis-h'></i></button>
											<ul class='dropdown-menu'>
												<li><a href='#' data-toggle='modal' data-target='#modal-ubah{{$pesertas->ids}}'>Ubah AK</a></li>
											</ul>
										</div>
									</td>
								@else
									<td> </td>
								@endif
							</tr>               
							@endforeach
						@else
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr> 
						@endif             
					</tbody>
				</table>
			</form>
			@foreach($peserta as $pesertas)
				<div class='modal fade' id='modal-ubah{{$pesertas->ids}}'>
					<div class='modal-dialog' style='width:50%'>
						<div class='modal-content'>
							<div class='modal-header'>
								<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
								<span aria-hidden='true'>&times;</span></button>
								<h4 class='modal-title'>Ubah Angka Kredit</h4>
							</div>
							<div class='modal-body'>
							<form action="{{ url('ganti-ak')}}" method="post" id="form-ak{{$pesertas->ids}}">
							@csrf
								<div class="row">
									<div class="col-md-3">
										Nama 
									</div>
									<div class="col-md-1">
										:
									</div>
									<div class="col-md-8">
										{{ $pesertas->nama }}
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										NIP
									</div>
									<div class="col-md-1">
										:
									</div>
									<div class="col-md-8">
										{{ $pesertas->nip }}
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										Pangkat/Gol.
									</div>
									<div class="col-md-1">
										:
									</div>
									<div class="col-md-8">
										{{ $pesertas->jabatan }}
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										TMT Pangkat/Gol.
									</div>
									<div class="col-md-1">
										:
									</div>
									<div class="col-md-8">
										{{ $pesertas->tmt_panggol }}
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										Pendidikan
									</div>
									<div class="col-md-1">
										:
									</div>
									<div class="col-md-8">
										{{ $pesertas->pendidikan_terakhir }}
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										Masukkan Angka Kredit
									</div>
									<div class="col-md-1">
										:
									</div>
									<div class="col-md-3">
										<input type="text" onkeypress="validate(event)" name="ak" class="form-control form-control-sm" required="" @if($pesertas->ak != "") value="{{ $pesertas->ak }}" @else @endif>
										<input type="hidden" onkeypress="Validate(event)" name="id_peserta_jadwal" class="form-control" value="{{ $pesertas->ids}}">
										<input type="hidden" onkeypress="Validate(event)" name="jenis" class="form-control" value="regular">
									</div>
								</div>
							</div>
							<div class='modal-footer'>
								<button type="submit" class='btn btn-primary pull-left' {{-- id="btn-form-ak{{$pesertas->ids}}" --}} form="form-ak{{$pesertas->ids}}">Simpan</button>
								<button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>
							</form>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				@foreach($pesertaInt as $pesertas)
				<div class='modal fade' id='modal-ubah{{$pesertas->ids}}'>
					<div class='modal-dialog' style='width:50%'>
						<div class='modal-content'>
							<div class='modal-header'>
								<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
								<span aria-hidden='true'>&times;</span></button>
								<h4 class='modal-title'>Ubah Angka Kredit</h4>
							</div>
							<div class='modal-body'>
								<form action="{{ url('ganti-ak')}}" method="post" id="form-ak-int{{$pesertas->ids}}">
								@csrf
									<div class="row">
										<div class="col-md-3">
											Nama 
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $pesertas->nama }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											NIP
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $pesertas->nip }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Pangkat/Gol.
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $pesertas->jabatan }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											TMT Pangkat/Gol.
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $pesertas->tmt_panggol }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Pendidikan
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $pesertas->pendidikan_terakhir }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Masukkan Angka Kredit
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-3">
											<input type="text" onkeypress="validate(event)" name="ak" class="form-control form-control-sm" required="" @if($pesertas->ak != "") value="{{ $pesertas->ak }}" @else @endif>
											<input type="hidden" onkeypress="Validate(event)" name="id_peserta_jadwal" class="form-control" value="{{ $pesertas->ids}}">
											<input type="hidden" onkeypress="Validate(event)" name="jenis" class="form-control" value="instansi">
										</div>
									</div>
							</div>
							<div class='modal-footer'>
								<button type="submit" class='btn btn-primary pull-left' {{-- id="btn-form-ak{{$pesertas->ids}}" --}} form="form-ak-int{{$pesertas->ids}}">Simpan</button>
								<button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>
							</form>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-9" style="text-align: right">
				<button type="submit" class="btn btn-sm btn-default1"  form="form-pertek">Simpan</button>  
				{{-- <button type="submit" class="btn btn-sm btn-warning" form="form-pertek" style="font-weight: 600;">Draft Pertek</button> --}}
				<a href="{{ url('pertek-sphu')}}"><button type="button" class="btn btn-sm btn-default2">Kembali</button></a>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="{{ asset('assets/dist/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript">
    function modifyVar(obj, val) {
		obj.valueOf = obj.toSource = obj.toString = function(){ return val; };
    }

    function setToFalse(boolVar) {
		modifyVar(boolVar, false);
	}

    function setToTrue(boolVar) {
		modifyVar(boolVar, true);
    }

    var validasi_telp = new Boolean(false);
    $(function(){
		$('#btn-form-pertek').on('click', function(){
			$('#form-pertek').submit();
		});
	})
	
	@if($peserta != "")      
		$(function(){
			@foreach($peserta as $pesertas)
				$('#btn-form-ak{{$pesertas->ids}}').on('click', function(){
					$('#form-ak{{$pesertas->ids}}').submit();
				});
			@endforeach
		});
	@endif
	
	@if($pesertaInt != "")      
		$(function(){
		@foreach($pesertaInt as $pesertas)
			$('#btn-form-ak{{$pesertas->ids}}').on('click', function(){
				$('#form-ak{{$pesertas->ids}}').submit();
			});
        @endforeach      
    });
	@endif

    $('#statuspertek').on('change', function() {
		var select = this.value;
		if(select == 'terkirim'){
			$('#tanggal_pertek').attr("disabled",false);
			$('#no_pertek').attr("disabled",false);
		}

		if(select == 'penyusunan'){
			$('#tanggal_pertek').attr("disabled",true);
			$('#no_pertek').attr("disabled",true);
		}
    });
	
	$('#btn-form-draft').on('click', function(){
		$('#label-draft').show();
        $('#val_draft').attr('disabled',false);
	});

    $(function () {
		$('#tanggal_pertek').datepicker({ dateFormat: 'dd, mm, yy' });
    });

    $(function () {
		$('#datetimepicker3').datetimepicker({
			format: 'DD/MM/YYYY'
		});
    });

    $(function () {
		$('#datetimepicker4').datetimepicker({
			format: 'DD/MM/YYYY'
		});
    });
    
	$(document).ready(function() {
		$('.js-example-basic-single').select2();
    });
  
    $(function () {
		$('#datetimepicker2').datetimepicker({
			format: 'LT'
		});
    });
  
    $(document).ready(function() {
		$('#pilih_no').select2();
		$('#pilih_nomor').hide();	
		$('#label-draft').hide();
		$('#val_draft').attr('disabled',true);


		if ( $('#statuspertek').val() == 'terkirim' ) {
			$('#tanggal_pertek').attr("disabled",false);
			$('#no_pertek').attr("disabled",false);
		} else {
			$('#tanggal_pertek').attr("disabled",true);
			$('#no_pertek').attr("disabled",true);
		}
		
		@if($inputinstansi != "")
			$('#pertek_tambah').show();
		@else
			$('#pertek_tambah').hide();
		@endif
    });

    function AddBusinessDays(weekDaysToAdd) {
		var curdate = new Date();
		var realDaysToAdd = 0;
		while (weekDaysToAdd > 0){
			curdate.setDate(curdate.getDate()+1);
			realDaysToAdd++;
			//check if current day is business day
			if (noWeekendsOrHolidays(curdate)[0]) {
				weekDaysToAdd--;
			}
		}
        return realDaysToAdd;
	}
	
	var date_billed = $('#datebilled').datepicker('getDate');
	var date_overdue = new Date();
	var weekDays = AddBusinessDays(30);
	date_overdue.setDate(date_billed.getDate() + weekDays);
	date_overdue = $.datepicker.formatDate('mm/dd/yy', date_overdue);
	$('#datepd').val(date_overdue).prop('readonly', true);        
	$('#statuspertek').on('change', function() {
		var val_status =  $('#statuspertek').val();// get id the value from the select
		$('#val_statuspertek').val(val_status);
	});
	
	$('#no_pertek').on('change', function() {
		var val_status =  $('#no_pertek').val();// get id the value from the select
		$('#val_nopertek').val(val_status);
	});
	
	$('#tanggal_pertek').on('change', function() {
		var val_status =  $("#tanggal_pertek").val(); // get id the value from the select
		$('#val_tanggalpertek').val(val_status);
	});
	
	@if($peserta != "")
		@foreach ($peserta as $pesertas)
			$('.checkbox #daftar{{ $pesertas->ids }}').change(function() {
				// this will contain a reference to the checkbox   
				if (this.checked) {
					var select = this.value;
					$('.formpeserta{{ $pesertas->ids}}').attr("disabled",false);
					$('.formpesertatidak{{ $pesertas->ids}}').attr("disabled",true);
					$('.formformasi{{ $pesertas->ids }}').attr("disabled",false);
				} else {
					var select = this.value;
					$('.formpeserta{{ $pesertas->ids }}').attr("disabled",true);
					$('.formpesertatidak{{ $pesertas->ids}}').attr("disabled",false);
					$('.formformasi{{ $pesertas->ids }}').attr("disabled",true);       
				}
			});
		@endforeach 
	@endif
	
	@if($pesertaInt != "")
		@foreach ($pesertaInt as $pesertas)
			$('.checkbox #daftar{{ $pesertas->ids }}').change(function() {
				// this will contain a reference to the checkbox   
				if (this.checked) {
					var select = this.value;
					$('.formpeserta{{ $pesertas->ids}}').attr("disabled",false);
					$('.formpesertatidak{{ $pesertas->ids}}').attr("disabled",true);
					$('.formformasi{{ $pesertas->ids }}').attr("disabled",false);
				} else {
					var select = this.value;
					$('.formpeserta{{ $pesertas->ids }}').attr("disabled",true);
					$('.formpesertatidak{{ $pesertas->ids}}').attr("disabled",false);
					$('.formformasi{{ $pesertas->ids }}').attr("disabled",true);       
				}
			});
        @endforeach 
	@endif
	
	$(document).ready(function(){
		@if ($peserta != "") 
			@foreach ($peserta as $pesertas)
				//this will contain a reference to the checkbox   
				if ( $('.checkbox #daftar{{ $pesertas->ids }}').attr('checked')) {
					$('.formpeserta{{ $pesertas->ids }}').attr("disabled",false);
					$('.formpesertatidak{{ $pesertas->ids}}').attr("disabled",true);
					$('.formformasi{{ $pesertas->ids }}').attr("disabled",false);       
				} else {
					$('.formpeserta{{ $pesertas->ids }}').attr("disabled",true);
					$('.formpesertatidak{{ $pesertas->ids}}').attr("disabled",false);
					$('.formformasi{{ $pesertas->ids }}').attr("disabled",true);       
				}
			@endforeach
		@endif
	});
	
	$(document).ready(function(){
		@if ($pesertaInt != "") 
			@foreach ($pesertaInt as $pesertas)
				//this will contain a reference to the checkbox   
				if ( $('.checkbox #daftar{{ $pesertas->ids }}').attr('checked')) {
					$('.formpeserta{{ $pesertas->ids }}').attr("disabled",false);
					$('.formpesertatidak{{ $pesertas->ids}}').attr("disabled",true);
					$('.formformasi{{ $pesertas->ids }}').attr("disabled",false);       
				} else {
					$('.formpeserta{{ $pesertas->ids }}').attr("disabled",true);
					$('.formpesertatidak{{ $pesertas->ids}}').attr("disabled",false);
					$('.formformasi{{ $pesertas->ids }}').attr("disabled",true);       
				}
			@endforeach
		@endif
	});

    $('#instansi').on('change', function() {
		var instansi =  $('#instansi option:selected').attr('nama');// get id the value from the select
		$('#namainstansi').val(instansi);
		var select = this.value;
		event.preventDefault();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});

		jQuery.ajax({
			method: 'get',
			url: "get-no-surat/" + select,
			success: function(result){
				if (result.msg == 'berhasil') {
					if (result.data) {
						$('#pilih_no').select2().find('option').remove().end();
						$('#pilih_no').select2({placeholder:"Pilih No. Surat Usulan"}).append(result.data);
					} else if(result.data_not){
						$('#pilih_no').select2().find('option').remove().end();
						$('#pilih_no').select2({
							placeholder:"tidak ditemukan"
						}); 
					}
				} else {
					$('#pilih_no').find('option').remove().end();
				}
			}
		});
	});
</script>
@endsection