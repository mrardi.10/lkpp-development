@extends('layout.app2')

@section('title')
Unggah Dokumen Portofolio
@endsection

@section('css')
<style>
  body{
    background-color: whitesmoke;
  }
  .main-page{
    margin-top: 20px;
  }

  .box-container{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    background-color: white;
    border-radius: 5px;
    padding: 3%;
  }

  .shadow{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    max-width: 250px;
    line-height: 15px;
    width: 100%;
    margin: 5px;
  }

  .custom-file{
    margin: 5px;
  }
  .fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
  }
  .fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
  }

  .textFile::-webkit-input-placeholder{
    font-size: 15px;
  }
  .textFile::-moz-input-placeholder{
    font-size: 15px;
  }
  .textFile::-ms-input-placeholder{
    font-size: 15px;
  }
  div.dataTables_wrapper div.dataTables_info{
    display: none;
  }

  div.dataTables_wrapper .row.col-sm-12{
    width: 120px;
  }

  p{
    font-weight: 500;
  }

  b{
    font-weight: 500;
  }

  .row a.btn{
    text-align: left;
    font-weight: 600;
    font-size: small;
  }

  .btn-default1{
    background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
    color: white;
    font-weight: 600;
    width: 100px; 
    margin: 10px;
    -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }


  .btn-default2{
    background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
    color: black;
    font-weight: 600;
    width: 100px; 
    margin: 10px;
    -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }

  .btn-default3{
    background: #fff;
    color: #000;
    font-weight: 500;
    width: 100px;
  }


  .row-input{
    padding-bottom: 10px;
  }

  .page div.verifikasi{
    display: none;
  }

  .btn-area{
    text-align: right;
  }

  .clickable-row:hover{
    cursor: pointer;
  }

  .checkmark {
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #00aaaa;
  }

  .modal .modal-dialog{
    max-width: 800px !important;
  }

  .line-center{
    line-height: 2.5;
  }

  span.berkas{
    font-size: 10px;
    font-weight: 600;
  }

  span.berkas label{
    margin-bottom: 0px !important;
  }
  [data-toggle="collapse"] .fa:before {  
    content: "\f139";
  }

  [data-toggle="collapse"].collapsed .fa:before {
    content: "\f13a";
  }

  .alert-row{
    margin-top: 35px;
  }

  .accor-body{
    /* margin: 0px 10px; */
  }
  ol.ol-sub-a{
    padding-inline-start: 18px !important;
  }

  .btn-link{
    color: #000 !important;
  }

  .btn-judul{
    width: 100%;
    text-align: left;
    background: #f8f8f8;
    -webkit-box-shadow: 0px 3px 4px -3px #000000; 
    box-shadow: 0px 3px 4px -3px #000000;
  }

  .collapse-body{
    /* padding: 15px; */
    border: 1px solid #b3aeae;
  }

  .collapse-all{
    margin: 10px 0px;
  }

  span.err{
    color: red;
    position: relative;
  }

  .lis{
    padding: 10px 30px;
    border-bottom: 1px solid #c3bdbd;
    /* background: #f1f1f1; */
    margin-bottom: 10px;
  }

  .bg-active{
    background: #f8f8f8;
  }

  .main-page img{
    width: 80px;
  }

  .table>tbody>tr.active>td,
  .table>tbody>tr.active>th,
  .table>tbody>tr>td.active,
  .table>tbody>tr>th.active,
  .table>tfoot>tr.active>td,
  .table>tfoot>tr.active>th,
  .table>tfoot>tr>td.active,
  .table>tfoot>tr>th.active,
  .table>thead>tr.active>td,
  .table>thead>tr.active>th,
  .table>thead>tr>td.active,
  .table>thead>tr>th.active {
    background-color: #fff;
  }

  .table-bordered > tbody > tr > td,
  .table-bordered > tbody > tr > th,
  .table-bordered > tfoot > tr > td,
  .table-bordered > tfoot > tr > th,
  .table-bordered > thead > tr > td,
  .table-bordered > thead > tr > th {
    border-color: #e4e5e7;
  }

  .table tr.header {
    /* font-weight: bold; */
    /* background-color: whitesmoke; */
    cursor: pointer;
    -webkit-user-select: none;
    /* Chrome all / Safari all */
    -moz-user-select: none;
    /* Firefox all */
    -ms-user-select: none;
    /* IE 10+ */
    user-select: none;
    /* Likely future */
  }

  .table tr:not(.header) {
    display: none;
  }

  .table .header td:after {
    content: "\002b";
    position: relative;
    top: 1px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: 400;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    float: right;
    color: #999;
    text-align: center;
    padding: 3px;
    transition: transform .25s linear;
    -webkit-transition: -webkit-transform .25s linear;
  }

  .table .header.active td:after {
    content: "\2212";
  }


  

</style>
@endsection

@section('content')
{{-- sdlajkljs --}}
{{-- sdasd --}}
{{-- aasa --}}
{{-- sadas --}}
{{-- asdasd --}}
{{-- dsad --}}
<div class="main-page">
  <div class="container">
    <div class="row box-container">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-12" style="">
            <div class="row">
              <div class="col-md-9">
                <h5>Unggah Dokumen Portofolio <br> Peserta Ujian Tanggal {{ Helper::tanggal_indo($jadwal->tanggal_verifikasi) }} dengan Metode {{ $jadwal->metode == 'verifikasi_portofolio' ? 'Verifikasi Portofolio' : 'Tes Tertulis' }}</h5>
                {{-- {{ $data }} --}}
                <hr>
                @if (session('msg'))
                @if (session('msg') == "berhasil")
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Berhasil Simpan Data</strong>
                </div> 
                @else
                <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Gagal Simpan Data</strong>
                </div> 
                @endif
                @endif
              </div>
              <div class="col-md-3" style="text-align:right">
                <i class="fa fa-bell"></i>
              </div>
              <div class="col-md-9 page">
                {{-- <form action="{{ url('store-dokumen-ujian') }}" method="post" enctype="multipart/form-data" id="store"> --}}
                  @csrf
                  <input type="hidden" name="tanggal_ujian" value="{{ Helper::tanggal_indo($jadwal->tanggal_ujian) }}">
                  <input type="hidden" name="lokasi_ujian" value="{{ $jadwal->lokasi_ujian }}">
                  <div class="row">
                    <div class="col-md-3">
                      Nama
                    </div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-8">
                      {{ $peserta->nama }}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      Pangkat/Gol.
                    </div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-8">{{ $peserta->jabatan }}</div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      Jenjang
                    </div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-8">
                      {{ ucwords($peserta->jenjang) }}
                    </div>
                  </div>
                  <div class="row alert-row">
                    <div class="col-md-12">
                      <div class="alert alert-primary" role="alert">
                        Dokumen yang diinput min. 100KB maks. 2MB, dengan format PDF, JPG, JPEG 
                      </div>
                    </div>
                  </div>
                  {{-- {{ $dokumen }} --}}
                  <input type="hidden" name="id_peserta" value="{{ $peserta->id }}">
                  <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                  <div class="row accor">
                    <div class="container">
                      <div id="accordion">
                        @php
                        $jumlah_poin = 0;
                        $jumlah_upload = 0;
                        @endphp
                        @foreach ($judul_input as $key => $poins)
                        @php
                        $jumlah_sebelumnya = $jumlah_poin;
                        $key++;
                        $alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
                        foreach($detail_input as $details){
                        if($details->id_judul_input == $poins->id){
                        $jumlah_poin++;
                        foreach ($input_form as $inputs){
                        if($inputs->id_detail_input == $details->id){
                        if($dokumen != ""){
                        foreach($dokumen as $dokumens){
                        if($inputs->id == $dokumens->id_detail_file_input){
                        $jumlah_upload++;
                      }
                    }
                  }
                }
              }
            }
          }
          @endphp
          <div class="collapse-all">
            <div id="heading{{ $key }}">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed btn-judul" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="false" aria-controls="collapse{{ $key }}" type="button">
                  {{ $alphabet[$key - 1].'.'.$poins->nama.' '.$poins->keterangan }} <span class="float-right">{{ floor($jumlah_upload/2) }}/{{ $jumlah_poin - $jumlah_sebelumnya }} <i class="fa" aria-hidden="true"></i></span>
                  @php
                  $jumlah_upload = 0;
                  @endphp
                </button>
              </h5>
            </div>
            <div id="collapse{{ $key }}" class="collapse collapse-body" aria-labelledby="heading{{ $key }}" data-parent="#accordion">
              <form id = "pesertaForm_{{ $poins->id }}"action="{{ url('unggah-dokumen-portofolio') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="tanggal_ujian" value="{{ Helper::tanggal_indo($jadwal->tanggal_ujian) }}">
                <input type="hidden" name="lokasi_ujian" value="{{ $jadwal->lokasi_ujian }}">
                <input type="hidden" name="id_peserta" value="{{ $peserta->id }}">
                <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                <div class="accor-body">
                  <table class="table table-bordered">
                    @php
                    $no_detail = 0;
                    @endphp
                    @foreach ($detail_input as $details)
                    <input type="hidden" name="id_judul" value="{{ $poins->id }}">
                    <input type="hidden" name="jenjang" value="{{ $peserta->jenjang }}">
                    @if ($details->id_judul_input == $poins->id)
                    @php
                    $no_detail++;
                    @endphp
                    <tr class="header {{ $details->nama == "" ? 'active' : '' }}">
                      <td colspan="3" style="padding-left: 20px">{{ $no_detail.'.'.$details->nama }}</td>
                    </tr>
                    @php
                    $no_in = 0;
                    @endphp
                    @foreach ($input_form as $inputs)
                    @if ($inputs->id_detail_input == $details->id)
                    @php
                    $alphabet_in = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
                    @endphp
                    <tr
                    @if ($details->nama == "")
                    style="display: table-row;"
                    @endif
                    >

                    <td rowspan="2" style="padding-left: 35px;">{{ $alphabet_in[$no_in].'.' }}</td>
                  @if ($inputs->nama_input_1 != "")
                    <td>{{ $inputs->title_1 }}</td>
                    <td>
                      @if ($inputs->title_1 != "Dokumen Hasil Pekerjaan")
                        <input id="uploadFile_{{ $inputs->nama_input_1 }}" class="{{ $inputs->nama_input_1 }} form-control textFile" placeholder="Tidak ada Dokumen yang dipilih" disabled="disabled" />
                          <div class="fileUpload btn btn-primary">
                            <span>Pilih Dokumen</span>
                            <input type="file" class="upload" name="{{ $inputs->nama_input_1 }}" id="cek_file_{{ $inputs->nama_input_1 }}"  class="{{ $inputs->nama_input_1 }} uploadBtn" accept="" ="application/pdf, .jpg, .jpeg, image/jpg, image/jpeg"/>
                          </div>
                      @endif

                      {{--required oninvalid="this.setCustomValidity('data tidak boleh kosong')" oninput="setCustomValidity('')"--}}
                      {{--<input type="file" name="{{ $inputs->nama_input_1 }}" id="cek_file_{{ $inputs->nama_input_1 }}" class="{{ $inputs->nama_input_1 }} " accept="" ="application/pdf, .jpg, .jpeg, image/jpg, image/jpeg"  >--}}


                      @if ($dokumen != "")
                          @php
                               $cek_upload = true;
                          @endphp

                        @foreach ($dokumen as $dokumens)
                            @if ($inputs->id == $dokumens->id_detail_file_input)
                                <div class="row" style="margin-top:10px">
                                    <div class="col-md-5">Berkas Sebelumnya</div>
                                    <div class="col-md-1 line-center">:</div>
                                    <div class="col-md-5" style="line-height: 3">
                                        @if ($dokumens->file == "")
                                        - 
                                        @else
                                            @php
                                                $file = explode('.',$dokumens->file);
                                            @endphp
                                                @if ($file[1] == 'pdf' || $file[1] == 'PDF')
                                                  <a href="{{ url('priview-file')."/dokumen_portofolio/".$dokumens->file }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:20px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                                @else
                                                  <a href="{{ url('priview-file')."/dokumen_portofolio/".$dokumens->file }}" target="_blank"><img src="{{ asset('storage/data/dokumen_portofolio')."/".$dokumens->file }}" class="img-rounded"></a>
                                                @endif
                                        @endif
                                  @php
                                  $cek_upload = false;
                                  @endphp
                                  </div>
                              </div>
                          @endif
                          @if ($inputs->id_detail_input == $dokumens->id_detail_input)
                              @if ($inputs->id > $dokumens->id_detail_file_input)
                                  <input id="uploadFile_{{ $inputs->nama_input_1 }}" class="{{ $inputs->nama_input_1 }} form-control textFile" placeholder="Tidak ada Dokumen yang dipilih" disabled="disabled" />
                                      <div class="fileUpload btn btn-primary">
                                        <span>Pilih Dokumen</span>
                                        <input type="file" class="upload" name="{{ $inputs->nama_input_1 }}" id="cek_file_{{ $inputs->nama_input_1 }}" class="{{ $inputs->nama_input_1 }} uploadBtn" accept="" ="application/pdf, .jpg, .jpeg, image/jpg, image/jpeg"/>
                                      </div>
                                  @php
                                    $cek_upload = false;
                                  @endphp
                              @endif    
                          @endif
                        @endforeach
                          {{-- @if ($cek_upload == true)
                              @if ($inputs->title_1 == "Dokumen Hasil Pekerjaan")--}}
                                  <label class="fileLabel"></label>
                              {{--@endif
                          @endif --}}
                      @endif
                            <span class="err" id="{{ "err".$inputs->nama_input_1 }}"></span>
                    </td>
                @endif
                  </tr>
                  <tr
                  @if ($details->nama == "")
                  style="display: table-row;"
                  @endif
                  >
                  @if ($inputs->nama_input_2 != "")
                  <td>
                    {{ $inputs->title_2 }}
                  </td>
                  <td>
                    <input type="text" class="form-control" name="{{ $inputs->nama_input_2 }}" maxlength="10" onkeypress='validate(event)'  
                    <?php 
                    if($dokumen != ""){
                      foreach ($dokumen as $dokumens) {
                        if ($inputs->id == $dokumens->id_detail_file_input) {
                          echo "value='".$dokumens->tahun."'";
                        }
                      }
                    }else{
                      echo "value=''";
                    }
                    ?>
                    >
                  </td>
                  @endif

                </tr>
                @if ($status_file != "")
                @foreach ($status_file as $statuf)
                @if ($statuf->id_detail_file_input == $inputs->id)
                @php
                if($statuf->status == ""){
                $title_badge = "-";
                $badge_class = "badge badge-secondary";
              }
              if($statuf->status == "sesuai"){
              $title_badge = "Sesuai";
              $badge_class = "badge badge-success";
            }
            if($statuf->status == "tidak_sesuai"){
            $title_badge = "Tidak Sesuai";
            $badge_class = "badge badge-warning";
          }
          @endphp
          <tr
          @if ($details->nama == "")
          style="display: table-row;"
          @endif
          >
          <td></td>
          <td>Status</td>
          <td><span class="{{ $badge_class }}">{{ $title_badge }}</span></td>
        </tr>
        <tr
        @if ($details->nama == "")
        style="display: table-row;"
        @endif
        >
        <td></td>
        <td>Keterangan</td>
        <td>{{ $statuf->keterangan == "" ? '-' : $statuf->keterangan }}</td>
      </tr>
      {{-- <div class="row">
        <div class="col-md-5 line-center">Status</div>
        <div class="col-md-1 line-center">:</div>
        <div class="col-md-5 line-center">
          <span class="{{ $badge_class }}">{{ $title_badge }}</span>
        </div>
      </div>
      <div class="row">
        <div class="col-md-5 line-center">Keterangan</div>
        <div class="col-md-1 line-center">:</div>
        <div class="col-md-5 line-center">
          {{ $statuf->keterangan == "" ? '-' : $statuf->keterangan }}
        </div>
      </div> --}}
      @endif
      @endforeach
      @endif
      @php
      $no_in++;
      @endphp
      @endif
      @endforeach
      @endif
      @endforeach
    </table>
  </div>
  <div class="accor-footer">
    <div class="row">
      <div class="col-md-12">
        {{-- <button type="button" class="btn btn-default1 float-right"  id="btnSubmit_{{ $poins->id }}" >Simpan</button> --}}
        <button type="submit" class="btn btn-default1 float-right" >Simpan</button>
      </div>
    </div>
  </div>
</form>
</div>
</div>    
@endforeach
</div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <a href="{{ url('tambah-peserta-ujian/'.$jadwal->id) }}" class="btn btn-sm btn-default2" style="text-align:center">Selesai</a>
  </div>
</div>
</div>                     
<div class="col-md-3 text-center">
  @include('layout.button_right_kpp')
</div>
</div>
</div>
</div>
</div> 
</div>
</div>
</div>
@endsection

@section('js')
<script>
  function modifyVar(obj, val) {
    obj.valueOf = obj.toSource = obj.toString = function(){ return val; };
  }

  function setToFalse(boolVar) {
    modifyVar(boolVar, false);
  }

  function setToTrue(boolVar) {
    modifyVar(boolVar, true);
  }
  @foreach ($judul_input as $key => $poins)
  @foreach ($detail_input as $details)
  @if ($details->id_judul_input == $poins->id)
  @foreach ($input_form as $inputs)
  @if ($inputs->id_detail_input == $details->id)
  var {{ 'validasi_'.$poins->id.'_'.$details->id.'_'.$inputs->id }} = new Boolean(false);
  var val_{{ $inputs->nama_input_1 }} = new Boolean(true);
  @endif
  @endforeach
  @endif
  @endforeach
  @endforeach
</script>
{{-- <script>
  @foreach ($judul_input as $key => $poins)
  $('#btnSubmit_{{ $poins->id }}').click(function (e) {
    var hasil_{{ $poins->id }} = new Boolean(true);

    @foreach ($detail_input as $details)
    @if ($details->id_judul_input == $poins->id)
    @foreach ($input_form as $num => $inputs)
    @if ($inputs->id_detail_input == $details->id)
          // console.log({{ $inputs->id }});
          // if(validasi_{{ $poins->id }}_{{ $details->id }}_{{ $inputs->id }} == false){
          //   setToFalse(hasil_{{ $poins->id }});
          // }

          var size_{!! $inputs->nama_input_1 !!} = $(".cek_file_{!! $inputs->nama_input_1 !!}")[0].files[0].size;
          var type_{!! $inputs->nama_input_1 !!} = $(".cek_file_{!! $inputs->nama_input_1 !!}")[0].files[0].type;

          if (type_{!! $inputs->nama_input_1 !!} == 'application/pdf') {
            if(size_{!! $inputs->nama_input_1 !!} > 2097152){
              $('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh lebih dari 2MB");
              setToFalse(val_{!! $inputs->nama_input_1 !!});
            }

            if(size_{!! $inputs->nama_input_1 !!} < 102796){
              $('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh kurang dari 100kb");
              setToFalse(val_{!! $inputs->nama_input_1 !!});
            }

            if (102796 < size_{!! $inputs->nama_input_1 !!} && size_{!! $inputs->nama_input_1 !!} < 2097152) {
              $('#errSuratBertugasPbj').html("");
              setToTrue(val_{!! $inputs->nama_input_1 !!});
            }
          } else if (type_{!! $inputs->nama_input_1 !!} == 'image/jpeg') {
            if(size_{!! $inputs->nama_input_1 !!} > 2097152){
              $('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh lebih dari 2MB");
              setToFalse(val_{!! $inputs->nama_input_1 !!});
            }

            if(size_{!! $inputs->nama_input_1 !!} < 102796){
              $('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh kurang dari 100kb");
              setToFalse(val_{!! $inputs->nama_input_1 !!});
            }

            if (102796 < size_{!! $inputs->nama_input_1 !!} && size_{!! $inputs->nama_input_1 !!} < 2097152) {
              $('#err{!! $inputs->nama_input_1 !!}').html("");
              setToTrue(val_{!! $inputs->nama_input_1 !!});
            }
          } else {
            $('#err{!! $inputs->nama_input_1 !!}').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(val_{!! $inputs->nama_input_1 !!});
          }
          @endif
          @php
          $cek = '';
          $jum_input = count($input_form);

          $cek .= $inputs->nama_input_1.' == true';
          if ($num != count($input_form)) {
            $cek .= ' && ';
          }
          @endphp
          @endforeach
          @endif
          @endforeach
          e.preventDefault();
          if (val_{{ $inputs->nama_input_1 }} == false){
            alert('Terdapat kesalahan/kekurangan pada berkas yang diunggah.\nSilakan periksa kembali berkas yang diunggah.');
          }else{
            $('#pesertaForm_{{ $poins->id }}').submit();
          }
        });
  @endforeach
</script> --}}
<script>


  $(document).ready(function() {
    //Fixing jQuery Click Events for the iPad
    var ua = navigator.userAgent,
    event = (ua.match(/iPad/i)) ? "touchstart" : "click";
    if ($('.table').length > 0) {
      $('.table .header').on(event, function() {
        $(this).toggleClass("active", "").nextUntil('.header').css('display', function(i, v) {
          return this.style.display === 'table-row' ? 'none' : 'table-row';
        });
      });
    }
  });
</script> 
<script>
  console.log('input change');
  @foreach ($judul_input as $key => $poins)
  @foreach ($detail_input as $details)
  @if ($details->id_judul_input == $poins->id)
  @foreach ($input_form as $num => $inputs)
  @if ($inputs->id_detail_input == $details->id)
  $('#cek_file_{!! $inputs->nama_input_1 !!}').bind('change', function() {
    var size_{!! $inputs->nama_input_1 !!} = this.files[0].size;
    var type_{!! $inputs->nama_input_1 !!} = this.files[0].type;

    if (type_{!! $inputs->nama_input_1 !!} == 'application/pdf') {
      if(size_{!! $inputs->nama_input_1 !!} > 2097152){
        $('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh lebih dari 2MB");
        setToFalse(val_{!! $inputs->nama_input_1 !!});
      }

      if(size_{!! $inputs->nama_input_1 !!} < 102796){
        $('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh kurang dari 100kb");
        setToFalse(val_{!! $inputs->nama_input_1 !!});
      }

      if (102796 < size_{!! $inputs->nama_input_1 !!} && size_{!! $inputs->nama_input_1 !!} < 2097152) {
        $('#errSuratBertugasPbj').html("");
        setToTrue(val_{!! $inputs->nama_input_1 !!});
      }
    } else if (type_{!! $inputs->nama_input_1 !!} == 'image/jpeg') {
      if(size_{!! $inputs->nama_input_1 !!} > 2097152){
        $('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh lebih dari 2MB");
        setToFalse(val_{!! $inputs->nama_input_1 !!});
      }

      if(size_{!! $inputs->nama_input_1 !!} < 102796){
        $('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh kurang dari 100kb");
        setToFalse(val_{!! $inputs->nama_input_1 !!});
      }

      if (102796 < size_{!! $inputs->nama_input_1 !!} && size_{!! $inputs->nama_input_1 !!} < 2097152) {
        $('#err{!! $inputs->nama_input_1 !!}').html("");
        setToTrue(val_{!! $inputs->nama_input_1 !!});
      }
    } else {
      $('#err{!! $inputs->nama_input_1 !!}').html("type file tidak boleh selain jpg,jpeg & pdf");
      setToFalse(val_{!! $inputs->nama_input_1 !!});
    }
  });
  @php
  $cek = '';
  $jum_input = count($input_form);

  $cek .= 'val_'.$inputs->nama_input_1.' == true';
  if ($num != count($input_form)) {
    $cek .= ' && ';
  }
  @endphp
  @endif
  @endforeach
  @endif
  @endforeach
  @endforeach
</script>
<script type="text/javascript">
  
  @foreach ($judul_input as $key => $poins)
    @foreach ($detail_input as $details)
      @if ($details->id_judul_input == $poins->id)
        @foreach ($input_form as $num => $inputs)
          @if ($inputs->id_detail_input == $details->id)
            document.getElementById("cek_file_{!! $inputs->nama_input_1 !!}").onchange = function () {
            document.getElementById("uploadFile_{!! $inputs->nama_input_1 !!}").value = this.value.replace("C:\\fakepath\\", "");
            };

          @endif
        @endforeach
      @endif
    @endforeach
  @endforeach
</script>
<script type="text/javascript">
  
  @foreach ($judul_input as $key => $poins)
    @foreach ($detail_input as $details)
      @if ($details->id_judul_input == $poins->id)
        @foreach ($input_form as $num => $inputs)
          @if ($inputs->id_detail_input == $details->id)
            document.getElementById("cek_file_{!! $inputs->nama_input_1 !!}").onchange = function () {
            document.getElementById("uploadFile_{!! $inputs->nama_input_1 !!}").value = this.value.replace("C:\\fakepath\\", "");
            };

          @endif
        @endforeach
      @endif
    @endforeach
  @endforeach
</script>
<script>
  @foreach ($judul_input as $key => $poins)
    @foreach ($detail_input as $details)
      @if ($details->id_judul_input == $poins->id)
        @foreach ($input_form as $num => $inputs)
          @if ($inputs->id_detail_input == $details->id)
            @if ($inputs->nama_input_1 != "")
              @if ($dokumen != "")
                @if ($cek_upload == true)
                  @if ($inputs->title_1 == "Dokumen Hasil Pekerjaan")
                      $(document).ready(function() {
                      $('.fileLabel').html("Harap Unggah SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi terlebih dahulu");
                      });
                  @endif
                @endif
              @endif
            @endif
          @endif
        @endforeach
      @endif
    @endforeach
  @endforeach

</script>
@endsection