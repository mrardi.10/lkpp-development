@extends('layout.app2')

@section('title')
Biodata
@endsection

@section('css')
<style>
  body{
    background-color: whitesmoke;
  }
  .box-container{
    background-color: white;
    border-radius: 5px;
    margin: 2% 10% 2% 5%;
    padding: 2%;
  }

  .shadow{
    -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 0px 10px 2px rgba(0,0,0,0.85);
    max-width: 250px;
    max-height: 30px;
    line-height: 15px;
    width: 80%;
    margin: 5px;
  }

  .box-tab{
    -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 3px 7px 0px rgba(0,0,0,0.85);
    border-radius: 5px;padding: 10px;
  }

  div.dataTables_wrapper div.dataTables_info{
    display: none;
  }

  div.dataTables_wrapper .row.col-sm-12{
    width: 120px;
  }

  p{
    font-weight: 500;
  }

  b{
    font-weight: 500;
  }
  .form-control{
    margin :5px;
    -webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    -moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    border-radius: 5px;
  }

  textarea{
    margin :5px;
    -webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    -moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    border-radius: 5px;
  }

  button{
    width: 120px;
  }

  .form-control{
    height: 30px;
  }

  
</style>
@endsection

@section('content')
<div class="main-page">
  <div class="container box-container">
    <div class="row">
      <h5> Pengusulan E-Formasi</h5>
    </div>
    <div class="row">
      <hr style="width: 70%;margin-left: 5px;">
    </div>
    <div class="row" style="margin-right: -50px;">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-9" style="">
            <div class="row">
              <div class="col-md-12">
                <p>Perhatikan petunjuk pengisian dan unggah data peserta dibawah ini sebelum melanjutkan<br>ke langkah berikutnya.</p>
                <p style="padding: 30px 60px;margin-top: -30px;">1. lorem ipsum<br>
                  2. lorem ipsum<br>
                  3. lorem ipsum<br>
                  4. lorem ipsum<br>
                  5. lorem ipsum<br></p>
                  <p>Perhatikan petunjuk pengisian dan unggah data peserta dibawah ini sebelum melanjutkan<br>ke langkah berikutnya.</p> 
                  <div class="row">
                    <hr style="width: 70%;margin-left: 5px;">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="rows"><a href="{{ url('biodata-admin') }}" class="btn btn-default shadow">Biodata admin PPK</a></div>
              <div class="rows"><a href="{{ url('pengusulan-eformasi') }}" class="btn btn-default shadow">Pengusulan e-Formasi</a></div>
              <div class="rows"><a href="{{ url('data-peserta-ppk') }}" class="btn btn-default shadow">Data Peserta</a></div>
              <div class="rows"><a href="{{ url('daftar-reguler-lkpp') }}" class="btn btn-default shadow">Jadwal & Daftar LKPP</a></div>
              <div class="rows"><a href="{{ url('daftar-instansi') }}" class="btn btn-default shadow">Jadwal & Daftar Instansi</a></div>
              <div class="rows"><a href="#" class="btn btn-default shadow">Ganti Password</a></div>
              <div class="rows"><a href="#" class="btn btn-default shadow" style="color: red;">LogOut</a></div>
            </div>
          </div>
        </div> 
      </div>
    </div>
    @endsection