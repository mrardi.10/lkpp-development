@extends('layout.app')
@section('title')
	@if ($action == 'add')
		Tambah Admin LKPP
	@elseif ($action == 'edit')
		Ubah Admin LKPP
	@endif
@endsection
@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }
    select.selectd{
        margin-bottom: 5px;
    }
</style>
@endsection
@section('content')
	@if ($action == 'add')
	<form action="" method="post">
	@csrf
		<div class="main-box">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3>Tambah Admin LKPP</h3><hr>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-xs-10">
						Nama
					</div>
					<div class="col-md-1 col-xs-1">:</div>
					<div class="col-md-5 col-xs-12">
						<div class="form-group">
							<input type='text' class="form-control" name="nama" value="{{ old('nama') }}" required/>
						</div>
						<span class="errmsg">{{ $errors->first('nama') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-xs-10">
						Email
					</div>
					<div class="col-md-1 col-xs-1">:</div>
					<div class="col-md-5 col-xs-12">
						<div class="form-group">
							<input type='email' class="form-control" name="email" value="{{ old('email') }}" required />
						</div>
						<span class="errmsg">{{ $errors->first('email') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-xs-10">
						Password (isi dengan password)
					</div>
					<div class="col-md-1 col-xs-1">:</div>
					<div class="col-md-5 col-xs-12">
						<div class="form-group">
							<input type='password' class="form-control" name="password" value="{{ old('password') }}" id="password" required/>
							<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword()"></span>
						</div>
						<span class="errmsg">{{ $errors->first('password') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-xs-10">
						Ulangi Password (isi dengan password)
					</div>
					<div class="col-md-1 col-xs-1">:</div>
					<div class="col-md-5 col-xs-12">
						<div class="form-group">
							<input type='password' class="form-control" name="ulang_password" value="{{ old('ulang_password') }}" id="u_password" required/>
							<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword2()"></span>
						</div>
						<span class="errmsg">{{ $errors->first('ulang_password') }}</span>
					</div>
				</div>
				<div class="row" style="margin-bottom:5px">
					<div class="col-md-3 col-xs-10">
						Unit Kerja
					</div>
					<div class="col-md-1 col-xs-1">:</div>
					<div class="col-md-5 col-xs-12">
						{!! Form::select('unit_kerja', $unit_kerja , null, ['placeholder' => 'Pilih Unit Kerja', 'class' => 'form-control unit-kerja', 'id' => 'select-single selectd']); !!}
						<span class="errmsg">{{ $errors->first('unit_kerja') }}</span>
					</div>
				</div>
				<div class="row" style="margin-bottom:5px">
					<div class="col-md-3 col-xs-10">
						Role Admin
					</div>
					<div class="col-md-1 col-xs-1">:</div>
					<div class="col-md-5 col-xs-12">
						<select name="role_admin" id="metode" class="form-control role" required>
							<option value="" disabled selected>Pilih Role Admin</option>							
							<option value="bangprof">Admin Bangprof</option>
							<option value="dsp">Admin DSP</option>
						</select>
						<span class="errmsg">{{ $errors->first('role_admin') }}</span>
					</div>
				</div>
				<div class="row" style="margin-bottom:5px">
					<div class="col-md-3 col-xs-10">
						Status
					</div>
					<div class="col-md-1 col-xs-1">:</div>
					<div class="col-md-5 col-xs-12">
						{!! Form::select('status_admin', array('aktif' => 'Aktif', 'tidak_Aktif' => 'Tidak Aktif') , null, ['placeholder' => 'Pilih Status', 'class' => 'form-control selectd', 'id' => 'select-single']); !!}
						<span class="errmsg">{{ $errors->first('status_admin') }}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-9" style="text-align: right">
						<button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
						<button type="submit" class="btn btn-sm btn-default1">Tambah</button>
					</div>
				</div>
			</div>
		</div>
	</form>
@endif

{{-- Edit --}}
@if ($action == 'edit')
	<form action="" method="post">
	@csrf
	<div class="main-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>Ubah Admin LKPP</h3><hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Nama
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='text' class="form-control" name="nama" value="{{ $data->name }}" required/>
					</div>
					<span class="errmsg">{{ $errors->first('nama') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Email
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='email' class="form-control" name="email" value="{{ $data->email }}" required />
					</div>
					<span class="errmsg">{{ $errors->first('email') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Password Baru (isi dengan password baru)
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='password' class="form-control" name="password" value="{{ old('password') }}" placeholder="input jika ingin ganti password" id="password"/>
						<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword()"></span>
					</div>
					<span class="errmsg">{{ $errors->first('password') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Ulangi Password (isi dengan password baru)
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='password' class="form-control" name="ulang_password" value="{{ old('ulang_password') }}" placeholder="input jika ingin ganti password" id="u_password"/>
						<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword2()"></span>
					</div>
					<span class="errmsg">{{ $errors->first('ulang_password') }}</span>
				</div>
			</div>
			<input type="hidden" name="old_password" value="{{ $data->password }}">
			<div class="row" style="margin-bottom:5px">
				<div class="col-md-3 col-xs-10">
					Unit Kerja
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					{!! Form::select('unit_kerja', $unit_kerja , $data->unit_kerja, ['placeholder' => 'Pilih Unit Kerja', 'class' => 'form-control unit-kerja', 'id' => 'select-singe']); !!}
					<span class="errmsg">{{ $errors->first('unit_kerja') }}</span>
				</div>
			</div>
			<div class="row" style="margin-bottom:5px">
				<div class="col-md-3 col-xs-10">
					Role Admin
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					{!! Form::select('role_admin', array('bangprof' => 'Admin Bangprof', 'dsp' => 'Admin DSP') , $data->role, ['placeholder' => 'Pilih Role Admin', 'class' => 'form-control role', 'id' => 'select-single']); !!}
					<span class="errmsg">{{ $errors->first('role_admin') }}</span>
				</div>
			</div>
			<div class="row" style="margin-bottom:5px">
				<div class="col-md-3 col-xs-10">
					Status
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					{!! Form::select('status_admin', array('aktif' => 'Aktif', 'tidak_Aktif' => 'Tidak Aktif') , $data->status_admin, ['placeholder' => 'Pilih Status', 'class' => 'form-control', 'id' => 'select-single']); !!}
					<span class="errmsg">{{ $errors->first('status_admin') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9" style="text-align: right">
					<button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
					<button type="submit" class="btn btn-sm btn-default1">Ubah</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endif
@endsection
@section('js')
<script type="text/javascript">
	$('.unit-kerja').on('change', function() {
		var select = this.value;
		if(select == '1'){
			$('.role').val('bangprof');
		}
		
		if(select == '2'){
			$('.role').val('dsp');
		}
	});

	$('.role').on('change', function() {
		var select = this.value;
		if(select == 'bangprof'){
			$('.unit-kerja').val('1');
		}
		
		if(select == 'dsp'){
			$('.unit-kerja').val('2');
		}
	});

	function showPassword() {
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
	
	function showPassword() {
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}

	function showPassword2() {
		var x = document.getElementById("u_password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
</script>
@endsection