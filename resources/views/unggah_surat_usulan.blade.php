@extends('layout.app2')

@section('title')
Unggah Surat Usulan
@endsection

@section('css')
<style>
  body{
    background-color: whitesmoke;
  }
  .main-page{
    margin-top: 20px;
  }

  .box-container{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    background-color: white;
    border-radius: 5px;
    padding: 3%;
  }

  .shadow{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    max-width: 250px;
    line-height: 15px;
    width: 100%;
    margin: 5px;
  }

  .custom-file{
    margin: 5px;
  }
  .fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
  }
  .fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
  }

  .textFile::-webkit-input-placeholder{
    font-size: 15px;
  }
  .textFile::-moz-input-placeholder{
    font-size: 15px;
  }
  .textFile::-ms-input-placeholder{
    font-size: 15px;
  }
  div.dataTables_wrapper div.dataTables_info{
    display: none;
  }

  div.dataTables_wrapper .row.col-sm-12{
    width: 120px;
  }


  p{
    font-weight: 500;
  }

  b{
    font-weight: 500;
  }

  .row a.btn{
    text-align: left;
    font-weight: 600;
    font-size: small;
  }

  .btn-default1{
    background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
    color: white;
    font-weight: 600;
    width: 100px; 
    margin: 10px;
    -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }


  .btn-default2{
    background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
    color: black;
    font-weight: 600;
    width: 100px; 
    margin: 10px;
    -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }

  .btn-default3{
    background: #fff;
    color: #000;
    font-weight: 500;
    width: 100px;
  }


  .row-input{
    padding-bottom: 10px;
  }

  .page div.verifikasi{
    display: none;
  }

  .btn-area{
    text-align: right;
  }

  .clickable-row:hover{
    cursor: pointer;
  }

  .checkmark {
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #00aaaa;
  }

  .modal .modal-dialog{
    max-width: 800px !important;
  }

  .line-center{
    line-height: 2.5;
  }

  span.berkas{
    font-size: 10px;
    font-weight: 600;
  }

  span.berkas label{
    margin-bottom: 0px !important;
  }
  [data-toggle="collapse"] .fa:before {  
    content: "\f139";
  }

  [data-toggle="collapse"].collapsed .fa:before {
    content: "\f13a";
  }

  .alert-row{
    margin-top: 35px;
  }

  .accor-body{
    /* margin: 0px 10px; */
  }
  ol.ol-sub-a{
    padding-inline-start: 18px !important;
  }

  .btn-link{
    color: #000 !important;
  }

  .btn-judul{
    width: 100%;
    text-align: left;
    background: #f8f8f8;
    -webkit-box-shadow: 0px 3px 4px -3px #000000; 
    box-shadow: 0px 3px 4px -3px #000000;
  }

  .collapse-body{
    /* padding: 15px; */
    border: 1px solid #b3aeae;
  }

  .collapse-all{
    margin: 10px 0px;
  }

  span.err{
    color: red;
    position: relative;
  }

  .lis{
    padding: 10px 30px;
    border-bottom: 1px solid #c3bdbd;
    /* background: #f1f1f1; */
    margin-bottom: 10px;
  }

  .bg-active{
    background: #f8f8f8;
  }

  .main-page img{
    width: 80px;
  }

  .table>tbody>tr.active>td,
  .table>tbody>tr.active>th,
  .table>tbody>tr>td.active,
  .table>tbody>tr>th.active,
  .table>tfoot>tr.active>td,
  .table>tfoot>tr.active>th,
  .table>tfoot>tr>td.active,
  .table>tfoot>tr>th.active,
  .table>thead>tr.active>td,
  .table>thead>tr.active>th,
  .table>thead>tr>td.active,
  .table>thead>tr>th.active {
    background-color: #fff;
  }

  .table-bordered > tbody > tr > td,
  .table-bordered > tbody > tr > th,
  .table-bordered > tfoot > tr > td,
  .table-bordered > tfoot > tr > th,
  .table-bordered > thead > tr > td,
  .table-bordered > thead > tr > th {
    border-color: #e4e5e7;
  }

  .table tr.header {
    /* font-weight: bold; */
    /* background-color: whitesmoke; */
    cursor: pointer;
    -webkit-user-select: none;
    /* Chrome all / Safari all */
    -moz-user-select: none;
    /* Firefox all */
    -ms-user-select: none;
    /* IE 10+ */
    user-select: none;
    /* Likely future */
  }

  .table tr:not(.header) {
    display: none;
  }

  .table .header td:after {
    content: "\002b";
    position: relative;
    top: 1px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: 400;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    float: right;
    color: #999;
    text-align: center;
    padding: 3px;
    transition: transform .25s linear;
    -webkit-transition: -webkit-transform .25s linear;
  }

  .table .header.active td:after {
    content: "\2212";
  }

  .table td{
    position: relative;
  }
  .error{
    color: red;
  }

</style>
@endsection

@section('content')
{{-- sdlajkljs --}}
{{-- sdasd --}}
{{-- aasa --}}
{{-- sadas --}}
{{-- asdasd --}}
{{-- dsad --}}
<div class="main-page">
  <div class="container">
    <div class="row box-container">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-12" style="">
            <div class="row">
              <div class="col-md-9">
                <h5>Unggah Surat Usulan Mengikuti Inpassing 
                </h5>
                {{-- {{ $data }} --}}
                <hr>
                @if (session('msg'))
                @if (session('msg') == "berhasil")
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Berhasil simpan data</strong>
                </div> 
                @else
                <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Gagal simpan data</strong>
                </div> 
                @endif
                @endif
                <div id="notifMin">

                </div>
              </div>
              <div class="col-md-3" style="text-align:right">
                <i class="fa fa-bell"></i>
              </div>
              <div class="col-md-9 page">
                {{-- <form action="{{ url('store-dokumen-ujian') }}" method="post" enctype="multipart/form-data" id="store"> --}}

                  @foreach($datapeserta as $pesertas)
                  <div class="row">
                    <div class="col-md-3">
                      Nama
                    </div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-8">
                      {{ $pesertas->nama }}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      Pangkat/Gol.
                    </div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-8">{{ $pesertas->jabatan }}</div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      Jenjang
                    </div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-8">
                      {{ ucwords($pesertas->jenjang) }}
                    </div>
                  </div>
                  <div class="row alert-row">
                    <div class="col-md-12">
                     <div class="alert alert-primary" role="alert">
                      <strong style="text-transform: uppercase;">Dokumen yang diunggah min. 100KB max. 2MB, dengan format PDF, JPG, JPEG</strong>
                    </div>
                  </div>
                </div>
                <form action="" method="post" enctype="multipart/form-data">
                  @csrf
                  @foreach($jadwal as $jadwals)
                  <input type="hidden" name="id_jadwal" value="{{ $jadwals->id }}">
                  <input type="hidden" name="metode" value="{{ $jadwals->metode }}">
                  @endforeach
                  <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-4">Surat Usulan Mengikuti Inpassing</div>
                      <div class="col-md-1">:</div>
                      <div class="col-md-6">
                        <input id="nama_surat" class="form-control textFile" placeholder="Tidak ada Dokumen yang dipilih" disabled="disabled" />
                        <div class="fileUpload btn btn-primary">
                          <span>Pilih Dokumen</span>
                          <input type="file"  name="surat_usulan_peserta" id="surat_usulan"  class="upload" accept="application/pdf, .jpg, .jpeg, image/jpg, image/jpeg"/>
                        </div>
                        <br>
                        <span id="err" class="error">{{ $errors->first('surat_usulan_peserta') }}</span>
                      </div>
                      
                    </div>
                    @foreach($dokumen as $dokumens)
                    <div class="row" style="margin-top:10px">
                      <div class="col-md-4">Berkas Sebelumnya</div>
                      <div class="col-md-1 line-center">:</div>
                      <div class="col-md-6" style="line-height: 3">
                       @if ($dokumens->file == "")
                       - 
                       @else
                       @php
                       $file = explode('.',$dokumens->file);
                       @endphp

                       @if ($file[1] == 'pdf' || $file[1] == 'PDF')
                       <a href="{{ url('priview-file')."/surat_usulan_inpassing/".$dokumens->file }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:20px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                       @else
                       <a href="{{ url('priview-file')."/surat_usulan_inpassing/".$dokumens->file }}" target="_blank"><img src="{{ asset('storage/data/surat_usulan_inpassing')."/".$dokumens->file }}" class="img-rounded"></a>
                       @endif
                       @endif

                     </div>
                   </div>
                   @endforeach
                   <div class="row" style="margin-top:2%;">
                    <div class="col-md-4">No. Surat Usulan Mengikuti Inpassing</div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-6">
                      
                      <input class="form-control" name="no_surat_usulan_peserta" type="text" value=@foreach($dokumen as $dokumens) {{ $dokumens->no_surat_usulan_peserta == '' ? '' : $dokumens->no_surat_usulan_peserta }} @endforeach >
                      <br>  
                      <span id="err" class="error">{{ $errors->first('no_surat_usulan_peserta') }}</span>
                      
                    </div>
                  </div>
                  <div class="row" style="margin-top: 3%;">
                    <div class="col-md-12">
                      <button type="submit" class="btn btn-md btn-success" style="text-align:center">Simpan</button>&nbsp;

                      @foreach($jadwal as $jadwals)
                      @foreach($dokumen as $dokumens)
                      @if ($dokumens->file == "")
                      @else
                      <a href="{{ url('tambah-peserta-ujian/'.$jadwals->id)}}"><button type="button" class="btn btn-md btn-primary" style="text-align:center" id="selesai">Selesai</button></a>
                      @endif
                      @endforeach
                      @endforeach
                    </div>
                  </div>
                </form>
                @endforeach
              </div>
            </div>                     
            <div class="col-md-3 text-center">
              @include('layout.button_right_kpp')
            </div>
          </div>
        </div>
      </div>
    </div> 

    @endsection

    @section('js')
    <script>


      function modifyVar(obj, val) {
        obj.valueOf = obj.toSource = obj.toString = function(){ return val; };
      }

      function setToFalse(boolVar) {
        modifyVar(boolVar, false);
      }

      function setToTrue(boolVar) {
        modifyVar(boolVar, true);
      }

      var val_surat_usulan = new Boolean(true);
      var val_no_surat_usulan = new Boolean(true);

    </script>
{{-- <script>
  @foreach ($judul_input as $key => $poins)
  $('#btnSubmit_{{ $poins->id }}').click(function (e) {
    var hasil_{{ $poins->id }} = new Boolean(true);

    @foreach ($detail_input as $details)
    @if ($details->id_judul_input == $poins->id)
    @foreach ($input_form as $num => $inputs)
    @if ($inputs->id_detail_input == $details->id)
          // console.log({{ $inputs->id }});
          // if(validasi_{{ $poins->id }}_{{ $details->id }}_{{ $inputs->id }} == false){
          //   setToFalse(hasil_{{ $poins->id }});
          // }

          var size_ = $(".cek_file_")[0].files[0].size;
          var type_ = $(".cek_file_")[0].files[0].type;

          if (type_ == 'application/pdf') {
            if(size_ > 2097152){
              $('#err').html("file tidak boleh lebih dari 2MB");
              setToFalse(val_surat_usulan);
            }

            if(size_ < 102796){
              $('#err').html("file tidak boleh kurang dari 100kb");
              setToFalse(val_surat_usulan);
            }

            if (102796 < size_ && size_ < 2097152) {
              $('#errSuratBertugasPbj').html("");
              setToTrue(val_surat_usulan);
            }
          } else if (type_ == 'image/jpeg') {
            if(size_ > 2097152){
              $('#err').html("file tidak boleh lebih dari 2MB");
              setToFalse(val_surat_usulan);
            }

            if(size_ < 102796){
              $('#err').html("file tidak boleh kurang dari 100kb");
              setToFalse(val_surat_usulan);
            }

            if (102796 < size_ && size_ < 2097152) {
              $('#err').html("");
              setToTrue(val_surat_usulan);
            }
          } else {
            $('#err').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(val_surat_usulan);
          }
          @endif
          @php
          $cek = '';
          $jum_input = count($input_form);

          $cek .= $inputs->nama_input_1.' == true';
          if ($num != count($input_form)) {
            $cek .= ' && ';
          }
          @endphp
          @endforeach
          @endif
          @endforeach
          e.preventDefault();
          if (val_surat_usulan{{ $inputs->nama_input_1 }} == false){
            alert('Terdapat kesalahan/kekurangan pada berkas yang diunggah.\nSilakan periksa kembali berkas yang diunggah.');
          }else{
            $('#pesertaForm_{{ $poins->id }}').submit();
          }
        });
  @endforeach
</script> --}}

<script>
  console.log('input change');
  $('#surat_usulan').bind('change', function() {
    var size_surat_usulan = this.files[0].size;
    var type_surat_usulan = this.files[0].type;

    if (type_surat_usulan == 'application/pdf') {
      if(size_surat_usulan > 2097152){
        $('#err').html("Ukuran dokumen lebih dari 2MB");
        setToFalse(val_surat_usulan);
      }

      if(size_surat_usulan < 102796){
        $('#err').html("Ukuran dokumen kurang dari 100KB");
        setToFalse(val_surat_usulan);
      }

      if (102796 < size_surat_usulan && size_surat_usulan < 2097152) {
        $('#errSuratBertugasPbj').html("");
        setToTrue(val_surat_usulan);
      }
    } else if (type_surat_usulan == 'image/jpeg') {
      if(size_surat_usulan > 2097152){
        $('#err').html("Ukuran dokumen lebih dari 2MB");
        setToFalse(val_surat_usulan);
      }

      if(size_surat_usulan < 102796){
        $('#err').html("Ukuran dokumen kurang dari 100KB");
        setToFalse(val_surat_usulan);
      }

      if (102796 < size_surat_usulan && size_surat_usulan < 2097152) {
        $('#err').html("");
        setToTrue(val_surat_usulan);
      }
    } else {
      $('#err').html("type file tidak boleh selain jpg, jpeg & pdf");
      setToFalse(val_surat_usulan);
    }
  });
</script>

{{-- <script type="text/javascript">
   @foreach ($judul_input as $key => $poins)
    @foreach ($detail_input as $details)
      @if ($details->id_judul_input == $poins->id)
        @foreach ($input_form as $num => $inputs)
          @if ($inputs->id_detail_input == $details->id)

  window.addEventListener('load', function() {
  document.querySelector('#cek_file_').addEventListener('change', function() {
      if (this.files && this.files[0]) {
          var img = document.querySelector('img');  // $('img')[0]
          img.src = URL.createObjectURL(this.files[0]); // set src to file url
          img.onload = imageIsLoaded; // optional onload event listener
      }
  });
});

          @endif
        @endforeach
      @endif
    @endforeach
  @endforeach



</script> --}}
<script type="text/javascript">
  $('#surat_usulan').change(function(e){
    var fileName = e.target.files[0].name;
    $('#nama_surat').val(fileName);
  });

</script>

@endsection