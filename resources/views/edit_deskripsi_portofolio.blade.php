@extends('layout.app')

@section('title')
    Edit Deskripsi Portofolio 
@endsection

@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }
</style>
@endsection

@section('content')
<form action="" method="post">
    @csrf
    <div class="main-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Edit Deskripsi Portofolio</h3>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 col-xs-10">
                    Deskripsi
                </div>
                <div class="col-md-1 col-xs-1">:</div>
                <div class="col-md-8 col-xs-12">
                    <div class="form-group">
                        <textarea name="text" id="editor">
                            @foreach($data as $datas)
                                @php 
                                    echo $datas->keterangan;
                                @endphp
                            @endforeach
                        </textarea>
                    </div>
                    <span class="errmsg">{{ $errors->first('id') }}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9" style="text-align: right">
                    <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
                    <button type="submit" class="btn btn-sm btn-default1">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    </form>    
@endsection

@section('js')
<script>
ClassicEditor
.create( document.querySelector( '#editor' ) )
.then( editor => {
    console.log( editor );
} )
.catch( error => {
    console.error( error );
} );
</script>
@endsection