@extends('layout.app2')
@section('title')
	Statistik Pelaksanaan Ujian
@stop
@section('css')
<style>
	body{
		width: 100%;
	}

	.bg-top{
		width: 100%;
		background-image: url('{{ asset('assets/img/back.jpg') }}'); /* The image used */
		background-color: #cccccc; /* Used if the image is unavailable */
		height: 300px; /* You must set a specified height */
		background-position: center; /* Center the image */
		background-repeat: no-repeat; /* Do not repeat the image */
		background-size: cover; /* Resize the background image to cover the entire container */
	}

	.info-txt{ 
		padding: 50px;
	}

	.info-txt hr{
		border-top: 1px solid rgba(0, 0, 0, 0.43);
	}

	.txt-tp{
		font-weight: 600;
	}

	.txt-btm{
		font-size: 22px;
	}

	.lbl-form{
		width: 100%;
		font-weight: 600;
		color: gray;
	}

	.card-head{
		color: white;
		padding: 10px 20px 10px 20px;
		background: #ff4141;
		margin: -1px;
	}

	.card-head h3{
		font-weight: 600;
	}

	.card-body{
		padding-bottom: 10px;
	}

	.card-bottom{
		flex: 1 1 auto;
		padding: 0px 20px 10px 20px;
		margin: 35px 0px;
	}

	.card{
		margin-top: -110px;
		-webkit-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
		-moz-box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
		box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.32);
	}

	.card .form-control{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
	}

	.btn-log{
		background: #ff4141;
		font-weight: 600;
		color: white;
		width: 100px;
	}
	
	.btn-daf{
		background: slategray;
		font-weight: 600;
		color: white;
		width: 100px;
	}

	.p-daf{
		margin: 15px 0px;
	}

	@media(min-width: 320px) and (max-width: 768px) {
		.card{
			margin-top: 10px;
		} 
	}
	
	.running{
		height: 50px;
		background: #3a394e;
	}

	#type {
		margin-bottom: 15px;
		font-size: 18px;
		font-weight: 200;
		color: #ffffff;
	}
	
	@media screen and (min-width: 768px) {
		#type {
			font-size: 23px;
		}
	}

	.txt-running{
		line-height: 2;
	}

	span.badge{
		vertical-align: -webkit-baseline-middle;
		width: 100%;
		font-size: 16px;
		font-weight: 600;
		color: #3a394e;
	}

	.submit-btn{
		width: 100%;
		text-align: right;
	}
  
	.submit-btn button{
		margin: 10px;
	}

	ul {
		list-style-type: none;
	}
	
	h4{
		color: #fff;
	}

	.bg-merah{
		background: #cd004d !important;
	}
	.bg-oren{
		background: #f37020 !important;
	}
	.bg-kuning{
		background: #fcb712 !important;
	}
	.bg-hijau{
		background: #0cb14d !important;
	}
	.bg-biru{
		background: #008ad2 !important;
	}
	.bg-ungu{
		background: #6460ac !important;
	}
</style>
@endsection
@section('content')
<div class="bg-top"></div>
<div class="running">
	<div class="row txt-running container-fluid">
		<div class="col-md-1" style="line-height: 2.5;">
			<span class="badge badge-warning" style="margin-left: 15px;"><i class="fa fa-info"></i> Info</span>
		</div>
		<div class="col-md-11">
			<marquee direction="left" scrollamount="4" behavior="scroll"  onmouseover="this.stop()" width="100%"  onmouseout="this.start()" loop="infinite" >
				<div id="type">
				@foreach($text as $texts)
					{{$texts->text }} <b style="color: #ff4141">||</b>  
				@endforeach
				</div>
			</marquee>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12 col-md-12 info-txt">
			<h5 class="txt-tp">Statistik Pelaksanaan Ujian</h5><hr>
			<p class="txt-btm">{{ $statistik->text }}</p>
		</div>
		<div class="col-md-4 info-txt" style="margin-top: 3%;">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-merah">
							<h4>Jumlah Instansi Pengusul</h4>
						</div>
						<div class="card-body">
							<canvas id="CHbar"></canvas>
							<hr>
							<span class="badge" style="background: #cd004d;color: #fff"> <div id="jumlah_menteri">  </div></span>
							<span class="badge" style="background: #bf0249;color: #fff"><div id="jumlah_provinsi">   </div></span>
							<span class="badge" style="background: #99053c;color: #fff"><div id="jumlah_kabupaten">   </div></span>
							<span class="badge" style="background: #750730;color: #fff"><div id="jumlah_kota">   </div></span>
							<span class="badge" style="background: #4f0722;color: #fff"><div id="jumlah_instansi">   </div></span>
						</div>
					</div>
				</div>
			</div>    
		</div>
		<div class="col-md-4 info-txt" style="margin-top: 3%;">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-oren">
							<h4>Jumlah Pengusulan Formasi Inpassing yang Disetujui</h4>
						</div>
						<div class="card-body">
							<canvas id="CHbar5"></canvas>
							<hr>
							<span class="badge" style="background: #f37020;color: #fff"><div id="jumlah_setuju_pertama">   </div></span>
							<span class="badge" style="background: #d6611a;color: #fff"> <div id="jumlah_setuju_muda">  </div></span>
							<span class="badge" style="background: #bf5717;color: #fff"><div id="jumlah_setuju_madya">   </div></span>
							<span class="badge" style="background: #a34810;color: #fff"><div id="jumlah_setuju_peserta">   </div></span>
						</div>
					</div>
				</div>
			</div>    
		</div>
		<div class="col-md-4 info-txt" style="margin-top: 3%;">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-kuning">
							<h4>Jumlah Peserta</h4>
						</div>
						<div class="card-body">
							<canvas id="CHbar2"></canvas>
							<hr>
							<span class="badge" style="background: #fcb712;color: #fff"><div id="jumlah_pertama">   </div></span>
							<span class="badge" style="background: #e3a510;color: #fff"> <div id="jumlah_muda">  </div></span>
							<span class="badge" style="background: #c7910e;color: #fff"><div id="jumlah_madya">   </div></span>
							<span class="badge" style="background: #b5840e;color: #fff"><div id="jumlah_peserta">   </div></span>
							
						</div>
					</div>
				</div>
			</div>     
		</div>
		<div class="col-md-4 info-txt" style="margin-top: 3%;">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-hijau">
							<h4>Jumlah Peserta Memenuhi Persyaratan</h4>
						</div>
						<div class="card-body">
							<canvas id="CHbar6"></canvas>
							<hr>
							<span class="badge" style="background: #0cb14d;color: #fff"><div id="jumlah_pertama_lengkap">   </div></span>
							<span class="badge" style="background: #0b9943;color: #fff"> <div id="jumlah_muda_lengkap">  </div></span>
							<span class="badge" style="background: #0a7a36;color: #fff"><div id="jumlah_madya_lengkap">   </div></span>
							<span class="badge" style="background: #095928;color: #fff"><div id="jumlah_peserta_lengkap">   </div></span>
							
						</div>
					</div>
				</div>
			</div>     
		</div>
		<div class="col-md-4 info-txt" style="margin-top: 3%;">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-biru">
							<h4>Jumlah Peserta Terdaftar Uji Kompetensi Penyesuaian/Inpassing</h4>
						</div>
						<div class="card-body">
							<canvas id="CHbar3"></canvas>
							<hr>
							<span class="badge" style="background: #008ad2;color: #fff"><div id="jumlah_pertama_jadwal">   </div></span>
							<span class="badge" style="background: #0478b5;color: #fff"> <div id="jumlah_muda_jadwal">  </div></span>
							<span class="badge" style="background: #066394;color: #fff"><div id="jumlah_madya_jadwal">   </div></span>
							<span class="badge" style="background: #064d73;color: #fff"><div id="jumlah_peserta_jadwal">   </div></span>
							
						</div>
					</div>
				</div>
			</div>     
		</div>
		<div class="col-md-4 info-txt" style="margin-top: 3%;">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-ungu">
							<h4>Jumlah Peserta Lulus Uji Kompetensi Penyesuaian/Inpassing</h4>
						</div>
						<div class="card-body">
							<canvas id="CHbar4"></canvas>
							<hr>
							<span class="badge" style="background: #6460ac;color: #fff"><div id="jumlah_pertama_lulus">   </div></span>
							<span class="badge" style="background: #5b5796;color: #fff"> <div id="jumlah_muda_lulus">  </div></span>
							<span class="badge" style="background: #4c4978;color: #fff"><div id="jumlah_madya_lulus">   </div></span>
							<span class="badge" style="background: #383657;color: #fff"><div id="jumlah_peserta_lulus">   </div></span>
							
						</div>
					</div>
				</div>
			</div>     
		</div>
		<div class="col-md-6 info-txt" style="margin-top: 3%;">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-secondary">
							<h4>Hasil Peserta Ujian (Regular LKPP)</h4>
						</div>
						<div class="card-body">
							<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title"></h4>
									</div>
									<div class="panel-body">
										<span class="col-md-1"></span><span class="badge bg-success col-md-5" style="color: #fff">Verifikasi Portofolio</span><span class="badge bg-info col-md-5" style="color: #fff">Tes Tertulis</span><span class="col-md-1"></span><hr>
										<div class="min-top">
											<div class="row">
												<div class="col-md-3 col-6">
													<select name='length_change' id='length_change2' class="form-control">
														<option value='5'>5</option>
														<option value='10'>10</option>
														<option value='15'>15</option>
														<option value='20'>20</option>
													</select>
												</div>
												<div class="col-md-4 col-6">
													<input type="text" class="form-control" id="myInputTextField2" name="search" placeholder="Cari">
												</div>
												<div class="col-md-4 col-6">
													<form action="" method="post">
													@csrf
														<select name='bulan' id='bulan' class="form-control form-control-sm" onchange="this.form.submit()">
															<option value='' {{ $bulan == '' ? 'selected' : '' }}>Pilih Bulan</option>
															<option value='1' {{ $bulan == 1 ? 'selected' : '' }}>Januari</option>
															<option value='2' {{ $bulan == 2 ? 'selected' : '' }}>Februari</option>
															<option value='3' {{ $bulan == 3 ? 'selected' : '' }}>Maret</option>
															<option value='4' {{ $bulan == 4 ? 'selected' : '' }}>April</option>
															<option value='5' {{ $bulan == 5 ? 'selected' : '' }}>Mei</option>
															<option value='6' {{ $bulan == 6 ? 'selected' : '' }}>Juni</option>
															<option value='7' {{ $bulan == 7 ? 'selected' : '' }}>Juli</option>
															<option value='8' {{ $bulan == 8 ? 'selected' : '' }}>Agustus</option>
															<option value='9' {{ $bulan == 9 ? 'selected' : '' }}>September</option>
															<option value='10' {{ $bulan == 10 ? 'selected' : '' }}>Oktober</option>
															<option value='11' {{ $bulan == 11 ? 'selected' : '' }}>November</option>
															<option value='12' {{ $bulan == 12 ? 'selected' : '' }}>Desember</option>
														</select>
													</form>
												</div> 
											</div>
										</div>
										<div class="table-responsive">
											<table id="example4" class="table table-bordered table-striped">
												<thead>
													<tr>
														<th>Jadwal Ujian</th>
													</tr>
												</thead>
												<tbody>
												@foreach($jadwal as $jadwals)
													<tr>
														<td><a href="{{ url('menu-statik-inpassing')."/".$jadwals->id }}" @if($jadwals->metode == 'verifikasi_portofolio') class="btn btn-success col-md-12" @else class="btn btn-info col-md-12" @endif>{{ Helper::tanggal_indo($jadwals->tanggal_ujian) }}</a></td>
													</tr>
												@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>     
		</div>
		<div class="col-md-6 info-txt" style="margin-top: 3%;">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-secondary">
							<h4>Hasil Peserta Ujian (Instansi)</h4>
						</div>
						<div class="card-body">
							<div class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title"></h4>
									</div>
									<div class="panel-body">
										<span class="badge bg-info col-md-12" style="color: #fff">Tes Tertulis</span><hr>
										<div class="min-top">
											<div class="row">
												<div class="col-md-3 col-6">
													<select name='length_change' id='length_change3' class="form-control">
														<option value='5'>5</option>
														<option value='10'>10</option>
														<option value='15'>15</option>
														<option value='20'>20</option>
													</select>
												</div>
												<div class="col-md-4 col-6">
													<input type="text" class="form-control" id="myInputTextField3" name="search" placeholder="Cari">
												</div>
												<div class="col-md-4 col-6">
													<form action="" method="post">
													@csrf
														<select name='bulan_int' id='bulan' class="form-control form-control-sm" onchange="this.form.submit()">
															<option value='' {{ $bulan_int == '' ? 'selected' : '' }}>Pilih Bulan</option>
															<option value='1' {{ $bulan_int == 1 ? 'selected' : '' }}>Januari</option>
															<option value='2' {{ $bulan_int == 2 ? 'selected' : '' }}>Februari</option>
															<option value='3' {{ $bulan_int == 3 ? 'selected' : '' }}>Maret</option>
															<option value='4' {{ $bulan_int == 4 ? 'selected' : '' }}>April</option>
															<option value='5' {{ $bulan_int == 5 ? 'selected' : '' }}>Mei</option>
															<option value='6' {{ $bulan_int == 6 ? 'selected' : '' }}>Juni</option>
															<option value='7' {{ $bulan_int == 7 ? 'selected' : '' }}>Juli</option>
															<option value='8' {{ $bulan_int == 8 ? 'selected' : '' }}>Agustus</option>
															<option value='9' {{ $bulan_int == 9 ? 'selected' : '' }}>September</option>
															<option value='10' {{ $bulan_int == 10 ? 'selected' : '' }}>Oktober</option>
															<option value='11' {{ $bulan_int == 11 ? 'selected' : '' }}>November</option>
															<option value='12' {{ $bulan_int == 12 ? 'selected' : '' }}>Desember</option>
														</select>
													</form>
												</div> 
											</div>
										</div>
										<div class="table-responsive">
											<table id="example5" class="table table-bordered table-striped">
												<thead>
													<tr>
														<th>Jadwal Ujian</th>
													</tr>
												</thead>
												<tbody>
												@foreach($jadwal_instansi as $jadwals)
												<tr>
													<td><a href="{{ url('menu-statik-instansi')."/".$jadwals->id }}" @if($jadwals->metode == 'verifikasi_portofolio') class="btn btn-success col-md-12" @else class="btn btn-info col-md-12" @endif>
													{{ Helper::tanggal_indo($jadwals->tanggal_ujian) }}</a></td>
												</tr>
												@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>     
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $('#refresh').click(function(){
		alert('coba');
		$.ajax({
			type:'GET',
			url:'refreshcaptcha',
			success:function(data){
				$(".captcha span").html(data.captcha);
			}
		});
    });
  
    $(document).ready(function() {
		var jumlah_daerah = parseInt(@foreach($prov as $provs) {{ $provs->jumlah}} @endforeach)+parseInt(@foreach($kota as $kotas) {{ $kotas->jumlah}} @endforeach)+parseInt(@foreach($kab as $kabs) {{ $kabs->jumlah}} @endforeach);     
		var jumlah_kabupaten = parseInt(@foreach($kab as $kabs) {{ $kabs->jumlah}} @endforeach);      
		var jumlah_kota = parseInt(@foreach($kota as $kotas) {{ $kotas->jumlah}} @endforeach);      
		var jumlah_prov = parseInt(@foreach($prov as $provs) {{ $provs->jumlah}} @endforeach);      
		var lembaga_menteri = parseInt(@foreach($kementrian as $kementrians) {{ $kementrians->jumlah}} @endforeach)+parseInt(@foreach($lembaga as $lembagas) {{ $lembagas->jumlah}} @endforeach);

		var total_instansi = parseInt(jumlah_daerah+lembaga_menteri);
		var total_peserta = parseInt(@foreach($pesertaSistem_total as $totals) {{ $totals->jumlah}} @endforeach);
		var total_peserta_jadwal = parseInt(@foreach($pesertaSistemJadwal_total as $totals) {{ $totals->jumlah}} @endforeach);
		var total_peserta_lulus = parseInt(@foreach($dataLulusTotal as $totals) {{ $totals->jumlah}} @endforeach);
		var total_peserta_lengkap = parseInt(@foreach($datalengkapTotal as $totals) {{ $totals->jumlah}} @endforeach);

		var disetujui_pertama = parseInt(@foreach($datadisetujuipertama as $totals) {{ $totals->jumlah}} @endforeach);
		var disetujui_muda = parseInt(@foreach($datadisetujuimuda as $totals) {{ $totals->jumlah}} @endforeach);
		var disetujui_madya = parseInt(@foreach($datadisetujuimadya as $totals) {{ $totals->jumlah}} @endforeach);
		var disetujui_total = parseInt(disetujui_pertama+disetujui_muda+disetujui_madya);


		$('#jumlah_kabupaten').html('Jumlah Kabupaten Pengusul : '+jumlah_kabupaten);
		$('#jumlah_kota').html('Jumlah Kota Pengusul : '+jumlah_kota);
		$('#jumlah_provinsi').html('Jumlah Provinsi Pengusul : '+jumlah_prov);
		$('#jumlah_menteri').html('Jumlah K/L Pengusul : '+lembaga_menteri);

		$('#jumlah_peserta').html('Jumlah Peserta : '+total_peserta);
		$('#jumlah_peserta_jadwal').html('Jumlah Peserta Terdaftar : '+total_peserta_jadwal);
		$('#jumlah_instansi').html('Jumlah Instansi Pengusul : '+total_instansi);
		$('#jumlah_peserta_lulus').html('Jumlah Peserta Lulus : '+total_peserta_lulus);
		$('#jumlah_peserta_lengkap').html('Jumlah Peserta <br> Memenuhi Persyaratan : '+total_peserta_lengkap);

		$('#jumlah_setuju_pertama').html('Pertama : '+disetujui_pertama);
		$('#jumlah_setuju_muda').html('Muda : '+disetujui_muda);
		$('#jumlah_setuju_madya').html('Madya : '+disetujui_madya);
		$('#jumlah_setuju_peserta').html('Total : '+disetujui_total);


		@foreach($pesertaSistem as $pesertas)
			@if($pesertas->jenjang == 'Madya')
				var jumlah_madya = {{$pesertas->jumlah}};
				$('#jumlah_madya').html('Jumlah Peserta Madya : '+jumlah_madya);
			@endif
			@if($pesertas->jenjang == 'Muda')
				var jumlah_muda = {{$pesertas->jumlah}};
				$('#jumlah_muda').html('Jumlah Peserta Muda : '+jumlah_muda);
			@endif
			@if($pesertas->jenjang == 'Pertama')
				var jumlah_pertama = {{$pesertas->jumlah}};
				$('#jumlah_pertama').html('Jumlah Peserta Pertama : '+jumlah_pertama);
			@endif
		@endforeach
		@foreach($dataLulusJenjang as $pesertas)
			@if($pesertas->jenjang == 'Madya')
				var jumlah_madya = {{$pesertas->jumlah}};
				$('#jumlah_madya_lulus').html('Jumlah Peserta Lulus Madya : '+jumlah_madya);
			@endif
			@if($pesertas->jenjang == 'Muda')
				var jumlah_muda = {{$pesertas->jumlah}};
				$('#jumlah_muda_lulus').html('Jumlah Peserta Lulus Muda : '+jumlah_muda);
			@endif
			@if($pesertas->jenjang == 'Pertama')
				var jumlah_pertama = {{$pesertas->jumlah}};
				$('#jumlah_pertama_lulus').html('Jumlah Peserta Lulus Pertama : '+jumlah_pertama);
			@endif
		@endforeach
		@foreach($pesertaSistemJadwal as $pesertas)
			@if($pesertas->jenjang == 'Madya')
				var jumlah_madya = {{$pesertas->jumlah}};
				$('#jumlah_madya_jadwal').html('Jumlah Peserta Madya : '+jumlah_madya);
			@endif
			@if($pesertas->jenjang == 'Muda')
				var jumlah_muda = {{$pesertas->jumlah}};
				$('#jumlah_muda_jadwal').html('Jumlah Peserta Muda : '+jumlah_muda);
			@endif
			@if($pesertas->jenjang == 'Pertama')
				var jumlah_pertama = {{$pesertas->jumlah}};
				$('#jumlah_pertama_jadwal').html('Jumlah Peserta Pertama : '+jumlah_pertama);
			@endif            
		@endforeach
		@foreach($datalengkapJenjang as $pesertas)
			@if($pesertas->jenjang == 'Madya')
				var jumlah_madya = {{$pesertas->jumlah}};
				$('#jumlah_madya_lengkap').html('Jumlah Peserta Madya : '+jumlah_madya);
			@endif
			@if($pesertas->jenjang == 'Muda')
				var jumlah_muda = {{$pesertas->jumlah}};
				$('#jumlah_muda_lengkap').html('Jumlah Peserta Muda : '+jumlah_muda);
			@endif
			@if($pesertas->jenjang == 'Pertama')
				var jumlah_pertama = {{$pesertas->jumlah}};
				$('#jumlah_pertama_lengkap').html('Jumlah Peserta Pertama : '+jumlah_pertama);
			@endif            
		@endforeach
    });
    
	$(function () {
		var jumlah_daerah = parseInt(@foreach($prov as $provs) {{ $provs->jumlah}} @endforeach)+parseInt(@foreach($kota as $kotas) {{ $kotas->jumlah}} @endforeach)+parseInt(@foreach($kab as $kabs) {{ $kabs->jumlah}} @endforeach);   
		var jumlah_kabupaten = parseInt(@foreach($kab as $kabs) {{ $kabs->jumlah}} @endforeach);      
		var jumlah_kota = parseInt(@foreach($kota as $kotas) {{ $kotas->jumlah}} @endforeach);      
		var jumlah_prov = parseInt(@foreach($prov as $provs) {{ $provs->jumlah}} @endforeach);        
		var lembaga_menteri = parseInt(@foreach($kementrian as $kementrians) {{ $kementrians->jumlah}} @endforeach)+parseInt(@foreach($lembaga as $lembagas) {{ $lembagas->jumlah}} @endforeach);     
		var ctx = document.getElementById('CHbar').getContext('2d');
		new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ['K/L','Provinsi','Kabupaten','Kota'],
				datasets: [{
					label: '# of Total',
					data: [lembaga_menteri,jumlah_prov,jumlah_kabupaten,jumlah_kota],
					backgroundColor: [
					'##cd004d',
					'##bf0249',
					'##99053c',
					'##750730',
					'##4f0722']
				}]
			},
			options: {
				legend: {
					display: false,
					labels: {
						display: false
					}
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true,
							fontSize: 10,
							max: 500
						}
					}],
					xAxes: [{
						ticks: {
							beginAtZero:true,
							fontSize: 11
						}
					}]
				}
			}
		});
    });
    
	$(function () {
		var ctx = document.getElementById('CHbar2').getContext('2d');
		new Chart(ctx, {
			type: 'bar',
			data: {
				labels: [@foreach($pesertaSistem as $pesertas)
					'{{ $pesertas->jenjang}}',
				@endforeach],
				datasets: [{
					label: '# of Total',
					data: [@foreach($pesertaSistem as $pesertas)
						'{{ $pesertas->jumlah}}',
					@endforeach],
					backgroundColor: [
					'#fcb712',
					'#e3a510',
					'#c7910e'
					

					]
				}]
			},
			options: {
				legend: {
					display: false,
					labels: {
						display: false
					}
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true,
							fontSize: 10,
							max: 800
						}
					}],
					xAxes: [{
						ticks: {
							beginAtZero:true,
							fontSize: 11
						}
					}]
				}
			}
		});
	});
    
	$(function () {	
		var ctx = document.getElementById('CHbar3').getContext('2d');
		new Chart(ctx, {
		type: 'bar',
		data: {
			labels: [
				@foreach($pesertaSistemJadwal as $pesertas)
					'{{ $pesertas->jenjang}}',
				@endforeach
			],
			datasets: [{
				label: '# of Total',
				data: [
					@foreach($pesertaSistemJadwal as $pesertas)      
						'{{ $pesertas->jumlah}}',
					@endforeach
				],
				backgroundColor: [
					'#008ad2',
					'#0478b5',
					'#066394'
					]
				}
			]
		},
		options: {
			legend: {
				display: false,
				labels: {
					display: false
				}
			},
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true,
						fontSize: 10,
						max: 800
					}
				}],
				xAxes: [{
					ticks: {
						beginAtZero:true,
						fontSize: 11
					}
				}]
			}
		}
	});
});

$(function () {	
		var ctx = document.getElementById('CHbar4').getContext('2d');
		new Chart(ctx, {
		type: 'bar',
		data: {
			labels: [
				@foreach($dataLulusJenjang as $pesertas)
					'{{ $pesertas->jenjang}}',
				@endforeach
			],
			datasets: [{
				label: '# of Total',
				data: [
					@foreach($dataLulusJenjang as $pesertas)      
						'{{ $pesertas->jumlah}}',
					@endforeach
				],
				backgroundColor: [
					'#6460ac',
					'#5b5796',
					'#4c4978',
					'#11451e'
					]
				}
			]
		},
		options: {
			legend: {
				display: false,
				labels: {
					display: false
				}
			},
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true,
						fontSize: 10,
						max: 800
					}
				}],
				xAxes: [{
					ticks: {
						beginAtZero:true,
						fontSize: 11
					}
				}]
			}
		}
	});
});

$(function () {	
		var disetujui_pertama = parseInt(@foreach($datadisetujuipertama as $totals) {{ $totals->jumlah}} @endforeach);
		var disetujui_muda = parseInt(@foreach($datadisetujuimuda as $totals) {{ $totals->jumlah}} @endforeach);
		var disetujui_madya = parseInt(@foreach($datadisetujuimadya as $totals) {{ $totals->jumlah}} @endforeach);
		var ctx = document.getElementById('CHbar5').getContext('2d');
		new Chart(ctx, {
		type: 'bar',
		data: {
			labels: [
				'Pertama','Muda','Madya'
			],
			datasets: [{
				label: '# of Total',
				data: [
					disetujui_pertama,disetujui_muda,disetujui_madya
				],
				backgroundColor: [
					'#f37020',
					'#d6611a',
					'#bf5717',
					'#a34810'
					]
				}
			]
		},
		options: {
			legend: {
				display: false,
				labels: {
					display: false
				}
			},
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true,
						fontSize: 10,
						max: 500
					}
				}],
				xAxes: [{
					ticks: {
						beginAtZero:true,
						fontSize: 11
					}
				}]
			}
		}
	});
});

$(function () {	
		var ctx = document.getElementById('CHbar6').getContext('2d');
		new Chart(ctx, {
		type: 'bar',
		data: {
			labels: [
				@foreach($datalengkapJenjang as $pesertas)
					'{{ $pesertas->jenjang}}',
				@endforeach
			],
			datasets: [{
				label: '# of Total',
				data: [
					@foreach($datalengkapJenjang as $pesertas)
						'{{ $pesertas->jumlah}}',
					@endforeach
				],
				backgroundColor: [
					'#0cb14d',
					'#0b9943',
					'#0a7a36',
					'#11451e'
					]
				}
			]
		},
		options: {
			legend: {
				display: false,
				labels: {
					display: false
				}
			},
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true,
						fontSize: 10,
						max: 500
					}
				}],
				xAxes: [{
					ticks: {
						beginAtZero:true,
						fontSize: 11
					}
				}]
			}
		}
	});
});
</script>
@endsection