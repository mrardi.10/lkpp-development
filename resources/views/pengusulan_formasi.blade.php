@extends('layout.app')

@section('title')
Pengusulan Formasi
@stop

@section('css')
<style type="text/css">
	.div-print{
		text-align: right;
	}
	.div-print button{
		color: black;
	}
</style>
@endsection

@section('content')
@if (session('msg'))
			@if (session('msg') == "berhasil")
			<div class="row">
				<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>Berhasil simpan Data</strong>
						</div>
				</div>
			</div> 
			@endif
			@if (session('msg') == "gagal")
			<div class="row">
					<div class="col-md-12">
							<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Gagal simpan Data</strong>
								</div>
					</div>
				</div> 
			@endif

			@if (session('msg') == "berhasil_update")
			<div class="row">
				<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>Berhasil ubah Data</strong>
						</div>
				</div>
			</div> 
			@endif
			@if (session('msg') == "gagal_update")
			<div class="row">
					<div class="col-md-12">
							<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Gagal ubah Data</strong>
								</div>
					</div>
				</div> 
      @endif
      
      @if (session('msg') == "berhasil_ganti")
			<div class="row">
				<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>Berhasil ubah Password</strong>
						</div>
				</div>
			</div> 
			@endif
			@if (session('msg') == "gagal_ganti")
			<div class="row">
					<div class="col-md-12">
							<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Gagal Ganti Password</strong>
								</div>
					</div>
				</div> 
			@endif
@endif
<div class="main-box">
		<div class="min-top">
			<div class="row">
				<div class="col-md-1 text-center">
					<b>Perlihatkan</b>
				</div>
				<div class="col-md-2">
						<select name='length_change' id='length_change' class="form-control">
								<option value='50'>50</option>
								<option value='100'>100</option>
								<option value='150'>150</option>
								<option value='200'>200</option>
						</select>
				</div>
				<div class="col-md-4 col-12">
						<div class="input-group">
								<div class="input-group addon">
									<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
									<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
								</div>
						</div>
				</div>
				<div class="col-md-5 col-12 div-print">
						<a href="{{ url('pengusulan-eformasi-excell') }}"><button class="btn btn-success" style="color: #fff"><i class="fa fa-print"></i> Print Excel</button></a>
				</div>
			</div> 
		</div>
		<div class="table-responsive">
			<table id="pengusulan" class="table table-bordered table-striped">
					<thead>
					<tr>
						<th>No.</th>
						<th>Nama</th>
						<th>Instansi</th>
						<th>Jumlah JF PPBJ Pertama</th>
						<th>Status Usulan</th>
						<th>Jumlah JF PPBJ Muda</th>
						<th>Status Usulan</th>
						<th>Jumlah JF PPBJ Madya</th>
						<th>Status Usulan</th>
						<th>Aksi</th>
					</tr>
					</thead>
					{{-- <tbody>
						@foreach ($eformasi as $key => $eformasis)
							<tr>
								<td>{{ $key++ + 1 }}</td>
								<td>{{ $eformasis->namas }}</td>
								<td>{{ $eformasis->instansis }}</td>
								<td>{{ $eformasis->pertama }}</td>
								<td>
									@if (is_null($eformasis->status_pertama))
											-
									@else
										{{ $eformasis->status_pertama == 'setuju' ? 'Setuju' : 'Tidak Setuju' }}
									@endif
								</td>
								<td>{{ $eformasis->muda }}</td>
								<td>
									@if (is_null($eformasis->status_muda))
										-	
									@else
										{{ $eformasis->status_muda == 'setuju' ? 'Setuju' : 'Tidak Setuju' }}
									@endif
								</td>
								<td>{{ $eformasis->madya }}</td>
								<td>
									@if (is_null($eformasis->status_madya))
											-
									@else
										{{ $eformasis->status_madya == 'setuju' ? 'Setuju' : 'Tidak Setuju' }}	
									@endif
								</td>
								<td>
									<div class="dropdown">
										<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
										<ul class="dropdown-menu">
												<li><a href="{{ url('verifikasi-usulan')."/".$eformasis->id }}">Verifikasi Formasi</a></li>
												<li><a href="{{ url('riwayat-eformasi')."/".$eformasis->id }}">Riwayat</a></li>
												@if(Auth::user()->role == 'superadmin')
												<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $eformasis->id }}">Hapus</a></li>		
												@endif
										</ul>
									</div>
								</td>
							</tr>
							<div class="modal fade" id="modal-default{{ $eformasis->id }}">
								<div class="modal-dialog" style="width:30%">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title">Hapus pengusulan eformasi</h4>
										</div>
										<div class="modal-body">
											<p>Apakah Anda yakin menghapus usulan formasi?</p>
										</div>
										<div class="modal-footer">
											<a href="{{ url('hapus-pengusulan-formasi')."/".$eformasis->id }}" type="button" class="btn btn-primary pull-left">HAPUS</a>
											<button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
						@endforeach
					</tbody> --}}
			</table>
		</div>
</div>
@stop

@section('js')
<script>
	// $(function() {
	// 	$('#pengusulan').DataTable({
	// 		processing: true,
	// 		serverSide: true,
	// 		ajax: 'pengusulan-eformasi-lkpp/json',
	// 		columns: [
	// 			{ data: 'id', name: 'id' },
	// 			{ data: 'namas', name: 'namas' },
	// 			{ data: 'instansis', name: 'instansis' },
	// 			{ data: 'pertama', name: 'pertama' },
	// 			{ data: 'status_pertama', name: 'status_pertama' },
	// 			{ data: 'muda', name: 'muda' },
	// 			{ data: 'status_muda', name: 'status_muda' },
	// 			{ data: 'madya', name: 'madya' },
	// 			{ data: 'status_madya', name: 'status_madya' },
	// 		]
	// 	});
	// });
	$(function() {
		$('#pengusulan').DataTable()
	})

	pTable = $('#pengusulan').DataTable({
		processing: false,
		serverSide: true,
		ajax: 'pengusulan-eformasi-lkpp/json',
		columns: [
			{ data: 'DT_RowIndex', name: 'DT_RowIndex' },
			{ data: 'namas', name: 'namas' },
			{ data: 'instansis', name: 'instansis' },
			{ data: 'pertama', name: 'pertama' },
			{ data: 'status_usulan_pertama', name: 'status_usulan_pertama' },
			{ data: 'muda', name: 'muda' },
			{ data: 'status_usulan_muda', name: 'status_usulan_muda' },
			{ data: 'madya', name: 'madya' },
			{ data: 'status_usulan_madya', name: 'status_usulan_madya' },
			{ data: 'intro', name: 'intro' },
		],
		language: {
			paginate: {
				previous: 'Sebelumnya',
				next:     'Selanjutnya'
			},
			aria: {
				paginate: {
					previous: 'Sebelumnya',
					next:     'Selanjutnya'
				}
			}
		}
	});
    $('#myInputTextField').keyup(function(){
		pTable.search($(this).val()).draw();
    })
    $('#length_change').val(pTable.page.len());
    $('#length_change').change( function() { 
		pTable.page.len( $(this).val() ).draw();
    });
</script>
@endsection