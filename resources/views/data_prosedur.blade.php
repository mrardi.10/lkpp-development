@extends('layout.app')

@section('title')
    Data Prosedur
@endsection

@section('css')
<style>
.btn-tbh{
	text-align: right;
}

.btn-jadwal{
	width: 120px;
	background: #E8382A;
	color: #fff;
	font-weight: 600;
}

.btn-jadwal:hover{
	color: aliceblue;
}
</style>
@stop

@section('content')
@if (session('msg'))
			@if (session('msg') == "berhasil")
			<div class="row">
				<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>Berhasil Simpan Data</strong>
						</div>
				</div>
			</div> 
			@endif
			@if (session('msg') == "gagal")
			<div class="row">
					<div class="col-md-12">
							<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Gagal Simpan Data</strong>
								</div>
					</div>
				</div> 
			@endif

			@if (session('msg') == "berhasil_update")
			<div class="row">
				<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>Berhasil Update Data</strong>
						</div>
				</div>
			</div> 
			@endif
			@if (session('msg') == "gagal_update")
			<div class="row">
					<div class="col-md-12">
							<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Gagal Update Data</strong>
								</div>
					</div>
				</div> 
			@endif
@endif
<h3>Text dan Video Menu Prosedur :</h3>
<div class="main-box">
		<div class="min-top">
			<div class="row">
				<div class="col-md-1 text-center">
					<b>Perlihatkan</b>
				</div>
				<div class="col-md-2 col-6">
						<select name='length_change' id='length_change' class="form-control">
                                <option value='10' selected>10</option>
								<option value='50'>50</option>
								<option value='100'>100</option>
								<option value='150'>150</option>
								<option value='200'>200</option>
						</select>
				</div>
				<div class="col-md-4 col-6">
						<div class="input-group">
								<div class="input-group addon">
									<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
									<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
								</div>
						</div>
				</div>
				
			</div> 
		</div>
		<div class="table-responsive">
			<table id="example1" class="table table-bordered table-striped">
					<thead>
					<tr>
						<th width="5%">No</th>
						<th width="85%">Text</th>
						<th width="10%">Aksi</th>
					</tr>
					</thead>
					<tbody>
						@foreach ($data as $key => $datas)
						<tr>
							<td>{{ $key++ + 1 }}</td>
							<td>
                                @php
                                echo $datas->text
                                @endphp 
                            </td>
							<td>
									<div class="dropdown">
											<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
											<ul class="dropdown-menu">
													<li><a href="{{ url('edit-prosedur')."/".$datas->id }}">Edit</a></li>
											</ul>
									</div>
							</td>
						</tr>
						@endforeach
					</tbody>
			</table>
		</div>
</div>

<h3>Data Berkas Prosedur :</h3>
<div class="main-box">
		<div class="min-top">
			<div class="row">
				<div class="col-md-1 text-center">
					<b>Perlihatkan</b>
				</div>
				<div class="col-md-2 col-6">
						<select name='length_change' id='length_change' class="form-control">
                                <option value='10' selected>10</option>
								<option value='50'>50</option>
								<option value='100'>100</option>
								<option value='150'>150</option>
								<option value='200'>200</option>
						</select>
				</div>
				<div class="col-md-4 col-6">
						<div class="input-group">
								<div class="input-group addon">
									<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
									<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
								</div>
						</div>
				</div>
				<div class="col-md-5 col-6 btn-tbh">
						<a href="{{ url('tambah-berkas-prosedur') }}"><button class="btn btn-sm btn-jadwal">Tambah Berkas</button></a>
				</div>
				
			</div> 
		</div>
		<div class="table-responsive">
			<table id="example1" class="table table-bordered table-striped">
					<thead>
					<tr>
						<th width="5%">No</th>
						<th>Judul</th>
						<th>Berkas</th>
						<th width="10%">Aksi</th>
					</tr>
					</thead>
					<tbody>
						@foreach ($data_berkas as $key => $datas)
						<tr>
							<td>{{ $key++ + 1 }}</td>
							<td>{{ $datas->judul_berkas }}</td>
							<td>
								@php
								$berkas = explode('.',$datas->berkas);
								@endphp
								@if ($berkas[1] == 'pdf' || $berkas[1] == 'PDF')
								<a href="{{ url('priview-file')."/berkas/".$datas->berkas }}" target="_blank"><label><i class="fa fa-file-pdf-o" style="font-size:7px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
								@else
								<a href="{{ url('priview-file')."/berkas/".$datas->berkas }}" target="_blank"><img src="{{ asset('storage/data/berkas')."/".$datas->berkas }}" class="img-rounded" width="60px"></a>
								@endif
							</td>
							<td>
									<div class="dropdown">
											<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
											<ul class="dropdown-menu">
													<li><a href="{{ url('edit-berkas-prosedur')."/".$datas->id }}">Edit</a></li>
													<li><a href="{{ url('hapus-berkas-prosedur')."/".$datas->id }}">Hapus</a></li>
											</ul>
									</div>
							</td>
						</tr>
						@endforeach
					</tbody>
			</table>
		</div>
</div>
@endsection