<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Simple Transactional Email</title>
    <style>
    /* -------------------------------------
        INLINED WITH htmlemail.io/inline
    ------------------------------------- */
    /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
        font-size: 16px !important;
      }
      table[class=body] .wrapper,
            table[class=body] .article {
        padding: 10px !important;
      }
      table[class=body] .content {
        padding: 0 !important;
      }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important;
      }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important;
      }
      table[class=body] .btn table {
        width: 100% !important;
      }
      table[class=body] .btn a {
        width: 100% !important;
      }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important;
      }
    }

    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
        line-height: 100%;
      }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }
      .btn-primary table td:hover {
        background-color: #34495e !important;
      }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
    }
    </style>
  </head>
  <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
      <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

            <!-- START CENTERED WHITE CONTAINER -->
          <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;"></span>
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                    <tr>
                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hi Admin PPK,</p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Bersama ini kami sampaikan hasil <b>Verifikasi Portofolio (Sementara)</b> dari Asesor LKPP untuk peserta berikut:{{--  ({{ $name }}) baru saja merekomendasikan peserta dengan data sebagai berikut: --}}</p>
                        <table style="width:100%">
                            <tr>
                                <td style="width:28%">Nama</td>
                                <td style="width:2%">:</td>
                                <td style="width:70%">{{ $nama_peserta }}</td>
                            </tr>
                            <tr>
                                <td style="width:28%">NIP</td>
                                <td style="width:2%">:</td>
                                <td style="width:70%">{{ $nip }}</td>
                            </tr>
                             <tr>
                                <td style="width:28%">Instansi</td>
                                <td style="width:2%">:</td>
                                <td style="width:70%">{{ $instansi }}</td>
                            </tr>
                            <tr>
                                <td style="width:28%">Jenjang</td>
                                <td style="width:2%">:</td>
                                <td style="width:70%">{{ $jenjang }}</td>
                            </tr>
                            <tr>
                                <td style="width:28%">Tanggal Ujian</td>
                                <td style="width:2%">:</td>
                                <td style="width:70%">{{ Helper::tanggal_indo($tgl_batas)}}</td>
                            </tr>
                            <tr>
                              <td style="width:28%">Status Verifikasi</td>
                              <td style="width:2%">:</td>
                              <td style="width:70%">
                                @if($rekomendasi == 'tidak_lulus')
                                  Tidak Sesuai
                                  @else
                                  Sesuai
                                  @endif
                        </td>
                          </tr>
                          
                        </table>
                        @if($rekomendasi == 'tidak_lulus' )
                        <br>  
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Adapun dokumen yang <b>Tidak Sesuai</b> pada:
                          <ol type="1">
                                 @foreach ($nama_input as $namas)
                              {{-- @if($namas->detail_input == $namas->detail_input2)
                              @if($ket->keterangan != "" && $ket->status == "tidak_sesuai") --}}
                              {{-- @if($namas->poins == $namas->JudulInput)
                              @if($namas->detail_input == $namas->detail_input2) --}}
                              @if($namas->keterangan != "" && $namas->status == "tidak_sesuai")
                               @if($namas->file_input == $namas->status_detail)
                            <li>  <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"><b>{{ $namas->judul_input}}</b> ({{$namas->nama_input}})<br>
                               
                                {{ $namas->nama_file_input}} <br>: <b>{{ $namas->keterangan}}</b>
                              
                              </p>
                              </li>
                            {{-- @endif
                            @endif --}}
                            @endif
                              @endif
                          @endforeach
                            </ol>
                        <br>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Portofolio dapat diperbaiki sampai dengan H+3 dari Tanggal {{ Helper::tanggal_indo($tgl_batas)}} Pukul 16.00 WIB. Adapun hasil akhir Verifikasi Portofolio dapat dilihat pada halaman Jadwal Uji Kompetensi. Terima kasih.</p>
                        <br>
                        @endif
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Salam,<br>Admin LKPP</p>
                        {{-- <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">terimakasih.</p> --}}
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>

            <!-- START FOOTER -->
            <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
              {{-- <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                <tr>
                  <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                    <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">Company Inc, 3 Abbey Road, San Francisco CA 94102</span>
                    <br> Don't like these emails? <a href="http://i.imgur.com/CScmqnj.gif" style="text-decoration: underline; color: #999999; font-size: 12px; text-align: center;">Unsubscribe</a>.
                  </td>
                </tr>
                <tr>
                  <td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                    Powered by <a href="http://htmlemail.io" style="color: #999999; font-size: 12px; text-align: center; text-decoration: none;">HTMLemail</a>.
                  </td>
                </tr>
              </table> --}}
            </div>
            <!-- END FOOTER -->

          <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>