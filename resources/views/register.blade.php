@extends('layout.app2')
@section('title')
	Register
@endsection
@section('css')
<style>
	.jud-register{
		padding: 15px;
		color: #3a394e;
    }

    .box{
        -webkit-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
        -moz-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
        box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.26);
    }

    .btn-default1{
        background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
        color: white;
        font-weight: 600;
        width: 100px; 
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .btn-default2{
        background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
        color: black;
        font-weight: 600;
        width: 100px; 
        margin: 10px;
        -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
        box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    }

    .btn-default1:hover{
        color: #eee;
        background: #c10013;
    }

    .btn-area{
        text-align: right;
    }

    .navi ul{
		padding:0;
		font-size:0;
		overflow:hidden;
		display:inline-block;
		width: 100%;
    }
	
    .navi li{
		display:inline-block;
		text-align: center;
    }
    
	.navi a{
		font-size:1rem;
		position:relative;
		display:inline-block;
		background:#eee;
		text-decoration:none;
		color:#555;
		padding:13px 25px 13px 10px;
		width: 100%;
		font-weight: 600;
    }
    
	.navi a:after,
    .navi a:before{
		position: absolute;
		content: "";
		height: -10px;
		width: 0px;
		top: 50%;
		left: -28px;
		margin-top: -25px;
		border: 25px solid #eee;
		border-right: 2 !important;
		border-left-color: transparent !important;
    }
    
	.navi a:before{
		left:-26px;
		border: 24px solid #555;
    }
    
	/* ACTIVE STYLES */
    .navi a.active{
        background: #e74c3c;
        color: #fff;
        -webkit-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
    }
    
	.navi a.active:after{border-color:#e74c3c;}
    
	/* HOVER STYLES */
    .navi a:hover{
        background: #e74c3c;
        color: #fff;
        -webkit-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        -moz-box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
        box-shadow: 1px 0px 5px 0px rgba(0,0,0,0.3);
    }
    
	.navi a:hover:after{ border-color:#e74c3c;}

    #page-two{
        display: none;
    }

    #page-three{
        display: none;
    }

    div#page-two div.container div.row{
        padding: 0px 5%; 
        margin-bottom: 1%;
    }

    div#page-two h5 span{
        color: #e74c3c;
    }

    label.custom-file-label{
        margin: 0px 15px;
    }

    .row .form-control{
		-webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
    }

    .row .custom-file-label{
        -webkit-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		-moz-box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19);
		box-shadow: 0px 3px 5px 2px rgba(0,0,0,0.19); 
    }
</style>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3 class="jud-register">Daftar Akun Admin Pejabat Pembina Kepegawaian (Admin PPK)</h3>
		</div>
		<div class="col-md-12">
			<div class="box">
				<div class="navi">
					<ul>
						<li style="width:30%"><a href="#" id="li-one" class="active">1.Peraturan</a></li>
						<li style="width:40%"><a href="#" id="li-two">2.Data Pribadi - Admin PPK</a></li>\
						<li style="width:30%"><a href="#" id="li-three">3.Konfirmasi</a></li>
					</ul>
				</div>
				<div class="page container-fluid" id="page-one">
					<p>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur, deleniti iure nostrum quidem animi velit deserunt culpa perferendis modi odio quod, libero vel incidunt delectus, officia assumenda provident aliquid excepturi?
					</p>
					<ol>
						<li>lorem ipsum</li>
						<li>lorem ipsum</li>
						<li>lorem ipsum</li>
					</ol>
					<p>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ullam itaque in qui ducimus assumenda doloremque. Natus rerum quae, aliquid magnam dicta iusto ipsa neque beatae labore non odit sit temporibus.
					</p>
					<ol>
						<li>lorem ipsum</li>
						<li>lorem ipsum</li>
						<li>lorem ipsum</li>
					</ol>
					<p>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur, deleniti iure nostrum quidem animi velit deserunt culpa perferendis modi odio quod, libero vel incidunt delectus, officia assumenda provident aliquid excepturi?
					</p>
					<div class="col-md-12 btn-area">
						<button class="btn btn-sm btn-default1" id="btnOne" onclick="btnOne()">Next<i class="fa fa-angle-right"></i></button>
					</div>
				</div>
				<form action="" method="POST" enctype="multipart/form-data">
				@csrf
					<div class="page container-fluid" id="page-two">
						<div class="container">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5>Nama<span>*</span></h5>
								</div>
                                <div class="div col-lg-1 col-md-1 col-1">
									:
                                </div>
								<div class="col-lg-7 col-md-7 col-12">
									<input type="text" name="nama" id="nama" class="form-control form-control-sm" value="{{ old('nama') }}" required>
									<span class="errmsg">{{ $errors->first('nama') }}</span>
								</div>
							</div>
                            <div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5>NIP<span>*</span></h5>
								</div>
								<div class="div col-lg-1 col-md-1 col-1">
									:
								</div>
								<div class="col-lg-7 col-md-7 col-12">
									<input type="text" name="nip" id="nip" class="form-control form-control-sm" value="{{ old('nip') }}" required>
									<span class="errmsg">{{ $errors->first('nip') }}</span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-10">
									<h5>Jabatan<span>*</span></h5>
								</div>
								<div class="div col-lg-1 col-md-1 col-1">
									:
								</div>
								<div class="col-lg-7 col-md-7 col-12">
									<input type="text" name="jabatan" id="jabatan" class="form-control form-control-sm" value="{{ old('jabatan') }}" required>
									<span class="errmsg">{{ $errors->first('jabatan') }}</span>
								</div>
							</div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Nama Instansi<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="nama_instansi" id="nama_instansi" class="form-control form-control-sm" value="{{ old('nama_instansi') }}" required>
                                    <span class="errmsg">{{ $errors->first('nama_instansi') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>Email<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="email" name="email" id="email" class="form-control form-control-sm" value="{{ old('email') }}" required>
                                    <span class="errmsg">{{ $errors->first('email') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>No Telp/HP<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="text" name="no_telp" id="no_telp" class="form-control form-control-sm" value="{{ old('no_telp') }}" required>
                                    <span class="errmsg">{{ $errors->first('no_telp') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-10">
                                    <h5>SK Admin PPK<span>*</span></h5>
                                </div>
                                <div class="div col-lg-1 col-md-1 col-1">
                                    :
                                </div>
                                <div class="col-lg-7 col-md-7 col-12">
                                    <input type="file" name="image" class="custom-file-input" id="customFile" value="{{ old('image') }}" required>
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                    <span class="errmsg">{{ $errors->first('image') }}</span>
                                </div>
                            </div>
                                
                        </div>
                        <div class="col-md-12 btn-area">
                                <button class="btn btn-sm btn-default2" id="btnTwoBack" onclick="btnTwoBack()"><i class="fa fa-angle-left"> Back</i></button>
                                <button class="btn btn-sm btn-default1" id="btnTwoNext">Next <i class="fa fa-angle-right"></i></button>
                        </div>
                    </div>
                    <div class="page container-fluid" id="page-three">
						<p>
							Pastikan Semua data yang anda isi benar.TOMBOL SUBMIT hanya berfungsi bila anda telah melengkapi semua isian. setelah melengkapi formulir anda akan mendapatkan email yang berisi link konfirmasi untuk mendapatkan email yang berisi link konfirmasi untuk dapat mengakses akun pendaftaran uji kompetensi inpassing JF PPBJ
						</p>
						<div class="col-md-12 btn-area">
							<button class="btn btn-sm btn-default2" id="btnThreeBack" onclick="btnThreeBack()"><i class="fa fa-angle-left"> Back</i></button>
							<button class="btn btn-sm btn-default1" type="submit">Submit <i class="fa fa-angle-right"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script>
    $('#btnOne').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-two").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "block";
        document.getElementById("page-three").style.display = "none";
    });

    $('#btnTwoBack').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-one").addClass("active");
        document.getElementById("page-one").style.display = "block";
        document.getElementById("page-two").style.display = "none";
        document.getElementById("page-three").style.display = "none";        
    });

    $('#btnTwoNext').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-three").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "none";
        document.getElementById("page-three").style.display = "block";        
    });

    $('#btnThreeBack').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-two").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "block";
        document.getElementById("page-three").style.display = "none";        
    });

    $('#li-one').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-one").addClass("active");
        document.getElementById("page-one").style.display = "block";
        document.getElementById("page-two").style.display = "none";
        document.getElementById("page-three").style.display = "none";        
    });

    $('#li-two').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-two").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "block";
        document.getElementById("page-three").style.display = "none";        
    });

    $('#li-three').click(function (e) {
        e.preventDefault();        
        $("ul li a.active").removeClass("active");
        $("ul li a#li-three").addClass("active");
        document.getElementById("page-one").style.display = "none";
        document.getElementById("page-two").style.display = "none";
        document.getElementById("page-three").style.display = "block";        
    });
    
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
@endsection