@extends('layout.app2')

@section('title')
Login
@stop

@section('css')
<style type="text/css">
  body, html {
  height: 100%;
  margin: px;
}

.bg {
  width: 100%;
  height: 300px;
  margin-bottom: 10px;
}
.marge{
  margin: 70px;
  margin-top: 30px;
  margin-bottom: 0px;
}
.box{
  float: right;
}

/*form login*/

h1 {
  text-align: center;
  /*ketebalan font*/
  font-weight: 300;
}

.tulisan_login {
  text-align: center;
  /*membuat semua huruf menjadi kapital*/
  text-transform: uppercase;
  color: #ffffff !important;
  padding-left: 20px;
  padding-top: 10px;
  margin-top: 5px;
  font-size: 25px;

}

.kotak_login {
  width: 350px;
  background: white;
  /*meletakkan form ke tengah*/
  margin: -150px auto;
  margin-right: 25px;
  padding: 30px 20px;
  float: right;
  position: sticky;
  -webkit-box-shadow: 0px 0px 7px 1px rgba(0,0,0,0.75);
-moz-box-shadow: 0px 0px 7px 1px rgba(0,0,0,0.75);
box-shadow: 0px 0px 7px 1px rgba(0,0,0,0.75);
}

label {
  font-size: 11pt;
}

.form_login {
  /*membuat lebar form penuh*/
  box-sizing: border-box;
  width: 100%;
  padding: 10px;
  font-size: 11pt;
  margin-bottom: 20px;
}

.tombol_login {
  background: #3ef00c;
  color: white;
  font-size: 11pt;
  width: 100%;
  border: none;
  border-radius: 3px;
  padding: 10px 20px;
}

.row{
  margin-right: -20px;
    margin-left: -20px;
}

.tek{
  font-size: 19px;
}
b{
  color: #ffffff !important;
}
/*end css login*/
</style>
@stop

@section('content')
<div><img src="{{ asset('assets/img/back.jpg')}}" class="bg" style="background-position: center;
  background-repeat: no-repeat;z-index: -3px;"></div>
  <div class="kotak_login">
    <div class="row" style="background-color: #ff0000;margin-top: -30px;margin-bottom: 10px;">
  <p class="tulisan_login"><b>LogIn</b></p>
    </div>
  <form>
    <label>Email Address</label>
    <input type="text" name="username" class="form_login" placeholder="Username atau email ..">

    <label>Password</label>
    <input type="text" name="password" class="form_login" placeholder="Password ..">

    <input type="submit" class="tombol_login" value="LOGIN">

    <br/>
    <br/>

    <!-- <center>
      <a class="link" href="https://www.malasngoding.com">kembali</a>
    </center> -->
  </form>
<p>Belum Punya Akun? Klik <a href="{{ url('daftar-peraturan') }}"><button type="submit" class="btn btn-danger" value="Daftar"> Daftar</button></a> </p>
</div>
<div class="rows marge">
  <div>
  <h4><strong>Selamat Datang di Sistem Informasi Uji Kompetensi Penyesuaian/Inpassing JF PPBJ-LKPP</strong><hr style="width: 64%"></h4>
  <p class="tek">Sebelum mendaftar silahkan membaca prosedur pendaftaran ujian terlebih dahulu <br><strong>HATI-HATI!</strong></p>
  <p class="tek">Banyak penipuan mengatasnamakan LKPP, baik undangan pelatihan maupun ujian yang palsu;<br>LKPP tidak memperjual belikan kunci jawaban dan soal.</p>
  <!-- form LogIn -->
  <!-- <div class="box">
    <div class="content">
      <div class="box-body">
        
      </div>
    </div>
  </div> -->
  <!-- end form LogIn -->

  </div>
</div>
@stop