@extends('layout.app2')

@section('title')
Berkas Ulang Formasi
@endsection

@section('css')
<style>
  body{
    background-color: whitesmoke;
  }

  .main-page{
    margin-top: 20px;
  }

  .box-container{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    background-color: white;
    border-radius: 5px;
    padding: 3%;
  }

  .shadow{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    max-width: 250px;
    line-height: 15px;
    width: 100%;
    margin: 5px;
  }

  div.dataTables_wrapper div.dataTables_info{
    display: none;
  }

  div.dataTables_wrapper .row.col-sm-12{
    width: 120px;
  }

  p{
    font-weight: 500;
  }

  b{
    font-weight: 500;
  }

  .row a.btn{
    text-align: left;
    font-weight: 600;
    font-size: small;
  }

  .form-control{
    margin :5px;
    -webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    -moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    border-radius: 5px;
  }

  textarea{
    margin :5px;
    -webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    -moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
    border-radius: 5px;
  }

  .form-control{
    height: 30px;
  }

  .btn-default1{
      background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
      color: white;
      font-weight: 600;
      width: 100px; 
      margin: 10px;
      -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }

  .btn-default2{
      background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
      color: black;
      font-weight: 600;
      width: 100px; 
      margin: 10px;
      -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
      box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }

  .custom-file label.custom-file-label{
      margin: 5px;
      -webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
      -moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
      box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
  }

  .row-input{
    padding-bottom: 10px;
  }

  .box-container img{
      width: 60px;
  }

  span.berkas-lama{
      margin: 6px;
  }

</style>
@endsection

@section('content')
<div class="main-page">
  <div class="container">
      <div class="row box-container">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-9" style="">
                <form action="" method="post" enctype="multipart/form-data">
                  @csrf
                <div class="row">
                <div class="col-md-12">
                <h5>Berkas Ulang e-Formasi</h5>
                <hr>
                @if (session('msg'))
                    @if (session('msg') == "berhasil")
                      <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Berhasil Simpan Data</strong>
                      </div> 
                    @else
                    <div class="alert alert-warning alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>Gagal Simpan Data</strong>
                    </div> 
                    @endif
                @endif
                </div>
                <form action="" method="post" enctype="multipart/form-data">
                <div class="col-md-11">
                    <p>Dokumen yang anda upload sebelumnya salah atau tidak sesuai dengan format dan ukuran yang di telah di tentukan.Anda hanya perlu mengupload file yang keterangan berkas lamanya = '-'.</p>
                    <div class="alert alert-primary" role="alert">
                      Berkas yang di input berformat:jpg/jpeg/png/pdf. ukuran file minimal 100kb maksimal 2MB.
                    </div>
                    <div class="row row-input">
                      <div class="col-lg-4 col-md-11 col-10">Pertama</div>
                      <div class="col-lg-1 col-md-1 col-1">:</div>
                      <div class="col-lg-7 col-md-12"><input type="text" style="width: 100%" class="form-control" name="pertama" value="{{ $data->pertama }}" oninput="if(value.length>18)value=value.slice(0,18)" pattern="[0-9]" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" disabled>
                      <span class="errmsg">{{ $errors->first('pertama') }}</span>
                      </div>
                    </div>
                    <div class="row row-input">
                      <div class="col-lg-4 col-md-11 col-10">Muda</div>
                      <div class="col-lg-1 col-md-1 col-1">:</div>
                      <div class="col-lg-7 col-md-12"><input type="text" style="width: 100%" class="form-control" name="muda" value="{{ $data->muda }}" oninput="if(value.length>18)value=value.slice(0,18)" pattern="[0-9]" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" disabled>
                      <span class="errmsg">{{ $errors->first('muda') }}</span>
                      </div>
                    </div>
                    <div class="row row-input">
                      <div class="col-lg-4 col-md-11 col-10">Madya</div>
                      <div class="col-lg-1 col-md-1 col-1">:</div>
                      <div class="col-lg-7 col-md-12"><input type="text" style="width: 100%" class="form-control" name="madya" value="{{ $data->madya }}" oninput="if(value.length>18)value=value.slice(0,18)" pattern="[0-9]" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" disabled>
                      <span class="errmsg">{{ $errors->first('madya') }}</span>
                      </div>
                    </div>
                    <div class="row row-input">
                      <div class="col-lg-4 col-md-11 col-10">Dokumen Hasil Perhitungan ABK</div>
                      <div class="col-lg-1 col-md-1 col-1">:</div>
                      <div class="col-lg-7 col-md-12">
                          <div class="custom-file">
                              <input type="file" class="custom-file-input" id="customFile" name="dokumen_hasil_perhitungan_abk" accept="image/jpg, application/pdf, image/jpeg, image/png" value="{{ old('dokumen_hasil_perhitungan_abk') }}">
                              <label class="custom-file-label" for="customFile">{{ old('dokumen_hasil_perhitungan_abk') == "" ? 'Pilih Berkas' : old('dokumen_hasil_perhitungan_abk')}}</label>
                              <span class="errmsg">{{ $errors->first('dokumen_hasil_perhitungan_abk') }}</span>
                            </div>
                            <span class="berkas-lama">
                            berkas lama:
                            @if (is_null($data->dokumen_hasil_perhitungan_abk) || $data->dokumen_hasil_perhitungan_abk == "-")
                                -
                            @else
                                @php
                                $dokumen_hasil_perhitungan_abk = explode('.',$data->dokumen_hasil_perhitungan_abk);
                                @endphp
                                @if ($dokumen_hasil_perhitungan_abk[1] == 'pdf' || $dokumen_hasil_perhitungan_abk[1] == 'PDF')
                                <a href="{{ url('priview-file')."/dokumen_hasil_perhitungan_abk/".$data->dokumen_hasil_perhitungan_abk }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                @else
                                <a href="{{ url('priview-file')."/dokumen_hasil_perhitungan_abk/".$data->dokumen_hasil_perhitungan_abk }}" target="_blank"><img src="{{ asset('storage/data/dokumen_hasil_perhitungan_abk')."/".$data->dokumen_hasil_perhitungan_abk }}" class="img-rounded"></a>
                                @endif 
                            @endif
                            </span>
                      </div>
                    </div>
                    <div class="row row-input">
                      <div class="col-lg-4 col-md-11 col-10">Surat Usulan/Rekomendasi Mengikuti Inpassing</div>
                      <div class="col-lg-1 col-md-1 col-1">:</div>
                      <div class="col-lg-7 col-md-12">
                          <div class="custom-file">
                              <input type="file" class="custom-file-input" id="customFile" name="surat_usulan" accept="image/jpg, application/pdf, image/jpeg, image/png" value="c:/text.jpg">
                              <label class="custom-file-label" for="customFile">{{ old('surat_usulan') == "" ? 'Pilih Berkas' : old('surat_usulan')}}</label>
                              <span class="errmsg">{{ $errors->first('surat_usulan') }}</span>
                          </div>
                          <span class="berkas-lama">
                          berkas lama:
                            @if (is_null($data->surat_usulan_rekomendasi)|| $data->surat_usulan_rekomendasi == "-" )
                                -
                            @else
                                @php
                                $surat_usulan = explode('.',$data->surat_usulan_rekomendasi);
                                @endphp
                                @if ($surat_usulan[1] == 'pdf' || $surat_usulan[1] == 'PDF')
                                <a href="{{ url('priview-file')."/surat_usulan/".$data->surat_usulan_rekomendasi }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                @else
                                <a href="{{ url('priview-file')."/surat_usulan/".$data->surat_usulan_rekomendasi }}" target="_blank"><img src="{{ asset('storage/data/surat_usulan')."/".$data->surat_usulan_rekomendasi }}" class="img-rounded"></a>
                                @endif 
                            @endif
                            </span>
                      </div>
                    </div>
                    <div class="row row-input">
                        <div class="col-lg-4 col-md-11 col-10">No Surat Usulan/Rekomendasi Mengikuti Inpassing</div>
                        <div class="col-lg-1 col-md-1 col-1">:</div>
                        <div class="col-lg-7 col-md-12"><input type="text" name="no_surat" style="width: 100%" class="form-control" value="{{ $data->no_surat_usulan_rekomendasi }}" disabled>
                        <span class="errmsg">{{ $errors->first('no_surat') }}</span>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-11" style="text-align : right">
                    <button type="reset" class="btn btn-default2">Batal</button> <button type="submit" class="btn btn-default1">Simpan</button>
                  </div>
                </div>
                </form>
              </div>
              <div class="col-md-3 text-center">
                @include('layout.button_right_kpp')
              </div>
            </form>
            </div>
          </div> 
        </div>
    </div>
  </div>
  @endsection

  @section('js')
  <script>
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });    
  </script>
  <script>document.foo.submit();</script>
  <script>
      document.addEventListener("DOMContentLoaded", function() {
          var elements = document.getElementsByTagName("INPUT");
          for (var i = 0; i < elements.length; i++) {
              elements[i].oninvalid = function(e) {
                  e.target.setCustomValidity("");
                  if (!e.target.validity.valid) {
                      e.target.setCustomValidity("Silakan isi kolom berikut.");
                  }
              };
              elements[i].oninput = function(e) {
                  e.target.setCustomValidity("");
              };
          }
      })
  </script>
  @endsection