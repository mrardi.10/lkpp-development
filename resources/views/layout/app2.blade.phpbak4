@inject('request', 'Illuminate\Http\Request')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ asset('assets/date_picker/bootstrap-datepicker.css') }}">
  <!-- select2 -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    
  <!-- select2-bootstrap4-theme -->
  <link href="select2-bootstrap4.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
  <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
</head>
<style>
  .navbar{
    margin-bottom: 0px !important;
    background-color: #3a394e !important;
    border-color: #3a394e !important;
    border-radius: 0px !important;
    padding: 0px !important;
  }

  /* .navbar-nav{
    margin-left: 50px;
    margin-right: 50px; 
  } */

  .navbar-dark .navbar-nav .active>.nav-link, .navbar-dark .navbar-nav .nav-link.active, .navbar-dark .navbar-nav .nav-link.show, .navbar-dark .navbar-nav .show>.nav-link{
    color: #ff4141;
    background-color: #3a394e;
    font-weight: 600;
    border-bottom: 3px solid #ff4141;
  }

  .navbar-dark .navbar-nav .nav-link{
    color: white;
  }

  .navbar-dark .navbar-nav .nav-link:hover{
    color: #ff4141;
  }

  .navbar-nav>li>a{
    padding-top: 20px !important;
    padding-bottom: 20px !important; 
    /* padding-left: 35px;
    padding-right: 35px; */
  }

  .navbar-expand-lg .navbar-nav .nav-link{
    padding-right: 2rem;
    padding-left: 2rem;
  }

  .navbar-toggler{
    margin: 10px;
  }

  table.dataTable{
      margin-top: 0px !important;
      margin-bottom: 0px !important;
    }

    .dataTables_wrapper div.row div.col-md-6{
      display: none;
    }

    div.min-top{
      background: #655E71;
      height: auto;
      padding: 1%;
      border-radius: 5px 5px 0px 0px;
    }

    div.min-top b{
      color: #fff;
    }

    .table-striped>tbody>tr{
      height: 20px;
    }

    button.btn-action{
      width: 100%;
      font-weight: 600;
      background: white;
      border-radius: 50px;
      border: 0px;
      -webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.32);
      -moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.32);
      box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.32);
    }

    .dataTables_info{
      display: none;
    }

    ul.pagination{
      margin: 2% !important;
      font-weight: 600;
    }

    .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
      background-color: red !important;
      border-color: red !important;
    }

    table tr th{
      vertical-align: text-top !important;
      background-color: #D4D4D4 !important;
      border-color: #D4D4D4 !important;
      color: #5B5A66;
    }

    .main-box{
      -webkit-box-shadow: 0px 4px 2px 0px rgba(0,0,0,0.14);
      -moz-box-shadow: 0px 4px 2px 0px rgba(0,0,0,0.14);
      box-shadow: 0px 4px 2px 0px rgba(0,0,0,0.14);
      border-radius: 5px; 
		  background: #fff;
    }

    .field-icon {
      float: right;
      margin-left: -25px;
      margin-top: -25px;
      margin-right: 5px;
      position: relative;
      z-index: 2;
    }

    span.errmsg{
      color: red;
    }

    i.fa-file-pdf{
      cursor: pointer;
    }

    .custom-file-control:before {
      content: "Cari";
    }
    .custom-file-control:empty::after {
      content: "Choose a file...";
    }

    /* Specific for spanish language */
    .custom-file-control:lang(es)::before {
      content : "Buscar";
    }
    .custom-file-control:lang(es):empty::after {
      content : "Seleccionar un fichero...";
    }

    /* Start by setting display:none to make this hidden.
    Then we position it in relation to the viewport window
    with position:fixed. Width, height, top and left speak
    for themselves. Background we set to 80% white with
    our animation centered, and no-repeating */
    .modal-ajax {
        display:    none;
        position:   fixed;
        z-index:    1000;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;
        background: rgba( 255, 255, 255, .8 ) 
                    url("{{ asset('assets/gif/loading.gif?v=2') }}") 
                    50% 50% 
                    no-repeat;
    }

    /* When the body has the loading class, we turn
      the scrollbar off with overflow:hidden */
    body.loading .modal-ajax {
        overflow: hidden;   
    }

    /* Anytime the body has the loading class, our
      modal element will be visible */
    body.loading .modal-ajax {
        display: block;
    }
</style>
@yield('css')

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
        <div class="container">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item {{ in_array($request->segment(1), ['home','inpassing','']) ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('') }}">Beranda
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item {{ in_array($request->segment(1), ['prosedur']) ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('prosedur') }}">Prosedur Pendaftaran</a>
              </li>
              <li class="nav-item {{ in_array($request->segment(1), ['jadwal-inpassing-lkpp','menu-peserta-inpassing']) ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('jadwal-inpassing-lkpp') }}" style="font-size:15px; padding-bottom: 8px !important; padding-top: 9px !important">Jadwal Uji Kompetensi <br>(Reguler LKPP)</a>
              </li>
              <li class="nav-item {{ in_array($request->segment(1), ['jadwal-inpassing-penyelenggara','menu-peserta-instansi']) ? 'active' : '' }}">
                  <a class="nav-link" href="{{ url('jadwal-inpassing-penyelenggara') }}" style="font-size:15px; padding-bottom: 8px !important; padding-top: 9px !important">Jadwal Uji Kompetensi <br>(Instansi)</a>
              </li>
              <li class="nav-item {{ in_array($request->segment(1), ['statik-plaksanaan-ujian']) ? 'active' : '' }}">
                  <a class="nav-link" href="{{ url('statik-plaksanaan-ujian') }}">Statistik</a>
              </li>
              <li class="nav-item {{ in_array($request->segment(1), ['kontak']) ? 'active' : '' }}">
                  <a class="nav-link" href="{{ url('kontak') }}">Kontak</a> 
              </li>
            </ul>
          </div>
        </div>
      </nav>
  
<div class="content">
  @yield('content')
</div>
<div class="modal-ajax"><!-- Place at bottom of page --></div>   
</body>
<script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('assets/date_picker/bootstrap-datepicker.js') }}"></script>
<!-- select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<!-- select2-bootstrap4-theme -->
<script src="script.js"></script>
<script>
  if ($("#selector").length > 0){
   $("#selector").select2();
  }
    $(function () {
      $('#example1').DataTable()
      $('#example2').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false
      })
    })
    oTable = $('#example1').DataTable({
    language: {
        paginate: {
            previous: 'Sebelumnya',
            next:     'Selanjutnya'
        },
        aria: {
            paginate: {
                previous: 'Sebelumnya',
                next:     'Selanjutnya'
            }
        },
        emptyTable: 'Tidak terdapat data di tabel'
    }
});
    $('#myInputTextField').keyup(function(){
          oTable.search($(this).val()).draw();
    })
    $('#length_change').val(oTable.page.len());
    $('#length_change').change( function() { 
      oTable.page.len( $(this).val() ).draw();
    });
</script>
<script>
    $('.table-responsive').on('show.bs.dropdown', function () {
         $('.table-responsive').css( "overflow", "inherit" );
    });
    
    $('.table-responsive').on('hide.bs.dropdown', function () {
         $('.table-responsive').css( "overflow", "auto" );
    })
</script>
<script>
function validate(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
  // Handle key press
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9]/;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
</script>
<script>
$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }    
});
</script>
@yield('js')
</html>
