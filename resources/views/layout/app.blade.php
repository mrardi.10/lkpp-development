@inject('request', 'Illuminate\Http\Request')
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>LKPP | @yield('title')</title>
		
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<!-- Bootstrap 3.3.7 -->
		<link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
		
		<!-- Font Awesome -->
		<link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
		
		<!-- Ionicons -->
		<link rel="stylesheet" href="{{ asset('assets/bower_components/Ionicons/css/ionicons.min.css') }}">
		
		<!-- Select2 -->
		<link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
		
		<!-- Theme style -->
		<link rel="stylesheet" href="{{ asset('assets/dist/css/AdminLTE.min.css') }}">
		
		<!-- AdminLTE Skins. Choose a skin from the css/skins
		folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="{{ asset('assets/dist/css/skins/_all-skins.min.css?v=4') }}">
		
		<!-- Morris chart -->
		<link rel="stylesheet" href="{{ asset('assets/bower_components/morris.js/morris.css') }}">
		
		<!-- jvectormap -->
		<link rel="stylesheet" href="{{ asset('assets/bower_components/jvectormap/jquery-jvectormap.css') }}">
		
		<!-- Date Picker -->
		<link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
		
		<!-- Daterange picker -->
		<link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
		
		<!-- bootstrap wysihtml5 - text editor -->
		<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
		
		<!-- DataTables -->
		<link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/date_picker/bootstrap-datepicker3.css') }}">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- Google Font -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		@yield('css')
		<style type="text/css">
			.i{
				margin: 10px;
				margin-bottom: 0px;
				margin-top: 0px;
				margin-left: 0px;
			}
			
			ul li a img:hover{
				filter: invert(40%) grayscale(100%) brightness(89%) sepia(200%) hue-rotate(-50deg) saturate(400%) contrast(1);
			}
			
			ul li.active a img{
				filter: invert(40%) grayscale(100%) brightness(89%) sepia(200%) hue-rotate(-50deg) saturate(400%) contrast(1);
			}
			
			.main-header .judul-halaman {float: left;font-size:15px;padding: 15px 15px;color:#fff;font-weight:bold;font-weight:600;}
			li.over {
				height: auto;
				width: auto;
			}
			
			.dataTables_info{
				display: none;
			}

			ul.pagination{
				margin: 2% !important;
				font-weight: 600;
			}

			.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
				background-color: red !important;
				border-color: red !important;
			}

			table tr th{
				vertical-align: text-top !important;
				background-color: #D4D4D4 !important;
				border-color: #D4D4D4 !important;
			}

			.main-box{
				-webkit-box-shadow: 0px 4px 2px 0px rgba(0,0,0,0.14);
				-moz-box-shadow: 0px 4px 2px 0px rgba(0,0,0,0.14);
				box-shadow: 0px 4px 2px 0px rgba(0,0,0,0.14);
				border-radius: 5px; 
				background: #fff;
			}

			table.dataTable{
				margin-top: 0px !important;
				margin-bottom: 0px !important;
			}

			.dataTables_wrapper div.row div.col-sm-6{
				display: none;
			}

			div.min-top{
				background: #655E71;
				height: auto;
				padding: 1%;
				border-radius: 5px 5px 0px 0px;
			}

			div.min-top b{
				color: #fff;
			}

			.table-striped>tbody>tr{
				font-weight: 600;
				height: 20px;
			}

			button.btn-action{
				width: 100%;
				font-weight: 600;
				background: white;
				border-radius: 50px;
				border: 0px;
				-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.32);
				-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.32);
				box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.32);
			}

			.btn-default1{
				background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
				color: white;
				font-weight: 600;
				width: 100px; 
				margin: 10px;
				-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
				-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
				box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
			}
			
			.btn-default3{
				background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
				color: white;
				font-weight: 600;
				width: 150px; 
				margin: 10px;
				-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
				-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
				box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
			}

			.btn-default2 {
				background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
				color: black;
				font-weight: 600;
				width: 100px; 
				margin: 10px;
				-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
				-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
				box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
			}

			.skin-blue .sidebar-menu>li>.treeview-menu{
				background: #f5f5f5 !important;
			}

			.skin-blue .sidebar-menu .treeview-menu>li>a {
				color: #615e5e !important;
			}

			.skin-blue .sidebar-menu .treeview-menu>li>a:hover {
				color: #000 !important;
			}
			
			ul.dropdown-menu{
				right: 0px;
				left: auto;
			}

			.ck.ck-editor__editable_inline>:last-child{
				font-weight: 200 !important;
			}

			.field-icon {
				float: right;
				margin-left: -25px;
				margin-top: -25px;
				margin-right: 5px;
				position: relative;
				z-index: 2;
			}

			span.errmsg{
				color: red;
			}
			
			i.fa-file-pdf-o{
				cursor: pointer;
				font-size: 45px !important;
			}
		</style>
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<!-- Logo -->
				<a href="{{ url('') }}" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
					<img src="{{ asset('assets/img/logo.png')}}" width="130">
				</a>
				
				<!-- Header Navbar: style can be found in header.less -->
				<nav class="navbar navbar-static-top">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
						<span class="sr-only"></span>
					</a>
					<div class="judul-halaman" >@yield('title')</div>
						<!-- User Account: style can be found in dropdown.less -->
						<div class="navbar-custom-menu">
							<ul class="nav navbar-nav">
								<li class="dropdown user user-menu">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										@if(Auth::user()->role == 'dsp')
											<img src="{{ asset('assets/dist/img/avatar5.png') }}" class="user-image" alt="User Image">
										@elseif(Auth::user()->role == 'superadmin')
											<img src="{{ asset('assets/dist/img/avatar04.png') }}" class="user-image" alt="User Image">
										@elseif(Auth::user()->role == 'bangprof')
											<img src="{{ asset('assets/dist/img/avatar3.png') }}" class="user-image" alt="User Image">
										@elseif(Auth::user()->role == 'verifikator')
											<img src="{{ asset('assets/dist/img/avatar2.png') }}" class="user-image" alt="User Image">
										@elseif(Auth::user()->role == 'asesor')
											<img src="{{ asset('assets/dist/img/avatar.png') }}" class="user-image" alt="User Image">
										@endif
											<span class="hidden-xs">{{ Auth::user()->name}}</span>
									</a>
									<ul class="dropdown-menu">
										<!-- User image -->
										<li class="user-header">
										@if(Auth::user()->role == 'dsp')
											<img src="{{ asset('assets/dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
										@elseif(Auth::user()->role == 'superadmin')
											<img src="{{ asset('assets/dist/img/avatar04.png') }}" class="img-circle" alt="User Image">
										@elseif(Auth::user()->role == 'bangprof')
											<img src="{{ asset('assets/dist/img/avatar3.png') }}" class="img-circle" alt="User Image">
										@elseif(Auth::user()->role == 'verifikator')
											<img src="{{ asset('assets/dist/img/avatar2.png') }}" class="img-circle" alt="User Image">
										@elseif(Auth::user()->role == 'asesor')
											<img src="{{ asset('assets/dist/img/avatar.png') }}" class="img-circle" alt="User Image">
										@endif
											<p>
												{{ Auth::user()->name}}
												<small>{{Auth::user()->role}}</small>
											</p>
										</li>
										<!-- Menu Body -->
										<!-- Menu Footer-->
										<li class="user-footer">
											<div class="pull-right">
												<a href="{{ route('logout') }}" class="btn btn-default btn-flat"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Keluar</a>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
				</nav>
			</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
			<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu" data-widget="tree">
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'verifikator' || Auth::user()->role == 'asesor')
					<li class=" {{ $request->segment(1) == 'home-lkpp' ? 'active' : '' }}">
						<a href="{{ url('dashboard') }}">
							<img src="{{ asset('assets/img/Group 274.png')}}" class="i"> <span>Dashboard</span>
						</a>  
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof')
					<li class="{{ in_array($request->segment(1), ['pengusulan-eformasi-lkpp','verifikasi-usulan']) ? 'active' : '' }}">
						<a href="{{ url('pengusulan-eformasi-lkpp')}}">
							<i class="fa fa-wpforms"></i>
							<span> Pengusulan Formasi</span>
						</a>  
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'verifikator')
					<li class="{{ in_array($request->segment(1), ['data-peserta','verifikasi-peserta']) ? 'active' : '' }}">
						<a href="{{ url('data-peserta') }}">
							<i class="fa fa-users"></i> 
							<span> Data Peserta</span>
						</a>
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'verifikator')
					<li class="{{ in_array($request->segment(1), ['data-admin','data-admin']) ? 'active' : '' }}">
						<a href="{{ url('data-admin' )}}">
							<i class="fa fa-user"></i>
							<span> Data Admin PPK</span>
						</a>
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin')
					<li class="">
						<a href="{{ url('data-admin-lkpp') }}">
							<i class="fa fa-black-tie"></i> 
							<span> Data Admin LKPP</span>
						</a>
					</li>
				@endif
				
				@if (Auth::user()->role == 'bangprof' || Auth::user()->role == 'superadmin')
					<li class="">
						<a href="{{ url('data-verifikator') }}">
							<i class="fa fa-file-text"></i> 
							<span> Data Verifikator</span>
						</a>
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin'  || Auth::user()->role == 'dsp')
					<li class="">
						<a href="{{ url('data-asesor') }}">
							<i class="fa fa-cubes"></i> 
							<span> Data Asesor</span>
						</a>
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'asesor' || Auth::user()->role == 'verifikator')
					<li class="">
						<a href="{{ url('jadwal-inpassing')}}">
							<i class="fa fa-calendar-check-o"></i>
							<span>  Jadwal Uji Kompetensi <br>
								<div style="margin-left: 11%">  (Reguler LKPP) </div>
							</span>
						</a>
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'verifikator'  )
					<li class="">
						<a href="{{ url('jadwal-inpassing-instansi')}}">
							<i class="fa fa-calendar-check-o"></i>
							<span> Jadwal Uji Kompetensi  <br>
								<div style="margin-left: 11%">(Instansi)</div>
							</span>
						</a>
					</li>
				@endif
				{{-- @if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
					<li class="">
						<a href="{{ url('verifikasi-portofolio')}}">
							<i class="fa fa-newspaper-o"></i>
							<span> Verifikasi Portofolio</span>
						</a>
					</li>
				@endif --}}
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
					<li class="">
						<a href="{{ url('generate-noseri') }}">
							<i class="fa fa-lightbulb-o"></i>
							<span> Generate No Seri </span>
						</a>  
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'verifikator')
					<li class="">
						<a href="{{ url('cetak-sertifikat') }}">
							<i class="fa fa-print"></i>
							<span> Cetak Sertifikat</span>
						</a>
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'verifikator')
					<li class="">
						<a href="{{ url('cetak-sktl') }}">
							<i class="fa fa-print"></i>
							<span> Cetak SKTL</span>
						</a>
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'verifikator')
					<li class="">
						<a href="{{ url('pertek-sphu') }}">
							<i class="fa fa-newspaper-o"></i>
							<span> Cetak Pertek & SPHU</span>
						</a>  
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
					<li class="">
						<a href="{{ url('import-hasil-ujian') }}">
							<i class="fa fa-database"></i>
							<span> Import Hasil Ujian Inpassing</span>
						</a>
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->role == 'verifikator')
					<li class="">
						<a href="{{ url('riwayat-ujian-peserta') }}">
							<i class="fa fa-list-alt"></i>
							<span> Riwayat Ujian Peserta</span>
						</a>
					</li>
				@endif
				
				@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
					<li class="treeview">
						<a href="#">
							<i class="fa fa-cog"></i>
							<span>Setting</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
						@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
							<li><a href="{{ url('data-register') }}"><i class="fa fa-circle-o"></i>Menu Register</a></li>
						@endif
						
						@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
							<li><a href="{{ url('text-running') }}"><i class="fa fa-circle-o"></i>Text Running</a></li>
						@endif
						
						@if (Auth::user()->role == 'superadmin')
							<li><a href="{{ url('data-instansi') }}"><i class="fa fa-circle-o"></i>Data Instansi</a></li>
						@endif
						
						@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
							<li><a href="{{ url('data-prosedur') }}"><i class="fa fa-circle-o"></i>Menu Prosedur</a></li>
						@endif
						
						@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
							<li><a href="{{ url('data-statistik') }}"><i class="fa fa-circle-o"></i>Menu Statistik</a></li>
						@endif
						
						@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
							<li><a href="{{ url('data-kontak') }}"><i class="fa fa-circle-o"></i>Menu Kontak</a></li>
						@endif
						
						@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
							<li><a href="{{ url('data-sertifikat') }}"><i class="fa fa-circle-o"></i>Data Sertifikat</a></li>
						@endif
						
						@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'asesor' || Auth::user()->role == 'dsp')
							<li><a href="{{ url('data-deskripsi-portofolio') }}"><i class="fa fa-circle-o"></i>Deskripsi Portofolio</a></li>
						@endif
						</ul>
					</li>
				@endif
				<li class="">
					<a href="#" class="btn btn-default shadow" style="color: red;" onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">
						<i class="fa fa-sign-out"></i>
						<span> Keluar</span>
					</a>
				</li>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
				</form>
			</ul>
		</section>
	<!-- /.sidebar -->
	</aside>
	
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			@yield('content')
			<!-- /.row (main row) -->
		</section>
		<!-- /.content -->
	</div>	
	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			{{-- <b>Version</b> 2.4.0 --}}
		</div>
		<strong>
			Inpassing LKPP &copy; 2019. All rights reserved.
		</strong>
	</footer>
	<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="{{ asset('assets/bower_components/jquery/dist/jquery.min.js') }}"></script>

<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('assets/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<!-- Morris.js charts -->
<script src="{{ asset('assets/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('assets/bower_components/morris.js/morris.min.js') }}"></script>

<!-- Sparkline -->
<script src="{{ asset('assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>

<!-- jvectormap -->
<script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>

<!-- jQuery Knob Chart -->
<script src="{{ asset('assets/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>

<!-- datepicker -->
<script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>

<!-- Slimscroll -->
<script src="{{ asset('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('assets/bower_components/fastclick/lib/fastclick.js') }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('assets/dist/js/pages/dashboard.js') }}"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/dist/js/demo.js') }}"></script>
<script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/date_picker/bootstrap-datepicker.js') }}"></script>

<!-- datepicker bootstrap -->
{{-- <script type="text/javascript" src="{{ asset('bower_components/jquery/jquery.min.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
{{-- <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" /> --}}
<link rel="stylesheet" href="{{ asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />
<script src="{{ asset('assets/ckeditor/ckeditor.js') }}"></script>
<script>
	$(function () {
		$('.select2').select2()
		$('[data-mask]').inputmask()
		$('#example1').DataTable()
		$('#example3').DataTable()
		$('#example2').DataTable({
			'paging'      : false,
			'lengthChange': false,
			'searching'   : false,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : false,
		})
	})
	oTable = $('#example1').DataTable({
		language: {
			paginate: {
				previous: 'Sebelumnya',
				next:     'Selanjutnya'
			},
			aria: {
				paginate: {
					previous: 'Sebelumnya',
					next:     'Selanjutnya'
				}
			},
			emptyTable: 'Tidak terdapat data dalam Tabel'
		}
	});
	
	$('#myInputTextField').keyup(function(){
		oTable.search($(this).val()).draw();
	})
	
	$('#length_change').val(oTable.page.len());
	$('#length_change').change( function() { 
		oTable.page.len( $(this).val() ).draw();
	});
	
	oTable1 = $('#example3').DataTable({
		language: {
			paginate: {
				previous: 'Sebelumnya',
					next:     'Selanjutnya'
			},
			aria: {
				paginate: {
					previous: 'Sebelumnya',
					next:     'Selanjutnya'
				}
			},
			emptyTable: 'Tidak terdapat data dalam Tabel'
		}
	});
	
	$('#myInputTextField1').keyup(function(){
		oTable1.search($(this).val()).draw();
	})
	
	$('#length_change1').val(oTable1.page.len());
	$('#length_change1').change( function() { 
		oTable1.page.len( $(this).val() ).draw();
	});

	$('.table-responsive').on('show.bs.dropdown', function () {
		$('.table-responsive').css( "overflow", "inherit" );
	});

	$('.table-responsive').on('hide.bs.dropdown', function () {
		$('.table-responsive').css( "overflow", "auto" );
	})

	function validate(evt) {
		var theEvent = evt || window.event;

		// Handle paste
		if (theEvent.type === 'paste') {
			key = event.clipboardData.getData('text/plain');
		} else {
			// Handle key press
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode(key);
		}
		
		var regex = /[0-9]/;
		if( !regex.test(key) ) {
			theEvent.returnValue = false;
			if(theEvent.preventDefault) theEvent.preventDefault();
		}
	}
</script>
@yield('js')
</body>
</html>