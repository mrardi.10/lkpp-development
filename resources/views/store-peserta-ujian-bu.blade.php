@extends('layout.app2')

@section('title')
Tambah Peserta Uji Kompetensi (Reguler LKPP)
@endsection

@section('css')
<style>
  body{
    background-color: whitesmoke;
  }
  .main-page{
    margin-top: 20px;
  }

  .box-container{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
    background-color: white;
    border-radius: 5px;
    padding: 3%;
  }

  .shadow{
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    -moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
    max-width: 250px;
    line-height: 15px;
    width: 100%;
    margin: 5px;
  }

  div.dataTables_wrapper div.dataTables_info{
    display: none;
  }

  div.dataTables_wrapper .row.col-sm-12{
    width: 120px;
  }

  p{
    font-weight: 500;
  }

  b{
    font-weight: 500;
  }

  .row a.btn{
    text-align: left;
    font-weight: 600;
    font-size: small;
  }

  .btn-default1{
    background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
    color: white;
    font-weight: 600;
    width: auto; 
    margin: 10px;
    -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }

  .btn-default2{
    background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
    color: black;
    font-weight: 600;
    width: 100px; 
    margin: 10px;
    -webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    -moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
    box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
  }

  .btn-default3{
    background: #fff;
    color: #000;
    font-weight: 500;
    width: 100px;
  }


  .row-input{
    padding-bottom: 10px;
  }

  .page div.verifikasi{
    display: none;
  }

  .btn-area{
    text-align: right;
  }

  .clickable-row:hover{
    cursor: pointer;
  }

  .checkmark {
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #00aaaa;
  }

  .modal .modal-dialog{
    max-width: 800px !important;
  }

  .line-center{
    line-height: 2.5;
  }
</style>
@endsection

@section('content')
<div class="main-page">
  <div class="container">
    <div class="row box-container">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-12" style="">
            <div class="row">
              <div class="col-md-9">
                <h5>Pilih Peserta Uji Kompetensi ({{ $jadwal->metode == 'verifikasi_portofolio' ? 'Verifikasi Portofolio' : 'Tes Tertulis' }}) {{-- $jadwal->metode == 'verifikasi_portofolio' ? 'Verifikasi Portofolio' : 'Tes Tertulis' --}} tanggal {{ $jadwal->metode == 'tes_tulis' ? Helper::tanggal_indo($jadwal->tanggal_tes) : Helper::tanggal_indo($jadwal->tanggal_verifikasi) }} {{-- dengan Metode  --}}
                </h5>



                @php
                $tanggal = $jadwal->batas_waktu_input;
                date_default_timezone_set("Asia/Jakarta");
                $date = $jadwal->batas_waktu_input;
                $today = strtotime("today midnight");
                if($today > strtotime($date)){
                  $batas_input = "expired";
                } else {
                  $batas_input = "active";
                }
                @endphp
                @if ($batas_input == "expired")
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Ujian telah melewati batas akhir pendaftaran, silakan pilih tanggal ujian berikutnya.</strong>
                </div>    
                @endif
                <hr>
                @if (session('msg'))
                @if (session('msg') == "penuh")
                          <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong> Ujian telah melewati batas kuota peserta, silakan pilih tanggal ujian yang lain.</strong>
                          </div> 
                          @endif
                @if (session('msg') == "metode_kosong")
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Anda Belum Memilih Peserta. Silakan cek pada kolom Daftarkan Peserta</strong>
                </div> 
                @endif
                @if (session('msg') == "validated_berkas")
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Anda Belum Mengunggah Portofolio. Silakan unggah berkas pada kolom Unggah Dokumen Portofolio</strong>
                </div> 
                @endif
                @endif
                @if($jadwal->metode == 'verifikasi_portofolio')
                <div class="row alert-row">
                  <div class="col-md-12">
                    <div class="alert alert-info" role="alert" style="text-align: justify;">
                      Peserta dapat mengumpulkan Dokumen Portofolio Jenjang di atasnya, namun tidak dapat mengumpulkan Dokumen Portofolio Jenjang di bawahnya. Contoh: Calon JF PPBJ Pertama dapat mengunggah Dokumen Portofolio untuk JF PPBJ Muda/Madya, tetapi calon JF PPBJ Madya tidak boleh mengunggah Dokumen Portofolio JF PPBJ Pertama/Muda. 
                    </div>
                  </div>
                </div>
                @endif
              </div>
              <div class="col-md-3" style="text-align:right">
                <div class="dropdown dropdown-notifications">
                  <button class="btn btn-sm btn-danger" data-toggle="dropdown" title="Rekomendasi Asessor" type="button"><i class="fa fa-bell"></i></button>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <h6 class="dropdown-header">Notifikasi Rekomendasi Assesor</h6>
                    <div class="dropdown-divider"></div>
                    @foreach ($notifikasiRekomendasi as $notif)
                    <li class="dropdown-item">{{ 'Tanggal '.Helper::tanggal_indo($notif->tanggal).' '.$notif->nama_peserta.' perlu perbaikan bukti dukung dari kompetensi yg Tidak Sesuai' }}</li>
                    @endforeach
                  </ul>
                </div>
              </div>
              <div class="col-md-9 page">
                <div class="main-box" id="view">
                  <div class="min-top">
                    <div class="row">
                      <div class="col-md-2 text-center">
                        <b>Perlihatkan</b>
                      </div>
                      <div class="col-md-2">
                        <select name='length_change' id='length_change' class="form-control form-control-sm">
                          <option value='50'>50</option>
                          <option value='100'>100</option>
                          <option value='150'>150</option>
                          <option value='200'>200</option>
                        </select>
                      </div>
                      <div class="col-md-3 col-12">
                        <div class="form-group" style="margin-bottom:0px !important">
                          <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fa fa-search"></i></span>
                            </div>
                            <input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
                          </div>
                        </div>
                      </div>
                    </div> 
                  </div>
                  <form action="" method="post" id="store">
                    @csrf
                    <input type="hidden" name="tanggal_ujian" value="{{ $jadwal->metode == 'tes_tulis' ? Helper::tanggal_indo($jadwal->tanggal_tes) : Helper::tanggal_indo($jadwal->tanggal_verifikasi) }}">
                    <input type="hidden" name="lokasi_ujian" value="{{ $jadwal->lokasi_ujian }}">
                    <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                    <input type="hidden" name="id_admin_ppk" value="{{ Auth::user()->id }}">
                    <div class="table-responsive">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th width="5%">No.</th>
                            <th width="20%">Nama</th>
                            <th width="15%">NIP</th>
                            <th width="15%">Pangkat/Gol.</th>
                            <th width="15%">Jenjang</th>
                            {{-- @if ($batas_input == "active" || $batas_input == "expired" && $status_peserta == "avail")  --}}
                            <th width="5%">Daftarkan Peserta </th>
                            @if ($jadwal->metode == 'verifikasi_portofolio')
                            <th width="25%"></th>
                            {{-- @endif --}}
                            @endif
                          </tr>
                        </thead>
                        <tbody>
                          @php
                              $no_urut = 1;
                          @endphp

             @foreach ($peserta as $key => $pesertas)
             @php
                                      $status_peserta = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $cek_ada1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $cek_ada2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $cek_ada3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                          if($cek_ada1 != "" || $cek_ada2 != "" || $cek_ada3 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }
                                    
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $cek_ada4 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($cek_ada4 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }

                                  }
                                }
                                @endphp


{{--                          @if($cekPeserta != 0 && $pesertas->status_inpassing != 6)--}}
                          <form action="" method="post"></form>
                          {{-- @if (in_array($pesertas->id, $cek_array, true)) --}}
                          <tr>
                            <td>{{ $no_urut++ }}</td>
                            {{-- <td>{{ $key++ + 1 }}</td> --}}
                            <td>{{ $pesertas->nama }}</td>
                            <td>{{ $pesertas->nip }}</td>
                            <td>{{ $pesertas->jabatan }}</td>
                            <td>{{  ucwords($pesertas->jenjang) }}</td>
                            @if ($batas_input == "active" || $batas_input == "expired" && $status_peserta == "avail") 
                            <td class="text-center">
                              <div class="checkbox">
                                @php
                                      $status_check = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                    if($chechked1 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked1->metode_ujian;
                                    }
                                    elseif($chechked3 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked3->metode_ujian;
                                    }elseif($chechked2 != ""){
                                      $status_check = "";
                                      $metod = "";
                                    }
                                    else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($chechked != ""){
                                      $status_check = "checked";
                                      $metod = $chechked->metode_ujian;
                                    }else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                @endphp
                                <input type="hidden" class="" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkboxx{{ $pesertas->id }}">
                                
                                <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox{{ $pesertas->id }}" {{ $status_check }}>
                                

                              </td>

                              @if ($jadwal->metode == 'verifikasi_portofolio')
                              <td align="center">
                                @if ($jadwal->metode == 'verifikasi_portofolio')
                                <form action="{{ url('unggah-dokumen-ujian') }}" method="post" id="unggah{{ $pesertas->id }}">
                                  @csrf
                                  <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                  <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                  <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) { 
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>    

      @endif
      @if ($jadwal->metode == 'tes_tulis')
      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
      <label>
        Tes tertulis
      </label>   
      @endif
    </td>
    @endif
    @endif
    <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
    <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
    <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
  </tr>
  {{--@endif--}}
  @endforeach 

   @foreach ($pesertaTerdaftar_tl as $key => $pesertas)
   @php
                                      $status_peserta = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $cek_ada1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $cek_ada2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $cek_ada3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                          if($cek_ada1 != "" || $cek_ada2 != "" || $cek_ada3 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }
                                    
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $cek_ada4 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($cek_ada4 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }

                                  }
                                }
                                @endphp


{{--                          @if($cekPeserta != 0 && $pesertas->status_inpassing != 6)--}}
                          <form action="" method="post"></form>
                          {{-- @if (in_array($pesertas->id, $cek_array, true)) --}}
                          <tr>
                            <td>{{ $no_urut++ }}</td>
                            {{-- <td>{{ $key++ + 1 }}</td> --}}
                            <td>{{ $pesertas->nama }}</td>
                            <td>{{ $pesertas->nip }}</td>
                            <td>{{ $pesertas->jabatan }}</td>
                            <td>{{  ucwords($pesertas->jenjang) }}</td>
                            @if ($batas_input == "active" || $batas_input == "expired" && $status_peserta == "avail") 
                            <td class="text-center">
                              <div class="checkbox5">
                                @php
                                      $status_check = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                    if($chechked1 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked1->metode_ujian;
                                    }
                                    elseif($chechked3 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked3->metode_ujian;
                                    }elseif($chechked2 != ""){
                                      $status_check = "";
                                      $metod = "";
                                    }
                                    else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($chechked != ""){
                                      $status_check = "checked";
                                      $metod = $chechked->metode_ujian;
                                    }else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                @endphp
                                <input type="hidden" class="" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox55{{ $pesertas->id }}">
                                
                                <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox5{{ $pesertas->id }}" {{ $status_check }}>
                                

                              </td>
                              @if ($jadwal->metode == 'verifikasi_portofolio')
                              <td align="center">
                                @if ($jadwal->metode == 'verifikasi_portofolio')
                                <form action="{{ url('unggah-dokumen-ujian') }}" method="post" id="unggah{{ $pesertas->id }}">
                                  @csrf
                                  <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                  <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                  <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) { 
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>    

      @endif
      @if ($jadwal->metode == 'tes_tulis')
      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
      <label>
        Tes tertulis
      </label>   
      @endif
    </td>
    @endif
    @endif
    <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
    <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
    <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
  </tr>
  {{--@endif--}}
  @endforeach


    @foreach ($pesertaTidakLulus as $key => $pesertas)
    @php
                                      $status_peserta = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $cek_ada1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $cek_ada2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $cek_ada3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                          if($cek_ada1 != "" || $cek_ada2 != "" || $cek_ada3 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }
                                    
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $cek_ada4 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($cek_ada4 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }

                                  }
                                }
                                @endphp


{{--                          @if($cekPeserta != 0 && $pesertas->status_inpassing != 6)--}}
                          <form action="" method="post"></form>
                          {{-- @if (in_array($pesertas->id, $cek_array, true)) --}}
                          <tr>
                            <td>{{ $no_urut++ }}</td>
                            {{-- <td>{{ $key++ + 1 }}</td> --}}
                            <td>{{ $pesertas->nama }}</td>
                            <td>{{ $pesertas->nip }}</td>
                            <td>{{ $pesertas->jabatan }}</td>
                            <td>{{  ucwords($pesertas->jenjang) }}</td>
                            @if ($batas_input == "active" || $batas_input == "expired" && $status_peserta == "avail") 
                            <td class="text-center">
                              <div class="checkbox2">
                                @php
                                      $status_check = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                    if($chechked1 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked1->metode_ujian;
                                    }
                                    elseif($chechked3 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked3->metode_ujian;
                                    }elseif($chechked2 != ""){
                                      $status_check = "";
                                      $metod = "";
                                    }
                                    else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($chechked != ""){
                                      $status_check = "checked";
                                      $metod = $chechked->metode_ujian;
                                    }else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                @endphp
                                <input type="hidden" class="" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox22{{ $pesertas->id }}">
          
                                <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox2{{ $pesertas->id }}" {{ $status_check }}>
                                

                              </td>
                              @if ($jadwal->metode == 'verifikasi_portofolio')
                              <td align="center">
                                @if ($jadwal->metode == 'verifikasi_portofolio')
                                <form action="{{ url('unggah-dokumen-ujian') }}" method="post" id="unggah{{ $pesertas->id }}">
                                  @csrf
                                  <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                  <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                  <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) { 
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>    

      @endif
      @if ($jadwal->metode == 'tes_tulis')
      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
      <label>
        Tes tertulis
      </label>   
      @endif
    </td>
    @endif
    @endif
    <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
    <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
    <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
  </tr>
  {{--@endif--}}
  @endforeach

  @if($pesertaTerdaftar_sm != "" && $pesertaTerdaftar != "")
  @foreach ($pesertaTerdaftar_sm as $key => $pesertas)
  @php
                                      $status_peserta = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $cek_ada1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $cek_ada2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $cek_ada3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                          if($cek_ada1 != "" || $cek_ada2 != "" || $cek_ada3 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }
                                    
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $cek_ada4 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($cek_ada4 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }

                                  }
                                }
                                @endphp


{{--                          @if($cekPeserta != 0 && $pesertas->status_inpassing != 6)--}}
                          <form action="" method="post"></form>
                          {{-- @if (in_array($pesertas->id, $cek_array, true)) --}}
                          <tr>
                            <td>{{ $no_urut++ }}</td>
                            {{-- <td>{{ $key++ + 1 }}</td> --}}
                            <td>{{ $pesertas->nama }}</td>
                            <td>{{ $pesertas->nip }}</td>
                            <td>{{ $pesertas->jabatan }}</td>
                            <td>{{  ucwords($pesertas->jenjang) }}</td>
                            @if ($batas_input == "active" || $batas_input == "expired" && $status_peserta == "avail") 
                            <td class="text-center">
                              <div class="checkbox4">
                                @php
                                      $status_check = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                    if($chechked1 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked1->metode_ujian;
                                    }
                                    elseif($chechked3 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked3->metode_ujian;
                                    }elseif($chechked2 != ""){
                                      $status_check = "";
                                      $metod = "";
                                    }
                                    else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($chechked != ""){
                                      $status_check = "checked";
                                      $metod = $chechked->metode_ujian;
                                    }else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                @endphp
                                <input type="hidden" class="" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox44{{ $pesertas->id }}">
                                
                                <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox4{{ $pesertas->id }}" {{ $status_check }}>
                                

                              </td>
                              @if ($jadwal->metode == 'verifikasi_portofolio')
                              <td align="center">
                                @if ($jadwal->metode == 'verifikasi_portofolio')
                                <form action="{{ url('unggah-dokumen-ujian') }}" method="post" id="unggah{{ $pesertas->id }}">
                                  @csrf
                                  <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                  <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                  <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) { 
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>    

      @endif
      @if ($jadwal->metode == 'tes_tulis')
      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
      <label>
        Tes tertulis
      </label>   
      @endif
    </td>
    @endif
    @endif
    <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
    <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
    <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
  </tr>
  {{--@endif--}}
  @endforeach
  @elseif($pesertaTerdaftar_sm != "")
   @foreach ($pesertaTerdaftar_sm as $key => $pesertas)
   @php
                                      $status_peserta = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $cek_ada1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $cek_ada2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $cek_ada3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                          if($cek_ada1 != "" || $cek_ada2 != "" || $cek_ada3 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }
                                    
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $cek_ada4 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($cek_ada4 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }

                                  }
                                }
                                @endphp


{{--                          @if($cekPeserta != 0 && $pesertas->status_inpassing != 6)--}}
                          <form action="" method="post"></form>
                          {{-- @if (in_array($pesertas->id, $cek_array, true)) --}}
                          <tr>
                            <td>{{ $no_urut++ }}</td>
                            {{-- <td>{{ $key++ + 1 }}</td> --}}
                            <td>{{ $pesertas->nama }}</td>
                            <td>{{ $pesertas->nip }}</td>
                            <td>{{ $pesertas->jabatan }}</td>
                            <td>{{  ucwords($pesertas->jenjang) }}</td>
                            @if ($batas_input == "active" || $batas_input == "expired" && $status_peserta == "avail") 
                            <td class="text-center">
                              <div class="checkbox3">
                                @php
                                      $status_check = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                    if($chechked1 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked1->metode_ujian;
                                    }
                                    elseif($chechked3 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked3->metode_ujian;
                                    }elseif($chechked2 != ""){
                                      $status_check = "";
                                      $metod = "";
                                    }
                                    else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($chechked != ""){
                                      $status_check = "checked";
                                      $metod = $chechked->metode_ujian;
                                    }else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                @endphp
                                <input type="hidden" class="" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox33{{ $pesertas->id }}">
                                
                                <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox3{{ $pesertas->id }}" {{ $status_check }}>


                              </td>
                              @if ($jadwal->metode == 'verifikasi_portofolio')
                              <td align="center">
                                @if ($jadwal->metode == 'verifikasi_portofolio')
                                <form action="{{ url('unggah-dokumen-ujian') }}" method="post" id="unggah{{ $pesertas->id }}">
                                  @csrf
                                  <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                  <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                  <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) { 
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>    

      @endif
      @if ($jadwal->metode == 'tes_tulis')
      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
      <label>
        Tes tertulis
      </label>   
      @endif
    </td>
    @endif
    @endif
    <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
    <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
    <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
  </tr>
  {{--@endif--}}
  @endforeach
  @elseif($pesertaTerdaftar != "")
  @foreach ($pesertaTerdaftar as $key => $pesertas)
  @php
                                      $status_peserta = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $cek_ada1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $cek_ada2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $cek_ada3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                          if($cek_ada1 != "" || $cek_ada2 != "" || $cek_ada3 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }
                                    
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $cek_ada4 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($cek_ada4 != ""){
                                      $status_peserta = "avail";
                                    }
                                    else{
                                      $status_peserta = "kosong"; 
                                    }

                                  }
                                }
                                @endphp


{{--                          @if($cekPeserta != 0 && $pesertas->status_inpassing != 6)--}}
                          <form action="" method="post"></form>
                          {{-- @if (in_array($pesertas->id, $cek_array, true)) --}}
                          <tr>
                            <td>{{ $no_urut++ }}</td>
                            {{-- <td>{{ $key++ + 1 }}</td> --}}
                            <td>{{ $pesertas->nama }}</td>
                            <td>{{ $pesertas->nip }}</td>
                            <td>{{ $pesertas->jabatan }}</td>
                            <td>{{  ucwords($pesertas->jenjang) }}</td>
                            @if ($batas_input == "active" || $batas_input == "expired" && $status_peserta == "avail") 
                            <td class="text-center">
                              <div class="checkbox1">
                                @php
                                      $status_check = "";
                                if($jadwal->metode == 'verifikasi_portofolio'){
                                  if($data != ""){
                                    $chechked1 = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    $chechked2 = $data->where('id_peserta',$pesertas->id)->where('status','=','0')->first();
                                    $chechked3 = $data->where('id_peserta',$pesertas->id)->where('status','=','2')->first();
                                    if($chechked1 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked1->metode_ujian;
                                    }
                                    elseif($chechked3 != ""){
                                      $status_check = "checked";
                                      $metod = $chechked3->metode_ujian;
                                    }elseif($chechked2 != ""){
                                      $status_check = "";
                                      $metod = "";
                                    }
                                    else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                if($jadwal->metode == 'tes_tulis'){
                                  if($data != ""){
                                    $chechked = $data->where('id_peserta',$pesertas->id)->where('status','=','1')->first();
                                    if($chechked != ""){
                                      $status_check = "checked";
                                      $metod = $chechked->metode_ujian;
                                    }else{
                                      $status_check = "";
                                      $metod = "";
                                    }
                                  }
                                }
                                @endphp
                                <input type="hidden" class="" value="{{ $pesertas->id }}" name="tidak_daftar[]" id="checkbox11{{ $pesertas->id }}">
                                
                                <input type="checkbox" class="checkmark" value="{{ $pesertas->id }}" name="daftarkan[]" id="checkbox1{{ $pesertas->id }}" {{ $status_check }}>
                                
                              </td>
                              @if ($jadwal->metode == 'verifikasi_portofolio')
                              <td align="center">
                                @if ($jadwal->metode == 'verifikasi_portofolio')
                                <form action="{{ url('unggah-dokumen-ujian') }}" method="post" id="unggah{{ $pesertas->id }}">
                                  @csrf
                                  <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
                                  <input type="hidden" name="id_peserta" value="{{ $pesertas->id }}">
                                  <input type="submit" value="Unggah Dokumen Portofolio" id="unggah{{ $pesertas->id }}" form="unggah{{ $pesertas->id }}" class="btn btn-secondary btn-sm">
            {{-- @php
              $count = count($cek_peserta);
              for ($i=0; $i < $count ; $i++) { 
              $id_terdaftar = $cek_peserta[$i]->id_peserta;
              if($pesertas->id == $id_terdaftar){
              if ($cek_peserta[$i]->id_portofolio != ""){
              echo '<span class="badge badge-light">Sudah Input Dokumen</span>';
            }
          }
        }
        @endphp --}}
      </form>    

      @endif
      @if ($jadwal->metode == 'tes_tulis')
      <input class="form-check-input" type="hidden" name="exampleRadios{{ $pesertas->id }}" id="radioVerifikasi" value="tes">
      <label>
        Tes tertulis
      </label>   
      @endif
    </td>
    @endif
    @endif
    <input type="hidden" name="id_portofolio_{{ $pesertas->id }}" value="" id="id_portofolio_{{ $pesertas->id }}">
    <input type="hidden" name="nama_peserta[]" value="{{ $pesertas->nama }}">
    <input type="hidden" name="metode" value="{{ $jadwal->metode }}">
  </tr>
  {{--@endif--}}
  @endforeach
  
  @endif
</tbody>
</table>
</div>
<button type="reset" class="btn btn-sm btn-default2" onclick="location.href = '{{ url('jadwal-ujian-lkpp') }}';">Batal</button>
@if ($batas_input == "active")
<button type="submit" class="btn btn-sm btn-default1" form="store">Simpan</button>
@endif
</form>
</div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="post" enctype="multipart/form-data" id="upload_form">
        @csrf
        <div class="modal-header">
          <h5 class="modal-title">Unggah Dokumen Portofolio</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container">
            <div class="alert alert-primary" role="alert">
              Dokumen yang di unggah minimal berukuran 100Kb maksimal 2MB.
            </div>
            <input type="hidden" name="id" value="" id="id_peserta_input">
            <div class="row">
              <div class="col-md-5 line-center">Kompetensi perencanaan PBJP</div>
              <div class="col-md-1 line-center">:</div>
              <div class="col-md-6">
                <input type="file" name="kompetensi_perencanaan_pbjp" class="custom-file-input" id="kompetensi_perencanaan_pbjp" value="{{ old('kompetensi_perencanaan_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required>
                <label class="custom-file-label" for="customFile">Choose file</label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-5 line-center">Kompetensi pemilihan PBJ</div>
              <div class="col-md-1 line-center">:</div>
              <div class="col-md-6">
                <input type="file" name="kompetensi_pemilihan_pbj" class="custom-file-input" id="kompetensi_pemilihan_pbj" value="{{ old('kompetensi_pemilihan_pbj') }}" accept="image/jpg, application/pdf, image/jpeg" required>
                <label class="custom-file-label" for="customFile">Choose file</label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-5 line-center">Kompetensi pengolahan Kontrak PBJP</div>
              <div class="col-md-1 line-center">:</div>
              <div class="col-md-6">
                <input type="file" name="kompetensi_pengelolaan_kontrak_pbjp" class="custom-file-input" id="kompetensi_pengelolaan_kontrak_pbjp" value="{{ old('kompetensi_pengelolaan_kontrak_pbjp') }}" accept="image/jpg, application/pdf, image/jpeg" required>
                <label class="custom-file-label" for="customFile">Choose file</label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-5 line-center">Kompetensi PBJP secara Swakelola</div>
              <div class="col-md-1 line-center">:</div>
              <div class="col-md-6">
                <input type="file" name="kompetensi_pbj_secara_swakelola" class="custom-file-input" id="kompetensi_pbj_secara_swakelola" value="{{ old('kompetensi_pbj_secara_swakelola') }}" accept="image/jpg, application/pdf, image/jpeg" required>
                <label class="custom-file-label" for="customFile">Choose file</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-sm btn-default2" data-dismiss="modal">Batal</i></button>
          @if ($batas_input == "active")
          <button class="btn btn-sm btn-default1" type="submit" form="store">Unggah</button>
          @endif
        </div>
      </form>
    </div>
  </div>
</div>
<div class="col-md-3 text-center">
  @include('layout.button_right_kpp')
</div>
</div>
</div>
</div>
</div> 
</div>
</div>
</div>
</div>
@endsection

@section('js')
<script>



  $('#btn-tambah').click(function (e) {
    e.preventDefault();
    document.getElementById("verif").style.display = "block";
    document.getElementById("view").style.display = "none";
  });

  $('#btn-back').click(function (e) {
    e.preventDefault();
    document.getElementById("verif").style.display = "none";
    document.getElementById("view").style.display = "block";
  });

  jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
      window.location = $(this).data("href");
    });
  });

  $('input[type=radio][id=radioVerifikasi]').change(function() {
    if (this.value == 'verifikasi') {
      jQuery.noConflict(); 
      $('#myModal').modal('show');
    }
    else if (this.value == 'tes') {

    }
  });

  $('input[type=radio][id=radioVerifikasi]').click(function() {
    if (this.value == 'verifikasiada') {
      jQuery.noConflict(); 
      $('#myModal2').modal('show');
    }
    else if (this.value == 'tes') {

    }
  });

    // $(document).ready(function() {
    //     $(".form-check #radioVerifikasi").attr('disabled', true);
    // });

    $('.checkbox #checkboxDaftar').change(function() {
    // this will contain a reference to the checkbox   
    if (this.checked) {
      var input = "input[name=exampleRadios" + $(this).val() + "]";
      $(input).attr('disabled', false);
      $('#id_peserta_input').val($(this).val());
    } else {
      var input = "input[name=exampleRadios" + $(this).val() + "]";
      $(input).attr('disabled', true);
      $('#id_peserta_input').val('');
    }
  });

    $(".custom-file-input").on("change", function() {
      var fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    
  </script> 
  
  <script type="text/javascript">
   @foreach ($peserta as $key => $pesertas)
   $('.checkbox #checkbox{{ $pesertas->id }}').change(function() {
    // this will contain a reference to the checkbox   
    if (this.checked) {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').show();
     $(".checkbox #checkboxx{{ $pesertas->id }}").attr("disabled",true);
   } else {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').hide();
     $(".checkbox #checkboxx{{ $pesertas->id }}").attr("disabled",false);
   }
 });
   @endforeach
 </script>
 <script type="text/javascript">
  $(document).ready(function(){
    @foreach ($peserta as $key => $pesertas)
    // this will contain a reference to the checkbox   
    if ( $('.checkbox #checkbox{{ $pesertas->id }}').attr('checked')) {
      $('#unggah{{ $pesertas->id }}').show();
      $(".checkbox #checkboxx{{ $pesertas->id }}").attr("disabled",true);
    } else {
     $('#unggah{{ $pesertas->id }}').hide();
     $(".checkbox #checkboxx{{ $pesertas->id }}").attr("disabled",false);
   }
   @endforeach
 });
  
</script>
<script type="text/javascript">
   @foreach ($pesertaTerdaftar as $key => $pesertas)
   $('.checkbox1 #checkbox1{{ $pesertas->id }}').change(function() {
    // this will contain a reference to the checkbox   
    if (this.checked) {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').show();
     $(".checkbox1 #checkbox11{{ $pesertas->id }}").attr("disabled",true);
   } else {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').hide();
     $(".checkbox1 #checkbox11{{ $pesertas->id }}").attr("disabled",false);
   }
 });
   @endforeach
 </script>
 <script type="text/javascript">
  $(document).ready(function(){
    @foreach ($pesertaTerdaftar as $key => $pesertas)
    // this will contain a reference to the checkbox   
    if ( $('.checkbox1 #checkbox1{{ $pesertas->id }}').attr('checked')) {
      $('#unggah{{ $pesertas->id }}').show();
      $(".checkbox1 #checkbox11{{ $pesertas->id }}").attr("disabled",true);
    } else {
     $('#unggah{{ $pesertas->id }}').hide();
     $(".checkbox1 #checkbox11{{ $pesertas->id }}").attr("disabled",false);
   }
   @endforeach
 });
</script>
<script type="text/javascript">
   @foreach ($pesertaTidakLulus as $key => $pesertas)
   $('.checkbox2 #checkbox2{{ $pesertas->id }}').change(function() {
    // this will contain a reference to the checkbox   
    if (this.checked) {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').show();
     $(".checkbox2 #checkbox22{{ $pesertas->id }}").attr("disabled",true);
   } else {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').hide();
     $(".checkbox2 #checkbox22{{ $pesertas->id }}").attr("disabled",false);
   }
 });
   @endforeach
 </script>
<script type="text/javascript">
  $(document).ready(function(){
    @foreach ($pesertaTidakLulus as $key => $pesertas)
    // this will contain a reference to the checkbox   
    if ( $('.checkbox2 #checkbox2{{ $pesertas->id }}').attr('checked')) {
      $('#unggah{{ $pesertas->id }}').show();
      $(".checkbox2 #checkbox22{{ $pesertas->id }}").attr("disabled",true);
    } else {
     $('#unggah{{ $pesertas->id }}').hide();
     $(".checkbox2 #checkbox22{{ $pesertas->id }}").attr("disabled",false);
   }
   @endforeach
 });
</script>
<script type="text/javascript">
   @foreach ($pesertaTidakLulus as $key => $pesertas)
   $('.checkbox3 #checkbox3{{ $pesertas->id }}').change(function() {
    // this will contain a reference to the checkbox   
    if (this.checked) {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').show();
           $(".checkbox3 #checkbox33{{ $pesertas->id }}").attr("disabled",true);
   } else {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').hide();
           $(".checkbox3 #checkbox33{{ $pesertas->id }}").attr("disabled",false);
   }
 });
   @endforeach
 </script>
<script type="text/javascript">
  $(document).ready(function(){
    @foreach ($pesertaTidakLulus as $key => $pesertas)
    // this will contain a reference to the checkbox   
    if ( $('.checkbox3 #checkbox3{{ $pesertas->id }}').attr('checked')) {
      $('#unggah{{ $pesertas->id }}').show();
            $(".checkbox3 #checkbox33{{ $pesertas->id }}").attr("disabled",true);
    } else {
     $('#unggah{{ $pesertas->id }}').hide();
     $(".checkbox3 #checkbox33{{ $pesertas->id }}").attr("disabled",false);
   }
   @endforeach
 });
</script>
<script type="text/javascript">
   @foreach ($pesertaTerdaftar_sm as $key => $pesertas)
   $('.checkbox4 #checkbox4{{ $pesertas->id }}').change(function() {
    // this will contain a reference to the checkbox   
    if (this.checked) {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').show();
     $(".checkbox4 #checkbox44{{ $pesertas->id }}").attr("disabled",true);
   } else {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').hide();
     $(".checkbox4 #checkbox44{{ $pesertas->id }}").attr("disabled",false);
   }
 });
   @endforeach
 </script>
<script type="text/javascript">
  $(document).ready(function(){
    @foreach ($pesertaTerdaftar_tl as $key => $pesertas)
    // this will contain a reference to the checkbox   
    if ( $('.checkbox4 #checkbox4{{ $pesertas->id }}').attr('checked')) {
      $('#unggah{{ $pesertas->id }}').show();
      $(".checkbox4 #checkbox44{{ $pesertas->id }}").attr("disabled",true);
    } else {
     $('#unggah{{ $pesertas->id }}').hide();
     $(".checkbox4 #checkbox44{{ $pesertas->id }}").attr("disabled",false);
   }
   @endforeach
 });
</script>
<script type="text/javascript">
   @foreach ($pesertaTerdaftar_tl as $key => $pesertas)
   $('.checkbox5 #checkbox5{{ $pesertas->id }}').change(function() {
    // this will contain a reference to the checkbox   
    if (this.checked) {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').show();
     $(".checkbox5 #checkbox55{{ $pesertas->id }}").attr("disabled",true);
   } else {
     var select = this.value;
     $('#unggah{{ $pesertas->id }}').hide();
     $(".checkbox5 #checkbox55{{ $pesertas->id }}").attr("disabled",false);
   }
 });
   @endforeach
 </script>
<script type="text/javascript">
  $(document).ready(function(){
    @foreach ($pesertaTerdaftar_sm as $key => $pesertas)
    // this will contain a reference to the checkbox   
    if ( $('.checkbox5 #checkbox5{{ $pesertas->id }}').attr('checked')) {
      $('#unggah{{ $pesertas->id }}').show();
      $(".checkbox5 #checkbox55{{ $pesertas->id }}").attr("disabled",true);
    } else {
     $('#unggah{{ $pesertas->id }}').hide();
     $(".checkbox5 #checkbox55{{ $pesertas->id }}").attr("disabled",false);
   }
   @endforeach
 });
</script>
<script>
  $(document).ready(function(){

   $('#upload_form').on('submit', function(event){
    event.preventDefault();
    $.ajax({
     url:"{{ url('upload-portofolio') }}",
     method:"POST",
     data:new FormData(this),
     dataType:'JSON',
     contentType: false,
     cache: false,
     processData: false,
     success:function(data)
     {
        // $('#message').css('display', 'block');
        // $('#message').html(data.message);
        // $('#message').addClass(data.class_name);
        // $('#uploaded_image').html(data.uploaded_image);
        if(data.msg == 'validator'){
          alert('validator error');
        }

        if(data.msg == 'berhasil'){
          var nameid = "#id_portofolio_" + data.id;
          $('.modal-body .custom-file-input').siblings(".custom-file-label").addClass("selected").html(' ');
          $('#myModal').modal('hide');
          $(nameid).val(data.id_db);
          alert('Berhasil Input');
        }

        if(data.msg == 'gagal'){
          alert('Gagal Input');
        }
      }
    })
  });

 });
</script>
@endsection