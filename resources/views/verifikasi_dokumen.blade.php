@extends('layout.app')

@section('title')
Verifikasi Dokumen
@stop
@section('css')
<style type="text/css">
	.form-control
	{
		border-radius: 5px;
		height: 27px;
    padding: 0px;
    padding-left: 10px;
	}

  .row{
    margin: 0px 10px 10px 10px; 
  }
  .image{
    border-radius: 5px;
    width: 100px;
    height :120px;
  }
</style>
@stop
@section('content')
<div class="col-md-11">
<div class="box">
            <div class="box-header">
              <h3 class="box-title" style="padding-top: 10px;">Verifikasi Dokumen Pernyataan</h3>
              <hr>
            </div>
            <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
            		<div class="col-md-4">
              	<label>Pas foto 3x4   </label>
              </div>
              <div class="col-md-1">
              	<label>:</label>
              </div>
              <div class="col-md-7">
                <img src="http://placehold.it/150x100" alt="..." class="image">
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>KTP </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>ktp.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Ijazah Terakhir (Min S1/D4)  </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>ijazah.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Surat Ket Bertugas di Bidang PBJ </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>sktugas.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Bukti SK di Bidang PBJ </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>buktisk.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>SK Kenaikkan Pangkat Terakhir </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>SKkenaikkan.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>SK Pengangkatan Kedalam Jabatan Terakhir </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>SK-pengangkatan.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Sertifikat Dasar PBJ </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>sertifikat-dasar.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>SKP 2 Tahun Terakhir </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>SKP.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Surat Ket. Tidak sedang dijatuhi Hukuman Disiplin </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>skhukum-disiplin.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Formulir Kesediaan Mengikuti Inpassing </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>form-kesediaan.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>SK Pengangkatan CPNS </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>SK-CPNS.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Sk Pengangkatan PNS </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>SK-PNS.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Surat Pernyataan bersedia diangkat dalam JFPPBJ </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>Surat-pernyataan.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Bukti Portofolio </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>portofolio.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Surat Usulan/Rekomendasi Mengikuti Inpassing </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label>Surat-usulan.pdf</label>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Status </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-5">
                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                  <option selected="selected">Pilih Status</option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                </select>
              </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                <label>Catatan </label>
              </div>
              <div class="col-md-1">
                <label>:</label>
              </div>
              <div class="col-md-5">
                <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
              </div>
            </div>
            <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3"><a href="{{ url('verifikasi-dokumen') }}"><button type="button" class="btn btn-block btn-danger">Next&nbsp;&nbsp;<strong>></strong></button></a></div>  
            <div class="col-md-2"></div>
            </div>
            

              
                  
              </div>
            </div>
            
            
            <!-- /.box-body -->
          </div>
</div>
@stop
