@extends('layout.app')
@section('title')
	Jadwal Uji Kompetensi (Reguler LKPP)
@stop
@section('css')
	<style>
	.btn-tbh{
		text-align: right;
	}

	.btn-jadwal{
		width: 120px;
		background: #E8382A;
		color: #fff;
		font-weight: 600;
	}

	.btn-jadwal:hover{
		color: aliceblue;
	}
	</style>
@stop
@section('content')
@if (session('msg'))
	@if (session('msg') == "berhasil")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil simpan data</strong>
				</div>
			</div>
		</div>
	@endif 
	
	@if (session('msg') == "gagal")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal simpan data</strong>
				</div>
			</div>
		</div> 
	@endif
	
	@if (session('msg') == "berhasil_update")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil ubah data</strong>
				</div>
			</div>
		</div>
	@endif 
	
	@if (session('msg') == "gagal_update")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal ubah data</strong>
				</div>
			</div>
		</div> 
	@endif

	@if (session('msg') == "berhasil_hapus")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil Hapus Data</strong>
				</div>
			</div>
		</div>
	@endif 
	
	@if (session('msg') == "gagal_hapus")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal Hapus Data</strong>
				</div>
			</div>
		</div> 
	@endif
@endif
<div class="main-box">
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2 col-6">
				<select name='length_change' id='length_change' class="form-control">
					<option value='50'>50</option>
					<option value='100'>100</option>
					<option value='150'>150</option>
					<option value='200'>200</option>
				</select>
			</div>
			<div class="col-md-3 col-6">
				<div class="input-group">
					<div class="input-group addon">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
						<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
					</div>
				</div>
			</div>
			<div class="col-md-1 text-center">
				<b>Bulan</b>
			</div>
			<div class="col-md-2 col-6">
				<form action="" method="post">
				@csrf
					<select name='bulan' id='bulan' class="form-control form-control-sm" onchange="this.form.submit()">
						<option value='' {{ $bulan == '' ? 'selected' : '' }}>Pilih Bulan</option>
						<option value='1' {{ $bulan == 1 ? 'selected' : '' }}>Januari</option>
						<option value='2' {{ $bulan == 2 ? 'selected' : '' }}>Februari</option>
						<option value='3' {{ $bulan == 3 ? 'selected' : '' }}>Maret</option>
						<option value='4' {{ $bulan == 4 ? 'selected' : '' }}>April</option>
						<option value='5' {{ $bulan == 5 ? 'selected' : '' }}>Mei</option>
						<option value='6' {{ $bulan == 6 ? 'selected' : '' }}>Juni</option>
						<option value='7' {{ $bulan == 7 ? 'selected' : '' }}>Juli</option>
						<option value='8' {{ $bulan == 8 ? 'selected' : '' }}>Agustus</option>
						<option value='9' {{ $bulan == 9 ? 'selected' : '' }}>September</option>
						<option value='10' {{ $bulan == 10 ? 'selected' : '' }}>Oktober</option>
						<option value='11' {{ $bulan == 11 ? 'selected' : '' }}>November</option>
						<option value='12' {{ $bulan == 12 ? 'selected' : '' }}>Desember</option>
					</select>
				</form>
			</div>
			<div class="col-md-3 col-6 btn-tbh">
			@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
				<a href="{{ url('tambah-jadwal-inpassing') }}"><button class="btn btn-sm btn-jadwal">Buat Jadwal</button></a>
			@endif
			</div>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>Metode Ujian</th>
					<th>Tanggal Ujian</th>
					<th>Waktu Ujian</th>
					<th>Kuota</th>
					<th>Batas Akhir Pendaftaran</th>
					<th style="width: 40%">CP</th>
					<th style="width: 5%;">Publish</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			@php
				$no_urut = 1;
			@endphp
			@foreach ($jadwal as $key => $jadwals)
			@if(Auth::user()->role == 'dsp' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'superadmin')
				<tr>
					<td>{{ $no_urut++  }}</td>
					<td>{{ Helper::getMetodeInpassing($jadwals->metode) }}</td>
					<td>{{ $jadwals->metode == 'tes_tulis' ? Helper::tanggal_indo($jadwals->tanggal_tes) : Helper::tanggal_indo($jadwals->tanggal_verifikasi) }}</td>
					<td>{{ $jadwals->metode == 'tes_tulis' ? $jadwals->waktu_ujian : $jadwals->waktu_verifikasi}}</td>
					<td>{{ $jadwals->jumlah_peserta."/".$jadwals->kapasitas }}</td>
					<td>{{  Helper::tanggal_indo($jadwals->batas_waktu_input) }}</td>
					<td style="width: 40%">{{ $jadwals->no_telp_cp_1.'('.$jadwals->nama_cp_1.') | '.$jadwals->no_telp_cp_2.'('.$jadwals->nama_cp_2.')' }}</td>
					<td style="width: 5%">{{ Helper::getPublishJadwal($jadwals->publish_jadwal) }}</td>
					<td>
						<div class="dropdown">
							<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
							<ul class="dropdown-menu">
								<li><a href="{{ url('peserta-jadwal-inpassing')."/".$jadwals->id }}">Lihat Peserta</a></li>
								@if($jadwals->metode == 'tes_tulis' && Auth::user()->role != 'verifikator')
									<li><a href="{{ url('cetak-absensi-reguller/'.$jadwals->id.'-1') }}">Cetak Absensi Jenjang Pertama</a></li>
									<li><a href="{{ url('cetak-absensi-reguller/'.$jadwals->id.'-2') }}">Cetak Absensi Jenjang Muda</a></li>
									<li><a href="{{ url('cetak-absensi-reguller/'.$jadwals->id.'-3') }}">Cetak Absensi Jenjang Madya</a></li>
								@endif
								@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
									<li><a href="{{ url('up-hasil-ujian/'.$jadwals->id) }}">Unggah Hasil Ujian</a></li>
								@endif
								@if(Auth::user()->role == 'superadmin')
									<li><a href="{{ url('edit-jadwal-inpassing/'.$jadwals->id) }}">Ubah</a></li>
									<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $jadwals->id }}">Hapus</a></li>
								@endif
								@if(Auth::user()->role == 'dsp')
									<li><a href="{{ url('edit-jadwal-inpassing/'.$jadwals->id) }}">Ubah</a></li>
									<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $jadwals->id }}">Hapus</a></li>
								@endif
								@if(Auth::user()->role == 'bangprof')
									<li><a href="{{ url('edit-jadwal-inpassing/'.$jadwals->id) }}">Ubah</a></li>
									<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $jadwals->id }}">Hapus</a></li>
								@endif
							</ul>
						</div>
					</td>
					@endif					
					@if(Auth::user()->role == 'asesor' || Auth::user()->role == 'verifikator')
					{{-- @if(Auth::user()->id == $jadwals->asesor || Auth::user()->id == $jadwals->assign) --}}				
					<tr>
						<td>{{ $no_urut++ }}</td>
						<td>{{ Helper::getMetodeInpassing($jadwals->metode) }}</td>
						<td>{{ $jadwals->metode == 'tes_tulis' ? Helper::tanggal_indo($jadwals->tanggal_tes) : Helper::tanggal_indo($jadwals->tanggal_verifikasi) }}</td>
						<td>{{ $jadwals->metode == 'tes_tulis' ? $jadwals->waktu_ujian : $jadwals->waktu_verifikasi}}</td>
						{{-- <td>{{ $jadwals->lokasi_ujian }}</td> --}}
						<td>{{ $jadwals->jumlah_peserta."/".$jadwals->kapasitas }}</td>
						<td>{{  Helper::tanggal_indo($jadwals->batas_waktu_input) }}</td>
						<td style="width: 40%">{{ $jadwals->no_telp_cp_1.'('.$jadwals->nama_cp_1.') | '.$jadwals->no_telp_cp_2.'('.$jadwals->nama_cp_2.')' }}</td>
						<td style="width: 5%">{{ $jadwals->publish_jadwal == 'ya' ? 'Ya' : 'Tidak' }}</td>
						<td>
							<div class="dropdown">
								<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
								<ul class="dropdown-menu">
									<li><a href="{{ url('peserta-jadwal-inpassing')."/".$jadwals->id }}">Lihat Peserta</a></li>
									@if($jadwals->metode == 'tes_tulis' && Auth::user()->role != 'verifikator')
										<li><a href="{{ url('cetak-absensi-reguller/'.$jadwals->id) }}">Cetak Absensi Jenjang Pertama</a></li>
										<li><a href="{{ url('cetak-absensi-reguller/'.$jadwals->id) }}">Cetak Absensi Jenjang Muda</a></li>
										<li><a href="{{ url('cetak-absensi-reguller/'.$jadwals->id) }}">Cetak Absensi Jenjang Madya</a></li>
									@endif
									{{--@endif
									@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
										<li><a href="{{ url('up-hasil-ujian/'.$jadwals->id) }}">Unggah Hasil Ujian</a></li>
									@endif
									
									@if(Auth::user()->role == 'superadmin')
										<li><a href="{{ url('edit-jadwal-inpassing/'.$jadwals->id) }}">Ubah</a></li>
										<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $jadwals->id }}">Hapus</a></li>
									@endif
									
									@if(Auth::user()->role == 'dsp')
										<li><a href="{{ url('edit-jadwal-inpassing/'.$jadwals->id) }}">Ubah</a></li>
										<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $jadwals->id }}">Hapus</a></li>
									@endif
									
									@if(Auth::user()->role == 'bangprof')
										<li><a href="{{ url('edit-jadwal-inpassing/'.$jadwals->id) }}">Ubah</a></li>
										<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $jadwals->id }}">Hapus</a></li>
									@endif --}}
								</ul>
							</div>
						</td>
						{{-- @endif  --}}
						@endif
					</tr>
					<div class="modal fade" id="modal-default{{ $jadwals->id }}">
						<div class="modal-dialog" style="width:30%">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h4 class="modal-title">Hapus Jadwal Reguler</h4>
								</div>
								<div class="modal-body">
									<p>Apakah Anda yakin menghapus Jadwal Reguler?</p>
								</div>
								<div class="modal-footer">
									<a href="{{ url('hapus-jadwal-inpassing/'.$jadwals->id) }}" type="button" class="btn btn-primary pull-left">HAPUS</a>
									<button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop