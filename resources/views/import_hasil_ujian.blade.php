@extends('layout.app')
@section('title')
	Import hasil Ujian
@stop
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
	.main-box .col-md-3{
		font-weight: 600;
        font-size: medium;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }

    .btng{
        color: red;
    }
</style>
@endsection
@section('content')
<form action="" method="post" enctype="multipart/form-data">
@csrf
@if (session('msg'))
	@if (session('msg') == "berhasil_input")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil Simpan Data</strong>
				</div>
			</div>
		</div> 
	@endif
	
	@if (session('msg') == "gagal_input")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal Simpan Data</strong>
				</div>
			</div>
		</div> 
	@endif
	
	@if (session('msg') == "berhasil_update")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil Update Data</strong>
				</div>
			</div>
		</div> 
	@endif
	
	@if (session('msg') == "gagal_update")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal Update Data</strong>
				</div>
			</div>
		</div> 
	@endif
	
	@if (session('msg') == "berhasil_ganti")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Berhasil Ganti Password</strong>
				</div>
			</div>
		</div> 
	@endif
	
	@if (session('msg') == "gagal_ganti")
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Gagal Ganti Password</strong>
				</div>
			</div>
		</div> 
	@endif
@endif

@if (session('gagal_sql'))
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal Export Data</strong><br>
		</div>
    </div>
    <div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<code>{{ session('gagal_sql') }}</code>
		</div>
    </div>
</div>
@endif

<div class="main-box">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Import Hasil Uji Inpassing</h3><hr>
            </div>
        </div>
        <div class="row">
			<div class="col-md-3 col-xs-10">
				Tanggal Inpassing <span class="btng">*</span>
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
				<div class="form-group">
					<div class='input-group date' id=''>
						<input type='text' class="form-control dates" name="tanggal_inpassing" value="{{ old('tanggal_inpassing') }}" id="tanggal_inpassing" />
                        <span class="input-group-addon" id="tgl">
							<span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span class="errmsg">{{ $errors->first('tanggal_inpassing') }}</span> 
                </div>
            </div>
        </div>
        <input type="hidden" name="metode" class="input_metode">
        <div class="row" id="metode">
			<div class="col-md-3 col-xs-10">
				Metode Uji Inpassing <span class="btng">*</span>
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                    <select name="metode" class="form-control" id="metode_ujian">
                        <option value="" disabled selected>Pilih</option>
                        <option value="verifikasi" {{ old('metode') == 'verifikasi' ? 'selected' : '' }}>Verifikasi Portofolio</option>
                        <option value="tertulis" {{ old('metode') == 'tertulis' ? 'selected' : '' }}>Uji Tertulis</option>
					</select>
				</div>
                <span class="errmsg">{{ $errors->first('metode') }}</span>
			</div>
		</div>
        <div class="row" id="metode2">
			<div class="col-md-3 col-xs-10">
				Metode Uji Inpassing <span class="btng">*</span>
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
				<b class="metode_text"></b>
            </div>
		</div>
		<div class="row" id="upload_file_portofolio">
			<div class="col-md-3 col-xs-10">
				Upload File <span class="btng">*</span>
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                  <input type='file' name="upload_file_portofolio" value="{{ old('upload_file_portofolio') }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg" class="upload_file_portofolio">
                  <span class="errmsg errmsg_portofolio">{{ $errors->first('upload_file_portofolio') }}</span>
                </div>
            </div>
        </div>
        <div class="row" id="upload_file_tertulis">
            <div class="col-md-3 col-xs-10">
                Upload File <span class="btng">*</span>
            </div>
            <div class="col-md-1 col-xs-1">:</div>
            <div class="col-md-5 col-xs-12">
                <div class="form-group">
                  <input type='file' name="upload_file_tertulis" value="{{ old('upload_file_tertulis') }}" accept="application/sql, .sql"/ class="upload_file_tertulis">
                  <span class="errmsg errmsg_tertulis">{{ $errors->first('upload_file_tertulis') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9" style="text-align: right">
                <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
                <button type="submit" class="btn btn-sm btn-default1">Upload</button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection
@section('js')
<script type="text/javascript">
	$('#metode_ujian').on('change', function() {
		var select = this.value;
		if(select == 'verifikasi'){
			$('#upload_file_portofolio').show();
			$('#upload_file_tertulis').hide();
		}

		if(select == 'tertulis'){
			$('#upload_file_portofolio').hide();
			$('#upload_file_tertulis').show();
		}
	});
	
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});
	
	$(function () {
		$('#datetimepicker3').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});
	
	$(function () {
		$('#datetimepicker4').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});
	
	$(function () {
		$('#datetimepicker2').datetimepicker({
			format: 'LT'
		});
	});

	$(document).ready(function() {
		$('#metode').hide();
		$('#metode2').hide();
		$('#upload_file_portofolio').hide();
		$('#upload_file_tertulis').hide();
		var metode = $('#metode_ujian').val();
		if (metode == 'verifikasi') {
			$('#metode').show();
			$('#upload_file_portofolio').show();
			$('#upload_file_tertulis').hide();
		}

		if (metode == 'tertulis') {
			$('#metode').show();
			$('#upload_file_portofolio').hide();
			$('#upload_file_tertulis').show();
		}
	});
	
	$(".dates").datepicker({
		onSelect: function(dateText) {
			console.log("Selected date: " + dateText + "; input's current value: " + this.value);
		}
	}).on("change", function() {
		$(this).datepicker('hide');
			console.log("Got change event from field" + this.value);
			var tanggal = this.value;
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: "POST",
				url: '{{url('cek-tgl-import-ujian')}}',
				data: {tanggal:tanggal},
				dataType: "json",
				success: function(res) {
					if(res.status){
						if (res.tipe_ujian == 'tes_tulis') {
							$('.metode_text').html('Tes Tertulis');
							$('#upload_file_portofolio').hide();
							$('#upload_file_tertulis').show();
							$('.input_metode').val('tertulis');
							$('#metode_ujian').val('tertulis');
							$('#metode_ujian').attr('disabled', 'disabled');
						}
						
						if (res.tipe_ujian == 'verifikasi_portofolio') {
							$('.metode_text').html('Verifikasi Portofolio');
							$('#upload_file_portofolio').show();
							$('#upload_file_tertulis').hide();
							$('.input_metode').val('verifikasi');
							$('#metode_ujian').val('verifikasi');
							$('#metode_ujian').attr('disabled', 'disabled');
						}
						
						if (res.tipe_ujian == 'dua_metode') {
							$('#metode').show();
							$('.input_metode').val('');
							$('#metode_ujian').val('');
							$('#metode_ujian').removeAttr('disabled');
						}
						
						$('#metode').show();
					} else {
						$('#metode').hide();
						$('#metode2').show();
						$('.metode_text').html('Tidak ada ujian di tanggal yang di pilih.');
						$('#upload_file_portofolio').hide();
						$('#upload_file_tertulis').hide();
						$('.input_metode').val('');
					}
				},
				error: function (jqXHR, exception) {
					console.log(exception);
				}
			});
		});
		
		$('.upload_file_tertulis').bind('change', function() {
			const name = event.target.files[0].name;
			const lastDot = name.lastIndexOf('.');
			const fileName = name.substring(0, lastDot);
			const ext = name.substring(lastDot + 1);
			
			if (ext != 'sql') {
				$('.errmsg_tertulis').html("type file tidak boleh selain .sql");
				console.log('file salah');
				$('.upload_file_tertulis').val("");
			} else {
				$('.errmsg_tertulis').html("");
				console.log('file benar');
			}
		});
</script>
@endsection