@extends('layout.app2')

@section('title')
Daftar Regular LKPP
@endsection

@section('css')
<style>
  body{
    background-color: whitesmoke;
  }
  .box-container{
    background-color: white;
    border-radius: 5px;
    margin: 2% 10% 2% 5%;
    padding: 2%;
  }

  .shadow{
    -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 0px 10px 2px rgba(0,0,0,0.85);
    max-width: 250px;
    max-height: 30px;
    line-height: 15px;
    width: 80%;
    margin: 5px;
  }

  .box-tab{
    -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 3px 7px 0px rgba(0,0,0,0.85);
    border-radius: 5px;padding: 10px;
  }

  div.dataTables_wrapper div.dataTables_info{
    display: none;
  }

  div.dataTables_wrapper .row.col-sm-12{
    width: 120px;
  }

  .pointer { 
    cursor: pointer; 
  }

  .cursors {
  display: flex;
  flex-wrap: wrap;
}
.cursors > div {
  flex: 150px;
  padding: 10px 2px;
  white-space: nowrap;
  border: 1px solid #eee;
  border-radius: 5px;
  margin: 0 5px 5px 0;
}
.cursors > div:hover {
  background: #eee;
}
</style>
@endsection

@section('content')
<div class="main-page">
  <div class="container box-container">
    <div class="row">
      <h4> Jadwal & Daftar Reguler LKPP</h4><br>
    </div>
    <div class="row">
      <hr style="width: 70%;margin-left: 5px;">
    </div>
    <div class="row" style="margin-right: -50px;">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-9 box-tab" style="">
            <table id="example" class="table table-striped table-bordered" style="width:100%;">
              <thead style="background-color: darkgray;">
                <tr>
                  <th>no</th>
                  <th>tgl verif portofolio</th>
                  <th>tgl tes tertulis</th>
                  <th>waktu tes</th>
                  <th>lokasi</th>
                  <th>kuota</th>
                </tr>
              </thead>
              <tbody>
                <!-- <a href="{{ url('peserta-ujian') }}"> -->
                  <tr class='clickable-row pointer' data-href="{{ url('peserta-ujian') }}">
                  <td>1</td>
                  <td>dummy</td>
                  <td>dummy</td>
                  <td>dummy</td>
                  <td>dummy</td>
                  <td>dummy</td>
                </tr>
              <!-- </a> -->
                <tr>
                  <td>1</td>
                  <td>dummy</td>
                  <td>dummy</td>
                  <td>dummy</td>
                  <td>dummy</td>
                  <td>dummy</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col-md-3">
            <div class="rows"><a href="{{ url('biodata') }}" class="btn btn-default shadow">Biodata admin PPK</a></div>
            <div class="rows"><a href="{{ url('pengusulan-eformasi') }}" class="btn btn-default shadow">Pengusulan e-Formasi</a></div>
            <div class="rows"><a href="{{ url('data-peserta-ppk') }}" class="btn btn-default shadow">Data Peserta</a></div>
            <div class="rows"><a href="{{ url('daftar-reguler-lkpp') }}" class="btn btn-default shadow">Jadwal & Daftar LKPP</a></div>
            <div class="rows"><a href="{{ url('daftar-instansi') }}" class="btn btn-default shadow">Jadwal & Daftar Instansi</a></div>
            <div class="rows"><a href="#" class="btn btn-default shadow">Ganti Password</a></div>
            <div class="rows"><a href="#" class="btn btn-default shadow" style="color: red;">LogOut</a></div>
          </div>
        </div>
      </div>
      <div>        
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
    </script>

    @endsection