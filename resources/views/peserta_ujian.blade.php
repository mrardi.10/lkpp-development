@extends('layout.app2')

@section('title')
Daftar Regular LKPP
@endsection

@section('css')
<style>
  body{
    background-color: whitesmoke;
  }
  .box-container{
    background-color: white;
    border-radius: 5px;
    margin: 2% 10% 2% 5%;
    padding: 2%;
  }

  .shadow{
    -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 0px 10px 2px rgba(0,0,0,0.85);
    max-width: 250px;
    max-height: 30px;
    line-height: 15px;
    width: 80%;
    margin: 5px;
  }

  .box-tab{
    -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 3px 7px 0px rgba(0,0,0,0.85);
    border-radius: 5px;padding: 10px;
  }

  div.dataTables_wrapper div.dataTables_info{
    display: none;
  }

  div.dataTables_wrapper .row.col-sm-12{
    width: 120px;
  }

  input.checkbox{

  }

  .custom-file{
    margin: 5px;
  }

</style>
@endsection

@section('content')
<div class="main-page">
  <div class="container box-container">
    <div class="row">
      <h4> Pilih Peserta Ujian Tanggal -------</h4><br>
    </div>
    <div class="row">
      <hr style="width: 70%;margin-left: 5px;">
    </div>
    <div class="row" style="margin-right: -50px;">
      <div class="col-md-12">
        <div class="modal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>Modal body text goes here.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-9 box-tab" style="">
            <table id="example" class="table table-striped table-bordered" style="width:100%;">
              <thead style="background-color: darkgray;">
                <tr>
                  <th>no</th>
                  <th>Nama</th>
                  <th>NIP</th>
                  <th>Jenjang</th>
                  <th>Gol</th>
                  <th style="width: 129px;">Daftarkan Peserta</th>
                  <th>Metode Ujian</th>
                </tr>
              </thead>
              <tbody>
                <!-- <a href="{{ url('peserta-ujian') }}"> -->
                  <tr>
                    <td>1</td>
                    <td>dummy</td>
                    <td>dummy</td>
                    <td>dummy</td>
                    <td>dummy</td>
                    <td style="text-align: center;"><input type='checkbox' /></td>
                    <td style="text-align: left;padding: 10px 26px;">
                      <label class="form-check-label" for="radio1">
                        <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2" data-toggle="modal" data-target=".bd-example-modal-lg">Verifikasi Portofolio
                      </label><br>
                      <label class="form-check-label" for="radio2">
                        <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2">Tes Tertulis
                      </label>
                    </td>
                  </tr>
                  <!-- </a> -->
                  <tr>
                    <td>1</td>
                    <td>dummy</td>
                    <td>dummy</td>
                    <td>dummy</td>
                    <td>dummy</td>
                    <td style="text-align: center;"><input type='checkbox' /></td>
                    <td style="text-align: left;padding: 10px 26px;">
                      <label class="form-check-label" for="radio3">
                        <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2">Verifikasi Portofolio
                      </label><br>
                      <label class="form-check-label" for="radio4">
                        <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2">Tes Tertulis
                      </label>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content" style="padding: 25px;">
                  Unggah Dokumen portofolio
                  <hr style="width: 100%;">
                  <table>
                    <tr><td>Kompetensi Perencana PBJP</td>
                      <td>:</td>
                      <td>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile">
                          <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                      </td>
                    </tr>
                    <tr><td>Kompetensi Pemilihan PBJ</td>
                      <td>:</td>
                      <td>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile">
                          <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                      </td>
                    </tr>
                    <tr><td>Kompetensi Pengelolaan Kontrak PBJP</td>
                      <td>:</td>
                      <td>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile">
                          <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                      </td>
                    </tr>
                    <tr><td>Kompetensi PBJ Secara Swakelola</td>
                      <td>:</td>
                      <td>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile">
                          <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                      </td>
                    </tr>
                  </table>
                  <div class="rows" style="text-align: right;">
                    <button class="btn btn-default" class="close" data-dismiss="modal" style="background-color: darkgray;width: 20%;
                    margin: 5px;">Batal</button>
                    <button class="btn btn-danger" style="width: 20%;
                    margin: 5px;">Unggah</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="rows"><a href="{{ url('biodata') }}" class="btn btn-default shadow">Biodata admin PPK</a></div>
              <div class="rows"><a href="{{ url('pengusulan-eformasi') }}" class="btn btn-default shadow">Pengusulan e-Formasi</a></div>
              <div class="rows"><a href="{{ url('data-peserta-ppk') }}" class="btn btn-default shadow">Data Peserta</a></div>
              <div class="rows"><a href="{{ url('daftar-reguler-lkpp') }}" class="btn btn-default shadow">Jadwal & Daftar LKPP</a></div>
              <div class="rows"><a href="{{ url('daftar-instansi') }}" class="btn btn-default shadow">Jadwal & Daftar Instansi</a></div>
              <div class="rows"><a href="#" class="btn btn-default shadow">Ganti Password</a></div>
              <div class="rows"><a href="#" class="btn btn-default shadow" style="color: red;">LogOut</a></div>
            </div>
          </div>
        </div>
        <div>        
        </div>
      </div>

      <script type="text/javascript">
        jQuery(document).ready(function($) {
          $(".clickable-row").click(function() {
            window.location = $(this).data("href");
          });
        });
      </script>

      @endsection