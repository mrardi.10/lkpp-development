@extends('layout.app')
@section('title')
	Riwayat Peserta
@stop
@section('css')
<style type="text/css">
	.clickable-row:hover{
		cursor: pointer;
	}
</style>
@stop
@section('content')
<div class="main-box">
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2">
				<select name='length_change' id='length_change' class="form-control">
					<option value='50'>50</option>
					<option value='100'>100</option>
					<option value='150'>150</option>
					<option value='200'>200</option>
				</select>
			</div>
			<div class="col-md-4 col-12">
				<div class="input-group">
					<div class="input-group addon">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
						<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
					</div>
				</div>
			</div>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th width="5%">No</th>
					<th width="15%">Tanggal</th>
					<th width="40%">Aksi</th>
					<th width="40%">Keterangan</th>
				</tr>
			</thead>
			<tbody>
				@php
					$no_urut = 1;
				@endphp
				@foreach ($data as $key => $datas)
					@if($datas->perihal == 'hasil_verif_regular_dsp' || $datas->perihal == 'hasil_tes_dsp' || $datas->perihal == 'hasil_tes' || $datas->perihal == 'hasil_verif_regular')
					<tr>	
						<td>{{ $no_urut++ }}</td>
						<td>{{ Helper::tanggal_indo($datas->tanggal) }}</td>
						<td>{{ $datas->nama_peserta.' '.$datas->description }}</td>
						@if($datas->perihal == 'hasil_verif_regular' || $datas->perihal == 'hasil_verif_regular_dsp')
							<td> @if($datas->keterangan == "")
								<ol type="1">
								@foreach ($namaInput as $key => $namainput)
									@if($namainput->id_peserta == $datas->id_peserta && $namainput->id_jadwal == $datas->id_jadwal && $namainput->id_admin_ppk == $datas->id_admin_ppk && $namainput->id_admin_lkpp == $datas->id_admin_lkpp)
										<li><b>{{$namainput->judul_input}}</b></li>
										<ul style="list-style-type:disc;">
											<li>{{ '( '.$namainput->nama_input.' )'}} <br> <I>{{$namainput->nama_file_input}}</I><br></li>
											<ul style="list-style-type: none">
												<li>: <b>{{ $namainput->keterangan}}</b></li>
											</ul>
										</ul>
									@endif
								@endforeach
								</ol>
						@else						
						@endif</td>
						@elseif($datas->perihal == 'hasil_tes_dsp' || $datas->perihal == 'hasil_tes')
							<td>{{ $datas->keterangan == "" ? "-" : $datas->keterangan }}</td>
						@endif
					</tr>
					@elseif($datas->perihal == 'verifikasi_berkas' || $datas->perihal == 'jadwal_peserta')
						<tr>
							<td>{{ $no_urut++ }}</td>
							<td>{{ Helper::tanggal_indo($datas->tanggal) }}</td>
							<td>{{ $datas->nama_peserta.' '.$datas->description.' '.$datas->admin_lkpp }}</td>
							<td>{{ $datas->keterangan == "" ? "-" : $datas->keterangan }}</td>
						</tr>
					@elseif($datas->perihal == 'jadwal_reguller' || $datas->perihal == 'jadwal_instansi')
						<tr>
							<td>{{ $no_urut++ }}</td>
							<td>{{ Helper::tanggal_indo($datas->tanggal) }}</td>
							<td>{{ $datas->nama_peserta.' '.$datas->description.' '.$datas->nama_admin }}</td>
							<td>{{ $datas->keterangan == "" ? "-" : $datas->keterangan }}</td>
						</tr>
					@else
					<tr>
					@if($datas->perihal == 'peserta')
						@if($datas->id_admin_lkpp != null && $datas->id_admin == null) 
							<td>{{ $no_urut++ }}</td>
							<td>{{ Helper::tanggal_indo($datas->tanggal) }}</td>
							<td>{{ $datas->nama_peserta.' '.$datas->description.' '.$datas->admin_lkpp }}</td>
						@endif
                        @if($datas->id_admin != null && $datas->id_admin_lkpp == null) 
							<td>{{ $no_urut++ }}</td>
							<td>{{ Helper::tanggal_indo($datas->tanggal) }}</td>
							<td>{{ $datas->nama_peserta.' '.$datas->description.' '.$datas->nama_admin }}</td>
                        @endif
					@elseif($datas->perihal == 'jadwal_regullers')
						<td>{{ $no_urut++ }}</td>
						<td>{{ Helper::tanggal_indo($datas->tanggal) }}</td>
						<td>{{ $datas->nama_peserta.' '.$datas->description }}</td>
					@else
						<td>{{ $no_urut++ }}</td>
						<td>{{ Helper::tanggal_indo($datas->tanggal) }}</td>
						<td>{{ $datas->nama_peserta.' '.$datas->description.' '.$datas->nama_admin }}</td>
					@endif					
					<td>{{ $datas->keterangan == "" ? "-" : $datas->keterangan }}</td>					
					</tr>
				@endif
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
@section('js')
<script>
	jQuery(document).ready(function($) {
		$(".clickable-row").click(function() {
			window.location = $(this).data("href");
		});
	});
</script>
@endsection