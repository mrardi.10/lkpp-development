@extends('layout.app')

@section('title')
    Verifikasi Portofolio
@endsection

@section('css')
<style type="text/css">
.form-control
{
  border-radius: 5px;
  height: 27px;
  padding: 0px;
  padding-left: 10px;
}

.how{
  padding-bottom: 10px;
}

.row{
  margin-bottom: 10px;
  margin-left: 15px;
  margin-right: 15px;
}

.main-box{
    font-weight: 600;
}
</style>
@stop

@section('content')
<form action="" method="post">
@csrf
<div class="main-box">
    <div class="row">
        <div class="col-md-12">
            <h3>Input Hasil Pleno Verifikasi</h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xm-11">Nama Peserta</div>
        <div class="col-lg-1 col-md-1 col-xm-11">:</div>
        <div class="col-lg-5 col-md-5 col-xm-12">
            {{ $data->namas }}
            <input type="hidden" name="id_peserta" value="{{ $data->ids }}">
            <input type="hidden" name="nama_peserta" value="{{ $data->namas }}">
            <input type="hidden" name="id_jadwal" value="{{ $data->id_jadwal }}">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xm-11">No Ujian</div>
        <div class="col-lg-1 col-md-1 col-xm-11">:</div>
        <div class="col-lg-5 col-md-5 col-xm-12">
            -
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xm-11">NIP</div>
        <div class="col-lg-1 col-md-1 col-xm-11">:</div>
        <div class="col-lg-5 col-md-5 col-xm-12">
            {{ $data->nips }}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xm-11">Jenjang</div>
        <div class="col-lg-1 col-md-1 col-xm-11">:</div>
        <div class="col-lg-5 col-md-5 col-xm-12">
            {{ $data->jenjangs }}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xm-11">Skor</div>
        <div class="col-lg-1 col-md-1 col-xm-11">:</div>
        <div class="col-lg-5 col-md-5 col-xm-12">
            -
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xm-11">Asesor Verifikasi</div>
        <div class="col-lg-1 col-md-1 col-xm-11">:</div>
        <div class="col-lg-5 col-md-5 col-xm-12">
            {!! Form::select('asesor_verifikasi', $asesor, old('asesor_verifikasi'), ['placeholder' => 'please select', 'class' => 'form-control']); !!}
            <span class="errmsg">{{ $errors->first('asesor_verifikasi') }}</span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xm-11">Rekomendasi</div>
        <div class="col-lg-1 col-md-1 col-xm-11">:</div>
        <div class="col-lg-5 col-md-5 col-xm-12">
            {!! Form::select('rekomendasi', array('lulus' => 'Lulus', 'tidak_lulus' => 'Tidak Lulus'), old('rekomendasi'), ['placeholder' => 'please select', 'class' => 'form-control']); !!}
            <span class="errmsg">{{ $errors->first('rekomendasi') }}</span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xm-11">Asesor Pleno 1</div>
        <div class="col-lg-1 col-md-1 col-xm-11">:</div>
        <div class="col-lg-5 col-md-5 col-xm-12">
            {!! Form::select('asesor_pleno_1', $asesor, old('asesor_pleno_1'), ['placeholder' => 'please select', 'class' => 'form-control']); !!}
            <span class="errmsg">{{ $errors->first('asesor_pleno_1') }}</span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xm-11">Asesor Pleno 2</div>
        <div class="col-lg-1 col-md-1 col-xm-11">:</div>
        <div class="col-lg-5 col-md-5 col-xm-12">
            {!! Form::select('asesor_pleno_2', $asesor, old('asesor_pleno_2'), ['placeholder' => 'please select', 'class' => 'form-control']); !!}
            <span class="errmsg">{{ $errors->first('asesor_pleno_2') }}</span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xm-11">keputusan Pleno</div>
        <div class="col-lg-1 col-md-1 col-xm-11">:</div>
        <div class="col-lg-5 col-md-5 col-xm-12">
            {!! Form::select('keputusan_pleno', array('lulus' => 'Lulus', 'tidak_lulus' => 'Tidak Lulus'), old('keputusan_pleno'), ['placeholder' => 'please select', 'class' => 'form-control']); !!}
            <span class="errmsg">{{ $errors->first('keputusan_pleno') }}</span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9" style="text-align:right">
            <button type="reset" class="btn btn-default2">Batal</button> <button type="submit" class="btn btn-default1">Simpan</button>  
        </div>
    </div>
</div>
</form>
@endsection