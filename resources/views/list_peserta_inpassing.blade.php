@extends('layout.app')
@section('title')
	Jadwal Uji Kompetensi (Reguler LKPP)
@endsection
@section('css')
<style>
	.dropdown-menu{
		left: -130px;
	}
	.btn-default2{
		margin: 10px 10px 0px 5px !important;
	}

	span.select2{
		width: 100% !important;
	}
	.div-print{
		text-align: right;
	}
	.div-print button{
		text-align: right;
		color: black;
	}
</style>
@endsection
@section('content')
@if (session('msg'))
@if (session('msg') == "berhasil")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil simpan data</strong>
		</div>
	</div>
</div>
@endif 
@if (session('msg') == "gagal")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal simpan data</strong>
		</div>
	</div>
</div> 
@endif
@if (session('msg') == "berhasil_hapus")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil hapus data</strong>
		</div>
	</div>
</div>
@endif 
@if (session('msg') == "gagal_hapus")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal hapus data</strong>
		</div>
	</div>
</div> 
@endif
@endif
<div class="row">
	<div class="col-md-12">
		<h3> {{ $jadwal->metode == 'tes_tulis' ? Helper::tanggal_indo($jadwal->tanggal_tes) : Helper::tanggal_indo($jadwal->tanggal_verifikasi) }} Metode Ujian : {{ $jadwal->metode == 'tes_tulis' ? "Tes Tertulis" : "Verifikasi Portofolio" }}</h3>
	</div>
</div>
<div class="main-box">
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2">
				<select name='length_change' id='length_change' class="form-control">
					<option value='50'>50</option>
					<option value='100'>100</option>
					<option value='150'>150</option>
					<option value='200'>200</option>
				</select>
			</div>
			<div class="col-md-4 col-12">
				<div class="input-group">
					<div class="input-group addon">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
						<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
					</div>
				</div>
			</div>
			<div class="col-md-5 col-12 div-print">
					<a href="{{ url('peserta-jadwal-inpassing-excell/'.$id) }}"><button class="btn btn-success" style="color: #fff"><i class="fa fa-print"></i> Print Excel</button></a>
			</div>
		</div> 
	</div>
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>NIP</th>
					<th>Instansi</th> 
					<th>Pangkat/Gol.</th>
					<th>Jenjang</th>
					<th>Foto</th>
					<th>Status</th>
					@if($jadwal->metode == 'tes_tulis' || $jadwal->metode == 'verifikasi_portofolio')
						<th>Verifikator</th>
					@endif
					@if($jadwal->metode == 'verifikasi_portofolio' || $jadwal->metode == 'tes_tulis')
						<th>Asesor</th>
					@endif
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@php
					$no_urut = 1;
				@endphp
				@foreach ($data as $key => $datas)
				@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
				<tr>
					<td>{{ $no_urut++ }}</td>
					<td>{{ $datas->nama }}</td>
					<td>{{ $datas->nip }}</td>
					<td>{{ $datas->nama_instansis }}</td> 
					<td>{{ $datas->jabatan }}</td>
					<td>{{ ucwords($datas->jenjang) }}</td>
					<td>
						@if ($datas->fotos == "")
						Tidak Ada foto
						@else
						@php
						$foto = explode('.',$datas->fotos);
						@endphp
						@if ($foto[1] == 'pdf' || $foto[1] == 'PDF')
						<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$datas->fotos }}" target="_blank"><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="Lihat Foto"></i></a>
						@else
						<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$datas->fotos }}" target="_blank"><img src="{{ asset('storage/data/pas_foto_3_x_4/'.$datas->fotos) }}" alt="" class="img-responsive" width="60px"></a>
						@endif
						@endif
					</td>
					
					@if($datas->status_jadwal == 'lulus' && $datas->publish == 'publish')
					<td>{{ 'Lulus' }}</td>
					@elseif($datas->status_jadwal == 'tidak_lulus' && $datas->publish == 'publish')
					<td>{{ 'Tidak Lulus' }}</td>
					@elseif($datas->status_jadwal == 'tidak_hadir' && $datas->publish == 'publish')
					<td>{{ 'Tidak Hadir' }}</td>
					@elseif($datas->status_jadwal == 'tidak_lengkap' && $datas->publish == 'publish')
					<td>{{ 'Dokumen Persyaratan Tidak Lengkap' }}</td>
					@else
					<td>{{ $datas->status }}</td>
					@endif
					<td>{{ $datas->verifikators == "" ? "-" : $datas->verifikators }}</td>
					<td>
					@if($datas->asesors == "")
						@php
						if (isset($get_ases[$datas->asesor])) {
							echo $get_ases[$datas->asesor];
						}else{
							echo "-";
						}
						@endphp
					@else
						{{ $datas->asesors }}
					@endif
					</td>
					{{-- <td>{{ $datas->metodes == "verifikasi" ? 'Verifikasi Portofolio' : 'Tes Tertulis' }}</td> --}}
					<td>
						<div class="dropdown">
							<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
							<ul class="dropdown-menu">
								@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof')
								<li><a href="#" data-toggle="modal" data-target="#myModal{{ $datas->id }}">Assign Verifikator Dokumen Persyaratan</a></li>
								@endif
								@if($jadwal->metode == 'verifikasi_portofolio')
								@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
								<li><a href="#" data-toggle="modal" data-target="#modalAsesor{{ $datas->id }}">Assign Asesor</a></li>
								@endif
								@endif
								@if($jadwal->metode == 'verifikasi_portofolio')
								@if ( Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp' || Auth::user()->role == 'bangprof')
								<li><a href="{{ url('detail-peserta-lkpp/'.$datas->id."/".$datas->ids) }}">Lihat Detail</a></li>
								@endif
								@endif
								@if($jadwal->metode == 'tes_tulis')
								@if ( Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp' || Auth::user()->role == 'bangprof')
								<li><a href="{{ url('detail-peserta-lkpp/'.$datas->id) }}">Lihat Detail</a></li>
								@endif
								@endif
								@if (Auth::user()->role == 'superadmin')
								<li><a href="{{ url('verifikasi-peserta/'.$datas->id) }}">Verifikasi Dokumen Persyaratan</a></li>
								@endif
								@if (Auth::user()->role == 'bangprof')
								@if ($datas->assign == Auth::user()->id )
								<li><a href="{{ url('verifikasi-peserta/'.$datas->id) }}">Verifikasi Dokumen Persyaratan</a></li>	
								@endif
								@endif
								@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
								@if($datas->metodes == 'verifikasi')
								<li><a href="{{ url('verifikasi-portofolio-peserta')."/".$datas->nip."/".$datas->ids }}">Verifikasi Portofolio</a></li>
								@endif
								@endif
								@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp')
								@if($datas->metodes == 'tes')
								<li><a href="{{ url('hasil-tes-peserta')."/".$datas->nip."/".$datas->ids }}">Input Hasil Tes</a></li>
								@endif
								@endif
								@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp' || Auth::user()->role == 'bangprof')
								<li><a href="#" data-toggle="modal" data-target="#modalpindahJadwal{{ $datas->id }}">Pindah Jadwal</a></li>
								@endif
								@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'dsp' || Auth::user()->role == 'bangprof')
								<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $datas->id }}">Hapus</a></li>
								@endif
							</ul>
						</div>
					</td>
				</tr>
				@endif
				@if(Auth::user()->role == 'asesor' || Auth::user()->role == 'verifikator')
				@if(Auth::user()->id == $datas->asesor || Auth::user()->id == $datas->assign)
				<tr>
					<td>{{ $no_urut++ }}</td>
					<td>{{ $datas->nama }}</td>
					<td>{{ $datas->nip }}</td>
					<td>{{ $datas->nama_instansis }}</td> 
					<td>{{ $datas->jabatan }}</td>
					<td>{{ ucwords($datas->jenjang) }}</td>
					<td>
						@if ($datas->fotos == "")
						Tidak Ada foto
						@else
						@php
						$foto = explode('.',$datas->fotos);
						@endphp
						@if ($foto[1] == 'pdf' || $foto[1] == 'PDF')
						<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$datas->fotos }}" target="_blank"><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="Lihat Foto"></i></a>
						@else
						<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$datas->fotos }}" target="_blank"><img src="{{ asset('storage/data/pas_foto_3_x_4/'.$datas->fotos) }}" alt="" class="img-responsive" width="60px"></a>
						@endif
						@endif
					</td>
					@if($datas->status_jadwal == 'lulus' && $datas->publish == 'publish')
					<td>{{ 'Lulus' }}</td>
					@elseif($datas->status_jadwal == 'tidak_lulus' && $datas->publish == 'publish')
					<td>{{ 'Tidak Lulus' }}</td>
					@elseif($datas->status_jadwal == 'tidak_hadir' && $datas->publish == 'publish')
					<td>{{ 'Tidak Hadir' }}</td>
					@elseif($datas->status_jadwal == 'tidak_lengkap' && $datas->publish == 'publish')
					<td>{{ 'Dokumen Persyaratan Tidak Lengkap' }}</td>
					@else
					<td>{{ $datas->status }}</td>
					@endif					
					<td>{{ $datas->verifikators == "" ? "-" : $datas->verifikators }}</td>										
					<td>
					@if($datas->asesors == "")
						@php
						if (isset($get_ases[$datas->asesor])) {
							echo $get_ases[$datas->asesor];
						}else{
							echo "-";
						}
						@endphp
					@else
						{{ $datas->asesors }}
					@endif
					</td>
					
					<td>
						<div class="dropdown">
							<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
							<ul class="dropdown-menu">
								@if (Auth::user()->role == 'asesor' || Auth::user()->role == 'verifikator')
								@if (Auth::user()->id == $datas->assign)
								<li><a href="{{ url('verifikasi-peserta/'.$datas->id) }}">Verifikasi Dokumen Persyaratan</a></li>
								@endif
								@if (Auth::user()->id == $datas->asesor)
								{{-- <li><a href="{{ url('verifikasi-peserta/'.$datas->id) }}">Verifikasi Dokumen Persyaratan</a></li> --}}
								@if($datas->metodes == 'verifikasi')
								<li><a href="{{ url('verifikasi-portofolio-peserta')."/".$datas->nip."/".$datas->ids }}">Verifikasi Portofolio</a></li>
								@endif	
								@endif
								@endif
								@if (Auth::user()->role == 'asesor' || Auth::user()->role == 'verifikator')
								<li><a href="{{ url('detail-peserta-lkpp/'.$datas->id."/".$datas->ids) }}">Lihat Detail</a></li>
								@endif
							</ul>
						</div>
					</td>
				</tr>
				@endif
				@endif
				<!-- Modal Verifikator-->
				<div class="modal fade" id="myModal{{ $datas->id }}" role="dialog">
					<div class="modal-dialog">
						<form method="post" action="{{ url('assign-verifikator-peserta') }}">
							@csrf
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Assign Verifikator Dokumen Persyaratan</h4>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-md-3">
											Nama Peserta
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->nama }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Instansi
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->nama_instansis }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Pangkat/Gol.
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->jabatan }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Jenjang
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->jenjang }}
										</div>
									</div>
									<div class="row" style="margin-top:5px">
										<div class="col-md-3">
											Verifikator Dokumen Persyaratan
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{!! Form::select('verifikator', $admin_bangprof , $datas->assign, ['placeholder' => 'pilih verifikator', 'class' => 'form-control select2', 'id' => 'select-single', 'required']); !!}
										</div>
										<input type="hidden" name="id_peserta" value="{{ $datas->id }}">
										<input type="hidden" name="nama_peserta1" value="{{ $datas->nama }}">
										<input type="hidden" name="modal" value="myModal{{ $datas->id }}">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-sm btn-default2" data-dismiss="modal">Batal</button>
									<button type="submit" class="btn btn-sm btn-default1">Assign</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- Modal Asesor-->
				<div class="modal fade" id="modalAsesor{{ $datas->id }}" role="dialog">
					<div class="modal-dialog">
						<form method="post" action="{{ url('assign-asesor') }}">
							@csrf
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Assign Asesor</h4>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-md-3">
											Nama Peserta
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->nama }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Instansi
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->nama_instansis }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Pangkat/Gol.
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->jabatan }}
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											Jenjang
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{{ $datas->jenjang }}
										</div>
									</div>
									<div class="row" style="margin-top:5px">
										<div class="col-md-3">
											Asesor
										</div>
										<div class="col-md-1">
											:
										</div>
										<div class="col-md-8">
											{!! Form::select('asesor', $asesor , $datas->asesor, ['placeholder' => 'Pilih Asesor', 'class' => 'form-control select2', 'id' => 'select-single', 'required']); !!}
										</div>
										<input type="hidden" name="id_peserta" value="{{ $datas->id }}">
										<input type="hidden" name="id_pesertajadwal" value="{{ $datas->ids }}">
										<input type="hidden" name="nama_peserta" value="{{ $datas->nama }}">
										<input type="hidden" name="nama_asesor" value="{{ $datas->asesors }}">
										<input type="hidden" name="modal" value="myModal{{ $datas->id }}">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-sm btn-default2" data-dismiss="modal">Batal</button>
									<button type="submit" class="btn btn-sm btn-default1">Assign</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- Modal Hapus -->
				<div class="modal fade" id="modal-default{{ $datas->id }}" role="dialog">
					<div class="modal-dialog" style="width:30%">
						<form method="post" action="{{ url('hapus-peserta-jadwal-lkpp')."/".$datas->id_pesertas }}">
							@csrf
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Hapus Peserta</h4>
								</div>
								<div class="modal-body">
									<p>Apakah Anda yakin menghapus Peserta dari jadwal?</p>
								</div>
								<input type="hidden" name="metodes"  readonly=""  value="{{$datas->metodess == "tes_tulis" ? "Tes Tertulis" : "Verifikasi Portofolio"}}" class="metodes">
								<input type="hidden" name="id_jadwal"  readonly=""  value="{{$datas->jadwal_peserta}}" class="metodes">
								<input type="hidden" name="tgl_uji"  readonly="" value="{{Helper::tanggal_indo($datas->tgl_ujian)}}" >
								<div class="modal-footer">
									{{-- <a href="{{ url('hapus-peserta-jadwal-lkpp')."/".$datas->id }}" type="button" class="btn btn-primary pull-left">HAPUS</a> --}}
									<button type="submit" class="btn btn-sm btn-default1 pull-left">HAPUS</button>
									<button type="button" class="btn btn-sm btn-default2" data-dismiss="modal">BATAL</button>
								</div>
							</div>
						</form>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>

					<!-- Modal Hapus -->
					<div class="modal fade" id="modalpindahJadwal{{ $datas->id }}" role="dialog">
						<div class="modal-dialog" >
							<form method="post" action="{{ url('pindah-jadwal')."/".$datas->id."/".$datas->jadwalregular }}">
								@csrf
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title">Pindah Jadwal Peserta</h4>
										</div>
										<div class="modal-body">
											<p>Apakah Anda yakin memindahkan jadwal peserta?</p>
											<div class="row">
												<div class="col-md-3">
													Nama Peserta
												</div>
												<div class="col-md-1">
													:
												</div>
												<div class="col-md-8">
													{{ $datas->nama }}
												</div>
											</div>
											<div class="row">
												<div class="col-md-3">
													Instansi
												</div>
												<div class="col-md-1">
													:
												</div>
												<div class="col-md-8">
													{{ $datas->nama_instansis }}
												</div>
											</div>
											<div class="row">
												<div class="col-md-3">
													Tanggal Ujian Saat Ini
												</div>
												<div class="col-md-1">
													:
												</div>
												<div class="col-md-8">
													{{ Helper::tanggal_indo($datas->tgl_ujian) }}
												</div>
											</div>
											<div class="row" style="margin-top:5px">
												<div class="col-md-3">
													Pilih Tanggal Ujian Baru
												</div>
												<div class="col-md-1">
													:
												</div>
												<div class="col-md-8">
													<select name="tgl_ujian" class="form-control select2 tgl-ujian{{ $datas->id }}" id="select-single1" required="" >
														<option disabled="true" selected="true">Pilih tanggal ujian</option>
														@foreach ($jadwalAvailable as $jadwals)
															<option value="{{ $jadwals->id }}" metodes="{{ $jadwals->metode}}" tgl_uji="{{ Helper::tanggal_indo($jadwals->tanggal_ujian) }}" jadwals="{{ $jadwals->id}}" tanggal_ujian="{{ $jadwals->tanggal_ujian}}">{{ Helper::tanggal_indo($jadwals->tanggal_ujian)}}</option>
														@endforeach
													</select>
												</div>
												<input type="hidden" name="metodes"  readonly="" id="metode{{ $datas->id }}" class="metodes">
												<input type="hidden" name="tgl_uji"  readonly="" id="tgl_uji{{ $datas->id }}" class="tgl_uji">
												<input type="hidden" name="id_jadwal"  readonly="" id="idjadwal{{ $datas->id }}" class="id_jadwal">
												<input type="hidden" name="tanggal_ujian"  readonly="" id="tanggal{{ $datas->id }}" class="tanggal_ujian">
												<input type="hidden" name="id_admin_ppk" value="{{ $datas->admin_ppk }}">
												<input type="hidden" name="jadwal_peserta" value="{{ $datas->jadwal_peserta }}">
												<input type="hidden" name="modal" value="myModal{{ $datas->id }}">
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-sm btn-default2" data-dismiss="modal">Batal</button>
												<button type="submit" class="btn btn-sm btn-default1">PINDAH</button>
											</div>
										</div>
									</form>
								</div>
								<!-- /.modal-dialog -->
							</div>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>    
			@endsection
@section('js')
<script>
@foreach($data as $datas)
	$('.tgl-ujian{{ $datas->id}}').change(function() {
		var metode =  $('.tgl-ujian{{ $datas->id}} option:selected').attr('metodes');// get id the value from the select
        var tgl_uji =  $('.tgl-ujian{{ $datas->id}} option:selected').attr('tgl_uji');// get id the value from the select
        var jadwals =  $('.tgl-ujian{{ $datas->id}} option:selected').attr('jadwals');// get id the value from the select
        var tanggal_ujian =  $('.tgl-ujian{{ $datas->id}} option:selected').attr('tanggal_ujian');// get id the value from the select
        $('#metode{{ $datas->id}}').val(metode);
        $('#tgl_uji{{ $datas->id}}').val(tgl_uji);   // set the textbox value
        $('#idjadwal{{ $datas->id}}').val(jadwals);   // set the textbox value
        $('#tanggal{{ $datas->id}}').val(tanggal_ujian);   // set the textbox value
    });
@endforeach
</script>
@endsection