@extends('layout.app')
@section('title')
	Generate No Seri
@stop
@section('css')
<style>
	.btn-tbh{
		text-align: right;
	}

	.btn-jadwal{
		width: 120px;
		background: #E8382A;
		color: #fff;
		font-weight: 600;
	}

	.btn-jadwal:hover{
		color: #000;
	}

	.btn-generate{
		width: 120px;
		background: #4C3E59;
		color: #fff;
		font-weight: 600;
		margin:1%;
		transition-duration: 0.5s;
	}
	
	.btn-generate:hover{
		background: #0B0411;
		color: #fff;
	}
</style>
@stop
@section('content')
@if (session('msg'))
@if (session('msg') == "berhasil")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil Generate Data</strong>
		</div>
	</div>
</div>
@endif
 
@if (session('msg') == "gagal")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal Generate Data</strong>
		</div>
	</div>
</div> 
@endif

@if (session('msg') == "berhasil_update")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil Update Data</strong>
		</div>
	</div>
</div>
@endif 

@if (session('msg') == "gagal_update")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal Update Data</strong>
		</div>
	</div>
</div> 
@endif

@if (session('msg') == "berhasil_hapus")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Berhasil Hapus Data</strong>
		</div>
	</div>
</div>
@endif 

@if (session('msg') == "gagal_hapus")
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Gagal Hapus Data</strong>
		</div>
	</div>
</div> 
@endif
@endif

<h2>Generate No Seri Sertifikat</h2><br>	
<div class="main-box">
	<div class="min-top">
		<div class="row">
			<div class="col-md-1 text-center">
				<b>Perlihatkan</b>
			</div>
			<div class="col-md-2 col-6">
				<select name='length_change' id='length_change' class="form-control">
					<option value='50'>50</option>
					<option value='100'>100</option>
					<option value='150'>150</option>
					<option value='200'>200</option>
				</select>
			</div>
			<div class="col-md-3 col-6">
				<div class="input-group">
					<div class="input-group addon">
						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
						<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
					</div>
				</div>
			</div>
		</div> 
	</div>
	<form action="" method="post">
	@csrf
	<div class="table-responsive">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>No Ujian</th>
					<th>Tanggal Inpassing</th>
					<th>NIP</th>
					<th>Metode Inpassing</th>
				</tr>
			</thead>
			<tbody>
				@foreach($data as $key => $datas)
				<input type="hidden" name="id_peserta" value="{{ $datas->id_peserta }}">
				<tr>
					<td>{{ $key++ + 1 }}</td>
					<td>{{ $datas->no_ujian == '' ? '-' : $datas->no_ujian }}</td>
					<td>{{ Helper::tanggal_indo($datas->tanggals) }}</td>
					<td>{{ $datas->nips }}</td>
					<td>{{ $datas->metode_ujian == 'tes' ? 'Uji Tertulis' : 'Verifikasi Portofolio' }}</td>
				</tr>
				@endforeach
				</tbody>
		</table>
	</div>
		<button type="button" class="btn btn-sm btn-generate" data-toggle="modal" data-target="#modal-default">Generate</button>
		<div class="modal fade" id="modal-default">
	        <div class="modal-dialog" style="width:30%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Generate No Seri Sertifikat</h4>
                    </div>
                    <div class="modal-body">
                        <p>Apakah Anda yakin generate no seri sertifikat?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary pull-left">GENERATE</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
                    </div>
                </div>
	        </div>
	    </div>
	</form>
</div>
@stop