@extends('layout.app')

@section('title')
Upload Hasil Jadwal Inpassing Instansi
@stop
@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }
    .err{
      color: red;
    }
</style>
@endsection

@section('content')
<form action="" method="post" enctype="multipart/form-data">
    @csrf
    <div class="main-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Upload Hasil Ujian Jadwal Inpassing Instansi tanggal {{ Helper::tanggal_indo($jadwal->tanggal_ujian)}}</h3>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-10">
                    Upload File
                </div>
                <div class="col-md-1 col-xs-1">:</div>
                <div class="col-md-5 col-xs-12">
                    <div class="form-group">
                        <input type='file' class="form-control hasil_ujian" name="hasil_ujian" value="{{ $jadwal->hasil_ujian }}" accept=".jpg, .jpeg, .pdf, image/jpg, application/pdf, image/jpeg" id="hasil_ujian" required/>
                    </div>
                     <span class="err" id="errhasil_ujian"></span>
                    <span class="errmsg">{{ $errors->first('hasil_ujian') }}</span>
                </div>
            </div>
            @if ($jadwal->hasil_ujian != "")
                    
            <div class="row">
                <div class="col-md-3 col-xs-10">
                    Berkas Sebelumnya
                </div>
                <div class="col-md-1 col-xs-1">:</div>
                <div class="col-md-5 col-xs-12">
                    @if ($jadwal->hasil_ujian == "")
                                  - 
                              @else
                                @php
                                    $file = explode('.',$jadwal->hasil_ujian);
                                @endphp

                                @if ($file[1] == 'pdf' || $file[1] == 'PDF')
                                    <a href="{{ url('priview-file')."/hasil_ujian/instansi".$jadwal->hasil_ujian }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:20px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                @else
                                    <a href="{{ url('priview-file')."/hasil_ujian/instansi".$jadwal->hasil_ujian }}" target="_blank"><img src="{{ asset('storage/data/hasil_ujian/instansi')."/".$jadwal->hasil_ujian }}" class="img-rounded img-responsive"></a>
                                @endif
                              @endif
            </div>
            @endif
            <div class="row">
                <div class="col-md-9" style="text-align: right">
                    <button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
                    <button type="submit" class="btn btn-sm btn-default1">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script type="text/javascript">
  $('.hasil_ujian').bind('change', function() {
        var size_hasil_ujian = this.files[0].size;
        var type_hasil_ujian = this.files[0].type;

        if (type_hasil_ujian == 'application/pdf') {
            if(size_hasil_ujian > 2097152){
                $('#errhasil_ujian').html("file tidak boleh lebih dari 2MB");
                setToFalse(hasil_ujian);
            }

            if(size_hasil_ujian < 102796){
                $('#errhasil_ujian').html("file tidak boleh kurang dari 100kb");
                setToFalse(hasil_ujian);
            }

            if (102796 < size_hasil_ujian && size_hasil_ujian < 2097152) {
                $('#errhasil_ujian').html("");
                setToTrue(hasil_ujian);
            }
        } else if (type_hasil_ujian == 'image/jpeg') {
            if(size_hasil_ujian > 2097152){
                $('#errhasil_ujian').html("file tidak boleh lebih dari 2MB");
                setToFalse(hasil_ujian);
            }

            if(size_hasil_ujian < 102796){
                $('#errhasil_ujian').html("file tidak boleh kurang dari 100kb");
                setToFalse(hasil_ujian);
            }

            if (102796 < size_hasil_ujian && size_hasil_ujian < 2097152) {
                $('#errhasil_ujian').html("");
                setToTrue(hasil_ujian);
            }
        } else {
            $('#errhasil_ujian').html("type file tidak boleh selain jpg,jpeg & pdf");
            setToFalse(hasil_ujian);
        }
    });
  </script>
@endsection