@extends('layout.app')

@section('title')
    Data Deskripsi Portofolio
@endsection

@section('css')
<style>
.btn-tbh{
	text-align: right;
}

.btn-jadwal{
	width: 120px;
	background: #E8382A;
	color: #fff;
	font-weight: 600;
}

.btn-jadwal:hover{
	color: aliceblue;
}
</style>
@stop

@section('content')
@if (session('msg'))
			@if (session('msg') == "berhasil")
			<div class="row">
				<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>Berhasil Simpan Data</strong>
						</div>
				</div>
			</div> 
			@endif
			@if (session('msg') == "gagal")
			<div class="row">
					<div class="col-md-12">
							<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Gagal Simpan Data</strong>
								</div>
					</div>
				</div> 
			@endif

			@if (session('msg') == "berhasil_update")
			<div class="row">
				<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>Berhasil Update Data</strong>
						</div>
				</div>
			</div> 
			@endif
			@if (session('msg') == "gagal_update")
			<div class="row">
					<div class="col-md-12">
							<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Gagal Update Data</strong>
								</div>
					</div>
				</div> 
			@endif
@endif
<div class="main-box">
		<div class="min-top">
			<div class="row">
				<div class="col-md-1 text-center">
					<b>Show</b>
				</div>
				<div class="col-md-2 col-6">
						<select name='length_change' id='length_change' class="form-control">
                                <option value='10' selected>10</option>
								<option value='50'>50</option>
								<option value='100'>100</option>
								<option value='150'>150</option>
								<option value='200'>200</option>
						</select>
				</div>
				<div class="col-md-4 col-6">
						<div class="input-group">
								<div class="input-group addon">
									<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
									<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
								</div>
						</div>
				</div>
				
			</div> 
		</div>
		<div class="table-responsive">
			<table id="example1" class="table table-bordered table-striped">
					<thead>
					<tr>
						<th>#</th>
						<th>Nama</th>
						<th>Jenjang</th>
						<th>Deskripsi</th>
						<th>Aksi</th>
					</tr>
					</thead>
					<tbody>
						@php
							$no_urut = 1;
						@endphp
						@foreach ($data as $key => $datas)
						<tr>
							<td>{{ $no_urut++ }}</td>
							<td>{{ $datas->nama }}</td>
							<td>{{ $datas->jenjang }}</td>
							<td>@php
                                echo $datas->keterangan
                                @endphp </td>
							<td>
								<div class="dropdown">
										<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
										<ul class="dropdown-menu">
												<li><a href="{{ url('edit-deskripsi-portofolio')."/".$datas->id }}">Edit</a></li>
										</ul>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
			</table>
		</div>
</div>    
@endsection