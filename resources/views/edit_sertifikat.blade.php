@extends('layout.app')
@section('css')
<style type="text/css">
	.form-control
	{
		border-radius: 5px;
		height: 27px;
		padding: 0px;
		padding-left: 10px;
	}

	.bin{
		color: #dd4b39;
	}

	.row{
		margin: 0px 15px 15px 15px; 
	}

	.main-box{
		font-weight: 600;
	}
</style>
@stop
@section('content')
<form action="/edit-punya-sertifikat" method="post" enctype="multipart/form-data">
	@csrf
		<div class="main-box">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<h3>Edit Data Sertifikat</h3><hr>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					No. Blanko
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="text" class="form-control" name="no_blanko" value="">
					<span class="errmsg"></span>
				</div>
			</div>
			<input type="hidden" id="id" name="id" value="{{ $id }}">
			<input type="hidden" id="id" name="metode_inp" value="{{ $data->metodes == 'verifikasi' ? 'Verifikasi Portofolio' : 'Tes Tertulis' }}">
			<input type="hidden" id="tipe" name="tipe" value="{{ $tipe }}">
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Tanggal Sertifikat <span class="btng">*</span>
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<div class='input-group date' id='datetimepicker'>
							<input type='text' class="form-control" name="tanggal_tes" value="{{ old('tanggal_tes') }}" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<span class="errmsg">{{ $errors->first('tanggal_tes') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Nama Di Sertifikat
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="text" class="form-control" name="nama" value="{{ $data->nama }}">
					<span class="errmsg"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					NIP Di Sertifikat
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="text" class="form-control" name="nip" value="{{ $data->nip }}" onkeypress='validate(event)' maxlength="18">
					<span class="errmsg"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Pas Foto 3 X 4
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
				<input type="file" name="pas_foto_3_x_4" class="custom-file-input" id="customFile" value="{{ old('pas_foto_3_x_4') }}" accept="image/jpg, application/pdf, image/jpeg">
					<input type="hidden" name="old_pas_foto_3_x_4" value="">
					<label class="custom-file-label" for="customFile"></label>
					<span class="errmsg"></span>
					<div class="col-md-7 col-xs-12">
						berkas lama:
					@if (is_null($data->pas_foto_3_x_4))
						-
					@else
						@php
							$pas_foto_3_x_4 = explode('.',$data->pas_foto_3_x_4);
						@endphp
						@if ($pas_foto_3_x_4[1] == 'pdf' || $pas_foto_3_x_4[1] == 'PDF')
							<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$data->pas_foto_3_x_4 }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
						@else
							<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$data->pas_foto_3_x_4 }}" target="_blank"><img src="{{ asset('storage/data/pas_foto_3_x_4')."/".$data->pas_foto_3_x_4 }}" height="42" width="42" class="img-rounded"></a>
						@endif
					@endif      
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-9" style="text-align:right">
					<button type="reset" class="btn btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
					<button type="submit" class="btn btn-default1">Simpan</button>
					<a href="{{ url('print-sertifikat-belakang/'.$tipe.'/'.$id.'/'.$id_jadwal) }}"><button type="button" class="btn btn-default3">Template Belakang</button></a>
				</div>
			</div>
		</div>
	</form>
@stop
@section('js')
<script type="text/javascript">
	$(function () {
		$('#datetimepicker').datetimepicker({
			format: 'DD/MM/YYYY'
		});
	});

</script>
@endsection