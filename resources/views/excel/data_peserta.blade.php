<table>
	<tr>
		<td colspan="8" style="font-size: 20px; text-align:center;height: 25px;">Data Peserta</td>
	</tr>
</table>
<table>
	<thead>
		<tr>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">No</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Nama</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">NIP</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Instansi</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Pangkat/Gol.</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Jenjang</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Status</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Verifikator Dokumen Persyaratan</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Keikusertaan Peserta</th>
		</tr>
	</thead>
	<tbody>
	@foreach ($peserta as $key => $pesertas)
	<tr>
		<td style="border: 2px solid black;text-align: center;">{{ $key++ + 1 }}</td>
		<td style="border: 2px solid black">{{ $pesertas->nama }}</td>
		<td style="border: 2px solid black;text-align: left;">@php echo $pesertas->nip; @endphp</td>
		<td style="border: 2px solid black">{{ $pesertas->nama_instansis }}</td>
		<td style="border: 2px solid black">{{ $pesertas->jabatan }}</td>
		<td style="border: 2px solid black">{{ ucwords($pesertas->jenjang) }}</td>
		<td style="border: 2px solid black">{{ $pesertas->status }}</td>
		<td style="border: 2px solid black">
			{{ $pesertas->bangprof == "" ? '-' : $pesertas->bangprof }}
		</td>
		@if($pesertas->status_peserta == 'tidak_aktif')
		<td style="border: 2px solid black;text-align: center;">{{ 'Tidak Aktif' }}</td>
		@elseif($pesertas->status_peserta == 'aktif')
		<td style="border: 2px solid black;text-align: center;">{{ 'Aktif' }}</td>
		@else
		<td style="border: 2px solid black;text-align: center;">-</td>
		@endif
	</tr>
	@endforeach
	</tbody>
</table>