<table>
	<tr>
		<td colspan="8" style="font-size: 20px; text-align:center;height: 25px;">Data Riwayat Ujian Peserta</td>
	</tr>
</table>
<table>
	<thead>
		<tr>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">No</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">No Ujian</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Nama</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">NIP</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Instansi</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Tanggal Ujian</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Metode Ujian</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Jenjang</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">No. Surat Permohonan Mengikuti Penyesuaian/Inpassing</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Hasil Ujian</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($data_riwayat as $key => $datas)
				<tr>
					<td style="border: 2px solid black;text-align: center;">{{ $key++ + 1 }}</td>
				@if($datas->no_ujian != "")
					<td style="border: 2px solid black">{{ $datas->no_ujian }}</td>
				@else
					<td style="border: 2px solid black">{{ '-' }}</td>
				@endif
					<td style="border: 2px solid black">{{ $datas->nama_peserta }}</td>
					<td style="border: 2px solid black;text-align: left;">{{ $datas->nip }}</td>
					<td style="border: 2px solid black">{{ $datas->nama_instansi }}</td>						
					<td style="border: 2px solid black">
					@if($datas->tgl_ujian != "")
						{{ Helper::tanggal_indo($datas->tgl_ujian) }}
					@else
						{{ '-' }}
					@endif
					</td>
					<td style="border: 2px solid black">
					@if($datas->metode == 'tes')
						{{ 'Tes Tertulis' }}
					@else
						{{ 'Verifikasi Portofolio' }}
					@endif
					</td>
					<td style="border: 2px solid black">{{ ucwords( $datas->jenjang)}}</td>
					@if ($datas->no_surat_usulan_peserta == "")
						<td style="border: 2px solid black">{{'-'}}</td>
					@else
						<td style="border: 2px solid black">{{$datas->no_surat_usulan_peserta}}</td>
					@endif                        
					@if($datas->hasil_ujian == "lulus")
						<td style="border: 2px solid black">{{ 'Lulus' }}</td>
					@elseif($datas->hasil_ujian == "tidak_lulus")
						<td style="border: 2px solid black">{{ 'Tidak Lulus' }}</td>
					@elseif($datas->hasil_ujian == "tidak_hadir")
						<td style="border: 2px solid black">{{ 'Tidak Hadir' }}</td>
					@elseif($datas->hasil_ujian == "tidak_lengkap")
						<td style="border: 2px solid black">{{ 'Dokumen Persyaratan Tidak Lengkap' }}</td>
					@else
						<td style="border: 2px solid black">-</td>
					@endif				
				</tr>
				@endforeach	
	</tbody>
</table>