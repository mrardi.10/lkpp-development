<table>
	<tr>
		<td colspan="8" style="font-size: 20px; text-align:center;height: 25px;">Peserta Jadwal Tanggal {{ $jadwal->metode == 'tes_tulis' ? Helper::tanggal_indo($jadwal->tanggal_tes) : Helper::tanggal_indo($jadwal->tanggal_verifikasi) }} Metode Ujian {{ $jadwal->metode == 'tes_tulis' ? "Tes Tertulis" : "Verifikasi Portofolio" }}</td>
	</tr>
</table>
<table>
	<thead>
		<tr>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">No</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Nama</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">NIP</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Instansi</th> 
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Pangkat/Gol.</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Jenjang</th>
			{{-- <th>Foto</th> --}}
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Status{{--  (Hasil Verifikasi Dokumen Portofolio) --}}</th>
			{{-- <th>Metode Ujian</th> --}}
			@if($jadwal->metode == 'tes_tulis' || $jadwal->metode == 'verifikasi_portofolio')
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Verifikator</th>
			@endif
			@if($jadwal->metode == 'verifikasi_portofolio' || $jadwal->metode == 'tes_tulis')
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Asesor</th>
			@endif
		</tr>
	</thead>
	<tbody>
		@php
			$no_urut = 1;
		@endphp
		@foreach ($data as $key => $datas)
		@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
		<tr>
			<td style="border: 2px solid black">{{ $no_urut++ }}</td>
			<td style="border: 2px solid black">{{ $datas->nama }}</td>
			<td style="border: 2px solid black;text-align: left;">{{ $datas->nip }}</td>
			<td style="border: 2px solid black">{{ $datas->nama_instansis }}</td> 
			<td style="border: 2px solid black">{{ $datas->jabatan }}</td>
			<td style="border: 2px solid black">{{ ucwords($datas->jenjang) }}</td>
			{{-- <td>
				@if ($datas->fotos == "")
				Tidak Ada foto
				@else
				@php
				$foto = explode('.',$datas->fotos);
				@endphp
				@if ($foto[1] == 'pdf' || $foto[1] == 'PDF')
				<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$datas->fotos }}" target="_blank"><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="Lihat Foto"></i></a>
				@else
				<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$datas->fotos }}" target="_blank"><img src="{{ asset('storage/data/pas_foto_3_x_4/'.$datas->fotos) }}" alt="" class="img-responsive" width="60px"></a>
				@endif
				@endif
			</td> --}}
			@if($datas->status_jadwal == 'lulus' && $datas->publish == 'publish')
					<td style="border: 2px solid black">{{ 'Lulus' }}</td>
					@elseif($datas->status_jadwal == 'tidak_lulus' && $datas->publish == 'publish')
					<td style="border: 2px solid black">{{ 'Tidak Lulus' }}</td>
					@elseif($datas->status_jadwal == 'tidak_hadir' && $datas->publish == 'publish')
					<td style="border: 2px solid black">{{ 'Tidak Hadir' }}</td>
					@elseif($datas->status_jadwal == 'tidak_lengkap' && $datas->publish == 'publish')
					<td style="border: 2px solid black">{{ 'Dokumen Persyaratan Tidak Lengkap' }}</td>
					@else
					<td style="border: 2px solid black">{{ $datas->status }}</td>
					@endif
			{{-- @if($datas->status_jadwal == 'lulus' && $datas->publish == 'publish')
			<td style="border: 2px solid black">{{ 'Lulus' }}</td>
			@elseif($datas->status_jadwal == 'tidak_lulus' && $datas->publish == 'publish')
			<td style="border: 2px solid black">{{ 'Tidak Lulus' }}</td>
			@elseif($datas->status_jadwal == 'tidak_hadir' && $datas->publish == 'publish')
			<td style="border: 2px solid black">{{ 'Tidak Hadir' }}</td>
			@else
			<td style="border: 2px solid black">{{ $datas->status }}</td>
			@endif --}}

			@if($jadwal->metode == 'tes_tulis'  || $jadwal->metode == 'verifikasi_portofolio')
			<td style="border: 2px solid black">{{ $datas->verifikators == "" ? "-" : $datas->verifikators }}</td>
			@endif
			<td style="border: 2px solid black">
			@if($datas->asesors == "")
						@php
						if (isset($get_ases[$datas->asesor])) {
							echo $get_ases[$datas->asesor];
						}else{
							echo "-";
						}
						@endphp
					@else
						{{ $datas->asesors }}
					@endif
			</td>
		</tr>
		@endif
		@if(Auth::user()->role == 'asesor' || Auth::user()->role == 'verifikator')
		@if(Auth::user()->id == $datas->asesor || Auth::user()->id == $datas->assign)
		<tr>
			<td style="border: 2px solid black">{{ $no_urut++ }}</td>
			<td style="border: 2px solid black">{{ $datas->nama }}</td>
			<td style="border: 2px solid black;text-align: left;">{{ $datas->nip }}</td>
			<td style="border: 2px solid black">{{ $datas->nama_instansis }}</td> 
			<td style="border: 2px solid black">{{ $datas->jabatan }}</td>
			<td style="border: 2px solid black">{{ ucwords($datas->jenjang) }}</td>
			{{-- <td>
				@if ($datas->fotos == "")
				Tidak Ada foto
				@else
				@php
				$foto = explode('.',$datas->fotos);
				@endphp
				@if ($foto[1] == 'pdf' || $foto[1] == 'PDF')
				<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$datas->fotos }}" target="_blank"><i class="fa fa-file-pdf-o" style="font-size:27px" data-toggle="tooltip" title="Lihat Foto"></i></a>
				@else
				<a href="{{ url('priview-file')."/pas_foto_3_x_4/".$datas->fotos }}" target="_blank"><img src="{{ asset('storage/data/pas_foto_3_x_4/'.$datas->fotos) }}" alt="" class="img-responsive" width="60px"></a>
				@endif
				@endif
			</td> --}}
			@if($datas->status_jadwal == 'lulus' && $datas->publish == 'publish')
			<td style="border: 2px solid black">{{ 'Lulus' }}</td>
			@elseif($datas->status_jadwal == 'tidak_lulus' && $datas->publish == 'publish')
			<td style="border: 2px solid black">{{ 'Tidak Lulus' }}</td>
			@elseif($datas->status_jadwal == 'tidak_hadir' && $datas->publish == 'publish')
			<td style="border: 2px solid black">{{ 'Tidak Hadir' }}</td>
			@elseif($datas->status_jadwal == 'tidak_lengkap' && $datas->publish == 'publish')
			<td style="border: 2px solid black">{{ 'Dokumen Persyaratan Tidak Lengkap' }}</td>
			@else
			<td style="border: 2px solid black">{{ $datas->status }}</td>
			@endif
			@if($jadwal->metode == 'tes_tulis'  || $jadwal->metode == 'verifikasi_portofolio')
			<td style="border: 2px solid black">{{ $datas->verifikators == "" ? "-" : $datas->verifikators }}</td>
			@endif
			<td style="border: 2px solid black">
			@if($datas->asesors == "")
						@php
						if (isset($get_ases[$datas->asesor])) {
							echo $get_ases[$datas->asesor];
						}else{
							echo "-";
						}
						@endphp
					@else
						{{ $datas->asesors }}
					@endif
			</td>
		</tr>
		@endif
		@endif
		@endforeach
	</tbody>
</table>