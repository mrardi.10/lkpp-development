<table>
	<tr>
		<td colspan="8" style="font-size: 20px; text-align:center;height: 25px;">Pengusulan Eformasi</td>
	</tr>
</table>
<table>
	<thead>
		<tr>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">No.</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Nama</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Instansi</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Jumlah JF PPBJ Pertama</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Status Usulan</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Jumlah JF PPBJ Muda</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Status Usulan</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Jumlah JF PPBJ Madya</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Status Usulan</th>
		</tr>
	</thead>
	<tbody>
	@foreach ($eformasi as $key => $eformasis)
		<tr>
			<td style="border: 2px solid black;">{{ $key++ + 1 }}</td>
			<td style="border: 2px solid black;">{{ $eformasis->namas }}</td>
			<td style="border: 2px solid black;">{{ $eformasis->instansis }}</td>
			<td style="border: 2px solid black;text-align: center;">{{ $eformasis->pertama }}</td>
			<td style="border: 2px solid black;text-align: center;">
				@if (is_null($eformasis->status_pertama))
						-
				@else
					{{ $eformasis->status_pertama == 'setuju' ? 'Setuju' : 'Tidak Setuju' }}
				@endif
			</td>
			<td style="border: 2px solid black;text-align: center;">{{ $eformasis->muda }}</td>
			<td style="border: 2px solid black;text-align: center;">
				@if (is_null($eformasis->status_muda))
					-	
				@else
					{{ $eformasis->status_muda == 'setuju' ? 'Setuju' : 'Tidak Setuju' }}
				@endif
			</td>
			<td style="border: 2px solid black;text-align: center;">{{ $eformasis->madya }}</td>
			<td style="border: 2px solid black;text-align: center;">
				@if (is_null($eformasis->status_madya))
						-
				@else
					{{ $eformasis->status_madya == 'setuju' ? 'Setuju' : 'Tidak Setuju' }}	
				@endif
			</td>
		</tr>
	@endforeach
	</tbody>
</table>