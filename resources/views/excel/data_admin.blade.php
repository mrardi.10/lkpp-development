<table>
	<tr>
		<td colspan="8" style="font-size: 20px; text-align:center;height: 25px;">Data Admin</td>
	</tr>
</table>
<table>
	<thead>
		<tr>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">No.</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Nama</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Instansi</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Email</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">No. Telepon/HP</th>
			<th style="border: 2px solid black;font-weight: 600;text-align: center;">Status</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($data as $key => $datas)
		<tr>
			<td style="border: 2px solid black;">{{ $key++ + 1 }}</td>
			<td style="border: 2px solid black;">{{ $datas->name }}</td>
			<td style="border: 2px solid black;">{{ $datas->instansis }}</td>
			<td style="border: 2px solid black;">{{ $datas->email }}</td>
			<td style="border: 2px solid black;text-align: left;">{{ $datas->no_telp }}</td>
			<td style="border: 2px solid black;">{{ $datas->email_verified_at != "" ? 'Aktif' : 'Tidak Aktif' }}</td>
		</tr>
		@endforeach
	</tbody>
</table>