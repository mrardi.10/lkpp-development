<table>
	<tr>
		<td colspan="8" style="font-size: 20px; text-align:center;height: 25px;">Peserta Jadwal Tanggal {{ $jadwal->metode == 'tes_tulis' ? Helper::tanggal_indo($jadwal->tanggal_tes) : Helper::tanggal_indo($jadwal->tanggal_verifikasi) }} Metode Ujian {{ $jadwal->metode == 'tes_tulis' ? "Tes Tertulis" : "Verifikasi Portofolio" }}</td>
	</tr>
</table>
<table>
	<thead>
		<tr>
			<th style="border: 2px solid black;font-weight:600;text-align: center;">No</th>
			<th style="border: 2px solid black;font-weight:600;text-align: center;">Nama</th>
			<th style="border: 2px solid black;font-weight:600;text-align: center;">NIP</th>
			{{-- <th>Instansi</th> --}}
			<th style="border: 2px solid black;font-weight:600;text-align: center;">Pangkat/Gol.</th>
			<th style="border: 2px solid black;font-weight:600;text-align: center;">Jenjang</th>
			{{-- <th style="border: 2px solid black;font-weight:600;text-align: center;">Foto</th> --}}
			<th style="border: 2px solid black;font-weight:600;text-align: center;">Status</th>
			@if($jadwal->metode == 'tes_tulis')
			<th style="border: 2px solid black;font-weight:600;text-align: center;">Verifikator</th>
			@endif
			@if($jadwal->metode == 'verifikasi_portofolio')
			<th style="border: 2px solid black;font-weight:600;text-align: center;">Asesor</th>
			@endif
		</tr>
	</thead>
	<tbody>
		@foreach ($data as $key => $datas)
		@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp' || Auth::user()->id == $datas->asesor|| Auth::user()->id == $datas->assign)
		<tr>
			<td style="border: 2px solid black;">{{ $key++ + 1 }}</td>
			<td style="border: 2px solid black;">{{ $datas->nama }}</td>
			<td style="border: 2px solid black;text-align: left;">{{ $datas->nip }}</td>
			{{-- <td>{{ $datas->nama_instansi }}</td> --}}
			<td style="border: 2px solid black;">{{ $datas->jabatan }}</td>
			<td style="border: 2px solid black;">{{ ucwords($datas->jenjang) }}</td>
			<td style="border: 2px solid black;">
				@if($datas->status == 'lulus' && $datas->publish == 'publish')
					<td>{{ 'Lulus' }}</td>
					@elseif($datas->status == 'tidak_lulus' && $datas->publish == 'publish')
					<td>{{ 'Tidak Lulus' }}</td>
					@elseif($datas->status == 'tidak_hadir' && $datas->publish == 'publish')
					<td>{{ 'Tidak Hadir' }}</td>
					@else
					<td>{{ $datas->statuss }}</td>
					@endif
			</td>
			@if($jadwal->metode == 'tes_tulis')
			<td style="border: 2px solid black;">{{ $datas->verifikators == "" ? "-" : $datas->verifikators }}</td>
			@endif
			@if($jadwal->metode == 'verifikasi_portofolio')
			<td style="border: 2px solid black;">{{ $datas->asesors == "" ? "-" : $datas->asesors }}</td>
			@endif
			@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof' || Auth::user()->role == 'verifikator' || Auth::user()->role == 'dsp' || Auth::user()->role == 'asesor')
			@endif
		</tr>
		@endif
		@endforeach
	</tbody>
</table>