@extends('layout.app2')
@section('title')
	Unggah Dokumen Portofolio
@endsection
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
	body{
		background-color: whitesmoke;
	}
	
	.main-page{
		margin-top: 20px;
	}

	.box-container{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		background-color: white;
		border-radius: 5px;
		padding: 3%;
	}

	.shadow{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		max-width: 250px;
		line-height: 15px;
		width: 100%;
		margin: 5px;
	}

	.custom-file{
		margin: 5px;
	}
	
	.fileUpload {
		position: relative;
		overflow: hidden;
		margin: 10px;
	}

	.fileUpload input.upload {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 20px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}

	.textFile::-webkit-input-placeholder{
		font-size: 15px;
	}
	
	.textFile::-moz-input-placeholder{
		font-size: 15px;
	}
	
	.textFile::-ms-input-placeholder{
		font-size: 15px;
	}
	
	div.dataTables_wrapper div.dataTables_info{
		display: none;
	}

	div.dataTables_wrapper .row.col-sm-12{
		width: 120px;
	}

	p{
		font-weight: 500;
	}

	b{
		font-weight: 500;
	}

	.row a.btn{
		text-align: left;
		font-weight: 600;
		font-size: small;
	}

	.btn-default1{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
		color: white;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default2{
		background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
		color: black;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default3{
		background: #fff;
		color: #000;
		font-weight: 500;
		width: 100px;
	}

	.row-input{
		padding-bottom: 10px;
	}

	.page div.verifikasi{
		display: none;
	}

	.btn-area{
		text-align: right;
	}

	.clickable-row:hover{
		cursor: pointer;
	}

	.checkmark {
		top: 0;
		left: 0;
		height: 25px;
		width: 25px;
		background-color: #00aaaa;
	}

	.modal .modal-dialog{
		max-width: 800px !important;
	}

	.line-center{
		line-height: 2.5;
	}

	span.berkas{
		font-size: 10px;
		font-weight: 600;
	}

	span.berkas label{
		margin-bottom: 0px !important;
	}
	
	[data-toggle="collapse"] .fa:before {  
		content: "\f139";
	}

	[data-toggle="collapse"].collapsed .fa:before {
		content: "\f13a";
	}

	.alert-row{
		margin-top: 35px;
	}

	.accor-body{
		/* margin: 0px 10px; */
	}
	
	ol.ol-sub-a{
		padding-inline-start: 18px !important;
	}

	.btn-link{
		color: #000 !important;
	}

	.btn-judul{
		width: 100%;
		text-align: left;
		background: #f8f8f8;
		-webkit-box-shadow: 0px 3px 4px -3px #000000; 
		box-shadow: 0px 3px 4px -3px #000000;
	}

	.collapse-body{
		/* padding: 15px; */
		border: 1px solid #b3aeae;
	}

	.collapse-all{
		margin: 10px 0px;
	}

	span.err{
		color: red;
		position: relative;
	}

	.lis{
		padding: 10px 30px;
		border-bottom: 1px solid #c3bdbd;
		/* background: #f1f1f1; */
		margin-bottom: 10px;
	}

	.bg-active{
		background: #f8f8f8;
	}

	.main-page img{
		width: 80px;
	}

	.table>tbody>tr.active>td,
	.table>tbody>tr.active>th,
	.table>tbody>tr>td.active,
	.table>tbody>tr>th.active,
	.table>tfoot>tr.active>td,
	.table>tfoot>tr.active>th,
	.table>tfoot>tr>td.active,
	.table>tfoot>tr>th.active,
	.table>thead>tr.active>td,
	.table>thead>tr.active>th,
	.table>thead>tr>td.active,
	.table>thead>tr>th.active {
		background-color: #fff;
	}

	.table-bordered > tbody > tr > td,
	.table-bordered > tbody > tr > th,
	.table-bordered > tfoot > tr > td,
	.table-bordered > tfoot > tr > th,
	.table-bordered > thead > tr > td,
	.table-bordered > thead > tr > th {
		border-color: #e4e5e7;
	}
	
	.table tr.header {
		/* font-weight: bold; */
		/* background-color: whitesmoke; */
		cursor: pointer;
		-webkit-user-select: none;
		/* Chrome all / Safari all */
		-moz-user-select: none;
		/* Firefox all */
		-ms-user-select: none;
		/* IE 10+ */
		user-select: none;
		/* Likely future */
	}

	.table tr:not(.header) {
		display: none;
	}

	.table .header td:after {
		content: "\002b";
		position: relative;
		top: 1px;
		display: inline-block;
		font-family: 'Glyphicons Halflings';
		font-style: normal;
		font-weight: 400;
		line-height: 1;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
		float: right;
		color: #999;
		text-align: center;
		padding: 3px;
		transition: transform .25s linear;
		-webkit-transition: -webkit-transform .25s linear;
	}

	.table .header.active td:after {
		content: "\2212";
	}

	.table td{
		position: relative;
	}
</style>
@endsection
@section('content')
{{-- @inject('request', 'Illuminate\Http\Request') --}}
<div class="main-page">
	<div class="container">
		<div class="row box-container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12" style="">
						<div class="row">
							<div class="col-md-9">
								<h5>Unggah Dokumen Portofolio <br>
									Peserta Uji Kompetensi Verifikasi Portofolio tanggal {{ Helper::tanggal_indo($jadwal->tanggal_verifikasi) }}
								</h5><hr>
								@if (session('msg'))
									@if (session('msg') == "berhasil")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil simpan data</strong>
										</div>
									@elseif (session('msg') == "berhasil_hapus_porto")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil hapus data</strong>
										</div>
									@elseif (session('msg') == "gagal_hapus_porto")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal simpan data</strong>
										</div>
									@elseif (session('msg') == "data_kosong")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Wajib Mengupload Dokumen Portofolio</strong>
										</div>
									@else
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal simpan data</strong>
										</div> 
									@endif
								@endif
								<div id="notifMin"></div>
							</div>
							<div class="col-md-3" style="text-align:right">
								<i class="fa fa-bell"></i>
							</div>
							<div class="col-md-9 page">
							{{-- <form action="{{ url('store-dokumen-ujian') }}" method="post" enctype="multipart/form-data" id="store"> --}}
							@csrf
								<input type="hidden" name="tanggal_ujian" value="{{ Helper::tanggal_indo($jadwal->tanggal_ujian) }}">
								<input type="hidden" name="lokasi_ujian" value="{{ $jadwal->lokasi_ujian }}">
								<div class="row">
									<div class="col-md-3">
										Nama
									</div>
									<div class="col-md-1">:</div>
									<div class="col-md-8">
										{{ $peserta->nama }}
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										Pangkat/Gol.
									</div>
									<div class="col-md-1">:</div>
									<div class="col-md-8">{{ $peserta->jabatan }}</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										Jenjang
									</div>
									<div class="col-md-1">:</div>
									<div class="col-md-8">
										{{ ucwords($peserta->jenjang) }}
									</div>
								</div>
								<div class="row alert-row">
									<div class="col-md-12">
										<div class="alert alert-primary" role="alert">
											<strong style="text-transform: uppercase;">Dokumen yang diunggah min. 100KB max. 2MB, dengan format PDF, JPG, JPEG</strong>
										</div>
									</div>
								</div>
								<input type="hidden" name="id_peserta" value="{{ $peserta->id }}">
								<input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
								<div class="row accor">
									<div class="container">
										<div id="accordion">
										@php
											$jumlah_poin = 0;
											$jumlah_upload = 0;
											$jumlah_upload_lain = 0;
										@endphp
										@foreach ($judul_input as $key => $poins)
											@php
												$jumlah_sebelumnya = $jumlah_poin;
												$key++;
												$alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
												foreach($detail_input as $details){
													if($details->id_judul_input == $poins->id){
														$jumlah_dokumen = 0;
														$jumlah_dokumen_lain = 0;
														$jumlah_poin++;
														foreach ($input_form as $inputs){
															if($inputs->id_detail_input == $details->id){
																if($dokumen != ""){
																	foreach($dokumen as $dokumens){
																		if ($inputs->title_1 != "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {
																			if($inputs->id == $dokumens->id_detail_file_input){
																				$jumlah_dokumen++;
																			}
																		}
																		
																		if ($inputs->title_1 == "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {
																			if($inputs->id == $dokumens->id_detail_file_input){
																				$jumlah_upload_lain++;
																			}
																		}
																	}
																}
															}
														}
														
														if ($details->nama != "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {
															if ($jumlah_dokumen == 2) {
																$jumlah_upload++;
															} 
														}
													}
												}
											@endphp
											<div class="collapse-all">
												<div id="heading{{ $key }}">
													<h5 class="mb-0">
														<button class="btn btn-link collapsed btn-judul" @if($jumlah_upload == 0 ) @elseif($jumlah_upload < $poins->min_komponen) style="background: #ed473b;" @else style="background: #5693f5;" @endif @if($jumlah_upload_lain != 0) style="background: #5693f5;" @endif data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="false" aria-controls="collapse{{ $key }}" type="button">
														{{ $alphabet[$key - 1].'. '.$poins->nama.' '.$poins->keterangan }} <span class="float-right">@if($poins->nama != "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan"){{ $jumlah_upload }}/{{ $jumlah_poin - $jumlah_sebelumnya }} @endif @if($poins->nama == "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {{ $jumlah_upload_lain }}/{{ $jumlah_poin - $jumlah_sebelumnya }} @endif <i class="fa" aria-hidden="true"></i></span>
														@php
															$jumlah_upload = 0;
															$jumlah_upload_lain = 0;
														@endphp 
														</button>
													</h5>
												</div>
												<div id="collapse{{ $key }}" class="collapse collapse-body" aria-labelledby="heading{{ $key }}" data-parent="#accordion">
												<form id = "pesertaForm_{{ $poins->id }}"action="{{ url('unggah-dokumen-portofolio') }}" method="post" enctype="multipart/form-data">
												@csrf 
													<input type="hidden" name="tanggal_ujian" value="{{ Helper::tanggal_indo($jadwal->tanggal_ujian) }}">
													<input type="hidden" name="lokasi_ujian" value="{{ $jadwal->lokasi_ujian }}">
													<input type="hidden" name="id_peserta" value="{{ $peserta->id }}">
													<input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}">
													<input type="hidden" name="nip" value="{{ $peserta->nip }}">
													<input type="hidden" name="nama_peserta" value="{{ $peserta->nama }}">
													<input type="hidden" name="id_judul" value="{{ $poins->id }}">
													<input type="hidden" name="jenjang" value="{{ $peserta->jenjang }}">
													<div class="accor-body">
														<table class="table table-bordered">
														@php
															$no_detail = 0;
														@endphp
														@foreach ($detail_input as $details)
															@if ($details->id_judul_input == $poins->id)
																@php
																	$no_detail++;
																@endphp
																<tr class="header {{ $details->nama == "" ? 'active' : '' }}">
																	<td colspan="3" style="padding-left: 20px">{{ $no_detail.'. '.$details->nama }} @if($details->keterangan == "") @else <a href="#" data-toggle="modal" style="margin-left: 1%;" class="btn btn-sm btn-info float-right" data-target="#modalDetailDeskripsi{{ $details->id }}">Lihat Deskripsi</a>@endif</td>
																</tr>
																@php
																	$no_in = 0;
																@endphp
																@foreach ($input_form as $inputs)
																	@if ($inputs->id_detail_input == $details->id)
																		@php
																			$alphabet_in = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
																		@endphp
																		<tr
																		@if ($details->nama == "") style="display: table-row;" @endif >
																			<td rowspan="2" style="padding-left: 35px;">{{ $alphabet_in[$no_in].'.' }}</td>
																			@if ($inputs->nama_input_1 != "")
																				<td>{{ $inputs->title_1 }}</td>
																				<td>
																				{{-- @if ($inputs->title_1 != "Dokumen Hasil Pekerjaan") --}}
																				<input id="uploadFile_{{ $inputs->nama_input_1 }}" class="{{ $inputs->nama_input_1 }} form-control textFile" placeholder="Tidak ada Dokumen yang dipilih" disabled="disabled" />
																				<div class="fileUpload btn btn-primary">
																					<span>Pilih Dokumen</span>
																					<input type="file" class="upload" name="{{ $inputs->nama_input_1 }}" id="cek_file_{{ $inputs->nama_input_1 }}"  class="{{ $inputs->nama_input_1 }} uploadBtn" accept="application/pdf, .jpg, .jpeg, image/jpg, image/jpeg"/>
																				</div>        
																				{{-- <input type="hidden" id="cek_tes_{{$inputs->id}}" value="1"> --}}                 
																				{{-- @endif --}}
																				@if ($dokumen != "")
																					@foreach ($dokumen as $dokumens)
																						{{-- @if ($inputs->id > $dokumens->id_detail_file_input)
																							<input id="uploadFile_{{ $inputs->nama_input_1 }}" class="{{ $inputs->nama_input_1 }} form-control textFile" placeholder="Tidak ada Dokumen yang dipilih" disabled="disabled" />
																							<div class="fileUpload btn btn-primary">
																								<span>Pilih Dokumen</span>
																								<input type="file" class="upload" name="{{ $inputs->nama_input_1 }}" id="cek_file_{{ $inputs->nama_input_1 }}"  class="{{ $inputs->nama_input_1 }} uploadBtn" accept="application/pdf, .jpg, .jpeg, image/jpg, image/jpeg"/>
																							</div>
																							@php
																								$cek_upload = false;
																							@endphp
																						@endif --}}

																						@if ($inputs->id == $dokumens->id_detail_file_input)
																						<input type="hidden" id="cek_tes_{{$inputs->id}}" value="1">
																							<div class="row" style="margin-top:10px">
																								<div class="col-md-5">Berkas Sebelumnya</div>
																								<div class="col-md-1 line-center">:</div>
																								<div class="col-md-5" style="line-height: 3">
																								@if ($dokumens->file == "")
																									- 
																								@else
																									@php
																										$file = explode('.',$dokumens->file);
																									@endphp
																									@if ($file[1] == 'pdf' || $file[1] == 'PDF')
																										<a href="{{ url('priview-file')."/dokumen_portofolio/".$dokumens->file }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:20px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
																										<a href="#" data-toggle="modal" style="margin-top: 2%;" class="btn btn-secondary" data-target="#modalHapusDokumen{{ $inputs->id }}"><i class="fa fa-trash"></i> Hapus </a>                                  
																									@else
																										<a href="{{ url('priview-file')."/dokumen_portofolio/".$dokumens->file }}" target="_blank"><img src="{{ asset('storage/data/dokumen_portofolio')."/".$dokumens->file }}" class="img-rounded"></a>
																										<a href="#" data-toggle="modal" style="margin-top: 2%;" class="btn btn-secondary" data-target="#modalHapusDokumen{{ $inputs->id }}"><i class="fa fa-trash"></i> Hapus</a>
																									@endif
																									
																								@endif
																								</div>
																							</div>
																						{{-- @else
																						<p>Tidak ada berkas yang diupload</p> --}}
																						@endif
																					@endforeach
																					@else
																					<input type="hidden" id="cek_tes_{{$inputs->id}}" value="0">

																				@endif    
																				<br>
																				<span class="err" id="{{ "err".$inputs->nama_input_1 }}"></span>
																			</td>
																		@endif
																	</tr>
																<tr
																@if ($details->nama == "")
																	style="display: table-row;"
																@endif
																>
																@if ($inputs->nama_input_3 != "")
																	<td>{{ $inputs->title_3 }}</td>
																	<td>
																		<input type="text" class="form-control" name="{{ $inputs->nama_input_3 }}" 
																		<?php 
																			if($dokumen != ""){
																				foreach ($dokumen as $dokumens) {
																					if ($inputs->id == $dokumens->id_detail_file_input) {
																						echo "value='".$dokumens->nama_sertifikat."'";
																					}
																				}
																			} else {
																				echo "value=''";
																			}
																		?>
																		>
																	</td>
																@else
																@endif
																</tr>
																<tr
																@if ($details->nama == "")
																	style="display: table-row;"
																@endif
																>
																@if ($inputs->nama_input_2 != "")
																	<td></td>
																	<td>{{ $inputs->title_2 }}</td>
																	<td>{{-- @if($inputs->nama_input_2 == "55_13_tahun_sertifikat" || $inputs->nama_input_2 == "55_14_tahun_sertifikat" || $inputs->nama_input_2 == "55_15_tahun_sertifikat") --}}
																		<input type="text" class="form-control" name="{{ $inputs->nama_input_2 }}"  onkeypress='validate(event)'
																		{{-- @endif --}}
																		<?php 
																			if($dokumen != ""){
																				foreach ($dokumen as $dokumens) {
																					if ($inputs->id == $dokumens->id_detail_file_input) {
																						echo "value='".$dokumens->tahun."'";
																					}
																				}
																			} else {
																				echo "value=''";
																			}
																		?>>
																	</td>
																@endif
																</tr>                
																@if ($status_file != "")
																	@foreach ($status_file as $statuf)
																		@if ($statuf->id_detail_file_input == $inputs->id)
																			@php
																				if($statuf->status == ""){
																					$title_badge = "-";
																					$badge_class = "badge badge-secondary";
																				}
																				
																				if($statuf->status == "sesuai"){
																					$title_badge = "Sesuai";
																					$badge_class = "badge badge-success";
																				}
																				
																				if($statuf->status == "tidak_sesuai"){
																					$title_badge = "Tidak Sesuai";
																					$badge_class = "badge badge-warning";
																				}
																			@endphp
																			<tr
																			@if ($details->nama == "")
																				style="display: table-row;"
																			@endif
																			>
																				<td></td>
																				<td>Status</td>
																				<td><span class="{{ $badge_class }}">{{ $title_badge }}</span></td>
																			</tr>
																			<tr
																				@if ($details->nama == "")
																					style="display: table-row;"
																				@endif
																			>
																			@if($statuf->status != "")
																				<td></td>
																				<td>Keterangan</td>
																				<td><textarea readonly="" class="form-control" rows="10" cols="100">{{ $statuf->keterangan == "" ? '-' : $statuf->keterangan }}</textarea></td>
																			@elseif($statuf->status == "")
																				<td></td>
																				<td>Keterangan</td>
																				<td><textarea readonly="" class="form-control" rows="10" cols="100">{{ '-' }}</textarea></td>
																			@endif
																			</tr>
																		@endif
																	@endforeach
																@endif
															<!-- Modal Deskripsi-->
															<div class="modal fade" id="modalDetailDeskripsi{{ $details->id }}" role="dialog">
																<div class="modal-dialog">
																<!-- Modal content-->
																	<div class="modal-content">
																		<div class="modal-header">
																			<h4 class="modal-title">Detail Deskripsi {{ $details->nama}}</h4>
																			<button type="button" class="close" data-dismiss="modal">&times;</button>
																		</div>
																		<div class="modal-body">
																			<div class="row">
																				<div class="col-md-12">
																					<p>Dokumen Hasil Pekerjaan:</p>
																				</div>
																				<div class="col-md-12">
																					<p>@php echo $details->keterangan; @endphp</p>
																				</div>
																			</div>
																		</div>
																		<div class="modal-footer">
																			<button type="button" class="btn btn-sm btn-default2" data-dismiss="modal">Kembali</button>
																		</div>
																	</div>
																</div>
															</div>
															<div class="modal fade" id="modalHapusDokumen{{ $inputs->id }}" role="dialog">
																<div class="modal-dialog" style="width: 30%">          
																	<!-- Modal content-->
																	<div class="modal-content">
																		<div class="modal-header">
																		{{-- <form action="{{ url('hapus-portofolio-dokumen/'.$inputs->id) }}" method="post" id="hapus{{ $inputs->id }}">
																			 @csrf --}}
																			 <input type="hidden" name="id_jadwal" value="{{ $jadwal->id }}" id="val_jadwal">
																			 <input type="hidden" name="id_peserta" value="{{ $peserta->id }}" id="val_peserta">
																			 <h4 class="modal-title">Hapus Dokumen Portofolio</h4>
																			 <button type="button" class="close" data-dismiss="modal">&times;</button>
																		</div>
																		<div class="modal-body">
																			<div class="row">
																				<div class="col-md-12">
																					<p>Apakah anda yakin menghapus dokumen portofolio?</p>
																				</div>
																			</div>
																		</div>
																		<div class="modal-footer">
																			<button type="button" class="btn btn-sm btn-default2 pull-left" data-dismiss="modal">BATAL</button>
																			<button class="btn btn-sm btn-default1 " id="hapus{{ $inputs->id }}" {{-- form="hapus{{ $inputs->id }}" --}} {{-- value="HAPUS" --}}>HAPUS</button>
																		{{-- </form> --}}
																		</div>
																	</div>
																</div>
															</div>
															@php
																$no_in++;
															@endphp
														@endif
													@endforeach
												@endif
											@endforeach
										</table>
									</div>
									<div class="accor-footer">
										<div class="row">
											<div class="col-md-12">
												<button type="submit" class="btn btn-default1 float-right" id="btnSubmit_{{ $poins->id }}">Simpan</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<button type="button" class="btn btn-md btn-primary" style="text-align:center" id="selesai">Selesai</button>
			</div>
		</div>
	</div>                     
	<div class="col-md-3 text-center">
		@include('layout.button_right_kpp')
	</div>
</div>
</div>
</div>
</div> 
</div>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
	function modifyVar(obj, val) {
		obj.valueOf = obj.toSource = obj.toString = function(){ return val; };
	}

	function setToFalse(boolVar) {
		modifyVar(boolVar, false);
	}

	function setToTrue(boolVar) {
		modifyVar(boolVar, true);
	}
  
	@foreach ($judul_input as $key => $poins)
		@foreach ($detail_input as $details)
			@if ($details->id_judul_input == $poins->id)
				@foreach ($input_form as $inputs)
					@if ($inputs->id_detail_input == $details->id)
						var {{ 'validasi_'.$poins->id.'_'.$details->id.'_'.$inputs->id }} = new Boolean(false);
						var val_{{ $inputs->nama_input_1 }} = new Boolean(true);
					@endif
				@endforeach
			@endif
		@endforeach
	@endforeach
	
	@foreach ($judul_input as $key => $poins)
	$('#btnSubmit_{{ $poins->id }}').click(function (e) {
		var hasil_{{ $poins->id }} = new Boolean(true);		
		@foreach ($detail_input as $details)
			@if ($details->id_judul_input == $poins->id)
				@foreach ($input_form as $num => $inputs)
					@if ($inputs->id_detail_input == $details->id)
						var size_{!! $inputs->nama_input_1 !!} = $(".cek_file_{!! $inputs->nama_input_1 !!}")[0].files[0].size;
						var type_{!! $inputs->nama_input_1 !!} = $(".cek_file_{!! $inputs->nama_input_1 !!}")[0].files[0].type;
						
						if (type_{!! $inputs->nama_input_1 !!} == 'application/pdf') {
							if(size_{!! $inputs->nama_input_1 !!} > 2097152){
								$('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh lebih dari 2MB");
								setToFalse(val_{!! $inputs->nama_input_1 !!});
							}
							
							if(size_{!! $inputs->nama_input_1 !!} < 102796){
								$('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh kurang dari 100kb");
								setToFalse(val_{!! $inputs->nama_input_1 !!});
							}
							
							if (102796 < size_{!! $inputs->nama_input_1 !!} && size_{!! $inputs->nama_input_1 !!} < 2097152) {
								$('#errSuratBertugasPbj').html("");
								setToTrue(val_{!! $inputs->nama_input_1 !!});
							}
						} else if (type_{!! $inputs->nama_input_1 !!} == 'image/jpeg') {
							if(size_{!! $inputs->nama_input_1 !!} > 2097152){
								$('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh lebih dari 2MB");
								setToFalse(val_{!! $inputs->nama_input_1 !!});
							}
							
							if(size_{!! $inputs->nama_input_1 !!} < 102796){
								$('#err{!! $inputs->nama_input_1 !!}').html("file tidak boleh kurang dari 100kb");
								setToFalse(val_{!! $inputs->nama_input_1 !!});
							}

							if (102796 < size_{!! $inputs->nama_input_1 !!} && size_{!! $inputs->nama_input_1 !!} < 2097152) {
								$('#err{!! $inputs->nama_input_1 !!}').html("");
								setToTrue(val_{!! $inputs->nama_input_1 !!});
							}
						} else {
							$('#err{!! $inputs->nama_input_1 !!}').html("type file tidak boleh selain jpg,jpeg & pdf");
							setToFalse(val_{!! $inputs->nama_input_1 !!});
						}
					@endif
					@php
						$cek = '';
						$jum_input = count($input_form);
						$cek .= $inputs->nama_input_1.' == true';
						
						if ($num != count($input_form)) {
							$cek .= ' && ';
						}
					@endphp
				@endforeach
			@endif
		@endforeach
		e.preventDefault();
		if (val_{{ $inputs->nama_input_1 }} == false){
			alert('Terdapat kesalahan/kekurangan pada berkas yang diunggah.\nSilakan periksa kembali berkas yang diunggah.');
		} else {
			$('#pesertaForm_{{ $poins->id }}').submit();
		}
	});
@endforeach
	window.pressed = function(){
		var a = document.getElementByClass('customFile1');
		if(a.value == ""){
			fileLabel.innerHTML = "Tidak ada dokumen dipilih ";
		} else {
			var theSplit = a.value.split('\\');
			fileLabel.innerHTML = theSplit[theSplit.length-1];
		}
	};
	
	$(document).ready(function() {
		//Fixing jQuery Click Events for the iPad
		var ua = navigator.userAgent,
		event = (ua.match(/iPad/i)) ? "touchstart" : "click";
		if ($('.table').length > 0) {
			$('.table .header').on(event, function() {
				$(this).toggleClass("active", "").nextUntil('.header').css('display', function(i, v) {
					return this.style.display === 'table-row' ? 'none' : 'table-row';
				});
			});
		}
	});

	console.log('input change');
	@foreach ($judul_input as $key => $poins)
		@foreach ($detail_input as $details)
			@if ($details->id_judul_input == $poins->id)
				@foreach ($input_form as $num => $inputs)
					@if ($inputs->id_detail_input == $details->id)
						$('#cek_file_{!! $inputs->nama_input_1 !!}').bind('change', function() {
							var size_{!! $inputs->nama_input_1 !!} = this.files[0].size;
							var type_{!! $inputs->nama_input_1 !!} = this.files[0].type;
							
							if (type_{!! $inputs->nama_input_1 !!} == 'application/pdf') {
								if (size_{!! $inputs->nama_input_1 !!} > 2097152){
									$('#err{!! $inputs->nama_input_1 !!}').html("Ukuran dokumen lebih dari 2MB");
									setToFalse(val_{!! $inputs->nama_input_1 !!});
								}
								
								if (size_{!! $inputs->nama_input_1 !!} < 102796){
									$('#err{!! $inputs->nama_input_1 !!}').html("Ukuran dokumen kurang dari 100KB");
									setToFalse(val_{!! $inputs->nama_input_1 !!});
								}
								
								if (102796 < size_{!! $inputs->nama_input_1 !!} && size_{!! $inputs->nama_input_1 !!} < 2097152) {
									$('#errSuratBertugasPbj').html("");
									setToTrue(val_{!! $inputs->nama_input_1 !!});
								}
							} else if (type_{!! $inputs->nama_input_1 !!} == 'image/jpeg') {
								if(size_{!! $inputs->nama_input_1 !!} > 2097152){
									$('#err{!! $inputs->nama_input_1 !!}').html("Ukuran dokumen lebih dari 2MB");
									setToFalse(val_{!! $inputs->nama_input_1 !!});
								}
								
								if(size_{!! $inputs->nama_input_1 !!} < 102796){
									$('#err{!! $inputs->nama_input_1 !!}').html("Ukuran dokumen kurang dari 100KB");
									setToFalse(val_{!! $inputs->nama_input_1 !!});
								}
								
								if (102796 < size_{!! $inputs->nama_input_1 !!} && size_{!! $inputs->nama_input_1 !!} < 2097152) {
									$('#err{!! $inputs->nama_input_1 !!}').html("");
									setToTrue(val_{!! $inputs->nama_input_1 !!});
								}
							} else {
								$('#err{!! $inputs->nama_input_1 !!}').html("type file tidak boleh selain jpg, jpeg & pdf");
								setToFalse(val_{!! $inputs->nama_input_1 !!});
							}
						});
						
						@php
							$cek = '';
							$jum_input = count($input_form);
							$cek .= 'val_'.$inputs->nama_input_1.' == true';
							
							if ($num != count($input_form)) {
								$cek .= ' && ';
							}
						@endphp
					@endif
				@endforeach
			@endif
		@endforeach
	@endforeach

	@foreach ($judul_input as $key => $poins)
		@foreach ($detail_input as $details)
			@if ($details->id_judul_input == $poins->id)
				@foreach ($input_form as $num => $inputs)
					@if ($inputs->id_detail_input == $details->id)
						$('#cek_file_{!! $inputs->nama_input_1 !!}').change(function(e){
							var fileName = e.target.files[0].name;
							$('#uploadFile_{!! $inputs->nama_input_1 !!}').val(fileName);
						});
					@endif
				@endforeach
			@endif
		@endforeach
	@endforeach
	
	$(document).ready(function(){
		@foreach ($judul_input as $key => $poins)
			@foreach ($detail_input as $details)
				@if ($details->id_judul_input == $poins->id)
					@foreach ($input_form as $num => $inputs)
						@if ($inputs->id_detail_input == $details->id)
							@if ($dokumen != "" )
								@foreach ($dokumen as $dokumens)
									@if ($inputs->id == $dokumens->id_detail_file_input)
										@if ($inputs->title_1 == "Dokumen Hasil Pekerjaan")
											@if ($dokumens->file == "")
												$('#btnSubmit_{{ $poins->id }}').attr("disabled",true);                    
											@else
												$('#btnSubmit_{{ $poins->id }}').attr("disabled",true);                    
											@endif
										@elseif($inputs->title_1 == "SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi")
											@if ($dokumens->file == "")
												$('#btnSubmit_{{ $poins->id }}').attr("disabled",true);                    
											@else
												$('#btnSubmit_{{ $poins->id }}').attr("disabled",true);                    
											@endif
										@endif
									@else
									$('#btnSubmit_{{ $poins->id }}').attr("disabled",true);
									@endif
								@endforeach
							@else
								$('#btnSubmit_{{ $poins->id }}').attr("disabled",true);                    
							@endif
							
							@if($poins->nama == "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan")
								$('#btnSubmit_{{ $poins->id }}').attr("disabled",false);
							@endif
						@endif
					@endforeach
				@endif
			@endforeach
		@endforeach
	});

	@foreach ($judul_input as $key => $poins)
		@foreach ($detail_input as $details)
			@if ($details->id_judul_input == $poins->id)
				@foreach ($input_form as $num => $inputs)
					@if ($inputs->id_detail_input == $details->id)
						
							$('#cek_file_{!! $inputs->nama_input_1 !!}').change(function(e){
								
								@if ($inputs->title_1 == "Dokumen Hasil Pekerjaan")
								var nama_input_{{$inputs->id-1}} =  'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi';

								$('#cek_tes_{{$inputs->id}}').val(1);
								if (nama_input_{{$inputs->id-1}} == "SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi"){
									
									if ($('#cek_tes_{{$inputs->id-1}}').val() == 1) {
										$('#btnSubmit_{{ $poins->id }}').attr("disabled",false);	
									}
								}
								
								@elseif($inputs->title_1 == "SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi")

								var nama_input_{{$inputs->id+1}} =  'Dokumen Hasil Pekerjaan';

								$('#cek_tes_{{$inputs->id}}').val(1);

								if (nama_input_{{$inputs->id+1}} == "Dokumen Hasil Pekerjaan"){
									
									if ($('#cek_tes_{{$inputs->id+1}}').val() == 1) {
										$('#btnSubmit_{{ $poins->id }}').attr("disabled",false);	
									}
								}
								

								@endif								
								
							});
					@endif
				@endforeach
			@endif
		@endforeach
	@endforeach
	
	@foreach ($judul_input as $key => $poins)
		@foreach ($detail_input as $details)
			@if ($details->id_judul_input == $poins->id)
				@foreach ($input_form as $num => $inputs)
					@if ($inputs->id_detail_input == $details->id)
						$(function() {
							$("#hapus{{ $inputs->id}}").click(function(){
								event.preventDefault();
								$.ajaxSetup({
									headers: {
										'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
									}
								});
							
								var id = {{ $inputs->id}};
								var id_jadwal = $("#val_jadwal").val();
								var id_peserta = $("#val_peserta").val();
								jQuery.ajax({
									url: "../../hapus-portofolio-dokumen/" + id + "/" + id_peserta + "/" + id_jadwal,
									type: 'get',
									success: function (result){
										if (result.msg == 'berhasil_hapus_porto') {
											alert('Berhasil Hapus!');
											location.reload();
										} else {
											alert('Gagal Hapus!');
											location.reload();
										}
									}
								});
							});
						});
					@endif
				@endforeach
			@endif
		@endforeach
	@endforeach
	
	@php
		$jumlah_poin = 0;
		$jumlah_upload = 0;
	@endphp
	
	@foreach ($judul_input as $key => $poins)
		@php
			foreach($detail_input as $details){
				if($details->id_judul_input == $poins->id){
					$jumlah_dokumen = 0;
					$jumlah_poin++;
					foreach ($input_form as $inputs){
						if($inputs->id_detail_input == $details->id){
							if($dokumen != ""){
								foreach($dokumen as $dokumens){
									if($inputs->id == $dokumens->id_detail_file_input){
										$jumlah_dokumen++;
									}
								}
							}
						}
					}
				
					if ($jumlah_dokumen == 2) {
						$jumlah_upload++;
					} 
				}
			}
		@endphp
	
		$("#selesai").click(function(){
			var min_komponen = '{{ $poins->min_komponen }}';
			var jumlah_upload = '{{ $jumlah_upload }}';  
			var nama = '{{ $poins->nama }}';
			var kurangdari = jumlah_upload < min_komponen;
			var sesuai = jumlah_upload >= min_komponen;
			
			if (kurangdari){
				alert('Dokumen ('+nama+') yang anda unggah kurang dari minimal komponen');
			} else if(sesuai && jumlah_upload == 0){
				if(kurangdari == ""){
					location.href = "{{ url('tambah-peserta-ujian/'.$jadwal->id) }}";
				}
			} else if(jumlah_upload >= min_komponen && nama != "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {
				if (kurangdari == ""){
					location.href = "{{ url('tambah-peserta-ujian/'.$jadwal->id) }}";
				}
			}
        
			@php
				$jumlah_upload = 0;
			@endphp
		});
	@endforeach
</script> 
@endsection