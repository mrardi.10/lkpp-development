@extends('layout.app')

@section('title')
Data Verifikator
@stop

@section('css')
<style>
.btn-tbh{
	text-align: right;
}

.btn-jadwal{
	width: 120px;
	background: #E8382A;
	color: #fff;
	font-weight: 600;
}

.btn-jadwal:hover{
	color: aliceblue;
}
</style>
@stop

@section('content')
@if (session('msg'))
			@if (session('msg') == "berhasil")
			<div class="row">
				<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>Berhasil simpan data</strong>
						</div>
				</div>
			</div>
			@endif 
			@if (session('msg') == "gagal")
			<div class="row">
					<div class="col-md-12">
							<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Gagal simpan data</strong>
								</div>
					</div>
				</div> 
			@endif
			@if (session('msg') == "berhasil_update")
			<div class="row">
				<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>Berhasil ubah data</strong>
						</div>
				</div>
			</div>
			@endif 
			@if (session('msg') == "gagal_update")
			<div class="row">
					<div class="col-md-12">
							<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Gagal ubah data</strong>
								</div>
					</div>
				</div> 
			@endif
			@if (session('msg') == "berhasil_hapus")
			<div class="row">
				<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<strong>Berhasil hapus data</strong>
						</div>
				</div>
			</div>
			@endif 
			@if (session('msg') == "gagal_hapus")
			<div class="row">
					<div class="col-md-12">
							<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Gagal hapus data</strong>
								</div>
					</div>
				</div> 
			@endif
	@endif
<div class="main-box">
		<div class="min-top">
			<div class="row">
				<div class="col-md-1 text-center">
					<b>Perlihatkan</b>
				</div>
				<div class="col-md-2">
						<select name='length_change' id='length_change' class="form-control">
								<option value='50'>50</option>
								<option value='100'>100</option>
								<option value='150'>150</option>
								<option value='200'>200</option>
						</select>
				</div>
				<div class="col-md-4 col-12">
						<div class="input-group">
								<div class="input-group addon">
									<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
									<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
								</div>
						</div>
				</div>
				<div class="col-md-5 col-xm-12 btn-tbh">
						@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof')
						<a href="{{ url('tambah-verifikator') }}"><button class="btn btn-sm btn-jadwal">Tambah Verifikator</button></a>
						@endif
				</div>
			</div> 
		</div>
		<div class="table-responsive">
			<table id="example1" class="table table-bordered table-striped">
					<thead>
					<tr>
						<th width="5%">No.</th>
						<th width="20%">Nama</th>
						<th width="15%">Email</th>
						<th width="10%">Status</th>
						<th width="15%">Jumlah Assign sebagai Verifikator</th> 
						<th width="15%">Jumlah Peserta (Menunggu Verifikasi Dok. Persyaratan)</th> 
						<th width="15%">Jumlah Peserta (Dok. Persyaratan tidak lengkap)</th> 
						<th width="5%">Aksi</th>
					</tr>
					</thead>
					<tbody>
						@php
						$jumlah_menunggu = 0;
						$jumlah_tidak_lengkap = 0;
						@endphp
					@foreach ($data as $key => $datas)
					<tr>
						<td>{{ $key++ + 1 }}</td>
						<td>{{ $datas->name }}</td>
						<td>{{ $datas->email }}</td>
						<td>{{ $datas->status_admin == 'aktif' ? 'Aktif' : 'Tidak Aktif' }}</td>
						<td>{{ $datas->jumlah_assign }}</td> 
						{{-- <td>{{ $jumlah_menunggu }}</td>  --}}
						{{-- @foreach($data3 as $data_3)
						@if($datas->id == $data_3->id)
						<td>{{ $data_3->jumlah_menunggu }}</td> 
						@else
						<td>{{ '0' }}</td> 
						@endif
						@endforeach --}}
						@if(isset($menunggu[$datas->id]))
						<td>{{ $menunggu[$datas->id]}}</td>
						@else
						<td>0</td>
						@endif
						@if(isset($tidak_lengkap[$datas->id]))
						<td>{{ $tidak_lengkap[$datas->id]}}</td>
						@else
						<td>0</td>
						@endif

						<td>
								<div class="dropdown">
										<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button">...</button>
										<ul class="dropdown-menu">
												@if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'bangprof')
												<li><a href="{{ url('detail-verifikator/'.$datas->id) }}">Lihat Detail</a></li>
												<li><a href="{{ url('ubah-verifikator/'.$datas->id) }}">Ubah</a></li>
												<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $datas->id }}">Hapus</a></li>
												@endif
										</ul>
									</div>	
						</td>
					</tr>
					<div class="modal fade" id="modal-default{{ $datas->id }}">
				        <div class="modal-dialog" style="width:30%">
			                <div class="modal-content">
			                    <div class="modal-header">
			                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                            <span aria-hidden="true">&times;</span></button>
			                        <h4 class="modal-title">Hapus Verifikator</h4>
			                    </div>
			                    <div class="modal-body">
			                        <p>Apakah Anda yakin menghapus Data Verifikator?</p>
			                    </div>
			                    <div class="modal-footer">
			                        <a href="{{ url('hapus-verifikator/'.$datas->id) }}" type="button" class="btn btn-primary pull-left">HAPUS</a>
			                        <button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
			                    </div>
			                </div>
				        </div>
				    </div>
					@endforeach	
					</tbody>
			</table>
		</div>
</div>
@stop

@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop