@extends('layout.app')
@section('title')
    Tambah Running Text
@endsection
@section('css')
<style type="text/css">
	.form-control
	{
		border-radius: 5px;
		height: 27px;
		padding: 0px;
		padding-left: 10px;
	}

	.col-md-7{
		margin-bottom: 10px;
	}

	.content-wrapper{
		min-height: 718.109px !important;
	}

	.btn-area{
		text-align: right;
	}
</style>
@stop
@section('content')
<div class="col-md-12">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title" style="padding-top: 10px;">Tambah Running Text</h3><hr>
		</div>
		<form action="" method="post" autocomplete="off">
		@csrf
			<div class="box-body">
				<div class="how">
					<div class="col-md-4">
						<label>Text</label>
					</div>
					<div class="col-md-1">
						<label>:</label>
					</div>
					<div class="col-md-6">
						<div class="input-group date">
							<textarea name="text" cols="100" rows="5" class="form-control">{{ old('text') }}</textarea>  
							<span class="errmsg">{{ $errors->first('text') }}</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 btn-area">
						<a href="{{ url('text-running')}}"><button class="btn btn-sm btn-default2">Kembali</button></a>
						<button class="btn btn-sm btn-default1" type="submit">Submit</button>
					</div>
				</div>        
			</div>
		</form>
	</div>
</div>
@endsection