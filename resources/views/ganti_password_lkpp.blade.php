@extends('layout.app')
@section('title')
	Ubah Password Admin PPK
@endsection
@section('css')
<style>
    .main-box{
        font-weight: 600;
        font-size: medium;
        padding: 20px;
    }

    .form-pjg{
        width: 50% !important;
    }

    .publish{
        width: 20px;
        height: 20px;
        border: 2px solid black;
        padding: 5px;
    }
    
	select.selectd{
        margin-bottom: 5px;
    }
</style>
@endsection
@section('content')
<form action="" method="post">
@csrf
	<div class="main-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>Ganti Password</h3><hr>
				</div>
			</div>
			<div class="row">
				<input type="hidden" name="id" value="{{ $id }}">
				<div class="col-md-3 col-xs-10">
					Password Baru
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='password' class="form-control" name="password" value="{{ old('password') }}" id="password" required/>
						<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword()"></span>
					</div>
					<span class="errmsg">{{ $errors->first('password') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-xs-10">
					Ulangi Password Baru
				</div>
				<div class="col-md-1 col-xs-1">:</div>
				<div class="col-md-5 col-xs-12">
					<div class="form-group">
						<input type='password' class="form-control" name="ulang_password" value="{{ old('ulang_password') }}" id="u_password" required/>
						<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword2()"></span>
					</div>
					<span class="errmsg">{{ $errors->first('ulang_password') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9" style="text-align: right">
					<button type="reset" class="btn btn-sm btn-default2" onclick="window.history.go(-1); return false;">Batal</button>
					<button type="submit" class="btn btn-sm btn-default1">Ubah</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection
@section('js')
<script type="text/javascript">
	function showPassword() {
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}

	function showPassword2() {
		var x = document.getElementById("u_password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
</script>
@endsection