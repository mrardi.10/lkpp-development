@extends('layout.app2')
@section('title')
	Ubah Formasi
@endsection
@section('css')
<style>
	body{
		background-color: whitesmoke;
	}

	.main-page{
		margin-top: 20px;
	}

	.box-container{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		background-color: white;
		border-radius: 5px;
		padding: 3%;
	}

	.shadow{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		max-width: 250px;
		line-height: 15px;
		width: 100%;
		margin: 5px;
	}

	div.dataTables_wrapper div.dataTables_info{
		display: none;
	}

	div.dataTables_wrapper .row.col-sm-12{
		width: 120px;
	}

	p{
		font-weight: 500;
	}

	b{
		font-weight: 500;
	}

	.row a.btn{
		text-align: left;
		font-weight: 600;
		font-size: small;
	}

	.form-control{
		margin :5px;
		-webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		-moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		border-radius: 5px;
	}

	textarea{
		margin :5px;
		-webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		-moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		border-radius: 5px;
	}

	.form-control{
		height: 30px;
	}

	.btn-default1{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
		color: white;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default2{
		background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
		color: black;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.custom-file label.custom-file-label{
		margin: 5px;
		-webkit-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		-moz-box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
		box-shadow: 0px 1px 10px -2px rgba(0,0,0,0.3);
	}

	.row-input{
		padding-bottom: 10px;
	}

	.box-container img{
		width: 60px;
	}

	span.berkas-lama{
		margin: 6px;
	}
    .ket_lam{
    font-size: 11px;

  }
</style>
@endsection
@section('content')
<div class="main-page">
	<div class="container">
		<div class="row box-container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-9" style="">
						<form action="" method="post" enctype="multipart/form-data" id="pesertaForm">
						@csrf
							<div class="row">
								<div class="col-md-12">
									<h5>Ubah Formasi</h5><hr>
									@if (session('msg'))
										@if (session('msg') == "berhasil")
											<div class="alert alert-success alert-dismissible">
												<button type="button" class="close" data-dismiss="alert">&times;</button>
												<strong>Berhasil Simpan Data</strong>
											</div> 
										@else
											<div class="alert alert-warning alert-dismissible">
												<button type="button" class="close" data-dismiss="alert">&times;</button>
												<strong>Gagal Simpan Data</strong>
											</div> 
										@endif
									@endif
								</div>
                <form action="" method="post" enctype="multipart/form-data">
                <div class="col-md-11">
                  <div style="text-align: justify;">
                      <p>Kolom usulan per jenjang diisi sesuai dengan angka pada kolom "Usul Inpassing" di website e-formasi KemenPAN-RB.
                        <br>
                      Mengacu pada SE MenPAN-RB No: B/528/M.SM.01.00/2018 tanggal 15 Oktober 2018, Perhitungan Beban Kerja JF PPBJ sesuai Perka LKPP 14/2013 agar  disampaikan ke Direktur Pengembangan Profesi dan Kelembagaan LKPP melalui email dit.bangprof@lkpp.go.id untuk divalidasi dan diterbitkan Rekomendasi. </p>
                      <p>Data yang dapat diubah hanya data yang statusnya Belum Disetujui.<br>Lampirkan dokumen yang terbaru jika ada perubahan data.</p> 
                    </div>
                     
                    <div class="alert alert-primary" role="alert">
                      Dokumen yang diunggah min. 100KB max. 2MB, dengan format PDF, JPG, JPEG
                    </div>
                    <div class="row row-input">
                      <div class="col-lg-4 col-md-11 col-10">Pertama</div>
                      <div class="col-lg-1 col-md-1 col-1">:</div>
                      <div class="col-lg-7 col-md-12"><input type="text" style="width: 100%" class="form-control pertama" name="pertama" value="{{ $data->pertama }}" oninput="if(value.length>18)value=value.slice(0,18)" pattern="[0-9]" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" {{ $data->status_pertama == 'setuju' ? 'readonly' : '' }}>
                      <span class="errmsg">{{ $errors->first('pertama') }}</span>
                      <span class="errmsg" id="errpertama"></span>
                      </div>
                    </div>
                    <div class="row row-input">
                      <div class="col-lg-4 col-md-11 col-10">Muda</div>
                      <div class="col-lg-1 col-md-1 col-1">:</div>
                      <div class="col-lg-7 col-md-12"><input type="text" style="width: 100%" class="form-control muda" name="muda" value="{{ $data->muda }}" oninput="if(value.length>18)value=value.slice(0,18)" pattern="[0-9]" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" {{ $data->status_muda == 'setuju' ? 'readonly' : '' }}>
                      <span class="errmsg">{{ $errors->first('muda') }}</span>
                      <span class="errmsg" id="errmuda"></span>
                      </div>
                    </div>
                    <div class="row row-input">
                      <div class="col-lg-4 col-md-11 col-10">Madya</div>
                      <div class="col-lg-1 col-md-1 col-1">:</div>
                      <div class="col-lg-7 col-md-12"><input type="text" style="width: 100%" class="form-control madya" name="madya" value="{{ $data->madya }}" oninput="if(value.length>18)value=value.slice(0,18)" pattern="[0-9]" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" {{ $data->status_madya == 'setuju' ? 'readonly' : '' }}>
                      <span class="errmsg">{{ $errors->first('madya') }}</span>
                      <span class="errmsg" id="errmadya"></span>
                      </div>
                    </div>
                    <div class="row row-input">
                      <div class="col-lg-4 col-md-11 col-10">Dokumen Hasil Penyusunan Kebutuhan <br> <span class="ket_lam">(sesuai dengan Lampiran III No.1 Peraturan LKPP 4/2019)</span></div>
                      <div class="col-lg-1 col-md-1 col-1">:</div>
                      <div class="col-lg-7 col-md-12">
                          <div class="custom-file">
                              <input type="file" class="custom-file-input dokumen_hasil_perhitungan_abk" id="customFile" name="dokumen_hasil_perhitungan_abk" accept="image/jpg, application/pdf, image/jpeg, .jpeg, .pdf, .jpg" value="{{ old('dokumen_hasil_perhitungan_abk') }}">
                              <input type="hidden" name="old_dokumen_hasil_perhitungan_abk" value="{{ $data->dokumen_hasil_perhitungan_abk }}">
                              <label class="custom-file-label" for="customFile">{{ old('dokumen_hasil_perhitungan_abk') == "" ? 'Pilih Berkas' : old('dokumen_hasil_perhitungan_abk')}}</label>
                              <span class="errmsg">{{ $errors->first('dokumen_hasil_perhitungan_abk') }}</span>
                              <span class="errmsg errcek_dokumen_hasil_perhitungan_abk"></span>
                            </div>
                            <span class="berkas-lama">
                            Berkas Lama:
                            @if (is_null($data->dokumen_hasil_perhitungan_abk) || $data->dokumen_hasil_perhitungan_abk == "-")
                                -
                            @else
                                @php
                                $dokumen_hasil_perhitungan_abk = explode('.',$data->dokumen_hasil_perhitungan_abk);
                                @endphp
                                @if ($dokumen_hasil_perhitungan_abk[1] == 'pdf' || $dokumen_hasil_perhitungan_abk[1] == 'PDF')
                                <a href="{{ url('priview-file')."/dokumen_hasil_perhitungan_abk/".$data->dokumen_hasil_perhitungan_abk }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                @else
                                <a href="{{ url('priview-file')."/dokumen_hasil_perhitungan_abk/".$data->dokumen_hasil_perhitungan_abk }}" target="_blank"><img src="{{ asset('storage/data/dokumen_hasil_perhitungan_abk')."/".$data->dokumen_hasil_perhitungan_abk }}" class="img-rounded"></a>
                                @endif 
                            @endif
                            </span>
                      </div>
                    </div>
                    <div class="row row-input">
                      <div class="col-lg-4 col-md-11 col-10"> Surat Permohonan Mengikuti Penyesuaian/Inpassing <br> <span class="ket_lam">(sesuai dengan Lampiran III No.2 Peraturan LKPP 4/2019)</span>
                        </div>
                      <div class="col-lg-1 col-md-1 col-1">:</div>
                      <div class="col-lg-7 col-md-12">
                          <div class="custom-file">
                              <input type="file" class="custom-file-input surat_usulan" id="customFile" name="surat_usulan" accept="image/jpg, application/pdf, image/jpeg, .jpeg, .pdf, .jpg" value="c:/text.jpg">
                              <input type="hidden" name="old_surat_usulan" value="{{ $data->surat_usulan_rekomendasi }}">
                              <label class="custom-file-label" for="customFile">{{ old('surat_usulan') == "" ? 'Pilih Berkas' : old('surat_usulan')}}</label>
                              <span class="errmsg">{{ $errors->first('surat_usulan') }}</span>
                              <span class="errmsg errcek_surat_usulan"></span>
                          </div>
                          <span class="berkas-lama">
                            Berkas Lama:
                            @if (is_null($data->surat_usulan_rekomendasi)|| $data->surat_usulan_rekomendasi == "-" )
                                -
                            @else
                                @php
                                $surat_usulan = explode('.',$data->surat_usulan_rekomendasi);
                                @endphp
                                @if ($surat_usulan[1] == 'pdf' || $surat_usulan[1] == 'PDF')
                                <a href="{{ url('priview-file')."/surat_usulan/".$data->surat_usulan_rekomendasi }}" target="_blank"><label><i class="far fa-file-pdf" style="font-size:15px" data-toggle="tooltip" title="klik untuk melihat dokumen "></i></label></a>
                                @else
                                <a href="{{ url('priview-file')."/surat_usulan/".$data->surat_usulan_rekomendasi }}" target="_blank"><img src="{{ asset('storage/data/surat_usulan')."/".$data->surat_usulan_rekomendasi }}" class="img-rounded"></a>
                                @endif 
                            @endif
                            </span>
                      </div>
                    </div>
                    <div class="row row-input">
                        <div class="col-lg-4 col-md-11 col-10">No. Surat Permohonan Mengikuti Penyesuaian/Inpassing</div>
                        <div class="col-lg-1 col-md-1 col-1">:</div>
                        @php
                            $read_surat = '';
                            // if(in_array('setuju', [$data->status_pertama,$data->status_muda,$data->status_madya])){
                            // $read_surat = 'readonly';  
                            // }
                        @endphp
                        <div class="col-lg-7 col-md-12"><input type="text" name="no_surat" style="width: 100%" class="form-control no_surat"  {{ $read_surat }}>
                        <span>*No. Surat Lama: {{ $data->no_surat_usulan_rekomendasi }}</span>
                        <span class="errmsg">{{ $errors->first('no_surat') }}</span>
                        <span class="errmsg" id="errno_surat"></span>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-11" style="text-align : right">
                    <button type="reset" class="btn btn-default2" onclick="window.history.go(-1); return false;">Batal</button> <button type="button" class="btn btn-default1" id="btnSubmit">Simpan</button>
                  </div>
                </div>
                </form>
              </div>
              <div class="col-md-3 text-center">
                @include('layout.button_right_kpp')
              </div>
            </form>
            </div>
          </div> 
        </div>
    </div>
  </div>
  @endsection

  @section('js')
  <script>
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });    
  </script>
  <script>document.foo.submit();</script>
  <script>
      document.addEventListener("DOMContentLoaded", function() {
          var elements = document.getElementsByTagName("INPUT");
          for (var i = 0; i < elements.length; i++) {
              elements[i].oninvalid = function(e) {
                  e.target.setCustomValidity("");
                  if (!e.target.validity.valid) {
                      e.target.setCustomValidity("Silakan isi kolom berikut.");
                  }
              };
              elements[i].oninput = function(e) {
                  e.target.setCustomValidity("");
              };
          }
      })
  </script>
  <script>
    function modifyVar(obj, val) {
          obj.valueOf = obj.toSource = obj.toString = function(){ return val; };
      }
  
      function setToFalse(boolVar) {
          modifyVar(boolVar, false);
      }
  
      function setToTrue(boolVar) {
          modifyVar(boolVar, true);
      }
    
    var muda = new Boolean(false);
    var madya = new Boolean(false);
    var pertama = new Boolean(false);
    var no_surat = new Boolean(false);
    var cek_dokumen_hasil_perhitungan_abk = new Boolean(false);
    var cek_surat_usulan = new Boolean(false);
    var dokumen_hasil_perhitungan_abk = new Boolean(true);
    var surat_usulan = new Boolean(true);
  
    $('.dokumen_hasil_perhitungan_abk').bind('change', function() {
          var size_dokumen_hasil_perhitungan_abk = this.files[0].size;
          if(size_dokumen_hasil_perhitungan_abk > 2097152){
              $('.errdokumen_hasil_perhitungan_abk').html("file tidak boleh lebih dari 2MB");
              setToFalse(dokumen_hasil_perhitungan_abk);
          }
  
          if(size_dokumen_hasil_perhitungan_abk < 102796){
              $('.errdokumen_hasil_perhitungan_abk').html("file tidak boleh kurang dari 100kb");
              setToFalse(dokumen_hasil_perhitungan_abk);
          }
  
          if (102796 < size_dokumen_hasil_perhitungan_abk && size_dokumen_hasil_perhitungan_abk < 2097152) {
              $('.errdokumen_hasil_perhitungan_abk').html("");
              setToTrue(dokumen_hasil_perhitungan_abk);
          }
      });
  
      $('.surat_usulan').bind('change', function() {
          var size_surat_usulan = this.files[0].size;
          if(size_surat_usulan > 2097152){
              $('.errsurat_usulan').html("file tidak boleh lebih dari 2MB");
              setToFalse(surat_usulan);
          }
  
          if(size_surat_usulan < 102796){
              $('.errsurat_usulan').html("file tidak boleh kurang dari 100kb");
              setToFalse(surat_usulan);
          }
  
          if (102796 < size_surat_usulan && size_surat_usulan < 2097152) {
              $('.errsurat_usulan').html("");
              setToTrue(surat_usulan);
          }
      });
  
      $('#btnSubmit').click(function (e) {
          e.preventDefault();
          if ($('.muda').val().length == 0) {
              $('#errmuda').html("Jumlah Muda Tidak Boleh Kosong");
              setToFalse(muda); 
          }else{
              $('#errmuda').html("");
              setToTrue(muda);
          }
  
          if ($('.madya').val().length == 0) {
              $('#errmadya').html("Jumlah Madya Tidak Boleh Kosong");
              setToFalse(madya); 
          }else{
              $('#errmadya').html("");
              setToTrue(madya);
          }
  
          if ($('.pertama').val().length == 0) {
              $('#errpertama').html("Jumlah Pertama Tidak Boleh Kosong");
              setToFalse(pertama); 
          }else{
              $('#errpertama').html("");
              setToTrue(pertama);
          }
  
          if ($('.no_surat').val().length == 0) {
              $('#errno_surat').html("No Surat tidak boleh kosong ");
              setToFalse(no_surat); 
          }else{
              $('#errno_surat').html("");
              setToTrue(no_surat);
          }
  
          if ($('.dokumen_hasil_perhitungan_abk').val().length == 0) {
              $('.errcek_dokumen_hasil_perhitungan_abk').html("Dokumen Hasil Perhitungan ABK wajib diisi");
              setToFalse(cek_dokumen_hasil_perhitungan_abk); 
          }else{
              $('.errcek_dokumen_hasil_perhitungan_abk').html("");
              setToTrue(cek_dokumen_hasil_perhitungan_abk);
          }
  
          if ($('.surat_usulan').val().length == 0) {
              $('.errcek_surat_usulan').html("Surat Usulan wajib diisi");
              setToFalse(cek_surat_usulan); 
          }else{
              $('.errcek_surat_usulan').html("");
              setToTrue(cek_surat_usulan);
          }
          
          console.log('dokumen_hasil_perhitungan_abk =' + dokumen_hasil_perhitungan_abk + '\n');
          console.log('surat_usulan =' + surat_usulan + '\n');
          if(cek_dokumen_hasil_perhitungan_abk == true && cek_surat_usulan == true && muda == true && madya == true && pertama == true && no_surat == true){
            if(dokumen_hasil_perhitungan_abk == true && surat_usulan == true){
              $('#pesertaForm').submit();
            }
          }else{
            if(dokumen_hasil_perhitungan_abk == false || surat_usulan == false){
              alert('upload file ada yang salah, silahkan cek kembali file yang anda upload agar bisa mengusulkan formasi');
            }
          }
      });
  </script>
  @endsection