@extends('layout.app2')
@section('title')
	Data Peserta
@endsection
@section('css')
<style>
	body{
		background-color: whitesmoke;
	}
	
	.main-page{
		margin-top: 20px;
	}

	.box-container{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		background-color: white;
		border-radius: 5px;
		padding: 3%;
	}

	.shadow{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		max-width: 250px;
		line-height: 15px;
		width: 100%;
		margin: 5px;
	}

	div.dataTables_wrapper div.dataTables_info{
		display: none;
	}

	div.dataTables_wrapper .row.col-sm-12{
		width: 120px;
	}

	p{
		font-weight: 500;
	}

	b{
		font-weight: 500;
	}

	.row a.btn{
		text-align: left;
		font-weight: 600;
		font-size: small;
	}

	.btn-default1{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
		color: white;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default2{
		background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
		color: black;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default3{
		background: #fff;
		color: #000;
		font-weight: 500;
		width: 100px;
	}

	.btn-default4{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
		color: #fff;
		font-weight: 600;
		width: 100px;
	}

	.row-input{
		padding-bottom: 10px;
	}

	.page div.verifikasi{
		display: none;
	}

	.btn-area{
		text-align: right;
	}
</style>
@endsection
@section('content')
<div class="main-page">
	<div class="container">
		<div class="row box-container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12" style="">
						<div class="row">
							<div class="col-md-10">
								<h5>Data Peserta</h5><hr>
								@if (session('msg'))
									@if (session('msg') == "berhasil")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil simpan data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "gagal")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal simpan data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "berhasil_update")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil ubah data</strong>
										</div>
									@endif
									
									@if (session('msg') == "gagal_update")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal ubah data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "berhasil_hapus")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil hapus data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "gagal_hapus")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal hapus data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "jenjang_penuh")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal tambah Peserta, Jenjang Peserta yang Anda pilih sudah memenuhi batas</strong>
										</div> 
									@endif
									
									@if (session('msg') == "berhasil_berkas_ulang")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil perbarui Berkas</strong>
										</div> 
									@endif
									
									@if (session('msg') == "gagal_berkas_ulang")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal perbarui Berkas</strong>
										</div> 
									@endif
								@endif
							</div>
							<div class="col-md-1" style="text-align:right">
								<div class="dropdown dropdown-notifications">
									<button class="btn btn-sm btn-info" data-toggle="dropdown" type="button"><i class="fa fa-bell"></i></button>
									<ul class="dropdown-menu dropdown-menu-right">
										<h6 class="dropdown-header">Notifikasi Peserta</h6>
										<div class="dropdown-divider"></div>
										@foreach ($notifikasi as $notif)
											<li class="dropdown-item">{{ 'Tanggal '.Helper::tanggal_indo($notif->tanggal).' '.$notif->nama_peserta.' '.$notif->description.' '.$notif->nama_admin }}</li>
										@endforeach
									</ul>
								</div>
							</div>
							<div class="col-md-1" style="text-align:right">
								<div class="dropdown dropdown-notifications">
									<button class="btn btn-sm btn-danger" data-toggle="dropdown" title="Rekomendasi Asessor" type="button"><i class="fa fa-bell"></i></button>
									<ul class="dropdown-menu dropdown-menu-right">
										<h6 class="dropdown-header">Notifikasi Rekomendasi Assesor</h6>
										<div class="dropdown-divider"></div>
										@foreach ($notifikasiRekomendasi as $notif)
									<li class="dropdown-item">{{ 'Tanggal '.Helper::tanggal_indo($notif->tanggal).' '.$notif->nama_peserta.' '.$notif->description }}</li>
										@endforeach
									</ul>
								</div>
							</div>
							<div class="col-md-9 page">
								<div class="main-box" id="view">
									<div class="min-top">
										<div class="row">
											<div class="col-md-2 text-center">
												<b>Perlihatkan</b>
											</div>
											<div class="col-md-2">
												<select name='length_change' id='length_change' class="form-control form-control-sm">
													<option value='50'>50</option>
													<option value='100'>100</option>
													<option value='150'>150</option>
													<option value='200'>200</option>
												</select>
											</div>
											<div class="col-md-4 col-12">
												<div class="form-group" style="margin-bottom:0px !important">
													<div class="input-group input-group-sm">
														<div class="input-group-prepend">
															<span class="input-group-text"><i class="fa fa-search"></i></span>
														</div>
														<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
													</div>
												</div>
											</div>
											<div class="col-md-4 col-12" style="text-align:right">
												<button class="btn btn-sm btn-default4" id="btn-tambah">Tambah</button>
											</div>
										</div> 
									</div>
									<div class="table-responsive">
										<table id="example1" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th class="no-sort">Nama</th>
													<th>NIP</th>
													<th>Pangkat/Gol.</th>
													<th>Jenjang</th>
													<th>Status</th>
													<th>Keikutsertaan Peserta</th>
													<th>Aksi</th>
												</tr>
											</thead>
											<tbody>
											@foreach ($peserta as $key => $pesertas)
												<tr>											
													<td>{{ $pesertas->nama }}</td>
													<td>{{ $pesertas->nip }}</td>
													<td>{{ $pesertas->jabatan }}</td>
													<td>{{ ucwords($pesertas->jenjang) }}</td>
													<td>{{ $pesertas->status }}</td>
													<td>{{ Helper::getStatusActive($pesertas->status_peserta)}}</td>
													<td>
														<div class="dropdown">
															<button class="btn btn-sm btn-default btn-action" data-toggle="dropdown" type="button"><i class="fas fa-ellipsis-h"></i></button>
															<ul class="dropdown-menu">
																<li><a class="dropdown-item" href="{{ url('detail-peserta/'.$pesertas->id) }}">Detail</a></li>
																<li><a class="dropdown-item" href="{{ url('edit-peserta/'.$pesertas->id) }}">Ubah</a></li>
																<li><a class="dropdown-item" href="{{ url('riwayat-user-ppk/'.$pesertas->id) }}">Riwayat</a></li>
																@if ($pesertas->status_inpassing == 2)
																	<li><a class="dropdown-item" href="{{ url('input-ulang-berkas/'.$pesertas->id) }}">Input Ulang Berkas</a></li>    
																@endif
															</ul>
														</div>
													</td>
												</tr>
											@endforeach
											</tbody>
										</table>
									</div>
								</div>
								<div class="verifikasi" id="verif">
									<div class="row">
										<div class="col-md-12">
											<p style="font-weight:600">Silakan mengisi data peserta yang diusulkan untuk mengikuti Penyesuaian/Inpassing dan pastikan dokumen di bawah ini telah tersedia dalam bentuk softcopy.</p>
											<p style="padding: 30px 60px;margin-top: -30px;">1. Surat Keterangan Bertugas di bidang PBJ min. 2 tahun<br>
												2. Bukti SK di bidang PBJ<br>
												3. Ijazah Terakhir (Min. S1/D4)<br>
												4. SK Kenaikan Pangkat Terakhir<br>
												5. SK Pengangkatan ke dalam Jabatan Terakhir<br>
												6. Sertifikat PBJ Tk. Dasar<br>
												7. Sasaran Kinerja Pegawai (2 Tahun Terakhir)<br>
												8. Surat keterangan Tidak Sedang Dijatuhi Hukuman<br>
												9. Formulir Kesediaan Mengikuti Penyesuaian/Inpassing<br>
												10. SK Pengangkatan CPNS<br>
												11. SK Pengangkatan PNS<br>
												12. KTP<br>
												13. Pas Foto Formal 3 X 4<br>
												14. Surat Pernyataan Bersedia Diangkat JF PPBJ<br>
												15. Surat Permohonan Mengikuti Penyesuaian/Inpassing<br>
												16. No. Surat Permohonan Mengikuti Penyesuaian/Inpassing
											</p>
											<div class="alert alert-primary" role="alert">
												<strong style="text-transform: uppercase;">Dokumen yang diunggah min. 100KB max. 2MB, dengan format PDF, JPG, JPEG</strong>
											</div>
											<div class="row"><hr></div>
										</div>
										<div class="col-md-12 btn-area">
											<button class="btn btn-sm btn-default2" id="btn-back" onclick="btnThreeBack()"><i class="fa fa-angle-left"></i> Kembali</button>
											<a href="{{ url('tambah-peserta-ppk') }}">
												<button class="btn btn-sm btn-default1">Paham <i class="fa fa-angle-right"></i></button>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3 text-center">
								@include('layout.button_right_kpp')
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
	</div>
</div>
@endsection
@section('js')
<script>
	$('#btn-tambah').click(function (e) {
		e.preventDefault();
		document.getElementById("verif").style.display = "block";
		document.getElementById("view").style.display = "none";
	});

	$('#btn-back').click(function (e) {
		e.preventDefault();
		document.getElementById("verif").style.display = "none";
		document.getElementById("view").style.display = "block";
	});
</script>  
@endsection