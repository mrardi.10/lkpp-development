@extends('layout.app')

@section('title')
    Edit Running Text
@endsection

@section('css')
<style type="text/css">
	.form-control
	{
		border-radius: 5px;
		height: 27px;
    padding: 0px;
    padding-left: 10px;
	}

  .col-md-7{
    margin-bottom: 10px;
  }

  .content-wrapper{
    min-height: 718.109px !important;
  }

  .btn-area{
    text-align: right;
  }
</style>
@stop

@section('content')
<div class="col-md-8">
        <div class="box">
                    <div class="box-header">
                      <h3 class="box-title" style="padding-top: 10px;">Edit Running Text</h3>
                      <hr>
                    </div>
                    <!-- /.box-header -->
                <form action="" method="post" autocomplete="off">
                  @csrf
                  <div class="box-body">
                        <div class="how">
                          <div class="col-md-4">
                          <label>Text</label>
                        </div>
                        <div class="col-md-1">
                          <label>:</label>
                        </div>
                        <div class="col-md-7">
                            <div class="input-group date">
                              <textarea name="text" cols="30" rows="5" class="form-control">{{ $data->text }}</textarea>  
                              <span>{{ $errors->first('text') }}</span>
                        </div>
                        </div>
                        </div>
                      <div class="row">
                        <div class="col-md-12 btn-area">
                            <button class="btn btn-sm btn-default2" id="btnTwoBack" onclick="btnTwoBack()">Batal</button>
                            <button class="btn btn-sm btn-default1" type="submit">Submit</button>
                        </div>
                      </div>
        
                        
                            
                  </div>
                </form>
                    </div>
                    
                    
                    <!-- /.box-body -->
                  </div>
        </div>    
@endsection