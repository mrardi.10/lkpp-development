@extends('layout.app2')
@section('title')
	Pengusulan Formasi
@endsection
@section('css')
<style>
	body{
		background-color: whitesmoke;
	}
	
	.main-page{
		margin-top: 20px;
	}

	.box-container{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.12);
		background-color: white;
		border-radius: 5px;
		padding: 3%;
	}

	.shadow{
		-webkit-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		-moz-box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.14) !important;
		max-width: 250px;
		line-height: 15px;
		width: 100%;
		margin: 5px;
	}

	div.dataTables_wrapper div.dataTables_info{
		display: none;
	}

	div.dataTables_wrapper .row.col-sm-12{
		width: 120px;
	}

	p{
		font-weight: 500;
	}

	b{
		font-weight: 500;
	}

	.row a.btn{
		text-align: left;
		font-weight: 600;
		font-size: small;
	}

	.btn-default1{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
		color: white;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default2{
		background-image: linear-gradient(to bottom, #e1dfdf, #dad8d9, #d2d1d2, #cbcbcb, #c4c4c4);
		color: black;
		font-weight: 600;
		width: 100px; 
		margin: 10px;
		-webkit-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		-moz-box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
		box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.24);
	}

	.btn-default3{
		background: #fff;
		color: #000;
		font-weight: 500;
		width: 100px;
	}

	.btn-default4{
		background-image: linear-gradient(to bottom, #ff0000, #f70101, #ee0101, #e60202, #de0202);
		color: #fff;
		font-weight: 600;
		width: 100px;
	}

	.row-input{
		padding-bottom: 10px;
	}
	
	.page div.verifikasi{
		display: none;
	}

	.btn-area{
		text-align: right;
    }
</style>
@endsection
@section('content')
<div class="main-page">
    <div class="container">
        <div class="row box-container">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12" style="">
						<div class="row">
							<div class="col-md-9">
								<h5>Rekapitulasi Usulan Formasi</h5><hr>
								@if (session('msg'))
									@if (session('msg') == "berhasil")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil simpan data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "gagal")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal simpan data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "berhasil_update")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil ubah data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "gagal_update")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal ubah data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "berhasil_ulang")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil mengajukan ulang formasi</strong>
										</div> 
									@endif
									
									@if (session('msg') == "gagal_ulang")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal mengajukan ulang formasi</strong>
										</div> 
									@endif
									
									@if (session('msg') == "berhasil_hapus")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil hapus data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "gagal_hapus")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal hapus data</strong>
										</div> 
									@endif
									
									@if (session('msg') == "tidak_setuju")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Usulan formasi Anda belum disetujui oleh Admin LKPP</strong>
										</div> 
									@endif
									
									@if (session('msg') == "berhasil_berkas")
										<div class="alert alert-success alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Berhasil Input Ulang Berkas</strong>
										</div> 
									@endif
									
									@if (session('msg') == "gagal_berkas")
										<div class="alert alert-warning alert-dismissible">
											<button type="button" class="close" data-dismiss="alert">&times;</button>
											<strong>Gagal Input Ulang Berkas</strong>
										</div> 
									@endif
								@endif
							</div>
							<div class="col-md-3" style="text-align:right">
								<div class="dropdown dropdown-notifications">
								<button class="btn btn-sm btn-info" data-toggle="dropdown" type="button"><i class="fa fa-bell"></i></button>
									<ul class="dropdown-menu dropdown-menu-right">
										<h6 class="dropdown-header">Notifikasi Eformasi</h6>
										<div class="dropdown-divider"></div>
										@foreach ($notifikasi as $notif)
											<li class="dropdown-item">{{ 'Tanggal '.Helper::tanggal_indo($notif->tanggal).' '.$notif->admin_name.' '.$notif->description }} Anda</li>
										@endforeach
									</ul>
								</div>
							</div>
							<div class="col-md-9 page">
								<div class="main-box" id="view">
									<div class="min-top">
										<div class="row">
											<div class="col-md-2 text-center">
												<b>Perlihatkan</b>
											</div>
											<div class="col-md-2">
												<select name='length_change' id='length_change' class="form-control form-control-sm">
													<option value='50'>50</option>
													<option value='100'>100</option>
													<option value='150'>150</option>
													<option value='200'>200</option>
												</select>
											</div>
											<div class="col-md-4 col-12">
												<div class="form-group" style="margin-bottom:0px !important">
													<div class="input-group input-group-sm">
														<div class="input-group-prepend">
															<span class="input-group-text"><i class="fa fa-search"></i></span>
														</div>
														<input type="text" class="form-control" id="myInputTextField" name="search" placeholder="Cari">
													</div>
												</div>
											</div>
											<div class="col-md-4 col-12" style="text-align:right">
											@if ($data1 != "")
												@if(in_array('tidak_setuju',[$data1->status_muda,$data1->status_madya,$data1->status_pertama]) || in_array('',[$data1->status_muda,$data1->status_madya,$data1->status_pertama]))
												@else
													<a href="{{ url('tambah-pengusulan-eformasi') }}"><button class="btn btn-sm btn-default4" id="btn-tambah">Tambah</button></a> 
												@endif
											@else
												<a href="{{ url('tambah-pengusulan-eformasi') }}"><button class="btn btn-sm btn-default4" id="btn-tambah">Tambah</button></a>
											@endif                                  
											</div>
										</div> 
									</div>
									<div class="table-responsive">
										<table id="example1" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th>No</th>
													<th>Jumlah JF PPBJ Pertama</th>
													<th>Status Usulan</th>
													<th>Jumlah JF PPBJ Muda</th>
													<th>Status Usulan</th>
													<th>Jumlah JF PPBJ Madya</th>
													<th>Status Usulan</th>
													<th>Aksi</th>
												</tr>
											</thead>
											<tbody>
											@foreach ($data as $key => $eformasis)
												<tr>
													<td>{{ $key++ + 1 }}</td>
													<td>{{ $eformasis->pertama }}</td>
													<td>
													@if (is_null($eformasis->status_pertama))
														-
													@else
														{{ Helper::getStatusEformasi($eformasis->status_pertama) }}
													@endif
													</td>
													<td>{{ $eformasis->muda }}</td>
													<td>
													@if (is_null($eformasis->status_muda))
														-
													@else
														{{ Helper::getStatusEformasi($eformasis->status_muda) }}
													@endif
													</td>
													<td>{{ $eformasis->madya }}</td>
													<td>
													@if (is_null($eformasis->status_madya))
														-
													@else
														{{ Helper::getStatusEformasi($eformasis->status_madya) }}
													@endif
													</td>
													<td>
														<div class="dropdown">
															<button class="btn btn-sm btn-default btn-action" data-toggle="dropdown" type="button"><i class="fas fa-ellipsis-h"></i></button>
															<ul class="dropdown-menu">
															@if (in_array('tidak_setuju', [$eformasis->status_pertama,$eformasis->status_muda,$eformasis->status_madya]))
																<li><a class="dropdown-item" href="{{ url('pengajuan-ulang-eformasi/'.$eformasis->id) }}">Pengajuan Ulang</a></li>
															@endif
															@if (in_array('', [$eformasis->status_pertama,$eformasis->status_muda,$eformasis->status_madya]))
																<li><a class="dropdown-item" href="{{ url('ubah-eformasi/'.$eformasis->id) }}">Ubah Eformasi</a></li>
															@endif
															@if($eformasis->dokumen_hasil_perhitungan_abk == '-' ||$eformasis->surat_usulan_rekomendasi == '-')
																<li><a class="dropdown-item" href="{{ url('berkas-ulang-eformasi/'.$eformasis->id) }}">Input Ulang Berkas</a></li>
															@endif
																<li><a class="dropdown-item" href="{{ url('detail-pengajuan-eformasi/'.$eformasis->id) }}">Lihat Detail</a></li>
															</ul>
														</div>
													</td>
												</tr>
											@endforeach
											</tbody>
										</table>
									</div>
								</div>                      
							</div>
							<div class="col-md-3 text-center">
								@include('layout.button_right_kpp')
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
	</div>
</div>
@endsection
@section('js')
@endsection