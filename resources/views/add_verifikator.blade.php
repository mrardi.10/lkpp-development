@extends('layout.app')
@section('title')

@if ($action == 'add')
	Tambah Verifikator
@elseif ($action == 'edit')
	Ubah Data Verifikator
@elseif ($action == 'detail')
	Detail Data Verifikator
@endif
@stop

@section('css')
<style type="text/css">
	.form-control
	{
		border-radius: 5px;
		height: 27px;
		padding: 0px;
		padding-left: 10px;
	}

	.bin{
		color: #dd4b39;
	}

	.row{
		margin: 0px 15px 15px 15px; 
	}

	.main-box{
		font-weight: 600;
	}
</style>
@stop
@section('content')
@if ($action == 'add')
	<form action="" method="post" enctype="multipart/form-data">
	@csrf
		<div class="main-box">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<h3>Tambah Verifikator</h3><hr>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">Nama</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="text" class="form-control" name="nama" value="{{ old('nama') }}">
					<span class="errmsg">{{ $errors->first('nama') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">Email</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="email" class="form-control" name="email" value="{{ old('email') }}">
					<span class="errmsg">{{ $errors->first('email') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">Password</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="password" class="form-control" name="password" value="{{ old('password') }}" id="password">
					<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword()"></span>
					<span class="errmsg">{{ $errors->first('password') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Ulangi Password
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="password" class="form-control" name="c_password" value="{{ old('c_password') }}" id="c_password">
					<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword2()"></span>
					<span class="errmsg">{{ $errors->first('c_password') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Status
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					{!! Form::select('status_admin', array('aktif' => 'Aktif', 'tidak_Aktif' => 'Tidak Aktif') ,old('status_admin'), ['placeholder' => 'Pilih Status', 'class' => 'form-control', 'id' => 'select-single']); !!}
					<span class="errmsg">{{ $errors->first('status_admin') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9" style="text-align:right">
					<button type="reset" class="btn btn-default2" onclick="window.history.go(-1); return false;">Batal</button> <button type="submit" class="btn btn-default1">Simpan</button>
				</div>
			</div>
		</div>
	</form>
@endif

@if ($action == 'edit')
	<form action="" method="post" enctype="multipart/form-data">
	@csrf
		<div class="main-box">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<h3>Ubah Data Verifikator</h3><hr>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Nama
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="text" class="form-control" name="nama" value="{{ $data->name }}">
					<span class="errmsg">{{ $errors->first('nama') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Email
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="email" class="form-control" name="email" value="{{ $data->email }}">
					<span class="errmsg">{{ $errors->first('email') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Password Baru (isi dengan password baru)
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="Input Jika ingin ganti password" id="password">
					<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword()"></span>
					<span class="errmsg">{{ $errors->first('password') }}</span>
					<input type="hidden" name="old_password" value="{{ $data->password }}">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Ulangi Password (isi dengan password baru)
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					<input type="password" class="form-control" name="c_password" value="{{ old('c_password') }}" id="c_password">
					<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="showPassword2()"></span>
					<span class="errmsg">{{ $errors->first('c_password') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xm-11">
					Status
				</div>
				<div class="col-lg-1 col-md-1 col-xm-1">:</div>
				<div class="col-lg-5">
					{!! Form::select('status_admin', array('aktif' => 'Aktif', 'tidak_Aktif' => 'Tidak Aktif') , $data->status_admin, ['placeholder' => 'Pilih Status', 'class' => 'form-control', 'id' => 'select-single']); !!}
					<span class="errmsg">{{ $errors->first('status_admin') }}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9" style="text-align:right">
					<button type="reset" class="btn btn-default2" onclick="window.history.go(-1); return false;">Batal</button> <button type="submit" class="btn btn-default1">Simpan</button>
				</div>
			</div>
		</div>
	</form>
@endif

@if ($action == 'detail')
	<div class="main-box">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<h3>Detail Data Verifikator</h3><hr>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-xm-11">
				Nama
			</div>
			<div class="col-lg-1 col-md-1 col-xm-1">:</div>
			<div class="col-lg-5">
				{{ $data->name }}
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-xm-11">
				Email
			</div>
			<div class="col-lg-1 col-md-1 col-xm-1">:</div>
			<div class="col-lg-5">
				{{ $data->email }}
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-xm-11">
				Status
			</div>
			<div class="col-lg-1 col-md-1 col-xm-1">:</div>
			<div class="col-lg-5">
				{{ ucwords($data->status_admin) }}
			</div>
		</div>
		<div class="row">
			<div class="col-md-9" style="text-align:right">
				<button type="reset" class="btn btn-default2" onclick="window.history.go(-1); return false;">Kembali</button>
			</div>
		</div>
	</div>
@endif
@stop

@section('js')
<script type="text/javascript">
	function showPassword() {
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
	
	function showPassword2() {
		var x = document.getElementById("c_password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
</script>
@endsection