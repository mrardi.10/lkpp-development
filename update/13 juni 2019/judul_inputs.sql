-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 13, 2019 at 02:54 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lobaldf7_lkpp`
--

-- --------------------------------------------------------

--
-- Table structure for table `judul_inputs`
--

CREATE TABLE `judul_inputs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenjang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `judul_inputs`
--

INSERT INTO `judul_inputs` (`id`, `nama`, `jenjang`, `poin`, `keterangan`, `status`, `urutan`, `created_at`, `updated_at`) VALUES
(1, 'Perencanaan PBJP', 'pertama', 'A', '(min. 2 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(2, 'Pemilihan Penyedia Barang/Jasa Pemerintah', 'pertama', 'B', '(min. 4 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(3, 'Pengelolaan Kontrak PBJP', 'pertama', 'C', '(min. 3 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(4, 'Pengelolaan PBJP secara Swakelola', 'pertama', 'D', '(min. 2 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(5, 'Perencanaan PBJP', 'muda', 'A', '(min. 3 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(6, 'Pemilihan Penyedia Barang/Jasa Pemerintah', 'muda', 'B', '(min. 4 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(7, 'Pengelolaan Kontrak PBJP', 'muda', 'C', '(min. 3 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(8, 'Pengelolaan PBJP secara Swakelola', 'muda', 'D', '(min. 2 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(9, 'Perencanaan PBJP', 'madya', 'A', '(min. 3 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(10, 'Pemilihan Penyedia Barang/Jasa Pemerintah', 'madya', 'B', '(min. 5 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(11, 'Pengelolaan Kontrak PBJP', 'madya', 'C', '(min. 3 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(12, 'Pengelolaan PBJP secara Swakelola', 'madya', 'D', '(min. 2 komponen)', NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(13, 'Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan', 'pertama', 'E', NULL, NULL, NULL, '2019-06-10 06:00:00', '2019-06-10 06:00:00'),
(14, 'Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan', 'muda', 'E', NULL, NULL, NULL, '2019-06-10 06:00:00', '2019-06-10 06:00:00'),
(15, 'Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan', 'madya', 'E', NULL, NULL, NULL, '2019-06-10 06:00:00', '2019-06-10 06:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `judul_inputs`
--
ALTER TABLE `judul_inputs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `judul_inputs`
--
ALTER TABLE `judul_inputs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
