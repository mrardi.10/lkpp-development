-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 13, 2019 at 02:55 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lobaldf7_lkpp`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_file_inputs`
--

CREATE TABLE `detail_file_inputs` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_detail_input` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_input_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_input_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_file_inputs`
--

INSERT INTO `detail_file_inputs` (`id`, `id_detail_input`, `title_1`, `title_2`, `nama_input_1`, `nama_input_2`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '1_1_surat_rekomendasi', '1_1_tahun_surat', NULL, '2019-05-21 22:01:12', '2019-05-21 22:01:12'),
(2, '1', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '1_1_dokumen_hasil_pekerjaan', '1_1_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:12', '2019-05-21 22:01:12'),
(3, '2', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '2_1_surat_rekomendasi', '2_1_tahun_surat', NULL, '2019-05-21 22:01:12', '2019-05-21 22:01:12'),
(4, '2', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '2_1_dokumen_hasil_pekerjaan', '2_1_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:12', '2019-05-21 22:01:12'),
(5, '3', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '3_1_surat_rekomendasi', '3_1_tahun_surat', NULL, '2019-05-21 22:01:12', '2019-05-21 22:01:12'),
(6, '3', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '3_1_dokumen_hasil_pekerjaan', '3_1_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(7, '4', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '4_2_surat_rekomendasi', '4_2_tahun_surat', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(8, '4', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '4_2_dokumen_hasil_pekerjaan', '4_2_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(9, '5', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '5_2_surat_rekomendasi', '5_2_tahun_surat', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(10, '5', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '5_2_dokumen_hasil_pekerjaan', '5_2_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(11, '6', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '6_2_surat_rekomendasi', '6_2_tahun_surat', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(12, '6', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '6_2_dokumen_hasil_pekerjaan', '6_2_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(13, '7', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '7_2_surat_rekomendasi', '7_2_tahun_surat', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(14, '7', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '7_2_dokumen_hasil_pekerjaan', '7_2_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(15, '8', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '8_2_surat_rekomendasi', '8_2_tahun_surat', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(16, '8', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '8_2_dokumen_hasil_pekerjaan', '8_2_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(17, '9', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '9_2_surat_rekomendasi', '9_2_tahun_surat', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(18, '9', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '9_2_dokumen_hasil_pekerjaan', '9_2_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:13', '2019-05-21 22:01:13'),
(19, '10', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '10_3_surat_rekomendasi', '10_3_tahun_surat', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(20, '10', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '10_3_dokumen_hasil_pekerjaan', '10_3_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(21, '11', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '11_3_surat_rekomendasi', '11_3_tahun_surat', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(22, '11', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '11_3_dokumen_hasil_pekerjaan', '11_3_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(23, '12', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '12_3_surat_rekomendasi', '12_3_tahun_surat', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(24, '12', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '12_3_dokumen_hasil_pekerjaan', '12_3_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(25, '13', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '13_3_surat_rekomendasi', '13_3_tahun_surat', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(26, '13', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '13_3_dokumen_hasil_pekerjaan', '13_3_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(27, '14', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '14_4_surat_rekomendasi', '14_4_tahun_surat', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(28, '14', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '14_4_dokumen_hasil_pekerjaan', '14_4_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(29, '15', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '15_4_surat_rekomendasi', '15_4_tahun_surat', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(30, '15', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '15_4_dokumen_hasil_pekerjaan', '15_4_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(31, '16', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '16_4_surat_rekomendasi', '16_4_tahun_surat', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(32, '16', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '16_4_dokumen_hasil_pekerjaan', '16_4_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(33, '17', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '17_5_surat_rekomendasi', '17_5_tahun_surat', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(34, '17', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '17_5_dokumen_hasil_pekerjaan', '17_5_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:14', '2019-05-21 22:01:14'),
(35, '18', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '18_5_surat_rekomendasi', '18_5_tahun_surat', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(36, '18', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '18_5_dokumen_hasil_pekerjaan', '18_5_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(37, '19', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '19_5_surat_rekomendasi', '19_5_tahun_surat', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(38, '19', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '19_5_dokumen_hasil_pekerjaan', '19_5_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(39, '20', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '20_6_surat_rekomendasi', '20_6_tahun_surat', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(40, '20', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '20_6_dokumen_hasil_pekerjaan', '20_6_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(41, '21', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '21_6_surat_rekomendasi', '21_6_tahun_surat', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(42, '21', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '21_6_dokumen_hasil_pekerjaan', '21_6_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(43, '22', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '22_6_surat_rekomendasi', '22_6_tahun_surat', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(44, '22', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '22_6_dokumen_hasil_pekerjaan', '22_6_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(45, '23', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '23_6_surat_rekomendasi', '23_6_tahun_surat', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(46, '23', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '23_6_dokumen_hasil_pekerjaan', '23_6_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(47, '24', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '24_6_surat_rekomendasi', '24_6_tahun_surat', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(48, '24', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '24_6_dokumen_hasil_pekerjaan', '24_6_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(49, '25', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '25_6_surat_rekomendasi', '25_6_tahun_surat', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(50, '25', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '25_6_dokumen_hasil_pekerjaan', '25_6_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(51, '26', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '26_7_surat_rekomendasi', '26_7_tahun_surat', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(52, '26', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '26_7_dokumen_hasil_pekerjaan', '26_7_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(53, '27', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '27_7_surat_rekomendasi', '27_7_tahun_surat', NULL, '2019-05-21 22:01:15', '2019-05-21 22:01:15'),
(54, '27', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '27_7_dokumen_hasil_pekerjaan', '27_7_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(55, '28', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '28_7_surat_rekomendasi', '28_7_tahun_surat', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(56, '28', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '28_7_dokumen_hasil_pekerjaan', '28_7_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(57, '29', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '29_5_surat_rekomendasi', '29_5_tahun_surat', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(58, '29', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '29_5_dokumen_hasil_pekerjaan', '29_5_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(59, '30', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '30_6_surat_rekomendasi', '30_6_tahun_surat', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(60, '30', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '30_6_dokumen_hasil_pekerjaan', '30_6_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(61, '31', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '31_7_surat_rekomendasi', '31_7_tahun_surat', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(62, '31', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '31_7_dokumen_hasil_pekerjaan', '31_7_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(63, '32', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '32_8_surat_rekomendasi', '32_8_tahun_surat', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(64, '32', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '32_8_dokumen_hasil_pekerjaan', '32_8_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(65, '33', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '33_8_surat_rekomendasi', '33_8_tahun_surat', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(66, '33', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '33_8_dokumen_hasil_pekerjaan', '33_8_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(67, '34', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '34_8_surat_rekomendasi', '34_8_tahun_surat', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(68, '34', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '34_8_dokumen_hasil_pekerjaan', '34_8_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(69, '35', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '35_9_surat_rekomendasi', '35_9_tahun_surat', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(70, '35', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '35_9_dokumen_hasil_pekerjaan', '35_9_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(71, '36', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '36_9_surat_rekomendasi', '36_9_tahun_surat', NULL, '2019-05-21 22:01:16', '2019-05-21 22:01:16'),
(72, '36', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '36_9_dokumen_hasil_pekerjaan', '36_9_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(73, '37', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '37_9_surat_rekomendasi', '37_9_tahun_surat', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(74, '37', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '37_9_dokumen_hasil_pekerjaan', '37_9_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(75, '38', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '38_10_surat_rekomendasi', '38_10_tahun_surat', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(76, '38', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '38_10_dokumen_hasil_pekerjaan', '38_10_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(77, '39', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '39_10_surat_rekomendasi', '39_10_tahun_surat', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(78, '39', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '39_10_dokumen_hasil_pekerjaan', '39_10_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(79, '40', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '40_10_surat_rekomendasi', '40_10_tahun_surat', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(80, '40', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '40_10_dokumen_hasil_pekerjaan', '40_10_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(81, '41', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '41_10_surat_rekomendasi', '41_10_tahun_surat', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(82, '41', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '41_10_dokumen_hasil_pekerjaan', '41_10_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(83, '42', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '42_10_surat_rekomendasi', '42_10_tahun_surat', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(84, '42', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '42_10_dokumen_hasil_pekerjaan', '42_10_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(85, '43', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '43_10_surat_rekomendasi', '43_10_tahun_surat', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(86, '43', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '43_10_dokumen_hasil_pekerjaan', '43_10_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:17', '2019-05-21 22:01:17'),
(87, '44', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '44_11_surat_rekomendasi', '44_11_tahun_surat', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(88, '44', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '44_11_dokumen_hasil_pekerjaan', '44_11_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(89, '45', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '45_11_surat_rekomendasi', '45_11_tahun_surat', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(90, '45', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '45_11_dokumen_hasil_pekerjaan', '45_11_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(91, '46', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '46_11_surat_rekomendasi', '46_11_tahun_surat', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(92, '46', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '46_11_dokumen_hasil_pekerjaan', '46_11_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(93, '47', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '47_9_surat_rekomendasi', '47_9_tahun_surat', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(94, '47', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '47_9_dokumen_hasil_pekerjaan', '47_9_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(95, '48', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '48_9_surat_rekomendasi', '48_9_tahun_surat', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(96, '48', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '48_9_dokumen_hasil_pekerjaan', '48_9_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(97, '49', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '49_10_surat_rekomendasi', '49_10_tahun_surat', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(98, '49', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '49_10_dokumen_hasil_pekerjaan', '49_10_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(99, '50', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '50_11_surat_rekomendasi', '50_11_tahun_surat', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(100, '50', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '50_11_dokumen_hasil_pekerjaan', '50_11_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(101, '51', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '51_11_surat_rekomendasi', '51_11_tahun_surat', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(102, '51', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '51_11_dokumen_hasil_pekerjaan', '51_11_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(103, '52', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '52_12_surat_rekomendasi', '52_12_tahun_surat', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(104, '52', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '52_12_dokumen_hasil_pekerjaan', '52_12_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(105, '53', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '53_12_surat_rekomendasi', '53_12_tahun_surat', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(106, '53', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '53_12_dokumen_hasil_pekerjaan', '53_12_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:18', '2019-05-21 22:01:18'),
(107, '54', 'SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', 'Tahun SK/ST/Surat Rekomendasi/Sertifikat Pelatihan Kompetensi', '54_12_surat_rekomendasi', '54_12_tahun_surat', NULL, '2019-05-21 22:01:19', '2019-05-21 22:01:19'),
(108, '54', 'Dokumen Hasil Pekerjaan', 'Tahun Paket Pekerjaan', '54_12_dokumen_hasil_pekerjaan', '54_12_tahun_paket_pekerjaan', NULL, '2019-05-21 22:01:19', '2019-05-21 22:01:19'),
(109, '55', 'Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan', '', '55_13_dokumen_hasil_pekerjaan', '', NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
(110, '56', 'Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan', '', '56_14_dokumen_hasil_pekerjaan', '', NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
(111, '57', 'Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan', '', '57_15_dokumen_hasil_pekerjaan', '', NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
(112, '55', NULL, 'Nama Sertifikat ', NULL, '55_13_nama_sertifikat', NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
(113, '55', NULL, 'Tahun Sertifikat ', NULL, '55_13_tahun_sertifikat', NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
(114, '56', NULL, 'Nama Sertifikat ', NULL, '56_14_nama_sertifikat', NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
(115, '56', NULL, 'Tahun Sertifikat ', NULL, '56_14_tahun_sertifikat', NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
(116, '57', NULL, 'Nama Sertifikat', NULL, '57_15_nama_sertifikat', NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
(117, '57', NULL, 'Tahun Sertifikat', NULL, '57_15_tahun_sertifikat', NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_file_inputs`
--
ALTER TABLE `detail_file_inputs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama_input_1` (`nama_input_1`,`nama_input_2`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_file_inputs`
--
ALTER TABLE `detail_file_inputs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
