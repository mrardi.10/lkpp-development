-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 25, 2019 at 09:06 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lobaldf7_lkpp`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabelnilai`
--

CREATE TABLE `tabelnilai` (
  `id` int(11) NOT NULL,
  `no_ujian` int(11) DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_soal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grup_soal` int(11) DEFAULT NULL,
  `jml_soal` int(11) DEFAULT NULL,
  `benar_bs` int(11) DEFAULT NULL,
  `salah_bs` int(11) DEFAULT NULL,
  `kosong_bs` int(11) DEFAULT NULL,
  `benar_pg` int(11) DEFAULT NULL,
  `salah_pg` int(11) DEFAULT NULL,
  `kosong_pg` int(11) DEFAULT NULL,
  `benar_sk` int(11) DEFAULT NULL,
  `salah_sk` int(11) DEFAULT NULL,
  `kosong_sk` int(11) DEFAULT NULL,
  `totbetul` int(11) DEFAULT NULL,
  `totsalah` int(11) DEFAULT NULL,
  `skor` int(11) DEFAULT NULL,
  `ket` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `predikat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jml_essai` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tglujian` date DEFAULT NULL,
  `jamujian` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ctksertfkt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noseri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ceksrtfkt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenjang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_jadwal` int(11) DEFAULT NULL,
  `no_peserta` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabelnilai`
--
ALTER TABLE `tabelnilai`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabelnilai`
--
ALTER TABLE `tabelnilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
