-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 25, 2019 at 09:07 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lobaldf7_lkpp`
--

-- --------------------------------------------------------

--
-- Table structure for table `peserta_jadwals`
--

CREATE TABLE `peserta_jadwals` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `id_peserta` int(11) NOT NULL,
  `id_admin_ppk` int(11) NOT NULL,
  `id_portofolio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_peserta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metode_ujian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_ujian` int(11) DEFAULT NULL,
  `no_seri_sertifikat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_ujian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hasil_pleno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_update` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `peserta_jadwals`
--
ALTER TABLE `peserta_jadwals`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `peserta_jadwals`
--
ALTER TABLE `peserta_jadwals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
