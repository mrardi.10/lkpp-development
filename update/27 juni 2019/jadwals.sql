-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 27, 2019 at 09:48 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lobaldf7_lkpp`
--

-- --------------------------------------------------------

--
-- Table structure for table `jadwals`
--

CREATE TABLE `jadwals` (
  `id` int(10) UNSIGNED NOT NULL,
  `metode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_ujian` date DEFAULT NULL,
  `tanggal_verifikasi` date DEFAULT NULL,
  `waktu_verifikasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_tes` date DEFAULT NULL,
  `waktu_ujian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_ujian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_ruang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kapasitas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batas_waktu_input` date NOT NULL,
  `publish_jadwal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_cp_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp_cp_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_cp_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp_cp_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipe_ujian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'regular',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jadwals`
--
ALTER TABLE `jadwals`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwals`
--
ALTER TABLE `jadwals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
