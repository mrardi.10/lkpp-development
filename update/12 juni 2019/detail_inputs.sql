-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 12, 2019 at 09:54 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lobaldf7_lkpp`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_inputs`
--

CREATE TABLE `detail_inputs` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_judul_input` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_inputs`
--

INSERT INTO `detail_inputs` (`id`, `id_judul_input`, `nama`, `poin`, `status`, `keterangan`, `urutan`, `created_at`, `updated_at`) VALUES
(1, '1', 'Penyusunan Spesifikasi Teknis dan KAK', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(2, '1', 'Penyusunan Perkiraan Harga untuk setiap tahapan PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(3, '1', 'Identifikasi/Reviu Kebutuhan dan Penetapan Barang/Jasa', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(4, '2', 'Reviu terhadap Dokumen Persiapan PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(5, '2', 'Penyusunan dan Penjelasan Dokumen Pemilihan', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(6, '2', 'Evaluasi Penawaran', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(7, '2', 'Penilaian Kualifikasi', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(8, '2', 'Negosiasi dalam PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(9, '2', 'Melakukan Pengadaan secara E-Purchasing dan pembelian melalui Toko Daring', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(10, '3', 'Perumusan Kontrak PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(11, '3', 'Pengendalian Pelaksanaan Kontrak PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(12, '3', 'Serah Terima Hasil PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(13, '3', 'Evaluasi Kinerja Penyedia PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(14, '4', 'Penyusunan Rencana dan Persiapan PBJP secara Swakelola', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(15, '4', 'Pelaksanaan PBJP secara Swakelola', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(16, '4', 'Pengawasan, Pengendalian, dan Pelaporan PBJP secara Swakelola', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(17, '5', 'Penyusunan Spesifikasi Teknis dan KAK', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(18, '5', 'Penyusunan Perkiraan Harga untuk setiap tahapan PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(19, '5', 'Identifikasi/Reviu Kebutuhan dan Penetapan Barang/Jasa', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(20, '6', 'Reviu terhadap Dokumen Persiapan PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(21, '6', 'Penyusunan dan Penjelasan Dokumen Pemilihan', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(22, '6', 'Evaluasi Penawaran', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(23, '6', 'Penilaian Kualifikasi', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(24, '6', 'Pengelolaan Sanggahan', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(25, '6', 'Penyusunan Daftar Penyedia', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(26, '7', 'Perumusan Kontrak PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(27, '7', 'Pengendalian Pelaksanaan Kontrak PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(28, '7', 'Serah Terima Hasil PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(29, '5', 'Perumusan Strategi Pengadaan, Pemaketan dan Cara Pengadaan', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(30, '6', 'Negosiasi dalam PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(31, '7', 'Evaluasi Kinerja Penyedia PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(32, '8', 'Penyusunan Rencana dan Persiapan PBJP secara Swakelola', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(33, '8', 'Pelaksanaan PBJP secara Swakelola', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(34, '8', 'Pengawasan, Pengendalian, dan Pelaporan PBJP secara Swakelola', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(35, '9', 'Penyusunan Spesifikasi Teknis dan KAK', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(36, '9', 'Penyusunan Perkiraan Harga untuk setiap tahapan PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(37, '9', 'Identifikasi/Reviu Kebutuhan dan Penetapan Barang/Jasa', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(38, '10', 'Reviu terhadap Dokumen Persiapan PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(39, '10', 'Penyusunan dan Penjelasan Dokumen Pemilihan', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(40, '10', 'Evaluasi Penawaran', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(41, '10', 'Penilaian Kualifikasi', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(42, '10', 'Pengelolaan Sanggahan', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(43, '10', 'Penyusunan Daftar Penyedia', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(44, '11', 'Perumusan Kontrak PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(45, '11', 'Pembentukan Tim Pengelola Kontrak PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(46, '11', 'Pengendalian Pelaksanaan Kontrak PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(47, '9', 'Perumusan Strategi Pengadaan, Pemaketan dan Cara Pengadaan', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(48, '9', 'Perumusan Organisasi PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(49, '10', 'Negosiasi dalam PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(50, '11', 'Serah Terima Hasil PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(51, '11', 'Evaluasi Kinerja Penyedia PBJP', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(52, '12', 'Penyusunan Rencana dan Persiapan PBJP secara Swakelola', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(53, '12', 'Pelaksanaan PBJP secara Swakelola', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(54, '12', 'Pengawasan, Pengendalian, dan Pelaporan PBJP secara Swakelola', NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 17:00:00'),
(55, '13', NULL, NULL, NULL, NULL, NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
(56, '14', NULL, NULL, NULL, NULL, NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
(57, '15', NULL, NULL, NULL, NULL, NULL, '2019-06-12 06:00:00', '2019-06-12 06:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_inputs`
--
ALTER TABLE `detail_inputs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_inputs`
--
ALTER TABLE `detail_inputs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
