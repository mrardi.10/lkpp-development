-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 26, 2019 at 10:09 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lobaldf7_lkpp`
--

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_instansis`
--

CREATE TABLE `jadwal_instansis` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_admin_ppk` int(11) NOT NULL,
  `tanggal_tes_tertulis` date NOT NULL,
  `waktu_uji_tertulis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi_ujian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_ruang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kapasitas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batas_waktu_input` date DEFAULT NULL,
  `nama_cp_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telp_cp_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_cp_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telp_cp_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_permohonan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_permohonan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `di_setujui_oleh` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jadwal_instansis`
--
ALTER TABLE `jadwal_instansis`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwal_instansis`
--
ALTER TABLE `jadwal_instansis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
