-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.60-MariaDB - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             10.2.0.5621
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table lkpp_dev_inp.status_peserta_inpassings
CREATE TABLE IF NOT EXISTS `status_peserta_inpassings` (
  `id` int(10) unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table lkpp_dev_inp.status_peserta_inpassings: ~16 rows (approximately)
DELETE FROM `status_peserta_inpassings`;
/*!40000 ALTER TABLE `status_peserta_inpassings` DISABLE KEYS */;
INSERT INTO `status_peserta_inpassings` (`id`, `status`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'Menunggu Verifikasi Dokumen Persyaratan ', 'Menunggu Verifikasi Dokumen Persyaratan (hingga H-12 ujian)', '2019-04-07 17:00:00', '2019-04-07 17:00:00'),
	(2, 'Dokumen Persyaratan Tidak Lengkap', 'Dokumen Persyaratan Ditolak (dapat dilengkapi sebelum H-10 ujian)', '2019-04-07 17:00:00', '2019-04-07 17:00:00'),
	(3, 'Dokumen Persyaratan Lengkap', '-', '2019-04-07 17:00:00', '2019-04-07 17:00:00'),
	(4, 'Menunggu Verifikasi Portofolio atau Uji Tulis', 'Menunggu Verifikasi Portofolio atau Uji Tulis (Tanggal xx) sesuai metode ujian yang dipilih', '2019-04-07 17:00:00', '2019-04-07 17:00:00'),
	(5, 'Dokumen Portofolio Tidak Lengkap ', 'Dokumen Portofolio Tidak Lengkap (dapat dilengkapi hingga H+5 ujian)', '2019-04-07 17:00:00', '2019-04-07 17:00:00'),
	(6, 'Lulus', '-', '2019-04-07 17:00:00', '2019-04-07 17:00:00'),
	(7, 'Tidak Lulus', '-', '2019-04-07 17:00:00', '2019-04-07 17:00:00'),
	(8, 'Cetak Sertifikat dan/atau SKTL', 'Jika Lulus', '2019-04-07 17:00:00', '2019-04-07 17:00:00'),
	(9, 'Penyusunan Pertek dan/atau Surat Penyampaian Hasil Ujian', 'Jika Tidak Lulus', '2019-04-07 17:00:00', '2019-04-07 17:00:00'),
	(10, 'Selesai', '-', '2019-04-07 17:00:00', '2019-04-07 17:00:00'),
	(11, 'Tidak Hadir Tes', 'Tidak Hadir Tes', '2019-05-17 06:00:00', '2019-05-17 06:00:00'),
	(12, 'Menunggu Verifikasi Portofolio', 'Menunggu Verifikasi Portofolio', '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
	(13, 'Menunggu Tes Tertulis', 'Menunggu Tes Tertulis', '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
	(14, 'Rekomendasi Lulus', 'Rekomendasi Asesor', '2019-06-12 06:00:00', '2019-06-12 06:00:00'),
	(15, 'Rekomendasi Tidak Lulus', 'Rekomendasi Asesor\r\n', NULL, NULL),
	(16, 'Menunggu Verifikasi Dokumen Surat Usulan', '', '2019-06-12 06:00:00', '2019-06-12 06:00:00');
/*!40000 ALTER TABLE `status_peserta_inpassings` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
