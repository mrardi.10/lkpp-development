<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusFilePortofoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_file_portofolios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_peserta')->nullable();
            $table->integer('id_jadwal')->nullable();
            $table->integer('id_detail_file_input')->nullable();
            $table->string('status')->nullable();
            $table->string('keterangan')->nullable();
            $table->integer('id_admin_ppk')->nullable();
            $table->integer('id_admin_lkpp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_file_portofolios');
    }
}
