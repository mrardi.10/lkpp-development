<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDokumenPortofolioCSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_portofolio_c_s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->nullable();
            $table->integer('id_portofolio')->nullable();
            $table->string('surat_perumusan_kontrak_pbjp')->nullable();
            $table->string('tahun_surat_perumusan_kontrak_pbjp')->nullable();
            $table->string('dokumen_perumusan_kontrak_pbjp')->nullable();
            $table->string('tahun_paket_perumusan_kontrak_pbjp')->nullable();
            $table->string('surat_pengendalian_pelaksanaan_kontrak_pbjp')->nullable();
            $table->string('tahun_surat_pengendalian_pelaksanaan_kontrak_pbjp')->nullable();
            $table->string('dokumen_pengendalian_pelaksanaan_kontrak_pbjp')->nullable();
            $table->string('tahun_paket_pengendalian_pelaksanaan_kontrak_pbjp')->nullable();
            $table->string('surat_serah_terima_hasil_pbjp')->nullable();
            $table->string('tahun_surat_serah_terima_hasil_pbjp')->nullable();
            $table->string('dokumen_serah_terima_hasil_pbjp')->nullable();
            $table->string('tahun_paket_serah_terima_hasil_pbjp')->nullable();
            $table->string('surat_evaluasi_kinerja_penyedia_pbjp')->nullable();
            $table->string('tahun_surat_evaluasi_kinerja_penyedia_pbjp')->nullable();
            $table->string('dokumen_evaluasi_kinerja_penyedia_pbjp')->nullable();
            $table->string('tahun_paket_evaluasi_kinerja_penyedia_pbjp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen_portofolio_c_s');
    }
}
