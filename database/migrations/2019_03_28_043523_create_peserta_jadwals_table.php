<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertaJadwalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta_jadwals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_jadwal');
            $table->integer('id_peserta');
            $table->integer('id_admin_ppk');
            $table->string('nama_peserta');
            $table->string('metode_ujian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peserta_jadwals');
    }
}
