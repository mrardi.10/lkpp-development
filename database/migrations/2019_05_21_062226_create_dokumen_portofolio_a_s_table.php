<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDokumenPortofolioASTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_portofolio_a_s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->nullable();
            $table->integer('id_portofolio')->nullable();
            $table->string('surat_penyusunan_spesifikasi_teknis_dan_kak')->nullable();
            $table->string('tahun_surat_penyusunan_spesifikasi_teknis_dan_kak')->nullable();
            $table->string('dokumen_penyusunan_spesifikasi_teknis_dan_kak')->nullable();
            $table->string('tahun_paket_penyusunan_spesifikasi_teknis_dan_kak')->nullable();
            $table->string('surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp')->nullable();
            $table->string('tahun_surat_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp')->nullable();
            $table->string('dokumen_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp')->nullable();
            $table->string('tahun_paket_penyusunan_perkiraan_harga_untuk_setiap_tahapan_pbjp')->nullable();
            $table->string('surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang')->nullable();
            $table->string('tahun_surat_identifikasi_reviu_kebutuhan_dan_penetapan_barang')->nullable();
            $table->string('dokumen_identifikasi_reviu_kebutuhan_dan_penetapan_barang')->nullable();
            $table->string('tahun_paket_identifikasi_reviu_kebutuhan_dan_penetapan_barang')->nullable();
            $table->string('surat_perumusan_strategi_pengadaan_pemaketan')->nullable();
            $table->string('tahun_surat_perumusan_strategi_pengadaan_pemaketan')->nullable();
            $table->string('dokumen_perumusan_strategi_pengadaan_pemaketan')->nullable();
            $table->string('tahun_paket_dokumen_perumusan_strategi_pengadaan_pemaketan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen_portofolio_a_s');
    }
}
