<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpanBerkasPortofoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simpan_berkas_portofolios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_detail_file_input');
            $table->string('file')->nullable();
            $table->string('tahun')->nullable();
            $table->integer('id_peserta')->nullable();
            $table->integer('id_jadwal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simpan_berkas_portofolios');
    }
}
