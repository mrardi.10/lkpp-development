<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifikasiPesertaJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peserta_jadwals', function(Blueprint $table) {
            $table->string('id_portofolio')->nullable()->after('id_admin_ppk');
            $table->string('nama_peserta')->nullable()->change();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peserta_jadwals', function($table) {
            $table->dropColumn('id_portofolio');
          });
    }
}
