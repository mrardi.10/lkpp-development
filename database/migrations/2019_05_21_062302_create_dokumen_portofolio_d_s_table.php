<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDokumenPortofolioDSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_portofolio_d_s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->nullable();
            $table->integer('id_portofolio')->nullable();
            $table->string('surat_penyusunan_rencana_dan_persiapan_pengadaan')->nullable();
            $table->string('tahun_surat_penyusunan_rencana_dan_persiapan_pengadaan')->nullable();
            $table->string('dokumen_penyusunan_rencana_dan_persiapan_pengadaan')->nullable();
            $table->string('tahun_paket_penyusunan_rencana_dan_persiapan_pengadaan')->nullable();
            $table->string('surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa')->nullable();
            $table->string('tahun_surat_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa')->nullable();
            $table->string('dokumen_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa')->nullable();
            $table->string('tahun_paket_pelaksanaan_dan_pengawasan_pengadaan_barang_jasa')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen_portofolio_d_s');
    }
}
