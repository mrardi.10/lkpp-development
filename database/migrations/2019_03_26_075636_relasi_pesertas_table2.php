<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelasiPesertasTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pesertas', function (Blueprint $table) {
            $table->bigInteger('id_admin_ppk')->unsigned()->change();
            $table->foreign('id_admin_ppk')->references('id')->on('users')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesertas', function(Blueprint $table) {
            $table->dropForeign('templates_id_admin_ppk_foreign');
            $table->dropIndex('templates_id_admin_ppk_foreign');
            $table->integer('id_admin_ppk')->change();
        });
    }
}
