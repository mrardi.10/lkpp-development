<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifikasiPesertaJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peserta_jadwals', function(Blueprint $table) {
            $table->string('status_ujian')->nullable()->after('metode_ujian');
            $table->string('publish')->nullable()->after('status_ujian');
            $table->integer('admin_update')->nullable()->after('hasil_pleno');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peserta_jadwals', function($table) {
            $table->dropColumn('status_ujian');
            $table->dropColumn('publish');
            $table->dropColumn('admin_update');
          });
    }
}
