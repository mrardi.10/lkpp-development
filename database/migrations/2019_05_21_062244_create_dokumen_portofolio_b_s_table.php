<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDokumenPortofolioBSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_portofolio_b_s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->nullable();
            $table->integer('id_portofolio')->nullable();
            $table->string('surat_reviu_terhadap_dokumen_persiapan_pbjp')->nullable();
            $table->string('tahun_surat_reviu_terhadap_dokumen_persiapan_pbjp')->nullable();
            $table->string('dokumen_reviu_terhadap_dokumen_persiapan_pbjp')->nullable();
            $table->string('tahun_paket_reviu_terhadap_dokumen_persiapan_pbjp')->nullable();
            $table->string('surat_penyusunan_dan_penjelasan_dokumen_pemilihan')->nullable();
            $table->string('tahun_surat_penyusunan_dan_penjelasan_dokumen_pemilihan')->nullable();
            $table->string('dokumen_penyusunan_dan_penjelasan_dokumen_pemilihan')->nullable();
            $table->string('tahun_paket_penyusunan_dan_penjelasan_dokumen_pemilihan')->nullable();
            $table->string('surat_evaluasi_penawaran')->nullable();
            $table->string('tahun_surat_evaluasi_penawaran')->nullable();
            $table->string('dokumen_evaluasi_penawaran')->nullable();
            $table->string('tahun_paket_evaluasi_penawaran')->nullable();
            $table->string('surat_penilian_kualifikasi')->nullable();
            $table->string('tahun_surat_penilian_kualifikasi')->nullable();
            $table->string('dokumen_penilian_kualifikasi')->nullable();
            $table->string('tahun_paket_penilian_kualifikasi')->nullable();
            $table->string('surat_pengelolaan_sanggahan')->nullable();
            $table->string('tahun_surat_pengelolaan_sanggahan')->nullable();
            $table->string('dokumen_pengelolaan_sanggahan')->nullable();
            $table->string('tahun_paket_pengelolaan_sanggahan')->nullable();
            $table->string('surat_penyusunan_daftar_penyedia')->nullable();
            $table->string('tahun_surat_penyusunan_daftar_penyedia')->nullable();
            $table->string('dokumen_penyusunan_daftar_penyedia')->nullable();
            $table->string('tahun_paket_penyusunan_daftar_penyedia')->nullable();
            $table->string('surat_negosiasi_dalam_pbjp')->nullable();
            $table->string('tahun_surat_surat_negosiasi_dalam_pbjp')->nullable();
            $table->string('dokumen_surat_negosiasi_dalam_pbjp')->nullable();
            $table->string('tahun_paket_surat_negosiasi_dalam_pbjp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen_portofolio_b_s');
    }
}
