<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_inputs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_judul_input')->nullable();
            $table->string('nama')->nullable();
            $table->string('poin')->nullable();
            $table->string('status')->nullable();
            $table->string('keterangan')->nullable();
            $table->integer('urutan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_inputs');
    }
}
