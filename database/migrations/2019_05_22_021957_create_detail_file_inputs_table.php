<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailFileInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_file_inputs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_detail_input');
            $table->string('title_1');
            $table->string('title_2');
            $table->string('nama_input_1');
            $table->string('nama_input_2');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_file_inputs');
    }
}
