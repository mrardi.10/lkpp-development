<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalInstansisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_instansis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_admin_ppk');
            $table->string('tanggal_tes_tertulis');
            $table->string('waktu_uji_tertulis');
            $table->string('lokasi_ujian');
            $table->string('jumlah_ruang');
            $table->string('kapasitas');
            $table->string('jumlah_unit');
            $table->string('nama_cp_1')->nullable();
            $table->string('telp_cp_1')->nullable();
            $table->string('nama_cp_2')->nullable();
            $table->string('telp_cp_2')->nullable();
            $table->string('surat_permohonan');
            $table->string('status_permohonan')->nullable();
            $table->string('di_setujui_oleh')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_instansis');
    }
}
