<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDokumenPortofoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_portofolios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_peserta')->nullable();
            $table->string('kompetensi_perencanaan_pbjp')->nullable();
            $table->string('kompetensi_pemilihan_pbj')->nullable();
            $table->string('kompetensi_pengelolaan_kontrak_pbjp')->nullable();
            $table->string('kompetensi_pbj_secara_swakelola')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen_portofolios');
    }
}
