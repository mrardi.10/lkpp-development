<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        $identity  = request()->get('email');
        $fieldName = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$fieldName => $identity]);
        return $fieldName;
    }

    protected function validateLogin(\Illuminate\Http\Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required', 'password' => 'required', 'captcha' => 'required|captcha',
        ]);
    }

    public function showLoginForm()
    {
        $text = DB::table('text_runnings')->orderBy('id','desc')->get();

        return view('auth.login', compact('text','from'));
    }

    public function captchaValidate(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'captcha' => 'required|captcha'
        ]);
    }

    public function refreshCaptcha()
    {
        $data = "{{ captcha_img() }}";
        return response()->json(['captcha'=> $data]);
    }

    // protected function sendFailedLoginResponse(Request $request)
    // {
    //     $request->session()->put('login_error', trans('auth.failed'));
    //     throw ValidationException::withMessages(
    //         [
    //             'error' => [trans('auth.failed')],
    //         ]
    //     );
    // }
}
