<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Instansi;
use Carbon\Carbon;
use Redirect;
use Validator;
use View;
use DB;

class HomePPKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_user = Auth::user()->id;
        $user_lulus = DB::table('users')
					  ->join('peserta_jadwals','users.id','=','peserta_jadwals.id_admin_ppk')
					  ->select('peserta_jadwals.id', 'peserta_jadwals.created_at')
					  ->where('users.id',$id_user)
					  ->where('peserta_jadwals.status_ujian','lulus')
					  ->where('peserta_jadwals.publish','publish')
					  ->get()
					  ->groupBy(function($date) {
							return Carbon::parse($date->created_at)->format('m'); // grouping by months
						});
		
		$users_tidak_lulus = DB::table('users')
							 ->join('peserta_jadwals','users.id','=','peserta_jadwals.id_admin_ppk')
							 ->select('peserta_jadwals.id', 'peserta_jadwals.created_at')
							 ->where('users.id',$id_user)
							 ->where('peserta_jadwals.status_ujian','tidak_lulus')
							 ->get()
							 ->groupBy(function($date) {
									return Carbon::parse($date->created_at)->format('m'); // grouping by months
								});
		
		$usercountLulus = [];
        $userLulus = [];
        $usercountTidakLulus = [];
        $userTidakLulus = [];

        foreach ($user_lulus as $key => $value) {
            $usercountLulus[(int)$key] = count($value);
        }

        for($i = 1; $i <= 12; $i++){
            if(!empty($usercountLulus[$i])){
                $userLulus[$i] = $usercountLulus[$i];    
            } else {
                $userLulus[$i] = 0;    
            }
        }

        foreach ($users_tidak_lulus as $key => $value) {
            $usercountTidakLulus[(int)$key] = count($value);
        }

        for($i = 1; $i <= 12; $i++){
            if(!empty($usercountTidakLulus[$i])){
                $userTidakLulus[$i] = $usercountTidakLulus[$i];    
            } else {
                $userTidakLulus[$i] = 0;    
            }
        }

        return View::make('biodata', compact('userLulus','userTidakLulus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $instansi = Instansi::pluck('nama','id');
        $data = Instansi::where('id',Auth::user()->nama_instansi)->first();
        return View::make('biodata_admin', compact('instansi','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = array(
            'nip' => 'required',
            'nama' => 'required',
            'jabatan' => 'required',
            'nama_satuan' => 'required',
            'no_telp' => 'required|digits_between:2,15',
            'foto_profile' => 'mimes:jpg,jpeg|min:100|max:2048',
            'sk_admin_ppk' => 'mimes:pdf,jpg,jpeg|min:100|max:2048'
        );

        $message = ['no_telp.digits_between' => 'No. Telepon/HP harus berjumlah 2 hingga 12 digit'];
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::to('biodata-admin-ppk')
                ->withErrors($validator)
                ->withInput();
        }

        if($request->hasFile('foto_profile')){
            //MENGAMBIL FILE IMAGE DARI FORM
            $file = $request->file('foto_profile');
			
            //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
            $uniqueFileName = Carbon::now()->timestamp ."_foto_profile.". $file->getClientOriginalExtension();
			
            //UPLOAD ORIGINAL FILE (BELUM DIUBAH DIMENSINYA)
            $destinationPath = 'storage/data/foto_profile';
            $file->move($destinationPath,$uniqueFileName);
            $unlik_pic = $destinationPath.'/'.Auth::user()->foto_profile;
        }

        if($request->hasFile('sk_admin_ppk')){
            //MENGAMBIL FILE IMAGE DARI FORM
            $sk_admin_ppk = $request->file('sk_admin_ppk');
            
			//MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
            $sk_admin_ppkName = Carbon::now()->timestamp ."_". $request->input('nip') .".". $sk_admin_ppk->getClientOriginalExtension();
            
			//UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
            $sk_admin_ppkPath = 'storage/data/sk_admin_ppk';
            $sk_admin_ppk->move($sk_admin_ppkPath,$sk_admin_ppkName);
            $unlik_sk_admin_ppk = $sk_admin_ppkPath.'/'.Auth::user()->sk_admin_ppk;
        }
        
        $id = $request->input('id');
        $data = User::find($id);
        $data->nip = $request->input('nip');
        $data->name = $request->input('nama');
        $data->jabatan = $request->input('jabatan');
        $data->nama_satuan = $request->input('nama_satuan');
        $data->no_telp = $request->input('no_telp');
        
		if($request->hasFile('foto_profile')){
			$data->foto_profile = $uniqueFileName;
        }
        
		if($request->hasFile('sk_admin_ppk')){
			$data->sk_admin_ppk = $sk_admin_ppkName;
        }
        
		if($data->save()){
            if($request->hasFile('foto_profile')){
				unlink($unlik_pic);
            }
            return Redirect::to('biodata-admin-ppk')->with('msg','berhasil simpan');
        } else {
            return Redirect::to('biodata-admin-ppk')->with('msg','gagal simpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}