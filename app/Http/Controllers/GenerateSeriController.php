<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PesertaJadwal;
use App\Peserta;
use App\PesertaInstansi;
use App\Hitung;
use Carbon\Carbon;
use View;
use DB;
use Redirect;

class GenerateSeriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data1 = DB::table('peserta_jadwals')
        ->join('pesertas','peserta_jadwals.id_peserta','=','pesertas.id')
        ->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
        ->select('peserta_jadwals.*','pesertas.nip as nips','jadwals.tanggal_ujian as tanggals')
        ->where('status_ujian','lulus')
        ->where('publish','publish')
        ->where('no_seri_sertifikat','null')
        ->orderBy('id_jadwal','asc')
        ->get();

        $data2 = DB::table('peserta_instansis')
        ->join('pesertas','peserta_instansis.id_peserta','=','pesertas.id')
        ->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id')
        ->select('peserta_instansis.*','pesertas.nip as nips','jadwal_instansis.tanggal_ujian as tanggals')
        ->where('status_ujian','lulus')
        ->where('publish','publish')
        ->where('no_seri_sertifikat','null')
        ->orderBy('id_jadwal','asc')
        ->get();

        $merged = $data2->merge($data1);
        $data = $merged->all();

        return View::make('generate-noseri', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data1 = DB::table('peserta_jadwals')
        ->join('pesertas','peserta_jadwals.id_peserta','=','pesertas.id')
        ->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
        ->select('peserta_jadwals.*','pesertas.nip as nips','jadwals.tanggal_ujian as tanggals')
        ->where('status_ujian','lulus')
        ->where('publish','publish')
        ->where('no_seri_sertifikat','null')
        ->orderBy('id_jadwal','asc')
        ->get();

        $data2 = DB::table('peserta_instansis')
        ->join('pesertas','peserta_instansis.id_peserta','=','pesertas.id')
        ->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id')
        ->select('peserta_instansis.*','pesertas.nip as nips','jadwal_instansis.tanggal_ujian as tanggals')
        ->where('status_ujian','lulus')
        ->where('publish','publish')
        ->where('no_seri_sertifikat','null')
        ->orderBy('id_jadwal','asc')
        ->get();

        $dt = Carbon::now();

        if (count($data1) != 0) {
            foreach ($data1 as $key => $datas) {
                $hitung = DB::table('hitungs')
                ->orderBy('id','desc')
                ->whereNull('deleted_at')
                ->first();

                if ($hitung) {
                    $get_no_seri_depan = (int) $hitung->counter_seri_depan;
                    $no_seri_depan = $get_no_seri_depan + 1;
                    $get_no_seri_belakang = (int) $hitung->counter_seri_belakang;
                    $no_seri_belakang = $get_no_seri_belakang + 1;
                }else{
                    $no_seri_depan = 1;
                    $no_seri_belakang = 1;
                }
                $peserta = PesertaJadwal::find($datas->id);
                if ($peserta) {
                    $get_jenjang = Peserta::find($peserta->id_peserta);
                    if ($get_jenjang) {
                        $nama_jenjang = $get_jenjang->jenjang;
                        $seri_jenjang = 0;
                        if (strtolower($nama_jenjang) == 'pertama') {
                            $seri_jenjang = 1;
                        }
                        if (strtolower($nama_jenjang) == 'muda') {
                            $seri_jenjang = 2;
                        }
                        if (strtolower($nama_jenjang) == 'madya') {
                            $seri_jenjang = 3;
                        }
                    }else{
                        $seri_jenjang = 0;
                    }

                    $peserta->no_seri_sertifikat = $dt->format('m').$dt->format('y').str_pad($no_seri_depan, 5, "0", STR_PAD_LEFT).$seri_jenjang.str_pad($no_seri_belakang, 6, "0", STR_PAD_LEFT);
                    if ($peserta->save()) {
                        $simpan_hitung = new Hitung();
                        $simpan_hitung->id_peserta = $peserta->id_peserta;
                        $simpan_hitung->counter_seri_depan = $no_seri_depan;
                        $simpan_hitung->counter_seri_belakang = $no_seri_belakang;
                        $simpan_hitung->save();
                    }
                }
            }
        }

        if (count($data2) != 0) {
            foreach ($data2 as $key => $datasInt) {
                $hitung = DB::table('hitungs')
                ->orderBy('id','desc')
                ->whereNull('deleted_at')
                ->first();

                if ($hitung) {
                    $get_no_seri_depan = (int) $hitung->counter_seri_depan;
                    $no_seri_depan = $get_no_seri_depan + 1;
                    $get_no_seri_belakang = (int) $hitung->counter_seri_belakang;
                    $no_seri_belakang = $get_no_seri_belakang + 1;
                }else{
                    $no_seri_depan = 00001;
                    $no_seri_belakang = 000001;
                }
                $pesertaInt = PesertaInstansi::find($datasInt->id);
                if ($pesertaInt) {
                    $get_jenjang = Peserta::find($pesertaInt->id_peserta);
                    if ($get_jenjang) {
                        $nama_jenjang = $get_jenjang->jenjang;
                        $seri_jenjang = 0;
                        if (strtolower($nama_jenjang) == 'pertama') {
                            $seri_jenjang = 1;
                        }
                        if (strtolower($nama_jenjang) == 'muda') {
                            $seri_jenjang = 2;
                        }
                        if (strtolower($nama_jenjang) == 'madya') {
                            $seri_jenjang = 3;
                        }
                    }else{
                        $seri_jenjang = 0;
                    }

                    $pesertaInt->no_seri_sertifikat = $dt->format('m').$dt->format('y').str_pad($no_seri_depan, 5, "0", STR_PAD_LEFT).$seri_jenjang.str_pad($no_seri_belakang, 6, "0", STR_PAD_LEFT);
                    if ($pesertaInt->save()) {
                        $simpan_hitung = new Hitung();
                        $simpan_hitung->id_peserta = $peserta->id_peserta;
                        $simpan_hitung->counter_seri_depan = $no_seri_depan;
                        $simpan_hitung->counter_seri_belakang = $no_seri_belakang;
                        $simpan_hitung->save();
                    }
                }
            }
        }

        return Redirect::to('generate-noseri')->with('msg','berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
