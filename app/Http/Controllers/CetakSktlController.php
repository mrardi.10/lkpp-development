<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PesertaJadwal;
use PDF;
use View;
use DB;
use Redirect;
use Helper;
use Carbon\Carbon;

class CetakSktlController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jadwal1 = DB::table('jadwals')
				   ->join('peserta_jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
				   ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"))
				   ->where('deleted_at',null)
				   ->groupBy('jadwals.id')
				   ->orderBy('jadwals.tanggal_ujian','DESC')
				   ->get();

        $jadwal2 = DB::table('jadwal_instansis')
				   ->where('status_permohonan','setuju')
				   ->orderBy('tanggal_ujian','DESC')
				   ->get(['id','tanggal_ujian','lokasi_ujian','metode','tipe_ujian']);

        $merged = $jadwal1->merge($jadwal2);
        $jadwal = $merged->all();
        return View::make('cetak_sktl', compact('jadwal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($tipe,$id)
    {
        if ($tipe == 'regular') {
            $data = DB::table('peserta_jadwals')
					->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
					->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
					->select('pesertas.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids','peserta_jadwals.no_ujian','peserta_jadwals.no_seri_sertifikat','peserta_jadwals.status_ujian','jadwals.tipe_ujian')
					->where('peserta_jadwals.id_jadwal',$id)
					->where('peserta_jadwals.status_ujian','tidak_lulus')
					->get();
        } else {
            $data = DB::table('peserta_instansis')
					->join('pesertas', 'peserta_instansis.id_peserta','=','pesertas.id')
					->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id')
					->select('pesertas.*','peserta_instansis.metode_ujian as metodes','peserta_instansis.id as ids','peserta_instansis.no_ujian','peserta_instansis.no_seri_sertifikat','peserta_instansis.status_ujian','jadwal_instansis.tipe_ujian')
					->where('peserta_instansis.id_jadwal',$id)
					->where('peserta_instansis.status_ujian','tidak_lulus')
					->get();
        }

        return View::make('cetak_sktl_peserta', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function print($tipe,$id)
    {
        if ($tipe == 'regular') {
			$data = DB::table('peserta_jadwals')
					->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwals', 'jadwals.id', '=', 'peserta_jadwals.id_jadwal','left')
					->select('pesertas.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids','peserta_jadwals.no_ujian','peserta_jadwals.no_seri_sertifikat','peserta_jadwals.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwals.tanggal_ujian as tanggal_ujian','jadwals.lokasi_ujian')
					->where('peserta_jadwals.status_ujian','tidak_lulus')
					->where('peserta_jadwals.id_peserta',$id)
					->first();
        } else {
            $data = DB::table('peserta_instansis')
					->join('pesertas', 'peserta_instansis.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwal_instansis', 'jadwal_instansis.id', '=', 'peserta_instansis.id_jadwal','left')
					->select('pesertas.*','peserta_instansis.metode_ujian as metodes','peserta_instansis.id as ids','peserta_instansis.no_ujian','peserta_instansis.no_seri_sertifikat','peserta_instansis.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwal_instansis.tanggal_ujian as tanggal_ujian','jadwal_instansis.lokasi_ujian')
					->where('peserta_instansis.status_ujian','tidak_lulus')
					->where('peserta_instansis.id_peserta',$id)
					// ->whereNotNull('no_seri_sertifikat')
					->first();
        }

         $hariini = date('Y-m-d');

         if($data) {
             $pdf = PDF::loadView('pdf.sktl_inpassing', compact('data','hariini'));
             return $pdf->stream('sktl_inpassing.pdf','I');
         } else {
             return Redirect::back();
         }
    }
	
	public function printWord($tipe,$id){
        if ($tipe == 'regular') {
			$data = DB::table('peserta_jadwals')
					->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwals', 'jadwals.id', '=', 'peserta_jadwals.id_jadwal','left')
					->select('pesertas.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids','peserta_jadwals.no_ujian','peserta_jadwals.no_seri_sertifikat','peserta_jadwals.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwals.tanggal_ujian as tanggal_ujian','jadwals.lokasi_ujian')
					->where('peserta_jadwals.status_ujian','tidak_lulus')
					->where('peserta_jadwals.id_peserta',$id)
					->first();
         } else {
			$data = DB::table('peserta_instansis')
					->join('pesertas', 'peserta_instansis.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwal_instansis', 'jadwal_instansis.id', '=', 'peserta_instansis.id_jadwal','left')
					->select('pesertas.*','peserta_instansis.metode_ujian as metodes','peserta_instansis.id as ids','peserta_instansis.no_ujian','peserta_instansis.no_seri_sertifikat','peserta_instansis.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwal_instansis.tanggal_ujian as tanggal_ujian','jadwal_instansis.lokasi_ujian')
					->where('peserta_instansis.status_ujian','tidak_lulus')
					->where('peserta_instansis.id_peserta',$id)
					->first();
        }

        $hariini = date('Y-m-d');
		$dt = Carbon::now();
        $namaBulan = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        $dt_tahun = $dt->year;
        $dt_bulan = $dt->month;
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $width = \PhpOffice\PhpWord\Shared\Converter::inchToTwip(8.27);
        $height = \PhpOffice\PhpWord\Shared\Converter::inchToTwip(11.69);
        $sectionStyle = array(
            'marginTop' => 600,
            'marginLeft' => 600,
            'marginRight' => 600,
            'pageSizeW' => $width,
            'pageSizeH' => $height 
        );
		
        $headerStyle = array(
            'marginTop' => 600,
            'marginLeft' => 600,
            'marginRight' => 600,
            'pageSizeW' => $width
        );
		
        $phpWord->setDefaultFontSize(11);
        $phpWord->setDefaultParagraphStyle(
            array(
                'alignment'  => \PhpOffice\PhpWord\SimpleType\Jc::BOTH,
                'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(6),
            )
        );
        $section = $phpWord->addSection($sectionStyle);
        $sectionStyle = $section->getStyle();
        
		// half inch left margin
        $sectionStyle->setMarginLeft(\PhpOffice\PhpWord\Shared\Converter::inchToTwip(1.083));
        
		// 2 cm right margin
        $sectionStyle->setMarginRight(\PhpOffice\PhpWord\Shared\Converter::inchToTwip(.985));
		
        $header = $section->addHeader('first');
        $header->addImage(public_path('../assets/img/sphu.png', array('marginLeft' => 0, 'marginRight' => 0)),
        array('width' => 450,
            'height' => 60,
            'wrappingStyle' => 'behind'
        ));
		
        $tableStyle = array(
            'borderColor' => '000',
            'borderSize'  => 6,
            'width'  => 100
        );

        $tableStyle2 = array('width' => 100);
        $tableBorderTop = array('borderTopColor' =>'000', 'borderTopSize' => 6);
        $tableBorderRight = array('borderRightColor' =>'000', 'borderRightSize' => 6);        
		$tableBorderLeft = array('borderLeftColor' =>'000', 'borderLeftSize' => 6);        
		$tableBorderBottom = array('borderBottomColor' =>'000', 'borderBottomSize' => 6);        
		$tablewhite = array('bgColor' => 'ffffff');
		
        $text = $section->addText('Surat Keterangan Hasil',array('bold' => true, 'size'=>15), array('align'=>'center', 'spaceAfter'=>100));
        $text = $section->addText('Uji Kompetensi Penyesuaian/Inpassing',array('bold' => true, 'size'=>15), array('align'=>'center', 'spaceAfter'=>100));
        $text = $section->addText('No.________',array('bold' => true, 'size'=>15), array('align'=>'center', 'spaceAfter'=>100));
        $text = $section->addText('');
        $text = $section->addText('');
        $text = $section->addText('');
        $text = $section->addText('Peserta Uji Kompetensi Penyesuaian/Inpassing',array('bold' => true));
        
		$firstRowStyle = array('bgColor' => '808080');
        $phpWord->addTableStyle('myTable3', $tableStyle2, $firstRowStyle);
        $table3 = $section->addTable('myTable3');
        $table3->addRow();
        
		// Add cells
        $table3_1 = $table3->addCell(3000, $tablewhite);
        $table3_1->addText('Nama');
        $table3_1->addText('NIP');
        $table3_1->addText('Unit Kerja');
        $table3_2 = $table3->addCell(500, $tablewhite);
        $table3_2->addText(':');
        $table3_2->addText(':');
        $table3_2->addText(':');
        $table3_3 = $table3->addCell(5000, $tablewhite);
        $table3_3->addText($data->nama);
        $table3_3->addText($data->nip);
        $table3_3->addText($data->satuan_kerja);
        $text = $section->addText('');
        $text = $section->addText('Telah mengikuti Uji Kompetensi Penyesuaian/Inpassing untuk :',array('bold' => true));
        $firstRowStyle = array('bgColor' => '808080');
        $phpWord->addTableStyle('myTable2', $tableStyle2, $firstRowStyle);
        $table2 = $section->addTable('myTable2');
        $table2->addRow();
        
		// Add cells
        $table2_1 = $table2->addCell(3000, $tablewhite);
        $table2_1->addText('Jenjang Jabatan');
        $table2_1->addText('Metode Ujian');
        $table2_1->addText('Tanggal Uji Kompetensi');
        $table2_1->addText('Tempat Uji Kompetensi');
        $table2_2 = $table2->addCell(500, $tablewhite);
        $table2_2->addText(':');
        $table2_2->addText(':');
        $table2_2->addText(':');
        $table2_2->addText(':');
        $table2_3 = $table2->addCell(5000, $tablewhite);
        $table2_3->addText($data->jenjang);
        
		if ($data->metodes == 'tes') {
            $table2_3->addText('Tes Tertulis');
        } else {
            $table2_3->addText('Verifikasi Portofolio');
        }
		
        $table2_3->addText(Helper::tanggal_indo($data->tanggal_ujian,true));
        $table2_3->addText($data->lokasi_ujian);
        $text = $section->addText('');
        $text = $section->addText('Peserta tersebut dinyatakan TIDAK LULUS, sehingga belum dapat diterbitkan Sertifikat Kompetensi Penyesuaian/Inpassing. Selanjutnya untuk mendapatkan Sertifikat, peserta dapat mengajukan asesmen ulang untuk metode verifikasi portofolio atau melalui metode tes tertulis sesuai dengan jadwal yang tertera di https://inpassing.lkpp.go.id.');
        $text = $section->addText('');
        $text = $section->addText('Demikian surat keterangan ini dibuat untuk menjadi perhatian dan agar dapat digunakan sebagaimana semestinya.');
        $text = $section->addText('');
        $text = $section->addText('');
        $firstRowStyle = array('bgColor' => '808080');
        $phpWord->addTableStyle('myTable', $tableStyle2, $firstRowStyle);
        $table = $section->addTable('myTable');
        $table->addRow();
        
		// Add cells
        $table_1 = $table->addCell(6000, $tablewhite);
        $table_1->addText('');
        $table_3 = $table->addCell(4500, $tablewhite);
        $table_3->addText('Jakarta, '.Helper::tanggal_indo($hariini).'<w:br/>Direktur Sertifikasi Profesi selaku<w:br/>Ketua Lembaga Sertifikasi Profesi LKPP',array('bold' => false, 'size'=>11), array('align'=>'left'));
        $table_3->addText('');
        $table_3->addText('');
        $table_3->addText('Dwi Wahyuni Kartianingsih');
        $footer = $section->addFooter('first');
        $phpWord->addNumberingStyle('multilevel',
									array('type' => 'multilevel',
										  'levels' => array(array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
															array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720))
										 ));
        $footer->addText('Tembusan :', array('bold' => true, 'size'=>8));
        $footer->addText('1. Direktur Pengembangan Profesi dan Kelembagaan LKPP,', array('bold' => true, 'size'=>8,'spaceAfter'=>10));
        $footer->addText('2. Kepala BKD /Kepala Biro Kepegawaian ybs.,', array('bold' => true, 'size'=>8));
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save(storage_path('../storage/data/sktl/sktl_'.$data->nama.'.docx'));
        return response()->download(storage_path('../storage/data/sktl/sktl_'.$data->nama.'.docx'));
    }
}