<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Exports\DataPesertaExport;
use Maatwebsite\Excel\Facades\Excel;
use App\User;
use App\Peserta;
use App\PesertaJadwal;
use App\PesertaInstansi;
use App\SimpanBerkasPortofolio;
use App\JudulInput;
use App\DetailInput;
use App\DetailFileInput;
use App\Dokumen;
use App\StatusFilePortofolio;
use App\RiwayatUser;
use App\PengusulanEformasi;
use App\SuratUsulan;
use App\province;
use App\regencie;
use Carbon\Carbon;
use Redirect;
use Validator;
use View;
use Mail;
use Image;
use File;
use DB;

class PesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_admin = Auth::user()->id;
        $peserta = DB::table('pesertas')
                    ->where('id_admin_ppk', $id_admin)
                    ->where('deleted_at',null)
                    ->orderBy('id','DESC')
                    ->get();
                    
        $notifikasi = DB::table('riwayat_users')
                    ->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
                    ->join('users', 'riwayat_users.id_admin_lkpp','=','users.id')
                    ->select('riwayat_users.*','pesertas.nama as nama_peserta','users.name as nama_admin')
                    ->where('perihal','verifikasi_berkas')
                    ->where('id_admin',$id_admin)
                    ->orderBy('id','desc')
                    ->get();
                    
        $notifikasiRekomendasi = DB::table('riwayat_users')
                                ->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
                                ->join('users', 'riwayat_users.id_admin','=','users.id')
                                ->select('riwayat_users.*','pesertas.nama as nama_peserta')
                                ->whereIn('perihal',['hasil_verif_regular','hasil_verif_regular_dsp'])
                                ->where('id_admin',Auth::user()->id)    
                                ->orderBy('id','desc')
                                ->groupBy('riwayat_users.id')
                                ->offset(0)
                                ->limit(10)
                                ->get();
        
        return View::make('data_peserta_ppk', compact('peserta','notifikasi','notifikasiRekomendasi'));
    }

    public function indexLkpp()
    {
        $peserta = DB::table('pesertas')
                    ->join('users','pesertas.assign','=','users.id','left')
                    ->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
                    ->select('pesertas.*','users.name as bangprof', 'instansis.nama as nama_instansis')
                    ->orderBy('pesertas.id','desc')
                    ->where('pesertas.deleted_at',NULL)
                    ->get();
        
        $admin_bangprof = User::where('role','verifikator')->where('deleted_at',null)->pluck('name','id');
        $sites = url('verifikasi-peserta');
        return View::make('data_peserta', compact('peserta','admin_bangprof','sites'));
    }

    public function assign(Request $request)
    {
        $id_peserta = $request->input('id_peserta');
        $id_bangprof = $request->input('verifikator');
        $namapeserta = $request->input('nama_peserta1');
        $modal = $request->input('modal');
        $datauser = User::where('id',$id_bangprof)->first();
        $peserta = Peserta::find($id_peserta);
        $peserta->assign = $id_bangprof;
        if($peserta->save()){
            $from = env('MAIL_USERNAME');
            $datamail = array('verifikator' => $datauser->name, 'email' => Auth::user()->email, 'role' => Auth::user()->role,'name' => Auth::user()->name, 'from' => $from,'nama_peserta' => $namapeserta);
            $admin_superadmin = DB::table('users')->where('role','superadmin')->get();
            $admin_bangprof = DB::table('users')->where('role','bangprof')->get();
            $admin_verifikator = DB::table('users')->where('id', $id_bangprof)->get();
            
            foreach($admin_superadmin as $superadmins){
                Mail::send('mail.assign_verifikator_sp', $datamail, function($message) use ($datamail,$superadmins) {
                    $message->to($superadmins->email)->subject('Assign Verifikator');
                    $message->from($datamail['from'],$datamail['name']);
                });
            }
            
            foreach($admin_bangprof as $bangprofs){
                Mail::send('mail.assign_verifikator_bp', $datamail, function($message) use ($datamail,$bangprofs) {
                    $message->to($bangprofs->email)->subject('Assign Verifikator');
                    $message->from($datamail['from'],$datamail['name']);
                });
            }
            
            foreach($admin_verifikator as $verifikators){
                Mail::send('mail.assign_verifikator_vr', $datamail, function($message) use ($datamail,$verifikators) {
                    $message->to($verifikators->email)->subject('Assign Verifikator');
                    $message->from($datamail['from'],$datamail['name']);
                });
            }
            
            $msg = 'berhasil';
        } else {
            $msg = 'gagal';
        }

        return Redirect::back()->with('msg',$msg);        
    }

    public function unassign($id)
    {
        if(Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp'){
            return Redirect::back();
        }
        
        $peserta = Peserta::find($id);
        $peserta->assign = 'tidak';
        $peserta->save();
        return Redirect::to('data-peserta');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id_user = Auth::user()->id;
        $provinsi = province::pluck('name','id');
        $no_surat = PengusulanEformasi::where('id_admin_ppk',$id_user)
                    ->where('deleted_at',null)
                    ->pluck('no_surat_usulan_rekomendasi','id');
       
       return View::make('tambah_peserta_ppk', compact('provinsi','no_surat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'nip'    => 'unique:pesertas|required|digits_between:2,18',
            'nama'    => 'required',
            'jabatan'    => 'required',
            'tmt_panggol'    => 'required',
            'jenjang'   => 'required',
            'no_surat'   => 'required',
            'email'    => 'unique:pesertas|required',
            'no_telp'    => 'required|digits_between:2,12',
            'tempat_lahir'    => 'required',
            'tanggal_lahir'    => 'required',
            'jenis_kelamin'    => 'required',
            'no_ktp'    => 'required|numeric',
            'no_sertifikat'    => 'required|numeric',
            'pendidikan_terakhir'    => 'required',
            'provinsi'    => 'required',
            'kab_kota'    => 'required',
            'no_surat_usulan_mengikuti_inpassing' => 'required',
            'surat_bertugas_pbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'bukti_sk_pbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'ijazah_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'sk_kenaikan_pangkat_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'sk_pengangkatan_jabatan_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'sertifikasi_dasar_pbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'skp_dua_tahun_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'surat_ket_tidak_dijatuhi_hukuman' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'formulir_kesediaan_mengikuti_inpassing' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'sk_pengangkatan_cpns' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'sk_pengangkatan_pns' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'ktp' => 'mimes:jpg,png,jpeg,pdf|max:2048',
            'pas_foto_3_x_4' => 'mimes:jpg,png,jpeg|max:2048',
            'surat_pernyataan_bersedia_jfppbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            // 'bukti_portofolio' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            'surat_usulan_mengikuti_inpassing' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048'
            
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('tambah-peserta-ppk')
            ->withErrors($validator)
            ->withInput();
        }

        $id_surat = $request->input('no_surat');
        $jenjang = strtolower($request->input('jenjang'));
        $eformasi = PengusulanEformasi::where('id_admin_ppk',Auth::user()->id)
                    ->where('id',$id_surat)
                    ->first();
        $jenjang_get = $eformasi->$jenjang;
        
        $peserta_terdaftar = Peserta::where('id_admin_ppk',Auth::user()->id)
                             ->where('jenjang',$jenjang)
                             ->where('id_formasi',$id_surat)
                             ->where('status_peserta','aktif')
                             ->count();

        if($peserta_terdaftar >= $jenjang_get){
            return Redirect::to('tambah-peserta-ppk')
            ->withErrors($validator)
            ->with('msg','jenjang_penuh')
            ->withInput();
        }

        //upload 1
        if ($request->hasFile('surat_bertugas_pbj')) {
            $surat_bertugas_pbj = $request->file('surat_bertugas_pbj');
            $surat_bertugas_pbj_name = Carbon::now()->timestamp ."_surat_bertugas_pbj".".". $surat_bertugas_pbj->getClientOriginalExtension();
            $surat_bertugas_pbjPath = 'storage/data/surat_bertugas_pbj';
            $surat_bertugas_pbj->move($surat_bertugas_pbjPath,$surat_bertugas_pbj_name);
        } else {
            $surat_bertugas_pbj_name = null;
        }

        //upload 2
        if ($request->hasFile('bukti_sk_pbj')) {
            $bukti_sk_pbj = $request->file('bukti_sk_pbj');
            $bukti_sk_pbj_name = Carbon::now()->timestamp ."_bukti_sk_pbj".".". $bukti_sk_pbj->getClientOriginalExtension();
            $bukti_sk_pbjPath = 'storage/data/bukti_sk_pbj';
            $bukti_sk_pbj->move($bukti_sk_pbjPath,$bukti_sk_pbj_name);
        } else {
            $bukti_sk_pbj_name = null;
        }

        //upload 3
        if ($request->hasFile('ijazah_terakhir')) {
            $ijazah_terakhir = $request->file('ijazah_terakhir');
            $ijazah_terakhir_name = Carbon::now()->timestamp ."_ijazah_terakhir".".". $ijazah_terakhir->getClientOriginalExtension();
            $ijazah_terakhirPath = 'storage/data/ijazah_terakhir';
            $ijazah_terakhir->move($ijazah_terakhirPath,$ijazah_terakhir_name);
        } else {
            $ijazah_terakhir_name = null;
        }

        //upload 4
        if ($request->hasFile('sk_kenaikan_pangkat_terakhir')) {
            $sk_kenaikan_pangkat_terakhir = $request->file('sk_kenaikan_pangkat_terakhir');
            $sk_kenaikan_pangkat_terakhir_name = Carbon::now()->timestamp ."_sk_kenaikan_pangkat_terakhir".".". $sk_kenaikan_pangkat_terakhir->getClientOriginalExtension();
            $sk_kenaikan_pangkat_terakhirPath = 'storage/data/sk_kenaikan_pangkat_terakhir';
            $sk_kenaikan_pangkat_terakhir->move($sk_kenaikan_pangkat_terakhirPath,$sk_kenaikan_pangkat_terakhir_name);
        } else {
            $sk_kenaikan_pangkat_terakhir_name = null;
        }

        //upload 5
        if ($request->hasFile('sk_pengangkatan_jabatan_terakhir')) {
            $sk_pengangkatan_jabatan_terakhir = $request->file('sk_pengangkatan_jabatan_terakhir');
            $sk_pengangkatan_jabatan_terakhir_name = Carbon::now()->timestamp ."_sk_pengangkatan_jabatan_terakhir".".". $sk_pengangkatan_jabatan_terakhir->getClientOriginalExtension();
            $sk_pengangkatan_jabatan_terakhirPath = 'storage/data/sk_pengangkatan_jabatan_terakhir';
            $sk_pengangkatan_jabatan_terakhir->move($sk_pengangkatan_jabatan_terakhirPath,$sk_pengangkatan_jabatan_terakhir_name);
        } else {
            $sk_pengangkatan_jabatan_terakhir_name = null;
        }

        //upload 6
        if ($request->hasFile('sertifikasi_dasar_pbj')) {
            $sertifikasi_dasar_pbj = $request->file('sertifikasi_dasar_pbj');
            $sertifikasi_dasar_pbj_name = Carbon::now()->timestamp ."_sertifikasi_dasar_pbj".".". $sertifikasi_dasar_pbj->getClientOriginalExtension();
            $sertifikasi_dasar_pbjPath = 'storage/data/sertifikasi_dasar_pbj';
            $sertifikasi_dasar_pbj->move($sertifikasi_dasar_pbjPath,$sertifikasi_dasar_pbj_name);
        } else {
            $sertifikasi_dasar_pbj_name = null;
        }

        //upload 7
        if ($request->hasFile('skp_dua_tahun_terakhir')) {
            $skp_dua_tahun_terakhir = $request->file('skp_dua_tahun_terakhir');
            $skp_dua_tahun_terakhir_name = Carbon::now()->timestamp ."_skp_dua_tahun_terakhir".".". $skp_dua_tahun_terakhir->getClientOriginalExtension();
            $skp_dua_tahun_terakhirPath = 'storage/data/skp_dua_tahun_terakhir';
            $skp_dua_tahun_terakhir->move($skp_dua_tahun_terakhirPath,$skp_dua_tahun_terakhir_name);
        } else {
            $skp_dua_tahun_terakhir_name = null;
        }

        //upload 8
        if ($request->hasFile('surat_ket_tidak_dijatuhi_hukuman')) {
            $surat_ket_tidak_dijatuhi_hukuman = $request->file('surat_ket_tidak_dijatuhi_hukuman');
            $surat_ket_tidak_dijatuhi_hukuman_name = Carbon::now()->timestamp ."_surat_ket_tidak_dijatuhi_hukuman".".". $surat_ket_tidak_dijatuhi_hukuman->getClientOriginalExtension();
            $surat_ket_tidak_dijatuhi_hukumanPath = 'storage/data/surat_ket_tidak_dijatuhi_hukuman';
            $surat_ket_tidak_dijatuhi_hukuman->move($surat_ket_tidak_dijatuhi_hukumanPath,$surat_ket_tidak_dijatuhi_hukuman_name);
        } else {
            $surat_ket_tidak_dijatuhi_hukuman_name = null;
        }

        //upload 9
        if ($request->hasFile('formulir_kesediaan_mengikuti_inpassing')) {
            $formulir_kesediaan_mengikuti_inpassing = $request->file('formulir_kesediaan_mengikuti_inpassing');
            $formulir_kesediaan_mengikuti_inpassing_name = Carbon::now()->timestamp ."_formulir_kesediaan_mengikuti_inpassing".".". $formulir_kesediaan_mengikuti_inpassing->getClientOriginalExtension();
            $formulir_kesediaan_mengikuti_inpassingPath = 'storage/data/formulir_kesediaan_mengikuti_inpassing';
            $formulir_kesediaan_mengikuti_inpassing->move($formulir_kesediaan_mengikuti_inpassingPath,$formulir_kesediaan_mengikuti_inpassing_name);
        } else {
            $formulir_kesediaan_mengikuti_inpassing_name = null;
        }

        //upload 10
        if ($request->hasFile('sk_pengangkatan_cpns')) {
            $sk_pengangkatan_cpns = $request->file('sk_pengangkatan_cpns');
            $sk_pengangkatan_cpns_name = Carbon::now()->timestamp ."_sk_pengangkatan_cpns".".". $sk_pengangkatan_cpns->getClientOriginalExtension();
            $sk_pengangkatan_cpnsPath = 'storage/data/sk_pengangkatan_cpns';
            $sk_pengangkatan_cpns->move($sk_pengangkatan_cpnsPath,$sk_pengangkatan_cpns_name);
        } else {
            $sk_pengangkatan_cpns_name = null;
        }

        //upload 11
        if ($request->hasFile('sk_pengangkatan_pns')) {
            $sk_pengangkatan_pns = $request->file('sk_pengangkatan_pns');
            $sk_pengangkatan_pns_name = Carbon::now()->timestamp ."_sk_pengangkatan_pns".".". $sk_pengangkatan_pns->getClientOriginalExtension();
            $sk_pengangkatan_pnsPath = 'storage/data/sk_pengangkatan_pns';
            $sk_pengangkatan_pns->move($sk_pengangkatan_pnsPath,$sk_pengangkatan_pns_name);
        } else {
            $sk_pengangkatan_pns_name = null;
        }

        //upload 12
        if ($request->hasFile('ktp')) {
            $ktp = $request->file('ktp');
            $ktp_name = Carbon::now()->timestamp ."_ktp".".". $ktp->getClientOriginalExtension();
            $ktpPath = 'storage/data/ktp';
            $ktp->move($ktpPath,$ktp_name);
        } else {
            $ktp_name = null;
        }

        //upload 13
        if ($request->hasFile('pas_foto_3_x_4')) {
            $pas_foto_3_x_4 = $request->file('pas_foto_3_x_4');
            $pas_foto_3_x_4_name = Carbon::now()->timestamp ."_pas_foto_3_x_4".".". $pas_foto_3_x_4->getClientOriginalExtension();
            $pas_foto_3_x_4Path = 'storage/data/pas_foto_3_x_4';
            $pas_foto_3_x_4->move($pas_foto_3_x_4Path,$pas_foto_3_x_4_name);
        } else {
            $pas_foto_3_x_4_name = null;
        }

        //upload 14
        if ($request->hasFile('surat_pernyataan_bersedia_jfppbj')) {
            $surat_pernyataan_bersedia_jfppbj = $request->file('surat_pernyataan_bersedia_jfppbj');
            $surat_pernyataan_bersedia_jfppbj_name = Carbon::now()->timestamp ."_surat_pernyataan_bersedia_jfppbj".".". $surat_pernyataan_bersedia_jfppbj->getClientOriginalExtension();
            $surat_pernyataan_bersedia_jfppbjPath = 'storage/data/surat_pernyataan_bersedia_jfppbj';
            $surat_pernyataan_bersedia_jfppbj->move($surat_pernyataan_bersedia_jfppbjPath,$surat_pernyataan_bersedia_jfppbj_name);
        } else {
            $surat_pernyataan_bersedia_jfppbj_name = null;
        }

        //upload 15
        // if ($request->hasFile('bukti_portofolio')) {
        // $bukti_portofolio = $request->file('bukti_portofolio');
        // $bukti_portofolio_name = Carbon::now()->timestamp ."_bukti_portofolio".".". $bukti_portofolio->getClientOriginalExtension();
        // $bukti_portofolioPath = 'storage/data/bukti_portofolio';
        // $bukti_portofolio->move($bukti_portofolioPath,$bukti_portofolio_name);
        // }else{
        //     $bukti_portofolio_name = null;
        // }

        //upload 16
        if ($request->hasFile('surat_usulan_mengikuti_inpassing')) {
            // Menuju Directory Tabel Dokumen
            $surat_usulan_mengikuti_inpassing = $request->file('surat_usulan_mengikuti_inpassing');
            $surat_usulan_mengikuti_inpassing_name = Carbon::now()->timestamp ."_surat_usulan_mengikuti_inpassing".".". $surat_usulan_mengikuti_inpassing->getClientOriginalExtension();
            $surat_usulan_mengikuti_inpassingPath = 'storage/data/surat_usulan_mengikuti_inpassing';
            $surat_usulan_mengikuti_inpassing->move($surat_usulan_mengikuti_inpassingPath,$surat_usulan_mengikuti_inpassing_name);
        } else {
            $surat_usulan_mengikuti_inpassing_name = null;
        }

        $dokumen = new Dokumen();
        $dokumen->surat_bertugas_pbj = $surat_bertugas_pbj_name;
        $dokumen->bukti_sk_pbj = $bukti_sk_pbj_name;
        $dokumen->ijazah_terakhir = $ijazah_terakhir_name;
        $dokumen->sk_kenaikan_pangkat_terakhir = $sk_kenaikan_pangkat_terakhir_name;
        $dokumen->sk_pengangkatan_jabatan_terakhir = $sk_pengangkatan_jabatan_terakhir_name;
        $dokumen->sertifikasi_dasar_pbj = $sertifikasi_dasar_pbj_name;
        $dokumen->skp_dua_tahun_terakhir = $skp_dua_tahun_terakhir_name;
        $dokumen->surat_ket_tidak_dijatuhi_hukuman = $surat_ket_tidak_dijatuhi_hukuman_name;
        $dokumen->formulir_kesediaan_mengikuti_inpassing = $formulir_kesediaan_mengikuti_inpassing_name;
        $dokumen->sk_pengangkatan_cpns = $sk_pengangkatan_cpns_name;
        $dokumen->sk_pengangkatan_pns = $sk_pengangkatan_pns_name;
        $dokumen->ktp = $ktp_name;
        $dokumen->pas_foto_3_x_4 = $pas_foto_3_x_4_name;
        $dokumen->surat_pernyataan_bersedia_jfppbj = $surat_pernyataan_bersedia_jfppbj_name;
        // $dokumen->bukti_portofolio = $bukti_portofolio_name;
        $dokumen->surat_usulan_mengikuti_inpassing = $surat_usulan_mengikuti_inpassing_name;

        if($dokumen->save()){
            $id_dokumen = $dokumen->id;
        } else {
            $id_dokumen = '0';
        }

        $peserta = new Peserta();
        $peserta->id_admin_ppk = Auth::user()->id;
        $peserta->nama = $request->input('nama');
        $peserta->nip = $request->input('nip');
        $peserta->jabatan = $request->input('jabatan');
        $tmt_panggol = explode('/',$request->input('tmt_panggol'));
        $peserta->tmt_panggol = $tmt_panggol[2].'-'.$tmt_panggol[1].'-'.$tmt_panggol[0];
        $peserta->jenjang = $request->input('jenjang');
        $peserta->nama_instansi = Auth::user()->nama_instansi;
        $peserta->email = $request->input('email');
        $peserta->no_telp = $request->input('no_telp');
        $peserta->tempat_lahir = $request->input('tempat_lahir');
        $peserta->no_surat_usulan_peserta = $request->input('no_surat_usulan_mengikuti_inpassing');
        $tanggal_lahir = explode('/',$request->input('tanggal_lahir'));
        $peserta->tanggal_lahir = $tanggal_lahir[2].'-'.$tanggal_lahir[1].'-'.$tanggal_lahir[0];

        //$peserta->tanggal_lahir = $request->input('tanggal_lahir');
        $peserta->jenis_kelamin = $request->input('jenis_kelamin');
        $peserta->no_ktp = $request->input('no_ktp');
        $peserta->no_sertifikat = $request->input('no_sertifikat');
        $peserta->pendidikan_terakhir = $request->input('pendidikan_terakhir');
        $peserta->satuan_kerja = $request->input('satuan_kerja');
        $peserta->provinsi = $request->input('provinsi');
        $peserta->kab_kota = $request->input('kab_kota');
        $peserta->status = 'Menunggu Verifikasi Dokumen Persyaratan';
        $peserta->status_peserta = 'aktif';
        $peserta->id_dokumen = $id_dokumen;
        $peserta->id_formasi = $request->input('no_surat');

        if($peserta->save()){
            $updatedokumen = Dokumen::find($id_dokumen);
            $updatedokumen->id_peserta = $peserta->id;
            $updatedokumen->save();

            if($request->hasFile('surat_usulan_mengikuti_inpassing')){
                // Menuju Directory Tabel Surat usulan
                $surat_usulan_mengikuti_inpassing_more = $request->file('surat_usulan_mengikuti_inpassing');
                $surat_usulan_mengikuti_inpassing_name_more = Carbon::now()->timestamp ."_surat_usulan_peserta".".". $surat_usulan_mengikuti_inpassing_more->getClientOriginalExtension();
                $surat_usulan_mengikuti_inpassingPath_more = 'storage/data/surat_usulan_inpassing';
                copy($surat_usulan_mengikuti_inpassingPath.'/'.$surat_usulan_mengikuti_inpassing_name, $surat_usulan_mengikuti_inpassingPath_more.'/'.$surat_usulan_mengikuti_inpassing_name_more);
                // $surat_usulan_mengikuti_inpassing_more->move($surat_usulan_mengikuti_inpassingPath,$surat_usulan_mengikuti_inpassing_name, $surat_usulan_mengikuti_inpassingPath_more.$surat_usulan_mengikuti_inpassing_name_more);
            } else {
                $surat_usulan_mengikuti_inpassing_name_more = null;            
            }

            $dokumenUsulan = new SuratUsulan();
            $dokumenUsulan->file =  $surat_usulan_mengikuti_inpassing_name_more;
            $dokumenUsulan->id_peserta =  $peserta->id;
            $dokumenUsulan->id_admin_ppk =  Auth::user()->id;
            $dokumenUsulan->no_surat_usulan_peserta = $request->input('no_surat_usulan_mengikuti_inpassing');
            $dokumenUsulan->save();

            $riwayat = new RiwayatUser();
            $riwayat->id_user = $peserta->id;
            $riwayat->id_admin = Auth::user()->id;
            $riwayat->perihal = "peserta";
            $riwayat->description = "didaftarkan sebagai peserta oleh";
            $riwayat->tanggal = Carbon::now()->toDateString(); 
            $riwayat->save();
            $msg = "berhasil";
        } else {
            $msg = "gagal";
        }

        return Redirect::to('data-peserta-ppk')->with('msg',$msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peserta = DB::table('pesertas')
                    ->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id')
                    ->join('peserta_jadwals', 'pesertas.id', '=', 'peserta_jadwals.id_peserta','left')
                    ->join('peserta_instansis', 'pesertas.id', '=', 'peserta_instansis.id_peserta','left')
                    ->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
                    ->join('provinces', 'pesertas.provinsi','=','provinces.id','left')
                    ->join('pengusulan_eformasis','pesertas.id_formasi','=','pengusulan_eformasis.id','left')
                    ->join('regencies', 'pesertas.kab_kota','=','regencies.id','left')
                    ->select('pesertas.*', 'dokumens.*','provinces.name as provinsis','regencies.name as kotas','pengusulan_eformasis.no_surat_usulan_rekomendasi as no_surats','instansis.nama as instansis','peserta_jadwals.id_jadwal as jadwals_id','peserta_instansis.id_jadwal as jadwal_instansis_id')
                    ->where('pesertas.id',$id)
                    ->first();
        
        $dataSuratUsulan = DB::table('surat_usulans')
                           ->where('id_peserta',$id)
                           ->orderBy('id','desc')
                           ->first();

        $status = DB::table('status_peserta_inpassings')
                  ->WhereIn('id', [1,2,3])
                  ->pluck('status','id');

        return View::make('assign_data_peserta', compact('peserta','status','dataSuratUsulan'));
    }

    public function detail($id)
    {
        $data = DB::table('pesertas')
                ->join('dokumens','pesertas.id','=','dokumens.id_peserta')
                ->join('instansis','pesertas.nama_instansi','=','instansis.id')
                ->join('pengusulan_eformasis','pesertas.id_formasi','=','pengusulan_eformasis.id','left')
                ->join('provinces','pesertas.provinsi','=','provinces.id','left')
                ->join('regencies','pesertas.kab_kota','=','regencies.id','left')
                ->select('pesertas.*','dokumens.*','instansis.nama as instansis','pengusulan_eformasis.no_surat_usulan_rekomendasi as no_surats','provinces.name as provinsis','regencies.name as kotas')
                ->where('pesertas.id',$id)
                ->first();
                
        $dataSuratUsulan = DB::table('surat_usulans')
                           ->where('id_peserta',$id)
                           ->orderBy('id','desc')
                           ->first();
        
        return View::make('detail_peserta', compact('data','dataSuratUsulan'));
    }

    public function detailPesertaLkpp1($id,$id_jadwal)
    {
        $data = DB::table('pesertas')
                ->join('dokumens','pesertas.id','=','dokumens.id_peserta')
                ->join('instansis','pesertas.nama_instansi','=','instansis.id')
                ->join('pengusulan_eformasis','pesertas.id_formasi','=','pengusulan_eformasis.id','left')
                ->join('provinces','pesertas.provinsi','=','provinces.id','left')
                ->join('regencies','pesertas.kab_kota','=','regencies.id','left')
                ->select('pesertas.*','dokumens.*','instansis.nama as instansis','pengusulan_eformasis.no_surat_usulan_rekomendasi as no_surats','provinces.name as provinsis','regencies.name as kotas')
                ->where('pesertas.id',$id)
                ->first();

        $dataSuratUsulan = DB::table('surat_usulans')
                            ->where('id_peserta',$id)
                            ->orderBy('id','desc')
                            ->first();

        $dataFile = DB::table('peserta_jadwals')
                ->join('pesertas','peserta_jadwals.id_peserta','=','pesertas.id')
                ->join('dokumens', 'pesertas.id_dokumen','=','dokumens.id','left')
                ->join('jadwals', 'peserta_jadwals.id_jadwal','=','jadwals.id','left')
                ->join('users as asesor', 'pesertas.asesor','=','asesor.id','left')
                ->select('peserta_jadwals.*','pesertas.*','dokumens.*','jadwals.*','jadwals.tanggal_ujian as tgl_ujian','asesor.name as asesors','pesertas.nama as namas','pesertas.status_inpassing as statuss')
                    ->where('peserta_jadwals.id',$id_jadwal)
                    ->first();

        $dokumen = SimpanBerkasPortofolio::where('id_peserta',$dataFile->id_peserta)->where('id_jadwal',$dataFile->id_jadwal)->get();


        $judul_input = JudulInput::where('jenjang',$dataFile->jenjang)->get();
        $detail_input = DetailInput::all();
        $input_form = DetailFileInput::all();
        $status_file = StatusFilePortofolio::where('id_peserta',$dataFile->id_peserta)
                        ->where('id_jadwal',$dataFile->id_jadwal)
                        ->where('id_admin_ppk',$dataFile->id_admin_ppk)
                        ->get();
        
        return View::make('detail_peserta_ppk1', compact('data','dataFile','dokumen','judul_input','detail_input','input_form','status_file','dataSuratUsulan'));
    }

    public function detailPesertaLkpp2($id,$id_jadwal)
    {
        $data = DB::table('pesertas')
                ->join('dokumens','pesertas.id','=','dokumens.id_peserta')
                ->join('instansis','pesertas.nama_instansi','=','instansis.id')
                ->join('pengusulan_eformasis','pesertas.id_formasi','=','pengusulan_eformasis.id','left')
                ->join('provinces','pesertas.provinsi','=','provinces.id','left')
                ->join('regencies','pesertas.kab_kota','=','regencies.id','left')
                ->select('pesertas.*','dokumens.*','instansis.nama as instansis','pengusulan_eformasis.no_surat_usulan_rekomendasi as no_surats','provinces.name as provinsis','regencies.name as kotas')
                ->where('pesertas.id',$id)
                ->first();

        $dataFile = DB::table('peserta_instansis')
                    ->join('pesertas','peserta_instansis.id_peserta','=','pesertas.id')
                    ->join('dokumens', 'peserta_instansis.id_peserta','=','dokumens.id','left')
                    ->join('users as asesor', 'pesertas.asesor','=','asesor.id','left')
                    ->select('peserta_instansis.*','pesertas.*','dokumens.*','asesor.name as asesors','pesertas.nama as namas')
                    ->where('peserta_instansis.id',$id_jadwal)
                    ->first();

        $dokumen = SimpanBerkasPortofolio::where('id_peserta',$dataFile->id_peserta)->get();
        $judul_input = JudulInput::where('jenjang',$dataFile->jenjang)->get();
        $detail_input = DetailInput::all();
        $input_form = DetailFileInput::all();
        $status_file = StatusFilePortofolio::where('id_peserta',$dataFile->id_peserta)
                        ->where('id_jadwal',$dataFile->id_jadwal)
                        ->get();
        
        return View::make('detail_peserta_ppk2', compact('data','dataFile','dokumen','judul_input','detail_input','input_form','status_file'));
    }

    public function detailPesertaLkpp($id)
    {
        $data = DB::table('pesertas')
                ->join('dokumens','pesertas.id','=','dokumens.id_peserta')
                ->join('instansis','pesertas.nama_instansi','=','instansis.id')
                ->join('pengusulan_eformasis','pesertas.id_formasi','=','pengusulan_eformasis.id','left')
                ->join('provinces','pesertas.provinsi','=','provinces.id','left')
                ->join('regencies','pesertas.kab_kota','=','regencies.id','left')
                ->select('pesertas.*','dokumens.*','instansis.nama as instansis','pengusulan_eformasis.no_surat_usulan_rekomendasi as no_surats','provinces.name as provinsis','regencies.name as kotas')
                ->where('pesertas.id',$id)
                ->first();
                
        $dataSuratUsulan = DB::table('surat_usulans')
                            ->where('id_peserta',$id)
                            ->orderBy('id','desc')
                            ->first();

        return View::make('detail_peserta_ppk', compact('data','dataSuratUsulan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id_user = Auth::user()->id;
        $provinsi = province::pluck('name','id');
        
        $data = DB::table('pesertas')
                ->join('dokumens','pesertas.id','=','dokumens.id_peserta')
                ->select('pesertas.*','dokumens.*')
                ->where('pesertas.id',$id)
                ->first();
        
        $suratusulan = DB::table('surat_usulans')
                        ->select('surat_usulans.id as id_usulan')
                        ->where('id_peserta',$id)
                        ->orderBy('id','desc')
                        ->first();
                        
        $no_surat = PengusulanEformasi::where('id_admin_ppk',$id_user)
                    ->where('deleted_at',null)
                    ->pluck('no_surat_usulan_rekomendasi','id');

        $regencies = regencie::pluck('name','id');
        return View::make('edit_peserta', compact('data','provinsi','regencies','no_surat','suratusulan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {    
        $sekarang = date('Y-m-d');
        $cek_terdaftar = DB::table('peserta_jadwals')
                         ->join('jadwals','jadwals.id','=','peserta_jadwals.id_jadwal')
                         ->select('jadwals.*','peserta_jadwals.*')
                         ->where('id_peserta',$id)
                         ->where('status',1)
                         ->where('publish',null)
                         ->orderBy('peserta_jadwals.id','desc')
                         ->first();
        
        $cek_terdaftar_int = DB::table('peserta_instansis')
                            ->join('jadwal_instansis','jadwal_instansis.id','=','peserta_instansis.id_jadwal')
                            ->select('jadwal_instansis.*','peserta_instansis.*')
                            ->where('id_peserta',$id)
                            ->where('status',1)
                            ->where('publish',null)
                            ->orderBy('peserta_instansis.id','desc')
                            ->first();

        $peserta = Peserta::find($id);
        if(Auth::user()->role == 'dsp'){
            return Redirect::back();
        }
        
        if(Auth::user()->role == 'bangprof'){
            if($peserta->assign == 'tidak'){
                return Redirect::back();
            }
        }
        
        $verifikasi_berkas = $request->input('verifikasi_berkas');
        if ($verifikasi_berkas == 'verified'){
            if($cek_terdaftar){
                if (strtotime($cek_terdaftar->tanggal_ujian) > $sekarang) {
                    if($cek_terdaftar->metode_ujian == 'tes'){
                        $status = DB::table('status_peserta_inpassings')->find(13); 
                        $status_inpassing = 13;
                    } elseif($cek_terdaftar->metode_ujian == 'verifikasi') {
                        $status = DB::table('status_peserta_inpassings')->find(12); 
                        $status_inpassing = 12;
                    } else {
                        $status = DB::table('status_peserta_inpassings')->find(1);
                        $status_inpassing = 1;
                    }
                }
            } elseif($cek_terdaftar_int){
                if (strtotime($cek_terdaftar_int->tanggal_ujian) > $sekarang) {
                    if($cek_terdaftar_int->metode_ujian == 'tes' ){
                        $status = DB::table('status_peserta_inpassings')->find(13); 
                        $status_inpassing = 13;
                    } else {
                        $status = DB::table('status_peserta_inpassings')->find(1);
                        $status_inpassing = 1;
                    }
                }
            } else {
                $status = DB::table('status_peserta_inpassings')->find(3); 
                $status_inpassing = 3;
            }
        }
        
        if ($verifikasi_berkas == 'not_verified'){
            if($cek_terdaftar){
                if($cek_terdaftar->metode_ujian == 'tes'){
                    $status = DB::table('status_peserta_inpassings')->find(2); 
                    $status_inpassing = 2;
                } elseif($cek_terdaftar->metode_ujian == 'verifikasi') {
                    $status = DB::table('status_peserta_inpassings')->find(2); 
                    $status_inpassing = 2;
                } else {
                    $status = DB::table('status_peserta_inpassings')->find(1);
                    $status_inpassing = 1;
                }
            } elseif($cek_terdaftar_int) {
                if($cek_terdaftar_int->metode_ujian == 'tes'){
                    $status = DB::table('status_peserta_inpassings')->find(2); 
                    $status_inpassing = 2;
                } else {
                    $status = DB::table('status_peserta_inpassings')->find(1);
                    $status_inpassing = 1;
                }
            } else {
                $status = DB::table('status_peserta_inpassings')->find(2); 
                $status_inpassing = 2;
            }
        }
        
        if ($verifikasi_berkas == ''){
            $status = DB::table('status_peserta_inpassings')->find(1);
            $status_inpassing = 1;
        }
        
        $peserta->status = $status->status;
        $peserta->status_inpassing = $status_inpassing;
        $peserta->catatan = $request->input('catatan');
        $peserta->verifikasi_berkas = $request->input('verifikasi_berkas');
        $id_status = $status_inpassing;
        
        if($peserta->save()){
            $riwayat = new RiwayatUser();
            $riwayat->id_user = $peserta->id;
            $riwayat->id_admin_lkpp = Auth::user()->id;
            $riwayat->id_admin = $peserta->id_admin_ppk;
            $riwayat->perihal = "verifikasi_berkas";
            if($id_status == 1){    
                $des = 'diubah Statusnya menjadi "Menunggu Verifikasi Dokumen Persyaratan" oleh';
                $psn_mail = "Menunggu Verifikasi Dokumen Persyaratan";
            }
            
            if($id_status == 2){
                $des = 'diubah Statusnya menjadi "Dokumen Persyaratan Tidak Lengkap" oleh';
                $psn_mail = "Dokumen Persyaratan Tidak Lengkap";
            }
            
            if($id_status == 3){
                $des = 'diubah Statusnya menjadi "Dokumen Persyaratan Lengkap" oleh';
                $psn_mail = "Dokumen Persyaratan Lengkap";
            }
            
            if($id_status == 13){
                $des = 'diubah Statusnya menjadi "Menunggu Tes Tertulis" oleh';
                $psn_mail = "Menunggu Tes Tertulis";
            }
            
            if($id_status == 12){
                $des = 'diubah Statusnya menjadi "Menunggu Verifikasi Portofolio" oleh';
                $psn_mail = "Menunggu Verifikasi Portofolio";
            }
            
            if($id_status == ""){
                $des = 'diubah Statusnya menjadi Default oleh';
                $psn_mail = "Default";
            }
            
            $riwayat->keterangan = $request->input('catatan');
            $riwayat->description = $des;
            $riwayat->tanggal = Carbon::now()->toDateString(); 
            $riwayat->save();
            $from = env('MAIL_USERNAME');
            $admin_ppk = User::find($peserta->id_admin_ppk);
            $data = array('pesan' => $psn_mail, 'status' => $verifikasi_berkas,'email' => $admin_ppk->email, 'name' => $admin_ppk->name, 'nama_peserta' => $peserta->nama, 'from' => $from,'catatan' => $request->input('catatan'));
            Mail::send('mail.status_peserta', $data, function($message) use ($data) {
                $message->to($data['email'], $data['name'])->subject('Status Peserta Terdaftar');
                $message->from($data['from'],'LKPP');
            });
            
            if(Auth::user()->role == 'asesor'){
                $redirect = "verifikasi-peserta/".$id;
            } else {
                $redirect = "data-peserta";
            }
            return Redirect::to($redirect)->with('msg','berhasil');
        } else {
            return Redirect::to($redirect)->with('msg','gagal');
        }
    }

    public function InputUlang($id){
        $id_user = Auth::user()->id;
        $provinsi = province::pluck('name','id');
        $data = DB::table('pesertas')
                ->join('dokumens','pesertas.id','=','dokumens.id_peserta')
                ->select('pesertas.*','dokumens.*')
                ->where('pesertas.id',$id)
                ->first();

        $suratusulan = DB::table('surat_usulans')
                       ->select('surat_usulans.id as id_usulan')
                       ->where('id_peserta',$id)
                       ->orderBy('id','desc')
                       ->first();
        
        $no_surat = PengusulanEformasi::where('id_admin_ppk',$id_user)
                    ->where('deleted_at',null)
                    ->pluck('no_surat_usulan_rekomendasi','id');
        
        $regencies = regencie::pluck('name','id');
        return View::make('ulang_berkas', compact('data','provinsi','regencies','no_surat','suratusulan'));
    }
    
    public function StoreInputUlang(Request $request,$id){
        $rules = array('nip' => 'required|digits_between:2,18',
                       'nama' => 'required',
                       'jabatan' => 'required',
                       'jenjang' => 'required',
                       'email' => 'required',
                       'no_telp' => 'required',
                       'tempat_lahir' => 'required',
                       'tanggal_lahir' => 'required',
                       'jenis_kelamin' => 'required',
                       'no_ktp' => 'required|numeric',
                       'no_sertifikat' => 'required|numeric',
                       'pendidikan_terakhir' => 'required',
                       'provinsi' => 'required',
                       'kab_kota' => 'required',
                       'no_surat_usulan_mengikuti_inpassing' => 'required',
                       'surat_bertugas_pbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'bukti_sk_pbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'ijazah_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'sk_kenaikan_pangkat_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'sk_pengangkatan_jabatan_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'sertifikasi_dasar_pbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'skp_dua_tahun_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'surat_ket_tidak_dijatuhi_hukuman' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'formulir_kesediaan_mengikuti_inpassing' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'sk_pengangkatan_cpns' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'sk_pengangkatan_pns' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'ktp' => 'mimes:jpg,png,jpeg,pdf|max:2048',
                       'pas_foto_3_x_4' => 'mimes:jpg,png,jpeg,pdf|max:2048',
                       'surat_pernyataan_bersedia_jfppbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'surat_usulan_mengikuti_inpassing' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048');
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
        }
        

        $id_surat = $request->input('no_surat');
        $jenjang = strtolower($request->input('jenjang'));
        $eformasi = PengusulanEformasi::where('id_admin_ppk',Auth::user()->id)
                    ->where('id',$id_surat)
                    ->first();
        $jenjang_get = $eformasi->$jenjang;
        $peserta_terdaftar = Peserta::where('id_admin_ppk',Auth::user()->id)
                            ->where('jenjang',$jenjang)
                            ->where('id_formasi',$id_surat)
                            ->where('status_peserta','aktif')
                            ->whereNotIn('id',[$id])
                            ->count();
        
        if($peserta_terdaftar >= $jenjang_get){
            return Redirect::to('edit-peserta/'.$id)
                    ->withErrors($validator)
                    ->with('msg','jenjang_penuh')
                    ->withInput();
        }

        //upload 1
        if ($request->hasFile('surat_bertugas_pbj')) {
            $surat_bertugas_pbj = $request->file('surat_bertugas_pbj');
            $surat_bertugas_pbj_name = Carbon::now()->timestamp ."_surat_bertugas_pbj".".". $surat_bertugas_pbj->getClientOriginalExtension();
            $surat_bertugas_pbjPath = 'storage/data/surat_bertugas_pbj';
            $surat_bertugas_pbj->move($surat_bertugas_pbjPath,$surat_bertugas_pbj_name);
            $old_surat_bertugas_pbj = $request->input('old_surat_bertugas_pbj');
            if($old_surat_bertugas_pbj != ""){
                unlink('storage/data/surat_bertugas_pbj/'.$old_surat_bertugas_pbj);
            }
        } else {
            $surat_bertugas_pbj_name = $request->input('old_surat_bertugas_pbj');
        }

        //upload 2
        if ($request->hasFile('bukti_sk_pbj')) {
            $bukti_sk_pbj = $request->file('bukti_sk_pbj');
            $bukti_sk_pbj_name = Carbon::now()->timestamp ."_bukti_sk_pbj".".". $bukti_sk_pbj->getClientOriginalExtension();
            $bukti_sk_pbjPath = 'storage/data/bukti_sk_pbj';
            $bukti_sk_pbj->move($bukti_sk_pbjPath,$bukti_sk_pbj_name);
            $old_bukti_sk_pbj = $request->input('old_bukti_sk_pbj');
            if($old_bukti_sk_pbj != ""){
                unlink('storage/data/bukti_sk_pbj/'.$old_bukti_sk_pbj);
            }
        } else {
            $bukti_sk_pbj_name = $request->input('old_bukti_sk_pbj');
        }

        //upload 3
        if ($request->hasFile('ijazah_terakhir')) {
            $ijazah_terakhir = $request->file('ijazah_terakhir');
            $ijazah_terakhir_name = Carbon::now()->timestamp ."_ijazah_terakhir".".". $ijazah_terakhir->getClientOriginalExtension();
            $ijazah_terakhirPath = 'storage/data/ijazah_terakhir';
            $ijazah_terakhir->move($ijazah_terakhirPath,$ijazah_terakhir_name);
            $old_ijazah_terakhir = $request->input('old_ijazah_terakhir');
            if($old_ijazah_terakhir != ""){
                unlink('storage/data/ijazah_terakhir/'.$old_ijazah_terakhir);
            }
        } else {
            $ijazah_terakhir_name = $request->input('old_ijazah_terakhir');
        }

        //upload 4
        if ($request->hasFile('sk_kenaikan_pangkat_terakhir')) {
            $sk_kenaikan_pangkat_terakhir = $request->file('sk_kenaikan_pangkat_terakhir');
            $sk_kenaikan_pangkat_terakhir_name = Carbon::now()->timestamp ."_sk_kenaikan_pangkat_terakhir".".". $sk_kenaikan_pangkat_terakhir->getClientOriginalExtension();
            $sk_kenaikan_pangkat_terakhirPath = 'storage/data/sk_kenaikan_pangkat_terakhir';
            $sk_kenaikan_pangkat_terakhir->move($sk_kenaikan_pangkat_terakhirPath,$sk_kenaikan_pangkat_terakhir_name);
            $old_sk_kenaikan_pangkat_terakhir = $request->input('old_sk_kenaikan_pangkat_terakhir');
            if($old_sk_kenaikan_pangkat_terakhir != ""){
                unlink('storage/data/sk_kenaikan_pangkat_terakhir/'.$old_sk_kenaikan_pangkat_terakhir);
            }
        } else {
            $sk_kenaikan_pangkat_terakhir_name = $request->input('old_sk_kenaikan_pangkat_terakhir');
        }

        //upload 5
        if ($request->hasFile('sk_pengangkatan_jabatan_terakhir')) {
            $sk_pengangkatan_jabatan_terakhir = $request->file('sk_pengangkatan_jabatan_terakhir');
            $sk_pengangkatan_jabatan_terakhir_name = Carbon::now()->timestamp ."_sk_pengangkatan_jabatan_terakhir".".". $sk_pengangkatan_jabatan_terakhir->getClientOriginalExtension();
            $sk_pengangkatan_jabatan_terakhirPath = 'storage/data/sk_pengangkatan_jabatan_terakhir';
            $sk_pengangkatan_jabatan_terakhir->move($sk_pengangkatan_jabatan_terakhirPath,$sk_pengangkatan_jabatan_terakhir_name);
            $old_sk_pengangkatan_jabatan_terakhir = $request->input('old_sk_pengangkatan_jabatan_terakhir');
            if($old_sk_pengangkatan_jabatan_terakhir != ""){
                unlink('storage/data/sk_pengangkatan_jabatan_terakhir/'.$old_sk_pengangkatan_jabatan_terakhir);
            }
        } else {
            $sk_pengangkatan_jabatan_terakhir_name = $request->input('old_sk_pengangkatan_jabatan_terakhir');
        }

        //upload 6
        if ($request->hasFile('sertifikasi_dasar_pbj')) {
            $sertifikasi_dasar_pbj = $request->file('sertifikasi_dasar_pbj');
            $sertifikasi_dasar_pbj_name = Carbon::now()->timestamp ."_sertifikasi_dasar_pbj".".". $sertifikasi_dasar_pbj->getClientOriginalExtension();
            $sertifikasi_dasar_pbjPath = 'storage/data/sertifikasi_dasar_pbj';
            $sertifikasi_dasar_pbj->move($sertifikasi_dasar_pbjPath,$sertifikasi_dasar_pbj_name);
            $old_sertifikasi_dasar_pbj = $request->input('old_sertifikasi_dasar_pbj');
            if($old_sertifikasi_dasar_pbj != ""){
                unlink('storage/data/sertifikasi_dasar_pbj/'.$old_sertifikasi_dasar_pbj);
            }
        } else {
            $sertifikasi_dasar_pbj_name = $request->input('old_sertifikasi_dasar_pbj');
        }

        //upload 7
        if ($request->hasFile('skp_dua_tahun_terakhir')) {
            $skp_dua_tahun_terakhir = $request->file('skp_dua_tahun_terakhir');
            $skp_dua_tahun_terakhir_name = Carbon::now()->timestamp ."_skp_dua_tahun_terakhir".".". $skp_dua_tahun_terakhir->getClientOriginalExtension();
            $skp_dua_tahun_terakhirPath = 'storage/data/skp_dua_tahun_terakhir';
            $skp_dua_tahun_terakhir->move($skp_dua_tahun_terakhirPath,$skp_dua_tahun_terakhir_name);
            $old_skp_dua_tahun_terakhir = $request->input('old_skp_dua_tahun_terakhir');
            if($old_skp_dua_tahun_terakhir != ""){
                unlink('storage/data/skp_dua_tahun_terakhir/'.$old_skp_dua_tahun_terakhir);
            }
        } else {
            $skp_dua_tahun_terakhir_name = $request->input('old_skp_dua_tahun_terakhir');
        }

        //upload 8
        if ($request->hasFile('surat_ket_tidak_dijatuhi_hukuman')) {
            $surat_ket_tidak_dijatuhi_hukuman = $request->file('surat_ket_tidak_dijatuhi_hukuman');
            $surat_ket_tidak_dijatuhi_hukuman_name = Carbon::now()->timestamp ."_surat_ket_tidak_dijatuhi_hukuman".".". $surat_ket_tidak_dijatuhi_hukuman->getClientOriginalExtension();
            $surat_ket_tidak_dijatuhi_hukumanPath = 'storage/data/surat_ket_tidak_dijatuhi_hukuman';
            $surat_ket_tidak_dijatuhi_hukuman->move($surat_ket_tidak_dijatuhi_hukumanPath,$surat_ket_tidak_dijatuhi_hukuman_name);
            $old_surat_ket_tidak_dijatuhi_hukuman = $request->input('old_surat_ket_tidak_dijatuhi_hukuman');
            if($old_surat_ket_tidak_dijatuhi_hukuman != ""){
                unlink('storage/data/surat_ket_tidak_dijatuhi_hukuman/'.$old_surat_ket_tidak_dijatuhi_hukuman);
            }
        } else {
            $surat_ket_tidak_dijatuhi_hukuman_name = $request->input('old_surat_ket_tidak_dijatuhi_hukuman');
        }

        //upload 9
        if ($request->hasFile('formulir_kesediaan_mengikuti_inpassing')) {
            $formulir_kesediaan_mengikuti_inpassing = $request->file('formulir_kesediaan_mengikuti_inpassing');
            $formulir_kesediaan_mengikuti_inpassing_name = Carbon::now()->timestamp ."_formulir_kesediaan_mengikuti_inpassing".".". $formulir_kesediaan_mengikuti_inpassing->getClientOriginalExtension();
            $formulir_kesediaan_mengikuti_inpassingPath = 'storage/data/formulir_kesediaan_mengikuti_inpassing';
            $formulir_kesediaan_mengikuti_inpassing->move($formulir_kesediaan_mengikuti_inpassingPath,$formulir_kesediaan_mengikuti_inpassing_name);
            $old_formulir_kesediaan_mengikuti_inpassing = $request->input('old_formulir_kesediaan_mengikuti_inpassing');
            if($old_formulir_kesediaan_mengikuti_inpassing != ""){
                unlink('storage/data/formulir_kesediaan_mengikuti_inpassing/'.$old_formulir_kesediaan_mengikuti_inpassing);
            }
        } else {
            $formulir_kesediaan_mengikuti_inpassing_name = $request->input('old_formulir_kesediaan_mengikuti_inpassing');
        }

        //upload 10
        if ($request->hasFile('sk_pengangkatan_cpns')) {
            $sk_pengangkatan_cpns = $request->file('sk_pengangkatan_cpns');
            $sk_pengangkatan_cpns_name = Carbon::now()->timestamp ."_sk_pengangkatan_cpns".".". $sk_pengangkatan_cpns->getClientOriginalExtension();
            $sk_pengangkatan_cpnsPath = 'storage/data/sk_pengangkatan_cpns';
            $sk_pengangkatan_cpns->move($sk_pengangkatan_cpnsPath,$sk_pengangkatan_cpns_name);
            $old_sk_pengangkatan_cpns = $request->input('old_sk_pengangkatan_cpns');
            if($old_sk_pengangkatan_cpns != ""){
                unlink('storage/data/sk_pengangkatan_cpns/'.$old_sk_pengangkatan_cpns);
            }
        } else {
            $sk_pengangkatan_cpns_name = $request->input('old_sk_pengangkatan_cpns');
        }

        //upload 11
        if ($request->hasFile('sk_pengangkatan_pns')) {
            $sk_pengangkatan_pns = $request->file('sk_pengangkatan_pns');
            $sk_pengangkatan_pns_name = Carbon::now()->timestamp ."_sk_pengangkatan_pns".".". $sk_pengangkatan_pns->getClientOriginalExtension();
            $sk_pengangkatan_pnsPath = 'storage/data/sk_pengangkatan_pns';
            $sk_pengangkatan_pns->move($sk_pengangkatan_pnsPath,$sk_pengangkatan_pns_name);
            $old_sk_pengangkatan_pns = $request->input('old_sk_pengangkatan_pns');
            if($old_sk_pengangkatan_pns != ""){
                unlink('storage/data/sk_pengangkatan_pns/'.$old_sk_pengangkatan_pns);
            }
        } else {
            $sk_pengangkatan_pns_name = $request->input('old_sk_pengangkatan_pns');
        }

        //upload 12
        if ($request->hasFile('ktp')) {
            $ktp = $request->file('ktp');
            $ktp_name = Carbon::now()->timestamp ."_ktp".".". $ktp->getClientOriginalExtension();
            $ktpPath = 'storage/data/ktp';
            $ktp->move($ktpPath,$ktp_name);
            $old_ktp = $request->input('old_ktp');
            if($old_ktp != ""){
                unlink('storage/data/ktp/'.$old_ktp);
            }
        } else {
            $ktp_name = $request->input('old_ktp');
        }

        //upload 13
        if ($request->hasFile('pas_foto_3_x_4')) {
            $pas_foto_3_x_4 = $request->file('pas_foto_3_x_4');
            $pas_foto_3_x_4_name = Carbon::now()->timestamp ."_pas_foto_3_x_4".".". $pas_foto_3_x_4->getClientOriginalExtension();
            $pas_foto_3_x_4Path = 'storage/data/pas_foto_3_x_4';
            $pas_foto_3_x_4->move($pas_foto_3_x_4Path,$pas_foto_3_x_4_name);
            $old_pas_foto_3_x_4 = $request->input('old_pas_foto_3_x_4');
            if($old_pas_foto_3_x_4 != ""){
                unlink('storage/data/pas_foto_3_x_4/'.$old_pas_foto_3_x_4);
            }
        } else {
            $pas_foto_3_x_4_name = $request->input('old_pas_foto_3_x_4');
        }

        //upload 14
        if ($request->hasFile('surat_pernyataan_bersedia_jfppbj')) {
            $surat_pernyataan_bersedia_jfppbj = $request->file('surat_pernyataan_bersedia_jfppbj');
            $surat_pernyataan_bersedia_jfppbj_name = Carbon::now()->timestamp ."_surat_pernyataan_bersedia_jfppbj".".". $surat_pernyataan_bersedia_jfppbj->getClientOriginalExtension();
            $surat_pernyataan_bersedia_jfppbjPath = 'storage/data/surat_pernyataan_bersedia_jfppbj';
            $surat_pernyataan_bersedia_jfppbj->move($surat_pernyataan_bersedia_jfppbjPath,$surat_pernyataan_bersedia_jfppbj_name);
            $old_surat_pernyataan_bersedia_jfppbj = $request->input('old_surat_pernyataan_bersedia_jfppbj');
            if($old_surat_pernyataan_bersedia_jfppbj != ""){
                unlink('storage/data/surat_pernyataan_bersedia_jfppbj/'.$old_surat_pernyataan_bersedia_jfppbj);
            }
        } else {
            $surat_pernyataan_bersedia_jfppbj_name = $request->input('old_surat_pernyataan_bersedia_jfppbj');
        }

        //upload 16
        if ($request->hasFile('surat_usulan_mengikuti_inpassing')) {
            $surat_usulan_mengikuti_inpassing = $request->file('surat_usulan_mengikuti_inpassing');
            $surat_usulan_mengikuti_inpassing_name = Carbon::now()->timestamp ."_surat_usulan_mengikuti_inpassing".".". $surat_usulan_mengikuti_inpassing->getClientOriginalExtension();
            $surat_usulan_mengikuti_inpassingPath = 'storage/data/surat_usulan_mengikuti_inpassing';
            $surat_usulan_mengikuti_inpassing->move($surat_usulan_mengikuti_inpassingPath,$surat_usulan_mengikuti_inpassing_name);
            $old_surat_usulan_mengikuti_inpassing = $request->input('old_surat_usulan_mengikuti_inpassing');
            if($old_surat_usulan_mengikuti_inpassing != ""){
                unlink('storage/data/surat_usulan_mengikuti_inpassing/'.$old_surat_usulan_mengikuti_inpassing);                   
            }
        } else {
            $surat_usulan_mengikuti_inpassing_name = $request->input('old_surat_usulan_mengikuti_inpassing');
        }
        
        $id_dokumen = $request->input('id_dokumen');
        $dokumen = Dokumen::find($id_dokumen);
        $dokumen->surat_bertugas_pbj = $surat_bertugas_pbj_name;
        $dokumen->bukti_sk_pbj = $bukti_sk_pbj_name;
        $dokumen->ijazah_terakhir = $ijazah_terakhir_name;
        $dokumen->sk_kenaikan_pangkat_terakhir = $sk_kenaikan_pangkat_terakhir_name;
        $dokumen->sk_pengangkatan_jabatan_terakhir = $sk_pengangkatan_jabatan_terakhir_name;
        $dokumen->sertifikasi_dasar_pbj = $sertifikasi_dasar_pbj_name;
        $dokumen->skp_dua_tahun_terakhir = $skp_dua_tahun_terakhir_name;
        $dokumen->surat_ket_tidak_dijatuhi_hukuman = $surat_ket_tidak_dijatuhi_hukuman_name;
        $dokumen->formulir_kesediaan_mengikuti_inpassing = $formulir_kesediaan_mengikuti_inpassing_name;
        $dokumen->sk_pengangkatan_cpns = $sk_pengangkatan_cpns_name;
        $dokumen->sk_pengangkatan_pns = $sk_pengangkatan_pns_name;
        $dokumen->ktp = $ktp_name;
        $dokumen->pas_foto_3_x_4 = $pas_foto_3_x_4_name;
        $dokumen->surat_pernyataan_bersedia_jfppbj = $surat_pernyataan_bersedia_jfppbj_name;
        // $dokumen->bukti_portofolio = $bukti_portofolio_name;
        $dokumen->surat_usulan_mengikuti_inpassing = $surat_usulan_mengikuti_inpassing_name;
        
        if($dokumen->save()){
            $id_dokumen = $dokumen->id;
        } else {
            $id_dokumen = '0';
        }

        $id_peserta = $request->input('id_peserta');
        $peserta = Peserta::find($id_peserta);
        $peserta->id_admin_ppk = Auth::user()->id;
        $peserta->nama = $request->input('nama');
        $peserta->nip = $request->input('nip');
        $peserta->jabatan = $request->input('jabatan');
        $tmt_panggol = explode('/',$request->input('tmt_panggol'));
        $peserta->tmt_panggol = $tmt_panggol[2].'-'.$tmt_panggol[1].'-'.$tmt_panggol[0];
        $peserta->jenjang = $request->input('jenjang');
        $peserta->nama_instansi = Auth::user()->nama_instansi;
        $peserta->email = $request->input('email');
        $peserta->no_telp = $request->input('no_telp');
        $peserta->tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = explode('/',$request->input('tanggal_lahir'));
        $peserta->tanggal_lahir = $tanggal_lahir[2].'-'.$tanggal_lahir[1].'-'.$tanggal_lahir[0];
        $peserta->jenis_kelamin = $request->input('jenis_kelamin');
        $peserta->no_ktp = $request->input('no_ktp');
        $peserta->no_sertifikat = $request->input('no_sertifikat');
        $peserta->pendidikan_terakhir = $request->input('pendidikan_terakhir');
        $peserta->satuan_kerja = $request->input('satuan_kerja');
        $peserta->provinsi = $request->input('provinsi');
        $peserta->kab_kota = $request->input('kab_kota');
        $peserta->status_peserta = $request->input('status_peserta');
        $peserta->no_surat_usulan_peserta = $request->input('no_surat_usulan_mengikuti_inpassing');
        $peserta->status = 'Menunggu Verifikasi Dokumen Persyaratan';
        $peserta->status_inpassing = 1;
        $peserta->verifikasi_berkas = null;
        $peserta->id_dokumen = $id_dokumen;
        $peserta->id_formasi = $request->input('no_surat');
        
        if($request->input('status_peserta') != ""){
            $peserta->status_peserta = $request->input('status_peserta');
        }
        
        if($peserta->save()){
            $updatedokumen = Dokumen::find($id_dokumen);
            $updatedokumen->id_peserta = $peserta->id;
            $updatedokumen->save();

            if($request->hasFile('surat_usulan_mengikuti_inpassing')){
             // Menuju Directory Tabel Surat usulan
             $surat_usulan_mengikuti_inpassing_more = $request->file('surat_usulan_mengikuti_inpassing');
             $surat_usulan_mengikuti_inpassing_name_more = Carbon::now()->timestamp ."_surat_usulan_peserta".".". $surat_usulan_mengikuti_inpassing_more->getClientOriginalExtension();
             $surat_usulan_mengikuti_inpassingPath_more = 'storage/data/surat_usulan_inpassing';
             
             copy($surat_usulan_mengikuti_inpassingPath.'/'.$surat_usulan_mengikuti_inpassing_name, $surat_usulan_mengikuti_inpassingPath_more.'/'.$surat_usulan_mengikuti_inpassing_name_more);
        } else {
            $surat_usulan_mengikuti_inpassing_name_more = "";
        }
            
            $id_usulan = $request->input('id_usulan');
            $updateUsulan = SuratUsulan::find($id_usulan);
            if ($updateUsulan != "") {
                $updateUsulan->id_peserta = $peserta->id;
                if ($surat_usulan_mengikuti_inpassing_name_more != "") {
                $updateUsulan->file = $surat_usulan_mengikuti_inpassing_name_more;
                }
                $updateUsulan->no_surat_usulan_peserta = $request->input('no_surat_usulan_mengikuti_inpassing');
                $updateUsulan->id_admin_ppk = Auth::user()->id;
                $updateUsulan->save();
            } else {
                $dokumenUsulan = new SuratUsulan();
                if ($surat_usulan_mengikuti_inpassing_name_more != "") {
                $dokumenUsulan->file = $surat_usulan_mengikuti_inpassing_name_more;
            }
                $dokumenUsulan->id_peserta =  $peserta->id;
                $dokumenUsulan->id_admin_ppk =  Auth::user()->id;
                $dokumenUsulan->no_surat_usulan_peserta = $request->input('no_surat_usulan_mengikuti_inpassing');
                $dokumenUsulan->save();
            }
            
            $from = env('MAIL_USERNAME');
            $datamail = array('perbaikan' => 'Dokumen sudah diperbaharui', 'email' => Auth::user()->email, 'name' => Auth::user()->name, 'from' => $from,'nip' => $request->input('nip'), 'nama_peserta' => $request->input('nama') );
            
            $admin_superadmin = DB::table('users')->where('role','superadmin')->get();
            $admin_dsp = DB::table('users')->where('role','dsp')->get();
            $admin_bangprof = DB::table('users')->where('role','bangprof')->get();
            if ($peserta->assign != "") {
                $admin_verifikator = DB::table('users')->where('id', $peserta->assign)->where('role', 'verifikator')->get();
            }
            
            $admin_instansi = DB::table('users')->where('id', Auth::user()->id)->get();
            
            foreach($admin_superadmin as $superadmins){
                Mail::send('mail.dokbaru_superadmin', $datamail, function($message) use ($datamail,$superadmins) {
                    $message->to($superadmins->email)->subject('Pembaharuan Dokumen Persyaratan');
                    $message->from($datamail['from'],$datamail['name']);
                });
            }
            
            foreach($admin_bangprof as $bangprofs){
                Mail::send('mail.dokbaru_bangprof', $datamail, function($message) use ($datamail,$bangprofs) {
                    $message->to($bangprofs->email)->subject('Pembaharuan Dokumen Persyaratan');
                    $message->from($datamail['from'],$datamail['name']);
                });
            }
            
            foreach($admin_dsp as $dsps){
                Mail::send('mail.dokbaru_dsp', $datamail, function($message) use ($datamail,$dsps) {
                    $message->to($dsps->email)->subject('Pembaharuan Dokumen Persyaratan');
                    $message->from($datamail['from'],$datamail['name']);
                });
            }
            
            foreach($admin_instansi as $instansis){
                Mail::send('mail.dokbaru_instansi', $datamail, function($message) use ($datamail,$instansis) {
                    $message->to($instansis->email)->subject('Pembaharuan Dokumen Persyaratan');
                    $message->from($datamail['from'],$datamail['name']);
                });
            }

            if ($peserta->assign != "") {
                foreach($admin_verifikator as $verifikators){
                Mail::send('mail.dokbaru_verifikator', $datamail, function($message) use ($datamail,$verifikators) {
                    $message->to($verifikators->email)->subject('Pembaharuan Dokumen Persyaratan');
                    $message->from($datamail['from'],$datamail['name']);
                });
            }
            }
            

            $msg = "berhasil_update";
        } else {
            $msg = "gagal_update";
        }
        
        return Redirect::to('data-peserta-ppk')->with('msg', $msg);
    }
    
    public function updatePeserta(Request $request, $id){
        $rules = array('nip'    => 'required|digits_between:2,18',
                       'nama'    => 'required',
                       'jabatan'    => 'required',
                       'jenjang'   => 'required',
                       'email'    => 'required',
                       'no_telp'    => 'required',
                       'tempat_lahir'    => 'required',
                       'tanggal_lahir'    => 'required',
                       'jenis_kelamin'    => 'required',
                       'no_ktp'    => 'required|numeric',
                       'no_sertifikat'    => 'required|numeric',
                       'pendidikan_terakhir'    => 'required',
                       'provinsi'    => 'required',
                       'kab_kota'    => 'required',
                       'no_surat_usulan_mengikuti_inpassing'    => 'required',
                       'surat_bertugas_pbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'bukti_sk_pbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'ijazah_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'sk_kenaikan_pangkat_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'sk_pengangkatan_jabatan_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'sertifikasi_dasar_pbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'skp_dua_tahun_terakhir' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'surat_ket_tidak_dijatuhi_hukuman' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'formulir_kesediaan_mengikuti_inpassing' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'sk_pengangkatan_cpns' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'sk_pengangkatan_pns' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'ktp' => 'mimes:jpg,png,jpeg,pdf|max:2048',
                       'pas_foto_3_x_4' => 'mimes:jpg,png,jpeg|max:2048',
                       'surat_pernyataan_bersedia_jfppbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
                       'surat_usulan_mengikuti_inpassing' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048');
        
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('edit-peserta/'.$id)
                    ->withErrors($validator)
                    ->withInput();
        }
        
        $id_surat = $request->input('no_surat');
        $jenjang = strtolower($request->input('jenjang'));
        $eformasi = PengusulanEformasi::where('id_admin_ppk',Auth::user()->id)
                    ->where('id',$id_surat)
                    ->first();
        $jenjang_get = $eformasi->$jenjang;
        $peserta_terdaftar = Peserta::where('id_admin_ppk',Auth::user()->id)
                            ->where('jenjang',$jenjang)
                            ->where('id_formasi',$id_surat)
                            ->where('status_peserta','aktif')
                            ->whereNotIn('id',[$id])
                            ->count();
        
        if($peserta_terdaftar >= $jenjang_get){
            return Redirect::to('edit-peserta/'.$id)
                    ->withErrors($validator)
                    ->with('msg','jenjang_penuh')
                    ->withInput();
        }

        //upload 1
        if ($request->hasFile('surat_bertugas_pbj')) {
            $surat_bertugas_pbj = $request->file('surat_bertugas_pbj');
            $surat_bertugas_pbj_name = Carbon::now()->timestamp ."_surat_bertugas_pbj".".". $surat_bertugas_pbj->getClientOriginalExtension();
            $surat_bertugas_pbjPath = 'storage/data/surat_bertugas_pbj';
            $surat_bertugas_pbj->move($surat_bertugas_pbjPath,$surat_bertugas_pbj_name);
            $old_surat_bertugas_pbj = $request->input('old_surat_bertugas_pbj');
            if($old_surat_bertugas_pbj != ""){
                unlink('storage/data/surat_bertugas_pbj/'.$old_surat_bertugas_pbj);
            }
        } else {
            $surat_bertugas_pbj_name = $request->input('old_surat_bertugas_pbj');
        }

        //upload 2
        if ($request->hasFile('bukti_sk_pbj')) {
            $bukti_sk_pbj = $request->file('bukti_sk_pbj');
            $bukti_sk_pbj_name = Carbon::now()->timestamp ."_bukti_sk_pbj".".". $bukti_sk_pbj->getClientOriginalExtension();
            $bukti_sk_pbjPath = 'storage/data/bukti_sk_pbj';
            $bukti_sk_pbj->move($bukti_sk_pbjPath,$bukti_sk_pbj_name);
            $old_bukti_sk_pbj = $request->input('old_bukti_sk_pbj');
            if($old_bukti_sk_pbj != ""){
                unlink('storage/data/bukti_sk_pbj/'.$old_bukti_sk_pbj);
            }
        } else {
            $bukti_sk_pbj_name = $request->input('old_bukti_sk_pbj');
        }

        //upload 3
        if ($request->hasFile('ijazah_terakhir')) {
            $ijazah_terakhir = $request->file('ijazah_terakhir');
            $ijazah_terakhir_name = Carbon::now()->timestamp ."_ijazah_terakhir".".". $ijazah_terakhir->getClientOriginalExtension();
            $ijazah_terakhirPath = 'storage/data/ijazah_terakhir';
            $ijazah_terakhir->move($ijazah_terakhirPath,$ijazah_terakhir_name);
            $old_ijazah_terakhir = $request->input('old_ijazah_terakhir');
            if($old_ijazah_terakhir != ""){
                unlink('storage/data/ijazah_terakhir/'.$old_ijazah_terakhir);
            }
        } else {
            $ijazah_terakhir_name = $request->input('old_ijazah_terakhir');
        }

        //upload 4
        if ($request->hasFile('sk_kenaikan_pangkat_terakhir')) {
            $sk_kenaikan_pangkat_terakhir = $request->file('sk_kenaikan_pangkat_terakhir');
            $sk_kenaikan_pangkat_terakhir_name = Carbon::now()->timestamp ."_sk_kenaikan_pangkat_terakhir".".". $sk_kenaikan_pangkat_terakhir->getClientOriginalExtension();
            $sk_kenaikan_pangkat_terakhirPath = 'storage/data/sk_kenaikan_pangkat_terakhir';
            $sk_kenaikan_pangkat_terakhir->move($sk_kenaikan_pangkat_terakhirPath,$sk_kenaikan_pangkat_terakhir_name);
            $old_sk_kenaikan_pangkat_terakhir = $request->input('old_sk_kenaikan_pangkat_terakhir');
            if($old_sk_kenaikan_pangkat_terakhir != ""){
                unlink('storage/data/sk_kenaikan_pangkat_terakhir/'.$old_sk_kenaikan_pangkat_terakhir);
            }
        } else {
            $sk_kenaikan_pangkat_terakhir_name = $request->input('old_sk_kenaikan_pangkat_terakhir');
        }

        //upload 5
        if ($request->hasFile('sk_pengangkatan_jabatan_terakhir')) {
            $sk_pengangkatan_jabatan_terakhir = $request->file('sk_pengangkatan_jabatan_terakhir');
            $sk_pengangkatan_jabatan_terakhir_name = Carbon::now()->timestamp ."_sk_pengangkatan_jabatan_terakhir".".". $sk_pengangkatan_jabatan_terakhir->getClientOriginalExtension();
            $sk_pengangkatan_jabatan_terakhirPath = 'storage/data/sk_pengangkatan_jabatan_terakhir';
            $sk_pengangkatan_jabatan_terakhir->move($sk_pengangkatan_jabatan_terakhirPath,$sk_pengangkatan_jabatan_terakhir_name);
            $old_sk_pengangkatan_jabatan_terakhir = $request->input('old_sk_pengangkatan_jabatan_terakhir');
            if($old_sk_pengangkatan_jabatan_terakhir != ""){
                unlink('storage/data/sk_pengangkatan_jabatan_terakhir/'.$old_sk_pengangkatan_jabatan_terakhir);
            }
        } else {
            $sk_pengangkatan_jabatan_terakhir_name = $request->input('old_sk_pengangkatan_jabatan_terakhir');
        }

        //upload 6
        if ($request->hasFile('sertifikasi_dasar_pbj')) {
            $sertifikasi_dasar_pbj = $request->file('sertifikasi_dasar_pbj');
            $sertifikasi_dasar_pbj_name = Carbon::now()->timestamp ."_sertifikasi_dasar_pbj".".". $sertifikasi_dasar_pbj->getClientOriginalExtension();
            $sertifikasi_dasar_pbjPath = 'storage/data/sertifikasi_dasar_pbj';
            $sertifikasi_dasar_pbj->move($sertifikasi_dasar_pbjPath,$sertifikasi_dasar_pbj_name);
            $old_sertifikasi_dasar_pbj = $request->input('old_sertifikasi_dasar_pbj');
            if($old_sertifikasi_dasar_pbj != ""){
                unlink('storage/data/sertifikasi_dasar_pbj/'.$old_sertifikasi_dasar_pbj);
            }
        } else {
            $sertifikasi_dasar_pbj_name = $request->input('old_sertifikasi_dasar_pbj');
        }

        //upload 7
        if ($request->hasFile('skp_dua_tahun_terakhir')) {
            $skp_dua_tahun_terakhir = $request->file('skp_dua_tahun_terakhir');
            $skp_dua_tahun_terakhir_name = Carbon::now()->timestamp ."_skp_dua_tahun_terakhir".".". $skp_dua_tahun_terakhir->getClientOriginalExtension();
            $skp_dua_tahun_terakhirPath = 'storage/data/skp_dua_tahun_terakhir';
            $skp_dua_tahun_terakhir->move($skp_dua_tahun_terakhirPath,$skp_dua_tahun_terakhir_name);
            $old_skp_dua_tahun_terakhir = $request->input('old_skp_dua_tahun_terakhir');
            if($old_skp_dua_tahun_terakhir != ""){
                unlink('storage/data/skp_dua_tahun_terakhir/'.$old_skp_dua_tahun_terakhir);
            }
        } else {
            $skp_dua_tahun_terakhir_name = $request->input('old_skp_dua_tahun_terakhir');
        }

        //upload 8
        if ($request->hasFile('surat_ket_tidak_dijatuhi_hukuman')) {
            $surat_ket_tidak_dijatuhi_hukuman = $request->file('surat_ket_tidak_dijatuhi_hukuman');
            $surat_ket_tidak_dijatuhi_hukuman_name = Carbon::now()->timestamp ."_surat_ket_tidak_dijatuhi_hukuman".".". $surat_ket_tidak_dijatuhi_hukuman->getClientOriginalExtension();
            $surat_ket_tidak_dijatuhi_hukumanPath = 'storage/data/surat_ket_tidak_dijatuhi_hukuman';
            $surat_ket_tidak_dijatuhi_hukuman->move($surat_ket_tidak_dijatuhi_hukumanPath,$surat_ket_tidak_dijatuhi_hukuman_name);
            $old_surat_ket_tidak_dijatuhi_hukuman = $request->input('old_surat_ket_tidak_dijatuhi_hukuman');
            if($old_surat_ket_tidak_dijatuhi_hukuman != ""){
                unlink('storage/data/surat_ket_tidak_dijatuhi_hukuman/'.$old_surat_ket_tidak_dijatuhi_hukuman);
            }
        } else {
            $surat_ket_tidak_dijatuhi_hukuman_name = $request->input('old_surat_ket_tidak_dijatuhi_hukuman');
        }

        //upload 9
        if ($request->hasFile('formulir_kesediaan_mengikuti_inpassing')) {
            $formulir_kesediaan_mengikuti_inpassing = $request->file('formulir_kesediaan_mengikuti_inpassing');
            $formulir_kesediaan_mengikuti_inpassing_name = Carbon::now()->timestamp ."_formulir_kesediaan_mengikuti_inpassing".".". $formulir_kesediaan_mengikuti_inpassing->getClientOriginalExtension();
            $formulir_kesediaan_mengikuti_inpassingPath = 'storage/data/formulir_kesediaan_mengikuti_inpassing';
            $formulir_kesediaan_mengikuti_inpassing->move($formulir_kesediaan_mengikuti_inpassingPath,$formulir_kesediaan_mengikuti_inpassing_name);
            $old_formulir_kesediaan_mengikuti_inpassing = $request->input('old_formulir_kesediaan_mengikuti_inpassing');
            if($old_formulir_kesediaan_mengikuti_inpassing != ""){
                unlink('storage/data/formulir_kesediaan_mengikuti_inpassing/'.$old_formulir_kesediaan_mengikuti_inpassing);
            }
        } else {
            $formulir_kesediaan_mengikuti_inpassing_name = $request->input('old_formulir_kesediaan_mengikuti_inpassing');
        }

        //upload 10
        if ($request->hasFile('sk_pengangkatan_cpns')) {
            $sk_pengangkatan_cpns = $request->file('sk_pengangkatan_cpns');
            $sk_pengangkatan_cpns_name = Carbon::now()->timestamp ."_sk_pengangkatan_cpns".".". $sk_pengangkatan_cpns->getClientOriginalExtension();
            $sk_pengangkatan_cpnsPath = 'storage/data/sk_pengangkatan_cpns';
            $sk_pengangkatan_cpns->move($sk_pengangkatan_cpnsPath,$sk_pengangkatan_cpns_name);
            $old_sk_pengangkatan_cpns = $request->input('old_sk_pengangkatan_cpns');
            if($old_sk_pengangkatan_cpns != ""){
                unlink('storage/data/sk_pengangkatan_cpns/'.$old_sk_pengangkatan_cpns);
            }
        } else {
            $sk_pengangkatan_cpns_name = $request->input('old_sk_pengangkatan_cpns');
        }

        //upload 11
        if ($request->hasFile('sk_pengangkatan_pns')) {
            $sk_pengangkatan_pns = $request->file('sk_pengangkatan_pns');
            $sk_pengangkatan_pns_name = Carbon::now()->timestamp ."_sk_pengangkatan_pns".".". $sk_pengangkatan_pns->getClientOriginalExtension();
            $sk_pengangkatan_pnsPath = 'storage/data/sk_pengangkatan_pns';
            $sk_pengangkatan_pns->move($sk_pengangkatan_pnsPath,$sk_pengangkatan_pns_name);
            $old_sk_pengangkatan_pns = $request->input('old_sk_pengangkatan_pns');
            if($old_sk_pengangkatan_pns != ""){
                unlink('storage/data/sk_pengangkatan_pns/'.$old_sk_pengangkatan_pns);
            }
        } else {
            $sk_pengangkatan_pns_name = $request->input('old_sk_pengangkatan_pns');
        }

        //upload 12
        if ($request->hasFile('ktp')) {
            $ktp = $request->file('ktp');
            $ktp_name = Carbon::now()->timestamp ."_ktp".".". $ktp->getClientOriginalExtension();
            $ktpPath = 'storage/data/ktp';
            $ktp->move($ktpPath,$ktp_name);
            $old_ktp = $request->input('old_ktp');
            if($old_ktp != ""){
                unlink('storage/data/ktp/'.$old_ktp);
            }
        } else {
            $ktp_name = $request->input('old_ktp');
        }

        //upload 13
        if ($request->hasFile('pas_foto_3_x_4')) {
            $pas_foto_3_x_4 = $request->file('pas_foto_3_x_4');
            $pas_foto_3_x_4_name = Carbon::now()->timestamp ."_pas_foto_3_x_4".".". $pas_foto_3_x_4->getClientOriginalExtension();
            $pas_foto_3_x_4Path = 'storage/data/pas_foto_3_x_4';
            $pas_foto_3_x_4->move($pas_foto_3_x_4Path,$pas_foto_3_x_4_name);
            $old_pas_foto_3_x_4 = $request->input('old_pas_foto_3_x_4');
            if($old_pas_foto_3_x_4 != ""){
                unlink('storage/data/pas_foto_3_x_4/'.$old_pas_foto_3_x_4);
            }
        } else {
            $pas_foto_3_x_4_name = $request->input('old_pas_foto_3_x_4');
        }

        //upload 14
        if ($request->hasFile('surat_pernyataan_bersedia_jfppbj')) {
            $surat_pernyataan_bersedia_jfppbj = $request->file('surat_pernyataan_bersedia_jfppbj');
            $surat_pernyataan_bersedia_jfppbj_name = Carbon::now()->timestamp ."_surat_pernyataan_bersedia_jfppbj".".". $surat_pernyataan_bersedia_jfppbj->getClientOriginalExtension();
            $surat_pernyataan_bersedia_jfppbjPath = 'storage/data/surat_pernyataan_bersedia_jfppbj';
            $surat_pernyataan_bersedia_jfppbj->move($surat_pernyataan_bersedia_jfppbjPath,$surat_pernyataan_bersedia_jfppbj_name);
            $old_surat_pernyataan_bersedia_jfppbj = $request->input('old_surat_pernyataan_bersedia_jfppbj');
            if($old_surat_pernyataan_bersedia_jfppbj != ""){
                unlink('storage/data/surat_pernyataan_bersedia_jfppbj/'.$old_surat_pernyataan_bersedia_jfppbj);
            }
        } else {
            $surat_pernyataan_bersedia_jfppbj_name = $request->input('old_surat_pernyataan_bersedia_jfppbj');
        }

        //upload 16
        if ($request->hasFile('surat_usulan_mengikuti_inpassing')) {
            $surat_usulan_mengikuti_inpassing = $request->file('surat_usulan_mengikuti_inpassing');
            $surat_usulan_mengikuti_inpassing_name = Carbon::now()->timestamp ."_surat_usulan_mengikuti_inpassing".".". $surat_usulan_mengikuti_inpassing->getClientOriginalExtension();
            $surat_usulan_mengikuti_inpassingPath = 'storage/data/surat_usulan_mengikuti_inpassing';
            $surat_usulan_mengikuti_inpassing->move($surat_usulan_mengikuti_inpassingPath,$surat_usulan_mengikuti_inpassing_name);
            $old_surat_usulan_mengikuti_inpassing = $request->input('old_surat_usulan_mengikuti_inpassing');
            if($old_surat_usulan_mengikuti_inpassing != ""){
                unlink('storage/data/surat_usulan_mengikuti_inpassing/'.$old_surat_usulan_mengikuti_inpassing);
            }
        } else {
            $surat_usulan_mengikuti_inpassing_name = $request->input('old_surat_usulan_mengikuti_inpassing');
        }
        
        $id_dokumen = $request->input('id_dokumen');
        $dokumen = Dokumen::find($id_dokumen);
        $dokumen->surat_bertugas_pbj = $surat_bertugas_pbj_name;
        $dokumen->bukti_sk_pbj = $bukti_sk_pbj_name;
        $dokumen->ijazah_terakhir = $ijazah_terakhir_name;
        $dokumen->sk_kenaikan_pangkat_terakhir = $sk_kenaikan_pangkat_terakhir_name;
        $dokumen->sk_pengangkatan_jabatan_terakhir = $sk_pengangkatan_jabatan_terakhir_name;
        $dokumen->sertifikasi_dasar_pbj = $sertifikasi_dasar_pbj_name;
        $dokumen->skp_dua_tahun_terakhir = $skp_dua_tahun_terakhir_name;
        $dokumen->surat_ket_tidak_dijatuhi_hukuman = $surat_ket_tidak_dijatuhi_hukuman_name;
        $dokumen->formulir_kesediaan_mengikuti_inpassing = $formulir_kesediaan_mengikuti_inpassing_name;
        $dokumen->sk_pengangkatan_cpns = $sk_pengangkatan_cpns_name;
        $dokumen->sk_pengangkatan_pns = $sk_pengangkatan_pns_name;
        $dokumen->ktp = $ktp_name;
        $dokumen->pas_foto_3_x_4 = $pas_foto_3_x_4_name;
        $dokumen->surat_pernyataan_bersedia_jfppbj = $surat_pernyataan_bersedia_jfppbj_name;
        $dokumen->surat_usulan_mengikuti_inpassing = $surat_usulan_mengikuti_inpassing_name;
        
        if($dokumen->save()){
            $id_dokumen = $dokumen->id;
        } else {
            $id_dokumen = '0';
        }
        
        $id_peserta = $request->input('id_peserta');
        $peserta = Peserta::find($id_peserta);
        $peserta->id_admin_ppk = Auth::user()->id;
        $peserta->nama = $request->input('nama');
        $peserta->nip = $request->input('nip');
        $peserta->jabatan = $request->input('jabatan');
        $tmt_panggol = explode('/',$request->input('tmt_panggol'));
        $peserta->tmt_panggol = $tmt_panggol[2].'-'.$tmt_panggol[1].'-'.$tmt_panggol[0];
        $peserta->jenjang = $request->input('jenjang');
        $peserta->nama_instansi = Auth::user()->nama_instansi;
        $peserta->email = $request->input('email');
        $peserta->no_telp = $request->input('no_telp');
        $peserta->tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = explode('/',$request->input('tanggal_lahir'));
        $peserta->tanggal_lahir = $tanggal_lahir[2].'-'.$tanggal_lahir[1].'-'.$tanggal_lahir[0];
        $peserta->jenis_kelamin = $request->input('jenis_kelamin');
        $peserta->no_ktp = $request->input('no_ktp');
        $peserta->no_sertifikat = $request->input('no_sertifikat');
        $peserta->pendidikan_terakhir = $request->input('pendidikan_terakhir');
        $peserta->satuan_kerja = $request->input('satuan_kerja');
        $peserta->provinsi = $request->input('provinsi');
        $peserta->kab_kota = $request->input('kab_kota');
        $peserta->status_peserta = $request->input('status_peserta');
        $peserta->no_surat_usulan_peserta = $request->input('no_surat_usulan_mengikuti_inpassing');
        $peserta->status = 'Menunggu Verifikasi Dokumen Persyaratan';
        $peserta->status_inpassing = 1;
        $peserta->verifikasi_berkas = null;
        $peserta->id_dokumen = $id_dokumen;
        $peserta->id_formasi = $request->input('no_surat');
        
        if($request->input('status_peserta') != ""){
            $peserta->status_peserta = $request->input('status_peserta');
        }
        
        if($peserta->save()){
            $updatedokumen = Dokumen::find($id_dokumen);
            $updatedokumen->id_peserta = $peserta->id;
            $updatedokumen->save();
        
        if($request->hasFile('surat_usulan_mengikuti_inpassing')){
             // Menuju Directory Tabel Surat usulan
             $surat_usulan_mengikuti_inpassing_more = $request->file('surat_usulan_mengikuti_inpassing');
             $surat_usulan_mengikuti_inpassing_name_more = Carbon::now()->timestamp ."_surat_usulan_peserta".".". $surat_usulan_mengikuti_inpassing_more->getClientOriginalExtension();
             $surat_usulan_mengikuti_inpassingPath_more = 'storage/data/surat_usulan_inpassing';
             
             copy($surat_usulan_mengikuti_inpassingPath.'/'.$surat_usulan_mengikuti_inpassing_name, $surat_usulan_mengikuti_inpassingPath_more.'/'.$surat_usulan_mengikuti_inpassing_name_more);
        } else {
            $surat_usulan_mengikuti_inpassing_name_more = "";
        }
        
        $id_usulan = $request->input('id_usulan');
        $updateUsulan = SuratUsulan::find($id_usulan);
        
        if ($updateUsulan != "") {
            $updateUsulan->id_peserta = $peserta->id;
            if ($surat_usulan_mengikuti_inpassing_name_more != "") {
                $updateUsulan->file = $surat_usulan_mengikuti_inpassing_name_more;
            }
        
            $updateUsulan->no_surat_usulan_peserta = $request->input('no_surat_usulan_mengikuti_inpassing');
            $updateUsulan->id_admin_ppk = Auth::user()->id;
            $updateUsulan->save();
        } else {
            $dokumenUsulan = new SuratUsulan();
            if ($surat_usulan_mengikuti_inpassing_name_more != "") {
                $dokumenUsulan->file = $surat_usulan_mengikuti_inpassing_name_more;
            }
            
            $dokumenUsulan->id_peserta =  $peserta->id;
            $dokumenUsulan->id_admin_ppk =  Auth::user()->id;
            $dokumenUsulan->no_surat_usulan_peserta = $request->input('no_surat_usulan_mengikuti_inpassing');
            $dokumenUsulan->save();
        }
        
        $from = env('MAIL_USERNAME');
        $datamail = array('perbaikan' => 'Dokumen sudah diperbaharui', 'email' => Auth::user()->email, 'name' => Auth::user()->name, 'from' => $from,'nip' => $request->input('nip'), 'nama_peserta' => $request->input('nama') );
        $admin_superadmin = DB::table('users')->where('role','superadmin')->get();
        $admin_dsp = DB::table('users')->where('role','dsp')->get();
        $admin_bangprof = DB::table('users')->where('role','bangprof')->get();
        $admin_instansi = DB::table('users')->where('id', Auth::user()->id)->get();
        
        foreach($admin_superadmin as $superadmins){
            Mail::send('mail.dokbaru_superadmin', $datamail, function($message) use ($datamail,$superadmins) {
                $message->to($superadmins->email)->subject('Pembaharuan Dokumen Persyaratan');
                $message->from($datamail['from'],$datamail['name']);
            });
        }
        
        foreach($admin_bangprof as $bangprofs){
            Mail::send('mail.dokbaru_bangprof', $datamail, function($message) use ($datamail,$bangprofs) {
                $message->to($bangprofs->email)->subject('Pembaharuan Dokumen Persyaratan');
                $message->from($datamail['from'],$datamail['name']);
            });
        }
        
        foreach($admin_dsp as $dsps){
            Mail::send('mail.dokbaru_dsp', $datamail, function($message) use ($datamail,$dsps) {
                $message->to($dsps->email)->subject('Pembaharuan Dokumen Persyaratan');
                $message->from($datamail['from'],$datamail['name']);
            });
        }
        
        foreach($admin_instansi as $instansis){
            Mail::send('mail.dokbaru_instansi', $datamail, function($message) use ($datamail,$instansis) {
                $message->to($instansis->email)->subject('Pembaharuan Dokumen Persyaratan');
                $message->from($datamail['from'],$datamail['name']);
            });
        }        
        
        $msg = "berhasil_update";
        } else {
            $msg = "gagal_update";
        }
    
        return Redirect::to('data-peserta-ppk')->with('msg', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $data = Peserta::find($id);
       $data->nip = 'akun_terhapus_'.$data->nip;
       $data->email = 'akun_terhapus_'.$data->email;
       $data->deleted_at = Carbon::now()->toDateTimeString();

       if($data->save()){
           $riwayat = new RiwayatUser();
           $riwayat->id_user = $data->id;
           $riwayat->id_admin = Auth::user()->id;
           $riwayat->perihal = "peserta";
           $riwayat->description = "di hapus akun oleh";
           $riwayat->tanggal = Carbon::now()->toDateString(); 
           $riwayat->save();
           return Redirect::to('data-peserta-ppk')->with('msg','berhasil_hapus');
       } else {
           return Redirect::to('data-peserta-ppk')->with('msg','gagal_hapus');
       }
   }

   public function destroyLkpp($id)
   {
        if(Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp'){
            return Redirect::back();
        }

        $data = Peserta::find($id);
        $date =Carbon::now()->toDateString();
        $peserta_jadwal = PesertaJadwal::where('id_peserta',$id)->first();
        $peserta_jadwal->status = 0;
        $data->nip = $data->nip . '_akun_terhapus_' . $date;
        $data->email = $data->email . '_akun_terhapus_' . $date;
        $data->deleted_at = Carbon::now()->toDateTimeString();
        if($data->save() && $peserta_jadwal->save()){
           $riwayat = new RiwayatUser();
           $riwayat->id_user = $data->id;
           $riwayat->id_admin_lkpp = Auth::user()->id;
           $riwayat->perihal = "peserta";
           $riwayat->description = "di hapus akun oleh";
           $riwayat->tanggal = Carbon::now()->toDateString(); 
           $riwayat->save();
           return Redirect::to('data-peserta')->with('msg','berhasil_hapus');
       } else {
           return Redirect::to('data-peserta')->with('msg','gagal_hapus');
       }
   }

   public function destroyJadwalLkpp(Request $request,$id){
       $metode = $request->input('metodes');
       $id_jadwal = $request->input('id_jadwal');
       $peserta_sts = Peserta::find($id);
       $peserta_jadwal = PesertaJadwal::where('id_peserta',$id)->where('id_jadwal',$id_jadwal)->first();

       if($metode == "Verifikasi Portofolio") {
           $id_status = 3;
           $status = "Dokumen Persyaratan Lengkap";
           $id_status_1 = 2;
           $status_1 = "Dokumen Persyaratan Tidak Lengkap";
           
           if($peserta_sts->verifikasi_berkas == "not_verified") {
                $peserta_sts->status = $status_1;
                $peserta_sts->status_inpassing = $id_status_1;
                $peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
                $peserta_sts->save();
            } elseif ($peserta_sts->verifikasi_berkas == null ) {
                $peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
                $peserta_sts->status_inpassing = 1;
                $peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
                $peserta_sts->save();
            } elseif ($peserta_sts->verifikasi_berkas == "verified") {
                $peserta_sts->status = $status;
                $peserta_sts->status_inpassing = $id_status;
                $peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
                $peserta_sts->save();
            }
            
            $peserta_jadwal->status = 0;
            if($peserta_jadwal->save()){
                $tgl_uji = $request->input('tgl_uji');
                $riwayat = new RiwayatUser();
                $riwayat->id_user = $peserta_sts->id;
                $riwayat->id_admin_lkpp = Auth::user()->id;
                $riwayat->perihal = "jadwal_peserta";
                $riwayat->description = "di hapus dari jadwal ".$metode." tanggal ".$tgl_uji." oleh";
                $riwayat->tanggal = Carbon::now()->toDateString(); 
                $riwayat->save();
                return Redirect::to('peserta-jadwal-inpassing/'.$id_jadwal)->with('msg','berhasil_hapus');
            }  
        } elseif ($metode == "Tes Tertulis") {
            $id_status = 3;
            $status = "Dokumen Persyaratan Lengkap";
            $id_status_1 = 2;
            $status_1 = "Dokumen Persyaratan Tidak Lengkap";
            
            if ($peserta_sts->verifikasi_berkas == "not_verified") {
                $peserta_sts->status = $status_1;
                $peserta_sts->status_inpassing = $id_status_1;
                $peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
                $peserta_sts->save();
            } elseif ($peserta_sts->verifikasi_berkas == null ) {
                $peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
                $peserta_sts->status_inpassing = 1;
                $peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
                $peserta_sts->save();
            } elseif ($peserta_sts->verifikasi_berkas == "verified") {
                $peserta_sts->status = $status;
                $peserta_sts->status_inpassing = $id_status;
                $peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
                $peserta_sts->save();
            }
            
            if($peserta_jadwal->delete()){
                $tgl_uji = $request->input('tgl_uji');
                $riwayat = new RiwayatUser();
                $riwayat->id_user = $peserta_sts->id;
                $riwayat->id_admin_lkpp = Auth::user()->id;
                $riwayat->perihal = "jadwal_peserta";
                $riwayat->description = "di hapus dari jadwal ".$metode." tanggal ".$tgl_uji." oleh";
                $riwayat->tanggal = Carbon::now()->toDateString(); 
                $riwayat->save();
                return Redirect::to('peserta-jadwal-inpassing/'.$id_jadwal)->with('msg','berhasil_hapus');
            }
        } else {
            return Redirect::to('peserta-jadwal-inpassing/'.$id_jadwal)->with('msg','gagal_hapus');
        }
    }

    public function destroyJadwalInstansiLkpp(Request $request,$id)
    {   
        $id_jadwal = $request->input('id_jadwal');
        $peserta_sts = Peserta::find($id);
        $peserta_jadwal = PesertaInstansi::where('id_peserta',$id)->where('id_jadwal',$id_jadwal)->first();
        
        $id_status = 3;
        $status = "Dokumen Persyaratan Lengkap";
        $id_status_1 = 2;
        $status_1 = "Dokumen Persyaratan Tidak Lengkap";

        if ($peserta_sts->verifikasi_berkas == "not_verified") {
            $peserta_sts->status = $status_1;
            $peserta_sts->status_inpassing = $id_status_1;
            $peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
            $peserta_sts->save();
        } elseif ($peserta_sts->verifikasi_berkas == null ) {
            $peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
            $peserta_sts->status_inpassing = 1;
            $peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
            $peserta_sts->save();
        } elseif ($peserta_sts->verifikasi_berkas == "verified") {
            $peserta_sts->status = $status;
            $peserta_sts->status_inpassing = $id_status;
            $peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
            $peserta_sts->save();
        }

        if($peserta_jadwal->delete()){
            $riwayat = new RiwayatUser();
            $metode = $request->input('metodes');
            $tgl_uji = $request->input('tgl_uji');
            $riwayat->id_user = $peserta_sts->id;
            $riwayat->id_admin_lkpp = Auth::user()->id;
            $riwayat->perihal = "jadwal_peserta";
            $riwayat->description = "di hapus dari jadwal ".$metode." tanggal ".$tgl_uji." oleh";
            $riwayat->tanggal = Carbon::now()->toDateString(); 
            $riwayat->save();
            return Redirect::to('lihat-peserta-instansi/'.$peserta_jadwal->id_jadwal)->with('msg','berhasil_hapus');
        } else {
            return Redirect::to('lihat-peserta-instansi/'.$peserta_jadwal->id_jadwal)->with('msg','gagal_hapus');
        }
    }

    public function regencies($id){
        if ($id == "") {
            return response()->json(['msg' => 'gagal']); 
        } else {
            $provinces_id = $id;
            $regencies = regencie::where('province_id', '=', $provinces_id)->get();
            $option = '';
        
            foreach ($regencies as $key => $data) {
                $option .= "<option value='".$data->id."'>".$data->name."</option>";
            }
        
            return response()->json(['msg' => 'berhasil', 'id' => $id, 'data' => $option]);
        }
    }

    public function cekJenjang($jenjang,$id){
        $eformasi = PengusulanEformasi::where('id_admin_ppk',Auth::user()->id)
                    ->where('id',$id)
                    ->first();
        
        $jenjang_get = $eformasi->$jenjang;
        $peserta_terdaftar = Peserta::where('id_admin_ppk',Auth::user()->id)
                             ->where('id_formasi',$id)
                             ->where('jenjang',$jenjang)
                             ->where('status_peserta','aktif')
                             ->count();

        if($peserta_terdaftar >= $jenjang_get){
            $msg = "penuh";
        } else {
            $msg = "oke";
        }

        return response()->json(['msg' => $msg, 'peserta_terdaftar' => $peserta_terdaftar, 'jenjang' => $jenjang_get, 'id' => $id, 'jen'  => $jenjang]);
    }

    public function cekJenjangEdit($jenjang,$id,$jenjang_lama,$id_lama){
        $eformasi = PengusulanEformasi::where('id_admin_ppk',Auth::user()->id)
                    ->where('id',$id)
                    ->first();
        
        $jenjang_get = $eformasi->$jenjang;
        $peserta_terdaftar = Peserta::where('id_admin_ppk',Auth::user()->id)
                             ->where('id_formasi',$id)
                             ->where('jenjang',$jenjang)
                             ->where('status_peserta','aktif')
                             ->count();

        if($jenjang == $jenjang_lama && $id == $id_lama){
            $jenjang_get = $jenjang_get + 1;
        }

        if($peserta_terdaftar >= $jenjang_get){
            $msg = "penuh";
        } else {
            $msg = "oke";
        }

        return response()->json(['msg' => $msg, 'peserta_terdaftar' => $peserta_terdaftar, 'jenjang' => $jenjang_get, 'id' => $id, 'jen'  => $jenjang]);
    }

    public function checkEmail(Request $request){
        $email = $request->input('email');
        $cek = Peserta::where('email',$email)->count();
        if($cek == 0){
            return response()->json(array("exists" => true));
        } else {
            return response()->json(array("exists" => false));
        }
    }

    public function checkNip(Request $request){
        $nip = $request->input('nip');
        $cek = Peserta::where('nip',$nip)->where('deleted_at',null)->count();
        
        if($cek == 0){
            return response()->json(array("exists" => true));
        } else {
            return response()->json(array("exists" => false));
        }
    }

    public function checkTelp(Request $request)
    {
        $telp = $request->input('no_telp');
        $cek = Peserta::where('no_telp',$telp)->where('deleted_at',null)->count();
        if($cek == 0){
            return response()->json(array("exists" => true));
        } else {
            return response()->json(array("exists" => false));
        }

        if (strlen($telp) < 2 || strlen($telp) > 12 ) {
            return response()->json(array("minus" => true));
        } else {
            return response()->json(array("minus" => false));
        }
    }

    public function checkNoSertifikat(Request $request){
        $no_serti = $request->input('no_sertifikat');
        $cek = Peserta::where('no_sertifikat',$no_serti)->where('deleted_at',null)->count();
        
        if($cek == 0){
            return response()->json(array("exists" => true));
        } else {
            return response()->json(array("exists" => false));
        }
    }

    public function dummy()
    {
        $id = Peserta::all();
        foreach ($id as $key => $ids) {
            $data = Peserta::where('id',$ids->id)->where('jabatan','IV/c')->first();
            if ($data) {
                $data->jabatan = 'Pembina Utama Muda (IV/c)';
                $data->save();
            }
        }
    }

    public function printExcell()
    {
        $nama_excel = 'Data Peserta lkpp.xlsx';
        return Excel::download(new DataPesertaExport(), $nama_excel);
    }
    
}