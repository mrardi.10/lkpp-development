<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Exports\PengusulanEformasiExport;
use Maatwebsite\Excel\Facades\Excel;
use App\User;
use App\PengusulanEformasi;
use App\RiwayatUser;
use Carbon\Carbon;
use DataTables;
use Redirect;
use Validator;
use View;
use Mail;
use Image;
use File;
use DB;

class EformasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $data = DB::table('pengusulan_eformasis')
				->join('users', 'pengusulan_eformasis.id_admin_ppk', '=', 'users.id')
				->select('pengusulan_eformasis.*', 'users.email as emails', 'users.nama_instansi as instansis', 'users.no_telp as no_telps')
				->where('id_admin_ppk',$id)
				->where('pengusulan_eformasis.deleted_at',null)
				->orderBy('pengusulan_eformasis.updated_at', 'desc')
				->get();
        
		$data1 = DB::table('pengusulan_eformasis')
				->join('users', 'pengusulan_eformasis.id_admin_ppk', '=', 'users.id')
				->select('pengusulan_eformasis.*', 'users.email as emails', 'users.nama_instansi as instansis', 'users.no_telp as no_telps')
				->where('pengusulan_eformasis.id_admin_ppk',$id)
				->where('pengusulan_eformasis.deleted_at',null)
				->orderBy('pengusulan_eformasis.id', 'DESC')
				->first();

		$notifikasi = DB::table('riwayat_users')
  					  ->join('users', 'riwayat_users.id_admin_lkpp','=','users.id')
					  ->select('riwayat_users.*','users.name as admin_name')
					  ->where('perihal','verifikasi_eformasi')
					  ->where('id_admin',$id)
					  ->orderBy('id','desc')
					  ->get();
        
		return View::make('list_pengusulan_eformasi', compact('data','data1','notifikasi'));
    }

    public function indexLkppJson(){
        $eformasi = DB::table('pengusulan_eformasis')
            ->join('users', 'pengusulan_eformasis.id_admin_ppk', '=', 'users.id')
            ->join('instansis', 'users.nama_instansi', '=', 'instansis.id')
            ->select('pengusulan_eformasis.*', 'users.email as emails', 'users.no_telp as no_telps', 'users.username as usernames', 'instansis.nama as instansis','users.name as namas')
            ->orderBy('pengusulan_eformasis.updated_at', 'DESC')
            ->where('pengusulan_eformasis.deleted_at',null)
            ->get();
        
        return Datatables::of($eformasi)
				->addIndexColumn()
				->addColumn('status_usulan_pertama', '@if (is_null($status_pertama))-@else{{ $status_pertama == "setuju" ? "Setuju" : "Tidak Setuju" }}@endif')
				->addColumn('status_usulan_muda', '@if (is_null($status_muda))-@else{{ $status_muda == "setuju" ? "Setuju" : "Tidak Setuju" }}@endif')
				->addColumn('status_usulan_madya', '@if(is_null($status_madya))-@else{{ $status_madya == "setuju" ? "Setuju" : "Tidak Setuju" }}@endif')
				->addColumn('link', '<a href="#">Html Column</a>')
				->addColumn('intro', '<div class="dropdown">
							<button class="btn btn-sm btn-default btn-action dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-ellipsis-h"></i></button>
							<ul class="dropdown-menu"><li><a href="{{ url("verifikasi-usulan")."/".$id }}">Verifikasi Formasi</a></li><li><a href="{{ url("riwayat-eformasi")."/".$id }}">Riwayat</a></li>@if(Auth::user()->role == "superadmin")<li><a href="#" data-toggle="modal" data-target="#modal-default{{ $id }}">Hapus</a></li>@endif
							</ul></div><div class="modal fade" id="modal-default{{ $id }}">
							<div class="modal-dialog" style="width:30%">
							<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Hapus Pengusulan Formasi</h4>
							</div>
							<div class="modal-body">
								<p>Apakah Anda yakin menghapus usulan formasi ini?</p>
							</div>
							<div class="modal-footer">
								<a href="{{ url("hapus-pengusulan-formasi")."/".$id }}" type="button" class="btn btn-primary pull-left">HAPUS</a>
								<button type="button" class="btn btn-default" data-dismiss="modal">BATAL</button>
							</div>
						</div>
					</div>
				</div>', 2)
				->rawColumns(['link', 'intro','status_usulan_pertama','status_usulan_muda','status_usulan_madya'])
				->make(true);
	}

    public function indexLkpp()
    {
        if(Auth::user()->role == 'dsp')
        {
            return Redirect::back();
        }
		
        $eformasi = DB::table('pengusulan_eformasis')
					->join('users', 'pengusulan_eformasis.id_admin_ppk', '=', 'users.id')
					->join('instansis', 'users.nama_instansi', '=', 'instansis.id')
					->select('pengusulan_eformasis.*', 'users.email as emails', 'users.no_telp as no_telps', 'users.username as usernames', 'instansis.nama as instansis','users.name as namas')
					->orderBy('pengusulan_eformasis.updated_at', 'DESC')
					->where('pengusulan_eformasis.deleted_at',null)
					->get();
        
        return View::make('pengusulan_formasi', compact('eformasi'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function create()
    {
        $id = Auth::user()->id;
        $data1 = DB::table('pengusulan_eformasis')
				 ->join('users', 'pengusulan_eformasis.id_admin_ppk', '=', 'users.id')
				 ->select('pengusulan_eformasis.*', 'users.email as emails', 'users.nama_instansi as instansis', 'users.no_telp as no_telps')
				 ->where('pengusulan_eformasis.id_admin_ppk',$id)
				 ->where('pengusulan_eformasis.deleted_at',null)
				 ->orderBy('pengusulan_eformasis.id', 'DESC')
				 ->first();

        if($data1 != ""){
			if(in_array('tidak_setuju',[$data1->status_muda,$data1->status_madya,$data1->status_pertama]) || in_array('',[$data1->status_muda,$data1->status_madya,$data1->status_pertama])){
				return Redirect::back();
			}
        }
        
		return View::make('pengusulan_eformasi');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array('pertama' => 'required|numeric',
					   'muda' => 'required|numeric',
					   'madya' => 'required|numeric',
					   'dokumen_hasil_perhitungan_abk' => 'required|mimes:jpg,jpeg,pdf|min:100|max:2048',
					   'surat_usulan' => 'required|mimes:jpg,jpeg,pdf|min:100|max:2048',
					   'no_surat' => 'required');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('tambah-pengusulan-eformasi')
                ->withErrors($validator)
                ->withInput(Input::all());
        }

        if ($request->has('dokumen_hasil_perhitungan_abk')) {
            $rules_dokumen = array('dokumen_hasil_perhitungan_abk' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048');
            $validator_dokumen = Validator::make(Input::all(), $rules_dokumen);
            if (!$validator_dokumen->fails()) {
                $dokumen = $request->file('dokumen_hasil_perhitungan_abk');
                $dokumen_name = Carbon::now()->timestamp ."_dokumen_abk".".". $dokumen->getClientOriginalExtension();
                $destination_dokumen = 'storage/data/dokumen_hasil_perhitungan_abk';
                $dokumen->move($destination_dokumen,$dokumen_name);
            } else {
                $dokumen_name = '-';
            }
        } else {
            $dokumen_name = '-';
        }

        if ($request->has('surat_usulan')) {
            $rules_surat = array('surat_usulan' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048');
            $validator_surat = Validator::make(Input::all(), $rules_surat);
            if (!$validator_surat->fails()) {
                $surat = $request->file('surat_usulan');
                $surat_name = Carbon::now()->timestamp ."_surat_usulan".".". $surat->getClientOriginalExtension();
                $destination_surat = 'storage/data/surat_usulan';
                $surat->move($destination_surat,$surat_name);
            } else {
                $surat_name = '-';
            }
        } else {
            $surat_name = '-';
        }

        $data = new PengusulanEformasi();
        $data->id_admin_ppk = Auth::user()->id;
        $data->pertama = $request->input('pertama');
        $data->muda = $request->input('muda');
        $data->madya = $request->input('madya');
        $data->dokumen_hasil_perhitungan_abk = $dokumen_name;
        $data->surat_usulan_rekomendasi = $surat_name;
        $data->no_surat_usulan_rekomendasi = $request->input('no_surat');
        
		if($data->save()){
            $riwayat = new RiwayatUser();
            $riwayat->id_admin = Auth::user()->id;
            $riwayat->perihal = "eformasi";
            $riwayat->description = "mengajukan usulan formasi";
            $riwayat->Keterangan = "Jumlah JF PPBJ Pertama : ".$data->pertama.", Jumlah JF PPBJ Muda : ".$data->muda.", Jumlah JF PPBJ Madya : ".$data->madya.".";
            $riwayat->tanggal = Carbon::now()->toDateString(); 
            $riwayat->save();
            
            $from = env('MAIL_USERNAME');
            $data = array('pertama' => $request->input('pertama'), 'madya' => $request->input('madya'),  'muda' => $request->input('muda'), 'email' => Auth::user()->email, 'name' => Auth::user()->name, 'from' => $from);
            $admin_sp = DB::table('users')->where('role','superadmin')->get();
            $admin_bp = DB::table('users')->where('role','bangprof')->get();
            
			Mail::send('mail.eformasi', $data, function($message) use ($data) {
				$message->to($data['email'], $data['name'])->subject('Pengusulan Formasi');
				$message->from($data['from'],'LKPP');
            });

            foreach($admin_sp as $lkpps){
                Mail::send('mail.eformasi_lkpp', $data, function($message) use ($data,$lkpps) {
					$message->to($lkpps->email)->subject('Pengusulan Formasi');
					$message->from($data['from'],'LKPP');
                });
            }
			
            foreach($admin_bp as $lkpps){
                Mail::send('mail.eformasi_lkpp', $data, function($message) use ($data,$lkpps) {
					$message->to($lkpps->email)->subject('Pengusulan Formasi');
					$message->from($data['from'],'LKPP');
                });
            }
			
            return Redirect::to('pengusulan-eformasi')->with('msg','berhasil');
        } else {
            return Redirect::to('pengusulan-eformasi')
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showFormasi($id)
    {
        $data = PengusulanEformasi::find($id);
        return View::make('lihat_formasi', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eformasi = DB::table('pengusulan_eformasis')
					->join('users', 'pengusulan_eformasis.id_admin_ppk', '=', 'users.id')
					->join('instansis', 'users.nama_instansi', '=', 'instansis.id')
					->select('pengusulan_eformasis.*', 'users.email as emails', 'instansis.nama as instansis', 'users.no_telp as no_telps', 'users.name as names', 'users.sk_admin_ppk as sk_admins', 'users.nip as nips', 'users.jabatan as jabatans', 'users.nama_satuan as nama_satuans')
					->orderBy('pengusulan_eformasis.id', 'desc')
					->where('pengusulan_eformasis.id',$id)
					->first();
    
		return View::make('verifikasi_usulan', compact('eformasi'));
    }

    public function editFormasi($id)
    {
        $data = PengusulanEformasi::find($id);
        return View::make('edit_formasi', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
	public function update(Request $request, $id)
    {
        $data = PengusulanEformasi::find($id);
        $data->status_pertama = $request->input('status_pertama');
        $data->status_muda = $request->input('status_muda');
        $data->status_madya = $request->input('status_madya');
        $data->deskripsi_pertama = $request->input('deskripsi_pertama');
        $data->deskripsi_muda = $request->input('deskripsi_muda');
        $data->deskripsi_madya = $request->input('deskripsi_madya');
        $statusP = $request->input('status_pertama'); 
        $statusm = $request->input('status_muda'); 
        $statusM = $request->input('status_madya'); 
        if($data->save()){
            if ($statusP == "setuju" && $statusm == "setuju" && $statusM == "setuju") {
                $riwayat = new RiwayatUser();
                $riwayat->id_admin = $data->id_admin_ppk;
                $riwayat->id_admin_lkpp = Auth::user()->id;
                $riwayat->perihal = "verifikasi_eformasi";
                $riwayat->description = "Pengusulan formasi inpassing disetujui.";
                $riwayat->keterangan = "Jumlah JF PPBJ Pertama : ".$data->pertama.", Jumlah JF PPBJ Muda : ".$data->muda.", Jumlah JF PPBJ Madya : ".$data->madya.".";
                $riwayat->tanggal = Carbon::now()->toDateString(); 
                $riwayat->save();
            } elseif($statusP == "tidak_setuju" && $statusP == "tidak_setuju" && $statusM == "tidak_setuju") {
				$status = "tidak disetujui";
				$riwayat = new RiwayatUser();
				$riwayat->id_admin = $data->id_admin_ppk;
				$riwayat->id_admin_lkpp = Auth::user()->id;
				$riwayat->perihal = "verifikasi_eformasi";
				$riwayat->description = "Pengusulan formasi inpassing ditolak.";
				$riwayat->keterangan = "Jumlah JF PPBJ Pertama : ".$data->pertama." ".$status.", Jumlah JF PPBJ Muda : ".$data->muda." ".$status.", Jumlah JF PPBJ Madya : ".$data->madya." ".$status.".";
				$riwayat->tanggal = Carbon::now()->toDateString(); 
				$riwayat->save();
            }

            $from = env('MAIL_USERNAME');
            $admin_ppk = User::find($data->id_admin_ppk);
            $data = array('status_pertama' => $request->input('status_pertama'), 'status_madya' => $request->input('status_madya'),  'status_muda' => $request->input('status_muda'), 'deskripsi_pertama' => $request->input('deskripsi_pertama'), 'deskripsi_madya' => $request->input('deskripsi_madya'),  'deskripsi_muda' => $request->input('deskripsi_muda'), 'email' => $admin_ppk->email, 'name' => $admin_ppk->name, 'from' => $from);
            $admin_sp = DB::table('users')->where('role','superadmin')->get();
            $admin_bp = DB::table('users')->where('role','bangprof')->get();
            
			Mail::send('mail.update_eformasi', $data, function($message) use ($data) {
				$message->to($data['email'], $data['name'])->subject('Hasil Pengusulan Formasi');
				$message->from($data['from'],'LKPP');
            });
          
            return Redirect::to('pengusulan-eformasi-lkpp')->with('msg','berhasil');
        } else {
            return Redirect::to('pengusulan-eformasi-lkpp')->with('msg','gagal');
        }
    }

    public function updateFormasi(Request $request,$id)
    {
        $rules = array('pertama' => 'required|numeric',
					   'muda' => 'required|numeric',
					   'madya' => 'required|numeric',
					   'dokumen_hasil_perhitungan_abk' => 'required|mimes:jpg,jpeg,pdf|min:100|max:2048',
					   'surat_usulan' => 'required|mimes:jpg,jpeg,pdf|min:100|max:2048',
					   'no_surat' => 'required');
		
        $message = [
            'dokumen_hasil_perhitungan_abk.min' => array('file' => 'Dokumen Hasil Perhitungan ABK min. 100kb maks. 2MB'),
            'dokumen_hasil_perhitungan_abk.max' => array('file' => 'Dokumen Hasil Perhitungan ABK min. 100kb maks. 2MB'),
            'surat_usulan.min' => array('file' => 'Surat Usulan Mengikuti Inpassing min. 100kb maks. 2MB'),
            'surat_usulan.max' => array('file' => 'Surat Usulan Mengikuti Inpassing min. 100kb maks. 2MB'),
            'dokumen_hasil_perhitungan_abk.required' => 'Dokumen Hasil Perhitungan ABK wajib diisi',
            'surat_usulan.required' => 'Surat Usulan wajib diisi',
            'no_surat.required' => 'No Surat tidak boleh kosong',
        ];

        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput(Input::all());
        }

        if ($request->has('dokumen_hasil_perhitungan_abk')) {
            $rules_dokumen = array('dokumen_hasil_perhitungan_abk' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048');
            $validator_dokumen = Validator::make(Input::all(), $rules_dokumen);
            if (!$validator_dokumen->fails()) {
                $dokumen = $request->file('dokumen_hasil_perhitungan_abk');
                $dokumen_name = Carbon::now()->timestamp ."_dokumen_abk".".". $dokumen->getClientOriginalExtension();
                $destination_dokumen = 'storage/data/dokumen_hasil_perhitungan_abk';
                $dokumen->move($destination_dokumen,$dokumen_name);
            } else {
                $dokumen_name = '-';
            }
        } else {
            $dokumen_name = $request->input('old_dokumen_hasil_perhitungan_abk');
        }

        if ($request->has('surat_usulan')) {
            $rules_surat = array('surat_usulan' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048');
            $validator_surat = Validator::make(Input::all(), $rules_surat);
            if (!$validator_surat->fails()) {
                $surat = $request->file('surat_usulan');
                $surat_name = Carbon::now()->timestamp ."_surat_usulan".".". $surat->getClientOriginalExtension();
                $destination_surat = 'storage/data/surat_usulan';
                $surat->move($destination_surat,$surat_name);
            } else {
                $surat_name = '-';
            }
        } else {
            $surat_name = $request->input('old_surat_usulan');
        }

        $data = PengusulanEformasi::find($id);
        $data->id_admin_ppk = Auth::user()->id;
        $data->pertama = $request->input('pertama');
        $data->muda = $request->input('muda');
        $data->madya = $request->input('madya');
        $data->dokumen_hasil_perhitungan_abk = $dokumen_name;
        $data->surat_usulan_rekomendasi = $surat_name;
        $data->no_surat_usulan_rekomendasi = $request->input('no_surat');
        if($data->save()){
            $riwayat = new RiwayatUser();
            $riwayat->id_admin = Auth::user()->id;
            $riwayat->perihal = "eformasi";
            $riwayat->description = "mengubah usulan formasi";
            $riwayat->Keterangan = "Jumlah JF PPBJ Pertama : ".$data->pertama.", Jumlah JF PPBJ Muda : ".$data->muda.", Jumlah JF PPBJ Madya : ".$data->madya.".";
            $riwayat->tanggal = Carbon::now()->toDateString(); 
            $riwayat->save();
			$from = env('MAIL_USERNAME');
            $data = array('pertama' => $request->input('pertama'), 'madya' => $request->input('madya'),  'muda' => $request->input('muda'), 'email' => Auth::user()->email, 'name' => Auth::user()->name, 'from' => $from);
            $admin_sp = DB::table('users')->where('role','superadmin')->get();
            $admin_bp = DB::table('users')->where('role','bangprof')->get();
            
			Mail::send('mail.eformasi_ubah', $data, function($message) use ($data) {
				$message->to($data['email'], $data['name'])->subject('Ubah Pengusulan Formasi');
				$message->from($data['from'],'LKPP');
            });
			
            foreach($admin_sp as $lkpps){
                Mail::send('mail.eformasi_lkpp_ubah', $data, function($message) use ($data,$lkpps) {
					$message->to($lkpps->email)->subject('Ubah Pengusulan Formasi');
					$message->from($data['from'],'LKPP');
                });
            }
			
            foreach($admin_bp as $lkpps){
                Mail::send('mail.eformasi_lkpp_ubah', $data, function($message) use ($data,$lkpps) {
					$message->to($lkpps->email)->subject('Ubah Pengusulan Formasi');
					$message->from($data['from'],'LKPP');
                });
            }
			
            return Redirect::to('pengusulan-eformasi')->with('msg','berhasil');
        } else {
            return Redirect::to('pengusulan-eformasi')
                ->withInput();
        }
    }

    public function pUlang($id)
    {
        $data = PengusulanEformasi::find($id);
        return View::make('pengajuan_ulang', compact('data'));
    }

    public function updatepUlang(Request $request,$id)
    {
        $rules = array(
            'pertama'    => 'required|numeric',
            'muda'    => 'required|numeric',
            'madya'    => 'required|numeric',
            'dokumen_hasil_perhitungan_abk' => 'required|mimes:jpg,jpeg,pdf|min:100|max:2048',
            'surat_usulan' => 'required|mimes:jpg,jpeg,pdf|min:100|max:2048',
            'no_surat'    => 'required'
        );

        $message = [
            'dokumen_hasil_perhitungan_abk.min' => array(
                'file' => 'Dokumen Hasil Perhitungan ABK min. 100kb maks. 2MB'
            ),
            'dokumen_hasil_perhitungan_abk.max' => array(
                'file' => 'Dokumen Hasil Perhitungan ABK min. 100kb maks. 2MB'
            ),
            'surat_usulan.min' => array(
                'file' => 'Surat Usulan Mengikuti Inpassing min. 100kb maks. 2MB'
            ),
            'surat_usulan.max' => array(
                'file' => 'Surat Usulan Mengikuti Inpassing min. 100kb maks. 2MB'
            ),
            'dokumen_hasil_perhitungan_abk.required' => 'Dokumen Hasil Perhitungan ABK wajib diisi',
            'surat_usulan.required' => 'Surat Usulan wajib diisi',
            'no_surat.required' => 'No Surat tidak boleh kosong',
        ];

        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //upload 1
        if ($request->hasFile('dokumen_hasil_perhitungan_abk')) {
            $dokumen_hasil_perhitungan_abk = $request->file('dokumen_hasil_perhitungan_abk');
            $dokumen_hasil_perhitungan_abk_name = Carbon::now()->timestamp ."dokumen_hasil_perhitungan_abk".".". $dokumen_hasil_perhitungan_abk->getClientOriginalExtension();
            $dokumen_hasil_perhitungan_abkPath = 'storage/data/dokumen_hasil_perhitungan_abk';
            $dokumen_hasil_perhitungan_abk->move($dokumen_hasil_perhitungan_abkPath,$dokumen_hasil_perhitungan_abk_name);
        } else {
            $dokumen_hasil_perhitungan_abk_name = $request->input('old_dokumen_hasil_perhitungan_abk');
        }

        //upload 2
        if ($request->hasFile('surat_usulan')) {
            $surat_usulan = $request->file('surat_usulan');
            $surat_usulan_name = Carbon::now()->timestamp ."surat_usulan".".". $surat_usulan->getClientOriginalExtension();
            $surat_usulanPath = 'storage/data/surat_usulan';
            $surat_usulan->move($surat_usulanPath,$surat_usulan_name);
        } else {
            $surat_usulan_name = $request->input('old_surat_usulan');
        }

        $data = PengusulanEformasi::find($id);
        if($request->input('status_pertama') == 'tidak_setuju'){
            $data->pertama = $request->input('pertama');
            $data->status_pertama = null;
        }
		
        if($request->input('status_muda') == 'tidak_setuju'){
            $data->muda = $request->input('muda');
            $data->status_muda = null;
        }
        
		if($request->input('status_madya') == 'tidak_setuju'){
            $data->madya = $request->input('madya');
            $data->status_madya = null;
        }

        $data->no_surat_usulan_rekomendasi = $request->input('no_surat');
        $data->dokumen_hasil_perhitungan_abk = $dokumen_hasil_perhitungan_abk_name;
        $data->surat_usulan_rekomendasi = $surat_usulan_name;

        if($data->save()){
            $riwayat = new RiwayatUser();
            $riwayat->id_admin = Auth::user()->id;
            $riwayat->perihal = "eformasi";
            $riwayat->description = "mengajukan usulan ulang formasi";
            $riwayat->Keterangan = "Jumlah JF PPBJ Pertama : ".$data->pertama.", Jumlah JF PPBJ Muda : ".$data->muda.", Jumlah JF PPBJ Madya : ".$data->madya.".";
            $riwayat->tanggal = Carbon::now()->toDateString(); 
            $riwayat->save();
            $from = env('MAIL_USERNAME');
            $data = array('pertama' => $request->input('pertama'), 'madya' => $request->input('madya'),  'muda' => $request->input('muda'), 'email' => Auth::user()->email, 'name' => Auth::user()->name, 'from' => $from);
            $admin_sp = DB::table('users')->where('role','superadmin')->get();
            $admin_bp = DB::table('users')->where('role','bangprof')->get();
            
			Mail::send('mail.ulang_eformasi', $data, function($message) use ($data) {
				$message->to($data['email'], $data['name'])->subject('Pengusulan ulang Eformasi');
				$message->from($data['from'],'LKPP');
            });
			
			foreach($admin_sp as $lkpps){
                Mail::send('mail.eformasi_lkpp_update', $data, function($message) use ($data,$lkpps) {
					$message->to($lkpps->email)->subject('Pengusulan ulang Formasi');
					$message->from($data['from'],'LKPP');
                });
            }
			
            foreach($admin_bp as $lkpps){
                Mail::send('mail.eformasi_lkpp_update', $data, function($message) use ($data,$lkpps) {
					$message->to($lkpps->email)->subject('Pengusulan ulang Formasi');
					$message->from($data['from'],'LKPP');
                });
            }
			
            return Redirect::to('pengusulan-eformasi')->with('msg','berhasil_ulang');
        } else {
            return Redirect::to('pengusulan-eformasi')->with('msg','gagal_ulang');
        }
    }

    public function bUlang($id)
    {
        $data = PengusulanEformasi::find($id);
        return View::make('berkas_ulang', compact('data'));
    }

    public function updatebUlang(Request $request,$id)
    {
        $data = PengusulanEformasi::find($id);
        if ($request->has('dokumen_hasil_perhitungan_abk')) {
            $rules_dokumen = array(
                'dokumen_hasil_perhitungan_abk' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            );
            $validator_dokumen = Validator::make(Input::all(), $rules_dokumen);
            if (!$validator_dokumen->fails()) {
                $dokumen = $request->file('dokumen_hasil_perhitungan_abk');
                $dokumen_name = Carbon::now()->timestamp ."_dokumen_abk".".". $dokumen->getClientOriginalExtension();
                $destination_dokumen = 'storage/data/dokumen_hasil_perhitungan_abk';
                $dokumen->move($destination_dokumen,$dokumen_name);
                $data->dokumen_hasil_perhitungan_abk = $dokumen_name;
            }
        }

        if ($request->has('surat_usulan')) {
            $rules_surat = array(
                'surat_usulan' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
            );
            $validator_surat = Validator::make(Input::all(), $rules_surat);
            if (!$validator_surat->fails()) {
                $surat = $request->file('surat_usulan');
                $surat_name = Carbon::now()->timestamp ."_surat_usulan".".". $surat->getClientOriginalExtension();
                $destination_surat = 'storage/data/surat_usulan';
                $surat->move($destination_surat,$surat_name);
                $data->surat_usulan_rekomendasi = $surat_name;
            }
        }

        if($data->save()){
            // $riwayat = new RiwayatUser();
            // $riwayat->id_admin = Auth::user()->id;
            // $riwayat->perihal = "eformasi";
            // $riwayat->description = "mengajukan usulan formasi";
            // $riwayat->Keterangan = "Jumlah JF PPBJ Pertama : ".$data->pertama.", Jumlah JF PPBJ Muda : ".$data->muda.", Jumlah JF PPBJ Madya : ".$data->madya.".";
            // $riwayat->tanggal = Carbon::now()->toDateString(); 
            // $riwayat->save();
            // $data = array('pertama' => $request->input('pertama'), 'madya' => $request->input('madya'),  'muda' => $request->input('muda'), 'email' => Auth::user()->email, 'name' => Auth::user()->name);
            // $email_lkpp = array();
            // $admin_lkpp = DB::table('users')->where('admin_type','lkpp')->get();
            // foreach($admin_lkpp as $lkpps){
            //     $email_lkpp = $lkpps->email;
            // }
            // Mail::send('mail.eformasi', $data, function($message) use ($data) {
            // $message->to($data['email'], $data['name'])->subject('Pengusulan Formasi');
            // $message->from('lkpp@globaldeva.com','LKPP');
            // });

            // Mail::send('mail.eformasi_lkpp', $data, function($message) use ($data,$email_lkpp) {
            // $message->to($email_lkpp)->subject('Pengusulan Formasi');
            // $message->from('lkpp@globaldeva.com','LKPP');
            // });
            return Redirect::to('pengusulan-eformasi')->with('msg','berhasil_berkas');
        }else{
            return Redirect::to('pengusulan-eformasi')->with('msg','gagal_berkas');
        }
    }

    public function riwayat($id)
    {
        $data = PengusulanEformasi::find($id);
        $riwayat = DB::table('riwayat_users')
				   ->join('users','riwayat_users.id_admin','=','users.id')
				   ->select('riwayat_users.*','users.name as namas')
				   ->where('id_admin',$data->id_admin_ppk)
				   ->orderBy('id','DESC')->get();

        return View::make('riwayat_eformasi_lkpp', compact('riwayat'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->role == 'bangprof' || Auth::user()->role == 'dsp')
        {
            return Redirect::back();
        }
		
        $data = PengusulanEformasi::find($id);
        $data->deleted_at = Carbon::now();
        $surat_1 = "storage/data/surat_usulan/".$data->suarat_usulan_rekomendasi;
        $dokumen = "storage/data/dokumen_hasil_perhitungan_abk/".$data->dokumen_hasil_perhitungan_abk;
        
		if(File::exists($surat_1)) {
            File::delete($surat_1);
        }
        
		if(File::exists($dokumen)) {
            File::delete($dokumen);
        }

        if($data->delete()){
            return Redirect::to('pengusulan-eformasi-lkpp')->with('msg','berhasil_hapus');
        }else{
            return Redirect::to('pengusulan-eformasi-lkpp')->with('msg','gagal_hapus');
        }
    }

    public function printExcell()
    {
        $nama_excel = 'pengusulan eformasi lkpp.xlsx';
        return Excel::download(new PengusulanEformasiExport(), $nama_excel);
    }
}
