<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\ImportUjian;
use App\PesertaJadwal;
use Redirect;
use Validator;
use View;
use Image;
use File;
use DB;

class ImportUjianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('import_hasil_ujian');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $metode = $request->input('metode');
        if ($metode == "verifikasi") {
            $rules = array(
                'tanggal_inpassing'    => 'required',
                'upload_file_portofolio'    => 'required|mimes:jpg,png,jpeg,pdf',
            );
        }

        if ($metode == "tertulis") {
            $rules = array(
                'tanggal_inpassing'    => 'required',
                'upload_file_tertulis'    => 'required',
            );
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('import-hasil-ujian')
                ->withErrors($validator)
                ->withInput();
        }

        if ($metode == "tertulis") {
        	$lines_2 = $request->file('upload_file_tertulis');
        	if($lines_2->getClientOriginalExtension() !== 'sql'){
			    return Redirect::to('import-hasil-ujian')
                ->with('msg','file_salah')
                ->withInput();
			}else{
				$lines_2 = $request->file('upload_file_tertulis');
				$templine = '';
				// Read in entire file
				$lines = file($lines_2);
				// Loop through each line
				foreach ($lines as $line)
				{
					// Skip it if it's a comment
					if (substr($line, 0, 2) == '--' || $line == '')
					    continue;

					// Add this line to the current segment
					$templine .= $line;
					// If it has a semicolon at the end, it's the end of the query
					if (substr(trim($line), -1, 1) == ';')
					{
					    // Perform the query
					    // mysql_query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
                        try {
                           $pisah = explode(' ', $templine);
                            if ($pisah[2] == 'tabelnilai') {
					            // DB::select(DB::raw($templine));
                                $pisah_value = explode('values', $templine);
                                $value_2 = str_replace("NULL","'NULL'",$pisah_value[1]);
                                $rest_1 = substr($pisah_value[0], 24, -2);
                                $rest_2 = substr($value_2, 3, -4);
                                $pisah_rest_1 = explode(',', $rest_1);
                                $pisah_rest_2 = explode("','", $rest_2);
                                $jum_rest = count($pisah_rest_1);
                                $jum_rest_2 = count($pisah_rest_2);
                                // for ($i=0; $i < $jum_rest ; $i++) { 
                                //     echo $pisah_rest_1[$i].' : '.$pisah_rest_2[$i].' no:'.$i.' <br>';
                                // }
                                //update user yang lulus
                                if ($pisah_rest_2[17] == 'LULUS') {
                                    $table_ujian = DB::table('peserta_jadwals')
                                        ->join('pesertas','peserta_jadwals.id_peserta','=','pesertas.id')
                                        ->select('peserta_jadwals.*','pesertas.nip')
                                        ->whereNull('pesertas.deleted_at')
                                        ->where('peserta_jadwals.id_jadwal',$pisah_rest_2[29])
                                        ->where('pesertas.nip',$pisah_rest_2[28])
                                        ->first();
                                    // dd($table_ujian);
							
                                    if ($table_ujian) {
                                        $update_ujian = PesertaJadwal::find($table_ujian->id);
                                        $update_ujian->status_ujian = 'lulus';
                                        $update_ujian->no_seri_sertifikat = strtolower($pisah_rest_2[25]);
                                        $update_ujian->save();
                                        // dd($update_ujian);
                                    }
                                }

                                if ($pisah_rest_2[17] == 'TIDAK LULUS') {
                                    $table_ujian = DB::table('peserta_jadwals')
                                        ->join('pesertas','peserta_jadwals.id_peserta','=','pesertas.id')
                                        ->select('peserta_jadwals.*','pesertas.nip')
                                        ->whereNull('pesertas.deleted_at')
                                        ->where('peserta_jadwals.id_jadwal',$pisah_rest_2[29])
                                        ->where('pesertas.nip',$pisah_rest_2[28])
                                        ->first();
                     
                                    if ($table_ujian) {
                                        $update_ujian = PesertaJadwal::find($table_ujian->id);
                                        $update_ujian->status_ujian = 'tidak_lulus';
                                        $update_ujian->no_seri_sertifikat = strtolower($pisah_rest_2[25]);
                                        $update_ujian->save();
                                    }
                                }
                            }else{
                                return Redirect::to('import-hasil-ujian')->with('gagal_sql','file sql tidak sesuai');
                            }
					    // Reset temp variable to empty
					    $templine = '';
                        } catch(\Illuminate\Database\QueryException $ex){ 
                          // dd($ex->getMessage()); 
                            return Redirect::to('import-hasil-ujian')->with('gagal_sql',$ex->getMessage());
                          // Note any method of class PDOException can be called on $ex.
                        }
					}
				}

                // $table_ujian = DB::table('peserta_jadwals')
                // ->join('pesertas','peserta_jadwals.id_peserta','=','pesertas.id')
                // ->select('peserta_jadwals.*','pesertas.nip')
                // ->whereNull('pesertas.deleted_at')
                // ->get();
                // dd($table_ujian);
	        }
		}else{
			if ($request->hasFile('upload_file_portofolio')) {
		        $upload_file_portofolio = $request->file('upload_file_portofolio');
		        $upload_file_portofolio_name = Carbon::now()->timestamp ."_upload_file_portofolio".".". $upload_file_portofolio->getClientOriginalExtension();
		        $upload_file_portofolioPath = 'storage/data/upload_file_portofolio';
		        $upload_file_portofolio->move($upload_file_portofolioPath,$upload_file_portofolio_name);
		    }
		}

		$data = new ImportUjian();
		$tanggal = explode('/', $request->input('tanggal_inpassing'));
		$data->tanggal = $tanggal[2].'-'.$tanggal[0].'-'.$tanggal[1];
		$data->metode = $request->input('metode');
		$data->id_admin = Auth::user()->id;
		if ($metode == "verifikasi") {
			$data->hasil_scan_portofolio = $upload_file_portofolio_name;
		}
		if ($data->save()) {
			return Redirect::to('import-hasil-ujian')->with('msg','berhasil_input');
		}else{
			return Redirect::to('import-hasil-ujian')->with('msg','gagal_input');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cekTanggal(Request $request)
    {
        $tanggal = $request->get('tanggal');
        $pisah_tanggal = explode('/', $tanggal);
        $fix_tanggal = $pisah_tanggal[2].'-'.$pisah_tanggal[0].'-'.$pisah_tanggal[1];
        $jum = 0;
		$data = DB::table('jadwals')
				->where('tanggal_ujian',$fix_tanggal)
				->whereNull('deleted_at')
				->count();
				
		$data_jadwal_instansi = DB::table('jadwal_instansis')
								->where('tanggal_tes',$fix_tanggal)
								->whereNull('deleted_at')
								->count();										
		
		if($data != 0){
			$jum = 1;
			$data_banyak = DB::table('jadwals')
								->where('tanggal_ujian',$fix_tanggal)
								->whereNull('deleted_at')
								->get();
		} else if ($data_jadwal_instansi != 0){
			$jum = 2;
			$data_banyak = DB::table('jadwal_instansis')
								->where('tanggal_tes',$fix_tanggal)
								->whereNull('deleted_at')
								->get();
		}
		
		
		
		//if ($data != 0) {
		if ($jum != 0) {
			$tipe_ujian = [];
            
			foreach ($data_banyak as $key => $datas) {
                $tipe_ujian[$key] = $datas->metode;
            }
			
            if (in_array('tes_tulis', $tipe_ujian) && in_array('verifikasi_portofolio', $tipe_ujian)) {
                $tipe_get = 'dua_metode';
            } else {
                $tipe_get = $data_banyak[0]->metode;
            }
			
            return response()->json(['status' => true,'tipe_ujian' => $tipe_get]);
        } else {
            return response()->json(['status' => false,'tanggal' => $fix_tanggal]);
        }
    }
}
