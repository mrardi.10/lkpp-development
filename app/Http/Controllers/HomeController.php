<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use View;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard()
    {
        return View::make('dashboard_lkpp');
    }

    public function pindah()
    {
        return View::make('pindah_jadwal');
    }

    public function login()
    {
        return view('login');
    }

    public function FileView($path,$file)
    {
        $pathToFile = storage_path('data'."/".$path."/".$file);
        return response()->file($pathToFile);
    }

    
}
