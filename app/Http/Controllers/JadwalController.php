<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Exports\PesertaJadwalExport;
use Maatwebsite\Excel\Facades\Excel;
use App\User;
use App\Jadwal;
use App\Peserta;
use App\PesertaJadwal;
use App\RiwayatUser;
use App\JudulInput;
use App\DetailInput;
use App\DetailFileInput;
use App\StatusFilePortofolio;
use App\SimpanBerkasPortofolio;
use App\SimpanBerkasPortofolioHistory;
use App\DokumenPortofolio;
use App\DokumenPortofolioA;
use App\DokumenPortofolioB;
use App\DokumenPortofolioC;
use App\DokumenPortofolioD;
use App\Dokumen;
use App\SuratUsulan;
use App\RiwayatVerif;
use Carbon\Carbon;
use Redirect;
use Validator;
use Helper;
use View;
use Image;
use File;
use Mail;
use DB;
use PDF;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bulan = $request->input('bulan');        

        if (Auth::user()->role == "verifikator") {
			$pesertaAs = DB::table('pesertas')
						 ->leftJoin('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta')
						 ->where('pesertas.deleted_at',null)
						 ->where('pesertas.assign',Auth::user()->id)
						 ->get();

			if (count($pesertaAs) == 0) {
				if ($bulan == "") {
					  $jadwal = DB::table('jadwals')
							    ->leftJoin('peserta_jadwals', function($leftJoin)
							    {
									$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
									->on('peserta_jadwals.status', '=', DB::raw("'1'"));
								})
								->leftJoin('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
								->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"),'pesertas.*','jadwals.id as id')
								->where('jadwals.deleted_at',null)
								->whereIn('jadwals.id',[])
								->groupBy('jadwals.id')
								->orderBy('jadwals.tanggal_ujian','DESC')
								->get();
				} else {
					$jadwal = DB::table('jadwals')
							  ->leftJoin('peserta_jadwals', function($leftJoin){
									$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
									->on('peserta_jadwals.status', '=', DB::raw("'1'"));
							  })
							  ->leftJoin('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
							  ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"),'pesertas.*','jadwals.id as id')
							  ->where('jadwals.deleted_at',null)
							  ->whereIn('jadwals.id',[])
						      ->groupBy('jadwals.id')
						      ->orderBy('jadwals.tanggal_ujian','DESC')
							  ->whereMonth('tanggal_ujian', '=', $bulan)
							  ->get();
				}
			} else {
				foreach ($pesertaAs as $pesertas) {
					$listss[] = $pesertas->id_jadwal;
				}
				
				if ($bulan == "") {
					$jadwal = DB::table('jadwals')
							  ->leftJoin('peserta_jadwals', function($leftJoin)
							  {
								  $leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
								  ->on('peserta_jadwals.status', '=', DB::raw("'1'"));
							  })
							  ->leftJoin('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
							  ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"),'pesertas.*','jadwals.id as id')
							  ->where('jadwals.deleted_at',null)
							  ->whereIn('jadwals.id',$listss)
							  ->groupBy('jadwals.id')
							  ->orderBy('jadwals.tanggal_ujian','DESC')
							  ->get();
				} else {
					$jadwal = DB::table('jadwals')
							  ->leftJoin('peserta_jadwals', function($leftJoin)
							 {
								$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
								->on('peserta_jadwals.status', '=', DB::raw("'1'"));
							 })
							 ->leftJoin('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
							 ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"),'pesertas.*','jadwals.id as id')
							 ->where('jadwals.deleted_at',null)
							 ->whereIn('jadwals.id',$listss)
							 ->groupBy('jadwals.id')
							 ->orderBy('jadwals.tanggal_ujian','DESC')
							 ->whereMonth('tanggal_ujian', '=', $bulan)
							 ->get();
				}
			}
        }

        if (Auth::user()->role == "asesor") {
			$pesertaAs = DB::table('pesertas')
						->leftJoin('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta')
						->where('pesertas.deleted_at',null)
						->where('pesertas.asesor',Auth::user()->id)
						->get();

			if (count($pesertaAs) == 0) {
				if ($bulan == "") {
					$jadwal = DB::table('jadwals')
							  ->leftJoin('peserta_jadwals', function($leftJoin)
							  {
								  $leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
								  ->on('peserta_jadwals.status', '=', DB::raw("'1'"));
							  })
							  ->leftJoin('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
							  ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"),'pesertas.*','jadwals.id as id')
							  ->where('jadwals.deleted_at',null)
							  ->whereIn('jadwals.id',[])
							  ->groupBy('jadwals.id')
							  ->orderBy('jadwals.tanggal_ujian','DESC')
							  ->get();
				} else {
					$jadwal = DB::table('jadwals')
							  ->leftJoin('peserta_jadwals', function($leftJoin)
							  {
								$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
								->on('peserta_jadwals.status', '=', DB::raw("'1'"));
							  })
							  ->leftJoin('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
							  ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"),'pesertas.*','jadwals.id as id')
							  ->where('jadwals.deleted_at',null)
							  ->whereIn('jadwals.id',[])
							  ->groupBy('jadwals.id')
							  ->orderBy('jadwals.tanggal_ujian','DESC')
							  ->whereMonth('tanggal_ujian', '=', $bulan)
							  ->get();
				}
			} else {
				foreach ($pesertaAs as $pesertas) {
                    $listss[] = $pesertas->id_jadwal;
				}
				
				if ($bulan == "") {
					$jadwal = DB::table('jadwals')
							  ->leftJoin('peserta_jadwals', function($leftJoin)
							  {
								  $leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
								  ->on('peserta_jadwals.status', '=', DB::raw("'1'"));
							  })
							  ->leftJoin('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
							  ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"),'pesertas.*','jadwals.id as id')
							  ->where('jadwals.deleted_at',null)
							  ->whereIn('jadwals.id',$listss)
							  ->groupBy('jadwals.id')
							  ->orderBy('jadwals.tanggal_ujian','DESC')
							  ->get();
                } else {
					$jadwal = DB::table('jadwals')
							  ->leftJoin('peserta_jadwals', function($leftJoin)
							  {
								$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
								->on('peserta_jadwals.status', '=', DB::raw("'1'"));
							  })
							  ->leftJoin('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
							  ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"),'pesertas.*','jadwals.id as id')
							  ->where('jadwals.deleted_at',null)
							  ->whereIn('jadwals.id',$listss)
							  ->groupBy('jadwals.id')
							  ->orderBy('jadwals.tanggal_ujian','DESC')
							  ->whereMonth('tanggal_ujian', '=', $bulan)
						      ->get();
                }
			}    
        }

        if (Auth::user()->role != "verifikator" && Auth::user()->role != "asesor") {
            if ($bulan == "") {
                $jadwal = DB::table('jadwals')
						  ->leftJoin('peserta_jadwals', function($leftJoin)
						  {
								$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
								->on('peserta_jadwals.status', '=', DB::raw("'1'"));
						  })
						  ->leftJoin('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
						  ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"),'pesertas.*','jadwals.id as id')
						  ->where('jadwals.deleted_at',null)
						  ->groupBy('jadwals.id')
						  ->orderBy('jadwals.tanggal_ujian','DESC')
						  ->get();
            } else {
                $jadwal = DB::table('jadwals')
						  ->leftJoin('peserta_jadwals', function($leftJoin)
							{
								$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
								->on('peserta_jadwals.status', '=', DB::raw("'1'"));
							})
							->leftJoin('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
							->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"),'pesertas.*','jadwals.id as id')
							->where('jadwals.deleted_at',null)
							->groupBy('jadwals.id')
							->orderBy('jadwals.tanggal_ujian','DESC')
							->whereMonth('tanggal_ujian', '=', $bulan)
							->get();
            }
        }
		
        return View::make('jadwal_inpassing', compact('jadwal','bulan'));
    }

    public function indexPpk(Request $request)
    {
        $bulan = $request->input('bulan');
        if ($bulan == "") {
            $jadwal = DB::table('jadwals')
					->leftJoin('peserta_jadwals', function($leftJoin)
					{
					 $leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
					 ->on('peserta_jadwals.status', '=', DB::raw("'1'"));
					})
					->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"))
					->where('deleted_at',null)
					->groupBy('jadwals.id')
					->orderBy('jadwals.tanggal_ujian','desc')
					->where('publish_jadwal','ya')
					->get();
					
			$notifikasiRekomendasi = DB::table('riwayat_users')
									->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
									->join('users', 'riwayat_users.id_admin','=','users.id')
									->select('riwayat_users.*','pesertas.nama as nama_peserta')
									->whereIn('perihal',['hasil_verif_regular','hasil_verif_regular_dsp'])
									->where('id_admin',Auth::user()->id)    
									->orderBy('id','desc')
									->groupBy('riwayat_users.id')
									->offset(0)
									->limit(10)
									->get();
        } else {
            $jadwal = DB::table('jadwals')
					  ->leftJoin('peserta_jadwals', function($leftJoin)
				  	  {
						  $leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
						  ->on('peserta_jadwals.status', '=', DB::raw("'1'"));
					  })
					  ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"))
					  ->where('deleted_at',null)
				 	  ->groupBy('jadwals.id')
					  ->orderBy('jadwals.tanggal_ujian','desc')
					  ->whereMonth('tanggal_ujian', '=', $bulan)
					  ->where('publish_jadwal','ya')
					  ->get();
					  
			$notifikasiRekomendasi = DB::table('riwayat_users')
									->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
									->join('users', 'riwayat_users.id_admin','=','users.id')
									->select('riwayat_users.*','pesertas.nama as nama_peserta')
									->whereIn('perihal',['hasil_verif_regular','hasil_verif_regular_dsp'])
									->where('id_admin',Auth::user()->id)    
									->orderBy('id','desc')
									->groupBy('riwayat_users.id')
									->offset(0)
									->limit(10)
									->get();
        }

        return View::make('jadwal_ujian_lkpp', compact('jadwal','bulan','notifikasiRekomendasi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        if (Auth::user()->role == 'bangprof'){
            return Redirect::back();
        }

        return View::make('add_jadwal_inpassing');
    }

    public function addPeserta($id)
    {
        $jadwal = Jadwal::find($id);
        $id_admin = Auth::user()->id;
        $dokumen = DokumenPortofolio::all();

        $terdaftar = PesertaJadwal::where('id_admin_ppk',$id_admin);
        if($terdaftar->count() > 0){
            //Ini yang perlu diperbaiki
            $data = $terdaftar->where('id_jadwal',$id)->get();
        } else {
            $data = "";
        }

        $peserta_jadwal_terdaftar = DB::table('peserta_jadwals')
									->where('id_jadwal','!=',$id)
									->where('status',1)
									->get();
		
        $peserta_jadwal_terdaftar_sama = DB::table('peserta_jadwals')
										 ->where('id_jadwal',$id)
										 ->where('status_ujian',NULL)
										 ->where('status_ujian','tidak_lulus')
										 ->where('status_ujian','lulus')
										 ->where('publish',NULL)
										 ->where('status',1)
										 ->get();
        
		$peserta_jadwal_terdaftar_sama_tl = DB::table('peserta_jadwals')
											->where('id_jadwal',$id)
											->where('status',1)
											->get();

		$peserta_jadwal_terdaftar_sama_tl_int = DB::table('peserta_instansis')
											->where('id_admin_ppk',Auth::user()->id)
											->where('id_jadwal',$id)
											->where('status',1)
											->get();
        
        $peserta_jadwal_tdlulus = DB::table('peserta_jadwals')
								  ->where('peserta_jadwals.id_jadwal','!=',$id)
								  ->whereIn('status_ujian',['tidak_lulus','tidak_lengkap'])
								  ->where('publish','publish')
								  ->get();


        $peserta_jadwal_tdlulus_porto = DB::table('peserta_jadwals')
										->where('peserta_jadwals.id_jadwal','!=',$id)
										->whereIn('status_ujian',['tidak_lulus','tidak_lengkap'])
										->where('publish','publish')
										->get();

        $peserta_jadwal_tdlulus_porto_instansi = DB::table('peserta_instansis')
												 ->where('id_admin_ppk',Auth::user()->id)
												 ->where('peserta_instansis.id_jadwal','!=',$id)
												 ->whereIn('status_ujian',['tidak_lulus','tidak_lengkap','tidak_hadir'])
												 ->where('publish','publish')
												 ->get();

        $peserta_jadwal_tdhadir = DB::table('peserta_jadwals')
								  ->where('peserta_jadwals.id_jadwal','!=',$id)
								  ->where('status_ujian','tidak_hadir')
								  ->where('publish','publish')
								  ->get();

        $peserta_jadwal_tdhadir_porto = DB::table('peserta_jadwals')
										->where('peserta_jadwals.id_jadwal','!=',$id)
										->where('status_ujian','tidak_hadir')
										->where('publish','publish')
										->get();


        $peserta_jadwal = DB::table('peserta_jadwals')
						  ->where('id_jadwal','!=',$id)
						  ->where('status_ujian','lulus')
						  ->where('status_ujian',NULL)
					  	  ->get();

        $peserta_instansi_terdaftar = DB::table('peserta_instansis')
									->where('id_admin_ppk',Auth::user()->id)
									->where('id_jadwal','!=',$id)
									->where('status',1)
									->get();

        $peserta_instansi = DB::table('peserta_instansis')
							->where('id_admin_ppk',Auth::user()->id)
							->where('status_ujian','lulus')
							->where('status_ujian',NULL)
							->get();

        //untuk mengambil data ID peserta
        $jml_pjs = count($peserta_jadwal);
        $id_data = "";

        foreach ($peserta_jadwal as $key => $pjs) {
            $id_data .= $pjs->id_peserta;
            if($key != $jml_pjs - 1){
                $id_data .= ",";
            }
			
            if($key == $jml_pjs - 1){
                $id_data .= "";
            }
        }

        $jml_pjs_sm = count($peserta_jadwal_terdaftar_sama);
        $id_data_sm = "";

        foreach ($peserta_jadwal_terdaftar_sama as $key => $pjs) {
            $id_data_sm .= $pjs->id_peserta;
            if($key != $jml_pjs_sm - 1){
                $id_data_sm .= ",";
            }
            
			if($key == $jml_pjs_sm - 1){
                $id_data_sm .= "";
            }
        }

        $jml_pjs_tl_porto_int = count($peserta_jadwal_tdlulus_porto_instansi);
        $id_data_tl_porto_int = "";

        foreach ($peserta_jadwal_tdlulus_porto_instansi as $key => $pjs) {
            $id_data_tl_porto_int .= $pjs->id_peserta;
            if($key != $jml_pjs_tl_porto_int - 1){
                $id_data_tl_porto_int .= ",";
            }
            
			if($key == $jml_pjs_tl_porto_int - 1){
                $id_data_tl_porto_int .= "";
            }
        }

        $jml_pjs_tl = count($peserta_jadwal_tdlulus);
        $id_data_tl = "";

        foreach ($peserta_jadwal_tdlulus as $key => $pjs) {
            $id_data_tl .= $pjs->id_peserta;
            if($key != $jml_pjs_tl - 1){
                $id_data_tl .= ",";
            }
            
			if($key == $jml_pjs_tl - 1){
                $id_data_tl .= "";
            }
        }

        $jml_pjs_tl_porto = count($peserta_jadwal_tdlulus_porto);
        $id_data_tl_porto = "";

        foreach ($peserta_jadwal_tdlulus_porto as $key => $pjs) {
            $id_data_tl_porto .= $pjs->id_peserta;
            if($key != $jml_pjs_tl_porto - 1){
                $id_data_tl_porto .= ",";
            }
            
			if($key == $jml_pjs_tl_porto - 1){
                $id_data_tl_porto .= "";
            }
        }

        $jml_pjs_th = count($peserta_jadwal_tdhadir);
        $id_data_th = "";

        foreach ($peserta_jadwal_tdhadir as $key => $pjs) {
            $id_data_th .= $pjs->id_peserta;
            if($key != $jml_pjs_th - 1){
                $id_data_th .= ",";
            }

            if($key == $jml_pjs_th - 1){
                $id_data_th .= "";
            }
        }

        $jml_pjs_th_porto = count($peserta_jadwal_tdhadir_porto);
        $id_data_th_porto = "";

        foreach ($peserta_jadwal_tdhadir_porto as $key => $pjs) {
            $id_data_th_porto .= $pjs->id_peserta;
            if($key != $jml_pjs_th_porto - 1){
                $id_data_th_porto .= ",";
            }

            if($key == $jml_pjs_th_porto - 1){
                $id_data_th_porto .= "";
            }
        }

        $jml_pjs_tl_sm = count($peserta_jadwal_terdaftar_sama_tl);
        $id_data_tl_sm = "";

        foreach ($peserta_jadwal_terdaftar_sama_tl as $key => $pjs) {
            $id_data_tl_sm .= $pjs->id_peserta;
            if($key != $jml_pjs_tl_sm - 1){
                $id_data_tl_sm .= ",";
            }

            if($key == $jml_pjs_tl_sm - 1){
                $id_data_tl_sm .= "";
            }
        }

        $jml_pjs_tl_sm_int = count($peserta_jadwal_terdaftar_sama_tl_int);
        $id_data_tl_sm_int = "";

        foreach ($peserta_jadwal_terdaftar_sama_tl_int as $key => $pjs) {
            $id_data_tl_sm_int .= $pjs->id_peserta;
            if($key != $jml_pjs_tl_sm_int - 1){
                $id_data_tl_sm_int .= ",";
            }

            if($key == $jml_pjs_tl_sm_int - 1){
                $id_data_tl_sm_int .= "";
            }
        }

        //untuk mengambil data ID peserta yang terdaftar
        $jml_pjs_tdf = count($peserta_jadwal_terdaftar);
        $id_data_tdf = "";

        foreach ($peserta_jadwal_terdaftar as $key => $pjs) {
            $id_data_tdf .= $pjs->id_peserta;
            if($key != $jml_pjs_tdf - 1){
                $id_data_tdf .= ",";
            }

            if($key == $jml_pjs_tdf - 1){
                $id_data_tdf .= "";
            }
        }

        //untuk mengambil data ID peserta Instansi yang terdaftar
        $jml_pjs_int_tdf = count($peserta_instansi_terdaftar);
        $id_data_int_tdf = "";

        foreach ($peserta_instansi_terdaftar as $key => $pjs) {
            $id_data_int_tdf .= $pjs->id_peserta;
            if($key != $jml_pjs_int_tdf - 1){
                $id_data_int_tdf .= ",";
            }

            if($key == $jml_pjs_int_tdf - 1){
                $id_data_int_tdf .= "";
            }
        }

        //untuk mengambil data ID peserta Instansi
        $jml_pjs_int = count($peserta_instansi);
        $id_data_int = "";
        foreach ($peserta_instansi as $key => $pjs) {
            $id_data_int .= $pjs->id_peserta;
            if($key != $jml_pjs_int - 1){
                $id_data_int .= ",";
            }

            if($key == $jml_pjs_int - 1){
                $id_data_int .= "";
            }
        }
		
        $ex_data = explode(",", $id_data);
        $ex_data_tl = explode(",", $id_data_tl);
        $ex_data_tl_porto = explode(",", $id_data_tl_porto);
        $ex_data_tl_porto_int = explode(",", $id_data_tl_porto_int);
        $ex_data_th = explode(",", $id_data_th);
        $ex_data_th_porto = explode(",", $id_data_th_porto);
        $ex_data_tdf = explode(",", $id_data_tdf);
        $ex_data_sm = explode(",", $id_data_sm);
        $ex_data_sm_tl = explode(",", $id_data_tl_sm);
        $ex_data_sm_tl_int = explode(",", $id_data_tl_sm_int);
        $ex_data_int = explode(",", $id_data_int);
        $ex_data_int_tdf = explode(",", $id_data_int_tdf);

        $status_boleh = array(1,3);
        //Kondisi Awal
       
        $peserta = DB::table('pesertas')
					->where('id_admin_ppk', $id_admin)
					->where('deleted_at',null)
					->where('status_peserta','aktif')
					->whereIn('status_inpassing', $status_boleh)
					->whereNotIn('id', $ex_data)
					->whereNotIn('id', $ex_data_int)
					->get();
        
        //Kondisi Tidak Lulus
        $status_tl = array(7,17);
        $gabungan = array_merge($ex_data_tl,$ex_data_tl_porto_int);
        $pesertaTidakLulus = DB::table('pesertas')
							->where('pesertas.id_admin_ppk', $id_admin)
							->where('pesertas.deleted_at',null)
							->where('status_peserta','aktif')
							->whereIn('pesertas.status_inpassing', $status_tl)
							->whereIn('pesertas.id', $gabungan)  
							->get();

        //Kondisi Tidak Lulus Portofolio (Bisa muncul apabila setelah melewati 60 hari dari tanggal ujian terakhir)
        $status_tl_1 = array(7,17);
        $gabungan1 = array_merge($ex_data_tl_porto,$ex_data_tl_porto_int);
        $pesertaTidakLulus_porto = DB::table('pesertas')
									->where('pesertas.id_admin_ppk', $id_admin)
									->where('pesertas.deleted_at',null)
									->where('status_peserta','aktif')
									->whereIn('pesertas.status_inpassing', $status_tl_1)
									->whereIn('pesertas.id', $gabungan) 
									->get();

        //Kondisi Tidak Hadir 
        $status_th = array(11);
        $gabungan2 = array_merge($ex_data_th,$ex_data_tl_porto_int);
        $pesertaTidakHadir = DB::table('pesertas')
							->where('pesertas.id_admin_ppk', $id_admin)
							->where('pesertas.deleted_at',null)
							->where('status_peserta','aktif')
							->whereIn('pesertas.status_inpassing', $status_th)
							->whereIn('pesertas.id', $gabungan2)
							->get();

        //Kondisi Tidak Hadir Portofolio (Bisa muncul apabila setelah melewati 60 hari dari tanggal ujian terakhir)
        $status_th_1 = array(11);
        $gabungan3 = array_merge($ex_data_th_porto,$ex_data_tl_porto_int);
        
		$pesertaTidakHadir_porto = DB::table('pesertas')
									->where('pesertas.id_admin_ppk', $id_admin)
									->where('pesertas.deleted_at',null)
									->where('status_peserta','aktif')
									->whereIn('pesertas.status_inpassing', $status_th_1)
									->whereIn('pesertas.id', $gabungan3)
									->get();

        //Kondisi Terdaftar dijadwal ujian yang tidak muncul selain jadwal yang didaftarkan
        $status_terdaftar = array(12,13);
        $pesertaTerdaftar = DB::table('pesertas')
							->where('id_admin_ppk', $id_admin)
							->where('deleted_at',null)
							->where('status_peserta','aktif')
							->whereIn('status_inpassing', $status_terdaftar)
							->whereNotIn('id', $ex_data_tdf)
							->whereNotIn('id', $ex_data_int_tdf)
							->get();

        $status_terdaftar_usulan = array(16);
        $pesertaTerdaftar_usul = DB::table('pesertas')
								->where('id_admin_ppk', $id_admin)
								->where('deleted_at',null)
								->where('status_peserta','aktif')
								->whereIn('status_inpassing', [16])
								->whereIn('id', $ex_data_sm_tl)
								->get();

        //Kondisi Terdaftar (Dokumen Persyaratan Tidak Lengkap) dijadwal ujian yang tidak muncul selain jadwal yang didaftarkan
        $status_terdaftar_tl = array(2);
        $pesertaTerdaftar_tl = DB::table('pesertas')
								->where('id_admin_ppk', $id_admin)
								->where('deleted_at',null)
								->where('status_peserta','aktif')
								->whereIn('status_inpassing', $status_terdaftar_tl)
								->whereNotIn('id', $ex_data_tdf)
								->whereNotIn('id', $ex_data_int_tdf)
								->get();

        //Kondisi Terdaftar (Dokumen Persyaratan Tidak Lengkap) dijadwal ujian yang tidak muncul di jadwal yang didaftarkan
        $status_terdaftar_tl_sama = array(2);
        $pesertaTerdaftar_tl_sama = DB::table('pesertas')
									->where('id_admin_ppk', $id_admin)
									->where('deleted_at',null)
									->where('status_peserta','aktif')
									->whereIn('status_inpassing', $status_terdaftar_tl_sama)
									->whereNotIn('id', $ex_data_sm_tl)
									// ->whereNotIn('id', $ex_data_sm_tl_int)
									->get();

        //Kondisi Terdaftar dijadwal ujian yang muncul di jadwal yang didaftarkan
        $status_terdaftar_sm = array(12,13);
        $pesertaTerdaftar_sm = DB::table('pesertas')
								->where('id_admin_ppk', $id_admin)
								->where('deleted_at',null)
								->where('status_peserta','aktif')
								->whereIn('status_inpassing', $status_terdaftar_sm)
								->whereIn('id', $ex_data_sm)
								->get();

        $status_terdaftar_usul = array(16);
        $pesertaTerdaftar_usulan = DB::table('pesertas')
									->where('id_admin_ppk', $id_admin)
									->where('deleted_at',null)
									->where('status_peserta','aktif')
									->whereIn('status_inpassing', $status_terdaftar_usul)
									->whereIn('id', $ex_data_sm)
									->get();

        //Kondisi Terdaftar dijadwal ujian yang muncul di jadwal yang didaftarkan
        $status_terdaftar_sm_tl = array(12,13);
        $pesertaTerdaftar_sm_tl = DB::table('pesertas')
									->where('id_admin_ppk', $id_admin)
									->where('deleted_at',null)
									->where('status_peserta','aktif')
									->whereIn('status_inpassing', $status_terdaftar_sm_tl)
									->whereIn('id', $ex_data_sm_tl)
									->get();
        
        $notifikasiRekomendasi = DB::table('riwayat_users')
								->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
								->join('users', 'riwayat_users.id_admin','=','users.id')
								->select('riwayat_users.*','pesertas.nama as nama_peserta')
								->where('perihal','jadwal_reguller')
								->where('description','di rekomendasikan tidak lulus Verifikasi Portofolio oleh')        
								->orderBy('id','desc')
								->limit(15)
								->get();

        $cek_peserta = $terdaftar->where('metode_ujian','verifikasi')->select('id_peserta','id_portofolio')->get();

        return View::make('store_peserta_ujian', compact('jadwal','peserta','pesertaTerdaftar','pesertaTidakLulus_porto','pesertaTidakHadir_porto','pesertaTidakHadir','pesertaTerdaftar_tl','pesertaTerdaftar_sm_tl','pesertaTerdaftar_sm','pesertaTerdaftar_tl_sama','pesertaTidakLulus','data','dokumen','cek_peserta','notifikasiRekomendasi','peserta_jadwal_tdlulus','peserta_jadwal_tdlulus_porto','pesertaTerdaftar_usulan','pesertaTerdaftar_usul'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $metode = $request->input('metode_ujian');
		
        if ($metode == "verifikasi_portofolio") {
            $rules = array('tanggal_verifikasi' => 'required',
						   'waktu_verifikasi' => 'required',
						   'kapasitas' => 'required',
						   'batas_waktu_input' => 'required',
						   'publish_jadwal' => 'required',
						   'nama_cp_1' => 'required',
						   'no_telp_cp_1' => 'required',
						   'nama_cp_2' => 'required',
						   'no_telp_cp_2' => 'required');
        }

        if ($metode == "tes_tulis") {
            $rules = array('tanggal_tes' => 'required',
						   'waktu_ujian' => 'required',
						   'lokasi_ujian' => 'required',
						   'jumlah_ruang' => 'required',
						   'kapasitas' => 'required',
						   'jumlah_unit' => 'required',
						   'batas_waktu_input' => 'required',
						   'publish_jadwal' => 'required',
						   'nama_cp_1' => 'required',
						   'no_telp_cp_1' => 'required',
						   'nama_cp_2' => 'required',
						   'no_telp_cp_2' => 'required');
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('tambah-jadwal-inpassing')
			->withErrors($validator)
            ->withInput();
        }

        $jadwal = new Jadwal();
        $jadwal->metode = $metode;

        if ($request->input('tanggal_tes') != "") {
            $tgl_tes = explode('/',$request->input('tanggal_tes'));
            $tgl_tes_expl = $tgl_tes[2].'-'.$tgl_tes[1].'-'.$tgl_tes[0];
        }
		
        if ($request->input('tanggal_verifikasi') != "") {
            $tgl_ver = explode('/',$request->input('tanggal_verifikasi'));
            $tgl_ver_expl = $tgl_ver[2].'-'.$tgl_ver[1].'-'.$tgl_ver[0];
        }
        
		$batas_waktu_input = explode('/',$request->input('batas_waktu_input'));
        $tgl_bts_expl = $batas_waktu_input[2].'-'.$batas_waktu_input[1].'-'.$batas_waktu_input[0];

        if ($metode == "tes_tulis") {
			if ($tgl_tes_expl < $tgl_bts_expl) {
				return Redirect::to('tambah-jadwal-inpassing')
				->with('msg','gagal')
				->withInput();
			}
		} elseif ($metode == "verifikasi_portofolio") {
			if ($tgl_ver_expl < $tgl_bts_expl ) {
				return Redirect::to('tambah-jadwal-inpassing')
				->with('msg','gagal')
				->withInput();

			}
		}

		if ($request->input('tanggal_verifikasi') != "") {
			$tanggal_verifikasi = explode('/',$request->input('tanggal_verifikasi'));
			$jadwal->tanggal_verifikasi = $tanggal_verifikasi[2].'-'.$tanggal_verifikasi[1].'-'.$tanggal_verifikasi[0];
			$jadwal->tanggal_ujian = $tanggal_verifikasi[2].'-'.$tanggal_verifikasi[1].'-'.$tanggal_verifikasi[0];
		}

		$jadwal->waktu_verifikasi = $request->input('waktu_verifikasi');
		
		if ($request->input('tanggal_tes') != "") {
			$tanggal_tes = explode('/',$request->input('tanggal_tes'));
			$jadwal->tanggal_tes = $tanggal_tes[2].'-'.$tanggal_tes[1].'-'.$tanggal_tes[0];
			$jadwal->tanggal_ujian = $tanggal_tes[2].'-'.$tanggal_tes[1].'-'.$tanggal_tes[0];
		}

		$jadwal->waktu_ujian = $request->input('waktu_ujian');

		if ($metode == "tes_tulis") {
			$jadwal->lokasi_ujian = $request->input('lokasi_ujian');
			$jadwal->jumlah_ruang = $request->input('jumlah_ruang');
			$jadwal->jumlah_unit = $request->input('jumlah_unit');
		} else {
			$jadwal->lokasi_ujian = '-';
			$jadwal->jumlah_ruang = '-';
			$jadwal->jumlah_unit = $request->input('kapasitas');
		}

		$jadwal->kapasitas = $request->input('kapasitas');
		$batas_waktu_input = explode('/',$request->input('batas_waktu_input'));
		$jadwal->batas_waktu_input = $batas_waktu_input[2].'-'.$batas_waktu_input[1].'-'.$batas_waktu_input[0];
		$jadwal->publish_jadwal = $request->input('publish_jadwal');
		$jadwal->nama_cp_1 = $request->input('nama_cp_1');
		$jadwal->no_telp_cp_1 = $request->input('no_telp_cp_1');
		$jadwal->nama_cp_2 = $request->input('nama_cp_2');
		$jadwal->no_telp_cp_2 = $request->input('no_telp_cp_2');
    
		if($jadwal->save()){
			return Redirect::to('jadwal-inpassing')->with('msg','berhasil');
		} else {
			return Redirect::to('jadwal-inpassing')->with('msg','gagal');
		}
	}

	public function storePeserta(Request $request,$id)
	{
		$cek_pernah = PesertaJadwal::where('id_jadwal',$id)->where('id_admin_ppk',Auth::user()->id)->count();
		$metode = $request->input('metode');
		$id_peserta = $request->input('daftarkan');
		$id_peserta_tdk = $request->input('tidak_daftar');
		$nama_peserta = $request->input('nama_peserta');
		
		//Ini yang perlu diperbaiki
		$cek_berkas = SimpanBerkasPortofolio::where('id_jadwal',$request->input('id_jadwal'))
					  ->where('id_peserta',$id_peserta)
					  ->count();

		if($id_peserta == ""){
			foreach($id_peserta_tdk as $id_pesertas){
				if($metode == 'tes_tulis' || $metode == 'verifikasi_portofolio'){
					$cek_peserta = PesertaJadwal::where('id_jadwal',$request->input('id_jadwal'))
									->where('id_peserta',$id_pesertas)->where('status',1)
									->get();
				
					if ($cek_peserta == "") {
						return Redirect::back()->with('msg','metode_kosong');
					}
				}
			}
		}

		if($metode == 'verifikasi_portofolio' && $id_peserta != "" && $cek_berkas == 0){
			return Redirect::back()->with('msg','validated_berkas');
		}

		if ($id_peserta_tdk != "") {
			foreach ($id_peserta_tdk as $id_pesertass) {
				if ($metode == 'tes_tulis') {
					$id_admin = $request->input('id_admin_ppk');

					//Ini yang perlu diperbaiki
					$cek = PesertaJadwal::where('id_jadwal',$id)
							->where('id_admin_ppk',$id_admin)
							->where('id_peserta',$id_pesertass)
							->delete();
					
					if ($cek) {
						$id_status = 3;
						$status = "Dokumen Persyaratan Lengkap";
						$id_status_1 = 2;
						$status_1 = "Dokumen Persyaratan Tidak Lengkap";
						$dok_usulan = SuratUsulan::where('id_peserta',$id_pesertass)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();

						if ($dok_usulan) {
							$dok_usulan->id_jadwal = 0;
							$dok_usulan->jenis_ujian = null;
							$dok_usulan->save();
						}

						$peserta_sts = Peserta::find($id_pesertass);
						if ($peserta_sts->verifikasi_berkas == "not_verified") {
							$peserta_sts->status = $status_1;
							$peserta_sts->status_inpassing = $id_status_1;
							$peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
							$peserta_sts->save();
						} elseif ($peserta_sts->verifikasi_berkas == null ) {
							$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
							$peserta_sts->status_inpassing = 1;
							$peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
							$peserta_sts->save();
						} elseif ($peserta_sts->verifikasi_berkas == "verified") {
							$peserta_sts->status = $status;
							$peserta_sts->status_inpassing = $id_status;
							$peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
							$peserta_sts->save();
						}

						$riwayat = new RiwayatUser();
						$riwayat->id_user = $id_pesertass;
						$riwayat->id_admin = Auth::user()->id;
						$tanggal_ujian_rw = $request->input('tanggal_ujian');
						$riwayat->perihal = "jadwal_reguller";
						$riwayat->description = "sudah tidak terdaftar pada ujian dengan metode Tes Tertulis tanggal ".Helper::tanggal_indo($tanggal_ujian_rw)." oleh";
						$riwayat->tanggal = Carbon::now()->toDateString(); 
						$riwayat->save();
						$msg = "berhasil";
					}
				}
				
				if($metode == 'verifikasi_portofolio'){
					$id_admin = $request->input('id_admin_ppk');
					$cek_peserta = PesertaJadwal::where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',$id_admin)->where('id_peserta',$id_pesertass)
					->count();

					if($cek_peserta==0){
						$msg = "berhasil";
					} else {
						$peserta_jadwal = PesertaJadwal::where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',$id_admin)->where('id_peserta',$id_pesertass)->first();					
						$dok_usulan = SuratUsulan::where('id_peserta',$id_pesertass)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();
						
						if ($dok_usulan) {
							$dok_usulan->id_jadwal = 0;
							$dok_usulan->jenis_ujian = null;
							$dok_usulan->save();
						}

						$pesertajadwal = PesertaJadwal::find($peserta_jadwal->id);
						$pesertajadwal->status = 0;
						$tanggal_ujian_rw = $request->input('tanggal_ujian');

						if ($pesertajadwal->save()) {
							$id_status = 3;
							$status = "Dokumen Persyaratan Lengkap";
							$id_status_1 = 2;
							$status_1 = "Dokumen Persyaratan Tidak Lengkap";
							$peserta_sts = Peserta::find($id_pesertass);
							
							if ($peserta_sts->verifikasi_berkas == "not_verified") {
								$peserta_sts->status = $status_1;
								$peserta_sts->status_inpassing = $id_status_1;
								$peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
								$peserta_sts->save();
							} elseif ($peserta_sts->verifikasi_berkas == null ) {
								$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
								$peserta_sts->status_inpassing = 1;
								$peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
								$peserta_sts->save();
							} elseif ($peserta_sts->verifikasi_berkas == "verified") {
								$peserta_sts->status = $status;
								$peserta_sts->status_inpassing = $id_status;
								$peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));
								$peserta_sts->save();
							}

							$riwayat = new RiwayatUser();
							$riwayat->id_user = $id_pesertass;
							$riwayat->id_admin = Auth::user()->id;
							$tanggal_ujian_rw = $request->input('tanggal_ujian');
							$riwayat->perihal = "jadwal_reguller";
							$riwayat->description = "sudah tidak terdaftar pada ujian dengan metode Verifikasi Portofolio tanggal ".Helper::tanggal_indo($tanggal_ujian_rw)." oleh";
							$riwayat->tanggal = Carbon::now()->toDateString(); 
							$riwayat->save();
							$msg = "berhasil";
						}
					}
				}
			}
		}

		if($id_peserta != ""){
			$id_admin = $request->input('id_admin_ppk');
			
			foreach($id_peserta as $id_pesertas){
				$peserta = Peserta::find($id_pesertas);
				if($metode == 'tes_tulis'){
					$cek_peserta = PesertaJadwal::where('id_jadwal',$request->input('id_jadwal'))
									->where('id_peserta',$id_pesertas)
									->count();

					$jumlah = DB::table('jadwals')
					->leftJoin('peserta_jadwals', function($leftJoin)
					{
						$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
						->on('peserta_jadwals.status', '=', DB::raw("'1'"));
					})
					->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"))
					->groupBy('jadwals.id')
					->where('publish_jadwal','ya')
					->where('jadwals.id',$id)
					->first();

					$jumlah_peserta = $jumlah->jumlah_peserta;
					$maks = $jumlah->kapasitas;
            
					if($cek_peserta == 0){
						if($jumlah_peserta >= $maks){
							return Redirect::to('tambah-peserta-ujian/'.$id)->with('msg','penuh');
						}

						$data = new PesertaJadwal();
						$data->id_jadwal = $request->input('id_jadwal');
						$data->id_peserta = $id_pesertas;
						$data->id_admin_ppk = Auth::user()->id;
						$data->nama_peserta = $peserta->nama;
						$data->no_ujian = substr($peserta->no_sertifikat,0,9);
						$data->metode_ujian = 'tes';
						$data->status = 1;
						$dok_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',0)->where('jenis_ujian',null)->where('id_admin_ppk',Auth::user()->id)->first();
						
						if ($dok_usulan) {
							$dok_usulan->id_jadwal = $request->input('id_jadwal');
							$dok_usulan->jenis_ujian = 'regular';
							$dok_usulan->save();
						}
               
						if($data->save()){
							$id_status = 13;
							$tanggal_ujian_rw = $request->input('tanggal_ujian');
							$lokasi_ujian_rw = $request->input('lokasi_ujian');
							$status = "Menunggu Tes Tertulis";
							$peserta_sts = Peserta::find($id_pesertas);

							$dokumen_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();
							
							if ($peserta_sts->status_inpassing == 2) {
								$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap" ;
								$peserta_sts->status_inpassing = 2;
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');								
								$peserta_sts->save();
							} elseif ($peserta_sts->status_inpassing == 1 ) {
								$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
								$peserta_sts->status_inpassing = 1;
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								$peserta_sts->save();
							} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")) {
							   $peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
							   $peserta_sts->status_inpassing = 16;
							   $peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							   $peserta_sts->save();
						   } elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
							   $peserta_sts->status = $status;
							   $peserta_sts->status_inpassing = $id_status;
							   $peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							   $peserta_sts->save();
							}

							$riwayat = new RiwayatUser();
							$riwayat->id_user = $id_pesertas;
							$riwayat->id_admin = Auth::user()->id;
							$riwayat->perihal = "jadwal_reguller";
							$riwayat->description = "didaftarkan ujian dengan metode Tes Tertulis tanggal ".Helper::tanggal_indo($tanggal_ujian_rw)." oleh";
							$riwayat->tanggal = Carbon::now()->toDateString(); 
							$riwayat->save();
							$msg = "berhasil";
						} else {
							$msg = "gagal";
						}
					} else {
						$peserta_jadwal = PesertaJadwal::where('id_jadwal',$request->input('id_jadwal'))
										  ->where('id_peserta',$id_pesertas)->where('status',1)->first();

						$peserta_jadwal_upl = PesertaJadwal::where('id_jadwal',$request->input('id_jadwal'))
											  ->where('id_peserta',$id_pesertas)->where('status',2)->first();

						$jumlah = DB::table('jadwals')
								->leftJoin('peserta_jadwals', function($leftJoin)
								{
									$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
									->on('peserta_jadwals.status', '=', DB::raw("'1'"));
								})
								->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"))
								->groupBy('jadwals.id')
								->where('publish_jadwal','ya')
								->where('jadwals.id',$id)
								->first();

						$jumlah_peserta = $jumlah->jumlah_peserta;
						$maks = $jumlah->kapasitas;
						
						if ($peserta_jadwal_upl != "") {
						  $pesertajadwal = PesertaJadwal::find($peserta_jadwal_upl->id);
						} else {
						  $pesertajadwal = "";
						}
						
						if ($peserta_jadwal != "") {
							$pesertajadwal_tdf = PesertaJadwal::find($peserta_jadwal->id);
						} else {
							$pesertajadwal_tdf ="";
						}
						
						if ($pesertajadwal != "") {
							if($jumlah_peserta >= $maks){
								return Redirect::to('jadwal-ujian-lkpp')->with('msg','penuh');
							}
							
							$dok_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',0)->where('jenis_ujian',null)->where('id_admin_ppk',Auth::user()->id)->first();
							if ($dok_usulan) {
								$dok_usulan->id_jadwal = $request->input('id_jadwal');
								$dok_usulan->jenis_ujian = 'regular';
								$dok_usulan->save();
							}
							
							$pesertajadwal->status = 1;
							if($pesertajadwal->save()){
							   $id_status = 13;
							   $tanggal_ujian_rw = $request->input('tanggal_ujian');
							   $lokasi_ujian_rw = $request->input('lokasi_ujian');
							   $status = "Menunggu Tes Tertulis";

							   $peserta_sts = Peserta::find($id_pesertas);
							   $dokumen_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();
							   if ($peserta_sts->status_inpassing == 2) {
								$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap" ;
								$peserta_sts->status_inpassing = 2;
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');

								$peserta_sts->save();
								} elseif ($peserta_sts->status_inpassing == 1 ) {
									$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
									$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
									$peserta_sts->status_inpassing = 1;
									$peserta_sts->save();
								} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")){
									$peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
									$peserta_sts->status_inpassing = 16;
									$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
									$peserta_sts->save();
								} elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
									$peserta_sts->status = $status;
									$peserta_sts->status_inpassing = $id_status;
									$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
									$peserta_sts->save();
								}

								$riwayat = new RiwayatUser();
								$riwayat->id_user = $id_pesertas;
								$riwayat->id_admin = Auth::user()->id;
								$riwayat->perihal = "jadwal_reguller";
								$riwayat->description = "didaftarkan ujian dengan metode Tes Tertulis tanggal ".Helper::tanggal_indo($tanggal_ujian_rw)." oleh";
								$riwayat->tanggal = Carbon::now()->toDateString(); 
								$riwayat->save();
								$msg = "berhasil";
							} else {
								$msg = "gagal";
							}
						} elseif($pesertajadwal_tdf != ""){
							$dok_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',0)->where('jenis_ujian',null)->where('id_admin_ppk',Auth::user()->id)->first();
							
							if ($dok_usulan) {
								$dok_usulan->id_jadwal = $request->input('id_jadwal');
								$dok_usulan->jenis_ujian = 'regular';
								$dok_usulan->save();
							}
							
							$pesertajadwal_tdf->status = 1;
							if($pesertajadwal_tdf->save()){
								$id_status = 13;
								$tanggal_ujian_rw = $request->input('tanggal_ujian');
								$lokasi_ujian_rw = $request->input('lokasi_ujian');
								$status = "Menunggu Tes Tertulis";
								$peserta_sts = Peserta::find($id_pesertas);
								$dokumen_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();
								if ($peserta_sts->status_inpassing == 2) {
									$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap" ;
									$peserta_sts->status_inpassing = 2;
									$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
									$peserta_sts->save();
								} elseif ($peserta_sts->status_inpassing == 1 ) {
									$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
									$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
									$peserta_sts->status_inpassing = 1;
									$peserta_sts->save();
								} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")){
								   $peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
								   $peserta_sts->status_inpassing = 16;
								   $peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								   $peserta_sts->save();
								} elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
									$peserta_sts->status = $status;
									$peserta_sts->status_inpassing = $id_status;
									$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
									$peserta_sts->save();
								}
							
								$msg = "berhasil";
							} else {
								$msg = "gagal";
							}
						}
					$msg = "berhasil";
				}
			}

			// update verifikasi peserta
			if($metode == 'verifikasi_portofolio'){
				$cek_peserta = PesertaJadwal::where('id_jadwal',$request->input('id_jadwal'))
								->where('id_peserta',$id_pesertas)
								->count();

				$jumlah = DB::table('jadwals')
				->leftJoin('peserta_jadwals', function($leftJoin)
				{
					$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
					->on('peserta_jadwals.status', '=', DB::raw("'1'"));
				})
				->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"))
				->groupBy('jadwals.id')
				->where('publish_jadwal','ya')
				->where('jadwals.id',$id)
				->first();

				$jumlah_peserta = $jumlah->jumlah_peserta;
				$maks = $jumlah->kapasitas;
    
				if($cek_peserta == 0){
					if($jumlah_peserta >= $maks){
						return Redirect::to('jadwal-ujian-lkpp')->with('msg','penuh');
					}

					$data = new PesertaJadwal();
					$data->id_jadwal = $request->input('id_jadwal');
					$data->id_peserta = $id_pesertas;
					$data->id_admin_ppk = Auth::user()->id;
					$data->nama_peserta = $peserta->nama;
					$data->no_ujian = $data->no_ujian = substr($peserta->no_sertifikat,0,9);
					$data->metode_ujian = 'verifikasi';
					$data->status  = 1;
					$dok_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',0)->where('jenis_ujian',null)->where('id_admin_ppk',Auth::user()->id)->first();
					
					if ($dok_usulan) {
						$dok_usulan->id_jadwal = $request->input('id_jadwal');
						$dok_usulan->jenis_ujian = 'regular';
						$dok_usulan->save();
					}

					if($data->save()){
						$id_status = 12;
						$tanggal_ujian_rw = $request->input('tanggal_ujian');
						$lokasi_ujian_rw = $request->input('lokasi_ujian');
						$status = "Menunggu Verifikasi Portofolio";
						$peserta_sts = Peserta::find($id_pesertas);
						$dokumen_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();
						
						if ($peserta_sts->status_inpassing == 2) {
							$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap";
							$peserta_sts->status_inpassing = 2;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							$peserta_sts->save();
						} elseif ($peserta_sts->status_inpassing == 1 ) {
							$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
							$peserta_sts->status_inpassing = 1;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							$peserta_sts->save();
						} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")) {
						   $peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
						   $peserta_sts->status_inpassing = 16;
						   $peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
						   $peserta_sts->save();
						} elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
							$peserta_sts->status = $status;
							$peserta_sts->status_inpassing = $id_status;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');					
							$peserta_sts->save();
						}
						
						$riwayat = new RiwayatUser();
						$riwayat->id_user = $id_pesertas;
						$riwayat->id_admin = Auth::user()->id;
						$riwayat->perihal = "jadwal_reguller";
						$riwayat->description = "didaftarkan ujian dengan metode Verifikasi Portofolio tanggal ".Helper::tanggal_indo($tanggal_ujian_rw)." oleh";
						$riwayat->tanggal = Carbon::now()->toDateString(); 
						$riwayat->save();
						$msg = "berhasil";
					} else {
						$msg = "gagal";
					}
				} else {
					$peserta_jadwal = PesertaJadwal::where('id_jadwal',$request->input('id_jadwal'))
									  ->where('id_peserta',$id_pesertas)->where('status',1)->first();

					$peserta_jadwal_upl = PesertaJadwal::where('id_jadwal',$request->input('id_jadwal'))
										  ->where('id_peserta',$id_pesertas)->where('status',2)->first();

					$peserta_jadwal_upl_tdk = PesertaJadwal::where('id_jadwal',$request->input('id_jadwal'))
											  ->where('id_peserta',$id_pesertas)->where('status',0)->first();

					$jumlah = DB::table('jadwals')
							  ->leftJoin('peserta_jadwals', function($leftJoin)
							  {
								  $leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
								  ->on('peserta_jadwals.status', '=', DB::raw("'1'"));
							  })
							  ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"))
							  ->groupBy('jadwals.id')
							  ->where('publish_jadwal','ya')
							  ->where('jadwals.id',$id)
							  ->first();

					$jumlah_peserta = $jumlah->jumlah_peserta;
					$maks = $jumlah->kapasitas;
					
					if ($peserta_jadwal_upl != "") {
						$pesertajadwal = PesertaJadwal::find($peserta_jadwal_upl->id);
					} else {
						$pesertajadwal = "";
					}
					
					if ($peserta_jadwal != "") {
						$pesertajadwal_tdf = PesertaJadwal::find($peserta_jadwal->id);
					} else {
						$pesertajadwal_tdf ="";
					}
					
					if ($peserta_jadwal_upl_tdk != "") {
						$pesertajadwal_tdf_tdk = PesertaJadwal::find($peserta_jadwal_upl_tdk->id);
					} else {
						$pesertajadwal_tdf_tdk ="";
					}
        
					if ($pesertajadwal != "") {
						if($jumlah_peserta >= $maks){
							return Redirect::to('jadwal-ujian-lkpp')->with('msg','penuh');
						}
						
						$dok_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',0)->where('jenis_ujian',null)->where('id_admin_ppk',Auth::user()->id)->first();
						if ($dok_usulan) {
							$dok_usulan->id_jadwal = $request->input('id_jadwal');
							$dok_usulan->jenis_ujian = 'regular';
							$dok_usulan->save();
						}
						
						$pesertajadwal->status = 1;
						if($pesertajadwal->save()){
							$id_status = 12;
							$tanggal_ujian_rw = $request->input('tanggal_ujian');
							$lokasi_ujian_rw = $request->input('lokasi_ujian');
							$status = "Menunggu Verifikasi Portofolio";
							$peserta_sts = Peserta::find($id_pesertas);
							$dokumen_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();
							
							if ($peserta_sts->status_inpassing == 2) {
								$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap" ;
								$peserta_sts->status_inpassing = 2;
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								$peserta_sts->save();
							} elseif ($peserta_sts->status_inpassing == 1 ) {
								$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								$peserta_sts->status_inpassing = 1;
								$peserta_sts->save();
							} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")){
								$peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
								$peserta_sts->status_inpassing = 16;
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								$peserta_sts->save();
							} elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
								$peserta_sts->status = $status;
								$peserta_sts->status_inpassing = $id_status;
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								$peserta_sts->save();
							}

							$riwayat = new RiwayatUser();
							$riwayat->id_user = $id_pesertas;
							$riwayat->id_admin = Auth::user()->id;
							$riwayat->perihal = "jadwal_reguller";
							$riwayat->description = "didaftarkan ujian dengan metode Verifikasi Portofolio tanggal ".Helper::tanggal_indo($tanggal_ujian_rw)." oleh";
							$riwayat->tanggal = Carbon::now()->toDateString(); 
							$riwayat->save();
							$msg = "berhasil";
						} else {
							$msg = "gagal";
						}
					} elseif($pesertajadwal_tdf_tdk != "") {
						if($jumlah_peserta >= $maks){
							return Redirect::to('jadwal-ujian-lkpp')->with('msg','penuh');
						}
						
						$dok_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',0)->where('jenis_ujian',null)->where('id_admin_ppk',Auth::user()->id)->first();
						if ($dok_usulan) {
							$dok_usulan->id_jadwal = $request->input('id_jadwal');
							$dok_usulan->jenis_ujian = 'regular';
							$dok_usulan->save();
						}
						
						$pesertajadwal_tdf_tdk->status = 1;
						if($pesertajadwal_tdf_tdk->save()){
							$id_status = 12;
							$tanggal_ujian_rw = $request->input('tanggal_ujian');
							$lokasi_ujian_rw = $request->input('lokasi_ujian');
							$status = "Menunggu Verifikasi Portofolio";
							$peserta_sts = Peserta::find($id_pesertas);
							$dokumen_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();
							if ($peserta_sts->status_inpassing == 2) {
								$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap" ;
								$peserta_sts->status_inpassing = 2;
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								$peserta_sts->save();
							} elseif ($peserta_sts->status_inpassing == 1 ) {
								$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								$peserta_sts->status_inpassing = 1;
								$peserta_sts->save();
							} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")){
							   $peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
							   $peserta_sts->status_inpassing = 16;
							   $peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							   $peserta_sts->save();
							} elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
								$peserta_sts->status = $status;
								$peserta_sts->status_inpassing = $id_status;
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								$peserta_sts->save();
							}

							$riwayat = new RiwayatUser();
							$riwayat->id_user = $id_pesertas;
							$riwayat->id_admin = Auth::user()->id;
							$riwayat->perihal = "jadwal_reguller";
							$riwayat->description = "didaftarkan ujian dengan metode Verifikasi Portofolio tanggal ".Helper::tanggal_indo($tanggal_ujian_rw)." oleh";
							$riwayat->tanggal = Carbon::now()->toDateString(); 
							$riwayat->save();
							$msg = "berhasil";
						} else {
							$msg = "gagal";
						}
					} elseif($pesertajadwal_tdf != ""){
						$dok_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',0)->where('jenis_ujian',null)->where('id_admin_ppk',Auth::user()->id)->first();
						if ($dok_usulan) {
							$dok_usulan->id_jadwal = $request->input('id_jadwal');
							$dok_usulan->jenis_ujian = 'regular';
							$dok_usulan->save();
						}
						
						$pesertajadwal_tdf->status = 1;
						if($pesertajadwal_tdf->save()){
							$id_status = 12;
							$tanggal_ujian_rw = $request->input('tanggal_ujian');
							$lokasi_ujian_rw = $request->input('lokasi_ujian');
							$status = "Menunggu Verifikasi Portofolio";

							$peserta_sts = Peserta::find($id_pesertas);
							$dokumen_usulan = SuratUsulan::where('id_peserta',$id_pesertas)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();
							$dokumen_usulan = isset($dokumen_usulan) ? $dokumen_usulan : new \stdClass();
							$dokumen_usulan->file = isset($dokumen_usulan->file) ? $dokumen_usulan->file : "";
						   
							if ($peserta_sts->status_inpassing == 2) {
								$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap" ;
								$peserta_sts->status_inpassing = 2;
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								$peserta_sts->save();
							} elseif ($peserta_sts->status_inpassing == 1 ) {
								$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								$peserta_sts->status_inpassing = 1;
								$peserta_sts->save();
							} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")){
							   $peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
							   $peserta_sts->status_inpassing = 16;
							   $peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							   $peserta_sts->save();
							} elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
								$peserta_sts->status = $status;
								$peserta_sts->status_inpassing = $id_status;
								$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
								$peserta_sts->save();
							}
						
							$msg = "berhasil";
						} else {
							$msg = "gagal";
						}
					}
				}
			}
			
			$msg = "berhasil";
			}
		}
	
		$msg = "berhasil";
		return Redirect::to('jadwal-ujian-lkpp')->with('msg',$msg);
	}

	public function pindahJadwal(Request $request,$id,$id_jadwal)
	{
		$metodes = $request->input('metodes');
		$idjadwal = $request->input('id_jadwal');
		$tgl_ujian = $request->input('tgl_uji');
		$id_admin = $request->input('id_admin_ppk');
		$pindah = PesertaJadwal::where('id_jadwal',$id_jadwal)->where('id_admin_ppk',$id_admin)->where('id_peserta',$id)->first();
		$pindahB = SimpanBerkasPortofolio::where('id_jadwal',$id_jadwal)->where('id_peserta',$id)->get();
		$pindahS = StatusFilePortofolio::where('id_jadwal',$id_jadwal)->where('id_peserta',$id)->get();
		$pindahSurat = SuratUsulan::where('id_jadwal',$id_jadwal)->where('id_peserta',$id)->where('jenis_ujian','regular')->get();
		$peserta_sts = Peserta::find($id);
		$dokumen_usulan = SuratUsulan::where('id_peserta',$id)->where('id_jadwal',$id_jadwal)->where('id_admin_ppk',$id_admin)->first();
		
		
		$pindahjadwal = PesertaJadwal::find($pindah->id);
		
		if ($metodes == 'verifikasi_portofolio') {
			$metode = 'Verifikasi Portofolio';
			if ($pindahB != "") {
				foreach ($pindahB as $pindahBerkas) {
					$pindahBerkas->id_jadwal = $idjadwal;       
					$pindahsave = $pindahBerkas->save();
				}
			}
			if ($pindahS != "") {
				foreach ($pindahS as $pindahStatus) {
					$pindahStatus->id_jadwal = $idjadwal;       
					$pindahsaveStatus = $pindahStatus->save();
				}
			}
			if ($pindahSurat != "") {
				foreach ($pindahSurat as $pindahSurat) {
					$pindahSurat->id_jadwal = $idjadwal;       
					$pindahsaveSurat = $pindahSurat->save();
				}
			}       

			$status = "Menunggu Verifikasi Portofolio";
			$id_status = 12;

			if ($peserta_sts->status_inpassing == 2) {
				$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap" ;
				$peserta_sts->status_inpassing = 2;
				$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');		
				$peserta_sts->save();
			} elseif ($peserta_sts->status_inpassing == 1 ) {
				$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
				$peserta_sts->status_inpassing = 1;
				$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
				$peserta_sts->save();
			} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")) {
				$peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
				$peserta_sts->status_inpassing = 16;
				$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
				$peserta_sts->save();
			} elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
				$peserta_sts->status = $status;
				$peserta_sts->status_inpassing = $id_status;
				$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
				$peserta_sts->save();
			}

			$pindahjadwal->id_jadwal = $idjadwal;			
			if ($pindahjadwal->save() || $pindahsave || $pindahsaveStatus || $pindahsaveSurat) {
				$riwayat = new RiwayatUser();
				$riwayat->id_user = $id;
				$riwayat->id_admin = Auth::user()->id;
				$tanggal_ujian_rw = $request->input('tgl_ujian');
				$riwayat->perihal = "jadwal_regullers";
				$riwayat->description = "telah dipindahkan ke jadwal Uji Kompetensi tanggal ".$tgl_ujian." dengan Metode Verifikasi Portofolio";
				$riwayat->tanggal = Carbon::now()->toDateString(); 
				$riwayat->save();
				$msg = "berhasil";
			} else {
				$msg = "gagal"; 
			}
		} else {
			$metode = 'Tes Tertulis';   
			$pindahjadwal->id_jadwal = $idjadwal;
			 if ($pindahSurat != "") {
				foreach ($pindahSurat as $pindahSurat) {
					$pindahSurat->id_jadwal = $idjadwal;       
					$pindahsaveSurat = $pindahSurat->save();
				}
			}

			$status = "Menunggu Tes Tertulis";
			$id_status = 13;

			if ($peserta_sts->status_inpassing == 2) {
				$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap" ;
				$peserta_sts->status_inpassing = 2;
				$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
				$peserta_sts->save();
			} elseif ($peserta_sts->status_inpassing == 1 ) {
				$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
				$peserta_sts->status_inpassing = 1;
				$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
				$peserta_sts->save();
			} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")) {
				$peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
				$peserta_sts->status_inpassing = 16;
				$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
				$peserta_sts->save();
			} elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
				$peserta_sts->status = $status;
				$peserta_sts->status_inpassing = $id_status;
				$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
				$peserta_sts->save();
			}

			if ($pindahjadwal->save()) {
				$riwayat = new RiwayatUser();
				$riwayat->id_user = $id;
				$riwayat->id_admin = Auth::user()->id;
				$tanggal_ujian_rw = $request->input('tgl_ujian');
				$riwayat->perihal = "jadwal_regullers";
				$riwayat->description = "telah dipindahkan ke jadwal Uji Kompetensi tanggal ".$tgl_ujian." dengan Metode Tes Tertulis";
				$riwayat->tanggal = Carbon::now()->toDateString(); 
				$riwayat->save();
				$msg = "berhasil";
			} else {
				$msg = "gagal"; 
			}
		}
	   
		return Redirect::to('jadwal-inpassing')->with('msg',$msg);
	}
	
	public function unggahDokumen(Request $request){
		$id_peserta = $request->input('id_peserta');
		$id_jadwal = $request->input('id_jadwal');
		$id_admin = Auth::user()->id;

		$cek_peserta = PesertaJadwal::where('id_admin_ppk',$id_admin)
						->where('id_peserta',$id_peserta)
						->where('id_jadwal',$id_jadwal)
						->where('metode_ujian','verifikasi')
						->get();

		if($cek_peserta->count() == 0){
			$dokumen = "";
		} else {
			$dokumen = SimpanBerkasPortofolio::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->get();
		}

		$peserta = Peserta::find($id_peserta);
		$jadwal = Jadwal::find($id_jadwal);
		$judul_input = JudulInput::where('jenjang',$peserta->jenjang)->get();
		$detail_input = DetailInput::all();
		$input_form = DetailFileInput::all();
		$status_file = StatusFilePortofolio::where('id_peserta',$id_peserta)
						->where('id_jadwal',$id_jadwal)
						->where('id_admin_ppk',$id_admin)
						->get();

		return View::make('unggah_dokumen_ujian', compact('peserta','jadwal','cek_peserta','dokumen','judul_input','detail_input','input_form','status_file'));
	}

	public function unggahDokumenLab(Request $request){
		$id_peserta = $request->input('id_peserta');
		$id_jadwal = $request->input('id_jadwal');
		$id_admin = Auth::user()->id;

		$cek_peserta = PesertaJadwal::where('id_admin_ppk',$id_admin)
						->where('id_peserta',$id_peserta)
						->where('metode_ujian','verifikasi');

		if($cek_peserta->count() == 0){
			$dokumen = "";
		} else {
			$dokumen = SimpanBerkasPortofolio::where('id_peserta',$id_peserta)->get();
		}

		$peserta = Peserta::find($id_peserta);
		$jadwal = Jadwal::find($id_jadwal);
		$judul_input = JudulInput::where('jenjang',$peserta->jenjang)->get();
		$detail_input = DetailInput::all();
		$input_form = DetailFileInput::all();
		$status_file = StatusFilePortofolio::where('id_peserta',$id_peserta)
						->where('id_jadwal',$id_jadwal)
						->get();

		return View::make('unggah_dokumen_ujian_lab', compact('peserta','jadwal','cek_peserta','dokumen','judul_input','detail_input','input_form','status_file'));
	}

	public function unggahDokumen2($id_peserta,$id_jadwal){
		$id_admin = Auth::user()->id;
		$cek_peserta = PesertaJadwal::where('id_admin_ppk',$id_admin)
						->where('id_peserta',$id_peserta)
						->where('id_jadwal',$id_jadwal)
						->where('metode_ujian','verifikasi');

		if($cek_peserta->count() == 0){
			$dokumen = "";
		} else {
			$dokumen = SimpanBerkasPortofolio::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->get();
		}

		$peserta = Peserta::find($id_peserta);
		$jadwal = Jadwal::find($id_jadwal);
		$judul_input = JudulInput::where('jenjang',$peserta->jenjang)->get();
		$detail_input = DetailInput::all();
		$input_form = DetailFileInput::all();
		$status_file = StatusFilePortofolio::where('id_peserta',$id_peserta)
						->where('id_jadwal',$id_jadwal)
						->get();

		return View::make('unggah_dokumen_ujian', compact('peserta','jadwal','cek_peserta','dokumen','judul_input','detail_input','input_form','status_file'));
	}

	public function storeDokumen(Request $request)
	{
		$id_peserta = $request->input('id_peserta');
		$id_jadwal = $request->input('id_jadwal');

		$jadwal = Jadwal::find($id_jadwal);
		$peserta = Peserta::find($id_peserta);

        //upload 1
		if ($request->hasFile('kompetensi_perencanaan_pbjp')) {
			$kompetensi_perencanaan_pbjp = $request->file('kompetensi_perencanaan_pbjp');
			$kompetensi_perencanaan_pbjp_name = Carbon::now()->timestamp ."_kompetensi_perencanaan_pbjp".".". $kompetensi_perencanaan_pbjp->getClientOriginalExtension();
			$kompetensi_perencanaan_pbjpPath = 'storage/data/kompetensi_perencanaan_pbjp';
			$kompetensi_perencanaan_pbjp->move($kompetensi_perencanaan_pbjpPath,$kompetensi_perencanaan_pbjp_name);
		}

        //upload 2
		if ($request->hasFile('kompetensi_pemilihan_pbj')) {
			$kompetensi_pemilihan_pbj = $request->file('kompetensi_pemilihan_pbj');
			$kompetensi_pemilihan_pbj_name = Carbon::now()->timestamp ."_kompetensi_pemilihan_pbj".".". $kompetensi_pemilihan_pbj->getClientOriginalExtension();
			$kompetensi_pemilihan_pbjPath = 'storage/data/kompetensi_pemilihan_pbj';
			$kompetensi_pemilihan_pbj->move($kompetensi_pemilihan_pbjPath,$kompetensi_pemilihan_pbj_name);
		}

        //upload 3
		if ($request->hasFile('kompetensi_pengelolaan_kontrak_pbjp')) {
			$kompetensi_pengelolaan_kontrak_pbjp = $request->file('kompetensi_pengelolaan_kontrak_pbjp');
			$kompetensi_pengelolaan_kontrak_pbjp_name = Carbon::now()->timestamp ."_kompetensi_pengelolaan_kontrak_pbjp".".". $kompetensi_pengelolaan_kontrak_pbjp->getClientOriginalExtension();
			$kompetensi_pengelolaan_kontrak_pbjpPath = 'storage/data/kompetensi_pengelolaan_kontrak_pbjp';
			$kompetensi_pengelolaan_kontrak_pbjp->move($kompetensi_pengelolaan_kontrak_pbjpPath,$kompetensi_pengelolaan_kontrak_pbjp_name);
		}

        //upload 4
		if ($request->hasFile('kompetensi_pbj_secara_swakelola')) {
			$kompetensi_pbj_secara_swakelola = $request->file('kompetensi_pbj_secara_swakelola');
			$kompetensi_pbj_secara_swakelola_name = Carbon::now()->timestamp ."_kompetensi_pbj_secara_swakelola".".". $kompetensi_pbj_secara_swakelola->getClientOriginalExtension();
			$kompetensi_pbj_secara_swakelolaPath = 'storage/data/kompetensi_pbj_secara_swakelola';
			$kompetensi_pbj_secara_swakelola->move($kompetensi_pbj_secara_swakelolaPath,$kompetensi_pbj_secara_swakelola_name);
		}

		$dokumen = DokumenPortofolio::where('id_peserta',$id_peserta);
		if($dokumen->count() == 0){
			$data = new DokumenPortofolio();
			$data->id_peserta = $request->input('id_peserta');

			if ($request->hasFile('kompetensi_perencanaan_pbjp')) {
				$data->kompetensi_perencanaan_pbjp = $kompetensi_perencanaan_pbjp_name;
			}
			
			if ($request->hasFile('kompetensi_pemilihan_pbj')) {
				$data->kompetensi_pemilihan_pbj = $kompetensi_pemilihan_pbj_name;
			}
			
			if ($request->hasFile('kompetensi_pengelolaan_kontrak_pbjp')) {
				$data->kompetensi_pengelolaan_kontrak_pbjp = $kompetensi_pengelolaan_kontrak_pbjp_name;
			}
			
			if ($request->hasFile('kompetensi_pbj_secara_swakelola')) {
				$data->kompetensi_pbj_secara_swakelola = $kompetensi_pbj_secara_swakelola_name;
			}
		} else {
			$id_dokumen = $dokumen->first()->id;
			$data = DokumenPortofolio::find($id_dokumen);
			if ($request->hasFile('kompetensi_perencanaan_pbjp')) {
				$data->kompetensi_perencanaan_pbjp = $kompetensi_perencanaan_pbjp_name;
			}
			
			if ($request->hasFile('kompetensi_pemilihan_pbj')) {
				$data->kompetensi_pemilihan_pbj = $kompetensi_pemilihan_pbj_name;
			}
			
			if ($request->hasFile('kompetensi_pengelolaan_kontrak_pbjp')) {
				$data->kompetensi_pengelolaan_kontrak_pbjp = $kompetensi_pengelolaan_kontrak_pbjp_name;
			}
        
			if ($request->hasFile('kompetensi_pbj_secara_swakelola')) {
				$data->kompetensi_pbj_secara_swakelola = $kompetensi_pbj_secara_swakelola_name;
			}
		}

		if($data->save()){
			$input = new PesertaJadwal();
			$input->id_peserta = $id_peserta;
			$input->id_jadwal = $id_jadwal;
			$input->id_admin_ppk = Auth::user()->id;
			$input->id_portofolio = $data->id;
			$input->nama_peserta = $peserta->nama;
			$input->metode_ujian = 'verifikasi';
			
			if($input->save()){
				$id_status = 12;
				$tanggal_ujian_rw = $request->input('tanggal_ujian');
				$lokasi_ujian_rw = $request->input('lokasi_ujian');
				$status = "Menunggu Verifikasi Portofolio";
				$peserta_sts = Peserta::find($id_peserta);

				if ($peserta_sts->status_inpassing != 1) {
					$peserta_sts->status = $status;
					$peserta_sts->status_inpassing = $id_status;
					$peserta_sts->save();
				}

				$riwayat = new RiwayatUser();
				$riwayat->id_user = $id_peserta;
				$riwayat->id_admin = Auth::user()->id;
				$riwayat->perihal = "jadwal_reguller";
				$riwayat->description = "didaftarkan ujian dengan metode Verifikasi Portofolio tanggal ".Helper::tanggal_indo($tanggal_ujian_rw)." di ".$lokasi_ujian_rw." oleh";
				$riwayat->tanggal = Carbon::now()->toDateString(); 
				$riwayat->save();
				return Redirect::to('tambah-peserta-ujian/'.$id_jadwal);
			} else {
				return Redirect::to('tambah-peserta-ujian/'.$id_jadwal);
			}
		} else {
			return Redirect::to('tambah-peserta-ujian/'.$id_jadwal);  
		}
	}

	public function simpanDokumen(Request $request)
	{
		$id_peserta = $request->input('id_peserta');
		$id_jadwal = $request->input('id_jadwal');
		$nama_field = $request->input('nama_field');
		$id_judul = $request->input('id_judul');

		$jenjang = $request->input('jenjang');
		$nip = $request->input('nip');
		$nama = $request->input('nama_peserta');
		$tgl_ujian =  $request->input('tanggal_ujian');
		$peserta = Peserta::find($id_peserta);

		$data = new SimpanBerkasPortofolio();
		$input = $request->all();
		$judul_input = JudulInput::find($id_judul);
		$detail_input = DetailInput::where('id_judul_input',$id_judul)->select('id')->get()->toArray();
		$detail_file = DetailFileInput::whereIn('id_detail_input',$detail_input)->get();

		$cek_peserta_tdf = PesertaJadwal::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->where('status',1)->first();
		$cek_peserta = PesertaJadwal::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->first();

		$input_judul = new JudulInput();
		$tgl_tes = $request->input('tanggal_tes');
		$tgl_ver = $request->input('tanggal_verifikasi');
		$batas_waktu_input = $request->input('batas_waktu_input');

		//kasih status
		if (is_null($cek_peserta)){
			$data = new PesertaJadwal();
			//Ini yang perlu diperbaiki
			$data->id_jadwal = $request->input('id_jadwal');
			$data->id_peserta = $id_peserta;
			$data->id_admin_ppk = Auth::user()->id;
			$data->nama_peserta = $peserta->nama;
			$data->no_ujian = substr($peserta->no_sertifikat,0,9);
			$data->metode_ujian = 'verifikasi';
			$data->status = 2;
			if($data->save()){
				$msg = "berhasil";
			} else {
				$msg = "gagal";
			}
		} elseif ($cek_peserta_tdf != ""){
			$data = PesertaJadwal::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->where('status',1)->first();

			//Ini yang perlu diperbaiki
			$data->id_jadwal = $request->input('id_jadwal');
			$data->id_peserta = $id_peserta;
			$data->id_admin_ppk = Auth::user()->id;
			$data->nama_peserta = $peserta->nama;
			$data->metode_ujian = 'verifikasi';
			$data->status = 1;

			if($data->save()){
				$msg = "berhasil";
			} else {
				$msg = "gagal";
			}
		} elseif ($cek_peserta != ""){
			$data = PesertaJadwal::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->first();
			
			//Ini yang perlu diperbaiki
			$data->id_jadwal = $request->input('id_jadwal');
			$data->id_peserta = $id_peserta;
			$data->id_admin_ppk = Auth::user()->id;
			$data->nama_peserta = $peserta->nama;
			$data->metode_ujian = 'verifikasi';
			$data->status = 2;
			
			if($data->save()){
				$msg = "berhasil";
			} else {
				$msg = "gagal";
			}
		}
		
		$admin_superadmin = "";
		$admin_dsp = "";
		$admin_bangprof = "";
		$admin_asesor = "";
		$admin_instansi = "";
        $nomor = 0;
		
		foreach($detail_file as $files){
			$nama_input_cek = $files->nama_input_1;
			if ($request->hasFile($nama_input_cek)) {
				$nomor++;
			}
		}

		if ($nomor== 0) {
			return Redirect::to('unggah-dokumen-ujian/'.$id_peserta.'/'.$id_jadwal)->with('msg','data_kosong');
        }
    
		foreach ($detail_file as $key => $files) {

		

			$simpan = new SimpanBerkasPortofolio();
			$nama_input = $files->nama_input_1;
			$nama_input_tahun = $files->nama_input_2;
			$nama_input_sertifikat = $files->nama_input_3;
			

			if ($nama_input != "") {
				if ($request->hasFile($nama_input)) {

					$cek = StatusFilePortofolio::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->where('id_detail_file_input',$files->id)->first(); 
							if ($cek && $cek->status == 'tidak_sesuai') {

								$data = SimpanBerkasPortofolio::where('id_detail_file_input',$files->id)
									 ->where('id_peserta',$id_peserta)->where('id_detail_input',$files->id_detail_input)
									 ->where('id_jadwal',$id_jadwal)->first();

        						$n = SimpanBerkasPortofolio::where('id_detail_file_input',$files->id)
									 ->where('id_peserta',$id_peserta)->where('id_detail_input',$files->id_detail_input)
									 ->where('id_jadwal',$id_jadwal)
									 ->count();

        							$new_id = "";
            					//created clone when edit
            					if ($data != "") {
            						$newDataList = $data->replicate()->setTable('simpan_berkas_portofolios_history')->save();

            						$newId = SimpanBerkasPortofolioHistory::where('id_detail_file_input',$files->id)
									 ->where('id_peserta',$id_peserta)->where('id_detail_input',$files->id_detail_input)
									 ->where('id_jadwal',$id_jadwal)->update(['edited' => $n]);

            					}

        					}

					$cek_id_detail = SimpanBerkasPortofolio::where('id_detail_file_input',$files->id)
									 ->where('id_peserta',$id_peserta)->where('id_detail_input',$files->id_detail_input)
									 ->where('id_jadwal',$id_jadwal)
									 ->delete();

					$file = $request->file($nama_input);
					$file_name = Carbon::now()->timestamp ."_".$nama_input.".". $file->getClientOriginalExtension();
					$filePath = 'storage/data/dokumen_portofolio/';
					$file->move($filePath,$file_name);
					
					$simpan->id_detail_file_input = $files->id;
					$simpan->id_detail_input = $files->id_detail_input;
					$simpan->file = $file_name;
					$simpan->tahun = $request->input($nama_input_tahun);
					$simpan->id_peserta = $id_peserta;
					$simpan->id_jadwal = $id_jadwal;
					$simpan->save();

					

					if ($cek && $cek->status == 'tidak_sesuai') {
						$simpanStatus = StatusFilePortofolio::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->where('id_detail_file_input',$files->id)->first();
						$simpanStatus->status = null;
						$simpanStatus->perbaikan = 1;
						$saveStatus = $simpanStatus->save(); 

						$from = env('MAIL_USERNAME');
						$datamail = array('perbaikan' => 'Dokumen sudah diperbaharui', 'email' => Auth::user()->email, 'name' => Auth::user()->name, 'from' => $from,'nip' => $nip, 'nama_peserta' => $nama, 'tanggal_ujian' => $tgl_ujian );

						$admin_superadmin = DB::table('users')->where('role','superadmin')->get();
						$admin_dsp = DB::table('users')->where('role','dsp')->get();
						$admin_bangprof = DB::table('users')->where('role','bangprof')->get();
						$admin_asesor = DB::table('users')->where('id', $cek->id_admin_lkpp)->where('role', 'asesor')->get();
						$admin_instansi = DB::table('users')->where('id', $cek->id_admin_ppk)->get();                   
                    } else {
						$admin_superadmin = "";
						$admin_dsp = "";
						$admin_bangprof = "";
						$admin_asesor = "";
						$admin_instansi = "";
                    }
                }
            }
        }
		
		foreach ($detail_file as $key => $files) {
            if ($files->title_1 == "Sertifikat Kompetensi Okupasi PPK/PP/Pokja Pemilihan") {
				if ($request->input($nama_input_tahun) != "" && $request->input($nama_input_sertifikat) != "") {
					$simpan->id_detail_file_input = $files->id;
					$simpan->id_detail_input = $files->id_detail_input;
					$simpan->nama_sertifikat = $request->input($nama_input_sertifikat);
					$simpan->tahun = $request->input($nama_input_tahun);
					$simpan->id_peserta = $id_peserta;
					$simpan->id_jadwal = $id_jadwal;
					$simpan->save();
				} elseif($request->input($nama_input_tahun) != ""){
					$simpan->id_detail_file_input = $files->id;
					$simpan->id_detail_input = $files->id_detail_input;
					$simpan->tahun = $request->input($nama_input_tahun);
					$simpan->id_peserta = $id_peserta;
					$simpan->id_jadwal = $id_jadwal;
					$simpan->save();    
				} else {
					$simpan->id_detail_file_input = $files->id;
					$simpan->id_detail_input = $files->id_detail_input;
					$simpan->tahun = $request->input($nama_input_tahun);
					$simpan->id_peserta = $id_peserta;
					$simpan->id_jadwal = $id_jadwal;
					$simpan->save();    
				}
			} 
		}
		
		if ($admin_superadmin != "") {
			foreach($admin_superadmin as $superadmins){
				Mail::send('mail.portobaru_superadmin', $datamail, function($message) use ($datamail,$superadmins) {
					$message->to($superadmins->email)->subject('Pembaharuan Dokumen Portofolio');
					$message->from($datamail['from'],$datamail['name']);
				});
			}
		}
		
		if ($admin_bangprof != "") {
			foreach($admin_bangprof as $bangprofs){
				Mail::send('mail.portobaru_bangprof', $datamail, function($message) use ($datamail,$bangprofs) {
					$message->to($bangprofs->email)->subject('Pembaharuan Dokumen Portofolio');
					$message->from($datamail['from'],$datamail['name']);
				});
			}
		}
		
		if ($admin_dsp != "") {
			foreach($admin_dsp as $dsps){
				Mail::send('mail.portobaru_dsp', $datamail, function($message) use ($datamail,$dsps) {
					$message->to($dsps->email)->subject('Pembaharuan Dokumen Portofolio');
					$message->from($datamail['from'],$datamail['name']);
				});
			}
		}
		
		if ($admin_asesor != "") {
			foreach($admin_asesor as $asesors){
				Mail::send('mail.portobaru_asesor', $datamail, function($message) use ($datamail,$asesors) {
					$message->to($asesors->email)->subject('Pembaharuan Dokumen Portofolio');
					$message->from($datamail['from'],$datamail['name']);
				});
			}
		}
		
		if ($admin_instansi != "") {
			foreach($admin_instansi as $instansis){
				Mail::send('mail.portobaru_instansi', $datamail, function($message) use ($datamail,$instansis) {
					$message->to($instansis->email)->subject('Pembaharuan Dokumen Portofolio');
					$message->from($datamail['from'],$datamail['name']);
				});
			}
		}

		return Redirect::to('unggah-dokumen-ujian/'.$id_peserta.'/'.$id_jadwal)->with('msg','berhasil');
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {   
        $sekarang = Carbon::now()->toDateString(); 
        $data = DB::table('peserta_jadwals')
				->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
				->join('dokumens', 'pesertas.id_dokumen','=','dokumens.id')
				->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
				->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id','left')
				->join('users as verifikator', 'pesertas.assign','=','verifikator.id','left')
				->join('users as asesor', 'peserta_jadwals.asesor','=','asesor.id','left')
				->select('pesertas.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids','dokumens.pas_foto_3_x_4 as fotos', 'instansis.nama as nama_instansis','verifikator.name as verifikators','asesor.name as asesors','peserta_jadwals.id_jadwal as jadwal_peserta' ,'jadwals.tanggal_ujian as tgl_ujian','peserta_jadwals.id_admin_ppk as admin_ppk','jadwals.id as jadwalregular','jadwals.metode as metodess','pesertas.id as id_pesertas','peserta_jadwals.status_ujian as status_jadwal','pesertas.status as status','peserta_jadwals.publish')
				->where('peserta_jadwals.id_jadwal',$id)
				->where('peserta_jadwals.status','1')
				->groupBy('peserta_jadwals.id')
				->get();
		
		$jadwal = DB::table('jadwals')
				  ->leftJoin('peserta_jadwals', function($leftJoin)
				  {
						$leftJoin->on('peserta_jadwals.id_jadwal', '=', 'jadwals.id')
						->on('peserta_jadwals.status', '=', DB::raw("'1'"));
				  })
				  ->leftJoin('pesertas','pesertas.id','=','peserta_jadwals.id_peserta')
				  ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlah_peserta"),'pesertas.*','jadwals.id as id')
			  	  ->where('jadwals.deleted_at',null)
				  ->groupBy('jadwals.id')
				  ->orderBy('jadwals.tanggal_ujian','DESC')
				  ->get();
				  
        $get_ases = User::pluck('name','id');
        $admin_bangprof = DB::table('users')->where('role','verifikator')->where('deleted_at',null)->pluck('name','id');
        $asesor = DB::table('users')->where('role','asesor')->where('deleted_at',null)->pluck('name','id');
        $jadwal = DB::table('jadwals')->where('id',$id)->first();
        $jadwalAvailable = DB::table('jadwals')->whereDate('batas_waktu_input','>=',$sekarang)->where('deleted_at',null)->orderby('jadwals.tanggal_ujian','desc')->get();
        return View::make('list_peserta_inpassing', compact('data','admin_bangprof','asesor','jadwal','jadwalAvailable','id','get_ases'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
    public function edit($id)
    {
        $jadwal = Jadwal::find($id);
        return View::make('edit_jadwal_inpassing', compact('jadwal'));
    }

    public function unggahUsulan($id,$peserta)
    {   
        $datapeserta = DB::table('pesertas')
						->select('pesertas.*')
						->where('id',$peserta)
						->get();
        
		$jadwal = DB::table('jadwals')
				  ->select('jadwals.*')
				  ->where('id',$id)
				  ->get();
        
		$dokumen = DB::table('surat_usulans')
					->select('surat_usulans.*')
					->where('id_peserta',$peserta)
					->where('id_jadwal',$id)
					->where('jenis_ujian','regular')
					->where('id_admin_ppk',Auth::user()->id)
					->get();

        return View::make('unggah_surat_usulan', compact('datapeserta','jadwal','dokumen'));
    }

    public function unggahStoreUsulan(Request $request,$peserta)
    {   
        $id_jadwal = $request->input('id_jadwal');
        $id_peserta = $request->input('id_peserta');
        $id_admin_ppk = Auth::user()->id;
        $peserta = Peserta::find($id_peserta);
        $cek_peserta_tdf = PesertaJadwal::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->where('status',1)->first();
        $cek_peserta = PesertaJadwal::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->first();

        $rules = array('no_surat_usulan_peserta' => 'required',
					   'surat_usulan_peserta' => 'mimes:jpg,jpeg,pdf|min:100|max:2048|required');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('unggah-surat-usulan/'.$id_jadwal.'/'.$id_peserta)
            ->withErrors($validator)
            ->withInput();
        }

        if ($request->hasFile('surat_usulan_peserta')) {
            $surat_usulan_peserta = $request->file('surat_usulan_peserta');
            $surat_usulan_peserta_name = Carbon::now()->timestamp ."_surat_usulan_peserta".".". $surat_usulan_peserta->getClientOriginalExtension();
            $surat_usulan_pesertaPath = 'storage/data/surat_usulan_inpassing';
            $surat_usulan_peserta->move($surat_usulan_pesertaPath,$surat_usulan_peserta_name);
        } else {
            $surat_usulan_peserta_name = null;
        }
        
        $dokumen = SuratUsulan::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->where('id_admin_ppk',$id_admin_ppk)->first();
		$pesertaNosurat = Peserta::find($id_peserta);
        
		if ($dokumen != "") {
            $dokumen->file = $surat_usulan_peserta_name;
            $dokumen->no_surat_usulan_peserta = $request->input('no_surat_usulan_peserta');
            $pesertaNosurat->no_surat_usulan_peserta = $request->input('no_surat_usulan_peserta');
            $pesertaNosurat->save();
            $dokumen->save();
        } else {
            $dokumensurat = New SuratUsulan;
            $dokumensurat->id_admin_ppk = $id_admin_ppk;   
            $dokumensurat->id_peserta = $id_peserta;   
            $dokumensurat->id_jadwal = $id_jadwal;
            $dokumensurat->jenis_ujian = 'regular';
            $dokumensurat->file = $surat_usulan_peserta_name;
            $dokumensurat->no_surat_usulan_peserta = $request->input('no_surat_usulan_peserta');
            $pesertaNosurat->no_surat_usulan_peserta = $request->input('no_surat_usulan_peserta');
            $pesertaNosurat->save();
            $dokumensurat->save();
        }

        if (is_null($cek_peserta)){
			$data = new PesertaJadwal();
			
			//Ini yang perlu diperbaiki
			$data->id_jadwal = $request->input('id_jadwal');
			$data->id_peserta = $id_peserta;
			$data->id_admin_ppk = Auth::user()->id;
			$data->nama_peserta = $peserta->nama;
			$data->no_ujian = substr($peserta->no_sertifikat,0,9);
			
			if ($request->input('metode') == 'verifikasi_portofolio') {
				$data->metode_ujian = 'verifikasi';
			} elseif($request->input('metode') == 'tes_tulis'){
				$data->metode_ujian = 'tes';    
			}
			
			$data->status =2;
			if($data->save()){
				$msg = "berhasil";
			} else {
				$msg = "gagal";
			}
		} elseif ($cek_peserta_tdf != ""){
			$data = PesertaJadwal::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->where('status',1)->first();
			//Ini yang perlu diperbaiki
			$data->id_jadwal = $request->input('id_jadwal');
			$data->id_peserta = $id_peserta;
			$data->id_admin_ppk = Auth::user()->id;
			$data->nama_peserta = $peserta->nama;
			if ($request->input('metode') == 'verifikasi_portofolio') {
				$data->metode_ujian = 'verifikasi';
			} elseif($request->input('metode') == 'tes_tulis') {
				$data->metode_ujian = 'tes';    
			}
			
			$data->status = 1;
			if($data->save()){
				$msg = "berhasil";
			} else {
				$msg = "gagal";
			}
		} elseif ($cek_peserta != ""){
			$data = PesertaJadwal::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->first();
			//Ini yang perlu diperbaiki
			$data->id_jadwal = $request->input('id_jadwal');
			$data->id_peserta = $id_peserta;
			$data->id_admin_ppk = Auth::user()->id;
			$data->nama_peserta = $peserta->nama;
			
			if ($request->input('metode') == 'verifikasi_portofolio') {
				$data->metode_ujian = 'verifikasi';
			} elseif($request->input('metode') == 'tes_tulis'){
				$data->metode_ujian = 'tes';    
			}
			
			$data->status = 2;
			if($data->save()){
				$msg = "berhasil";
			} else {
				$msg = "gagal";
			}
		}
		
        return Redirect::to('unggah-surat-usulan/'.$id_jadwal.'/'.$id_peserta)->with('msg',$msg);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $metode = $request->input('metode_ujian');
        if ($metode == "verifikasi_portofolio") {
            $rules = array('tanggal_verifikasi' => 'required',
						   'waktu_verifikasi' => 'required',
						   'kapasitas' => 'required',
						   'batas_waktu_input' => 'required',
						   'publish_jadwal' => 'required',
						   'nama_cp_1' => 'required',
						   'no_telp_cp_1' => 'required',
						   'nama_cp_2' => 'required',
						   'no_telp_cp_2' => 'required');
        }

        if ($metode == "tes_tulis") {
            $rules = array('tanggal_tes' => 'required',
						   'waktu_ujian' => 'required',
						   'lokasi_ujian' => 'required',
						   'jumlah_ruang' => 'required',
						   'kapasitas' => 'required',
						   'jumlah_unit' => 'required',
						   'batas_waktu_input' => 'required',
						   'publish_jadwal' => 'required',
						   'nama_cp_1' => 'required',
						   'no_telp_cp_1' => 'required',
						   'nama_cp_2' => 'required',
						   'no_telp_cp_2' => 'required');
        }

        $validator = Validator::make(Input::all(), $rules);
        $redirect = "edit-jadwal-inpassing/".$id;
        
		if ($validator->fails()) {
            return Redirect::to($redirect)
            ->withErrors($validator)
            ->withInput();
        }

        $jadwal = Jadwal::find($id);

        if ($request->input('tanggal_tes') != "") {
            $tgl_tes = explode('/',$request->input('tanggal_tes'));
            $tgl_tes_expl = $tgl_tes[2].'-'.$tgl_tes[1].'-'.$tgl_tes[0];
        }
        
		if ($request->input('tanggal_verifikasi') != "") {
            $tgl_ver = explode('/',$request->input('tanggal_verifikasi'));
            $tgl_ver_expl = $tgl_ver[2].'-'.$tgl_ver[1].'-'.$tgl_ver[0];
        }
        
		$batas_waktu_input = explode('/',$request->input('batas_waktu_input'));
        $tgl_bts_expl = $batas_waktu_input[2].'-'.$batas_waktu_input[1].'-'.$batas_waktu_input[0];

        if ($metode == "tes_tulis") {
			if ($tgl_tes_expl < $tgl_bts_expl) {
				return Redirect::to('edit-jadwal-inpassing/'.$id)
				->with('msg','gagal')
				->withInput();
			}
		} elseif ($metode == "verifikasi_portofolio") {
			if ($tgl_ver_expl < $tgl_bts_expl ) {
				return Redirect::to('edit-jadwal-inpassing/'.$id)
				->with('msg','gagal')
				->withInput();
			}
		}

		if ($request->input('tanggal_verifikasi') != "") {
			$tanggal_verifikasi = explode('/',$request->input('tanggal_verifikasi'));
			$jadwal->tanggal_verifikasi = $tanggal_verifikasi[2].'-'.$tanggal_verifikasi[1].'-'.$tanggal_verifikasi[0];
			$jadwal->tanggal_ujian = $tanggal_verifikasi[2].'-'.$tanggal_verifikasi[1].'-'.$tanggal_verifikasi[0];
		}

		$jadwal->waktu_verifikasi = $request->input('waktu_verifikasi');
		
		if ($request->input('tanggal_tes') != "") {
			$tanggal_tes = explode('/',$request->input('tanggal_tes'));
			$jadwal->tanggal_tes = $tanggal_tes[2].'-'.$tanggal_tes[1].'-'.$tanggal_tes[0];
			$jadwal->tanggal_ujian = $tanggal_tes[2].'-'.$tanggal_tes[1].'-'.$tanggal_tes[0];
		}
		
		if ($metode == "tes_tulis") {
			$jadwal->lokasi_ujian = $request->input('lokasi_ujian');
			$jadwal->jumlah_ruang = $request->input('jumlah_ruang');
			$jadwal->jumlah_unit = $request->input('jumlah_unit');
		} else {
			$jadwal->lokasi_ujian = '-';
			$jadwal->jumlah_ruang = '-';
			$jadwal->jumlah_unit = $request->input('kapasitas');
		}

		$jadwal->kapasitas = $request->input('kapasitas');
		
		$batas_waktu_input = explode('/',$request->input('batas_waktu_input'));
		$jadwal->batas_waktu_input = $batas_waktu_input[2].'-'.$batas_waktu_input[1].'-'.$batas_waktu_input[0];
		$jadwal->publish_jadwal = $request->input('publish_jadwal');
		$jadwal->nama_cp_1 = $request->input('nama_cp_1');
		$jadwal->no_telp_cp_1 = $request->input('no_telp_cp_1');
		$jadwal->nama_cp_2 = $request->input('nama_cp_2');
		$jadwal->no_telp_cp_2 = $request->input('no_telp_cp_2');
		
		if($jadwal->save()){
			return Redirect::to('jadwal-inpassing')->with('msg','berhasil_update');
		} else {
			return Redirect::to('jadwal-inpassing')->with('msg','gagal_update');
		}
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role == 'asesor' || Auth::user()->role == 'verifikator' || Auth::user()->role == 'ppk'){
            return Redirect::back();
        }

        $data = Jadwal::find($id);
        $data->deleted_at = Carbon::today();
        $data->publish_jadwal = 'tidak';
        $pesertajadwal = PesertaJadwal::where('id_jadwal',$id)->first();
        
		if ($pesertajadwal) {
			$pesertajadwal->status = 0;
		}
		
		if ($data->save() || $pesertajadwal->save()) {
			return Redirect::to('jadwal-inpassing')->with('msg','berhasil_hapus');
		} else {
			return Redirect::to('jadwal-inpassing')->with('msg','gagal_hapus');
		}
	}

	public function verifPortofolio($nip,$id)
	{
		$data = DB::table('peserta_jadwals')
				->join('pesertas','peserta_jadwals.id_peserta','=','pesertas.id')
				->join('dokumens', 'pesertas.id_dokumen','=','dokumens.id','left')
				->join('jadwals', 'peserta_jadwals.id_jadwal','=','jadwals.id','left')
				->join('users as asesor', 'pesertas.asesor','=','asesor.id','left')
				->select('peserta_jadwals.*','pesertas.*','dokumens.*','jadwals.*','jadwals.tanggal_ujian as tgl_ujian','asesor.name as asesors','pesertas.nama as namas','pesertas.status_inpassing as statuss','peserta_jadwals.id as ids')
				->where('peserta_jadwals.id',$id)
				->first();

		$dokumen = SimpanBerkasPortofolio::where('id_peserta',$data->id_peserta)->where('id_jadwal',$data->id_jadwal)->get();
		$dokumen_other = SimpanBerkasPortofolioHistory::where('id_peserta',$data->id_peserta)->where('id_jadwal',$data->id_jadwal)->get();
		$judul_input = JudulInput::where('jenjang',$data->jenjang)->get();
		$detail_input = DetailInput::all();
		$input_form = DetailFileInput::all();
		$status_file = StatusFilePortofolio::where('id_peserta',$data->id_peserta)
						->where('id_jadwal',$data->id_jadwal)
						->get();


		if($data->metode_ujian == "tes"){
			return Redirect::back()->with('msg','salah_metode');
		} else{
			return View::make('verifikasi_berkas_portofolio', compact('data','dokumen','dokumen_other','judul_input','detail_input','input_form','status_file'));
		}
	}

	// public function statusPorto(Request $request, $nip, $id){
	// 	$jenjang = $request->input('jenjang');

		

	// 	$metode = "Verifikasi Portofolio";
	// 	$poin = JudulInput::where('jenjang',$jenjang)->select('id')->get()->toArray();
	// 	$detail = DetailInput::whereIn('id_judul_input',$poin)->select('id')->get()->toArray();
	// 	$input = DetailFileInput::whereIn('id_detail_input',$detail)->get();

	// 	$data = PesertaJadwal::find($id);

	// 	if ($data == "") {
	// 		return response()->json(array("msg" => 'gagal_total'));
	// 	}

	// 	$jadwalpeserta = Jadwal::where('id',$data->id_jadwal)->first();
		
	// 	$namaInstansi = DB::table('peserta_jadwals')
	// 					->join('users','users.id','=','peserta_jadwals.id_admin_ppk','left')
	// 					->join('instansis','instansis.id','=','users.nama_instansi','left')
	// 					->select('peserta_jadwals.*','instansis.*','users.*','instansis.nama as namainstansi','users.name as namappk')
	// 					->where('peserta_jadwals.id',$id)
	// 					->first();
    
	// 	$judul_input_em = JudulInput::where('jenjang',$jenjang)->get();
	// 	$detail_input_em = DetailInput::all();
	// 	$input_form_em = DetailFileInput::all();

        
	// 			$detail = [];
	// 			foreach ($input as $inputs){

	// 				$detail[$inputs->id] =  $request->input('status_'.$inputs->id);

	// 				$cek = StatusFilePortofolio::where('id_peserta',$data->id_peserta)->where('id_jadwal',$data->id_jadwal)->where('id_detail_file_input',$inputs->id)->where('id_admin_ppk',$data->id_admin_ppk)->first();
            
	// 				if ($cek) {	
	// 					$simpan = StatusFilePortofolio::where('id_peserta',$data->id_peserta)->where('id_jadwal',$data->id_jadwal)->where('id_detail_file_input',$inputs->id)->where('id_admin_ppk',$data->id_admin_ppk)->first();
	// 				} else {
	// 					$simpan = new StatusFilePortofolio();
	// 				}
	
	// 				// $keterangan_post = $request->input('keterangan_'.$inputs->id);
	// 				$status_post = $request->input('status_'.$inputs->id);

	// 				if ($detail[$inputs->id] != undefined) {
	// 					$simpan->id_peserta = $data->id_peserta;
	// 					$simpan->id_jadwal = $data->id_jadwal;
	// 					$simpan->id_detail_file_input = $inputs->id;
		
	// 					if ($request->input('status_'.$inputs->id) == 'sesuai') {
	// 						$simpan->keterangan = null;
	// 					} elseif($request->input('status_'.$inputs->id) == 'tidak_sesuai'){
	// 						$simpan->keterangan = $request->input('keterangan_'.$inputs->id);        
	// 					}
				
	// 					$simpan->perbaikan = 0;
	// 					$simpan->status = $request->input('status_'.$inputs->id);
	// 					$simpan->id_admin_ppk = $data->id_admin_ppk;
	// 					$simpan-> Auth::user()->id;
						
	// 					if ($simpan->save()) {
	// 						return response()->json(array("msg" => 'berhasil'));
	// 					}else{
	// 						return response()->json(array("msg" => 'gagal','detail',$detail));
	// 					}
	// 				}

					
					
	// 			}

				
				

		
	// }


	public function StoreVerifPortofolio(Request $request, $nip, $id){

		$jenjang = $request->input('jenjang');
		$metode = "Verifikasi Portofolio";
		$poin = JudulInput::where('jenjang',$jenjang)->select('id')->get()->toArray();
		$detail = DetailInput::whereIn('id_judul_input',$poin)->select('id')->get()->toArray();
		$input = DetailFileInput::whereIn('id_detail_input',$detail)->get();

		$data = PesertaJadwal::find($id);
		$jadwalpeserta = Jadwal::where('id',$data->id_jadwal)->first();
		
		$namaInstansi = DB::table('peserta_jadwals')
						->join('users','users.id','=','peserta_jadwals.id_admin_ppk','left')
						->join('instansis','instansis.id','=','users.nama_instansi','left')
						->select('peserta_jadwals.*','instansis.*','users.*','instansis.nama as namainstansi','users.name as namappk')
						->where('peserta_jadwals.id',$id)
						->first();
    
		$judul_input_em = JudulInput::where('jenjang',$jenjang)->get();
		$detail_input_em = DetailInput::all();
		$input_form_em = DetailFileInput::all();
        $rekomendasi_status = '';

        //Untuk Pergantian Status
		if ($request->input('simpan') == "status") {
				
				foreach ($input as $inputs){
					$cek = StatusFilePortofolio::where('id_peserta',$data->id_peserta)->where('id_jadwal',$data->id_jadwal)->where('id_detail_file_input',$inputs->id)->where('id_admin_ppk',$data->id_admin_ppk)->first();
            
					if ($cek) {	
						$simpan = StatusFilePortofolio::where('id_peserta',$data->id_peserta)->where('id_jadwal',$data->id_jadwal)->where('id_detail_file_input',$inputs->id)->where('id_admin_ppk',$data->id_admin_ppk)->first();
					} else {
						$simpan = new StatusFilePortofolio();
					}
	
					$keterangan_post = $request->input('keterangan_'.$inputs->id);
					$status_post = $request->input('status_'.$inputs->id);
					
					$simpan->id_peserta = $data->id_peserta;
					$simpan->id_jadwal = $data->id_jadwal;
					$simpan->id_detail_file_input = $inputs->id;
	
					if ($request->input('status_'.$inputs->id) == 'sesuai') {
						$simpan->keterangan = null;
					} elseif($request->input('status_'.$inputs->id) == 'tidak_sesuai'){
						$simpan->keterangan = $request->input('keterangan_'.$inputs->id);        
					}
				
					$simpan->perbaikan = 0;
					$simpan->status = $request->input('status_'.$inputs->id);
					$simpan->id_admin_ppk = $data->id_admin_ppk;
					$simpan->id_admin_lkpp = Auth::user()->id;
					$simpan->save(); 
				}

				return Redirect::to('verifikasi-portofolio-peserta/'.$nip.'/'.$id)->with('msg','berhasil');

		}
		// Untuk yang proses verif normal
		else{
			if ($request->input('rekomendasi') == 'lulus') {
            	$rekomendasi_status = 'Lulus Verifikasi Portofolio';
        	} elseif ($request->input('rekomendasi') == 'tidak_lengkap') {
        	    $rekomendasi_status = 'Dokumen Persyaratan Tidak Lengkap Verifikasi Portofolio';
        	} else{
        	    $rekomendasi_status = 'Tidak Lulus Verifikasi Portofolio';
        	}


        	if ($request->input('publish') != "" && $request->input('rekomendasi') != ""){
            if (Auth::user()->role == 'dsp' || Auth::user()->role == 'superadmin') {
                $riwayat = new RiwayatUser();
                $riwayat->id_user = $data->id_peserta;
                $riwayat->id_admin = $data->id_admin_ppk;
                $riwayat->id_admin_lkpp = Auth::user()->id;
                $tanggal_ujian_rw = $request->input('tanggal_ujian');
                $riwayat->perihal = "hasil_verif_regular_dsp";
                $riwayat->description = 'dinyatakan '.$rekomendasi_status.' tanggal '.$tanggal_ujian_rw.'';
                $riwayat->keterangan = null; 
                $riwayat->tanggal = Carbon::now()->toDateString(); 
                $riwayat->save();
                $riwayat_id = $riwayat->id;
            }
        } elseif ($request->input('publish') == "" && $request->input('rekomendasi') != "") {
            if (Auth::user()->role == 'asesor' || Auth::user()->role == 'superadmin') {
                // bagian ini yang perlu dicek
                // pengecekan jika tidak lulus diberi keterangan kompetensi yang tidak sesuai
                $riwayat = new RiwayatUser();
                $riwayat->id_user = $data->id_peserta;
                $riwayat->id_admin = $data->id_admin_ppk;
                $riwayat->id_admin_lkpp = Auth::user()->id;
                $tanggal_ujian_rw = $request->input('tanggal_ujian');
                $riwayat->perihal = "hasil_verif_regular";
                $riwayat->description = 'direkomendasikan '.$rekomendasi_status.' tanggal '.$tanggal_ujian_rw.'';
                $riwayat->keterangan = null; 
                $riwayat->tanggal = Carbon::now()->toDateString(); 
                $riwayat->save();
                $riwayat_id = $riwayat->id;
            }
        }


		foreach ($input as $inputs){
			$cek = StatusFilePortofolio::where('id_peserta',$data->id_peserta)->where('id_jadwal',$data->id_jadwal)->where('id_detail_file_input',$inputs->id)->where('id_admin_ppk',$data->id_admin_ppk)->first();
            
			if ($cek) {
				$simpan = StatusFilePortofolio::where('id_peserta',$data->id_peserta)->where('id_jadwal',$data->id_jadwal)->where('id_detail_file_input',$inputs->id)->where('id_admin_ppk',$data->id_admin_ppk)->first();
			} else {
				$simpan = new StatusFilePortofolio();
			}

			$keterangan_post = $request->input('keterangan_'.$inputs->id);
			$status_post = $request->input('status_'.$inputs->id);
			
			$simpan->id_peserta = $data->id_peserta;
			$simpan->id_jadwal = $data->id_jadwal;
			$simpan->id_detail_file_input = $inputs->id;

			if ($request->input('status_'.$inputs->id) == 'sesuai') {
				$simpan->keterangan = null;
			} elseif($request->input('status_'.$inputs->id) == 'tidak_sesuai'){
				$simpan->keterangan = $request->input('keterangan_'.$inputs->id);        
			}
			
			$simpan->perbaikan = 0;
			$simpan->status = $request->input('status_'.$inputs->id);
			$simpan->id_admin_ppk = $data->id_admin_ppk;
			$simpan->id_admin_lkpp = Auth::user()->id;
			$simpan->save(); 
			
			$rekomendasi_status = '';
			if ($request->input('rekomendasi') == 'lulus') {
				$rekomendasi_status = 'Lulus Verifikasi Portofolio';
			} elseif ($request->input('rekomendasi') == 'tidak_lengkap') {
				$rekomendasi_status = 'Dokumen Persyaratan Tidak Lengkap Verifikasi Portofolio';
			} else {
				$rekomendasi_status = 'Tidak Lulus Verifikasi Portofolio';
			}

			if ($request->input('publish') != "" && $request->input('rekomendasi') != ""){
				if (Auth::user()->role == 'dsp' || Auth::user()->role == 'superadmin') {
					$riwayat = new RiwayatVerif();
					$riwayat->id_riwayat = $riwayat_id;
					$riwayat->id_user = $data->id_peserta;
					$riwayat->id_admin = $data->id_admin_ppk;
					$riwayat->id_jadwal = $data->id_jadwal;
					$riwayat->id_admin_lkpp = Auth::user()->id;
					$tanggal_ujian_rw = $request->input('tanggal_ujian');
					$riwayat->perihal = "hasil_verif_regular_dsp";
					$riwayat->description = 'dinyatakan '.$rekomendasi_status.' tanggal '.$tanggal_ujian_rw.'';
					
					if ($request->input('status_'.$inputs->id) == 'sesuai') {
						$riwayat->keterangan = null;
					} elseif($request->input('status_'.$inputs->id) == 'tidak_sesuai'){
						$riwayat->keterangan = $request->input('keterangan_'.$inputs->id);      
					}
					
					$riwayat->tanggal = Carbon::now()->toDateString(); 
					$riwayat->save();
				}
			} elseif ($request->input('publish') == "" && $request->input('rekomendasi') != ""){
				if (Auth::user()->role == 'asesor' || Auth::user()->role == 'superadmin') {
                // bagian ini yang perlu dicek
                // pengecekan jika tidak lulus diberi keterangan kompetensi yang tidak sesuai
                $riwayat = new RiwayatVerif();
                $riwayat->id_riwayat = $riwayat_id;
                $riwayat->id_user = $data->id_peserta;
                $riwayat->id_admin = $data->id_admin_ppk;
                $riwayat->id_jadwal = $data->id_jadwal;
                $riwayat->id_admin_lkpp = Auth::user()->id;
                $tanggal_ujian_rw = $request->input('tanggal_ujian');
                $riwayat->perihal = "hasil_verif_regular";
                $riwayat->description = 'direkomendasikan '.$rekomendasi_status.' tanggal '.$tanggal_ujian_rw.'';
                
				if ($request->input('status_'.$inputs->id) == 'sesuai') {
                    $riwayat->keterangan = null;
                } elseif($request->input('status_'.$inputs->id) == 'tidak_sesuai'){
                    $riwayat->keterangan = $request->input('keterangan_'.$inputs->id);      
                }
				
                $riwayat->tanggal = Carbon::now()->toDateString(); 
                $riwayat->save();
            }
        }

		$namaInput =  DB::table('detail_inputs')
						->join('judul_inputs', 'detail_inputs.id_judul_input','=','judul_inputs.id','left')
						->join('detail_file_inputs', 'detail_inputs.id','=','detail_file_inputs.id_detail_input','left')
						->join('status_file_portofolios', 'status_file_portofolios.id_detail_file_input','=','detail_file_inputs.id','left')
						->select('detail_inputs.*','detail_file_inputs.title_1 as nama_file_input','detail_inputs.nama as nama_input','status_file_portofolios.*','detail_file_inputs.id as file_input','detail_inputs.id as detail_input','detail_inputs.id_judul_input as JudulInput','judul_inputs.id as poins','status_file_portofolios.id_detail_file_input as status_detail','detail_file_inputs.id_detail_input as detail_input2','judul_inputs.nama as judul_input','status_file_portofolios.keterangan as keterangan','status_file_portofolios.status')
						->where('judul_inputs.jenjang',$jenjang)
						->where('status_file_portofolios.id_peserta',$data->id_peserta)
						->where('status_file_portofolios.id_jadwal',$data->id_jadwal)        
						->where('status_file_portofolios.status','!=',null)        
						->get();

		$from = env('MAIL_USERNAME');
		$datamail = array('rekomendasi' => $request->input('rekomendasi'), 'email' => Auth::user()->email, 'name' => Auth::user()->name, 'from' => $from, 'nip' => $nip, 'nama_peserta' => $request->input('nama_peserta'), 'tgl_batas' => $jadwalpeserta->tanggal_ujian, 'nama_input' => $namaInput,'metode' => $metode, 'instansi' => $namaInstansi->namainstansi,'nama_ppk' => $namaInstansi->namappk ,'poin' => $judul_input_em,'detail' => $detail_input_em,'input' => $input_form_em,'jenjang' => $jenjang, 'tanggal_batas' => $jadwalpeserta->tanggal_ujian);

		$admin_superadmin = DB::table('users')->where('role','superadmin')->get();
		$admin_dsp = DB::table('users')->where('role','dsp')->get();
		$admin_bangprof = DB::table('users')->where('role','bangprof')->get();
		$admin_instansi = DB::table('users')->where('id', $data->id_admin_ppk)->get();
		}

		if (Auth::user()->role == 'asesor' || Auth::user()->role == 'superadmin') {
			if ($request->input('publish') == "" && $request->input('rekomendasi') != "" ){
				foreach($admin_superadmin as $superadmins){
					Mail::send('mail.rekomendasi_superadmin', $datamail, function($message) use ($datamail,$superadmins) {
						$message->to($superadmins->email)->subject('Pemberitahuan Hasil Verifikasi Portofolio');
						$message->from($datamail['from'],$datamail['name']);
					});
				}
				
				foreach($admin_bangprof as $bangprofs){
					Mail::send('mail.rekomendasi_bangprof', $datamail, function($message) use ($datamail,$bangprofs) {
						$message->to($bangprofs->email)->subject('Pemberitahuan Hasil Verifikasi Portofolio');
						$message->from($datamail['from'],$datamail['name']);
					});
				}
				
				foreach($admin_dsp as $dsps){
					Mail::send('mail.rekomendasi_dsp', $datamail, function($message) use ($datamail,$dsps) {
						$message->to($dsps->email)->subject('Pemberitahuan Hasil Verifikasi Portofolio');
						$message->from($datamail['from'],$datamail['name']);
					});
				}
				
				$rekomen = $request->input('rekomendasi');
				if ($rekomen == 'tidak_lulus') {
					foreach($admin_instansi as $instansis){
						Mail::send('mail.rekomendasi_instansi', $datamail, function($message) use ($datamail,$instansis) {
							$message->to($instansis->email)->subject('Pemberitahuan Hasil Verifikasi Portofolio');
							$message->from($datamail['from'],'Admin LKPP');
						});
					}
				}
			}
		}
		
		if (Auth::user()->role == 'dsp' || Auth::user()->role == 'superadmin') {
			if ($request->input('publish') != "" && $request->input('rekomendasi') != ""){
				foreach($admin_superadmin as $superadmins){
					Mail::send('mail.hasiltes_superadmin', $datamail, function($message) use ($datamail,$superadmins) {
						$message->to($superadmins->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
						$message->from($datamail['from'],$datamail['name']);
					});
				}
				
				foreach($admin_bangprof as $bangprofs){
					Mail::send('mail.hasiltes_bangprof', $datamail, function($message) use ($datamail,$bangprofs) {
						$message->to($bangprofs->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
						$message->from($datamail['from'],$datamail['name']);
					});
				}
				
				foreach($admin_dsp as $dsps){
					Mail::send('mail.hasiltes_dsp', $datamail, function($message) use ($datamail,$dsps) {
						$message->to($dsps->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
						$message->from($datamail['from'],$datamail['name']);
					});
				}
				
				foreach($admin_instansi as $instansis){
					Mail::send('mail.hasiltes_instansi', $datamail, function($message) use ($datamail,$instansis) {
						$message->to($instansis->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
						$message->from($datamail['from'],'Admin LKPP');
					});
				}
			}
		}

		if ($request->input('rekomendasi') != "") {
			if (Auth::user()->role == 'dsp' || Auth::user()->role == 'superadmin') {
				if ($request->input('publish') != "" ){
					$data->publish = $request->input('publish');
					$data->status_ujian = $request->input('rekomendasi');
					$data->perbaikan_porto = 'open';
				}
			}
			
			if ( Auth::user()->role == 'asesor' || Auth::user()->role == 'superadmin') {
				if ($request->input('publish') == ""){
					$data->status_ujian = $request->input('rekomendasi');
					$data->publish = null;
					$data->perbaikan_porto = 'open';
				}
			}
			
			$data->admin_update = Auth::user()->id;
			if($data->save()){
				$status_ujian = $request->input('rekomendasi');
				if (Auth::user()->role == 'asesor' || Auth::user()->role == 'superadmin') {
					if ($request->input('publish') == ""){
						if ($status_ujian == 'lulus'){
							$id_status = 12;
							$status = "Menunggu Verifikasi Portofolio";
						} elseif ($status_ujian == 'tidak_lulus') {
							$id_status = 12;
							$status = "Menunggu Verifikasi Portofolio";
						} else {
							$id_status = 4;
							$status = "Menunggu Verifikasi Portofolio atau Uji Tulis";
						}
					}
				}
				
				if (Auth::user()->role == 'dsp' || Auth::user()->role == 'superadmin') {
					if ($request->input('publish') != ""){
						if ($status_ujian == 'lulus'){
							$id_status = 6;
							$status = "Lulus";
						}
						
						if ($status_ujian == 'tidak_lulus'){
							$id_status = 7;
							$status = "Tidak Lulus";
						}
						
						if ($status_ujian == 'tidak_lengkap'){
							$id_status = 17;
							$status = "Dokumen Persyaratan Tidak Lengkap";
						}
						
						if ($status_ujian == ''){
							$id_status = 4;
							$status = "Menunggu Verifikasi Portofolio";
						}
					}   
				}

				if (Auth::user()->role == 'dsp' || Auth::user()->role == 'superadmin') {
					if ($request->input('publish') != ""){
						$peserta = Peserta::find($data->id_peserta);
						$peserta->status = $status;
						$peserta->status_inpassing = $id_status;
						$peserta->save();
					}
				} elseif(Auth::user()->role == 'asesor' || Auth::user()->role == 'superadmin') {
					if ($request->input('publish') == ""){
						$peserta = Peserta::find($data->id_peserta);
						$peserta->status = $status;
						$peserta->status_inpassing = $id_status;
						$peserta->save();        
					}
				}
			}
		}
		
		return Redirect::to('peserta-jadwal-inpassing/'.$data->id_jadwal)->with('msg','berhasil');

		}


		
		

        
	}

	public function inputHasil($nip,$id) {
		$data = DB::table('peserta_jadwals')
				->join('pesertas','peserta_jadwals.id_peserta','=','pesertas.id')
				->join('jadwals','jadwals.id','=','peserta_jadwals.id_jadwal','left')
				->join('dokumens', 'peserta_jadwals.id_peserta','=','dokumens.id','left')
				->select('peserta_jadwals.*','pesertas.*','dokumens.*','pesertas.nama as namas','jadwals.*','pesertas.status as statuss')
				->where('peserta_jadwals.id',$id)
				->first();
    
		if($data->metode_ujian == "verifikasi"){
			return Redirect::back()->with('msg','salah_metode');
		} else {
			return View::make('input_hasil_peserta', compact('data'));
		}
	}

	public function storeHasil(Request $request,$nip,$id){
		$data = PesertaJadwal::find($id);
		$jadwalpeserta = Jadwal::where('id',$data->id_jadwal)->first();
		$data->status_ujian = $request->input('rekomendasi');
		$metode = "Tes Tertulis";
		$data->publish = $request->input('publish');
		$data->admin_update = Auth::user()->id;
		
		if($data->save()){
			$namaInstansi = DB::table('peserta_jadwals')
							 ->join('users','users.id','=','peserta_jadwals.id_admin_ppk','left')
							 ->join('instansis','instansis.id','=','users.nama_instansi','left')
							 ->select('peserta_jadwals.*','instansis.*','users.*','instansis.nama as namainstansi','users.name as namappk')
							 ->where('peserta_jadwals.id',$id)
							 ->first();

			$from = env('MAIL_USERNAME');
			$datamail = array('rekomendasi' => $request->input('rekomendasi'), 'email' => Auth::user()->email, 'name' => Auth::user()->name, 'from' => $from, 'nip' => $nip, 'nama_peserta' => $request->input('nama_peserta'),'metode' => $metode,'tanggal_batas' => $jadwalpeserta->tanggal_ujian,'nama_ppk' => $namaInstansi->namappk );
			$admin_superadmin = DB::table('users')->where('role','superadmin')->get();
			$admin_dsp = DB::table('users')->where('role','dsp')->get();
			$admin_bangprof = DB::table('users')->where('role','bangprof')->get();
			$admin_instansi = DB::table('users')->where('id', $data->id_admin_ppk)->get();

			if (Auth::user()->role == 'dsp' || Auth::user()->role == 'superadmin') {
				if ($request->input('publish') != "" ){
					foreach($admin_superadmin as $superadmins){
						Mail::send('mail.hasiltes_superadmin', $datamail, function($message) use ($datamail,$superadmins) {
							$message->to($superadmins->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
							$message->from($datamail['from'],$datamail['name']);
						});
					}
					
					foreach($admin_bangprof as $bangprofs){
						Mail::send('mail.hasiltes_bangprof', $datamail, function($message) use ($datamail,$bangprofs) {
							$message->to($bangprofs->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
							$message->from($datamail['from'],$datamail['name']);
						});
					}
					
					foreach($admin_dsp as $dsps){
						Mail::send('mail.hasiltes_dsp', $datamail, function($message) use ($datamail,$dsps) {
							$message->to($dsps->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
							$message->from($datamail['from'],$datamail['name']);
						});
					}
					
					foreach($admin_instansi as $instansis){
						Mail::send('mail.hasiltes_instansi', $datamail, function($message) use ($datamail,$instansis) {
							$message->to($instansis->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
							$message->from($datamail['from'],'Admin LKPP');
						});
					}
				}
			}
			
			$status_ujian = $request->input('rekomendasi');
			if ($status_ujian == 'lulus'){
				$id_status = 6;
				$status = "Lulus";
			}

			if ($status_ujian == 'tidak_lulus'){
				$id_status = 7;
				$status = "Tidak Lulus";
			}

			if ($status_ujian == 'tidak_hadir'){
				$id_status = 11;
				$status = "Tidak Hadir Tes";
			}

			if ($status_ujian == 'tidak_lengkap'){
				$id_status = 17;
				$status = "Dokumen Persyaratan Tidak Lengkap";
			}
			
			if ($status_ujian == ''){
				$id_status = 4;
				$status = "Menunggu Verifikasi Portofolio atau Uji Tulis";
			}

			if ($request->input('publish') != ""){
				$peserta = Peserta::find($data->id_peserta);
				$peserta->status = $status;
				$peserta->status_inpassing = $id_status;
				$peserta->save();
			}

			$status_rekomendasi = $request->input('rekomendasi');
			if ($status_rekomendasi == 'lulus') {
				$status_riwayat = 'Lulus Tes Tertulis';
			}

			if ($status_rekomendasi == 'tidak_lulus') {
				$status_riwayat = 'Tidak Lulus Tes Tertulis';
			}
			
			if ($status_rekomendasi == 'tidak_hadir') {
				$status_riwayat = 'Tidak Hadir Tes Tertulis';
			}
			
			if ($status_rekomendasi == 'tidak_lengkap'){
								$status_riwayat = "Dokumen Persyaratan Tidak Lengkap";
			}
			
			if ($status_rekomendasi == '') {
				$status_riwayat = '';
			}

			if ($request->input('publish') != ""){
				$riwayat = new RiwayatUser();
				$riwayat->id_user = $data->id_peserta;
				$riwayat->id_admin = Auth::user()->id;
				$tanggal_ujian_rw = $request->input('tanggal_ujian');
				$riwayat->perihal = "hasil_tes_dsp";
				$riwayat->description = 'dinyatakan '.$status_riwayat.' tanggal '.$tanggal_ujian_rw;
				$riwayat->tanggal = Carbon::now()->toDateString(); 
				$riwayat->save();
			} else {
				$riwayat = new RiwayatUser();
				$riwayat->id_user = $data->id_peserta;
				$riwayat->id_admin = Auth::user()->id;
				$riwayat->perihal = "hasil_tes";
				$riwayat->description = "di rekomendasikan ".$status_riwayat.' tanggal '.$tanggal_ujian_rw;
				$riwayat->tanggal = Carbon::now()->toDateString(); 
				$riwayat->save();
			}
			
			return Redirect::to('peserta-jadwal-inpassing/'.$data->id_jadwal)->with('msg','berhasil');
		} else {
			return Redirect::to('peserta-jadwal-inpassing/'.$data->id_jadwal)->with('msg','gagal');
		}
	}

	public function upHasilUjian(Request $request,$id){
		$jadwal = Jadwal::find($id);
		return View::make('up_data_hasil_ujian', compact('jadwal'));
	}

	public function StoreupHasilUjian(Request $request,$id){
		$rules = array('hasil_ujian' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048');
		$validator = Validator::make(Input::all(), $rules);
		
		if ($validator->fails()) {
			return Redirect::to('up-hasil_ujian/'.$id)
			->withErrors($validator)
			->withInput();
		}

		if ($request->hasFile('hasil_ujian')) {
			$hasil_ujian = $request->file('hasil_ujian');
			$hasil_ujian_name = Carbon::now()->timestamp ."_hasil_ujian_regular".".". $hasil_ujian->getClientOriginalExtension();
			$hasil_ujianPath = 'storage/data/hasil_ujian/regular';
			$hasil_ujian->move($hasil_ujianPath,$hasil_ujian_name);
		} else {
			$hasil_ujian_name = null;
		}

		$dokumen = Jadwal::find($id);
		$dokumen->hasil_ujian = $hasil_ujian_name;
		
		if($dokumen->save()){
			$pesertajadwal = PesertaJadwal::where('id_jadwal',$id)->where('status',1)->select('id_peserta')->get()->toArray();
			$peserta = Peserta::whereIn('id',$pesertajadwal)->first();
			if($peserta != ""){
				if ($peserta->status_inpassing == 2) {
					$status_tidak_lulus = 7;
					$peserta->status_inpassing = $status_tidak_lulus;
					$peserta->status = "Tidak Lulus";
					$peserta->save();
					$msg = "berhasil";
				}
			} else {
				$msg = "berhasil";
			}
			
			$msg = "berhasil";
		} else {
		   $msg = "gagal";
		}

		return Redirect::to('jadwal-inpassing')->with('msg',$msg);
	}

	public function printAbsensi($id)
	{	
		$jenjang = substr($id,-1);		
		
		switch ($jenjang) {
			case "1":
				$jenjang = "Pertama";
				break;
			case "2":
				$jenjang = "Muda";
				break;
			case "3":
				$jenjang = "Madya";
				break;
			default:
				$jenjang = "";
		}
	
		$id = str_replace(substr($id,-2), "", $id);
		$data = DB::table('peserta_jadwals')
				->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
				->join('dokumens', 'peserta_jadwals.id_peserta','=','dokumens.id')
				->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
				->join('users as verifikator', 'pesertas.assign','=','verifikator.id','left')
				->join('users as asesor', 'pesertas.asesor','=','asesor.id','left')
				->select('pesertas.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids','peserta_jadwals.no_ujian','dokumens.pas_foto_3_x_4 as fotos', 'instansis.nama as nama_instansis','verifikator.name as verifikators','asesor.name as asesors')
				->where([['peserta_jadwals.id_jadwal', '=', $id],['pesertas.jenjang', '=', $jenjang]])
				->get();
		
		$jadwal = Jadwal::find($id);
		$penyelengara = 'LKPP';
		$pdf = PDF::loadView('pdf.cetak_absensi_reguller', compact('data','jadwal','penyelengara','jenjang'));
		return $pdf->stream('absensi_reguller.pdf');
	}

	public function uploadPortofolio(Request $request)
	{
		$rules = array('id' => 'required',
					   'kompetensi_perencanaan_pbjp' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
					   'kompetensi_pemilihan_pbj' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
					   'kompetensi_pengelolaan_kontrak_pbjp' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048',
					   'kompetensi_pbj_secara_swakelola' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048');

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return response()->json(['msg'  => "validator"]);
		}

		//upload 1
		if ($request->hasFile('kompetensi_perencanaan_pbjp')) {
			$kompetensi_perencanaan_pbjp = $request->file('kompetensi_perencanaan_pbjp');
			$kompetensi_perencanaan_pbjp_name = Carbon::now()->timestamp ."_kompetensi_perencanaan_pbjp".".". $kompetensi_perencanaan_pbjp->getClientOriginalExtension();
			$kompetensi_perencanaan_pbjpPath = 'storage/data/kompetensi_perencanaan_pbjp';
			$kompetensi_perencanaan_pbjp->move($kompetensi_perencanaan_pbjpPath,$kompetensi_perencanaan_pbjp_name);
		}

        //upload 2
		if ($request->hasFile('kompetensi_pemilihan_pbj')) {
			$kompetensi_pemilihan_pbj = $request->file('kompetensi_pemilihan_pbj');
			$kompetensi_pemilihan_pbj_name = Carbon::now()->timestamp ."_kompetensi_pemilihan_pbj".".". $kompetensi_pemilihan_pbj->getClientOriginalExtension();
			$kompetensi_pemilihan_pbjPath = 'storage/data/kompetensi_pemilihan_pbj';
			$kompetensi_pemilihan_pbj->move($kompetensi_pemilihan_pbjPath,$kompetensi_pemilihan_pbj_name);
		}

        //upload 3
		if ($request->hasFile('kompetensi_pengelolaan_kontrak_pbjp')) {
			$kompetensi_pengelolaan_kontrak_pbjp = $request->file('kompetensi_pengelolaan_kontrak_pbjp');
			$kompetensi_pengelolaan_kontrak_pbjp_name = Carbon::now()->timestamp ."_kompetensi_pengelolaan_kontrak_pbjp".".". $kompetensi_pengelolaan_kontrak_pbjp->getClientOriginalExtension();
			$kompetensi_pengelolaan_kontrak_pbjpPath = 'storage/data/kompetensi_pengelolaan_kontrak_pbjp';
			$kompetensi_pengelolaan_kontrak_pbjp->move($kompetensi_pengelolaan_kontrak_pbjpPath,$kompetensi_pengelolaan_kontrak_pbjp_name);
		}

        //upload 4
		if ($request->hasFile('kompetensi_pbj_secara_swakelola')) {
			$kompetensi_pbj_secara_swakelola = $request->file('kompetensi_pbj_secara_swakelola');
			$kompetensi_pbj_secara_swakelola_name = Carbon::now()->timestamp ."_kompetensi_pbj_secara_swakelola".".". $kompetensi_pbj_secara_swakelola->getClientOriginalExtension();
			$kompetensi_pbj_secara_swakelolaPath = 'storage/data/kompetensi_pbj_secara_swakelola';
			$kompetensi_pbj_secara_swakelola->move($kompetensi_pbj_secara_swakelolaPath,$kompetensi_pbj_secara_swakelola_name);
		}

		$id_peserta = $request->input('id');
		$dokumen = DokumenPortofolio::where('id_peserta',$id_peserta);
		
		if($dokumen->count() == 0){
			$data = new DokumenPortofolio();
			$data->id_peserta = $request->input('id');
			if ($request->hasFile('kompetensi_perencanaan_pbjp')) {
				$data->kompetensi_perencanaan_pbjp = $kompetensi_perencanaan_pbjp_name;
			}
			
			if ($request->hasFile('kompetensi_pemilihan_pbj')) {
				$data->kompetensi_pemilihan_pbj = $kompetensi_pemilihan_pbj_name;
			}
			
			if ($request->hasFile('kompetensi_pengelolaan_kontrak_pbjp')) {
				$data->kompetensi_pengelolaan_kontrak_pbjp = $kompetensi_pengelolaan_kontrak_pbjp_name;
			}
			
			if ($request->hasFile('kompetensi_pbj_secara_swakelola')) {
				$data->kompetensi_pbj_secara_swakelola = $kompetensi_pbj_secara_swakelola_name;
			}
			
			$id_portofolio = $request->input('id');
		} else {
			$id_dokumen = $dokumen->first()->id;
			$data = DokumenPortofolio::find($id_dokumen);
			
			if ($request->hasFile('kompetensi_perencanaan_pbjp')) {
				$data->kompetensi_perencanaan_pbjp = $kompetensi_perencanaan_pbjp_name;
			}
			
			if ($request->hasFile('kompetensi_pemilihan_pbj')) {
				$data->kompetensi_pemilihan_pbj = $kompetensi_pemilihan_pbj_name;
			}
			
			if ($request->hasFile('kompetensi_pengelolaan_kontrak_pbjp')) {
				$data->kompetensi_pengelolaan_kontrak_pbjp = $kompetensi_pengelolaan_kontrak_pbjp_name;
			}
			
			if ($request->hasFile('kompetensi_pbj_secara_swakelola')) {
				$data->kompetensi_pbj_secara_swakelola = $kompetensi_pbj_secara_swakelola_name;
			}
			
			$id_portofolio = $request->input('id');
		}

		if($data->save()){
			return response()->json(['msg' => "berhasil",
									 'id' => $id_portofolio,
									 'id_db' => $data->id]);
		} else {
			return response()->json([
				'msg'  => "gagal",
			]); 
		}
	}
    
	public function printExcell($id)
    {
        $jadwal = DB::table('jadwals')->where('id',$id)->first();
        if ($jadwal->metode == 'tes_tulis') {
            $tanggal_jadwal = Helper::tanggal_indo($jadwal->tanggal_tes);
        } else {
            $tanggal_jadwal = Helper::tanggal_indo($jadwal->tanggal_verifikasi);
        }
		
        $nama_excel = 'list peserta jadwal reguler '.$tanggal_jadwal.'.xlsx';
        return Excel::download(new PesertaJadwalExport($id), $nama_excel);
    }
	
	public function indexDeskripsi()
    {
        $data = DB::table('detail_inputs')
				->join('judul_inputs','detail_inputs.id_judul_input','=','judul_inputs.id','left')
				->select('judul_inputs.*','detail_inputs.*','detail_inputs.id as id')
				->orderBy('detail_inputs.id','asc')
				->get();

        return View::make('data_deskripsi_portofolio',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editDeskripsi($id)
    {
        $data = DB::table('detail_inputs')
				->join('judul_inputs','detail_inputs.id_judul_input','=','judul_inputs.id','left')
				->select('judul_inputs.*','detail_inputs.*','detail_inputs.id as id','detail_inputs.keterangan as keterangan')
				->where('detail_inputs.id',$id)
				->get();

        return View::make('edit_deskripsi_portofolio', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateDeskripsi(Request $request, $id)
    {
        $data = DetailInput::find($id);
        $data->keterangan = $request->input('text');
     
		if($data->save()){
            return Redirect::to('data-deskripsi-portofolio')->with('msg','berhasil');
        } else {
            return Redirect::to('data-deskripsi-portofolio')->with('msg','gagal');
        }
    }

    public function hapusDokumen(Request $request,$id,$id_peserta,$id_jadwal){
		if ($id == "") {
			return response()->json(['msg' => 'gagal_hapus_porto']);
		} else {
			$data = SimpanBerkasPortofolio::where('id_detail_file_input',$id)
					->where('id_peserta',$id_peserta)
					->where('id_jadwal',$id_jadwal)
					->first();

			$data_status = StatusFilePortofolio::where('id_detail_file_input',$id)
							->where('id_peserta',$id_peserta)
							->where('id_jadwal',$id_jadwal)
							->first();

			if ($data != "") {
				$datadelete = $data->delete();
			} else {
				$datadelete = "" ;
			}

			if ($data_status != "") {
				$datadelete_status = $data_status->delete();
			} else {
				$datadelete_status = "";
			}

			if ($datadelete || $datadelete_status) {
				return response()->json(['msg' => 'berhasil_hapus_porto']); 
			} else {
				return response()->json(['msg' => 'gagal_hapus_porto']);
			}
		}
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}