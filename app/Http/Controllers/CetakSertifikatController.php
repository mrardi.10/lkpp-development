<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PesertaJadwal;
use App\MasterSertifikat;
use App\JudulInput;
use App\DetailInput;
use PDF;
use View;
use DB;
use Redirect;
use Carbon\Carbon;
use Helper;


class CetakSertifikatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jadwal1 = DB::table('jadwals')
				   ->join('peserta_jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
				   ->select('jadwals.*', DB::raw("count(peserta_jadwals.id) as jumlahpeserta"))
				   ->where('deleted_at',null)
				   ->groupBy('jadwals.id')
				   ->orderBy('jadwals.tanggal_ujian','DESC')
				   ->get();

		$jadwal2 = DB::table('jadwal_instansis')
				   ->where('status_permohonan','setuju')
				   ->orderBy('tanggal_ujian','DESC')
				   ->get(['id','tanggal_ujian','lokasi_ujian','metode','tipe_ujian']);

        $merged = $jadwal1->merge($jadwal2);
        $jadwal = $merged->all();
        return View::make('cetak_sertifikat', compact('jadwal1','jadwal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
	public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
	public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
	public function show($tipe,$id)
    {
        if ($tipe == 'regular') {
            $data = DB::table('peserta_jadwals')
					->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
					->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
					->select('pesertas.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids','peserta_jadwals.no_ujian','peserta_jadwals.no_seri_sertifikat','peserta_jadwals.status_ujian','jadwals.tipe_ujian')
					->where('peserta_jadwals.id_jadwal',$id)
					->where('peserta_jadwals.status_ujian','lulus')
					->whereNotNull('no_seri_sertifikat')
					->get();
        } else {
            $data = DB::table('peserta_instansis')
					->join('pesertas','peserta_instansis.id_peserta','=','pesertas.id')
					->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id')
					->select('pesertas.*','peserta_instansis.metode_ujian as metodes','peserta_instansis.id as ids','peserta_instansis.no_ujian','peserta_instansis.no_seri_sertifikat','peserta_instansis.status_ujian','jadwal_instansis.tipe_ujian')
					->where('peserta_instansis.id_jadwal',$id)
					->where('status_ujian','lulus')
					->whereNotNull('no_seri_sertifikat')
					->get();
        }

        return View::make('cetak_peserta_inpassing', compact('data','id'));
    }
	
	

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function data()
    {
        $data = MasterSertifikat::get();
        return View::make('edit_data_sertifikat', compact('data'));
    }
	
	public function storedata(Request $request){
        
		if (Auth::user()->role == 'asesor' || Auth::user()->role == 'verifikator' || Auth::user()->role == 'ppk'){
            return Redirect::back();
        }

        $nama = $request->input('nama');
        $nip = $request->input('nip');
        $jabatan = $request->input('jabatan');
        $jabatan_english = $request->input('jabatan_english');
        $data = MasterSertifikat::first();
		
        if ($data == "") {
			$input = New MasterSertifikat;
			$input->nama_deputi = $nama;
			$input->nip_deputi = $nip;
			$input->jabatan = $jabatan;
			$input->jabatan_english = $jabatan_english;
			$savedata = $input->save();
		}
		elseif($data != "") {
			$id = 1;
			$input = MasterSertifikat::find($id);
			$input->nama_deputi = $nama;
			$input->nip_deputi = $nip;
			$input->jabatan = $jabatan;
			$input->jabatan_english = $jabatan_english;
			$savedata = $input->save();
		}
		
		if ($savedata) {
			return Redirect::to('data-sertifikat')->with('msg','berhasil');
		} else {
			return Redirect::to('data-sertifikat')->with('msg','gagal');
		}
	}
    
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
	public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	public function editSertifikat($tipe, $id, $id_jadwal)
    {
		if ($tipe == 'regular') {
            $data = DB::table('peserta_jadwals')
					->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwals', 'jadwals.id', '=', 'peserta_jadwals.id_jadwal','left')
					->select('pesertas.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids','peserta_jadwals.no_ujian','peserta_jadwals.no_seri_sertifikat','peserta_jadwals.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwals.tanggal_ujian as tanggal_ujian')
					->where('peserta_jadwals.status_ujian','lulus')
					->where('peserta_jadwals.id_peserta',$id)
					->whereNotNull('no_seri_sertifikat')
					->orderBy('peserta_jadwals.id', 'DESC')
					->first();
		} else {
            $data = DB::table('peserta_instansis')
					->join('pesertas', 'peserta_instansis.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwal_instansis', 'jadwal_instansis.id', '=', 'peserta_instansis.id_jadwal','left')
					->select('pesertas.*','peserta_instansis.metode_ujian as metodes','peserta_instansis.id as ids','peserta_instansis.no_ujian','peserta_instansis.no_seri_sertifikat','peserta_instansis.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwal_instansis.tanggal_ujian as tanggal_ujian')
					->where('peserta_instansis.status_ujian','lulus')
					->where('peserta_instansis.id_peserta',$id)
					->whereNotNull('no_seri_sertifikat')
					->orderBy('peserta_instansis.id', 'DESC')
					->first();
		}
		
        return View::make('edit_sertifikat', compact('data','tipe','id','id_jadwal'));
    }
	
    public function print($tipe, $id)
    {
        if ($tipe == 'regular') {
            $data = DB::table('peserta_jadwals')
					->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwals', 'jadwals.id', '=', 'peserta_jadwals.id_jadwal','left')
					->select('pesertas.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids','peserta_jadwals.no_ujian','peserta_jadwals.no_seri_sertifikat','peserta_jadwals.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwals.tanggal_ujian as tanggal_ujian')
					->where('peserta_jadwals.status_ujian','lulus')
					->where('peserta_jadwals.id_peserta',$id)
					->whereNotNull('no_seri_sertifikat')
					->orderBy('peserta_jadwals.id', 'DESC')
					->first();
		} else {
            $data = DB::table('peserta_instansis')
					->join('pesertas', 'peserta_instansis.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwal_instansis', 'jadwal_instansis.id', '=', 'peserta_instansis.id_jadwal','left')
					->select('pesertas.*','peserta_instansis.metode_ujian as metodes','peserta_instansis.id as ids','peserta_instansis.no_ujian','peserta_instansis.no_seri_sertifikat','peserta_instansis.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwal_instansis.tanggal_ujian as tanggal_ujian')
					->where('peserta_instansis.status_ujian','lulus')
					->where('peserta_instansis.id_peserta',$id)
					->whereNotNull('no_seri_sertifikat')
					->orderBy('peserta_instansis.id', 'DESC')
					->first();
		}

        if ($data) {
            $data_deputi = DB::table('master_sertifikat')->first();
			$metode_inpassing = $data->metodes == 'verifikasi' ? 'Verifikasi Portofolio' : 'Tes Tertulis';
			$tgl_sertifikat = $data->tanggal_ujian;
            $pdf = PDF::loadView('pdf.sertifikat_inpassing', compact('data','data_deputi','metode_inpassing','tgl_sertifikat'));
            return $pdf->stream('sertifikat_inpassing.pdf','I');
        } else {
            return Redirect::back();
        }
    }
	
	public function sertifikatEdit(Request $request)
    {
		$tgl_sertifikat = Helper::getDBFormat($_POST['tanggal_tes']);
		$metode_inpassing = isset($_POST['metode_inp']) ? $_POST['metode_inp'] : "";
		
		$tipe = $_POST['tipe'];
		$id = $_POST['id'];
		if ($tipe == 'regular') {
            $data = DB::table('peserta_jadwals')
					->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwals', 'jadwals.id', '=', 'peserta_jadwals.id_jadwal','left')
					->select('pesertas.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids','peserta_jadwals.no_ujian','peserta_jadwals.no_seri_sertifikat','peserta_jadwals.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwals.tanggal_ujian as tanggal_ujian')
					->where('peserta_jadwals.status_ujian','lulus')
					->where('peserta_jadwals.id_peserta',$id)
					->whereNotNull('no_seri_sertifikat')
					->orderBy('peserta_jadwals.id', 'DESC')
					->first();
		} else {
            $data = DB::table('peserta_instansis')
					->join('pesertas', 'peserta_instansis.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwal_instansis', 'jadwal_instansis.id', '=', 'peserta_instansis.id_jadwal','left')
					->select('pesertas.*','peserta_instansis.metode_ujian as metodes','peserta_instansis.id as ids','peserta_instansis.no_ujian','peserta_instansis.no_seri_sertifikat','peserta_instansis.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwal_instansis.tanggal_ujian as tanggal_ujian')
					->where('peserta_instansis.status_ujian','lulus')
					->where('peserta_instansis.id_peserta',$id)
					->whereNotNull('no_seri_sertifikat')
					->orderBy('peserta_jadwals.id', 'DESC')
					->first();
		}

		
		
        if ($data) {
            $data_deputi = DB::table('master_sertifikat')->first();
			$data->nip = $_POST['nip'];
			$data->nama = $_POST['nama'];
			
			if ($request->hasFile('pas_foto_3_x_4')) {
				$pas_foto_3_x_4 = $request->file('pas_foto_3_x_4');
				$pas_foto_3_x_4_name = Carbon::now()->timestamp ."_pas_foto_3_x_4".".". $pas_foto_3_x_4->getClientOriginalExtension();
				$pas_foto_3_x_4Path = 'storage/data/pas_foto_3_x_4';
				$pas_foto_3_x_4->move($pas_foto_3_x_4Path,$pas_foto_3_x_4_name);
				$data->pas_foto_3_x_4 = $pas_foto_3_x_4_name;
			} else {
				$pas_foto_3_x_4_name = null;
			}
			
			$pdf = PDF::loadView('pdf.sertifikat_inpassing', compact('data','data_deputi','tgl_sertifikat','metode_inpassing'));
            return $pdf->stream('sertifikat_inpassing.pdf','I');
        } else {
            return Redirect::back();
        }
	}
	
	public function templateBelakang($tipe, $id, $id_jadwal)
    {
		ob_end_clean();
		
		if ($tipe == 'regular') {
            $data = DB::table('peserta_jadwals')
					->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwals', 'jadwals.id', '=', 'peserta_jadwals.id_jadwal','left')
					->select('pesertas.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids','peserta_jadwals.no_ujian','peserta_jadwals.no_seri_sertifikat','peserta_jadwals.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwals.tanggal_ujian as tanggal_ujian')
					->where('peserta_jadwals.status_ujian','lulus')
					->where('peserta_jadwals.id_peserta',$id)
					->whereNotNull('no_seri_sertifikat')
					->orderBy('peserta_jadwals.id', 'DESC')
					->first();
		} else {
            $data = DB::table('peserta_instansis')
					->join('pesertas', 'peserta_instansis.id_peserta','=','pesertas.id')
					->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
					->join('dokumens', 'pesertas.id_dokumen', '=', 'dokumens.id','left')
					->join('jadwal_instansis', 'jadwal_instansis.id', '=', 'peserta_instansis.id_jadwal','left')
					->select('pesertas.*','peserta_instansis.metode_ujian as metodes','peserta_instansis.id as ids','peserta_instansis.no_ujian','peserta_instansis.no_seri_sertifikat','peserta_instansis.status_ujian','dokumens.pas_foto_3_x_4','instansis.nama as instansis','jadwal_instansis.tanggal_ujian as tanggal_ujian')
					->where('peserta_instansis.status_ujian','lulus')
					->where('peserta_instansis.id_peserta',$id)
					->whereNotNull('no_seri_sertifikat')
					->orderBy('peserta_instansis.id', 'DESC')
					->first();
		}

        if ($data) {
			
								
            $data_deputi = DB::table('master_sertifikat')->first();
			$judul = array();
			$detail = array();
			$lulus = array();
			$judul_input = JudulInput::where('jenjang',$data->jenjang)->get();
			$detail_input = DetailInput::all();
			$jum = 0;
			
			foreach($judul_input as $j){
				$judul[$jum] = $j->nama;
				$jm = 0;
				$arrs = array();
				$ja = array();
				
				foreach($detail_input as $d){
					$stat = 0;
					if($d->id_judul_input == $j->id){
						$arrs[$jm] = $d->nama;
						$detail_file_inp = DB::table('detail_file_inputs')
											->select('detail_file_inputs.id')
											->where('detail_file_inputs.id_detail_input',$d->id)
											->get();
						
						$arr_df = array();
						foreach($detail_file_inp as $df){
							$data_verif_porto = DB::table('status_file_portofolios')
												->select('status_file_portofolios.id_detail_file_input','status_file_portofolios.status','status_file_portofolios.id_peserta','status_file_portofolios.id_jadwal')
												->where('status_file_portofolios.id_peserta',$id)
												->where('status_file_portofolios.id_jadwal',$id_jadwal)
												->where('status_file_portofolios.id_detail_file_input',$df->id)
												->get();
												
							foreach($data_verif_porto as $a){
								$tt = $a->status;
								if($tt == "sesuai") $stat++;
							}
						}
						
						$ja[$jm] = $stat == 2 ? "1" : "0";
						$jm++;
					}				
				}
				
				$lulus[$jum] = $ja;
				$detail[$jum] = $arrs;
				$jum++;
			}
			
			$jenjang = $data->jenjang;
			$metode_inpassing = $data->metodes == 'verifikasi' ? 'Verifikasi Portofolio' : 'Tes Tertulis';
			$pdf = PDF::loadView('pdf.sertifikat_inpassing_belakang3', compact('data','judul','detail','lulus'));
			return $pdf->stream('sertifikat_inpassing_belakang.pdf','I');
        } else {
            return Redirect::back();
        }
    }
}