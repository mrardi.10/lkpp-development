<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use App\Asesor;
use App\User;
use App\Peserta;
use App\PesertaJadwal;
use View;
use Redirect;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;

class AsesorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data = DB::table('users')
				->select('users.*','users.unit_kerja as unit_kerjas')
				->where('role','asesor')
				->where('deleted_at',null)
				->orderBy('id','DESC')
				->get();
        
		return View::make('data_asesor', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function create()
    {
        if(Auth::user()->role == 'bangprof'){
			return Redirect::back();
		}
        
		$action = 'add';
		$unit_kerja = DB::table('unit_kerjas')->pluck('name','id');
        return View::make('create_asesor', compact('unit_kerja','action'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
	public function store(Request $request)
    {
        $rules = array('nama'  => 'required',
					   'nip'  => 'required',
					   'email' => 'required|unique:users',
					   'password' => 'required|min:6',
					   'c_password' => 'required|same:password',
					   'unit_kerja' => 'required',
					   'status_admin' => 'required');

        $message = ['c_password.same' => 'Pasword harus sama.'];
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::to('tambah-asesor')
					->withErrors($validator)
					->withInput();
        }

        $data = new User();
        $data->name = $request->input('nama');
        $data->nip = $request->input('nip');
        $data->email = $request->input('email');
        $data->password = Hash::make($request->input('password'));
        $data->role = 'asesor';
        $data->admin_type = 'lkpp';
        $data->email_verified_at = Carbon::now();
        $data->unit_kerja = $request->input('unit_kerja');
        $data->jabatan = '-';
        $data->nama_instansi = '-';
        $data->no_telp = '-';
        $data->sk_admin_ppk = '-';
        $data->status_admin = $request->input('status_admin');
        if($data->save())
        {
            return Redirect::to('data-asesor')->with('msg','berhasil');
        } else {
            return Redirect::to('data-asesor')->with('msg','berhasil');
        }
    }

    public function assign(Request $request)
    {
        $id = $request->input('id_peserta');
        $id_pesertajadwal = $request->input('id_pesertajadwal');
        $ass = $request->input('asesor');
        $data = Peserta::find($id);
        $datauser = User::where('id',$ass)->first();
        $datapesertajadwal = PesertaJadwal::find($id_pesertajadwal);
        $datapesertajadwal->asesor = $ass;
        $datapesertajadwal->save();
        $data->asesor = $ass;
        if($data->save()){
            $from = env('MAIL_USERNAME');
            $datamail = array('asesor' => $datauser->name, 'email' => Auth::user()->email, 'role' => Auth::user()->role,'name' => Auth::user()->name, 'from' => $from,'nama_peserta' => $request->input('nama_peserta'));
            $admin_superadmin = DB::table('users')->where('role','superadmin')->get();
            $admin_dsp = DB::table('users')->where('role','dsp')->get();
            $admin_asesor = DB::table('users')->where('id', $ass)->get();
            foreach($admin_superadmin as $superadmins){
                Mail::send('mail.assign_asesor_sp', $datamail, function($message) use ($datamail,$superadmins) {
                    $message->to($superadmins->email)->subject('Assign Asesor');
                    $message->from($datamail['from'],$datamail['name']);
                });
            }
			
            foreach($admin_dsp as $dsps){
                Mail::send('mail.assign_asesor_dsp', $datamail, function($message) use ($datamail,$dsps) {
                    $message->to($dsps->email)->subject('Assign Asesor');
                    $message->from($datamail['from'],$datamail['name']);
                });
            }
			
            foreach($admin_asesor as $asesors){
                Mail::send('mail.assign_asesor_ass', $datamail, function($message) use ($datamail,$asesors) {
                    $message->to($asesors->email)->subject('Assign Asesor');
                    $message->from($datamail['from'],$datamail['name']);
                });
            }
			
            $msg = 'berhasil';  
        } else {
            $msg = 'gagal';
        }

        return Redirect::back()->with('msg',$msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
	public function show($id)
    {
        $action = 'detail';
        $data = DB::table('users')
				->join('unit_kerjas','users.unit_kerja','=','unit_kerjas.id','left')
				->select('users.*','unit_kerjas.name as unit_kerjas')
				->where('users.id',$id)
				->first();
    
		return View::make('create_asesor', compact('action','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        if(Auth::user()->role == 'bangprof'){
			return Redirect::back();
		}

        $action = 'edit';
		$data = User::find($id);
		$unit_kerja = DB::table('unit_kerjas')->pluck('name','id');
		return View::make('create_asesor', compact('unit_kerja','action','data'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $password = $request->input('password');
        if ($password != "") {
            $rules = array('nama' => 'required',
						   'nip' => 'required',
						   'email' => 'required',
						   'password' => 'required|min:6',
						   'c_password' => 'required|same:password',
						   'unit_kerja' => 'required',
						   'status_admin' => 'required');
            
			$password_input = Hash::make($request->input('password'));
        } else {
            $rules = array('nama'  => 'required',
						   'nip'  => 'required',
						   'email' => 'required',
						   'unit_kerja' => 'required',
						   'status_admin' => 'required'); 
            
			$password_input = $request->input('old_password');
        }

        $message = ['c_password.same' => 'Password harus sama.'];
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::back()
				->withErrors($validator)
				->withInput();
        }

        $data = User::find($id);
        $data->name = $request->input('nama');
        $data->nip = $request->input('nip');
        $data->email = $request->input('email');
        $data->password = $password_input;
        $data->role = 'asesor';
        $data->admin_type = 'lkpp';
        $data->email_verified_at = Carbon::now();
        $data->unit_kerja = $request->input('unit_kerja');
        $data->jabatan = '-';
        $data->nama_instansi = '-';
        $data->no_telp = '-';
        $data->sk_admin_ppk = '-';
        $data->status_admin = $request->input('status_admin');
        if($data->save())
        {
            return Redirect::to('data-asesor')->with('msg','berhasil_update');
        } else {
            return Redirect::to('data-asesor')->with('msg','berhasil_update');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);
        if($data->delete())
        {
            return Redirect::to('data-asesor')->with('msg','berhasil_hapus');
        } else {
            return Redirect::to('data-asesor')->with('msg','gagal_hapus');
        }
    }
}