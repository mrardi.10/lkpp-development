<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Pertek;
use App\Sphu;
use App\SuratUsulan;
use App\Peserta;
use App\Pesertapertek;
use App\Pesertasphu;
use App\PesertaJadwal;
use App\PesertaInstansi;
use Carbon\Carbon;
use DB;
use Validator;
use Helper;
use View;
use Redirect;

class PertekSphuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$bulan = $request->input('bulan');

        if ($bulan == "") {
			$pertek = DB::table('perteks')
					  ->join('instansis', 'instansis.id','=','perteks.instansi')
					  ->select('perteks.*','instansis.nama as nama_instansi')
					  ->orderby('perteks.id','desc')
					  ->get();
		} else {
			$pertek = DB::table('perteks')
					  ->join('instansis', 'instansis.id','=','perteks.instansi')
					  ->select('perteks.*','instansis.nama as nama_instansi')
					  ->orderby('perteks.id','desc')
					  ->whereMonth('tanggal_pertek', '=', $bulan)
					  ->get();
		}

		if ($bulan == "") {
			$sphu = DB::table('sphus')
					->join('instansis', 'instansis.id','=','sphus.instansi')
					->select('sphus.*','instansis.nama as nama_instansi')
					->orderby('sphus.id','desc')
					->get();
        } else {
			$sphu = DB::table('sphus')
					->join('instansis', 'instansis.id','=','sphus.instansi')
					->select('sphus.*','instansis.nama as nama_instansi')
					->orderby('sphus.id','desc')
					->whereMonth('tanggal_sphu', '=', $bulan)
					->get();

		}
    	
		return View::make('pertek_sphu', compact('data','bulan','pertek','sphu'));
    }

    public function indexlkpp($id)
    {
    	$data = DB::table('riwayat_users')
				->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
				->join('users', 'riwayat_users.id_admin','=','users.id','left')
				->join('users as admin', 'riwayat_users.id_admin_lkpp','=','admin.id','left')
				->select('riwayat_users.*','pesertas.nama as nama_peserta','users.name as nama_admin','admin.name as admin_lkpp')
				->where('pesertas.id',$id)
				->orderby('id','desc')  
				->get();

		return View::make('riwayat_user_lkpp', compact('data'));
    }

    public function ujianPeserta(request $request)
    {       
    	$nip = $request->input('nip');

    	if ($nip == "") {
    		$data = DB::table('riwayat_users')
					->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
					->join('users', 'riwayat_users.id_admin','=','users.id','left')
					->join('users as admin', 'riwayat_users.id_admin_lkpp','=','admin.id','left')
					->select('riwayat_users.*','pesertas.nama as nama_peserta','pesertas.nip as nip','users.name as nama_admin','admin.name as admin_lkpp')
					->orderby('id','desc')  
					->get();
    	} else {
    		$data = DB::table('riwayat_users')
					->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
					->join('users', 'riwayat_users.id_admin','=','users.id','left')
					->join('users as admin', 'riwayat_users.id_admin_lkpp','=','admin.id','left')
					->select('riwayat_users.*','pesertas.nama as nama_peserta','pesertas.nip as nip','users.name as nama_admin','admin.name as admin_lkpp')
					->where('pesertas.nip',$nip)
					->orderby('id','desc')  
					->get();
    	}
    	
		return View::make('riwayat_ujian_peserta', compact('data','nip'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPertek()
    {
    	if (Auth::user()->role == 'dsp'){
    		return Redirect::back();
    	}

    	$instansi = DB::table('instansis')
					->orderby('nama','asc')
					->get();

    	$terdaftar = Pesertapertek::get();
    	$inputinstansi = Input::get('instansi');
    	$no_surat = Input::get('no_surat_usulan_peserta');
		$peserta =  DB::table('instansis')
					->join('pesertas','pesertas.nama_instansi','=','instansis.id')
					->join('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta')
					->Join('surat_usulans', function($Join){
						$Join->on('surat_usulans.id_jadwal', '=', 'peserta_jadwals.id_jadwal')
						->on('surat_usulans.id_peserta', '=', 'peserta_jadwals.id_peserta')
						->on('surat_usulans.jenis_ujian', '=', DB::raw("'regular'"))
						->where('surat_usulans.status', '=', 0);	
					})
					->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
					->select('pesertas.*','peserta_jadwals.*','peserta_jadwals.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwals.tanggal_ujian as tanggal_ujian','peserta_jadwals.id_jadwal as id_jadwal','peserta_jadwals.metode_ujian as metode','peserta_jadwals.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_jadwals.id as ids')
					->where('pesertas.deleted_at', null)
					->where('pesertas.status_peserta','aktif')
					->whereIn('peserta_jadwals.status_ujian', ['lulus','tidak_lulus','tidak_hadir','tidak_lengkap'])
					->where('peserta_jadwals.publish','publish')
					->where('instansis.id',$inputinstansi)
					->where(function ($query) use ($no_surat) {
						if ($no_surat != "") {
							foreach($no_surat as $key => $value){
								$lists[] = $value;
							}
							
							$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
						} else { }
					})
					->groupby('peserta_jadwals.id')
					->orderby('surat_usulans.no_surat_usulan_peserta')
					->get();
		
		$pesertaInt =  DB::table('instansis')
						->join('pesertas','pesertas.nama_instansi','=','instansis.id','left')       
						->join('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta','left')
						->Join('surat_usulans', function($Join){
							$Join->on('surat_usulans.id_jadwal', '=', 'peserta_instansis.id_jadwal')
							->on('surat_usulans.id_peserta', '=', 'peserta_instansis.id_peserta')
							->on('surat_usulans.jenis_ujian', '=', DB::raw("'instansi'"))
							->where('surat_usulans.status', '=', 0);
						})
						->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id','left')
						->select('pesertas.*','peserta_instansis.*','peserta_instansis.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi' ,'jadwal_instansis.tanggal_ujian as tanggal_ujian','peserta_instansis.id_jadwal as id_jadwal','peserta_instansis.metode_ujian as metode','peserta_instansis.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_instansis.id as ids')
						->where('pesertas.deleted_at', null)
						->where('pesertas.status_peserta','aktif')
						->whereIn('peserta_instansis.status_ujian', ['lulus','tidak_lulus','tidak_hadir','tidak_lengkap'])
						->where('peserta_instansis.publish','publish')
						->where('pesertas.nama_instansi',$inputinstansi)
						->where(function ($query) use ($no_surat) {
							if ($no_surat != "") {
								foreach($no_surat as $key => $value)
								{
									$lists[] = $value;
								}
								$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
							} else { }
						})
						->groupby('peserta_instansis.id')
						->orderby('surat_usulans.no_surat_usulan_peserta')
						->get();
					
		if ($inputinstansi != "") {
    		$Pilihinstansi = Input::get('instansi');
    		$NoSurat =  DB::table('instansis')
						->join('pesertas','pesertas.nama_instansi','=','instansis.id','left')
						->join('surat_usulans','surat_usulans.id_peserta','=','pesertas.id','left')
						->select('surat_usulans.no_surat_usulan_peserta as no_surat')
						->where('instansis.id',$Pilihinstansi)
						->groupby('surat_usulans.no_surat_usulan_peserta')
						->get();

    		if ($no_surat != "") {
    			foreach($no_surat as $key => $value){
    				$input_nosurat[] = $value;
    			}
    		} else {
    			$input_nosurat = "";
    		}
    	} else {
    		$Pilihinstansi = ""; 
    		$NoSurat = "";
    		$input_nosurat = "";
    		$statuspertek = "";
    		$val_no_usulan = "";
    	}
        
    	return View::make('add_pertek',compact('instansi','peserta','pesertaInt','inputinstansi','NoSurat','Pilihinstansi','input_nosurat','terdaftar','statuspertek','val_no_usulan'));
    }

    public function storePertek(Request $request)
    {
    	$instansi = $request->input('instansi_input');
        $no_surat_usulan = $request->input('no_surat_usulan');
        $no_surat_usulan_tidak = $request->input('no_surat_usulan_tidak');
        $daftar_peserta = $request->input('peserta');
        $tidak_daftar_peserta = $request->input('pesertatidak');
        $no_surat_usulan_int = $request->input('no_surat_usulan_int');
        $no_surat_usulan_tidak_int = $request->input('no_surat_usulan_tidak_int');
        $daftar_peserta_int = $request->input('peserta_int');
        $tidak_daftar_peserta_int = $request->input('pesertatidak_int');
        $status_pertek = $request->input('status_pertek');
        $no_pertek = $request->input('no_pertek');
        $tanggal_pertek = $request->input('tanggal_pertek');
        $id_jadwal = $request->input('id_jadwal');
        $draft = $request->input('draft');
        $eformasi = $request->input('id_formasi');
        $idformasi = "";

        if ($tidak_daftar_peserta != "") {
            foreach( $no_surat_usulan_tidak as $index => $nosurat ) {
				$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta[$index])->first();
				if ($statususulan) {
					$statususulan->status = 0;
					$statususulan->save();
				} else{}
				$pesertaPertek = Pesertapertek::where('nomor_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta[$index])->delete();
			}
        }

        if ($tidak_daftar_peserta_int != "") {
            foreach( $no_surat_usulan_tidak_int as $index => $nosurat ) {
				$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta_int[$index])->first();
				if ($statususulan) {
					$statususulan->status = 0;
					$statususulan->save();
				} else{}
				
				$pesertaPertek = Pesertapertek::where('nomor_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta_int[$index])->delete();
			}
        }
		
		$pertek = new Pertek();
		
		if ($instansi !="") {
			$pertek->instansi = $instansi;
		}
		
		if ($status_pertek !="") {
			$pertek->status_pertek = $status_pertek;
		}
		
		if ($draft !="") {
			$pertek->draft = $draft;
		}
		
		if ($no_pertek != "") {
			$pertek->no_pertek = $no_pertek;
		}
		
		if ($tanggal_pertek != "") {
			$tgl_pertek = explode('/',$tanggal_pertek);
			$pertek->tanggal_pertek = $tgl_pertek[2].'-'.$tgl_pertek[0].'-'.$tgl_pertek[1];
		}
		
		$savepertek = $pertek->save();
		$nopertek = $pertek->id;

        if ($daftar_peserta != "") {
            if ($no_surat_usulan != "") {
                foreach( $no_surat_usulan as $index => $nosurat ) {
					$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$daftar_peserta[$index])->first();
					$statususulan->status = 1;
					$statususulan->save();
					$pesertaPertek = new Pesertapertek();
					$pesertaPertek->id_pertek = $nopertek;
					$pesertaPertek->id_peserta = $daftar_peserta[$index];
					
					if ($no_pertek != "") {
						$pesertaPertek->no_pertek = $no_pertek;
					}
					
					$pesertaPertek->nomor_surat_usulan_peserta = $nosurat;
					$savepeserta = $pesertaPertek->save();                
                }

                if ($savepertek && $savepeserta) {
                    $msg = "berhasil";
                } else {
                    $msg = "gagal"; 
                }             
            }
        }
		
        if ($daftar_peserta_int != "") {
            if ($no_surat_usulan_int != "") {
                foreach( $no_surat_usulan_int as $index => $nosurat ) {
					$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$daftar_peserta_int[$index])->first();
					$statususulan->status = 1;
					$statususulan->save();
					$pesertaPertek = new Pesertapertek();
					$pesertaPertek->id_pertek = $nopertek;
					$pesertaPertek->id_peserta = $daftar_peserta_int[$index];
					
					if ($no_pertek != "") {
						$pesertaPertek->no_pertek = $no_pertek;
					}
					
					$pesertaPertek->nomor_surat_usulan_peserta = $nosurat;
					$savepeserta = $pesertaPertek->save();                
                }

                if ($savepertek && $savepeserta) {
                    $msg = "berhasil";
                } else {
                    $msg = "gagal"; 
                }             
            }
        }
		
        $msg = "berhasil";        
        return Redirect::to('edit-pertek/'.$nopertek)->with('msg',$msg);
    }

    public function editPertek($id)
    {
        if (Auth::user()->role == 'dsp'){
            return Redirect::back();
        }

        $perteks = DB::table('perteks')
					->join('peserta_perteks','peserta_perteks.id_pertek','=','perteks.id')
					->select('peserta_perteks.nomor_surat_usulan_peserta as no_surat','perteks.*')
					->where('perteks.id',$id)
					->groupby('peserta_perteks.id')
					->get();

        $statuspertek = Pertek::find($id);
        
		$instansis = DB::table('perteks')
					->select('perteks.instansi')
					->where('perteks.id',$id)
					->get();

        $instansi = DB::table('instansis')
					->orderby('nama','asc')
					->get();

        $NoSuratUsulan =  DB::table('perteks')
						  ->join('pesertas','pesertas.nama_instansi','=','perteks.instansi','left')
						  ->join('surat_usulans','surat_usulans.id_peserta','=','pesertas.id','left')
						  ->select('surat_usulans.no_surat_usulan_peserta as no_surat')
						  ->where('perteks.id',$id)
						  ->groupby('surat_usulans.no_surat_usulan_peserta')
						  ->get();

        $terdaftar = Pesertapertek::get();
        $inputinstansi = Input::get('instansi');
		
        $no_surat = Input::get('no_surat_usulan_peserta');
        if ($no_surat == "") {
            $tampil_ready = 'tidak_tampil';
        } else {
            $tampil_ready = 'tampil';
        }

        $peserta =  DB::table('instansis')
					->join('pesertas','pesertas.nama_instansi','=','instansis.id')
					->join('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta')
					->Join('surat_usulans', function($Join){
						$Join->on('surat_usulans.id_jadwal', '=', 'peserta_jadwals.id_jadwal')
						->on('surat_usulans.id_peserta', '=', 'peserta_jadwals.id_peserta');
					})
					->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
					->select('pesertas.*','peserta_jadwals.*','peserta_jadwals.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwals.tanggal_ujian as tanggal_ujian','peserta_jadwals.id_jadwal as id_jadwal','peserta_jadwals.metode_ujian as metode','peserta_jadwals.ak_kumulatif as ak','peserta_jadwals.id as ids','surat_usulans.no_surat_usulan_peserta')
					->where('pesertas.deleted_at', null)
					->where('pesertas.status_peserta','aktif')
					->whereIn('peserta_jadwals.status_ujian', ['lulus','tidak_lulus','tidak_hadir','tidak_lengkap'])
					->where('peserta_jadwals.publish','publish')
					->where('instansis.id',$inputinstansi)
					->where(function ($query) use ($no_surat) {
						if ($no_surat != "") {
							foreach($no_surat as $key => $value){
								$lists[] = $value;
							}
							
							$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
						} else {}
					})
					->groupby('peserta_jadwals.id')
					->get();
		
		$pesertaInt =  DB::table('instansis')
						->join('pesertas','pesertas.nama_instansi','=','instansis.id','left')        
						->join('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta','left')
						->Join('surat_usulans', function($Join){
							$Join->on('surat_usulans.id_jadwal', '=', 'peserta_instansis.id_jadwal')
							->on('surat_usulans.id_peserta', '=', 'peserta_instansis.id_peserta');
						})
						->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id','left')
						->select('pesertas.*','peserta_instansis.*','peserta_instansis.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi' ,'jadwal_instansis.tanggal_ujian as tanggal_ujian','peserta_instansis.id_jadwal as id_jadwal','peserta_instansis.metode_ujian as metode','peserta_instansis.ak_kumulatif as ak','peserta_instansis.id as ids','surat_usulans.no_surat_usulan_peserta')
						->where('pesertas.deleted_at', null)
						->where('pesertas.status_peserta','aktif')
						->whereIn('peserta_instansis.status_ujian', ['lulus','tidak_lulus','tidak_hadir','tidak_lengkap'])
						->where('peserta_instansis.publish','publish')
						->where('pesertas.nama_instansi',$inputinstansi)
						->where(function ($query) use ($no_surat) {
							if ($no_surat != "") {
								foreach($no_surat as $key => $value){
									$lists[] = $value;
								}
								$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
							} else {}
						})
						->groupby('peserta_instansis.id')
						->get();
		
		if ($inputinstansi != "") {
            $Pilihinstansi = Input::get('instansi');
            $NoSurat =  DB::table('instansis')
						->join('pesertas','pesertas.nama_instansi','=','instansis.id','left')
						->join('surat_usulans','surat_usulans.id_peserta','=','pesertas.id','left')
						->select('surat_usulans.no_surat_usulan_peserta as no_surat')
						->where('instansis.id',$Pilihinstansi)
						->groupby('surat_usulans.no_surat_usulan_peserta')
						->get();
       
            if ($no_surat != "") {
                foreach($no_surat as $key => $value){
                    $input_nosurat[] = $value;
                }
            } else {
                $input_nosurat = "";
            }
        } else {
            $Pilihinstansi = ""; 
            $NoSurat = "";
            $input_nosurat = "";
            $statuspertek = "";
            $val_no_usulan = "";
        }
        
        return View::make('edit_pertek',compact('instansi','peserta','pesertaInt','inputinstansi','NoSurat','Pilihinstansi','input_nosurat','terdaftar','statuspertek','val_no_usulan','perteks','perteksInt','instansis','NoSuratUsulan','tampil_ready','id'));
    }


    public function updatePertek(Request $request,$id)
    {
        $instansi = $request->input('instansi_input');
        $no_surat_usulan = $request->input('no_surat_usulan');
        $status_pertek   = $request->input('status_pertek');
        $no_pertek   = $request->input('no_pertek');
        $tanggal_pertek  = $request->input('tanggal_pertek');
        $daftar_peserta = $request->input('peserta');
        $tidak_daftar_peserta = $request->input('pesertatidak');
        $draft = $request->input('draft');
        $eformasi = $request->input('id_formasi');
        $idformasi = "";
		
		if ($tidak_daftar_peserta != "") {
			foreach (array_combine($no_surat_usulan ,$tidak_daftar_peserta ) as $nomor => $peserta) {
				$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nomor)->where('id_peserta',$peserta)->first();
				if ($statususulan) {
					$statususulan->status = 0;
					$statususulan->save();
				} else {}
				$pesertaPertek = Pesertapertek::where('no_surat_usulan_peserta',$nomor)->where('id_peserta',$peserta)->delete();
			}
        }

        if ($daftar_peserta != "") {
            if ($no_surat_usulan != "") {                
                $pertek  = Pertek::find($id);
                if ($instansi !="") {
                    $pertek->instansi = $instansi;
                }
				
                if ($status_pertek   !="") {
                    $pertek->status_pertek  = $status_pertek   ;
                }
                
				if ($draft !="") {
                    $pertek->draft = $draft;
                }
                
				if ($no_pertek   != "") {
                    $pertek->no_pertek  = $no_pertek;
                }
                
                if ($tanggal_pertek  != "") {
                    $tgl_pertek  = explode('/',$tanggal_pertek  );
                    $pertek->tanggal_pertek     = $tgl_pertek  [2].'-'.$tgl_pertek [0].'-'.$tgl_pertek [1];
                }

                $savepertek  = $pertek->save();
                $nopertek    = $pertek->id;
				$pesertapertek   = Pesertapertek::Where('id_pertek',$id);
				
				if ($pesertapertek) {
					$pesertapertek->delete();
				}
                    
                foreach (array_combine($no_surat_usulan ,$daftar_peserta ) as $nomor => $peserta) {
					$pesertapertek = New Pesertapertek();
					$pesertapertek->id_pertek  = $nopertek    ;
					$pesertapertek->id_peserta = $peserta;
					if ($no_pertek != "") {
						$pesertapertek->no_pertek  = $no_pertek   ;
					}
					
					$pesertapertek->nomor_surat_usulan_peserta = $nomor;
					$savepeserta = $pesertapertek->save();                
                }

                if ($savepertek  && $savepeserta) {
                    $msg = "berhasil";
                } else {
                    $msg = "gagal"; 
                }             
            }
        } else {
             $msg = "gagal"; 
        }  
              
        return Redirect::to('pertek-sphu')->with('msg',$msg);
    }

    public function NoSurat($id){
    	if ($id == "") {
    		return response()->json(['msg' => 'gagal']); 
    	} else {
    		$instansi = $id;
    		$NoSurat =  DB::table('instansis')
						->join('pesertas','pesertas.nama_instansi','=','instansis.id','left')
						->join('surat_usulans','surat_usulans.id_peserta','=','pesertas.id','left')
						->select('surat_usulans.no_surat_usulan_peserta as no_surat')
						->where('instansis.id',$instansi)
						->groupby('surat_usulans.no_surat_usulan_peserta')
						->get();

    		$peserta =  DB::table('instansis')
						->join('pesertas','pesertas.nama_instansi','=','instansis.id','left')
						->select('pesertas.*')
						->where('instansis.id',$instansi)
						->where('pesertas.deleted_at', null)
						->whereIn('pesertas.status_inpassing', [6,7,11])
						->get();

    		$option = '';
            $option_not = '';
    		foreach ($NoSurat as $key => $data) {
                if ($data->no_surat != null) {
					$option .= "<option value='".$data->no_surat."'>".$data->no_surat."</option>";
                } else {
					$option_not .= "<option >tidak ditemukan</option>";
				}
			}
            
    		return response()->json(['msg' => 'berhasil', 'id' => $id, 'data' => $option , 'data_not' => $option_not ]);
    	}
    }

    public function searchInstansi(){
    	$inputinstansi = Input::get('instansi');
    	$peserta = DB::table('pesertas')
					->where('deleted_at',null)
					->where(function ($query) use ($inputinstansi) {
						if ($inputinstansi != "") {
							$query->where('nama_instansi',$inputinstansi);
						}
		
						if ($inputinstansi == "") {
							// $query->where('nama_instansi',$inputinstansi);
						}
					})
					->orderBy('pesertas.id','DESC')
					->get();
    	return View::make('add_pertek',compact('instansi','peserta','inputinstansi'));
    }


    public function createSPHU()
    {
        if (Auth::user()->role == 'dsp'){
            return Redirect::back();
        }

        $instansi = DB::table('instansis')
					->orderby('nama','asc')
					->get();

		$terdaftar = Pesertasphu::get();
        $inputinstansi = Input::get('instansi');
        $no_surat = Input::get('no_surat_usulan_peserta');

        $peserta =  DB::table('instansis')
					->join('pesertas','pesertas.nama_instansi','=','instansis.id')
					->join('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta')
					->Join('surat_usulans', function($Join)
					{
						$Join->on('surat_usulans.id_jadwal', '=', 'peserta_jadwals.id_jadwal')
						->on('surat_usulans.id_peserta', '=', 'peserta_jadwals.id_peserta')
						->on('surat_usulans.jenis_ujian', '=', DB::raw("'regular'"))
						->where('surat_usulans.status', '=', 0);		
					})
					->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
					->select('pesertas.*','peserta_jadwals.*','peserta_jadwals.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwals.tanggal_ujian as tanggal_ujian','peserta_jadwals.id_jadwal as id_jadwal','peserta_jadwals.metode_ujian as metode','peserta_jadwals.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_jadwals.id as ids')
					->where('pesertas.deleted_at', null)
					->where('pesertas.status_peserta','aktif')
					->whereIn('peserta_jadwals.status_ujian', ['tidak_lulus','tidak_hadir','tidak_lengkap'])
					->where('peserta_jadwals.publish','publish')
					->where('instansis.id',$inputinstansi)
					->where(function ($query) use ($no_surat) {
						if ($no_surat != "") {
							foreach($no_surat as $key => $value){
								$lists[] = $value;
							}
						
						$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
						} else {}
					})
					->groupby('peserta_jadwals.id')
					->orderby('surat_usulans.no_surat_usulan_peserta')
					->get();

        $pesertaInt =  DB::table('instansis')
						->join('pesertas','pesertas.nama_instansi','=','instansis.id','left')        
						->join('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta','left')
						->Join('surat_usulans', function($Join)
						{
							$Join->on('surat_usulans.id_jadwal', '=', 'peserta_instansis.id_jadwal')
							->on('surat_usulans.id_peserta', '=', 'peserta_instansis.id_peserta')
							->on('surat_usulans.jenis_ujian', '=', DB::raw("'instansi'"))
							->where('surat_usulans.status', '=', 0);
						})
						->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id','left')
						->select('pesertas.*','peserta_instansis.*','peserta_instansis.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi' ,'jadwal_instansis.tanggal_ujian as tanggal_ujian','peserta_instansis.id_jadwal as id_jadwal','peserta_instansis.metode_ujian as metode','peserta_instansis.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_instansis.id as ids')
						->where('pesertas.deleted_at', null)
						->where('pesertas.status_peserta','aktif')
						->whereIn('peserta_instansis.status_ujian', ['tidak_lulus','tidak_hadir','tidak_lengkap'])
						->where('peserta_instansis.publish','publish')
						->where('pesertas.nama_instansi',$inputinstansi)
						->where(function ($query) use ($no_surat) {
							if ($no_surat != "") {
								foreach($no_surat as $key => $value)
								{
									$lists[] = $value;
								}
								
								$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
							} else {}
						})
						->groupby('peserta_instansis.id')
						->orderby('surat_usulans.no_surat_usulan_peserta')
						->get();        

		if ($inputinstansi != "") {
            $Pilihinstansi = Input::get('instansi');
            $NoSurat =  DB::table('instansis')
						->join('pesertas','pesertas.nama_instansi','=','instansis.id','left')
						->join('surat_usulans','surat_usulans.id_peserta','=','pesertas.id','left')
						->select('surat_usulans.no_surat_usulan_peserta as no_surat')
						->where('instansis.id',$Pilihinstansi)
						->groupby('surat_usulans.no_surat_usulan_peserta')
						->get();
						
            if ($no_surat != "") {
                foreach($no_surat as $key => $value){
                    $input_nosurat[] = $value;
                }
            } else {
                $input_nosurat = "";
            }
        } else {
            $Pilihinstansi = ""; 
            $NoSurat = "";
            $input_nosurat = "";
            $statussphu  = "";
            $val_no_usulan = "";
        }
       
        return View::make('add_sphu ',compact('instansi','peserta','pesertaInt','inputinstansi','NoSurat','Pilihinstansi','input_nosurat','terdaftar','statussphu   ','val_no_usulan'));
    }
	
	public function editSPHU($id){
        if (Auth::user()->role == 'dsp'){
            return Redirect::back();
        }

        $sphus = DB::table('sphus')
				->join('peserta_sphus','peserta_sphus.id_sphu','=','sphus.id')
				->select('peserta_sphus.nomor_surat_usulan_peserta as no_surat','sphus.*')
				->where('sphus.id',$id)
				->groupby('peserta_sphus.id')
				->get();

        $statussphu = Sphu::find($id);
        $instansis = DB::table('sphus')
					->select('sphus.instansi')
					->where('sphus.id',$id)
					->get();

        $instansi = DB::table('instansis')
					->orderby('nama','asc')
					->get();
	
        $NoSuratUsulan =  DB::table('sphus')
						  ->join('pesertas','pesertas.nama_instansi','=','sphus.instansi','left')
						  ->join('surat_usulans','surat_usulans.id_peserta','=','pesertas.id','left')
						  ->select('surat_usulans.no_surat_usulan_peserta as no_surat')
						  ->where('sphus.id',$id)
						  ->groupby('surat_usulans.no_surat_usulan_peserta')
						  ->get();

        $terdaftar = Pesertasphu::get();
        $inputinstansi = Input::get('instansi');
        $no_surat = Input::get('no_surat_usulan_peserta');

        if ($inputinstansi == "") {
            $tampil_ready = 'tidak_tampil';
        } else {
            $tampil_ready = 'tampil';
        }

        $peserta =  DB::table('instansis')
					->join('pesertas','pesertas.nama_instansi','=','instansis.id')
					->join('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta')
					 ->Join('surat_usulans', function($Join){
						 $Join->on('surat_usulans.id_jadwal', '=', 'peserta_jadwals.id_jadwal')
						 ->on('surat_usulans.id_peserta', '=', 'peserta_jadwals.id_peserta');
					})
					->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
					->select('pesertas.*','peserta_jadwals.*','peserta_jadwals.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwals.tanggal_ujian as tanggal_ujian','peserta_jadwals.id_jadwal as id_jadwal','peserta_jadwals.metode_ujian as metode','peserta_jadwals.ak_kumulatif as ak','peserta_jadwals.id as ids','surat_usulans.no_surat_usulan_peserta')
					->where('pesertas.deleted_at', null)
					->where('pesertas.status_peserta','aktif')
					->whereIn('peserta_jadwals.status_ujian', ['tidak_lulus','tidak_hadir','tidak_lengkap'])
					->where('peserta_jadwals.publish','publish')
					->where('instansis.id',$inputinstansi)
					->where(function ($query) use ($no_surat) {
						if ($no_surat != "") {
							foreach($no_surat as $key => $value)
							{
								$lists[] = $value;
							}
							
							$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
						}
						else{

						}
					})
					->groupby('peserta_jadwals.id')
					->get();

        $pesertaInt =  DB::table('instansis')
						->join('pesertas','pesertas.nama_instansi','=','instansis.id','left')        
						->join('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta','left')
						->Join('surat_usulans', function($Join)
						{
							$Join->on('surat_usulans.id_jadwal', '=', 'peserta_instansis.id_jadwal')
							->on('surat_usulans.id_peserta', '=', 'peserta_instansis.id_peserta');
						})
						->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id','left')
						->select('pesertas.*','peserta_instansis.*','peserta_instansis.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi' ,'jadwal_instansis.tanggal_ujian as tanggal_ujian','peserta_instansis.id_jadwal as id_jadwal','peserta_instansis.metode_ujian as metode','peserta_instansis.ak_kumulatif as ak','peserta_instansis.id as ids','surat_usulans.no_surat_usulan_peserta')
						->where('pesertas.deleted_at', null)
						->where('pesertas.status_peserta','aktif')
						->whereIn('peserta_instansis.status_ujian', ['tidak_lulus','tidak_hadir','tidak_lengkap'])
						->where('peserta_instansis.publish','publish')
						->where('pesertas.nama_instansi',$inputinstansi)
						->where(function ($query) use ($no_surat) {
							if ($no_surat != "") {
								foreach($no_surat as $key => $value)
								{
									$lists[] = $value;
								}
								$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
							} else {}
						})
						->groupby('peserta_instansis.id')
						->get();
		
		if ($inputinstansi != "") {
            $Pilihinstansi = Input::get('instansi');
            $NoSurat =  DB::table('instansis')
						->join('pesertas','pesertas.nama_instansi','=','instansis.id','left')
						->join('surat_usulans','surat_usulans.id_peserta','=','pesertas.id','left')
						->select('surat_usulans.no_surat_usulan_peserta as no_surat')
						->where('instansis.id',$Pilihinstansi)
						->groupby('surat_usulans.no_surat_usulan_peserta')
						->get();        

            if ($no_surat != "") {
                foreach($no_surat as $key => $value){
                    $input_nosurat[] = $value;
                }
            } else {
                $input_nosurat = "";
            }
        } else {
            $Pilihinstansi = ""; 
            $NoSurat = "";
            $input_nosurat = "";
            $statussphu = "";
            $val_no_usulan = "";
        }

        return View::make('edit_sphu',compact('instansi','peserta','pesertaInt','inputinstansi','NoSurat','Pilihinstansi','input_nosurat','terdaftar','statussphu','val_no_usulan','sphus','sphusInt','instansis','NoSuratUsulan','id','tampil_ready'));
    }
	
	public function updateSPHU(Request $request,$id){
        $instansi = $request->input('instansi_input');
        $no_surat_usulan = $request->input('no_surat_usulan');
        $status_sphu = $request->input('status_sphu ');
        $no_sphu  = $request->input('no_sphu');
        $tanggal_sphu = $request->input('tanggal_sphu');
        $daftar_peserta = $request->input('peserta');
        $tidak_daftar_peserta = $request->input('pesertatidak');
        $draft = $request->input('draft');
        $eformasi = $request->input('id_formasi');
        $idformasi = "";
		
		if ($tidak_daftar_peserta != "") {
			foreach (array_combine($no_surat_usulan ,$tidak_daftar_peserta ) as $nomor => $peserta) {
				$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nomor)->where('id_peserta',$peserta)->first();
				if ($statususulan) {
					$statususulan->status = 0;
					$statususulan->save();
				} else {}
				$pesertaPertek = Pesertasphu::where('no_surat_usulan_peserta',$nomor)->where('id_peserta',$peserta)->delete();
			}
        }

        if ($daftar_peserta != "") {
            if ($no_surat_usulan != "") {
                $sphu= Sphu::find($id);
                if ($instansi !="") {
                    $sphu->instansi = $instansi;
                }
                
				if ($status_sphu !="") {
                    $sphu->status_sphu = $status_sphu;
                }
                
				if ($draft !="") {
                    $sphu->draft = $draft;
                }
                
				if ($no_sphu != "") {
                    $sphu->no_sphu = $no_sphu           ;
                }
                
                if ($tanggal_sphu != "") {
                    $tgl_sphu = explode('/',$tanggal_sphu         );
                    $sphu->tanggal_sphu = $tgl_sphu[2].'-'.$tgl_sphu[0].'-'.$tgl_sphu[1];
                }

                $savesphu= $sphu->save();
                $nosphu  = $sphu->id;
				$pesertasphu = Pesertasphu::Where('id_sphu',$id);
				if ($pesertasphu) {
					$pesertasphu->delete();
				}
                    
                foreach (array_combine($no_surat_usulan ,$daftar_peserta ) as $nomor => $peserta) {
					$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nomor)->where('id_peserta',$peserta)->first();
					$statususulan->status = 2;
					$statususulan->save();
					$pesertasphu = New Pesertasphu();
					$pesertasphu->id_sphu  = $nosphu;
					$pesertasphu->id_peserta = $peserta;
					
					if ($no_sphu != "") {
						$pesertasphu->no_sphu  = $no_sphu  ;
					}
					
					$pesertasphu->nomor_surat_usulan_peserta = $nomor;
					$savepeserta = $pesertasphu->save();                
                }
				
                if ($savesphu&& $savepeserta) {
                    $msg = "berhasil";
                } else {
                    $msg = "gagal"; 
                }             
            }
        } else {
             $msg = "gagal"; 
        }  
             
        return Redirect::to('pertek-sphu')->with('msg',$msg);
    }
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function DraftPertek(Request $request)
    {   
		if ($request->input('submit') == 'Draft Pertek') {
			$daftar_peserta = $request->input('peserta');
			$daftar_peserta_int = $request->input('peserta_int');
			
			$peserta_jadwal = $request->input('peserta_jadwal');
			$peserta_jadwal_int = $request->input('peserta_jadwal_int');

			$no_pertek = $request->input('no_pertek');
			$tanggal_pertek = $request->input('tanggal_pertek');
			$inputinstansi = $request->input('instansi_input');
        
			$no_surat = $request->input('no_surat_usulan');
			$no_surat_int = $request->input('no_surat_usulan_int');         

			$data_instansi = DB::table('instansis')->where('id',$inputinstansi)->first();
			
			$pesertaLulus =  DB::table('instansis')
		 					 ->join('pesertas','pesertas.nama_instansi','=','instansis.id')
							 ->join('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta')
							 ->Join('surat_usulans', function($Join){
								$Join->on('surat_usulans.id_jadwal', '=', 'peserta_jadwals.id_jadwal')
								->on('surat_usulans.id_peserta', '=', 'peserta_jadwals.id_peserta');
							})
							->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
							->select('pesertas.*','peserta_jadwals.*','peserta_jadwals.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwals.tanggal_ujian as tanggal_ujian','peserta_jadwals.id_jadwal as id_jadwal','peserta_jadwals.metode_ujian as metode','peserta_jadwals.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_jadwals.id as ids','peserta_jadwals.status_ujian as statuss','instansis.nama as instansi','instansis.jenis as jenis')
							->where('pesertas.deleted_at', null)
							->where('pesertas.status_peserta','aktif')
							->whereIn('peserta_jadwals.status_ujian', ['lulus'])
							->where('peserta_jadwals.publish','publish')
							->where('instansis.id',$inputinstansi)
							->where(function ($query) use ($no_surat) {
								if ($no_surat != "") {
									foreach($no_surat as $key => $value){
										$lists[] = $value;
									}
									
									$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
								} else {
									$query->whereIn('surat_usulans.no_surat_usulan_peserta', []);
								}
							})
							->where(function ($query) use ($peserta_jadwal) {
								if ($peserta_jadwal != "") {
									foreach($peserta_jadwal as $key => $value){
										$lists[] = $value;
									}
									$query->whereIn('peserta_jadwals.id', $lists);
								} else {
									$query->whereIn('peserta_jadwals.id', []);
								}
							})
							->where(function ($query) use ($daftar_peserta) {
								if ($daftar_peserta != "") {
									foreach($daftar_peserta as $key => $value){
										$lists[] = $value;
									}
									
									$query->whereIn('surat_usulans.id_peserta', $lists);
								} else {
									$query->whereIn('surat_usulans.id_peserta', []);   
								}
							})
							->groupby('surat_usulans.id')
							->get();
							
							$pesertaIntLulus =  DB::table('instansis')
												->join('pesertas','pesertas.nama_instansi','=','instansis.id')
												->join('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta')
												->Join('surat_usulans', function($Join){
													$Join->on('surat_usulans.id_jadwal', '=', 'peserta_instansis.id_jadwal')
													->on('surat_usulans.id_peserta', '=', 'peserta_instansis.id_peserta');
												})
												->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id')
												->select('pesertas.*','peserta_instansis.*','peserta_instansis.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwal_instansis.tanggal_ujian as tanggal_ujian','peserta_instansis.id_jadwal as id_jadwal','peserta_instansis.metode_ujian as metode','peserta_instansis.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_instansis.id as ids','peserta_instansis.status_ujian as statuss','instansis.nama as instansi','instansis.jenis as jenis')
												->where('pesertas.deleted_at', null)
												->where('pesertas.status_peserta','aktif')
												->whereIn('peserta_instansis.status_ujian', ['lulus'])
												->where('peserta_instansis.publish','publish')
												->where('instansis.id',$inputinstansi)
												->where(function ($query) use ($no_surat_int) {
													if ($no_surat_int != "") {
														foreach($no_surat_int as $key => $value)
														{
															$lists[] = $value;
														}
														$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
													} else {
														$query->whereIn('surat_usulans.no_surat_usulan_peserta', []);
													}
												})
												->where(function ($query) use ($peserta_jadwal_int) {
													if ($peserta_jadwal_int != "") {
														foreach($peserta_jadwal_int as $key => $value){
															$lists[] = $value;
														}
														
														$query->whereIn('peserta_instansis.id', $lists);
													} else {
														$query->whereIn('peserta_instansis.id', []);
													}
												})
												->where(function ($query) use ($daftar_peserta_int) {
													if ($daftar_peserta_int != "") {
														foreach($daftar_peserta_int as $key => $value){
															$lists[] = $value;
														}
														$query->whereIn('surat_usulans.id_peserta', $lists);
													} else {
														$query->whereIn('surat_usulans.id_peserta', []);   
													}
												})
												->groupby('surat_usulans.id')
												->get();
							
							$pesertaTidakLulus =  DB::table('instansis')
												  ->join('pesertas','pesertas.nama_instansi','=','instansis.id')
												  ->join('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta')
												  ->Join('surat_usulans', function($Join){
													  $Join->on('surat_usulans.id_jadwal', '=', 'peserta_jadwals.id_jadwal')
													  ->on('surat_usulans.id_peserta', '=', 'peserta_jadwals.id_peserta');
													})
												  ->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
												  ->select('pesertas.*','peserta_jadwals.*','peserta_jadwals.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwals.tanggal_ujian as tanggal_ujian','peserta_jadwals.id_jadwal as id_jadwal','peserta_jadwals.metode_ujian as metode','peserta_jadwals.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_jadwals.id as ids','peserta_jadwals.status_ujian as statuss','instansis.nama as instansi','instansis.jenis as jenis')
												  ->where('pesertas.deleted_at', null)
												  ->where('pesertas.status_peserta','aktif')
												  ->whereIn('peserta_jadwals.status_ujian', ['tidak_lulus','tidak_hadir','tidak_lengkap'])
												  ->where('peserta_jadwals.publish','publish')
												  ->where('instansis.id',$inputinstansi)
												  ->where(function ($query) use ($no_surat) {
														if ($no_surat != "") {
															foreach($no_surat as $key => $value){
																$lists[] = $value;
															}
															$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
														} else {
															$query->whereIn('surat_usulans.no_surat_usulan_peserta', []);
														}
													})
													->where(function ($query) use ($peserta_jadwal) {
														if ($peserta_jadwal != "") {
															foreach($peserta_jadwal as $key => $value){
																$lists[] = $value;
															}
															$query->whereIn('peserta_jadwals.id', $lists);
														} else {
															$query->whereIn('peserta_jadwals.id', []);
														}
													})
													->where(function ($query) use ($daftar_peserta) {
														if ($daftar_peserta != "") {
															foreach($daftar_peserta as $key => $value){
																$lists[] = $value;
															}
														
															$query->whereIn('surat_usulans.id_peserta', $lists);
														} else {
															$query->whereIn('surat_usulans.id_peserta', []);   
														}
													})
													->groupby('surat_usulans.id')
													->get();

								$pesertaTidakIntLulus =  DB::table('instansis')
														 ->join('pesertas','pesertas.nama_instansi','=','instansis.id')
														 ->join('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta')
														 ->Join('surat_usulans', function($Join){
															 $Join->on('surat_usulans.id_jadwal', '=', 'peserta_instansis.id_jadwal')
															 ->on('surat_usulans.id_peserta', '=', 'peserta_instansis.id_peserta');
														 })
														->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id')
														->select('pesertas.*','peserta_instansis.*','peserta_instansis.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwal_instansis.tanggal_ujian as tanggal_ujian','peserta_instansis.id_jadwal as id_jadwal','peserta_instansis.metode_ujian as metode','peserta_instansis.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_instansis.id as ids','peserta_instansis.status_ujian as statuss','instansis.nama as instansi','instansis.jenis as jenis')
														->where('pesertas.deleted_at', null)
														->where('pesertas.status_peserta','aktif')
														->whereIn('peserta_instansis.status_ujian', ['tidak_lulus','tidak_hadir','tidak_lengkap'])
														->where('peserta_instansis.publish','publish')
														->where('instansis.id',$inputinstansi)
														->where(function ($query) use ($no_surat_int) {
															if ($no_surat_int != "") {
																foreach($no_surat_int as $key => $value){
																	$lists[] = $value;
																}
																
																$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
															} else {
																$query->whereIn('surat_usulans.no_surat_usulan_peserta', []);
															}
														})
														->where(function ($query) use ($peserta_jadwal_int) {
															if ($peserta_jadwal_int != "") {
																foreach ($peserta_jadwal_int as $key => $value) {
																	$lists[] = $value;
																}
																
																$query->whereIn('peserta_instansis.id', $lists);
															} else {
																$query->whereIn('peserta_instansis.id', []);
															}
														})
														->where(function ($query) use ($daftar_peserta_int) {
															if ($daftar_peserta_int != "") {
																foreach ($daftar_peserta_int as $key => $value) {
																	$lists[] = $value;
																}
																
																$query->whereIn('surat_usulans.id_peserta', $lists);
															} else {
																$query->whereIn('surat_usulans.id_peserta', []);   
															}
														})
														->groupby('surat_usulans.id')
														->get();
								$dt = Carbon::now();
        $namaBulan = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        $dt_tahun = $dt->year;
        $dt_bulan = $dt->month;
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $width = \PhpOffice\PhpWord\Shared\Converter::inchToTwip(8.27);
        $height = \PhpOffice\PhpWord\Shared\Converter::inchToTwip(11.69);
        $sectionStyle = array(
            'marginTop' => 600,
            'marginLeft' => 600,
            'marginRight' => 600,
            'pageSizeW' => $width,
            'pageSizeH' => $height 
        );
        $headerStyle = array(
            'marginTop' => 250,
            'marginLeft' => 600,
            'marginRight' => 600,
            'pageSizeW' => $width,
        );
        $phpWord->setDefaultFontSize(11);
        $phpWord->setDefaultParagraphStyle(
            array(
                'alignment'  => \PhpOffice\PhpWord\SimpleType\Jc::BOTH,
                'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(6),
            )
        );
        $section = $phpWord->addSection($sectionStyle);
        $sectionStyle = $section->getStyle();
        // half inch left margin
        $sectionStyle->setMarginLeft(\PhpOffice\PhpWord\Shared\Converter::inchToTwip(1.083));
        // 2 cm right margin
        $sectionStyle->setMarginRight(\PhpOffice\PhpWord\Shared\Converter::inchToTwip(.985));
        $header = $section->addHeader('first');
        $header->addImage(public_path('../assets/img/header_lkpp.png', array('marginLeft' => 0, 'marginRight' => 0)),
        array(
            'width'         => 450,
            'height'        => 60,
            'wrappingStyle' => 'behind'
        ));
        $tableStyle = array(
            'borderColor' => '000',
            'borderSize'  => 6,
            'width'  => 100
        );
        $tableStyle2 = array(
            'width'  => 100
        );
        $tableBorderTop = array(
            'borderTopColor' =>'000',
            'borderTopSize' => 6
        );
        $tableBorderRight = array(
            'borderRightColor' =>'000',
            'borderRightSize' => 6
        );
        $tableBorderLeft = array(
            'borderLeftColor' =>'000',
            'borderLeftSize' => 6
        );
        $tableBorderBottom = array(
            'borderBottomColor' =>'000',
            'borderBottomSize' => 6
        );
        $tablewhite = array(
            'bgColor' => 'ffffff'
        );

        $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center', 'bgColor' => 'FFFF00');
        $cellRowContinue = array('vMerge' => 'continue');
        $cellColSpan = array('gridSpan' => 2);
        $cellHCentered = array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER);
        $cellVCentered = array('valign' => 'center');
        $firstRowStyle = array('bgColor' => '808080');
        $phpWord->addTableStyle('myTable', $tableStyle2, $firstRowStyle);
        $table = $section->addTable('myTable');
        $table->addRow();
        // Add cells
        $table_1 = $table->addCell(1500, $tablewhite);
        $table_1->addText('Nomor');
        $table_1->addText('Lampiran');
        $table_1->addText('Hal');
        $table_2 = $table->addCell(500, $tablewhite);
        $table_2->addText(':');
        $table_2->addText(':');
        $table_2->addText(':');
        $table_3 = $table->addCell(5000, $tablewhite);
        $table_3->addText('           /D.3/'.$dt_bulan.'/'.$dt_tahun);
        $table_3->addText('Satu Berkas');
        $table_3->addText('Rekomendasi Pengangkatan dan Penyampaian Hasil Uji Kompetensi Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa',null,array('align' => 'left'));
        $table->addCell(1500,$tablewhite)->addText($namaBulan[$dt_bulan].' '.$dt_tahun);
        $text = $section->addText('');
        $text = $section->addText('');
        if ($data_instansi->jenis == "PROVINSI" ) {
            $text = $section->addText('Yth.<w:br/>Gubernur '.$data_instansi->nama,array('bold' => true));
        }
        if ($data_instansi->jenis == "KOTA" ) {
            $text = $section->addText('Yth.<w:br/>Wali Kota '.$data_instansi->nama,array('bold' => true));
        }
        if ($data_instansi->jenis == "KABUPATEN" ) {
            $text = $section->addText('Yth.<w:br/>Bupati '.$data_instansi->nama,array('bold' => true));
        }
        if ($data_instansi->jenis == "LEMBAGA" || $data_instansi->jenis == "KEMENTRIAN" ) {
            $text = $section->addText('Yth.<w:br/>'.$data_instansi->nama,array('bold' => true));
        }

        if ($tanggal_pertek != "") {
            $tgl_pertek = explode('/',$tanggal_pertek);
            $tanggal_pertek = $tgl_pertek[2].'-'.$tgl_pertek[0].' - '.$tgl_pertek[1];
        }
        else{
            $tanggal_pertek = date('Y-m-d');
        }

        if ($no_pertek != "") {
            $no_pertek;
        }
        else{
            $no_pertek = '-';
        }
        
        $text = $section->addText('di');
        $text = $section->addText('Tempat', null, array('keepNext' => true,'indentation' => array('firstLine' => 300)));
        $text = $section->addText('');
        $text = $section->addText('Dengan hormat,', null, array('keepNext' => true, 'indentation' => array('firstLine' => 360)));
		if ($data_instansi->jenis == "KABUPATEN" || $data_instansi->jenis == "KOTA" || $data_instansi->jenis == "PROVINSI") {
			$text = $section->addText('Menindaklanjuti Surat Permohonan Untuk Mengikuti Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa Nomor: '.$no_pertek.' tanggal '.Helper::tanggal_indo($tanggal_pertek).' dari Pj. Sekretaris Daerah '.$data_instansi->nama.' dan mengacu kepada Peraturan LKPP Nomor 4 Tahun 2019 tentang Petunjuk Pelaksanaan Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa sebagai ketentuan pelaksanaan Peraturan Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi (PermenPAN-RB) Nomor 42 Tahun 2018 tentang Pengangkatan Pegawai Negeri Sipil dalam Jabatan Fungsional melalui Penyesuaian/Inpassing, kami sampaikan hal-hal sebagai berikut: ', null, array('keepNext' => true, 'indentation' => array('firstLine' => 360)));
        }
		
		if($data_instansi->jenis == "KEMENTRIAN" || $data_instansi->jenis == "LEMBAGA"){
			$text = $section->addText('Menindaklanjuti Surat Permohonan Untuk Mengikuti Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa Nomor: '.$no_pertek.' tanggal '.Helper::tanggal_indo($tanggal_pertek).' dari Kepala Biro '.$data_instansi->nama.' dan mengacu kepada Peraturan LKPP Nomor 4 Tahun 2019 tentang Petunjuk Pelaksanaan Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa sebagai ketentuan pelaksanaan Peraturan Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi (PermenPAN-RB) Nomor 42 Tahun 2018 tentang Pengangkatan Pegawai Negeri Sipil dalam Jabatan Fungsional melalui Penyesuaian/Inpassing, kami sampaikan hal-hal sebagai berikut: ', null, array('keepNext' => true, 'indentation' => array('firstLine' => 360)));
        }
        
		$phpWord->addNumberingStyle(
            'multilevel',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720)
                )
            )
        );

        $jumlahlulus = count($pesertaLulus);
        $jumlahIntlulus = count($pesertaIntLulus);
        $totallulus = $jumlahlulus + $jumlahIntlulus;

        $jumlahtidaklulus = count($pesertaTidakLulus);
        $jumlahtidakIntlulus = count($pesertaTidakIntLulus);
        $totaltidaklulus = $jumlahtidaklulus + $jumlahtidakIntlulus;

        $totalpeserta = $totallulus + $totaltidaklulus ;
        $data_peserta = "";

        if (count($pesertaTidakLulus) == 0  && count($pesertaTidakIntLulus) == 0) {
            if ($pesertaLulus != "" ) {
                $jml_peserta_lulus = count($pesertaLulus);
                foreach ($pesertaLulus as $key => $value) {
					$data_peserta .= ' Sdr. '.$value->nama;

					if($jml_peserta_lulus - 1 != $key){
						$data_peserta .= ',';
					}
				}
            } else {
                $data_peserta = "";
            }
			
			if ($pesertaIntLulus != "" ) {
                $jml_peserta_int_lulus = count($pesertaIntLulus);
                foreach ($pesertaIntLulus as $key => $value) {
					$data_peserta .= 'Sdr. '.$value->nama;

					if($jml_peserta_int_lulus - 1 != $key){
						$data_peserta .= ', ';
					}
                }                
            } else {
                $data_peserta = "";
            }
			
			$section->addListItem('Berdasarkan hasil Uji Kompetensi Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa (JF PPBJ) yang dilaksanakan bagi '.$data_peserta.' pada tanggal 16 Juli 2019 dengan Verifikasi Portofolio, Sdr. Radityo Adi Prabowo dinyatakan lulus Uji Kompetensi Penyesuaian/Inpassing (Sertifikat Kelulusan terlampir).', 0, null, 'multilevel');
			$section->addListItem($data_peserta.' direkomendasikan untuk diangkat dalam JF PPBJ sepanjang terdapat formasi dengan Jenjang Jabatan dan Angka Kredit Kumulatif berdasarkan Lampiran Lembar Rekomendasi.', 0, null, 'multilevel');
			$section->addListItem('Apabila '.$data_peserta.' sampai dengan periode pengangkatan dalam JF PPBJ, mengalami Kenaikan Pangkat, Penyesuaian Pendidikan dan/atau Masa Kerja Tambahan, maka Angka Kredit Kumulatif yang tercantum dalam Lampiran Lembar Rekomendasi dapat disesuaikan dengan Lampiran II PermenPAN-RB Nomor 42 Tahun 2018 tentang Pengangkatan Pegawai Negeri Sipil dalam Jabatan Fungsional melalui Penyesuaian/Inpassing.', 0, null, 'multilevel');
			$section->addListItem('Kenaikan Pangkat yang dimaksud pada angka (3) adalah Kenaikan Pangkat setingkat lebih tinggi yang masih berada dalam Jenjang Jabatan yang sama pada saat mengikuti Uji Kompetensi Penyesuaian/Inpassing JF PPBJ. Apabila Pangkat baru yang diperoleh berada pada Jenjang Jabatan setingkat lebih tinggi dari Jenjang Jabatan pada saat mengikuti Uji Kompetensi Penyesuaian/Inpassing, maka yang bersangkutan harus mengikuti Uji Kompetensi Penyesuaian/Inpassing ulang sesuai dengan Jenjang Jabatan barunya.', 0, null, 'multilevel');
			$section->addListItem('Proses pengangkatan sebagaimana dimaksud pada angka (2) dan (3), dilakukan oleh Pejabat Pembina Kepegawaian berdasarkan Penetapan Kebutuhan per Jenjang Jabatan yang disahkan oleh Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi. ', 0, null, 'multilevel');
			$section->addListItem('Guna optimalisasi pelaksanaan pembinaan terhadap Pejabat Fungsional Pengelola Pengadaan Barang/Jasa, mohon agar tembusan Surat Keputusan Pengangkatan dalam JF PPBJ dapat segera disampaikan kepada Badan Kepegawaian Negara dan dilaporkan melalui Sistem Aplikasi Pelayanan Kepegawaian (SAPK) BKN serta kepada LKPP untuk diinput dalam database JF PPBJ.', 0, null, 'multilevel');
			$section->addListItem('Rekomendasi ini berlaku sampai dengan 6 April 2021.', 0, null, 'multilevel');     
		} else {
			$section->addListItem('Berdasarkan hasil Uji Kompetensi Penyesuaian/Inpassing JF PPBJ yang dilaksanakan bagi '.$totalpeserta.' orang PNS di lingkungan Pemerintah '.$data_instansi->nama.' pada tanggal 28 Agustus 2019 dengan Verifikasi Portofolio, '.$totallulus.' orang PNS dinyatakan lulus dan '.$totaltidaklulus.' orang PNS lainnya dinyatakan belum lulus Uji Kompetensi Penyesuaian/Inpassing (Sertifikat Kelulusan dan Surat Keterangan Hasil Uji Kompetensi terlampir).', 0, null, 'multilevel');
			$section->addListItem('Bagi '.$totallulus.' orang PNS yang lulus Uji Kompetensi Penyesuaian/Inpassing direkomendasikan untuk diangkat dalam JF PPBJ sepanjang terdapat formasi dengan Jenjang Jabatan dan Angka Kredit Kumulatif berdasarkan Lampiran Lembar Rekomendasi.', 0, null, 'multilevel');
			$section->addListItem('Apabila PNS pada angka (2) sampai dengan periode pengangkatan dalam JF PPBJ, mengalami Kenaikan Pangkat, Penyesuaian Pendidikan dan/atau Masa Kerja Tambahan, maka Angka Kredit Kumulatif yang tercantum dalam Lampiran Lembar Rekomendasi dapat disesuaikan dengan Lampiran II PermenPAN-RB Nomor 42 Tahun 2018 tentang Pengangkatan Pegawai Negeri Sipil dalam Jabatan Fungsional melalui Penyesuaian/Inpassing.', 0, null, 'multilevel');
			$section->addListItem('Kenaikan Pangkat yang dimaksud pada angka (3) adalah Kenaikan Pangkat setingkat lebih tinggi yang masih berada dalam Jenjang Jabatan yang sama pada saat mengikuti Uji Kompetensi Penyesuaian/Inpassing JF PPBJ. Apabila Pangkat baru yang diperoleh berada pada Jenjang Jabatan setingkat lebih tinggi dari Jenjang Jabatan pada saat mengikuti Uji Kompetensi Penyesuaian/Inpassing, maka yang bersangkutan harus mengikuti Uji Kompetensi Penyesuaian/Inpassing ulang sesuai dengan Jenjang Jabatan barunya.', 0, null, 'multilevel');
			$section->addListItem('Proses pengangkatan sebagaimana dimaksud pada angka (2) dan (3), dilakukan oleh Pejabat Pembina Kepegawaian berdasarkan Penetapan Kebutuhan per Jenjang Jabatan yang disahkan oleh Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi. ', 0, null, 'multilevel');
			$section->addListItem('Peserta yang belum lulus dapat mengikuti Uji Kompetensi Penyesuaian/Inpassing JF PPBJ ulang dengan cara didaftarkan ke jadwal Uji Kompetensi yang tertera pada alamat https://inpassing.lkpp.go.id. Adapun ketentuan Uji Kompetensi ulang ialah:', 0, null, 'multilevel');

			$section->addListItem('Apabila mengikuti Uji Kompetensi ulang dengan metode Verifikasi Portofolio, peserta dapat didaftarkan paling cepat 2 (dua) bulan setelah hasil Uji Kompetensi dengan Verifikasi Portofolio/Tes Tertulis terakhir; atau', 1, null,'multilevel');
			$section->addListItem('Apabila mengikuti Uji Kompetensi ulang dengan metode Tes Tertulis, peserta dapat langsung segera setelah hasil Uji Kompetensi dengan Verifikasi Portofolio/Tes Tertulis terakhir.', 1, null,'multilevel');

			$section->addListItem('Guna optimalisasi pelaksanaan pembinaan terhadap Pejabat Fungsional Pengelola Pengadaan Barang/Jasa, mohon agar tembusan Surat Keputusan Pengangkatan dalam JF PPBJ dapat segera disampaikan kepada Badan Kepegawaian Negara dan dilaporkan melalui Sistem Aplikasi Pelayanan Kepegawaian (SAPK) BKN serta kepada LKPP untuk diinput dalam database JF PPBJ.', 0, null, 'multilevel');
			$section->addListItem('Rekomendasi ini berlaku sampai dengan 6 April 2021.', 0, null, 'multilevel');     
        }
        
        $text = $section->addText('Atas perhatian dan kerja sama Bapak, kami ucapkan terima kasih.', null, array('keepNext' => true, 'indentation' => array('firstLine' => 340)));
        $text = $section->addText('');
        $phpWord->addNumberingStyle(
            'multilevel2',
            array(
                'type' => 'multilevel',
                'levels' => array(
                    array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                    array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
                )
            )
        );
        
		$phpWord->addTableStyle('myTable2', $tableStyle2, $firstRowStyle);
        $table2 = $section->addTable('myTable2');
        $table2->addRow();
        $table2_1 = $table2->addCell(5500, $tablewhite);
        $table2_1->addText('');
        $table2_1->addText('');
        $table2_1->addText('');
        $table2_1->addText('');
        $table2_1->addText('');
        $table2_2 = $table2->addCell(4000, $tablewhite);
        $table2_2->addText('Deputi Bidang Pengembangan dan Pembinaan Sumber Daya Manusia', array('bold' => true));
        $table2_2->addText('');
        $table2_2->addText('');
        $table2_2->addText('Robin Asad Suryo', array('bold' => true));
        $table2->addRow();
        $table2_3 = $table2->addCell(9500, ['gridSpan' => 2, 'bgColor' => 'ffffff']);
        $table2_3->addText('');
        $table2_3->addText('Tembusan :');
        if ($data_instansi->jenis == "KABUPATEN" || $data_instansi->jenis == "KOTA" || $data_instansi->jenis == "PROVINSI") {
            $table2_3->addListItem('Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi.', 0, null, 'multilevel2');
            $table2_3->addListItem('Kepala Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah.', 0, null, 'multilevel2');
            $table2_3->addListItem('Sekretaris Daerah '.$data_instansi->nama.'.', 0, null, 'multilevel2');
            $table2_3->addListItem('Kepala Kantor Regional IV BKN Makassar.', 0, null, 'multilevel2');
        }
        
		if ($data_instansi->jenis == "KEMENTRIAN" || $data_instansi->jenis == "LEMBAGA") {
            $table2_3->addListItem('Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi.', 0, null, 'multilevel2');
            $table2_3->addListItem('Kepala Badan Kepegawaian Negara.', 0, null, 'multilevel2');
            $table2_3->addListItem('Kepala Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah.', 0, null, 'multilevel2');
            $table2_3->addListItem('Kepala Biro '.$data_instansi->nama.'.', 0, null, 'multilevel2');
        }
        
        //ganti halaman
        $section->addPageBreak();
        $text = $section->addText('');
        $text = $section->addText('');
        $text = $section->addText('Lampiran I');
        $text = $section->addText('Nomor        :       /D.3/'.$dt_bulan.'/'.$dt_tahun);
        $text = $section->addText('Tanggal      :       '.$namaBulan[$dt_bulan].' '.$dt_tahun);
        $text = $section->addText('');
        $text = $section->addText('');
        $text = $section->addText('REKOMENDASI PENGANGKATAN DALAM JABATAN FUNGSIONAL PENGELOLA PENGADAAN BARANG/JASA MELALUI MEKANISME PENYESUAIAN/INPASSING DI LINGKUNGAN PEMERINTAH  '.strtoupper($data_instansi->nama),array('bold' => true, 'size'=>11), array('align'=>'center', 'spaceAfter'=>100));

        $table3header = array('bgColor' => '000000', 'valign'  => 'center');
        $table3headertext = array('bold' => true, 'color'  => 'ffffff');
        $phpWord->addTableStyle('myTable3', $tableStyle, $firstRowStyle);
        $no = 1;
        if ($pesertaLulus != "" || $pesertaIntLulus != "") {
			$table3 = $section->addTable('myTable3');
			$table3->addRow();
			$table3->addCell(700, $table3header)->addText('No', $table3headertext, array('align' => 'center'));
			$table3->addCell(2000, $table3header)->addText('Nama', $table3headertext, array('align' => 'center'));
			$table3->addCell(2000, $table3header)->addText('NIP', $table3headertext, array('align' => 'center'));
			$table3->addCell(2000, $table3header)->addText('Pangkat/Gol. Ruang', $table3headertext, array('align' => 'center'));
			$table3->addCell(1300, $table3header)->addText('Jenjang Jabatan', $table3headertext, array('align' => 'center'));
			$table3->addCell(1500, $table3header)->addText('Angka Kredit Kumulatif', $table3headertext, array('align' => 'center'));
        
            foreach ($pesertaLulus as $pesertas) {
                $table3->addRow();
                $table3->addCell()->addText($no++, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->nama, null, array('align' => 'left'));
                $table3->addCell()->addText($pesertas->nip, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->jabatan, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->jenjang, null, array('align' => 'center'));
                if ($pesertas->statuss == 'lulus') {
                    $table3->addCell()->addText($pesertas->ak_kumulatif, null, array('align' => 'center'));
                }                
            }
                 
            foreach ($pesertaIntLulus as $pesertas) {
                $table3->addRow();
                $table3->addCell()->addText($no++, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->nama, null, array('align' => 'left'));
                $table3->addCell()->addText($pesertas->nip, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->jabatan, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->jenjang, null, array('align' => 'center'));
                if ($pesertas->statuss == 'lulus') {
                    $table3->addCell()->addText($pesertas->ak_kumulatif, null, array('align' => 'center'));
                }
            }
        }
        
        if (count($pesertaTidakLulus) == 0  && count($pesertaTidakIntLulus) == 0) {

        } else {
			$section->addPageBreak();
            $text = $section->addText('');
            $text = $section->addText('');
            $text = $section->addText('Lampiran II');
            $text = $section->addText('Nomor        :       /D.3/'.$dt_bulan.'/'.$dt_tahun);
            $text = $section->addText('Tanggal      :       '.$namaBulan[$dt_bulan].' '.$dt_tahun);
            $text = $section->addText('');
            $text = $section->addText('');
            $text = $section->addText('HASIL UJI KOMPETENSI PENYESUAIAN/INPASSING JABATAN FUNGSIONAL PENGELOLA PENGADAAN BARANG/JASA DI LINGKUNGAN PEMERINTAH '.strtoupper($data_instansi->nama),array('bold' => true, 'size'=>11), array('align'=>'center', 'spaceAfter'=>100));

            $table3header = array('bgColor' => '000000', 'valign'  => 'center');
			$table3headertext = array('bold' => true, 'color'  => 'ffffff');
			$phpWord->addTableStyle('myTable3', $tableStyle, $firstRowStyle);
			$no_lagi = 1;
        
			$table3 = $section->addTable('myTable3');
			$table3->addRow();
			$table3->addCell(700, $table3header)->addText('No', $table3headertext, array('align' => 'center'));
			$table3->addCell(2000, $table3header)->addText('Nama', $table3headertext, array('align' => 'center'));
			$table3->addCell(2000, $table3header)->addText('NIP', $table3headertext, array('align' => 'center'));
			$table3->addCell(2000, $table3header)->addText('Pangkat/Gol. Ruang', $table3headertext, array('align' => 'center'));
			$table3->addCell(1300, $table3header)->addText('Jenjang Jabatan', $table3headertext, array('align' => 'center'));
			$table3->addCell(1500, $table3header)->addText('Keterangan', $table3headertext, array('align' => 'center'));
			
            foreach ($pesertaTidakLulus as $pesertas) {
                $table3->addRow();
                $table3->addCell()->addText($no_lagi++, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->nama, null, array('align' => 'left'));
                $table3->addCell()->addText($pesertas->nip, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->jabatan, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->jenjang, null, array('align' => 'center'));
                if ($pesertas->statuss == 'tidak_lulus') {
                    $table3->addCell()->addText('Tidak Lulus', null, array('align' => 'center'));
                } elseif($pesertas->statuss == 'tidak_hadir') {
                    $table3->addCell()->addText('Tidak Hadir Tes Tertulis', null, array('align' => 'center'));
                } elseif($pesertas->statuss == 'tidak_lengkap') {
                    $table3->addCell()->addText('Dokumen Persyaratan Tidak Lengkap', null, array('align' => 'center'));
                }                
            }
        
            foreach ($pesertaTidakIntLulus as $pesertas) {
                $table3->addRow();
                $table3->addCell()->addText($no_lagi++, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->nama, null, array('align' => 'left'));
                $table3->addCell()->addText($pesertas->nip, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->jabatan, null, array('align' => 'center'));
                $table3->addCell()->addText($pesertas->jenjang, null, array('align' => 'center'));
                
				if ($pesertas->statuss == 'tidak_lulus') {
                    $table3->addCell()->addText('Tidak Lulus ', null, array('align' => 'center'));
                } elseif($pesertas->statuss == 'tidak_hadir') {
                    $table3->addCell()->addText('Tidak Hadir Tes Tertulis', null, array('align' => 'center'));
                } elseif($pesertas->statuss == 'tidak_lengkap') {
                    $table3->addCell()->addText('Dokumen Persyaratan Tidak Lengkap', null, array('align' => 'center'));
                }                
            }
		}
		
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save(storage_path('../storage/data/pertek/pertek.docx'));
		return response()->download(storage_path('../storage/data/pertek/pertek.docx'));
    } else {
        $instansi = $request->input('instansi_input');
        $no_surat_usulan = $request->input('no_surat_usulan');
        $no_surat_usulan_tidak = $request->input('no_surat_usulan_tidak');
        $daftar_peserta = $request->input('peserta');
        $tidak_daftar_peserta = $request->input('pesertatidak');
        $no_surat_usulan_int = $request->input('no_surat_usulan_int');
        $no_surat_usulan_tidak_int = $request->input('no_surat_usulan_tidak_int');
        $daftar_peserta_int = $request->input('peserta_int');
        $tidak_daftar_peserta_int = $request->input('pesertatidak_int');
        $status_pertek = $request->input('status_pertek');
        $no_pertek = $request->input('no_pertek');
        $tanggal_pertek = $request->input('tanggal_pertek');
        $id_jadwal = $request->input('id_jadwal');
        $id_pertek = $request->input('id_pertek');
        $draft = $request->input('draft');
        $eformasi = $request->input('id_formasi');
        $idformasi = "";

        if ($tidak_daftar_peserta != "") {
            foreach( $no_surat_usulan_tidak as $index => $nosurat ) {
				$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta[$index])->first();
				if ($statususulan) {
					$statususulan->status = 0;
					$statususulan->save();
				} else {}
				
				$pesertaPertek = Pesertapertek::where('nomor_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta[$index])->delete();
			}
        }

        if ($tidak_daftar_peserta_int != "") {
            foreach( $no_surat_usulan_tidak_int as $index => $nosurat ) {
				$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta_int[$index])->first();
				if ($statususulan) {
					$statususulan->status = 0;
					$statususulan->save();
				} else{}
				$pesertaPertek = Pesertapertek::where('nomor_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta_int[$index])->delete();
			}
        }
		
		$pertek = Pertek::find($id_pertek);
		if ($instansi !="") {
			$pertek->instansi = $instansi;
		}
		
		if ($status_pertek !="") {
			$pertek->status_pertek = $status_pertek;
		}
		
		if ($draft !="") {
			$pertek->draft = $draft;
		}
		
		if ($no_pertek != "") {
			$pertek->no_pertek = $no_pertek;
		}
		
		if ($tanggal_pertek != "") {
			$tgl_pertek = explode('/',$tanggal_pertek);
			$pertek->tanggal_pertek = $tgl_pertek[2].'-'.$tgl_pertek[0].'-'.$tgl_pertek[1];
		}
		
		$savepertek = $pertek->save();
		$nopertek = $pertek->id;
		$pesertapertek   = Pesertapertek::Where('id_pertek',$id_pertek);
		
		if ($pesertapertek) {
			$pesertapertek->delete();
		}

        if ($daftar_peserta != "") {
            if ($no_surat_usulan != "") {
				foreach( $no_surat_usulan as $index => $nosurat ) {
					$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$daftar_peserta[$index])->first();
					$statususulan->status = 1;
					$statususulan->save();
					$pesertaPertek = new Pesertapertek();
					$pesertaPertek->id_pertek = $nopertek;
					$pesertaPertek->id_peserta = $daftar_peserta[$index];
					if ($no_pertek != "") {
						$pesertaPertek->no_pertek = $no_pertek;
					}
					
					$pesertaPertek->nomor_surat_usulan_peserta = $nosurat;
					$savepeserta = $pesertaPertek->save();                
                }

                if ($savepertek && $savepeserta) {
                    $msg = "berhasil";
                } else {
                    $msg = "gagal"; 
                }             
            }
        }
		
        if ($daftar_peserta_int != "") {
            if ($no_surat_usulan_int != "") {
                foreach( $no_surat_usulan_int as $index => $nosurat ) {
					$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$daftar_peserta_int[$index])->first();
					$statususulan->status = 1;
					$statususulan->save();
					$pesertaPertek = new Pesertapertek();
					$pesertaPertek->id_pertek = $nopertek;
					$pesertaPertek->id_peserta = $daftar_peserta_int[$index];
					
					if ($no_pertek != "") {
						$pesertaPertek->no_pertek = $no_pertek;
					}
					
					$pesertaPertek->nomor_surat_usulan_peserta = $nosurat;
					$savepeserta = $pesertaPertek->save();                
                }

                if ($savepertek && $savepeserta) {
                    $msg = "berhasil";
                } else {
                    $msg = "gagal"; 
                }            
            }
        }

        $msg = "berhasil";
        return Redirect::to('pertek-sphu')->with('msg',$msg)->with('bagian','pertek');
    }
}
	public function DraftSPHU(Request $request){
		if ($request->input('submit') == 'Draft SPHU') {
			$daftar_peserta = $request->input('peserta');
			$daftar_peserta_int = $request->input('peserta_int');
			$peserta_jadwal = $request->input('peserta_jadwal');
			$peserta_jadwal_int = $request->input('peserta_jadwal_int');
			$no_sphu = $request->input('no_sphu');
			$tanggal_sphu = $request->input('tanggal_sphu');
			$inputinstansi = $request->input('instansi_input');        
			$no_surat = $request->input('no_surat_usulan');
			$no_surat_int = $request->input('no_surat_usulan_int');
			$data_instansi = DB::table('instansis')->where('id',$inputinstansi)->first();
			$pesertaLulus =  DB::table('instansis')
							 ->join('pesertas','pesertas.nama_instansi','=','instansis.id')
							 ->join('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta')
							 ->Join('surat_usulans', function($Join){
								 $Join->on('surat_usulans.id_jadwal', '=', 'peserta_jadwals.id_jadwal')
								 ->on('surat_usulans.id_peserta', '=', 'peserta_jadwals.id_peserta');
							 })
							 ->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
							 ->select('pesertas.*','peserta_jadwals.*','peserta_jadwals.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwals.tanggal_ujian as tanggal_ujian','peserta_jadwals.id_jadwal as id_jadwal','peserta_jadwals.metode_ujian as metode','peserta_jadwals.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_jadwals.id as ids','peserta_jadwals.status_ujian as statuss')
							 ->where('pesertas.deleted_at', null)
							 ->where('pesertas.status_peserta','aktif')
							 ->whereIn('peserta_jadwals.status_ujian', ['lulus'])
							 ->where('peserta_jadwals.publish','publish')
							 ->where('instansis.id',$inputinstansi)
							 ->where(function ($query) use ($no_surat) {
								if ($no_surat != "") {
									foreach($no_surat as $key => $value){
										$lists[] = $value;
									}
									$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
								} else {
									
								}
							})
							->where(function ($query) use ($daftar_peserta) {
								if ($daftar_peserta != "") {
									foreach($daftar_peserta as $key => $value){
										$lists[] = $value;
									}
									
									$query->whereIn('surat_usulans.id_peserta', $lists);
								} else {
									
								}
							})
							->groupby('surat_usulans.id')
							->get();

			$pesertaIntLulus =  DB::table('instansis')
								->join('pesertas','pesertas.nama_instansi','=','instansis.id')
								->join('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta')
								->Join('surat_usulans', function($Join){
									$Join->on('surat_usulans.id_jadwal', '=', 'peserta_instansis.id_jadwal')
									->on('surat_usulans.id_peserta', '=', 'peserta_instansis.id_peserta');
								})
								->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id')
								->select('pesertas.*','peserta_instansis.*','peserta_instansis.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwal_instansis.tanggal_ujian as tanggal_ujian','peserta_instansis.id_jadwal as id_jadwal','peserta_instansis.metode_ujian as metode','peserta_instansis.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_instansis.id as ids','peserta_instansis.status_ujian as statuss')
								->where('pesertas.deleted_at', null)
								->where('pesertas.status_peserta','aktif')
								->whereIn('peserta_instansis.status_ujian', ['lulus'])
								->where('peserta_instansis.publish','publish')
								->where('instansis.id',$inputinstansi)
								->where(function ($query) use ($no_surat_int) {
									if ($no_surat_int != "") {
										foreach ($no_surat_int as $key => $value) {
											$lists[] = $value;
										}
										
										$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
									} else {

									}
								})
								->where(function ($query) use ($daftar_peserta_int) {
									if ($daftar_peserta_int != "") {
										foreach ($daftar_peserta_int as $key => $value) {
											$lists[] = $value;
										}
										
										$query->whereIn('surat_usulans.id_peserta', $lists);
									} else {
										
									}
								})
								->groupby('surat_usulans.id')
								->get();

			$pesertaTidakLulus =  DB::table('instansis')
								  ->join('pesertas','pesertas.nama_instansi','=','instansis.id')
								  ->join('peserta_jadwals','pesertas.id','=','peserta_jadwals.id_peserta')
								  ->Join('surat_usulans', function($Join){
									  $Join->on('surat_usulans.id_jadwal', '=', 'peserta_jadwals.id_jadwal')
									  ->on('surat_usulans.id_peserta', '=', 'peserta_jadwals.id_peserta');
									})
									->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id')
									->select('pesertas.*','peserta_jadwals.*','peserta_jadwals.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwals.tanggal_ujian as tanggal_ujian','peserta_jadwals.id_jadwal as id_jadwal','peserta_jadwals.metode_ujian as metode','peserta_jadwals.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_jadwals.id as ids','peserta_jadwals.status_ujian as statuss')
									->where('pesertas.deleted_at', null)
									->where('pesertas.status_peserta','aktif')
									->whereIn('peserta_jadwals.status_ujian', ['tidak_lulus','tidak_hadir','tidak_lengkap'])
									->where('peserta_jadwals.publish','publish')
									->where('instansis.id',$inputinstansi)
									->where(function ($query) use ($no_surat) {
										if ($no_surat != "") {
											foreach($no_surat as $key => $value){
												$lists[] = $value;
											}
										
											$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
										} else {
											$query->whereIn('surat_usulans.no_surat_usulan_peserta', []);
										}
									})->where(function ($query) use ($peserta_jadwal) {
										if ($peserta_jadwal != "") {
											foreach($peserta_jadwal as $key => $value){
												$lists[] = $value;
											}										
											
											$query->whereIn('peserta_jadwals.id', $lists);
										} else {
											$query->whereIn('peserta_jadwals.id', []);
										}
									})->where(function ($query) use ($daftar_peserta) {
										if ($daftar_peserta != "") {
											foreach($daftar_peserta as $key => $value){
												$lists[] = $value;
											}
										
											$query->whereIn('surat_usulans.id_peserta', $lists);
										} else {
											$query->whereIn('surat_usulans.id_peserta', []);
										}
									})->groupby('surat_usulans.id')->get();
			
			$pesertaTidakIntLulus =  DB::table('instansis')
									 ->join('pesertas','pesertas.nama_instansi','=','instansis.id')
									 ->join('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta')
									 ->Join('surat_usulans', function($Join){
											$Join->on('surat_usulans.id_jadwal', '=', 'peserta_instansis.id_jadwal')
											->on('surat_usulans.id_peserta', '=', 'peserta_instansis.id_peserta');
									 })
									 ->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id')
									 ->select('pesertas.*','peserta_instansis.*','peserta_instansis.status_ujian as status','pesertas.id as id_peserta','instansis.id as nama_instansi','jadwal_instansis.tanggal_ujian as tanggal_ujian','peserta_instansis.id_jadwal as id_jadwal','peserta_instansis.metode_ujian as metode','peserta_instansis.ak_kumulatif as ak','surat_usulans.no_surat_usulan_peserta','peserta_instansis.id as ids','peserta_instansis.status_ujian as statuss')
									 ->where('pesertas.deleted_at', null)
									 ->where('pesertas.status_peserta','aktif')
									 ->whereIn('peserta_instansis.status_ujian', ['tidak_lulus','tidak_hadir','tidak_lengkap'])
									 ->where('peserta_instansis.publish','publish')
									 ->where('instansis.id',$inputinstansi)
									 ->where(function ($query) use ($no_surat_int) {
										if ($no_surat_int != "") {
											foreach($no_surat_int as $key => $value){
												$lists[] = $value;
											}
											
											$query->whereIn('surat_usulans.no_surat_usulan_peserta', $lists);
										} else {
											$query->whereIn('surat_usulans.no_surat_usulan_peserta', []);
										}															
									})->where(function ($query) use ($peserta_jadwal_int) {
										if ($peserta_jadwal_int != "") {
											foreach($peserta_jadwal_int as $key => $value){
												$lists[] = $value;
											}
											
											$query->whereIn('peserta_instansis.id', $lists);
										} else {
											$query->whereIn('peserta_instansis.id', []);
										}
									})->where(function ($query) use ($daftar_peserta_int) {
										if ($daftar_peserta_int != "") {
											foreach($daftar_peserta_int as $key => $value){
												$lists[] = $value;
											}
											
											$query->whereIn('surat_usulans.id_peserta', $lists);
										} else {
											$query->whereIn('surat_usulans.id_peserta', []);
										}
									})->groupby('surat_usulans.id')->get();
										$dt = Carbon::now();
										$namaBulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
										$dt_tahun = $dt->year;
										$dt_bulan = $dt->month;
										$phpWord = new \PhpOffice\PhpWord\PhpWord();
										$width = \PhpOffice\PhpWord\Shared\Converter::inchToTwip(8.27);
										$height = \PhpOffice\PhpWord\Shared\Converter::inchToTwip(11.69);
										$sectionStyle = array('marginTop' => 600, 'marginLeft' => 600, 'marginRight' => 600, 'pageSizeW' => $width, 'pageSizeH' => $height);
										$headerStyle = array('marginTop' => 250, 'marginLeft' => 600, 'marginRight' => 600, 'pageSizeW' => $width);
										$phpWord->setDefaultFontSize(11);
										$phpWord->setDefaultParagraphStyle(array('alignment'  => \PhpOffice\PhpWord\SimpleType\Jc::BOTH,
																				 'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(6)));
										$section = $phpWord->addSection($sectionStyle);
										$sectionStyle = $section->getStyle();
										
										// half inch left margin
										$sectionStyle->setMarginLeft(\PhpOffice\PhpWord\Shared\Converter::inchToTwip(1.083));
														
										// 2 cm right margin
										$sectionStyle->setMarginRight(\PhpOffice\PhpWord\Shared\Converter::inchToTwip(.985));
										$header = $section->addHeader('first');
										$header->addImage(public_path('../assets/img/header_lkpp.png', array('marginLeft' => 0, 'marginRight' => 0)),
												 array('width' => 450, 'height' => 60, 'wrappingStyle' => 'behind'));															  
										$tableStyle = array('borderColor' => '000', 'borderSize'  => 6, 'width'  => 100);
										$tableStyle2 = array('width' => 100);
										$tableBorderTop = array('borderTopColor' =>'000', 'borderTopSize' => 6);
										$tableBorderRight = array('borderRightColor' =>'000', 'borderRightSize' => 6);
										$tableBorderLeft = array('borderLeftColor' =>'000', 'borderLeftSize' => 6);
										$tableBorderBottom = array('borderBottomColor' =>'000', 'borderBottomSize' => 6);
										$tablewhite = array('bgColor' => 'ffffff');
										$cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center', 'bgColor' => 'FFFF00');
										$cellRowContinue = array('vMerge' => 'continue');
										$cellColSpan = array('gridSpan' => 2);
										$cellHCentered = array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER);
										$cellVCentered = array('valign' => 'center');
										$firstRowStyle = array('bgColor' => '808080');
										$phpWord->addTableStyle('myTable', $tableStyle2, $firstRowStyle);
										$table = $section->addTable('myTable');
										$table->addRow();
										
										// Add cells
										$table_1 = $table->addCell(1500, $tablewhite);
										$table_1->addText('Nomor');
										$table_1->addText('Lampiran');
										$table_1->addText('Hal');
										$table_2 = $table->addCell(500, $tablewhite);
										$table_2->addText(':');
										$table_2->addText(':');
										$table_2->addText(':');
										$table_3 = $table->addCell(5000, $tablewhite);
										$table_3->addText('           /D.3/'.$dt_bulan.'/'.$dt_tahun);
										$table_3->addText('Satu Berkas');
										$table_3->addText('Rekomendasi Pengangkatan dan Penyampaian Hasil Uji Kompetensi Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa',null,array('align' => 'left'));
										$table->addCell(1500,$tablewhite)->addText($namaBulan[$dt_bulan].' '.$dt_tahun);
										$text = $section->addText('');
										$text = $section->addText('');
										
										if ($data_instansi->jenis == "PROVINSI" ) {
											$text = $section->addText('Yth.<w:br/>Gubernur '.$data_instansi->nama,array('bold' => true));
										}
										
										if ($data_instansi->jenis == "KOTA" ) {
											$text = $section->addText('Yth.<w:br/>Wali Kota '.$data_instansi->nama,array('bold' => true));
										}
										
										if ($data_instansi->jenis == "KABUPATEN" ) {
											$text = $section->addText('Yth.<w:br/>Bupati '.$data_instansi->nama,array('bold' => true));
										}
										
										if ($data_instansi->jenis == "LEMBAGA" || $data_instansi->jenis == "KEMENTRIAN" ) {
											$text = $section->addText('Yth.<w:br/>'.$data_instansi->nama,array('bold' => true));
										}
										
										if ($tanggal_sphu != "") {
											$tgl_sphu = explode('/',$tanggal_sphu);
											$tanggal_sphu = $tgl_sphu[2].'-'.$tgl_sphu[0].' - '.$tgl_sphu[1];
										} else {
											$tanggal_sphu = date('Y-m-d');
										}
										
										if ($no_sphu != "") {
											$no_sphu;
										} else {
											$no_sphu = '-';
										}
										
										$text = $section->addText('di');
										$text = $section->addText('Tempat', null, array('keepNext' => true,'indentation' => array('firstLine' => 300)));
										$text = $section->addText('');
										$text = $section->addText('Dengan hormat,', null, array('keepNext' => true, 'indentation' => array('firstLine' => 360)));
										
										if ($data_instansi->jenis == "KABUPATEN" || $data_instansi->jenis == "KOTA" || $data_instansi->jenis == "PROVINSI") {
											$text = $section->addText('Menindaklanjuti Surat Permohonan Untuk Mengikuti Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa Nomor: '.$no_sphu.' tanggal '.Helper::tanggal_indo($tanggal_sphu).' dari Pj. Sekretaris Daerah '.$data_instansi->nama.' dan mengacu kepada Peraturan LKPP Nomor 4 Tahun 2019 tentang Petunjuk Pelaksanaan Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa sebagai ketentuan pelaksanaan Peraturan Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi (PermenPAN-RB) Nomor 42 Tahun 2018 tentang Pengangkatan Pegawai Negeri Sipil dalam Jabatan Fungsional melalui Penyesuaian/Inpassing, kami sampaikan hal-hal sebagai berikut: ', null, array('keepNext' => true, 'indentation' => array('firstLine' => 360)));
										}
										
										if($data_instansi->jenis == "KEMENTRIAN" || $data_instansi->jenis == "LEMBAGA"){
											$text = $section->addText('Menindaklanjuti Surat Permohonan Untuk Mengikuti Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa Nomor: '.$no_sphu.' tanggal '.Helper::tanggal_indo($tanggal_sphu).' dari Kepala Biro '.$data_instansi->nama.' dan mengacu kepada Peraturan LKPP Nomor 4 Tahun 2019 tentang Petunjuk Pelaksanaan Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa sebagai ketentuan pelaksanaan Peraturan Menteri Pendayagunaan Aparatur Negara dan Reformasi Birokrasi (PermenPAN-RB) Nomor 42 Tahun 2018 tentang Pengangkatan Pegawai Negeri Sipil dalam Jabatan Fungsional melalui Penyesuaian/Inpassing, kami sampaikan hal-hal sebagai berikut: ', null, array('keepNext' => true, 'indentation' => array('firstLine' => 360)));
										}
										
										$phpWord->addNumberingStyle('multilevel', array('type' => 'multilevel',
																						'levels' => array( array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
																						array('format' => 'lowerLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720))));
										
										$jumlahlulus = count($pesertaLulus);
										$jumlahIntlulus = count($pesertaIntLulus);
										$totallulus = $jumlahlulus + $jumlahIntlulus;
										$jumlahtidaklulus = count($pesertaTidakLulus);
										$jumlahtidakIntlulus = count($pesertaTidakIntLulus);
										$totaltidaklulus = $jumlahtidaklulus + $jumlahtidakIntlulus;
										$totalpeserta = $totallulus + $totaltidaklulus ;
										$section->addListItem('Berdasarkan hasil Uji Kompetensi Penyesuaian/Inpassing Jabatan Fungsional Pengelola Pengadaan Barang/Jasa (JF PPBJ) yang dilaksanakan bagi '.$totaltidaklulus.' orang PNS di lingkungan '.$data_instansi->nama.' pada tanggal 3 September 2019 dengan Tes Tertulis, peserta dinyatakan belum lulus Uji Kompetensi Penyesuaian/Inpassing  (Surat Keterangan Hasil Uji Kompetensi terlampir).', 0, null, 'multilevel');
										$section->addListItem('Peserta yang belum lulus dapat mengikuti Uji Kompetensi Penyesuaian/Inpassing JF PPBJ ulang dengan cara didaftarkan ke jadwal Uji Kompetensi yang tertera pada alamat https://inpassing.lkpp.go.id. Adapun ketentuan Uji Kompetensi ulang ialah:', 0, null, 'multilevel');
										$section->addListItem('Mengusulkan kembali Portofolio paling cepat 2 (dua) bulan setelah hasil Uji Kompetensi dengan metode Verifikasi Portofolio/Tes Tertulis terakhir; atau ', 1, null, 'multilevel');
										$section->addListItem('Mengikuti Tes Tertulis sesuai jadwal yang tersedia tanpa dibatasi jumlahnya.', 1, null, 'multilevel');
										$text = $section->addText('Atas perhatian dan kerja sama Bapak, kami ucapkan terima kasih.', null, array('keepNext' => true, 'indentation' => array('firstLine' => 360)));
										$text = $section->addText('');
										$phpWord->addNumberingStyle('multilevel2', array('type' => 'multilevel', 'levels' => array(array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
																																				   array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720))));
										$phpWord->addTableStyle('myTable2', $tableStyle2, $firstRowStyle);
										$table2 = $section->addTable('myTable2');
										$table2->addRow();
										$table2_1 = $table2->addCell(5500, $tablewhite);
										$table2_1->addText('');
										$table2_1->addText('');
										$table2_1->addText('');
										$table2_1->addText('');
										$table2_1->addText('');
										$table2_2 = $table2->addCell(4000, $tablewhite);
										$table2_2->addText('Direktur Pengembangan Profesi dan Kelembagaan', array('bold' => true));
										$table2_2->addText('');
										$table2_2->addText('');
										$table2_2->addText('');
										$table2_2->addText('');
										$table2_2->addText('Tatang Rustandar Wiraatmadja', array('bold' => true));
										$table2->addRow();
										$table2_3 = $table2->addCell(9500, ['gridSpan' => 2, 'bgColor' => 'ffffff']);
										$table2_3->addText('Tembusan :');
													
										if ($data_instansi->jenis == "KABUPATEN" ) {
											$table2_3->addListItem('Bupati '.$data_instansi->nama, 0, null, 'multilevel2');
											$table2_3->addListItem('Deputi Bidang Pengembangan dan Pembinaan Sumber Daya Manusia LKPP.', 0, null, 'multilevel2');
										}
														
										if ($data_instansi->jenis == "KOTA") {
											$table2_3->addListItem('Walikota '.$data_instansi->nama, 0, null, 'multilevel2');
											$table2_3->addListItem('Deputi Bidang Pengembangan dan Pembinaan Sumber Daya Manusia LKPP.', 0, null, 'multilevel2');
										}
														
										if ($data_instansi->jenis == "PROVINSI") {
											$table2_3->addListItem('Gubernur '.$data_instansi->nama, 0, null, 'multilevel2');
											$table2_3->addListItem('Deputi Bidang Pengembangan dan Pembinaan Sumber Daya Manusia LKPP.', 0, null, 'multilevel2');
										}
														
										if ($data_instansi->jenis == "KEMENTRIAN" || $data_instansi->jenis == "LEMBAGA") {
											$table2_3->addListItem('Sekretaris Jenderal '.$data_instansi->nama, 0, null, 'multilevel2');
											$table2_3->addListItem('Deputi Bidang Pengembangan dan Pembinaan Sumber Daya Manusia LKPP.', 0, null, 'multilevel2');            
										}
														
										$section->addPageBreak();														
										$text = $section->addText('Lampiran I');
										$text = $section->addText('Nomor        :       /D.3/'.$dt_bulan.'/'.$dt_tahun);
										$text = $section->addText('Tanggal      :       '.$namaBulan[$dt_bulan].' '.$dt_tahun);
										$text = $section->addText('');
										$text = $section->addText('');
										$text = $section->addText('HASIL UJI KOMPETENSI PENYESUAIAN/INPASSING JABATAN FUNGSIONAL PENGELOLA PENGADAAN BARANG/JASA DI LINGKUNGAN PEMERINTAH '.$data_instansi->nama,array('bold' => true, 'size'=>11), array('align'=>'center', 'spaceAfter'=>100));
										$table3header = array('bgColor' => '000000', 'valign'  => 'center');
										$table3headertext = array('bold' => true, 'color'  => 'ffffff');
										$phpWord->addTableStyle('myTable3', $tableStyle, $firstRowStyle);
										$table3 = $section->addTable('myTable3');
										$table3->addRow();
										$table3->addCell(700, $table3header)->addText('No', $table3headertext, array('align' => 'center'));
										$table3->addCell(2000, $table3header)->addText('Nama', $table3headertext, array('align' => 'center'));
										$table3->addCell(2000, $table3header)->addText('NIP', $table3headertext, array('align' => 'center'));
										$table3->addCell(2000, $table3header)->addText('Pangkat/Gol. Ruang', $table3headertext, array('align' => 'center'));
										$table3->addCell(1300, $table3header)->addText('Jenjang Jabatan', $table3headertext, array('align' => 'center'));
										$table3->addCell(1500, $table3header)->addText('Keterangan', $table3headertext, array('align' => 'center'));
										
										$no = 1;
										if ($pesertaTidakLulus != "") {
											foreach ($pesertaTidakLulus as $pesertas) {
												$table3->addRow();
												$table3->addCell()->addText($no++, null, array('align' => 'center'));
												$table3->addCell()->addText($pesertas->nama, null, array('align' => 'left'));
												$table3->addCell()->addText($pesertas->nip, null, array('align' => 'center'));
												$table3->addCell()->addText($pesertas->jabatan, null, array('align' => 'center'));
												$table3->addCell()->addText($pesertas->jenjang, null, array('align' => 'center'));
												if ($pesertas->statuss == 'tidak_lulus') {
													$table3->addCell()->addText('Belum Lulus Tes Tertulis', null, array('align' => 'center'));
												} elseif($pesertas->statuss == 'tidak_hadir') {
													$table3->addCell()->addText('Tidak Hadir Tes Tertulis', null, array('align' => 'center'));
												} elseif($pesertas->statuss == 'tidak_lengkap') {
													$table3->addCell()->addText('Dokumen Persyaratan Tidak Lengkap', null, array('align' => 'center'));
												}
											}
										}
										
										if ($pesertaTidakIntLulus != "") {
											foreach ($pesertaTidakIntLulus as $pesertas) {
												$table3->addRow();
												$table3->addCell()->addText($no++, null, array('align' => 'center'));
												$table3->addCell()->addText($pesertas->nama, null, array('align' => 'left'));
												$table3->addCell()->addText($pesertas->nip, null, array('align' => 'center'));
												$table3->addCell()->addText($pesertas->jabatan, null, array('align' => 'center'));
												$table3->addCell()->addText($pesertas->jenjang, null, array('align' => 'center'));
												if ($pesertas->statuss == 'tidak_lulus') {
													$table3->addCell()->addText('Belum Lulus Tes Tertulis', null, array('align' => 'center'));
												} elseif($pesertas->statuss == 'tidak_hadir') {
													$table3->addCell()->addText('Tidak Hadir Tes Tertulis', null, array('align' => 'center'));
												} elseif($pesertas->statuss == 'tidak_lengkap') {
													$table3->addCell()->addText('Dokumen Persyaratan Tidak Lengkap', null, array('align' => 'center'));
												}                
											}
										}
										
										$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
										$objWriter->save(storage_path('../storage/data/sphu/sphu.docx'));
										return response()->download(storage_path('../storage/data/sphu/sphu.docx'));
									} else {
										$instansi = $request->input('instansi_input');
										$no_surat_usulan = $request->input('no_surat_usulan');
										$no_surat_usulan_tidak = $request->input('no_surat_usulan_tidak');
										$daftar_peserta = $request->input('peserta');
										$tidak_daftar_peserta = $request->input('pesertatidak');

										$no_surat_usulan_int = $request->input('no_surat_usulan_int');
										$no_surat_usulan_tidak_int = $request->input('no_surat_usulan_tidak_int');
										$daftar_peserta_int = $request->input('peserta_int');
										$tidak_daftar_peserta_int = $request->input('pesertatidak_int');
        
										$status_sphu  = $request->input('status_sphu ');
										$no_sphu = $request->input('no_sphu');
										$tanggal_sphu = $request->input('tanggal_sphu');
        
										$id_sphu = $request->input('id_sphu');
										$draft = $request->input('draft');
										$eformasi = $request->input('id_formasi');
										$idformasi = "";

										if ($tidak_daftar_peserta != "") {
											foreach( $no_surat_usulan_tidak as $index => $nosurat ) {
												$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta[$index])->first();
												if ($statususulan) {
													$statususulan->status = 0;
													$statususulan->save();
												} else {
													
												}
												
												$pesertaSPHU = pesertaSphu::where('nomor_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta[$index])->delete();
											}
										}

										if ($tidak_daftar_peserta_int != "") {
											foreach( $no_surat_usulan_tidak_int as $index => $nosurat ) {
												$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta_int[$index])->first();
												if ($statususulan) {
													$statususulan->status = 0;
													$statususulan->save();
												} else {

												}
												
												$pesertaSPHU = Pesertasphu::where('nomor_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta_int[$index])->delete();
											}
										}

										$sphu = Sphu::find($id_sphu);
										if ($instansi !="") {
											$sphu->instansi = $instansi;
										}
										
										if ($status_sphu !="") {
											$sphu->status_sphu = $status_sphu;
										}
										
										if ($draft !="") {
											$sphu->draft = $draft;										
										}
										
										if ($no_sphu != "") {
											$sphu->no_sphu = $no_sphu;
										}

										if ($tanggal_sphu != "") {
											$tgl_sphu = explode('/',$tanggal_sphu);
											$sphu->tanggal_sphu = $tgl_sphu[2].'-'.$tgl_sphu[0].'-'.$tgl_sphu[1];
										}
										
										$savesphu = $sphu->save();
										$nosphu = $sphu->id;

										$pesertasphu   = Pesertasphu::Where('id_sphu',$id_sphu);
										if ($pesertasphu) {
											$pesertasphu->delete();
										}

										if ($daftar_peserta != "") {
											if ($no_surat_usulan != "") {
												foreach( $no_surat_usulan as $index => $nosurat ) {
													$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$daftar_peserta[$index])->first();
													$statususulan->status = 2;
													$statususulan->save();
													
													$pesertaSphu = new Pesertasphu();
													$pesertaSphu->id_sphu = $nosphu;
													$pesertaSphu->id_peserta = $daftar_peserta[$index];
													
													if ($no_sphu != "") {
														$pesertaSphu->no_sphu = $no_sphu;
													}
													
													$pesertaSphu->nomor_surat_usulan_peserta = $nosurat;
													$savepeserta = $pesertaSphu->save();                
												}

												if ($savesphu && $savepeserta) {
													$msg = "berhasil";
												} else {
													$msg = "gagal"; 
												}
											}
										}
										
										if ($daftar_peserta_int != "") {
											if ($no_surat_usulan_int != "") {                                    
												foreach( $no_surat_usulan_int as $index => $nosurat ) {
													$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$daftar_peserta_int[$index])->first();
													$statususulan->status = 2;
													$statususulan->save();
													$pesertaSphu = new Pesertasphu();
													$pesertaSphu->id_sphu = $nosphu;
													$pesertaSphu->id_peserta = $daftar_peserta_int[$index];
													
													if ($no_sphu != "") {
														$pesertaSphu->no_sphu = $no_sphu;
													}
													
													$pesertaSphu->nomor_surat_usulan_peserta = $nosurat;
													$savepeserta = $pesertaSphu->save();                
												}
												
												if ($savesphu && $savepeserta) {
													$msg = "berhasil";
												} else {
													$msg = "gagal"; 
												}             
											}
										}
										
										$msg = "berhasil";
										return Redirect::to('pertek-sphu')->with('msg',$msg)->with('bagian','sphu');
									}
        
    }




    public function storeSPHU(Request $request){
        $instansi = $request->input('instansi_input');
        $no_surat_usulan = $request->input('no_surat_usulan');
        $no_surat_usulan_tidak = $request->input('no_surat_usulan_tidak');
        $daftar_peserta = $request->input('peserta');
        $tidak_daftar_peserta = $request->input('pesertatidak');

        $no_surat_usulan_int = $request->input('no_surat_usulan_int');
        $no_surat_usulan_tidak_int = $request->input('no_surat_usulan_tidak_int');
        $daftar_peserta_int = $request->input('peserta_int');
        $tidak_daftar_peserta_int = $request->input('pesertatidak_int');

        $status_sphu = $request->input('status_sphu');
        $no_sphu = $request->input('no_sphu');
        $tanggal_sphu = $request->input('tanggal_sphu');
        
        $draft = $request->input('draft');
        $eformasi = $request->input('id_formasi');
        $idformasi = "";
		
		if ($tidak_daftar_peserta != "") {
			foreach( $no_surat_usulan_tidak as $index => $nosurat ) {
				$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta[$index])->first();
				if ($statususulan) {
					$statususulan->status = 0;
					$statususulan->save();
				} else{
					
				}
				$pesertaSphu = Pesertasphu::where('nomor_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta[$index])->delete();
			}
        }

        if ($tidak_daftar_peserta_int != "") {
			foreach ($no_surat_usulan_tidak_int as $index => $nosurat) {
				$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta_int[$index])->first();
				if ($statususulan) {
					$statususulan->status = 0;
					$statususulan->save();
				} else {
					
				}
				
				$pesertaSphu = Pesertasphu::where('nomor_surat_usulan_peserta',$nosurat)->where('id_peserta',$tidak_daftar_peserta_int[$index])->delete();
			}
        }
		
		$sphu = new sphu();
		if ($instansi !="") {
			$sphu->instansi = $instansi;
		}
		
		if ($status_sphu !="") {
			$sphu->status_sphu = $status_sphu;
		}
		
		if ($draft !="") {
			$sphu->draft = $draft;
		}
		
		if ($no_sphu != "") {
			$sphu->no_sphu = $no_sphu;
		}
		
		if ($tanggal_sphu != "") {
			$tgl_sphu = explode('/',$tanggal_sphu);
			$sphu->tanggal_sphu = $tgl_sphu[2].'-'.$tgl_sphu[0].'-'.$tgl_sphu[1];
		}
		
		$savesphu = $sphu->save();
		
		$nosphu = $sphu->id;
        if ($daftar_peserta != "") {
            if ($no_surat_usulan != "") {
                foreach( $no_surat_usulan as $index => $nosurat ) {
					$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$daftar_peserta[$index])->first();
					$statususulan->status = 2;
					$statususulan->save();
					
					$pesertasphu = new Pesertasphu();
					$pesertasphu->id_sphu = $nosphu;
					$pesertasphu->id_peserta = $daftar_peserta[$index];
					if ($no_sphu != "") {
						$pesertasphu->no_sphu = $no_sphu;
					}
					
					$pesertasphu->nomor_surat_usulan_peserta = $nosurat;
					$savepeserta = $pesertasphu->save();                
                }

                if ($savesphu && $savepeserta) {
                    $msg = "berhasil";
                } else {
                    $msg = "gagal"; 
                }             
            }
        }
		
		if ($daftar_peserta_int != "") {
			if ($no_surat_usulan_int != "") {
                foreach( $no_surat_usulan_int as $index => $nosurat ) {
					$statususulan = SuratUsulan::where('no_surat_usulan_peserta',$nosurat)->where('id_peserta',$daftar_peserta_int[$index])->first();
					$statususulan->status = 1;
					$statususulan->save();
					
					$pesertaSphu = new Pesertasphu();
					$pesertaSphu->id_sphu = $nosphu;
					$pesertaSphu->id_peserta = $daftar_peserta_int[$index];
					
					if ($no_sphu != "") {
						$pesertaSphu->no_sphu = $no_sphu;
					}
					
					$pesertaSphu->nomor_surat_usulan_peserta = $nosurat;
					$savepeserta = $pesertaSphu->save();                
                }
				
                if ($savepertek && $savepeserta) {
                    $msg = "berhasil";
                } else {
                    $msg = "gagal"; 
                }
            }
        }
      
        $msg = "berhasil";
        return Redirect::to('edit-sphu/'.$nosphu)->with('msg',$msg)->with('bagian','sphu');
    }


    public function storeAK(Request $request)
    {

    	$id_peserta_jadwal = $request->input('id_peserta_jadwal');
        $jenis = $request->input('jenis');

        if($jenis == 'regular') {
            $peserta_ak = PesertaJadwal::where('id',$id_peserta_jadwal)->first();
            if ($peserta_ak) {
                $peserta_ak->ak_kumulatif = $request->input('ak');
                if($peserta_ak->save()){
                    $msg = "berhasil";
                } else {
                    $msg = "gagal";
				}
            } else {
				$msg = "gagal";
			}
        }
		
        if($jenis == 'instansi') {
            $peserta_ak = PesertaInstansi::where('id',$id_peserta_jadwal)->first();
            if ($peserta_ak) {
                $peserta_ak->ak_kumulatif = $request->input('ak');
				if($peserta_ak->save()){
                    $msg = "berhasil";
                } else {
					$msg = "gagal";
				}
            } else {
				$msg = "gagal";
			}
        }
		
    	if($jenis == ''){
    		$msg = "gagal";
    	}
		
    	return Redirect::back()->with('msg',$msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPertek($id)
    {
    	if (Auth::user()->role == 'dsp' ){
    		return Redirect::back();
    	}

    	$data = Pertek::where('id',$id)->get();
    	$datapeserta = Pesertapertek::where('id_pertek',$id)->get();

    	foreach($data as $datas){
    		$deletedata = $datas->delete();
    	}
		
    	foreach($datapeserta as $datas){
    		$deletedatapeserta = $datas->delete();
    	}
    	
    	if ($deletedata && $deletedatapeserta) {
    		return Redirect::to('pertek-sphu')->with('msg','berhasil_hapus');
    	} else {
    		return Redirect::to('pertek-sphu')->with('msg','gagal_hapus');
    	}
    }
	
    public function destroySPHU($id)
    {
    	if (Auth::user()->role == 'dsp' ){
    		return Redirect::back();
    	}

    	$data = Sphu::where('id',$id)->get();
    	$datapeserta = Pesertasphu::where('id_sphu',$id)->get();

    	foreach($data as $datas){
    		$deletedata = $datas->delete();
    	}
    	
		foreach($datapeserta as $datas){
    		$deletedatapeserta = $datas->delete();
    	}
    
    	if ($deletedata && $deletedatapeserta) {
    		return Redirect::to('pertek-sphu')->with('msg','berhasil_hapus');
    	} else {
    		return Redirect::to('pertek-sphu')->with('msg','gagal_hapus');
    	}
    }

    public function uploadFile(Request $request,$id){
        if ($request->hasFile('file_pertek')) {
            $upload_file_portofolio = $request->file('file_pertek');
            $upload_file_portofolio_name = Carbon::now()->timestamp ."_".$id."_file_pertek".".".$upload_file_portofolio->getClientOriginalExtension();
            $upload_file_portofolioPath = 'storage/data/file_pertek';
            $upload_file_portofolio->move($upload_file_portofolioPath,$upload_file_portofolio_name);
        }

        if (file_exists(storage_path('data/file_pertek/'.$upload_file_portofolio_name))) {
            $data = Pertek::find($id);
            $data->file_pertek = $upload_file_portofolio_name;
            if($data->save()){
                return Redirect::to('edit-pertek/'.$id)->with('msg','berhasil');
            } else {
                return Redirect::to('edit-pertek/'.$id)->with('msg','gagal');
            }
        } else {
            return Redirect::to('edit-pertek/'.$id)->with('msg','gagal');
        }
    }

    public function uploadSphu(Request $request,$id){
        if ($request->hasFile('file_sphu')) {
            $upload_file_portofolio = $request->file('file_sphu');
            $upload_file_portofolio_name = Carbon::now()->timestamp ."_".$id."_file_sphu".".".$upload_file_portofolio->getClientOriginalExtension();
            $upload_file_portofolioPath = 'storage/data/file_sphu';
            $upload_file_portofolio->move($upload_file_portofolioPath,$upload_file_portofolio_name);

            if (file_exists(storage_path('data/file_sphu/'.$upload_file_portofolio_name))) {
                $data = Sphu::find($id);
                $data->file_sphu = $upload_file_portofolio_name;
                if($data->save()){
                    return Redirect::to('edit-sphu/'.$id)->with('msg','berhasil');
                } else {
                    return Redirect::to('edit-sphu/'.$id)->with('msg','gagal');
                }
            } else {
                return Redirect::to('edit-sphu/'.$id)->with('msg','gagal');
            }
        }
    }

    public function downloadFile($id){
        $pertek = Pertek::find($id);
        return response()->download(storage_path('data/file_pertek/'.$pertek->file_pertek));
    }

    public function downloadSphu($id){
        $sphu = Sphu::find($id);
        return response()->download(storage_path('data/file_sphu/'.$sphu->file_sphu));
    }
}