<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Instansi;
use Redirect;
use Validator;
use View;
use DB;

class InstansiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('instansis')->orderBy('id','desc')->get();
        return View::make('data_instansi', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'add';
        return View::make('tambah_instansi', compact('action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array('id' => 'required',
					   'prp_id' => 'required|numeric',
					   'kbp_id' => 'required|numeric',
					   'nama' => 'required',
					   'jenis' => 'required');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = new Instansi();
        $data->id = $request->input('id');
        $data->prp_id = $request->input('prp_id');
        $data->kbp_id = $request->input('kbp_id');
        $data->nama = $request->input('nama');
        $data->jenis = $request->input('jenis');
        $data->website = $request->input('website');
        
		if($data->save()){
            return Redirect::to('data-instansi')->with('msg','berhasil');
        } else {
            return Redirect::to('data-instansi')->with('msg','gagal');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Instansi::find($id);
        $action = 'edit';
        return View::make('tambah_instansi', compact('action','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array('prp_id' => 'required|numeric',
					   'kbp_id' => 'required|numeric',
					   'nama' => 'required');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = Instansi::find($id);
        $data->prp_id = $request->input('prp_id');
        $data->kbp_id = $request->input('kbp_id');
        $data->nama = $request->input('nama');
        
		if($request->input('jenis') != ""){
			$data->jenis = $request->input('jenis');
        }
        
		$data->website = $request->input('website');
        if($data->save()){
            return Redirect::to('data-instansi')->with('msg','berhasil');
        } else {
            return Redirect::to('data-instansi')->with('msg','gagal');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
	public function destroy($id)
    {
        $data = Instansi::find($id);
        if($data->delete()){
            return Redirect::to('data-instansi')->with('msg','berhasil_hapus');
        } else {
            return Redirect::to('data-instansi')->with('msg','gagal_hapus');
        }
    }
}