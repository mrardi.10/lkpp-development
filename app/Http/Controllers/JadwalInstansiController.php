<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Exports\PesertaInstansiExport;
use Maatwebsite\Excel\Facades\Excel;
use View;
use Helper;
use Validator;
use Redirect;
use App\JadwalInstansi;
use App\RiwayatUser;
use App\Dokumen;
use App\Peserta;
use App\PesertaInstansi;
use App\SuratUsulanInt;
use App\SuratUsulan;
use Mail;
use Carbon\Carbon;
use DB;
use PDF;

class JadwalInstansiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = Auth::user()->id;
        $bulan = $request->input('bulan');

        if($bulan == ''){
			$data = DB::table('jadwal_instansis')->leftJoin('peserta_instansis', function($leftJoin)
            {
                $leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
                ->on('peserta_instansis.status', '=', DB::raw("'1'"));
            })
            ->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
            ->select('jadwal_instansis.*', DB::raw("count(peserta_instansis.id) as jumlah_peserta"), 'users.nama_instansi as nama_instansis')
            ->groupBy('jadwal_instansis.id')
            ->orderBy('jadwal_instansis.tanggal_ujian','desc')
            ->where('jadwal_instansis.id_admin_ppk',$id_admin)
            ->get();
			
            $notifikasiRekomendasi = DB::table('riwayat_users')
                                ->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
                                ->join('users', 'riwayat_users.id_admin','=','users.id')
                                ->select('riwayat_users.*','pesertas.nama as nama_peserta')
                                ->whereIn('perihal',['hasil_verif_regular','hasil_verif_regular_dsp'])
                                ->where('id_admin',Auth::user()->id)    
                                ->orderBy('id','desc')
                                ->groupBy('riwayat_users.id')
                                ->offset(0)
                                ->limit(10)
                                ->get();
        } else {
            $data = DB::table('jadwal_instansis')->leftJoin('peserta_instansis', function($leftJoin){
						$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
						->on('peserta_instansis.status', '=', DB::raw("'1'"));
					})
					->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
					->select('jadwal_instansis.*', DB::raw("count(peserta_instansis.id) as jumlah_peserta"), 'users.nama_instansi as nama_instansis')
					->groupBy('jadwal_instansis.id')
					->orderBy('jadwal_instansis.tanggal_ujian','desc')
					->whereMonth('tanggal_ujian', '=', $bulan)
					->where('jadwal_instansis.id_admin_ppk',$id_admin)
					->get();

            $notifikasiRekomendasi = DB::table('riwayat_users')
                                ->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
                                ->join('users', 'riwayat_users.id_admin','=','users.id')
                                ->select('riwayat_users.*','pesertas.nama as nama_peserta')
                                ->whereIn('perihal',['hasil_verif_regular','hasil_verif_regular_dsp'])
                                ->where('id_admin',Auth::user()->id)    
                                ->orderBy('id','desc')
                                ->groupBy('riwayat_users.id')
                                ->offset(0)
                                ->limit(10)
                                ->get();
        }
		
        return View::make('daftar_instansi', compact('data','bulan','notifikasiRekomendasi'));
    }

    public function indexLkpp(Request $request)
    {
        $bulan = $request->input('bulan'); 
		
		if (Auth::user()->role == "asesor") {                    
			$pesertaAs = DB::table('pesertas')
						 ->leftJoin('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta')
						 ->where('pesertas.deleted_at',null)
						 ->where('pesertas.asesor',Auth::user()->id)
						 ->get();

        if (count($pesertaAs) == 0) {
            if ($bulan == "") {
                 $data = DB::table('jadwal_instansis')
						->leftJoin('peserta_instansis', function($leftJoin)
						{
							$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
							->on('peserta_instansis.status', '=', DB::raw("'1'"));
						})
						->join('pesertas', 'pesertas.id','=','peserta_instansis.id_peserta','left')
						->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
						->join('instansis','users.nama_instansi','=','instansis.id','left')
						->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"),'pesertas.*','jadwal_instansis.*')
						->where('jadwal_instansis.deleted_at',NULL)
						->whereIn('jadwal_instansis.id',[])  
						->groupBy('jadwal_instansis.id')
						->orderBy('jadwal_instansis.tanggal_ujian','desc')
						->get();
			} else {
				$data = DB::table('jadwal_instansis')
						->leftJoin('peserta_instansis', function($leftJoin)
						{
							$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
							->on('peserta_instansis.status', '=', DB::raw("'1'"));
						})
						->join('pesertas', 'pesertas.id','=','peserta_instansis.id_peserta','left')
						->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
						->join('instansis','users.nama_instansi','=','instansis.id','left')
						->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"),'pesertas.*','jadwal_instansis.*')
						->where('jadwal_instansis.deleted_at',NULL)  
						->whereIn('jadwal_instansis.id',[])  
						->groupBy('jadwal_instansis.id')
						->orderBy('jadwal_instansis.tanggal_ujian','desc')
						->whereMonth('tanggal_ujian', '=', $bulan)
						->get();
			}            
        } else {
            foreach ($pesertaAs as $pesertas) {
				$listss[] = $pesertas->id_jadwal;
            }
			
            if ($bulan == "") {
				$data = DB::table('jadwal_instansis')
						->leftJoin('peserta_instansis', function($leftJoin)
						{
							$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
							->on('peserta_instansis.status', '=', DB::raw("'1'"));
						})
						->join('pesertas', 'pesertas.id','=','peserta_instansis.id_peserta','left')
						->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
						->join('instansis','users.nama_instansi','=','instansis.id','left')
						->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"),'pesertas.*','jadwal_instansis.*')
						->where('jadwal_instansis.deleted_at',NULL)
						->whereIn('jadwal_instansis.id',$listss)  
						->groupBy('jadwal_instansis.id')
						->orderBy('jadwal_instansis.tanggal_ujian','desc')
						->get();
			} else {
				$data = DB::table('jadwal_instansis')
						->leftJoin('peserta_instansis', function($leftJoin)
						{
							$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
							->on('peserta_instansis.status', '=', DB::raw("'1'"));
						})
						->join('pesertas', 'pesertas.id','=','peserta_instansis.id_peserta','left')
						->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
						->join('instansis','users.nama_instansi','=','instansis.id','left')
						->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"),'pesertas.*','jadwal_instansis.*')
						->where('jadwal_instansis.deleted_at',NULL)  
						->whereIn('jadwal_instansis.id',$listss)  
						->groupBy('jadwal_instansis.id')
						->orderBy('jadwal_instansis.tanggal_ujian','desc')
						->whereMonth('tanggal_ujian', '=', $bulan)
						->get();
			}
        }
	}

    if (Auth::user()->role == "verifikator") {
			$pesertaAs = DB::table('pesertas')
						->leftJoin('peserta_instansis','pesertas.id','=','peserta_instansis.id_peserta')
						->where('pesertas.deleted_at',null)
						->where('pesertas.assign',Auth::user()->id)
						->get();
			
			if (count($pesertaAs) == 0) {
				if ($bulan == "") {
					 $data = DB::table('jadwal_instansis')
						   ->leftJoin('peserta_instansis', function($leftJoin){
								$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
								->on('peserta_instansis.status', '=', DB::raw("'1'"));
							})
							->join('pesertas', 'pesertas.id','=','peserta_instansis.id_peserta','left')
							->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
							->join('instansis','users.nama_instansi','=','instansis.id','left')
							->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"),'pesertas.*','jadwal_instansis.*')
							->where('jadwal_instansis.deleted_at',NULL)
							->whereIn('jadwal_instansis.id',[])  
							->groupBy('jadwal_instansis.id')
							->orderBy('jadwal_instansis.tanggal_ujian','desc')
							->get();
			} else {
				$data = DB::table('jadwal_instansis')
						->leftJoin('peserta_instansis', function($leftJoin){
							$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
							->on('peserta_instansis.status', '=', DB::raw("'1'"));
						})
						->join('pesertas', 'pesertas.id','=','peserta_instansis.id_peserta','left')
						->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
						->join('instansis','users.nama_instansi','=','instansis.id','left')
						->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"),'pesertas.*','jadwal_instansis.*')
						->where('jadwal_instansis.deleted_at',NULL)  
						->whereIn('jadwal_instansis.id',[])  
						->groupBy('jadwal_instansis.id')
						->orderBy('jadwal_instansis.tanggal_ujian','desc')
						->whereMonth('tanggal_ujian', '=', $bulan)
						->get();
			}
		} else {
			foreach ($pesertaAs as $pesertas) {
				$listss[] = $pesertas->id_jadwal;
			}
			
			if ($bulan == "") {
				$data = DB::table('jadwal_instansis')
						->leftJoin('peserta_instansis', function($leftJoin){
							$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
							->on('peserta_instansis.status', '=', DB::raw("'1'"));
						})
						->join('pesertas', 'pesertas.id','=','peserta_instansis.id_peserta','left')
						->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
						->join('instansis','users.nama_instansi','=','instansis.id','left')
						->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"),'pesertas.*','jadwal_instansis.*')
						->where('jadwal_instansis.deleted_at',NULL)
						->whereIn('jadwal_instansis.id',$listss)  
						->groupBy('jadwal_instansis.id')
						->orderBy('jadwal_instansis.tanggal_ujian','desc')
						->get();
			} else {
				$data = DB::table('jadwal_instansis')
						->leftJoin('peserta_instansis', function($leftJoin){
							$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
							->on('peserta_instansis.status', '=', DB::raw("'1'"));
						})
						->join('pesertas', 'pesertas.id','=','peserta_instansis.id_peserta','left')
						->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
						->join('instansis','users.nama_instansi','=','instansis.id','left')
						->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"),'pesertas.*','jadwal_instansis.*')
						->where('jadwal_instansis.deleted_at',NULL)  
						->whereIn('jadwal_instansis.id',$listss)  
						->groupBy('jadwal_instansis.id')
						->orderBy('jadwal_instansis.tanggal_ujian','desc')
						->whereMonth('tanggal_ujian', '=', $bulan)
						->get();
			}
		}
	}

    if (Auth::user()->role != "verifikator" && Auth::user()->role != "asesor") {
        if ($bulan == "") {
            $data = DB::table('jadwal_instansis')
					->leftJoin('peserta_instansis', function($leftJoin){
						$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
						->on('peserta_instansis.status', '=', DB::raw("'1'"));
					})
					->join('pesertas', 'pesertas.id','=','peserta_instansis.id_peserta','left')
					->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
					->join('instansis','users.nama_instansi','=','instansis.id','left')
					->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"),'pesertas.*','jadwal_instansis.*')
					->where('jadwal_instansis.deleted_at',NULL)
					->groupBy('jadwal_instansis.id')
					->orderBy('jadwal_instansis.tanggal_ujian','desc')
					->get();
		} else {
			$data = DB::table('jadwal_instansis')
					->leftJoin('peserta_instansis', function($leftJoin){
						$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
						->on('peserta_instansis.status', '=', DB::raw("'1'"));
					})
					->join('pesertas', 'pesertas.id','=','peserta_instansis.id_peserta','left')
					->join('users','jadwal_instansis.id_admin_ppk','=','users.id')
					->join('instansis','users.nama_instansi','=','instansis.id','left')
					->select('jadwal_instansis.*', 'instansis.nama as instansis', DB::raw("count(peserta_instansis.id) as jumlah_peserta"),'pesertas.*','jadwal_instansis.*')
					->where('jadwal_instansis.deleted_at',NULL)  
					->groupBy('jadwal_instansis.id')
					->orderBy('jadwal_instansis.tanggal_ujian','desc')
					->whereMonth('tanggal_ujian', '=', $bulan)
					->get();
		}
	}
	
	return View::make('daftar_jadwal_instansi', compact('data','bulan'));
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('tambah_permohonan_ujian');
    }

    public function addPeserta($id)
    {
		$jadwal = JadwalInstansi::find($id);
		$id_admin = Auth::user()->id;

		$peserta_jadwal = DB::table('peserta_instansis')
						  ->where('id_jadwal','!=',$id)
						  ->where('status_ujian','lulus')
						  ->where('status_ujian',NULL)
						  ->get();

        $peserta_jadwal_tdlulus = DB::table('peserta_instansis')
								  ->where('id_jadwal','!=',$id)
								  ->where('status_ujian','tidak_lulus')
								  ->where('publish','publish')
								  ->get();

		$peserta_jadwal_tdhadir = DB::table('peserta_instansis')
								  ->where('peserta_instansis.id_jadwal','!=',$id)
								  ->where('status_ujian','tidak_hadir')
								  ->where('publish','publish')
								  ->get();

         $peserta_jadwal_terdaftar_sama = DB::table('peserta_instansis')
   										  ->where('id_jadwal',$id)
										  ->where('status_ujian',NULL)
										  ->get();

        $peserta_jadwal_terdaftar = DB::table('peserta_instansis')
									->where('id_jadwal','!=',$id)
									->where('status_ujian',NULL)
									->get();
       
       $peserta_reguller = DB::table('peserta_jadwals')
							->where('id_admin_ppk',Auth::user()->id)
							->where('status_ujian','lulus')
							->where('status_ujian',NULL)
							->get();

        $peserta_reguller_terdaftar = DB::table('peserta_jadwals')
									   ->where('id_admin_ppk',Auth::user()->id)
									   ->where('status_ujian',NULL)
									   ->where('status',1)
									   ->get();

       $jml_pjs = count($peserta_jadwal);
       $id_data = "";

       foreach ($peserta_jadwal as $key => $pjs) {
			$id_data .= $pjs->id_peserta;
			
			if($key != $jml_pjs - 1){
				$id_data .= ",";
			}
			
			if($key == $jml_pjs - 1){
				$id_data .= "";
			}
		}

		$jml_pjs_sm = count($peserta_jadwal_terdaftar_sama);
        $id_data_sm = "";

        foreach ($peserta_jadwal_terdaftar_sama as $key => $pjs) {
            $id_data_sm .= $pjs->id_peserta;
            
			if($key != $jml_pjs_sm - 1){
                $id_data_sm .= ",";
            }
        
			if($key == $jml_pjs_sm - 1){
                $id_data_sm .= "";
            }
        }

		$jml_pjs_tl = count($peserta_jadwal_tdlulus);
        $id_data_tl = "";

        foreach ($peserta_jadwal_tdlulus as $key => $pjs) {
            $id_data_tl .= $pjs->id_peserta;
            if($key != $jml_pjs_tl - 1){
                $id_data_tl .= ",";
            }
        
			if($key == $jml_pjs_tl - 1){
                $id_data_tl .= "";
            }
        }

        $jml_pjs_th = count($peserta_jadwal_tdhadir);
        $id_data_th = "";

        foreach ($peserta_jadwal_tdhadir as $key => $pjs) {
            $id_data_th .= $pjs->id_peserta;
            if($key != $jml_pjs_th - 1){
                $id_data_th .= ",";
            }
            
			if($key == $jml_pjs_th - 1){
                $id_data_th .= "";
            }
        }

		$jml_pjs_tdf = count($peserta_jadwal_terdaftar);
		$id_data_tdf = "";
		
		foreach ($peserta_jadwal_terdaftar as $key => $pjs) {
			$id_data_tdf .= $pjs->id_peserta;
			if($key != $jml_pjs_tdf - 1){
				$id_data_tdf .= ",";
			}
			
			if($key == $jml_pjs_tdf - 1){
				$id_data_tdf .= "";
			}
		}

		$jml_pjs_rgl = count($peserta_reguller);
		$id_data_rgl = "";
		
		foreach ($peserta_reguller as $key => $pjs) {
			$id_data_rgl .= $pjs->id_peserta;
			if($key != $jml_pjs_rgl - 1){
				$id_data_rgl .= ",";
			}
			
			if($key == $jml_pjs_rgl - 1){
				$id_data_rgl .= "";
			}
		}

		$jml_pjs_rgl_tdf = count($peserta_reguller_terdaftar);
		$id_data_rgl_tdf = "";
		
		foreach ($peserta_reguller_terdaftar as $key => $pjs) {
			$id_data_rgl_tdf .= $pjs->id_peserta;
			if($key != $jml_pjs_rgl_tdf - 1){
				$id_data_rgl_tdf .= ",";
			}
			
			if($key == $jml_pjs_rgl_tdf - 1){
				$id_data_rgl_tdf .= "";
			}
		}

		$peserta_reguller = DB::table('peserta_jadwals')
							->where('id_admin_ppk',Auth::user()->id)
							->get('id_peserta')
							->toArray();

		$status_boleh = array(1,3);
		$ex_data = explode(",", $id_data);
		$ex_data_tl = explode(",", $id_data_tl);
		$ex_data_sm = explode(",", $id_data_sm);
		$ex_data_rgl = explode(",", $id_data_rgl);
		$th_data = explode(",", $id_data_th);
		
		$peserta = Peserta::where('id_admin_ppk',$id_admin)
					->where('deleted_at',null)
					->where('status_peserta','aktif')
					->whereIn('status_inpassing', $status_boleh)
					->whereNotIn('id', $ex_data)
					->whereNotIn('id', $ex_data_rgl)
					->whereNotIn('id', $ex_data_tl)
					->whereNotIn('id', $th_data)
					->get();

		$status_boleh_tl = array(7);
		$pesertaTidakLulus = Peserta::where('id_admin_ppk',$id_admin)
							->where('deleted_at',null)
							->where('status_peserta','aktif')
							->whereIn('status_inpassing', $status_boleh_tl)
							->whereIn('id', $ex_data_tl)
							->get();

		$status_boleh_th = array(11);
		$pesertaTidakHadir = Peserta::where('id_admin_ppk',$id_admin)
							->where('deleted_at',null)
							->where('status_peserta','aktif')
							->whereIn('status_inpassing', $status_boleh_th)
							->whereIn('id', $th_data)
							->get();

		$status_tdf = array(12,13);
		$ex_data_tdf = explode(",", $id_data_tdf);
		$ex_data_rgl_tdf = explode(",", $id_data_rgl_tdf);
		$pesertaTerdaftar = Peserta::where('id_admin_ppk',$id_admin)
							->where('deleted_at',null)
							->where('status_peserta','aktif')
							->whereIn('status_inpassing', $status_tdf)
							->whereNotIn('id', $ex_data_tdf)
							->whereNotIn('id', $ex_data_rgl_tdf)
							->get();

		$status_terdaftar_sm = array(12,13);
        $pesertaTerdaftar_sm = DB::table('pesertas')
								->where('id_admin_ppk', $id_admin)
								->where('deleted_at',null)
								->where('status_peserta','aktif')
								->whereIn('status_inpassing', $status_terdaftar_sm)
								->whereIn('id', $ex_data_sm)
								->get();

		$status_terdaftar_usul = array(16);
        $pesertaTerdaftar_usul = DB::table('pesertas')
								->where('id_admin_ppk', $id_admin)
								->where('deleted_at',null)
								->where('status_peserta','aktif')
								->whereIn('status_inpassing', $status_terdaftar_usul)
								->whereIn('id', $ex_data_sm)
								->get();

        $status_terdaftar_tl = array(2);
        $pesertaTerdaftar_tl = DB::table('pesertas')
								->where('id_admin_ppk', $id_admin)
								->where('deleted_at',null)
								->where('status_peserta','aktif')
								->whereIn('status_inpassing', $status_terdaftar_tl)
								->whereNotIn('id', $ex_data_tdf)
								->whereNotIn('id', $ex_data_rgl_tdf)
								->get();

		$status_tl = array(7);
        $pesertaTidakLulus = DB::table('pesertas')
							->where('id_admin_ppk', $id_admin)
							->where('deleted_at',null)
							->where('status_peserta','aktif')
							->whereIn('status_inpassing', $status_tl)
							->whereNotIn('id', $ex_data_tl)
							->get();

		$terdaftar = PesertaInstansi::where('id_admin_ppk',$id_admin);
		if($terdaftar->count() > 0){
			$data = $terdaftar->where('id_jadwal',$id)->get();
		} else {
			$data = "";
		}
    
		return View::make('tambah_peserta_instansi', compact('jadwal','peserta','pesertaTerdaftar','pesertaTidakLulus','pesertaTidakHadir','pesertaTerdaftar_sm','pesertaTerdaftar_tl','pesertaTerdaftar_usul','data'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
	public function store(Request $request)
    {   
       $metode = $request->input('metode_ujian');    
		
		$rules = array('tanggal_tes' => 'required',
					   'waktu_ujian' => 'required',
					   'lokasi_ujian' => 'required',
					   'jumlah_ruang' => 'required',
					   'kapasitas' => 'required',
					   'jumlah_unit' => 'required',
					   'nama_cp_1' => 'required',
					   'no_telp_cp_1' => 'required',
					   'nama_cp_2' => 'required',
					   'no_telp_cp_2' => 'required',
					   'surat_permohonan' => 'required|mimes:pdf,jpg,jpeg|min:100|max:2048');    
		
		$messages = ['surat_permohonan.required' => 'file surat permohonan harus di isi.',
					 'surat_permohonan.max' => ['file' => 'ukuran file surat permohonan tidak boleh lebih dari 2MB.'],
					 'surat_permohonan.min' => ['file' => 'ukuran file surat permohonan harus diatas 100kb.']
					];

		$validator = Validator::make(Input::all(), $rules, $messages);
		if ($validator->fails()) {
			return Redirect::to('permohonan-ujian-instansi')
					->withErrors($validator)
					->withInput();
		}

		$surat_permohonan = $request->file('surat_permohonan');
		$surat_permohonanName = Carbon::now()->timestamp ."_surat_permohonan".".". $surat_permohonan->getClientOriginalExtension();
		$surat_permohonanPath = 'storage/data/surat_permohonan';
		$surat_permohonan->move($surat_permohonanPath, $surat_permohonanName);
		
		$data = new JadwalInstansi();
		$data->metode = $metode;    

		if($request->input('tanggal_verifikasi') != "") {
			$tanggal_verifikasi = explode('/',$request->input('tanggal_verifikasi'));
			$data->tanggal_verifikasi = $tanggal_verifikasi[2].'-'.$tanggal_verifikasi[1].'-'.$tanggal_verifikasi[0];
			$data->tanggal_ujian = $tanggal_verifikasi[2].'-'.$tanggal_verifikasi[1].'-'.$tanggal_verifikasi[0];
		}
		
		$data->waktu_verifikasi = $request->input('waktu_verifikasi');
		
		if ($request->input('tanggal_tes') != "") {
			$tanggal_tes = explode('/',$request->input('tanggal_tes'));
			$data->tanggal_tes = $tanggal_tes[2].'-'.$tanggal_tes[1].'-'.$tanggal_tes[0];
			$data->tanggal_ujian = $tanggal_tes[2].'-'.$tanggal_tes[1].'-'.$tanggal_tes[0];
		}
		
		$data->waktu_ujian = $request->input('waktu_ujian');

		$data->id_admin_ppk = Auth::user()->id;        
		if($metode == "tes_tulis") {
			$data->lokasi_ujian = $request->input('lokasi_ujian');
			$data->jumlah_ruang = $request->input('jumlah_ruang');
			$data->jumlah_unit = $request->input('jumlah_unit');
		} else {
			$data->lokasi_ujian = '-';
			$data->jumlah_ruang = '-';
			$data->jumlah_unit = $request->input('kapasitas');
		}
        
		$data->kapasitas = $request->input('kapasitas');			
		$data->nama_cp_1 = $request->input('nama_cp_1');
		$data->telp_cp_1 = $request->input('no_telp_cp_1');
		$data->nama_cp_2 = $request->input('nama_cp_2');
		$data->telp_cp_2 = $request->input('no_telp_cp_2');

		$data->surat_permohonan = $surat_permohonanName;
		if($data->save()){
			return Redirect::to('daftar-instansi')->with('msg','berhasil');
		} else {
			return Redirect::to('daftar-instansi')->with('msg','gagal');
		}
	}

	public function storePeserta(Request $request, $id)
	{
		$id_peserta = $request->input('daftarkan');
		$data_tdk = $request->input('tidak_daftar');
		$id_admin = $request->input('id_admin_ppk');
		$metode = $request->input('metode');   

		if($id_peserta == ""){
			foreach($data_tdk as $id_pesertas){
				if($metode == 'tes_tulis' || $metode == 'verifikasi_portofolio'){
					$cek_peserta = PesertaInstansi::where('id_jadwal',$request->input('id_jadwal'))
									->where('id_peserta',$id_pesertas)->where('status',1)->get();
					if ($cek_peserta == "") {
						return Redirect::back()->with('msg','metode_kosong');
					}       
				}
			}
		}
		
		if($data_tdk != ""){
			foreach ($data_tdk as $id_pesertass) {
				$id_admin = $request->input('id_admin_ppk');
				$cek = PesertaInstansi::where('id_jadwal',$id)
						->where('id_admin_ppk',$id_admin)
						->where('id_peserta',$id_pesertass)
						->where('status',1)
						->delete();
				if($cek){
					$dok_usulan = SuratUsulan::where('id_peserta',$id_pesertass)->where('id_jadwal',$request->input('id_jadwal'))->where('jenis_ujian','instansi')->where('id_admin_ppk',Auth::user()->id)->first();
					
					if($dok_usulan) {
						$dok_usulan->id_jadwal = 0;
						$dok_usulan->jenis_ujian = null;
						$dok_usulan->save();
					}
					
					$id_status = 3;
					$status = "Dokumen Persyaratan Lengkap";
					$id_status_1 = 2;
					$status_1 = "Dokumen Persyaratan Tidak Lengkap";
					$peserta_sts = Peserta::find($id_pesertass);
					if ($peserta_sts->verifikasi_berkas == "not_verified") {
						$peserta_sts->status = $status_1;
						$peserta_sts->status_inpassing = $id_status_1;
						$peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));            
						$peserta_sts->save();
					} elseif ($peserta_sts->verifikasi_berkas == null ) {
						$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
                        $peserta_sts->status_inpassing = 1;
						$peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));           
						$peserta_sts->save();
                    } elseif ($peserta_sts->verifikasi_berkas == "verified") {
						$peserta_sts->status = $status;
						$peserta_sts->status_inpassing = $id_status;
						$peserta_sts->tanggal_ujian_terakhir = date('Y-m-d', strtotime($request->arriveDateTime));            
						$peserta_sts->save();
					}
					
					$riwayat = new RiwayatUser();
					$riwayat->id_user = $id_pesertass;
					$riwayat->id_admin = Auth::user()->id;
					$tanggal_ujian_rw = $request->input('tanggal_ujian');
					$riwayat->perihal = "jadwal_instansi";
					$riwayat->description = "sudah tidak terdaftar pada ujian dengan metode Tes Tertulis tanggal ".Helper::tanggal_indo($tanggal_ujian_rw)." oleh";
					$riwayat->tanggal = Carbon::now()->toDateString(); 
					$riwayat->save();
					$msg = "berhasil";
				}
			}
		}

		if($id_peserta != ""){        
			foreach($id_peserta as $datas){            
				$cek_peserta = PesertaInstansi::where('id_jadwal',$request->input('id_jadwal'))
								->where('id_peserta',$datas)
								->where('status',1)
								->count();
				
				$peserta = Peserta::find($datas);
				$jumlah = DB::table('jadwal_instansis')->leftJoin('peserta_instansis', function($leftJoin){
							$leftJoin->on('peserta_instansis.id_jadwal', '=', 'jadwal_instansis.id')
							->on('peserta_instansis.status', '=', DB::raw("'1'"));
						 })
						->select('jadwal_instansis.*', DB::raw("count(peserta_instansis.id) as jumlah_peserta_terdaftar"))
						->groupBy('jadwal_instansis.id')
						->where('status_permohonan','setuju')
						->where('jadwal_instansis.id',$id)
						->first(); 
				
				$id_admin = Auth::user()->id;
				$jumlah_peserta = $jumlah->jumlah_peserta_terdaftar;
				$maks = $jumlah->kapasitas;                 
				
				if($cek_peserta > 0){
					$peserta_jadwal = PesertaInstansi::where('id_jadwal',$request->input('id_jadwal'))
									  ->where('id_peserta',$datas)->where('status',1)->first();
					$pesertajadwal = PesertaInstansi::find($peserta_jadwal->id);
					$pesertajadwal->status = 1;
					if($pesertajadwal->save()){
						$dok_usulan = SuratUsulan::where('id_peserta',$datas)->where('id_jadwal',0)->where('jenis_ujian',null)->where('id_admin_ppk',Auth::user()->id)->first();
						if ($dok_usulan) {
							$dok_usulan->id_jadwal = $request->input('id_jadwal');
							$dok_usulan->jenis_ujian = 'instansi';
							$dok_usulan->save();
						}
						
						$id_status = 13;
						$tanggal_ujian_rw = $request->input('tanggal_ujian');
						$lokasi_ujian_rw = $request->input('lokasi_ujian');
						$status = "Menunggu Tes Tertulis";
						
						$peserta_sts = Peserta::find($datas);
						$dokumen_usulan = SuratUsulan::where('id_peserta',$datas)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();
						
						if($peserta_sts->status_inpassing == 2) {
							$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap" ;
							$peserta_sts->status_inpassing = 2;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');                        
							$peserta_sts->save();
						} elseif ($peserta_sts->status_inpassing == 1 ) {
							$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
							$peserta_sts->status_inpassing = 1;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');                    
							$peserta_sts->save();
						} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")) {
							$peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
							$peserta_sts->status_inpassing = 16;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							$peserta_sts->save();
						} elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
							$peserta_sts->status = $status;
							$peserta_sts->status_inpassing = $id_status;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');                    
							$peserta_sts->save();
						}
					}
				} elseif($cek_peserta == 0) {
                    if($jumlah_peserta >= $maks){
						return Redirect::to('tambah-peserta-ujian-instansi/'.$id)->with('msg','penuh');
					}

                    $data = new PesertaInstansi();
                    $data->id_jadwal = $request->input('id_jadwal');
                    $data->id_peserta = $datas;
                    $data->id_admin_ppk = $id_admin;
                    $data->nama_peserta = $peserta->nama;
                    $data->no_ujian = substr($peserta->no_sertifikat,0,9);
                    $data->metode_ujian = 'tes';
                    $data->status = 1;
                    if($data->save()){
						$dok_usulan = SuratUsulan::where('id_peserta',$datas)->where('id_jadwal',0)->where('jenis_ujian',null)->where('id_admin_ppk',Auth::user()->id)->first();
						if($dok_usulan) {
							$dok_usulan->id_jadwal = $request->input('id_jadwal');
							$dok_usulan->jenis_ujian = 'instansi';
							$dok_usulan->save();
						}
						
						$id_status = 13;
						$tanggal_ujian_rw = $request->input('tanggal_ujian');
						$lokasi_ujian_rw = $request->input('lokasi_ujian');
						$status = "Menunggu Tes Tertulis";
						
						$peserta_sts = Peserta::find($datas);
						$dokumen_usulan = SuratUsulan::where('id_peserta',$datas)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();
						
						if($peserta_sts->status_inpassing == 2) {
							$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap" ;
							$peserta_sts->status_inpassing = 2;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							$peserta_sts->save();
						} elseif ($peserta_sts->status_inpassing == 1 ) {
							$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
							$peserta_sts->status_inpassing = 1;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							$peserta_sts->save();
						} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")) {
							$peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
							$peserta_sts->status_inpassing = 16;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							$peserta_sts->save();
						} elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
							$peserta_sts->status = $status;
							$peserta_sts->status_inpassing = $id_status;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							$peserta_sts->save();
						}                   
						
						$riwayat = new RiwayatUser();
						$riwayat->id_user = $datas;
						$riwayat->id_admin = Auth::user()->id;
						$riwayat->perihal = "jadwal_instansi";
						$riwayat->description = "didaftarkan ujian dengan metode Tes Tertulis tanggal ".Helper::tanggal_indo($tanggal_ujian_rw)." oleh";
						$riwayat->tanggal = Carbon::now()->toDateString(); 
						$riwayat->save();
						$msg = "berhasil";
					}
				} else {
					if($jumlah_peserta >= $maks){
						return Redirect::to('tambah-peserta-ujian-instansi/'.$id)->with('msg','penuh');
					}
					$peserta_jadwal = PesertaInstansi::where('id_jadwal',$request->input('id_jadwal'))
									  ->where('id_peserta',$datas)->where('status',2)->first();
					$pesertajadwal = PesertaInstansi::find($peserta_jadwal->id);
					$pesertajadwal->status = 1;
					if($pesertajadwal->save()){
						$dok_usulan = SuratUsulan::where('id_peserta',$datas)->where('id_jadwal',0)->where('jenis_ujian',null)->where('id_admin_ppk',Auth::user()->id)->first();
						if ($dok_usulan) {
							$dok_usulan->id_jadwal = $request->input('id_jadwal');
							$dok_usulan->jenis_ujian = 'instansi';
							$dok_usulan->save();
						}
						
						$id_status = 13;
						$tanggal_ujian_rw = $request->input('tanggal_ujian');
						$lokasi_ujian_rw = $request->input('lokasi_ujian');
						$status = "Menunggu Tes Tertulis";
						
						$peserta_sts = Peserta::find($datas);
						$dokumen_usulan = SuratUsulan::where('id_peserta',$datas)->where('id_jadwal',$request->input('id_jadwal'))->where('id_admin_ppk',Auth::user()->id)->first();
						
						if($peserta_sts->status_inpassing == 2) {
							$peserta_sts->status = "Dokumen Persyaratan Tidak Lengkap" ;
							$peserta_sts->status_inpassing = 2;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');                        
							$peserta_sts->save();
						} elseif ($peserta_sts->status_inpassing == 1 ) {
							$peserta_sts->status = "Menunggu Verifikasi Dokumen Persyaratan";
							$peserta_sts->status_inpassing = 1;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');                    
							$peserta_sts->save();
						} elseif (($peserta_sts->status_inpassing == 7 && $dokumen_usulan->file != "") || ($peserta_sts->status_inpassing == 11 && $dokumen_usulan->file != "")) {
							$peserta_sts->status = "Menunggu Verifikasi Surat Usulan";
							$peserta_sts->status_inpassing = 16;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');
							$peserta_sts->save();
						} elseif ($peserta_sts->status_inpassing != 1 || $peserta_sts->status_inpassing != 2 || $peserta_sts->status_inpassing != 7 || $peserta_sts->status_inpassing != 11) {
							$peserta_sts->status = $status;
							$peserta_sts->status_inpassing = $id_status;
							$peserta_sts->tanggal_ujian_terakhir = $request->input('tanggal_ujian');                    
							$peserta_sts->save();
						}

						$riwayat = new RiwayatUser();
						$riwayat->id_user = $datas;
						$riwayat->id_admin = Auth::user()->id;
						$riwayat->perihal = "jadwal_instansi";
						$riwayat->description = "didaftarkan ujian dengan metode Tes Tertulis tanggal ".Helper::tanggal_indo($tanggal_ujian_rw)." oleh";
						$riwayat->tanggal = Carbon::now()->toDateString(); 
						$riwayat->save();
						$msg = "berhasil";
					}
				}
				$msg = "berhasil";
			}
		}
		
		$msg = "berhasil";
		return Redirect::to('daftar-instansi')->with('msg',$msg);
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
	public function show($id)
    {
        $data = DB::table('peserta_instansis')
				->join('pesertas', 'peserta_instansis.id_peserta','=','pesertas.id')
				->join('dokumens', 'pesertas.id_dokumen','=','dokumens.id')
				->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
				->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id','left')
				->join('users as verifikator', 'pesertas.assign','=','verifikator.id','left')
				->join('users as asesor', 'pesertas.asesor','=','asesor.id','left')
				->select('pesertas.*','peserta_instansis.metode_ujian as metodes','peserta_instansis.id as ids','dokumens.pas_foto_3_x_4 as fotos', 'instansis.nama as nama_instansis','verifikator.name as verifikators','asesor.name as asesors','jadwal_instansis.metode as metodes','jadwal_instansis.tanggal_ujian as tgl_uji','pesertas.id as id_pesertas','peserta_instansis.id_jadwal as jadwal_peserta','peserta_instansis.status_ujian as status','pesertas.status as statuss','peserta_instansis.publish')
				->where('peserta_instansis.id_jadwal',$id)
				->where('peserta_instansis.status',1)
				->groupBy('peserta_instansis.id')
				->get();
        
		$admin_bangprof = DB::table('users')->where('role','verifikator')->where('deleted_at',null)->pluck('name','id');
        $jadwal = DB::table('jadwal_instansis')->where('id',$id)->first();
        return View::make('list_peserta_instansi', compact('data','admin_bangprof','jadwal','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
    public function edit($id)
    {
        $data = DB::table('jadwal_instansis')
				->join('users', 'jadwal_instansis.id_admin_ppk','=','users.id')
				->join('instansis','users.nama_instansi','=','instansis.id','left')
				->select('jadwal_instansis.*','users.nip as nips', 'users.name as names', 'instansis.nama as instansis')
				->where('jadwal_instansis.id',$id)
				->first();
        
		return View::make('verifikasi_permohonan_instansi', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

		if ($request->input('rekomendasi') == 'setuju') {
			$rules = array('batas_waktu_input' => 'required');    
		} else {
			$rules = array('') ;
		}
	  
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()) {
			return Redirect::to('verifikasi-permohonan-instansi/'.$id)
					->withErrors($validator)
					->withInput();
		}

        $data = JadwalInstansi::find($id);
        $data->status_permohonan = $request->input('rekomendasi');
        $metode = $request->input('metode');
        
		if($request->input('rekomendasi') == 'setuju'){
			if ($request->input('tanggal_ujian') != "") {
				$tgl_tes = explode('/',$request->input('tanggal_ujian'));
				$tgl_tes_expl = $tgl_tes[2].'-'.$tgl_tes[1].'-'.$tgl_tes[0];
			}
			
			$batas_waktu_input = explode('/',$request->input('batas_waktu_input'));
			$tgl_bts_expl = $batas_waktu_input[2].'-'.$batas_waktu_input[1].'-'.$batas_waktu_input[0];
			
			if($metode == "tes_tulis") {
				if($tgl_tes_expl < $tgl_bts_expl) {
					return Redirect::to('verifikasi-permohonan-instansi/'.$id)
							->with('msg','gagal')
							->withInput();
				}
			} elseif ($metode == "verifikasi_portofolio") {
				if ($tgl_tes_expl < $tgl_bts_expl ) {
					return Redirect::to('verifikasi-permohonan-instansi/'.$id)
							->with('msg','gagal')
							->withInput();            
				}
			}
			
			$data->batas_waktu_input = $tgl_bts_expl;
		} else {
			$data->batas_waktu_input = null;
		}
		
		$data->di_setujui_oleh = Auth::user()->id;
		
		if($data->save()){
			return Redirect::to('jadwal-inpassing-instansi')->with('msg','berhasil');
		} else {
			return Redirect::to('jadwal-inpassing-instansi')->with('msg','gagal');
		}
	}

	public function inputHasil($nip,$id){
		$data = DB::table('peserta_instansis')
				->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id','left')
				->join('pesertas','peserta_instansis.id_peserta','=','pesertas.id')
				->join('dokumens', 'peserta_instansis.id_peserta','=','dokumens.id','left')
				->select('peserta_instansis.*','pesertas.*','dokumens.*','pesertas.nama as namas','jadwal_instansis.*','pesertas.status as statuss')
				->where('peserta_instansis.id',$id)
				->first();

		return View::make('input_hasil_peserta', compact('data'));
	}

	public function storeHasil(Request $request,$nip,$id){
		$data = PesertaInstansi::find($id);
		$jadwalpeserta = JadwalInstansi::where('id',$data->id_jadwal)->first();
		$metode = "Tes Tertulis";
		$data->status_ujian = $request->input('rekomendasi');
		$data->publish = $request->input('publish');
		$data->admin_update = Auth::user()->id;
		
		if($data->save()){
			$namaInstansi = DB::table('peserta_instansis')
							->join('users','users.id','=','peserta_instansis.id_admin_ppk','left')
							->join('instansis','instansis.id','=','users.nama_instansi','left')
							->select('peserta_instansis.*','instansis.*','users.*','instansis.nama as namainstansi','users.name as namappk')
							->where('peserta_instansis.id',$id)
							->first();
			$from = env('MAIL_USERNAME');
			$datamail = array('rekomendasi' => $request->input('rekomendasi'), 'email' => Auth::user()->email, 'name' => Auth::user()->name, 'from' => $from, 'nip' => $nip, 'nama_peserta' => $request->input('nama_peserta'),'metode' => $metode,'tanggal_batas' => $jadwalpeserta->tanggal_ujian,'nama_ppk' => $namaInstansi->namappk);
			$admin_superadmin = DB::table('users')->where('role','superadmin')->get();
			$admin_dsp = DB::table('users')->where('role','dsp')->get();
			$admin_bangprof = DB::table('users')->where('role','bangprof')->get();
			$admin_instansi = DB::table('users')->where('id', $data->id_admin_ppk)->get();
			
			if (Auth::user()->role == 'dsp' || Auth::user()->role == 'superadmin') {
				if ($request->input('publish') != "" ){
					foreach($admin_superadmin as $superadmins){
						Mail::send('mail.hasiltes_superadmin', $datamail, function($message) use ($datamail,$superadmins) {
							$message->to($superadmins->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
							$message->from($datamail['from'],$datamail['name']);
						});
					}
					
					foreach($admin_bangprof as $bangprofs){
						Mail::send('mail.hasiltes_bangprof', $datamail, function($message) use ($datamail,$bangprofs) {
							$message->to($bangprofs->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
							$message->from($datamail['from'],$datamail['name']);
						});
					}
					
					foreach($admin_dsp as $dsps){
						Mail::send('mail.hasiltes_dsp', $datamail, function($message) use ($datamail,$dsps) {
							$message->to($dsps->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
							$message->from($datamail['from'],$datamail['name']);
						});
					}
					
					foreach($admin_instansi as $instansis){
						Mail::send('mail.hasiltes_instansi', $datamail, function($message) use ($datamail,$instansis) {
							$message->to($instansis->email)->subject('Hasil Sidang Pleno Penyesuaian/Inpassing JF PPBJ');
							$message->from($datamail['from'],'Admin LKPP');
						});
					}
				}
			}

			$status_ujian = $request->input('rekomendasi');
			if($status_ujian == 'lulus'){
				$id_status = 6;
				$status = "Lulus";
			}
			
			if($status_ujian == 'tidak_lulus'){
				$id_status = 7;
				$status = "Tidak Lulus";
			}
			
			if($status_ujian == 'tidak_lengkap'){
				$id_status = 17;
				$status = "Dokumen Persyaratan Tidak Lengkap";
			}

			if($status_ujian == ''){
				$id_status = 4;
				$status = "Menunggu Verifikasi Portofolio atau Uji Tulis";
			}

			$status_rekomendasi = $request->input('rekomendasi');
			if($status_rekomendasi == 'lulus') {
				$status_riwayat = 'Lulus Tes Tertulis';
			}
			
			if ($status_rekomendasi == 'tidak_lulus') {
				$status_riwayat = 'Tidak Lulus Tes Tertulis';
			}
			
			if ($status_rekomendasi == 'tidak_hadir') {
				$status_riwayat = 'Tidak Hadir Tes Tertulis';
			}
			
			if ($status_rekomendasi == 'tidak_lengkap'){
				$status_riwayat = "Dokumen Persyaratan Tidak Lengkap";
			}
			
			if ($status_rekomendasi == '') {
				$status_riwayat = '';
			}

			if ($request->input('publish') != ""){
				$peserta = Peserta::find($data->id_peserta);
				$peserta->status = $status;
				$peserta->status_inpassing = $id_status;
				$peserta->save();
			}

			if ($request->input('publish') != ""){
				$riwayat = new RiwayatUser();
				$riwayat->id_user = $data->id_peserta;
				$riwayat->id_admin = Auth::user()->id;
				$tanggal_ujian_rw = $request->input('tanggal_ujian');
				$riwayat->perihal = "hasil_tes_dsp";
				$riwayat->description = 'dinyatakan '.$status_riwayat.' tanggal '.$tanggal_ujian_rw;
				$riwayat->tanggal = Carbon::now()->toDateString(); 
				$riwayat->save();
			} else {
				$riwayat = new RiwayatUser();
				$riwayat->id_user = $data->id_peserta;
				$riwayat->id_admin = Auth::user()->id;
				$riwayat->perihal = "hasil_tes";
				$riwayat->description = "di rekomendasikan ".$status_riwayat.' tanggal '.$tanggal_ujian_rw;
				$riwayat->tanggal = Carbon::now()->toDateString(); 
				$riwayat->save();
			}
			return Redirect::to('lihat-peserta-instansi/'.$data->id_jadwal)->with('msg','berhasil');
		} else {
			return Redirect::to('lihat-peserta-instansi/'.$data->id_jadwal)->with('msg','gagal');
		}
	}

	public function printAbsensi($id){
		require_once getcwd() . '/vendor/autoload.php';
		
		$data = DB::table('peserta_instansis')
				->join('pesertas', 'peserta_instansis.id_peserta','=','pesertas.id')
				->join('dokumens', 'peserta_instansis.id_peserta','=','dokumens.id')
				->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
				->select('pesertas.*','peserta_instansis.metode_ujian as metodes','peserta_instansis.id as ids','peserta_instansis.no_ujian','dokumens.pas_foto_3_x_4 as fotos', 'instansis.nama as nama_instansis')
				->where('peserta_instansis.id_jadwal',$id)
				->get();	

		$jadwal = JadwalInstansi::find($id);
		$penyelengara = 'Instansi';	    	
		$mpdf = new \Mpdf\Mpdf(['format' => 'A4-L']);
		$mpdf->WriteHTML(View::make('pdf.cetak_absensi_reguller', compact('data','jadwal','penyelengara')));
		$mpdf->Output();
	}

	public function upHasilUjian(Request $request,$id){
		$jadwal = JadwalInstansi::find($id);
		return View::make('up_data_hasil_ujian_instansi', compact('jadwal'));
    }

	public function StoreupHasilUjian(Request $request,$id){     
		$rules = array('hasil_ujian' => 'mimes:jpg,png,jpeg,pdf|min:100|max:2048');
		$validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
			return Redirect::to('up-hasil-ujian-instansi/'.$id)
					->withErrors($validator)
					->withInput();
		}

        if ($request->hasFile('hasil_ujian')) {
            $hasil_ujian = $request->file('hasil_ujian');
            $hasil_ujian_name = Carbon::now()->timestamp ."_hasil_ujian_instansi".".". $hasil_ujian->getClientOriginalExtension();
            $hasil_ujianPath = 'storage/data/hasil_ujian/instansi';
            $hasil_ujian->move($hasil_ujianPath,$hasil_ujian_name);
        } else {
            $hasil_ujian_name = null;
        }

        $dokumen = JadwalInstansi::find($id);
        $dokumen->hasil_ujian = $hasil_ujian_name;
        
		if($dokumen->save()){
             $msg = "berhasil";
        } else {
             $msg = "gagal";
        }

        return Redirect::to('jadwal-inpassing-instansi')->with('msg',$msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function unggahUsulan($id,$peserta)
    {   
        $datapeserta = DB::table('pesertas')
						->select('pesertas.*')
						->where('id',$peserta)
						->get();
        
		$jadwal = DB::table('jadwals')
				  ->select('jadwals.*')
				  ->where('id',$id)
				  ->get();
        
		$dokumen = DB::table('surat_usulans')
  				   ->select('surat_usulans.*')
				   ->where('id_peserta',$peserta)
				   ->where('id_jadwal',$id)
				   ->where('jenis_ujian','instansi')
				   ->where('id_admin_ppk',Auth::user()->id)
				   ->get();

        return View::make('unggah_surat_usulan_int', compact('datapeserta','jadwal','dokumen'));
    }

    public function unggahStoreUsulan(Request $request,$peserta){   
        $id_jadwal = $request->input('id_jadwal');
        $id_peserta = $request->input('id_peserta');
        $id_admin_ppk = Auth::user()->id;
        $peserta = Peserta::find($id_peserta);
        $cek_peserta_tdf = PesertaInstansi::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->where('status',1)->first();
        $cek_peserta = PesertaInstansi::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->first();

        $rules = array('no_surat_usulan_peserta' => 'required',
					   'surat_usulan_peserta' => 'mimes:jpg,jpeg,pdf|min:100|max:2048|required');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('unggah-surat-usulan-int/'.$id_jadwal.'/'.$id_peserta)
					->withErrors($validator)
					->withInput();
        }

        if ($request->hasFile('surat_usulan_peserta')) {
            $surat_usulan_peserta = $request->file('surat_usulan_peserta');
            $surat_usulan_peserta_name = Carbon::now()->timestamp ."_surat_usulan_peserta".".". $surat_usulan_peserta->getClientOriginalExtension();
            $surat_usulan_pesertaPath = 'storage/data/surat_usulan_inpassing';
            $surat_usulan_peserta->move($surat_usulan_pesertaPath,$surat_usulan_peserta_name);
        } else {
            $surat_usulan_peserta_name = null;
        }
        
        $dokumen = SuratUsulan::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->where('id_admin_ppk',$id_admin_ppk)->first();
        $pesertaNosurat = Peserta::find($id_peserta);
        if ($dokumen != "") {
            $dokumen->file = $surat_usulan_peserta_name;
            $dokumen->no_surat_usulan_peserta = $request->input('no_surat_usulan_peserta');
            $pesertaNosurat->no_surat_usulan_peserta = $request->input('no_surat_usulan_peserta');
            $pesertaNosurat->save();
            $dokumen->save();
        } else {
            $dokumensurat = New SuratUsulan;
            $dokumensurat->id_admin_ppk = $id_admin_ppk;   
            $dokumensurat->id_peserta = $id_peserta;   
            $dokumensurat->id_jadwal = $id_jadwal;
            $dokumensurat->jenis_ujian = 'instansi';
            $dokumensurat->file = $surat_usulan_peserta_name;
            $dokumensurat->no_surat_usulan_peserta = $request->input('no_surat_usulan_peserta');
            $pesertaNosurat->no_surat_usulan_peserta = $request->input('no_surat_usulan_peserta');
            $pesertaNosurat->save();
            $dokumensurat->save();
        }

        if (is_null($cek_peserta)){
			$data = new PesertaInstansi();
        
			//Ini yang perlu diperbaiki
			$data->id_jadwal = $request->input('id_jadwal');
			$data->id_peserta = $id_peserta;
			$data->id_admin_ppk = Auth::user()->id;
			$data->nama_peserta = $peserta->nama;
			$data->no_ujian = substr($peserta->no_sertifikat,0,9);
        
			if ($request->input('metode') == 'verifikasi_portofolio') {
				$data->metode_ujian = 'verifikasi';
			} elseif($request->input('metode') == 'tes_tulis'){
				$data->metode_ujian = 'tes';    
			}
        
			$data->status=2;
			if($data->save()){
				$msg = "berhasil";
			} else {
				$msg = "gagal";
			}
		} elseif ($cek_peserta_tdf != ""){
			$data = PesertaInstansi::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->where('status',1)->first();        
			
			//Ini yang perlu diperbaiki
			$data->id_jadwal = $request->input('id_jadwal');
			$data->id_peserta = $id_peserta;
			$data->id_admin_ppk = Auth::user()->id;
			$data->nama_peserta = $peserta->nama;
			
			if ($request->input('metode') == 'verifikasi_portofolio') {
				$data->metode_ujian = 'verifikasi';
			} elseif($request->input('metode') == 'tes_tulis'){
				$data->metode_ujian = 'tes';    
			}
		
			$data->status = 1;
			if($data->save()){
				$msg = "berhasil";
			} else {
				$msg = "gagal";
			}
		} elseif ($cek_peserta != "") {
			$data = PesertaInstansi::where('id_peserta',$id_peserta)->where('id_jadwal',$id_jadwal)->first();        
			
			//Ini yang perlu diperbaiki
			$data->id_jadwal = $request->input('id_jadwal');
			$data->id_peserta = $id_peserta;
			$data->id_admin_ppk = Auth::user()->id;
			$data->nama_peserta = $peserta->nama;
			
			if ($request->input('metode') == 'verifikasi_portofolio') {
				$data->metode_ujian = 'verifikasi';
			} elseif($request->input('metode') == 'tes_tulis'){
				$data->metode_ujian = 'tes';    
			}
		
			$data->status = 2;
			if($data->save()){
				$msg = "berhasil";
			} else {
				$msg = "gagal";
			}        
		}

        return Redirect::to('unggah-surat-usulan-int/'.$id_jadwal.'/'.$id_peserta)->with('msg',$msg);
    }

    public function destroy($id)
    {
        if (Auth::user()->role == 'asesor' || Auth::user()->role == 'verifikator' || Auth::user()->role == 'ppk'){
            return Redirect::back();
        }

        $data = JadwalInstansi::find($id);
        $data->deleted_at = Carbon::today();
        $data->status_permohonan = null;
        $data->di_setujui_oleh = null;
        $pesertajadwal = PesertaInstansi::where('id_jadwal',$id)->first();
        if($pesertajadwal){
            $pesertajadwal->status = 0;
        }
        
		if ($data->save() || $pesertajadwal->save()) {
            return Redirect::to('jadwal-inpassing-instansi')->with('msg','berhasil_hapus');
        } else {
            return Redirect::to('jadwal-inpassing-instansi')->with('msg','gagal_hapus');
        }
    }
	
    public function printExcell($id)
    {
        $jadwal = DB::table('jadwal_instansis')->where('id',$id)->first();
        
		if ($jadwal->metode == 'tes_tulis') {
            $tanggal_jadwal = Helper::tanggal_indo($jadwal->tanggal_tes);
        } else {
            $tanggal_jadwal = Helper::tanggal_indo($jadwal->tanggal_verifikasi);
        }
        
		$nama_excel = 'list peserta jadwal instansi '.$tanggal_jadwal.'.xlsx';
        return Excel::download(new PesertaInstansiExport($id), $nama_excel);
    }
}