<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\VerifikasiPortofolio;
use App\PesertaJadwal;
use Redirect;
use View;
use Validator;
use DB;

class VerifikasiPortofolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('jadwals')->orderBy('id','DESC')->get();
        return View::make('tampilan_verifikasi', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $rules = array(
            'id_peserta' => 'required',
            'id_jadwal' => 'required',
            'nama_peserta'    => 'required',
            'asesor_verifikasi'    => 'required',
            'rekomendasi'    => 'required',
            'asesor_pleno_1'    => 'required',
            'asesor_pleno_2'    => 'required',
            'keputusan_pleno'    => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('edit-hasil-pleno/'.$id)
                ->withErrors($validator)
                ->withInput();
        }

        $data = new VerifikasiPortofolio();
        $data->id_peserta = $request->input('id_peserta');
        $data->id_peserta_jadwal = $id;
        $data->id_jadwal = $request->input('id_jadwal');
        $data->nama_peserta = $request->input('nama_peserta');
        $data->asesor_verifikasi = $request->input('asesor_verifikasi');
        $data->rekomendasi = $request->input('rekomendasi');
        $data->asesor_pleno_1 = $request->input('asesor_pleno_1');
        $data->asesor_pleno_2 = $request->input('asesor_pleno_2');
        $data->keputusan_pleno = $request->input('keputusan_pleno');
        $data->admin_post = Auth::user()->name;
        if($data->save()){
            $peserta = PesertaJadwal::find($id);
            $peserta->hasil_pleno = $request->input('keputusan_pleno');
            $peserta->save();
            return Redirect::to('verifikasi-portofolio-detail/'.$request->input('id_jadwal'))->with('msg','berhasil');
        }else{
            return Redirect::to('verifikasi-portofolio-detail/'.$request->input('id_jadwal'))->with('msg','gagal');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('peserta_jadwals')
        ->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
        ->select('peserta_jadwals.*','pesertas.nip as nips','pesertas.jenjang as jenjangs','pesertas.status as statuss')
        ->where('id_jadwal',$id)
        ->where('peserta_jadwals.metode_ujian','verifikasi')
        ->get();
        return View::make('tanggal_verifikasi', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('peserta_jadwals')
        ->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
        ->select('peserta_jadwals.*','pesertas.nip as nips','pesertas.jenjang as jenjangs','pesertas.status as statuss','pesertas.nama as namas','pesertas.id as ids')
        ->where('peserta_jadwals.id',$id)
        ->first();

        $asesor = DB::table('asesors')->pluck('name','name');
        return View::make('edit_pleno', compact('data','asesor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
