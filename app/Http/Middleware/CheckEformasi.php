<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use DB;
use Redirect;

class CheckEformasi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = Auth::user()->id;

        $data1 = DB::table('pengusulan_eformasis')
        ->join('users', 'pengusulan_eformasis.id_admin_ppk', '=', 'users.id','left')
        ->select('pengusulan_eformasis.*', 'users.email as emails', 'users.nama_instansi as instansis', 'users.no_telp as no_telps')
        ->where('id_admin_ppk',$id)
        ->where('pengusulan_eformasis.deleted_at',null)
        ->orderBy('pengusulan_eformasis.id', 'DESC')
        ->first();

        if($data1 != ""){
        if(in_array('tidak_setuju',[$data1->status_muda,$data1->status_madya,$data1->status_pertama])){
            return Redirect::to('pengusulan-eformasi')->with('msg','tidak_setuju');
        }
            if(in_array('',[$data1->status_muda,$data1->status_madya,$data1->status_pertama])){
                return Redirect::to('pengusulan-eformasi')->with('msg','tidak_setuju');
            }
        }else{
            return Redirect::to('pengusulan-eformasi')->with('msg','tidak_setuju');  
        }

        return $next($request);
    }
}
