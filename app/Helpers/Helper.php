<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

class Helper
{
    public static function shout(string $string)
    {
        return strtoupper($string);
    }

    public static function tanggal_indo(string $tanggal,$cetak_hari = false)
    {
        $bulan = array (
            1 =>'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
       $hari = array ( 1 =>    'Senin',
                'Selasa',
                'Rabu',
                'Kamis',
                'Jumat',
                'Sabtu',
                'Minggu'
            );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
        $anInt = intval($pecahkan[2]);
        $tgl_indo = $anInt . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
        if ($cetak_hari) {
        $num = date('N', strtotime($tanggal));
        return $hari[$num] . ', ' . $tgl_indo;
    }
    return $tgl_indo;
    }
	
	public static function getGenderStatus($gender = null){
		$gender = isset($gender) ? (string) $gender : "";
		
		if($gender == "pria"){
			return "Laki-laki";
		} elseif($gender == "wanita") {
			return "Perempuan";
		} else {
			return "N/A";
		}
	}
	
	public static function getStatusPermohonan($status = null){
		
		if($status == ""){
			return "Belum Dilihat";
		} elseif($status == "setuju"){
			return "Setuju";
		} else {
			return "Tidak Setuju";
		}
	}
	
	public static function getPublishJadwal($status_publish = null){
		
		if($status_publish == "ya"){
			return "Ya";
		} else {
			return "Tidak";
		}
	}
	
	public static function getNamaJenjang($kode_jenjang = null){		
		switch ($kode_jenjang) {
			case "1":
				return "Pertama";
			break;
			case "2":
				return "Muda";
			break;
			case "3":
				return "Madya";
			break;
			default:
				return "N/A";
		}
	}
	
	public static function getStatusActive($status = null){
		
		if($status == "aktif"){
			return "Aktif";
		} elseif($status == "tidak_aktif") {
			return "Tidak Aktif";
		} else {
			return "-";
		}
	}
	
	public static function getFormDate($tanggal = null){
		$tanggal = explode('-', $tanggal);
		return $tanggal[2].'/'.$tanggal[1].'/'.$tanggal[0];
	}
	
	public static function getDBFormat($tanggal = null){
		$tanggal = explode('/', $tanggal);
		return $tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0];
	}
	
	public static function getMetodeInpassing($metode = null){
		$metode = isset($metode) ? (string) $metode : "";
		
		if(isset($metode)){
			if($metode == "tes_tulis"){
				return "Tes Tertulis";
			} else {
				return "Verifikasi Portofolio";
			}
		} else {
			return "N/A";
		}
	}
	
	public static function getBiodataPage(){
		return url("biodata");		
	}
	
	public static function getCurrentDateTime(){
		return date('Y-m-d H:i:s');
	}
	
	public static function getStatusUjian($status_ujian = null){				
		if($status_ujian == "lulus"){
			return "Lulus";
		} else {
			return "Tidak Lulus";
		}
	}
	
	public static function getStatusEformasi($status_eformasi = null){				
		if(isset($status_eformasi)){
			if($status_eformasi == "setuju"){
				return "Disetujui";
			} else {
				return "Tidak Disetujui";
			}
		} else {
			return "N/A";
		}
	}
	
	public static function getReadbleDateFormat($tanggal=null){
		if(isset($tanggal)){
			$ubah = gmdate($tanggal, time() + 60 * 60 * 8);
			$pecah = explode("-", $ubah);
			if (!isset($pecah[2]))
				return null;
			$tanggal = $pecah[2];
			$bulan = Helper::bulan($pecah[1]);
			$tahun = $pecah[0];
			return $tanggal . ' ' . $bulan . ' ' . $tahun;
		}
		else{
			return "";
		}
	}
	
	
	public static function bulan($bln) {
        switch ($bln) {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
            default:
                return false;
        }
    }
}