<?php

namespace App\Exports;

use App\Peserta;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Shared\Date;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use DB;

class DataRiwayatUjianExport  extends DefaultValueBinder implements FromView,ShouldAutoSize,WithCustomValueBinder
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
    	$riwayat = DB::table('riwayat_users')
                    ->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
                    ->join('peserta_jadwals', 'riwayat_users.id_user','=','peserta_jadwals.id_peserta','left')
                    ->join('dokumens', 'riwayat_users.id_user','=','dokumens.id_peserta','left')
                    ->join('instansis', 'pesertas.nama_instansi','=','instansis.id','left')
                    ->join('jadwals', 'jadwals.id','=','peserta_jadwals.id_jadwal','left')
                    ->join('users', 'riwayat_users.id_admin','=','users.id','left')
                    ->join('users as admin', 'riwayat_users.id_admin_lkpp','=','admin.id','left')
                    ->leftJoin('surat_usulans', function($leftJoin){
                        $leftJoin->on('peserta_jadwals.id_jadwal', '=', 'surat_usulans.id_jadwal')
                                ->on('peserta_jadwals.id_peserta', '=', 'surat_usulans.id_peserta')
                                ->on('surat_usulans.jenis_ujian', '=', DB::Raw("'regular'"));
                    })
                    ->select('riwayat_users.*','pesertas.nama as nama_peserta','pesertas.nip as nip','users.name as nama_admin','admin.name as admin_lkpp','jadwals.tanggal_ujian as tgl_ujian','peserta_jadwals.status_ujian as hasil_ujian','pesertas.jenjang as jenjang','peserta_jadwals.metode_ujian as metode','peserta_jadwals.no_ujian as no_ujian','instansis.nama as nama_instansi','dokumens.*','peserta_jadwals.publish as publish','surat_usulans.*','surat_usulans.file as file')
                    ->where('peserta_jadwals.publish','publish') 
                    ->groupby('peserta_jadwals.id') 
                    ->orderby('peserta_jadwals.id','asc')
                    ->get();

        $riwayat_int = DB::table('riwayat_users')
                        ->join('pesertas', 'riwayat_users.id_user','=','pesertas.id')
                        ->join('peserta_instansis', 'riwayat_users.id_user','=','peserta_instansis.id_peserta','left')
                        ->join('dokumens', 'riwayat_users.id_user','=','dokumens.id_peserta','left')
                        ->join('instansis', 'pesertas.nama_instansi','=','instansis.id','left')
                        ->join('jadwal_instansis', 'jadwal_instansis.id','=','peserta_instansis.id_jadwal','left')
                        ->join('users', 'riwayat_users.id_admin','=','users.id','left')
                        ->join('users as admin', 'riwayat_users.id_admin_lkpp','=','admin.id','left')
                        ->leftJoin('surat_usulans', function($leftJoin){
                            $leftJoin->on('peserta_instansis.id_jadwal', '=', 'surat_usulans.id_jadwal')
                                    ->on('peserta_instansis.id_peserta', '=', 'surat_usulans.id_peserta')
                                    ->on('surat_usulans.jenis_ujian', '=', DB::Raw("'instansi'"));
                        })
                        ->select('riwayat_users.*','pesertas.nama as nama_peserta','pesertas.nip as nip','users.name as nama_admin','admin.name as admin_lkpp','jadwal_instansis.tanggal_ujian as tgl_ujian','peserta_instansis.status_ujian as hasil_ujian','pesertas.jenjang as jenjang','peserta_instansis.metode_ujian as metode','peserta_instansis.no_ujian as no_ujian','instansis.nama as nama_instansi','dokumens.*','peserta_instansis.publish as publish','surat_usulans.*','surat_usulans.file as file')        
                        ->where('peserta_instansis.publish','publish')  
                        ->groupby('peserta_instansis.id') 
                        ->orderby('peserta_instansis.id','asc')
                        ->get();

        $merged = $riwayat->merge($riwayat_int);
        $datanih = $merged->all();

        $data = array(
        	'data_riwayat' => $datanih
        );
        return view('excel.data_riwayat_ujian', $data);
    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    // public function columnFormats(): array
    // {
    //     return [
    //         'C' => NumberFormat::FORMAT_TEXT,
    //     ];
    // }
}
