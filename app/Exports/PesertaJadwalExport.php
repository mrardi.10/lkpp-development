<?php

namespace App\Exports;

use App\Peserta;
use App\user;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use DB;
use Carbon\Carbon;

class PesertaJadwalExport extends DefaultValueBinder implements FromView,ShouldAutoSize,WithCustomValueBinder
{
    use Exportable;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $id = $this->id;
        $sekarang = Carbon::now()->toDateString(); 
        $data = DB::table('peserta_jadwals')
        ->join('pesertas', 'peserta_jadwals.id_peserta','=','pesertas.id')
        ->join('dokumens', 'pesertas.id_dokumen','=','dokumens.id')
        ->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
        ->join('jadwals','peserta_jadwals.id_jadwal','=','jadwals.id','left')
        ->join('users as verifikator', 'pesertas.assign','=','verifikator.id','left')
        ->join('users as asesor', 'peserta_jadwals.asesor','=','asesor.id','left')
        ->select('pesertas.*','peserta_jadwals.metode_ujian as metodes','peserta_jadwals.id as ids','dokumens.pas_foto_3_x_4 as fotos', 'instansis.nama as nama_instansis','verifikator.name as verifikators','asesor.name as asesors','peserta_jadwals.id_jadwal as jadwal_peserta' ,'jadwals.tanggal_ujian as tgl_ujian','peserta_jadwals.id_admin_ppk as admin_ppk','jadwals.id as jadwalregular','jadwals.metode as metodess','pesertas.id as id_pesertas','peserta_jadwals.status_ujian as status_jadwal','pesertas.status as status','peserta_jadwals.publish')
        ->where('peserta_jadwals.id_jadwal',$id)
        ->where('peserta_jadwals.status','1')
        ->groupBy('peserta_jadwals.id')
        ->get();
         $get_ases = User::pluck('name','id');
        // dd($data);
        $admin_bangprof = DB::table('users')->where('role','verifikator')->where('deleted_at',null)->pluck('name','id');
        $asesor = DB::table('users')->where('role','asesor')->where('deleted_at',null)->pluck('name','id');
        $jadwal = DB::table('jadwals')->where('id',$id)->first();
        $jadwalAvailable = DB::table('jadwals')->whereDate('batas_waktu_input','>',$sekarang)->where('deleted_at',null)->get();
        $datas = array(
            'sekarang' => $sekarang,
            'data' => $data,
            'jadwal' => $jadwal,
            'jadwalAvailable' => $jadwalAvailable,
            'get_ases' => $get_ases
        );
        return view('excel.peserta_jadwal', $datas);
    }

      public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    // public function columnFormats(): array
    // {
    //     return [
    //         'C' => NumberFormat::FORMAT_NUMBER,
    //     ];
    // }
}
