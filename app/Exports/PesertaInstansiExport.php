<?php

namespace App\Exports;

use App\Peserta;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithDrawings;
use DB;
use Carbon\Carbon;

class PesertaInstansiExport implements FromView,ShouldAutoSize, WithColumnFormatting
{
    use Exportable;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $id = $this->id;
        $data = DB::table('peserta_instansis')
                ->join('pesertas', 'peserta_instansis.id_peserta','=','pesertas.id')
                ->join('dokumens', 'pesertas.id_dokumen','=','dokumens.id')
                ->join('instansis','pesertas.nama_instansi','=','instansis.id','left')
                ->join('jadwal_instansis','peserta_instansis.id_jadwal','=','jadwal_instansis.id','left')
                ->join('users as verifikator', 'pesertas.assign','=','verifikator.id','left')
                ->join('users as asesor', 'pesertas.asesor','=','asesor.id','left')
                ->select('pesertas.*','peserta_instansis.metode_ujian as metodes','peserta_instansis.id as ids','dokumens.pas_foto_3_x_4 as fotos', 'instansis.nama as nama_instansis','verifikator.name as verifikators','asesor.name as asesors','jadwal_instansis.metode as metodes','jadwal_instansis.tanggal_ujian as tgl_uji','pesertas.id as id_pesertas','peserta_instansis.id_jadwal as jadwal_peserta','peserta_instansis.status_ujian as status','pesertas.status as statuss','peserta_instansis.publish')
                ->where('peserta_instansis.id_jadwal',$id)
                ->where('peserta_instansis.status',1)
                ->groupBy('peserta_instansis.id')
                ->get();
        
        $jadwal = DB::table('jadwals')->where('id',$id)->first();
        $datas = array(
            'data' => $data,
            'jadwal' => $jadwal
        );
        return view('excel.peserta_instansi', $datas);
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }
}
