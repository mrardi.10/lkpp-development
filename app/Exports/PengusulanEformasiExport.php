<?php

namespace App\Exports;

use App\PengusulanEformasi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithDrawings;
use DB;

class PengusulanEformasiExport implements FromView,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
    	$eformasi = DB::table('pengusulan_eformasis')
            ->join('users', 'pengusulan_eformasis.id_admin_ppk', '=', 'users.id')
            ->join('instansis', 'users.nama_instansi', '=', 'instansis.id')
            ->select('pengusulan_eformasis.*', 'users.email as emails', 'users.no_telp as no_telps', 'users.username as usernames', 'instansis.nama as instansis','users.name as namas')
            ->orderBy('pengusulan_eformasis.updated_at', 'DESC')
            ->where('pengusulan_eformasis.deleted_at',null)
            ->get();
        $data = array(
        	'eformasi' => $eformasi
        );
        return view('excel.pengusulan_eformasi', $data);
    }
}
